<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width"/>
	</head>
	<body style="margin: 0; padding: 0; font-family: sans-serif; font-weight: lighter;">
		<div style="margin: 20px auto; padding: 0 20px; max-width: 500px; width: 100%; border: 1px solid #ecf0f1;">
			<header style="margin: 0 -20px; padding-top: 15px; padding-bottom: 15px; color: #2c3e50; background-color: #ecf0f1; font-size: 24px; font-weight: lighter; text-align:center">New feedback from <?php echo $user_name ?></header>
			<p style="margin: 0 -20px; padding: 10px 20px 10px; border-bottom: 1px solid #ecf0f1">Name: <?php echo $user_name ?><br>
			   E-mail: <?php echo $user_email ?><br>
			   IP: <?php echo $user_ip ?></p>
			<p>Message:</p>
			<div class="message" style="margin: 10px; font-family: monospace;"><?php echo $user_message ?></div>
			<p>To answer this user need only respond to this email.</p>
		</div>
	</body>
</html>