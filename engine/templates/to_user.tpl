<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width"/>
	</head>
	<body style="margin: 0; padding: 0; font-family: sans-serif; font-weight: lighter;">
		<div style="margin: 20px auto; padding: 0 20px; max-width: 500px; width: 100%; border: 1px solid #ecf0f1;">
			<header style="margin: 0 -20px; padding-top: 15px; padding-bottom: 15px; color: #2c3e50; background-color: #ecf0f1; font-size: 24px; font-weight: lighter; text-align:center">Thanks for feedback, <?php echo $user_name ?></header>
			<p style="margin: 0 -20px; padding: 10px 20px 10px; border-bottom: 1px solid #ecf0f1">We all really very important for us your feedback. We will reply to you as fast as we can. Remember that you are our favorite visitor.</p>
			<p>Your message:</p>
			<div class="message" style="margin: 10px; font-family: monospace;"><?php echo $user_message ?></div>
			<p>If you forget something, you can reply to this email and add what you want to say.</p>
		</div>
	</body>
</html>