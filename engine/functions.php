<?php

# Email check
function email_check( $email ) {
	return filter_var( $email, FILTER_VALIDATE_EMAIL );
}

# Config get
function conf( $name ) {
	global $conf;
	return $conf[ $name ];
}

function response( $status, $body = false ) {
	return json_encode( array( 'status' => $status, 'data' => $body ) );
}