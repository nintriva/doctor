/* 
	author: istockphp.com
*/
jQuery(function($) {
	
	$("a.topopup").click(function() {
			loading(); // loading
			setTimeout(function(){ // then show popup, deley in .5 second
				loadPopup(); // function show popup 
			}, 500); // .5 second
	return false;
	});
	$("a.topopup2").click(function() {
			loading(); // loading
			setTimeout(function(){ // then show popup, deley in .5 second
				loadPopup2(); // function show popup 
			}, 500); // .5 second
	return false;
	});
	$("a.emailpopup").live('click',function() {
			loading(); // loading
			var reminder_index = $(this).parent().index('div.email_sms');
			var reminder_hidden_id = $('.reminder_hidden_class').eq(reminder_index).val();
			reminderVal(reminder_hidden_id,'Email','view');
			setTimeout(function(){ // then show popup, deley in .5 second
				loadPopup11(); // function show popup 
			}, 500); // .5 second
	return false;
	});	
	$("a.editEmailpopup").live('click',function() {
			loading(); // loading
			var reminder_index = $(this).parent().index('div.email_sms');
			$('#reminder_index_id').val(reminder_index);
			$('#email_after_save').hide();
			$('#email_edit_content').show();
			$('#email_save_reset').show();
			var reminder_hidden_id = $('.reminder_hidden_class').eq(reminder_index).val();
			reminderVal(reminder_hidden_id,'Email','edit');
			setTimeout(function(){ // then show popup, deley in .5 second
				loadPopup12(); // function show popup 
			}, 500); // .5 second
	return false;
	});
	$("a.remindpopup").live('click',function() {
			loading(); // loading
			var reminder_index = $(this).parent().index('div.email_sms');
			var reminder_hidden_id = $('.reminder_hidden_class').eq(reminder_index).val();
			reminderVal(reminder_hidden_id,'SMS','view');
			setTimeout(function(){ // then show popup, deley in .5 second
				loadPopup9(); // function show popup 
			}, 500); // .5 second
	return false;
	});
	$("a.editSMSpop").live('click',function() {
			loading(); // loading
			var reminder_index = $(this).parent().index('div.email_sms');
			$('#reminder_index_id').val(reminder_index);
			$('#sms_after_save').hide();
			$('#sms_edit_content').show();
			$('#sms_save_reset').show();
			var reminder_hidden_id = $('.reminder_hidden_class').eq(reminder_index).val();
			reminderVal(reminder_hidden_id,'SMS','edit');
			setTimeout(function(){ // then show popup, deley in .5 second
				loadPopup10(); // function show popup 
			}, 500); // .5 second
	return false;
	});
	$("a.addPopup").click(function() {
			loading(); // loading
			setTimeout(function(){ // then show popup, deley in .5 second
				loadPopup3(); // function show popup 
			}, 500); // .5 second
	return false;
	});
	
	$("a.langPopup").click(function() {
			loading(); // loading
			setTimeout(function(){ // then show popup, deley in .5 second
				loadPopup4(); // function show popup 
			}, 500); // .5 second
	return false;
	});
	$("a.specialityPopup").click(function() {
			loading(); // loading
			setTimeout(function(){ // then show popup, deley in .5 second
				loadPopupSpeciality(); // function show popup 
			}, 500); // .5 second
	return false;
	});
	$("a.conditionPopup").click(function() {
			loading(); // loading
			setTimeout(function(){ // then show popup, deley in .5 second
				loadPopupCondition(); // function show popup 
			}, 500); // .5 second
	return false;
	});
	$("a.procedurePopup").click(function() {
			loading(); // loading
			setTimeout(function(){ // then show popup, deley in .5 second
				loadPopupProcedure(); // function show popup 
			}, 500); // .5 second
	return false;
	});
	$("a.videoPopup").click(function() {
			loading(); // loading
			setTimeout(function(){ // then show popup, deley in .5 second
				loadPopupVideo(); // function show popup 
			}, 500); // .5 second
	return false;
	});
	$("a.plan1Popup").click(function() {
			loading(); // loading
			setTimeout(function(){ // then show popup, deley in .5 second
				loadPopup5(); // function show popup 
			}, 500); // .5 second
	return false;
	});
	$("a.plan_popup_click").click(function() {
			loading(); // loading
			var sel_ind = $("a.plan_popup_click").index(this);
			setTimeout(function(){ // then show popup, deley in .5 second
				loadPopupPlan(sel_ind); // function show popup 
			}, 500); // .5 second
	return false;
	});
	$("a.plan2Popup").click(function() {
			loading(); // loading
			setTimeout(function(){ // then show popup, deley in .5 second
				loadPopup6(); // function show popup 
			}, 500); // .5 second
	return false;
	});
	$("a.plan3Popup").click(function() {
			loading(); // loading
			setTimeout(function(){ // then show popup, deley in .5 second
				loadPopup7(); // function show popup 
			}, 500); // .5 second
	return false;
	});
	$("a.plan4Popup").click(function() {
			loading(); // loading
			setTimeout(function(){ // then show popup, deley in .5 second
				loadPopup8(); // function show popup 
			}, 500); // .5 second
	return false;
	});
	/* event for close the popup */
	$("div.close").hover(
					function() {
						$('span.ecs_tooltip').hide();
					},
					function () {
    					$('span.ecs_tooltip').hide();
  					}
				);
	
	$("div.close").click(function() {
		disablePopup();  // function close pop up
	});
	$("div.close_sms").click(function() {
		disablePopup();  // function close pop up
	});
	$(".cancel_button").click(function() {
		disablePopup();  // function close pop up
	});
	$(".cancel_button_scp").live('click',function() {
		disablePopup();  // function close pop up
		removeSCP();
	});
	$(".language_save_button").click(function() {
		disablePopup();  // function close pop up
		languageUpdate();
	});
	$(".speciality_save_button").click(function() {
		disablePopup();  // function close pop up
		//specialityUpdate();
		specialityConProUpdate();
	});
	$(".condition_save_button").click(function() {
		disablePopup();  // function close pop up
		conditionUpdate();
	});
	$(".procedure_save_button").click(function() {
		disablePopup();  // function close pop up
		procedureUpdate();
	});
	$(".insurance_save_button").click(function() {
		disablePopup();  // function close pop up
		insuranceUpdate();
	});
	$(".plan_save_button").click(function() {
		var sel_ind = $("input.plan_save_button").index(this);
		disablePopup();  // function close pop up
		planUpdate(sel_ind);
	});
	$(this).keyup(function(event) {
		if (event.which == 27) { // 27 is 'Ecs' in the keyboard
			disablePopup();  // function close pop up
		}  	
	});
	
	$("div#backgroundPopup").click(function() {
		disablePopup();  // function close pop up
	});
	
	$('a.livebox').click(function() {
		alert('Hello World!');
	return false;
	});
	

	 /************** start: functions. **************/
	function loading() {
		$("div.loader").show();  
	}
	function closeloading() {
		$("div.loader").fadeOut('normal');  
	}
	
	var popupStatus = 0; // set value
	
	function loadPopup() { 
		if(popupStatus == 0) { // if value is 0, show popup
			closeloading(); // fadeout loading
			$("#toPopup").fadeIn(0500);// fadein popup div
			$("#backgroundPopup").css("opacity", "0.7"); // css opacity, supports IE7, IE8
			$("#backgroundPopup").fadeIn(0001); 
			popupStatus = 1; // and set value to 1
		}	
	}
		
	function disablePopup() {
		if(popupStatus == 1) { // if value is 1, close popup
			$("#toPopup").fadeOut("normal"); 
			$("#toPopup2").fadeOut("normal");
			$("#addPopup").fadeOut("normal");
			$("#langPopup").fadeOut("normal");
			$("#plan1Popup").fadeOut("normal"); 
			$("#plan2Popup").fadeOut("normal"); 
			$("#plan3Popup").fadeOut("normal");
			$("#plan4Popup").fadeOut("normal");
			$("#backgroundPopup").fadeOut("normal"); 
			$(".planOfPopup").fadeOut("normal");  
			$("#specialityPopup").fadeOut("normal"); 
			$("#conditionPopup").fadeOut("normal");
			$("#procedurePopup").fadeOut("normal");
			$("#videoPopup").fadeOut("normal");
			$("#remindpop").fadeOut("normal");
			$("#editpopup").fadeOut("normal");
			$("#emailpop").fadeOut("normal");
			$("#EditEmailpop").fadeOut("normal");
			popupStatus = 0;  // and set value to 0
		}
	}
	
	function loadPopup2() { 
		if(popupStatus == 0) { // if value is 0, show popup
			closeloading(); // fadeout loading
			
			$("#toPopup2").fadeIn(0500);// fadein popup div
			$("#backgroundPopup").css("opacity", "0.7"); // css opacity, supports IE7, IE8
			$("#backgroundPopup").fadeIn(0001); 
			popupStatus = 1; // and set value to 1
		}	
	}
	
	function loadPopup3() { 
		if(popupStatus == 0) { // if value is 0, show popup
			closeloading(); // fadeout loading
			$("#addPopup").fadeIn(0500); // fadein popup div
			$("#backgroundPopup").css("opacity", "0.7"); // css opacity, supports IE7, IE8
			$("#backgroundPopup").fadeIn(0001); 
			popupStatus = 1; // and set value to 1
		}	
	}
	
	function loadPopup4() { 
		if(popupStatus == 0) { // if value is 0, show popup
			closeloading(); // fadeout loading
			$("#langPopup").fadeIn(0500); // fadein popup div
			$("#backgroundPopup").css("opacity", "0.7"); // css opacity, supports IE7, IE8
			$("#backgroundPopup").fadeIn(0001); 
			popupStatus = 1; // and set value to 1
		}	
	}
	
	function loadPopupCondition() { 
		if(popupStatus == 0) { // if value is 0, show popup
			closeloading(); // fadeout loading
			$("#conditionPopup").fadeIn(0500); // fadein popup div
			$("#backgroundPopup").css("opacity", "0.7"); // css opacity, supports IE7, IE8
			$("#backgroundPopup").fadeIn(0001); 
			popupStatus = 1; // and set value to 1
		}	
	}
	
	function loadPopupProcedure() { 
		if(popupStatus == 0) { // if value is 0, show popup
			closeloading(); // fadeout loading
			$("#procedurePopup").fadeIn(0500); // fadein popup div
			$("#backgroundPopup").css("opacity", "0.7"); // css opacity, supports IE7, IE8
			$("#backgroundPopup").fadeIn(0001); 
			popupStatus = 1; // and set value to 1
		}	
	}
	
	function loadPopupVideo() { 
		if(popupStatus == 0) { // if value is 0, show popup
			closeloading(); // fadeout loading
			$("#videoPopup").fadeIn(0500); // fadein popup div
			$("#backgroundPopup").css("opacity", "0.7"); // css opacity, supports IE7, IE8
			$("#backgroundPopup").fadeIn(0001); 
			popupStatus = 1; // and set value to 1
		}	
	}
	
	function loadPopupSpeciality() { 
		if(popupStatus == 0) { // if value is 0, show popup
			closeloading(); // fadeout loading
			$("#specialityPopup").fadeIn(0500); // fadein popup div
			$("#backgroundPopup").css("opacity", "0.7"); // css opacity, supports IE7, IE8
			$("#backgroundPopup").fadeIn(0001); 
			popupStatus = 1; // and set value to 1
		}	
	}
	
	function loadPopup5() { 
		if(popupStatus == 0) { // if value is 0, show popup
			closeloading(); // fadeout loading
			$("#plan1Popup").fadeIn(0500); // fadein popup div
			$("#backgroundPopup").css("opacity", "0.7"); // css opacity, supports IE7, IE8
			$("#backgroundPopup").fadeIn(0001); 
			popupStatus = 1; // and set value to 1
		}	
	}
	
	function loadPopupPlan(id) { 
		if(popupStatus == 0) { // if value is 0, show popup
			closeloading(); // fadeout loading
			$(".planOfPopup").eq(id).fadeIn(0500); // fadein popup div
			$("#backgroundPopup").css("opacity", "0.7"); // css opacity, supports IE7, IE8
			$("#backgroundPopup").fadeIn(0001); 
			popupStatus = 1; // and set value to 1
		}	
	}
	
	function loadPopup6() { 
		if(popupStatus == 0) { // if value is 0, show popup
			closeloading(); // fadeout loading
			$("#plan2Popup").fadeIn(0500); // fadein popup div
			$("#backgroundPopup").css("opacity", "0.7"); // css opacity, supports IE7, IE8
			$("#backgroundPopup").fadeIn(0001); 
			popupStatus = 1; // and set value to 1
		}	
	}
	function loadPopup7() { 
		if(popupStatus == 0) { // if value is 0, show popup
			closeloading(); // fadeout loading
			$("#plan3Popup").fadeIn(0500); // fadein popup div
			$("#backgroundPopup").css("opacity", "0.7"); // css opacity, supports IE7, IE8
			$("#backgroundPopup").fadeIn(0001); 
			popupStatus = 1; // and set value to 1
		}	
	}
	function loadPopup8() { 
		if(popupStatus == 0) { // if value is 0, show popup
			closeloading(); // fadeout loading
			$("#plan4Popup").fadeIn(0500); // fadein popup div
			$("#backgroundPopup").css("opacity", "0.7"); // css opacity, supports IE7, IE8
			$("#backgroundPopup").fadeIn(0001); 
			popupStatus = 1; // and set value to 1
		}	
	}
	function loadPopup9() { 
		if(popupStatus == 0) { // if value is 0, show popup
			closeloading(); // fadeout loading
			$("#remindpop").fadeIn(0500); // fadein popup div
			$("#backgroundPopup").css("opacity", "0.7"); // css opacity, supports IE7, IE8
			$("#backgroundPopup").fadeIn(0001); 
			popupStatus = 1; // and set value to 1
		}	
	}
	function loadPopup10() { 
		if(popupStatus == 0) { // if value is 0, show popup
			closeloading(); // fadeout loading
			$("#editpopup").fadeIn(0500); // fadein popup div
			$("#backgroundPopup").css("opacity", "0.7"); // css opacity, supports IE7, IE8
			$("#backgroundPopup").fadeIn(0001); 
			popupStatus = 1; // and set value to 1
		}	
	}
	function loadPopup11() { 
		if(popupStatus == 0) { // if value is 0, show popup
			closeloading(); // fadeout loading
			$("#emailpop").fadeIn(0500); // fadein popup div
			$("#backgroundPopup").css("opacity", "0.7"); // css opacity, supports IE7, IE8
			$("#backgroundPopup").fadeIn(0001); 
			popupStatus = 1; // and set value to 1
		}	
	}
	function loadPopup12() { 
		if(popupStatus == 0) { // if value is 0, show popup
			closeloading(); // fadeout loading
			$("#EditEmailpop").fadeIn(0500); // fadein popup div
			$("#backgroundPopup").css("opacity", "0.7"); // css opacity, supports IE7, IE8
			$("#backgroundPopup").fadeIn(0001); 
			popupStatus = 1; // and set value to 1
		}	
	}
	/************** end: functions. **************/
}); // jQuery End