var regexZip = /^\d{5}$/;
var regexPhone = /\W?\d\d\d\W?\d\d\d\W?\d\d\d\d/;

var regexEmail = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/;
var regexText = /^[a-zA-Z,\s]+(([\'\,\.\-][a-zA-Z,\s])?[a-zA-Z,\s][\.\,]*)*$/;   // /^\s*[a-zA-Z,\s]+\s*$/;
var regexNumber = /^[-+]?\d*\.?\d*$/;
var regexUrl = /^[a-zA-Z0-9\-\.]+\.(com|org|net|mil|edu|gov|COM|ORG|NET|MIL|EDU|GOV)$/;
var regexTxtNum = /^([1-zA-Z0-1@.\s]{1,255})$/;  // validates against illegal characters
var regexTxtNumQuote = /^[a-zA-Z0-9\s.\",-_'&%!@#$*()+?]+$/;  // validates against illegal characters but allows quotes, hyphen, apostrophe, comma, dash, spaces
var regex;

var sEvar4global = '';

// old phone regex: /\W?\d\d\d\W?\d\d\d\W?\d\d\d\d/;
// new phone regex: /^([\(]{1}[0-9]{3}[\)]{1}[0-9]{3}[\-]{1}[0-9]{4})$/;

validateControl = function(sender, args) {
    var elem = document.getElementById(sender.id);
    //alert(elem.id);
    args.IsValid = false;
    if (validateSubmit(args.Value, '', elem.exType, elem.exInvalid, elem.exValid)) {
        args.IsValid = true;

    };
    return args.IsValid;
};

function validateSubmit(exToValidate, errHolder, exType, exInvalid, exValid) {

    if (exInvalid == '' && exToValidate == '') {
        if (errHolder != '') {
            document.getElementById(errHolder).style.display = 'inline';
        };
        return false;
    };

    if (exValid != '' && exValid == exToValidate) {
        return true;
    };

    if (exToValidate == exInvalid && exInvalid != '') {
        if (errHolder != '') {
            document.getElementById(errHolder).style.display = 'inline';
        };
        return false;
    };


    switch (exType) {
        case 'zip':
            regex = regexZip;
            break;
        case 'phone':
            regex = regexPhone;
            var ph = exToValidate.replace(/-/gi, '').replace('(', '').replace(')', '').length;
            if (ph != 10 && exToValidate != exValid) {
                if (errHolder != '') {
                    document.getElementById(errHolder).style.display = 'inline';
                };
                return false;
            };
            break;
        case 'email':
            regex = regexEmail;
            break;
        case 'text':
            regex = regexText;
            break;
        case 'number':
            regex = regexNumber;
            break;
        case 'url':
            regex = regexUrl;
            break;
        case 'txtNum':
            regex = regexTxtNum;
            break;
        case 'txtNumQuote':
            regex = regexTxtNumQuote;
            break;
        case 'any':
            return true;
            break;
    }

    if (regex.test(exToValidate) == false) {
        if (errHolder != '') {
            document.getElementById(errHolder).style.display = 'inline';
        }
        return false;
    }
    return true;
};

function Watermark(txtField, bFocus, sWatermarkText, errHolder) {
    document.getElementById(txtField).value = trim(document.getElementById(txtField).value);
    ClearErrorHolder(errHolder)
    if (bFocus) {
        if (document.getElementById(txtField).value == sWatermarkText) {
            document.getElementById(txtField).value = '';
        };
    }
    else {
        if (document.getElementById(txtField).value == '') {
            document.getElementById(txtField).value = sWatermarkText;
        };
    };
};

function ClearErrorHolder(errHolder) {
    if (errHolder != '') {
        document.getElementById(errHolder).style.display = 'none';
    };
};

function isNumberKey(evt, buttonName) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode == 45) {
        //return true;
        return false; //Ticket 1800D-3756 
    }
    else if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    else if (charCode == 13) {
        if (buttonName != '') {
            document.getElementById(buttonName).click();
        }
        return false;
    }
    return true;
};


//Bill 10/26/09 - PT 19367 
function checkZipState(controlClientId, controlClientName, controlServerName, validation1, validation2, sZip, sCity, sState, spReqFieldZip, spReqFieldCity, spReqFieldState) {

    var bReturn = validation1 && validation2;

    //alert('sZip:' + sZip);
    //alert('sCity:' + sCity);
    //alert('sState:' + sState);

    if (sCity != 'Enter your City' && sZip == 'Enter your ZIP code') {

        document.getElementById(spReqFieldZip).style.display = 'none';

        if (validation2 == 1) {
            if (sState == '') {
                bReturn = 0;
                document.getElementById(spReqFieldState).style.display = 'inline';
                //alert('bReturn:' + bReturn);
                alert('Please select a State');
                return false;
            }
            else {
                bReturn = 1;
                document.getElementById(spReqFieldState).style.display = 'none';
                //alert('bReturn:' + bReturn);
            };
        };

    } else {

        if (validation1 == 1) {
            bReturn = 1;
        } else {
            alert('Please Enter a valid Zip Code');
        };

    };


    return bReturn;
}


//Bill 9/30/09 remove the leading 0's and 1's for phone number
function trimLeadZero(s) {
    while (s.substr(0, 1) == '0' || s.substr(0, 1) == '1' && s.length > 1) {
        s = s.substr(1, 9999);
        //alert(s);
    }
    return s;
}


//dennis 9/18/09 remove the leading 0 for phone number
function isPhoneNumberKey(evt, buttonName, vtext) {
    try {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (vtext == '' && charCode == 48) {
            return false;
        }
        if (charCode == 45) {
            //return true;
            return false; //Ticket 1800D-3756 
        }
        else if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        else if (charCode == 13) {
            if (buttonName != '') {
                document.getElementById(buttonName).click();
            }
            return false;
        }
        return true;
    } catch (e)
    { };
};

function isEnterKey(evt, buttonName) {
    var charCode = (evt.which) ? evt.which : event.keyCode

    if (charCode == 13) {
        if (buttonName != '') {
            document.getElementById(buttonName).click();
        }
        return false;
    }
    return true;
};

function ShowCallNow(objId, elementId, vertAdjust, horizAdjust, intCid, intFid, campId, eVar4Global) {
    sEvar4global = eVar4Global;
    cid = intCid;
    campaignId = campId;
    if (intFid != undefined && intFid != '') {
        fid = intFid;
    };
    document.getElementById("trArrow").className = '';
    document.getElementById('txtCallNowName').value = '';
    document.getElementById('txtCallNowPhone').value = '';
    document.getElementById('spCallNowReqName').style.display = 'none';
    document.getElementById('spCallNowReqPhone').style.display = 'none';
    document.getElementById('spCallNowRequestSent2').style.display = 'none';
    document.getElementById('trCallNow').style.display = 'inline';
    ShowElement(objId, elementId, vertAdjust, horizAdjust);
};


function ShowElement(objId, elementId, vertAdjust, horizAdjust) {
    // var cursor = getPosition(event);
    // offsetLeft "X" and offsetTop "Y" return the relative position to the parent node.
    // To get the absolute position (which is in fact a relative position as well, 
    // but to the body element)
    // you need to catch the offset position of all the parents, if any

    var obj = document.getElementById(objId);
    var curleft = curtop = 0;
    if (obj.offsetParent) {
        do {
            curleft += obj.offsetLeft;
            curtop += obj.offsetTop;
        } while (obj = obj.offsetParent);
    }

    curtop += vertAdjust;
    curleft += horizAdjust;

    if (navigator.userAgent.indexOf('Safari') > 0) {
        curleft -= 1;
    }
    else if (navigator.userAgent.indexOf('Firefox') > 0) {
        curleft -= 5;
    }

    document.getElementById(elementId).style.top = curtop + 'px';
    document.getElementById(elementId).style.left = curleft + 'px';
    document.getElementById(elementId).style.display = 'inline';
}

function getPosition(e) {
    e = e || window.event;
    var cursor = { x: 0, y: 0 };
    if (e.pageX || e.pageY) {
        cursor.x = e.pageX; cursor.y = e.pageY;
    }
    else {
        cursor.x = e.clientX + (document.documentElement.scrollLeft || document.body.scrollLeft) - document.documentElement.clientLeft;
        cursor.y = e.clientY + (document.documentElement.scrollTop || document.body.scrollTop) - document.documentElement.clientTop;
    }
    return cursor;
};

function HideElement(elementId) {
    document.getElementById(elementId).style.display = 'none';
};

function NewYorkZip(oZip, controlServerName) {
    var szipcode = oZip.value;
    if (szipcode.length != 5) {

    }
    else {

        //var insOpt = document.getElementById('ctl00_cphMain_sDentalInsPay');
        var insOpt = $get(oZip.id.replace(controlServerName, 'sDentalInsPay')); //Bill 10/26/2010 - PT 25633
        
        if (szipcode == '00501' || szipcode == '00504' || szipcode == '06390' || szipcode == '15000')
        {

            //insOpt[5].disabled = null;
            
            try {
                insOpt.options[8] = null; 
            }
            catch (ex) {
  
            }
            var len = insOpt.length++; // Increase the size of list and return the size
            insOpt.options[len].value = 'Union';
            insOpt.options[len].text = 'Coverage through a Union';
            insOpt[8].disabled = null;


        }
        else {
            if (szipcode.substring(0, 2) == '10' || szipcode.substring(0, 2) == '11' || szipcode.substring(0, 2) == '12' || szipcode.substring(0, 2) == '13' || szipcode.substring(0, 2) == '14') {

                //insOpt[5].disabled = null;
                try {
                    insOpt.options[8] = null; 
                }
                catch (ex) {

                }
                var len = insOpt.length++; // Increase the size of list and return the size
                insOpt.options[len].value = 'Union';
                insOpt.options[len].text = 'Coverage through a Union';
                insOpt[8].disabled = null;

            }
                
            else {
                {

                    //insOpt[5].disabled = 'disabled';
                    //insOpt.options[5] = null;
                    //insOpt[0].selected = 'selected';
                    //insOpt[5].disabled = null;
                    try {
                        insOpt.options[8] = null; 
                    }
                    catch (ex) {

                    }
                    
//                    try {
//                        elSel.add(elOptNew, null); // standards compliant; doesn't work in IE
//                    }
//                    catch (ex) {
//                        elSel.add(elOptNew); // IE only
//                    }


                }
            }
        }
    }
};

function checkrequired(controlClientId, controlClientName, controlServerName, validation1, validation2, sEvent, sEvar4, sEvar5, sEvar6, sEvar17) {

    if (sEvent == undefined) {
        sEvent = '';
    };

    if (sEvar4 == undefined) {
        sEvar4 = '';
    };

    if (sEvar5 == undefined) {
        sEvar5 = '';
    };

    if (sEvar6 == undefined) {
        sEvar6 = '';
    };

    if (sEvar17 == undefined) {
        sEvar17 = '';
    };


    var bReturn = validation1 && validation2;

//    if (document.getElementById('ctl00_cphMain_sDentalInsurance').value == 'Union' && document.getElementById('ctl00_cphMain_iUnion').disabled == true) {
//        ZipInsuranceTypeError(true, 'Union coverage not valid for this zip code.');
//        bReturn = false;
//    };

//    if (document.getElementById('ctl00_cphMain_sDentalInsurance').value == 'Union' && document.getElementById('ctl00_cphMain_iUnion').disabled == false && document.getElementById('ctl00_cphMain_iUnion').selectedIndex == 0) {
//        InsuranceTypeError(true, 'Please, select union insurance carrier.');
//        bReturn = false;
//    };
    


//    var insOpt = document.getElementsByName(controlClientName.replace(controlServerName, 'rbgDentalInsurance'))
//    if (document.getElementById(controlClientId.replace(controlServerName, 'Union')).checked && insOpt[0].checked &&
//         document.getElementById(controlClientId.replace(controlServerName, 'iUnion')).value == 0) {
//        InsuranceTypeError(true, 'Please, select insurance from the list.')
//        document.getElementById(controlClientId.replace(controlServerName, 'iUnion')).focus();
//        bReturn = false;
//    }

//    if (insOpt[0].checked == false && insOpt[1].checked == false) {
//        document.getElementById('payins').style.display = 'none'
//        insOpt[1].checked = true
//    }

//    if (insOpt[0].checked == true) {
//        var bOpt = false;
//        var refOpt = document.getElementsByName(controlClientName.replace(controlServerName, 'sReferralType'));

//        for (var i = 0; i < refOpt.length; i++) {
//            if (refOpt[i].checked) {
//                bOpt = true;
//            }
//        }

//        if (bOpt == false) {
//            bReturn = false;
//            InsuranceTypeError(true, 'Please, select insurance type.')
//        }
//    }
    //alert('bReturn: ' + bReturn);
    viewMore(sEvent, sEvar4, sEvar5, sEvar6, sEvar17, bReturn);

    var d = new Date();
    var t = DateAdd("ms", 1000, d);

    var nd = new Date();
    nd = DateAdd("ms", 0, d);

    while (t > nd) {
        var d = new Date();
        nd = DateAdd("ms", 0, d);
    }

    return bReturn;
}

function DateAdd(timeU, byMany, dateObj) {
    var millisecond = 1;
    var second = millisecond * 1000;
    var minute = second * 60;
    var hour = minute * 60;
    var day = hour * 24;
    var year = day * 365;

    var newDate;
    var dVal = dateObj.valueOf();
    switch (timeU) {
        case "ms": newDate = new Date(dVal + millisecond * byMany); break;
        case "s": newDate = new Date(dVal + second * byMany); break;
        case "mi": newDate = new Date(dVal + minute * byMany); break;
        case "h": newDate = new Date(dVal + hour * byMany); break;
        case "d": newDate = new Date(dVal + day * byMany); break;
        case "y": newDate = new Date(dVal + year * byMany); break;
    }
    return newDate;
}

function dummy() {
    var a = 1;
    var b = 1;
    var c
    c = a + b;
}
function InsuranceTypeError(err, message) {
    if (err) {
//        document.getElementById('payins').style.borderColor = 'Red';
        document.getElementById('spInsTypeError').innerHTML = message;
    }
    else {
//        document.getElementById('payins').style.borderColor = '#B0BDCE';
        document.getElementById('spInsTypeError').innerHTML = message;
    }
}

function ZipInsuranceTypeError(err, message) {
    if (err) {
        document.getElementById('spInsTypeError').innerHTML = message;
    }
}

function GotoDefaultButton(evt, buttonName) {
    var charCode = (evt.which) ? evt.which : event.keyCode;

    if (charCode == 13) {
        if (buttonName != '') {
            document.getElementById(buttonName).click();
        }
        return false;
    }
    return true;
}

//=======TRIM=====
function LTrim(value) {
    var re = /\s*((\S+\s*)*)/;
    return value.replace(re, "$1");
}

function RTrim(value) {
    var re = /((\s*\S+)*)\s*/;
    return value.replace(re, "$1");
}

function trim(value) {
    return LTrim(RTrim(value));
}
//================


//================BEGIN COMMERCIALS ======================
function selectvid() {
    var a = document.getElementById('arc');
    var i = a.selectedIndex;
    if (a.selectedIndex > 0) {
        var v = a.options[i].value;
        var t = a.options[i].text;
        playvid(v);
        AddText('Archives: ' + t);
    };
};

function playvid(v) {

    var newvid = '<embed type="application/x-mplayer2" pluginspage="http://www.microsoft.com/Windows/MediaPlayer/" src="http://swf2.1800dentist.com/wmv/' + v + '" name="Player9" width="350" height="235" autostart="1" showcontrols="1"></embed>';
    document.getElementById('playercontainer').innerHTML = newvid;
    return false;
};

function playvidhome(v) {
    var newvid = '<iframe src="../FLASH/Default.aspx" style="width:177px; height:137px; overflow:hidden; border:0px;"></iframe>';
    document.getElementById('playercontainerhome').innerHTML = newvid;
    return false;
};

function AddText(a) {
    document.getElementById('comTitle').innerText = a;
}
//================END COMMERCIALS ======================

function viewMoreParams(sEvent, sEvar4, sEvar5, sEvar6, sEvar17, bReturn) {

    element_clicked = true;
    // sEvar4 - location
    // sEvar5 - Dental Need
    // sEvar6 - PyamentType
    // sEvar17 - ZipCode

    if (sEvar4 == undefined) {
        sEvar4 = '';
    };

    if (sEvar5 == undefined) {
        sEvar5 = '';
    };

    if (sEvar6 == undefined) {
        sEvar6 = '';
    };

    if (sEvar17 == undefined) {
        sEvar17 = '';
    };

    if (bReturn == undefined) {
        bReturn = true;
    };
    //alert('bReturn: ' + bReturn);
    viewMore(sEvent, sEvar4, sEvar5, sEvar6, sEvar17, bReturn);

    var d = new Date();
    var t = DateAdd("ms", 1000, d);

    var nd = new Date();
    nd = DateAdd("ms", 0, d);

    while (t > nd) {
        var d = new Date();
        nd = DateAdd("ms", 0, d);
    }

    return bReturn;


};


function viewMore(sEvent, sEvar4, sEvar5, sEvar6, sEvar17, bReturn) {
    // sEvar4 - location
    // sEvar5 - Dental Need
    // sEvar6 - PyamentType
    // sEvar17 - ZipCode


    if (sEvar4 == undefined) {
        sEvar4 = '';
    };

    if (sEvar5 == undefined) {
        sEvar5 = '';
    };

    if (sEvar6 == undefined) {
        sEvar6 = '';
    };

    if (sEvar17 == undefined) {
        sEvar17 = '';
    };

    if (sEvar4 != '' && sEvar4global != '' && sEvent == 'event13') {
        sEvar4 = sEvar4global;
    };

    if (bReturn == undefined) {
        bReturn = true;
    };

//    if (sEvar6 == '') {
//        try {
//            switch (document.getElementById('ctl00_cphMain__Home_find_rbInsurance').checked) {
//                case false:
//                    sEvar6 = 'cash_check';
//                    break;
//                case true:
//                    sEvar6 = 'insurance';
//                    var pt = document.getElementsByName('ctl00$cphMain$_Home_find$sReferralType');
//                    for (var i = 0; i < pt.length; i++) {
//                        //alert(pt[i].value);
//                        if (pt[i].checked) {
//                            sEvar6 = pt[i].value;
//                            break;
//                        };
//                    };
//                    break;
//            };
//        }
//        catch (e)
//        { };
//    };

    // var s = s_gi('denbetaadx1800ver3');
    var s = s_gi(s_account);
    var sView = sEvent + ' ' + sEvar4;

//     alert('event: ' + sEvent + ', evar4: ' + sEvar4 +', evar5: ' + sEvar5 + ', evar6: ' + sEvar6 + ', evar17: ' + sEvar17);

    // alert(sView);

    s.eVar4 = sEvar4;
    s.eVar5 = sEvar5;
    s.eVar6 = sEvar6;
    s.eVar17 = sEvar17;

    s.linkTrackVars = 'eVar4,eVar5,eVar6,eVar17,events';
    s.linkTrackEvents = sEvent;

    s.events = sEvent;
    //alert('bReturn: ' + bReturn);
    try {
    if (bReturn == true) {
        s.tl(this, 'o', sView);
        }

        // o - any custom link
        // d - track download link
        // e - exit link
    }
    catch (ex) { };

};

function clickMapOnClick(obj) {
    s_objectID = obj.id;
};

//function printArticle() {
//    alert('Print Article');
//    var s = s_gi('denbetaadx1800ver3');
//    s.linkTrackVars = 'events';
//    s.linkTrackEvents = 'event12';
//    s.events = 'event12';
//    s.tl(this, 'o', 'Print Article');
//}


//================

function ChildAge(iChoice) {
    if (document.getElementById(iChoice).value == 2003) {
        ShowElementChildAge('childage');
    } else {
        HideElement('childage')
    }
}

//function UnionDisplay(sDentalInsurance) {
//    if (document.getElementById(sDentalInsurance).value == 'Union') {
//        ShowElementUnion('lblUnion');
//    } else {
//        HideElement('lblUnion');
//    }
//}


function ChildAge2(iChoice2) {
    if (document.getElementById(iChoice2).value == 2003) {
        ShowElementChildAge('childage2');
    } else {
        HideElement('childage2')
    }
}

function ShowElementChildAge(elementId) {
    document.getElementById(elementId).style.display = 'inline';
};

//function ShowElementUnion(elementId) {
//    document.getElementById(elementId).style.display = 'inline';
//};


//****************COOKIES
function getCookie(c_name) {
    if (document.cookie.length > 0) {
        c_start = document.cookie.indexOf(c_name + "=")
        if (c_start != -1) {
            c_start = c_start + c_name.length + 1
            c_end = document.cookie.indexOf(";", c_start)
            if (c_end == -1) c_end = document.cookie.length
            return unescape(document.cookie.substring(c_start, c_end))
        }
    }
    return ""
}

function setCookie(c_name, value, expiredays) {
    var exdate = new Date()
    exdate.setDate(exdate.getDate() + expiredays)
    document.cookie = c_name + "=" + escape(value) +
       ((expiredays == null) ? "" : ";expires=" + exdate.toGMTString() + ";path=/;")
};
//**************************

//=================PHONE FORMATING=========================================
/// in phone field insert the following
/// onfocus="formatPhone(this)" 
var n;
var p;
var p1;
var phoneField;

function formatPhone(m) {
    n = m.id;
    p1 = m;
    phoneField = n;
    ValidatePhone()
}

function ValidatePhone() {
    if (document.getElementById(n).value != '') {
        p = p1.value
        if (p.length == 3) {

            pp = p;
            d4 = p.indexOf('(')
            d5 = p.indexOf(')')
            if (d4 == -1) {
                pp = "(" + pp;
            }
            if (d5 == -1) {
                pp = pp + ")";
            }

            document.getElementById(phoneField).value = "";
            document.getElementById(phoneField).value = pp;

        }
        if (p.length > 3) {

            d1 = p.indexOf('(')
            d2 = p.indexOf(')')
            if (d2 == -1) {
                l30 = p.length;
                p30 = p.substring(0, 4);

                p30 = p30 + ")"
                p31 = p.substring(4, l30);
                pp = p30 + p31;

                document.getElementById(phoneField).value = ""
                document.getElementById(phoneField).value = pp;
            }
        }

        if (p.length > 5) {
            p11 = p.substring(d1 + 1, d2);

            if (p11.length > 3) {
                p12 = p11;
                l12 = p12.length;
                l15 = p.length

                p13 = p11.substring(0, 3);
                p14 = p11.substring(3, l12);
                p15 = p.substring(d2 + 1, l15);
                document.getElementById(phoneField).value = "";
                pp = "(" + p13 + ")" + p14 + p15;
                document.getElementById(phoneField).value = pp;
            }

            l16 = p.length;
            p16 = p.substring(d2 + 1, l16);
            l17 = p16.length;

            if (l17 > 3 && p16.indexOf('-') == -1) {
                p17 = p.substring(d2 + 1, d2 + 4);
                p18 = p.substring(d2 + 4, l16);
                p19 = p.substring(0, d2 + 1);
                pp = p19 + p17 + "-" + p18;
                document.getElementById(phoneField).value = "";
                document.getElementById(phoneField).value = pp;
            }
        }
        setTimeout(ValidatePhone, 100)
    }
}

function isEmailValid(email) {
    var regexEmail = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/;
    return regexEmail.test(email)
}
//========================END PHONE VALIDATION===========================


//========================POSITIONING SCRIPT===========================
function GetBodyOffsetX(el_name, shift) {

    var x = 0;
    var y = 0;
    var elem = document.getElementById(el_name);

    do {
        x += elem.offsetLeft;
        y += elem.offsetTop;

        if (elem.tagName == "BODY") break;
        elem = elem.offsetParent;

    } while (1 > 0);
    shift[0] = x;
    shift[1] = y;
    return x;
}
function SetPopupOnTarget(target_element, popup_element, coorX, coorY) {
    var shift = new Array(2);
    var pElem = document.getElementById(popup_element)

    GetBodyOffsetX(target_element, shift);
    document.getElementById(popup_element).style.left = shift[0] + coorX + 'px';
    document.getElementById(popup_element).style.top = shift[1] + coorY + 'px';
}
//========================END POSITIONING SCRIPT===========================

//================DREAMWEAVER ROLL-OVER IMAGES SCRIPT======================


function MM_swapImgRestore() { //v3.0
    var i, x, a = document.MM_sr; for (i = 0; a && i < a.length && (x = a[i]) && x.oSrc; i++) x.src = x.oSrc;
}
function MM_preloadImages() { //v3.0
    var d = document; if (d.images) {
        if (!d.MM_p) d.MM_p = new Array();
        var i, j = d.MM_p.length, a = MM_preloadImages.arguments; for (i = 0; i < a.length; i++)
            if (a[i].indexOf("#") != 0) { d.MM_p[j] = new Image; d.MM_p[j++].src = a[i]; } 
    }
}

function MM_findObj(n, d) { //v4.01
    var p, i, x; if (!d) d = document; if ((p = n.indexOf("?")) > 0 && parent.frames.length) {
        d = parent.frames[n.substring(p + 1)].document; n = n.substring(0, p);
    }
    if (!(x = d[n]) && d.all) x = d.all[n]; for (i = 0; !x && i < d.forms.length; i++) x = d.forms[i][n];
    for (i = 0; !x && d.layers && i < d.layers.length; i++) x = MM_findObj(n, d.layers[i].document);
    if (!x && d.getElementById) x = d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
    var i, j = 0, x, a = MM_swapImage.arguments; document.MM_sr = new Array; for (i = 0; i < (a.length - 2); i += 3)
        if ((x = MM_findObj(a[i])) != null) { document.MM_sr[j++] = x; if (!x.oSrc) x.oSrc = x.src; x.src = a[i + 2]; }
}


//================DREAMWEAVER ROLL-OVER IMAGES SCRIPT END==================

//================BEGIN CONTACT US FORM VALIDATION SCRIPT======================
function Feedback(validName, validEmail, validTopicId, validText, name, email, topic, content) {
    var b = validName == true && validEmail == true && validTopicId == true && validText == true;
    if (b) {
        //PageMethods.SendFeedback(name, email, topic, content, OnSuccess, OnError);
        FuturedonticsADX.wsAjaxCalls.SendFeedback(name, email, topic, content, OnSuccess, OnError);
    }
    return false;
}

function OnSuccess(result, userContext, method) {
    switch (method) {
        case 'SendFeedback':
            alert('Thank you. Your feedback has been sent.')
            break;
    }
}

function OnError(err) {
    alert(err.get_message());
}
//================END CONTACT US FORM VALIDATION SCRIPT======================


//========================BEGIN OPTIO MOVIE SCRIPT===========================

function OpenOptio(lang, mod, prod) {
    var strID = '7cb2075ed27dc57d31cb3efa5a713f57';
    var strPkgInfo = 'language=' + lang + '&loadmodule=' + mod + '&pid=' + prod;
    window.open('http://www.optiopublishing.com/media?' + strPkgInfo + '&id=' + strID, 'OptioDentistryWindow', 'toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no,width=640,height=560,bgcolor=#000000');
}

optio_layer = new Array(3);
optio_layer[1] = 'od2_English';
optio_layer[2] = 'ortho_English';

function OptioSetPage(page) {
    for (i = 1; i <= 2; i++) {
        var layer = document.getElementById("optio_" + optio_layer[i]);

        if (layer) {
            if (optio_layer[i] == page) {
                layer.style.visibility = "visible";
                layer.style.height = "auto";
                layer.style.overflow = "visible";
            }
            else {
                layer.style.visibility = "hidden";
                layer.style.height = "1px";
                layer.style.overflow = "hidden";
            }
        }
    }
}

//========================END OPTIO MOVIE SCRIPT=============================


//========================SURVEY=============================================



//========================END SURVEY=========================================

//========================BEGIN CONTACT PAGE=================================
function NothingToDo() {
    return false;
}
//========================ENDCONTACT PAGE====================================

//========================Encyclopedia Article===============================
function OpenFootnote(link) {
    window.open(link, "footnote", "width=600,height=400,top=100,left=100,scrollbars=yes,resizable=1");
    return false;
}
//========================Encyclopedia Article===============================

//========================Get More help Begin ===============================


//========================Get MOre Help End===============================



//============================BEGIN PNG FIX======================================

//window.onLoad = window.setTimeout("pngFix()", 1000);

//function pngFix() {

//    try {

//        var arVersion = navigator.appVersion.split("MSIE")
//        var version = parseFloat(arVersion[1])

//        if ((version >= 5.5) && (document.body.filters)) {
//            for (var i = 0; i < document.images.length; i++) {
//                var img = document.images[i]
//                var imgName = img.src.toUpperCase()
//                if (imgName.substring(imgName.length - 3, imgName.length) == "PNG") {
//                    var imgID = (img.id) ? "id='" + img.id + "' " : ""
//                    var imgClass = (img.className) ? "class='" + img.className + "' " : ""
//                    var imgTitle = (img.title) ? "title='" + img.title + "' " : "title='" + img.alt + "' "
//                    var imgStyle = "display:inline-block;" + img.style.cssText
//                    if (img.align == "left") imgStyle = "float:left;" + imgStyle
//                    if (img.align == "right") imgStyle = "float:right;" + imgStyle
//                    if (img.parentElement.href) imgStyle = "cursor:hand;" + imgStyle
//                    var strNewHTML = "<span " + imgID + imgClass + imgTitle
//         + " style=\"" + "width:" + img.width + "px; height:" + img.height + "px;" + imgStyle + ";"
//         + "filter:progid:DXImageTransform.Microsoft.AlphaImageLoader"
//         + "(src=\'" + img.src + "\', sizingMethod='scale');\"></span>"
//                    img.outerHTML = strNewHTML
//                    i = i - 1
//                }
//            }
//        }
//    }
//    catch (ex) { }
//}

//============================END PNG FIX======================================

//============================BEGIN MSB VIDEOS=================================

function playvidMSBhome(v) {
    var newvid = '<iframe src="http://swf2.1800dentist.com/contests/0909video/my-smile-bites-intro.html" style="width:272px; height:207px; overflow:hidden; border:0px; border-collapse:collapse; position:relative; top:35px; left:405px;"></iframe>';
    document.getElementById('divMSBIntro').innerHTML = newvid;
    return false;
};

//============================END MSB VIDEOS===================================


//========== BEGIN getCookie ==========

if (getCookie('callId') == '') {
    setCookie('callId', '0', '1');
};


var GlobalYear = '<%=Date.Now.Year %>';

//========== END getCookie ==========

//========== BEGIN DentalNewsSignUpSubmit ==========

function DentalNewsSignUpSubmit() {

    //var bFirstNameDN = validateSubmit(document.getElementById('txtFirstNameDN').value, 'spReqFirstNameDN', 'any', 'first name');
    //var bLastNameDN = validateSubmit(document.getElementById('txtLastNameDN').value, 'spReqLastNameDN', 'any', 'last name');
    var bEmailDN = validateSubmit(document.getElementById('txtEmailDN').value, 'spEmailDN', 'email', 'Enter your email');
    var bFirstNameDN = ' '
    var bLastNameDN = ' '

    var sEmailDN
    if (document.getElementById('txtEmailDN').value == 'Enter your email') {
        sEmailDN = ""
    } else { sEmailDN = document.getElementById('txtEmailDN').value; }

    //var bValid = bFirstNameDN && bLastNameDN && bEmailDN
    var bValid = bEmailDN
    if (bValid) {
        FuturedonticsADX.wsAjaxCalls.DentalNewsSignUp(bFirstNameDN, bLastNameDN, document.getElementById('txtEmailDN').value, OnSuccessHelpDN, OnErrorHelpDN);
        document.getElementById('spEmailDN').style.display = 'none';
    }
}

function OnSuccessHelpDN(result, userContext, method) {
    document.getElementById('spMessageDN').style.display = 'block';
    document.getElementById('imgsubmitDN').style.display = 'none';
};

function OnErrorHelpDN(error, context, method) {
    alert(error.get_message())
};

//========== End DentalNewsSignUpSubmit ==========

//============================END PNG FIX======================================

//============================BEGIN Set distance pt.22741=================================

function SetDistance(distance) {
    fDistance = distance;
};

//============================END Set distance pt.22741===================================

//============================Imported from dentistlist.js  pt.23119=================================

var map
var mapCovered = true

function showAll() { //

    mapCovered = false;
    var hLat = document.getElementsByName('hLat');
    var hLong = document.getElementsByName('hLong');
    var hDetails = document.getElementsByName('hDentistDetails');
    map = new YMap(document.getElementById('yMap'), YAHOO_MAP_REG);
    map.addZoomLong();
    map.enableDragMap;


    for (var i = 0; i < hLat.length; i++) {
        var geoPoint = new YGeoPoint(hLat[i].value, hLong[i].value)
        var newMarker = new YMarker(geoPoint, createCustomMarkerImage('images/letter' + i + '.gif'), 1)
        newMarker.addAutoExpand("<div style='width:150px;font-size:11px'>" + hDetails[i].value + "</div>")
        map.addOverlay(newMarker);
        document.getElementById('letter' + i).style.visibility = 'visible';
    }

    var geoPoint = new YGeoPoint(hLat[0].value, hLong[0].value)
    var newMarker = new YMarker(geoPoint, createCustomMarkerImage('images/letter' + 0 + '.gif'), 1)
    newMarker.addAutoExpand("<div style='width:150px;font-size:11px'>" + hDetails[0].value + "</div>")
    map.addOverlay(newMarker);
    document.getElementById('letter' + 0).style.visibility = 'visible';
    map.drawZoomAndCenter(geoPoint, 6)

    //var s = s_gi('denadxprod');
    var s = s_gi(s_account);
    s.linkTrackVars = 'events';
    s.linkTrackEvents = 'event6';
    s.events = 'event6';
    s.tl(this, 'o', 'View Map');
}

function ShowMap(latitude, longtitude, dentistDetails, index) {

    if (mapCovered == false) {
        document.getElementById('cMap').style.backgroundColor = 'white';
        document.getElementById('cMap').style.borderWidth = '1px';

        map = new YMap(document.getElementById('yMap'), YAHOO_MAP_REG);
        //map.addTypeControl();
        map.addZoomLong();
        map.enableDragMap;
        var geoPoint = new YGeoPoint(latitude, longtitude)
        map.drawZoomAndCenter(geoPoint, 6)

        var newMarker = new YMarker(geoPoint, createCustomMarkerImage('img/letter' + index + '.gif'), 1)

        newMarker.addAutoExpand("<div style='width:150px;font-size:11px'>" + dentistDetails + "</div>")
        map.addOverlay(newMarker);

        //var pc = new YCoordPoint(5,40);
        //pc.translate('left','bottom');
        //map.addPanControl(pc);
    }
}

function createCustomMarkerImage(imageUrl) {
    var myImage = new YImage();
    myImage.src = imageUrl;
    myImage.size = new YSize(33, 33);
    myImage.offsetSmartWindow = new YCoordPoint(0, -5);
    return myImage;
}

function CenterIndex(i) {

    map = new YMap(document.getElementById('yMap' + i), YAHOO_MAP_REG);
    map.addZoomLong();
    map.enableDragMap;

    var hLat = document.getElementsByName('hLat');
    var hLong = document.getElementsByName('hLong');
    var hDetails = document.getElementsByName('hDentistDetails');

    var geoPoint = new YGeoPoint(hLat[i].value, hLong[i].value)
    //var newMarker = new YMarker(geoPoint, createCustomMarkerImage('images/letter' + i + '.gif'), 1)
    var newMarker = new YMarker(geoPoint)
    //newMarker.addAutoExpand("<div style='width:150px;font-size:11px'>" + hDetails[i].value + "</div>")
    map.addOverlay(newMarker);
    //document.getElementById('letter' + i).style.visibility = 'visible';
    map.drawZoomAndCenter(geoPoint, 6)

    //var s = s_gi('denadxprod');
    var s = s_gi(s_account);
    s.linkTrackVars = 'events,eVar12';
    s.linkTrackEvents = 'event7';
    s.events = 'event7';
    s.eVar12 = i;
    s.tl(this, 'o', 'Center Map');
}

//============================END======================================


//=============== PT 31064 - phone field validation BEGIN ==============
function stripPhone() {

    var stripped = document.getElementById('txtCallNowPhone').value;

    while ( (stripped.charAt(0) == '1' && stripped.charAt(1) == '-') || (stripped.charAt(0) == '1' && stripped.charAt(1) == '.') )
        stripped = stripped.substr(2);

    stripped = stripped.replace(/\s/g, '');

    while (stripped.charAt(0) == '1' && stripped.length == 11)
        stripped = stripped.substr(1);

    stripped = stripped.replace(/[\(\)\.\-\ ]/g, '');

    //alert('stripped: ' + stripped);

    return stripped;
};

function validatePhone() {

    var error = "";

    var stripped = stripPhone();


    if (stripped == "") {
        error = "Required\n";
        document.getElementById('spCallNowReqPhone').innerHTML = error;
        document.getElementById('spCallNowReqPhone').style.display = 'inline';
        return false;
    } else if (isNaN(parseInt(stripped))) {
        error = "Invalid phone number.\n";
        document.getElementById('spCallNowReqPhone').innerHTML = error
        document.getElementById('spCallNowReqPhone').style.display = 'inline';
        return false;
    } else if (!(stripped.length == 10)) {
        error = "Invalid phone number.\n";
        document.getElementById('spCallNowReqPhone').innerHTML = error
        document.getElementById('spCallNowReqPhone').style.display = 'inline';
        return false;
    }
    return true;
}
//=============== PT 31064 - phone field validation END ================


/////////////////////////////////////
//BEGIN optimizelyBuckets COOKIE PROCESSING///////

function getOptimizelyBuckets() {
    var myOptimizelyEndUserId = getCookie('optimizelyEndUserId');
    myOptimizelyEndUserId = myOptimizelyEndUserId.replace("|", "%7C")
    var myOptimizelyBuckets = getCookie('optimizelyBuckets');
    myOptimizelyBuckets = myOptimizelyBuckets.replace("|", "%7C")
    var myVisitId = getCookie('visitId');

    //check if visit id is integer
    if (!parseInt(myVisitId)) {
        myVisitId = ''
    };

    if (myOptimizelyEndUserId != '' && myOptimizelyBuckets != '' && myVisitId != '') {
        FuturedonticsADX.wsAjaxCalls.UpdateOptimizelyBuckets(myVisitId, myOptimizelyBuckets, myOptimizelyEndUserId)
    };
    //Call Targus
    try
    {
        if (getCookie('callId').length > 1)
        {
            FuturedonticsADX.wsAjaxCalls.ProcessRequest(getCookie('callId'), OnSuccessTargus, OnErrorTargus);
        }
    }
    catch(e) {}
}

function OnSuccessTargus()
{
    FuturedonticsADX.wsAjaxCalls.TargusIcompleteCheck(getCookie('callId'));
}

function OnErrorTargus()
{
    FuturedonticsADX.wsAjaxCalls.TargusIcompleteCheck(getCookie('callId'));
}

//END optimizelyBuckets COOKIE PROCESSING/////////
/////////////////////////////////////

//Safari Bug
if (window.navigator.userAgent.indexOf('Safari') != -1) {
    if (window.navigator.userAgent.indexOf('Chrome') == -1) {
        //alert('safari browser');
        try {
            $(document).ready(function () {
                $("div.searchBox").css("margin-right", "-20px");
            });
        } catch (ex) { };
    };

};



