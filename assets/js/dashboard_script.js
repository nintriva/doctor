/* 
	author: istockphp.com
*/
jQuery(function($) {
	
	$("a.tick").click(function() {
			var app_id = $(this).attr('accesskey');
			$('#hidden_app_id').val(app_id);
			$('#app_title_id').html($('#pres_app_title_id_'+app_id).val());
			loading(); // loading
			setTimeout(function(){ // then show popup, deley in .5 second
				loadPopup(); // function show popup 
			}, 500); // .5 second
	return false;
	});
	
	$("a.cross").click(function() {
			var app_id = $(this).attr('accesskey');
			$('#cancel_hidden_app_id').val(app_id);
			$('#cancel_app_title_id').html($('#pres_app_title_id_'+app_id).val());
			loading(); // loading
			setTimeout(function(){ // then show popup, deley in .5 second
				loadPopup2(); // function show popup 
			}, 500); // .5 second
	return false;
	});
	
	$("a.cancel_view").click(function() {
			var app_id = $(this).attr('accesskey');
			$('#popup_content_val').html($('#cancel_view_val_'+app_id).val());
			loading(); // loading
			setTimeout(function(){ // then show popup, deley in .5 second
				loadPopupCancel(); // function show popup 
			}, 500); // .5 second
	return false;
	});
	
	$("a.popupApp").live('click',function() {
			var time_stamp = $(this).attr('accesskey');
			var address_id = $(this).attr('data-id');
			$('#app_popup_heading').html(time_stamp);
			profileAppList(time_stamp,address_id);
			//$('#popup_content_val').html(app_id);
			loading(); // loading
			setTimeout(function(){ // then show popup, deley in .5 second
				loadPopup(); // function show popup 
			}, 100); // .5 second
	return false;
	});


	$("a.popupApp-profile").live('click',function() {
		var time_stamp = $(this).attr('accesskey');
		var address_id = $(this).attr('data-id');
		$('#app_popup_heading').html(time_stamp);
		profileAppList(time_stamp,address_id);
		//$('#popup_content_val').html(app_id);
		loading(); // loading
		setTimeout(function(){ // then show popup, deley in .5 second
			loadPopup(); // function show popup 
		}, 500); // .5 second
		return false;
	});
	
	/* event for close the popup */
	$("div.close").hover(
					function() {
						$('span.ecs_tooltip').show();
					},
					function () {
    					$('span.ecs_tooltip').hide();
  					}
				);
	
	$("div.close_view").click(function() {
		disablePopup();  // function close pop up
	});
	$("input.cancelbt").click(function() {
		disablePopup();  // function close pop up
	});
	$("input.acceptbt").click(function() {
		appointmentConfirm();
		disablePopup();  // function close pop up
	});
	$("input.acceptbtcancel").click(function() {
		appointmentCancel();
		if($('#mandatory_text').val()!='')
		disablePopup();  // function close pop up
	});
	/*$(this).keyup(function(event) {
		if (event.which == 27) { // 27 is 'Ecs' in the keyboard
			disablePopup();  // function close pop up
		}  	
	});
	
	$("div#backgroundPopup").click(function() {
		disablePopup();  // function close pop up
	});*/
	
	$('a.livebox').click(function() {
		alert('Hello World!');
	return false;
	});
	

	 /************** start: functions. **************/
	function loading() {
		$("div.loader").show();  
	}
	function closeloading() {
		$("div.loader").fadeOut('normal');  
	}
	
	var popupStatus = 0; // set value
	
	function loadPopup() { 
		if(popupStatus == 0) { // if value is 0, show popup
			closeloading(); // fadeout loading
			$("#toPopup").fadeIn(0500); // fadein popup div
			$("#backgroundPopup").css("opacity", "0.7"); // css opacity, supports IE7, IE8
			$("#backgroundPopup").fadeIn(0001); 
			popupStatus = 1; // and set value to 1
		}	
	}
	
	function loadPopup2() { 
		if(popupStatus == 0) { // if value is 0, show popup
			closeloading(); // fadeout loading
			$("#toPopup2").fadeIn(0500); // fadein popup div
			$("#backgroundPopup").css("opacity", "0.7"); // css opacity, supports IE7, IE8
			$("#backgroundPopup").fadeIn(0001); 
			popupStatus = 1; // and set value to 1
		}	
	}
	
	function loadPopupCancel() { 
		if(popupStatus == 0) { // if value is 0, show popup
			closeloading(); // fadeout loading
			$("#toPopup").fadeIn(0500); // fadein popup div
			$("#backgroundPopup").css("opacity", "0.7"); // css opacity, supports IE7, IE8
			$("#backgroundPopup").fadeIn(0001); 
			popupStatus = 1; // and set value to 1
		}	
	}
		
	function disablePopup() {
		if(popupStatus == 1) { // if value is 1, close popup
			$("#toPopup").fadeOut("normal");
			$("#toPopup2").fadeOut("normal");  
			$("#backgroundPopup").fadeOut("normal");  
			popupStatus = 0;  // and set value to 0
		}
	}
	/************** end: functions. **************/
}); // jQuery End