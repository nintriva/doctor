CREATE TABLE IF NOT EXISTS `da_doctor_offers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `doctor_id` int(11) NOT NULL,
  `name` varchar(125) NOT NULL,
  `description` text NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `location` varchar(255) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `status` enum('1','0') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

ALTER TABLE `da_condition` ADD `speciality_id` INT NOT NULL AFTER `id` ;

ALTER TABLE `da_procedure` ADD `speciality_id` INT NOT NULL AFTER `id` ,
ADD `condition_id` INT NOT NULL AFTER `speciality_id` ;

ALTER TABLE `da_procedure` DROP `speciality_id` ;

ALTER TABLE `da_doctor_schedule` ADD `address_id` INT NOT NULL AFTER `name` ;

ALTER TABLE `da_doctor_schedule_time` ADD `on_off` TINYINT(1) NOT NULL AFTER `address_id`;

CREATE TABLE IF NOT EXISTS `da_patient` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(32) NOT NULL,
  `user_email` varchar(32) NOT NULL,
  `password` varchar(32) NOT NULL,
  `user_first_name` varchar(32) NOT NULL,
  `user_last_name` varchar(32) NOT NULL,
  `user_dob` date NOT NULL,
  `user_sex` varchar(10) NOT NULL,
  `entry_type` varchar(5) NOT NULL,
  `visitor` varchar(5) NOT NULL,
  `patient_name` varchar(32) NOT NULL,
  `patient_email` varchar(32) NOT NULL,
  `patient_dob` date NOT NULL,
  `patient_sex` int(11) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

ALTER TABLE `da_patient` CHANGE `patient_sex` `patient_sex` VARCHAR( 11 ) NOT NULL ;

CREATE TABLE IF NOT EXISTS `da_doctor_book` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `book_time` varchar(100) NOT NULL,
  `doctor_id` int(11) NOT NULL,
  `address_id` int(11) NOT NULL,
  `speciality_id` int(11) NOT NULL,
  `procedure_id` int(11) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

ALTER TABLE `da_doctor_book` ADD `book_duration` VARCHAR( 25 ) NOT NULL AFTER `book_time` ;

ALTER TABLE `da_doctor` ADD `google_account` VARCHAR( 125 ) NOT NULL AFTER `password` ;

ALTER TABLE `da_doctor_offers` ADD `address_id` INT NOT NULL AFTER `name` ;

ALTER TABLE `da_doctor_book` ADD `confirm` TINYINT( 1 ) NOT NULL AFTER `status` ;

ALTER TABLE `da_doctor` ADD `middle_name` VARCHAR( 32 ) NOT NULL AFTER `first_name` ;

ALTER TABLE `da_doctor` ADD `fax` VARCHAR( 15 ) NOT NULL AFTER `phone` ;

ALTER TABLE `da_doctor` ADD `website` VARCHAR( 100 ) NOT NULL AFTER `password` ;

ALTER TABLE `da_doctor` ADD `major_activity` VARCHAR( 255 ) NOT NULL AFTER `board_certifications` ,
ADD `practice` VARCHAR( 255 ) NOT NULL AFTER `major_activity` ;

ALTER TABLE `da_doctor` ADD `city` VARCHAR( 32 ) NOT NULL AFTER `addr2` ;

ALTER TABLE `da_doctor` ADD `full_name` VARCHAR( 100 ) NOT NULL AFTER `username` ;

ALTER TABLE `da_doctor` CHANGE `country` `country` VARCHAR( 100 ) NOT NULL ;

ALTER TABLE `da_doctor_book` ADD `comments` VARCHAR( 255 ) NOT NULL AFTER `procedure_id` ;

CREATE TABLE IF NOT EXISTS `da_todo_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `doctor_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(255) NOT NULL,
  `priority` varchar(2) NOT NULL,
  `deadline` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

ALTER TABLE `da_todo_list` CHANGE `deadline` `deadline` DATETIME NOT NULL ;

CREATE TABLE IF NOT EXISTS `da_doctor_review` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `doctor_id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `rating` int(2) NOT NULL,
  `title` varchar(100) NOT NULL,
  `message` varchar(255) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

ALTER TABLE `da_doctor_review` CHANGE `rating` `overall_rating` INT( 2 ) NOT NULL ;

ALTER TABLE `da_doctor_review` ADD `bedside_manner_rating` INT( 2 ) NOT NULL AFTER `overall_rating` ,
ADD `wait_time_rating` INT( 2 ) NOT NULL AFTER `bedside_manner_rating` ;

ALTER TABLE `da_doctor_book` ADD `created_by` VARCHAR( 10 ) NOT NULL AFTER `date_modified` ,
ADD `patient_name` VARCHAR( 32 ) NOT NULL AFTER `created_by` ;

ALTER TABLE `da_patient` ADD `fb_id` VARCHAR( 36 ) NOT NULL AFTER `id` ;

ALTER TABLE `da_doctor_book` ADD `patient_insurance` VARCHAR( 10 ) NOT NULL AFTER `procedure_id` ;

ALTER TABLE `da_patient` ADD `user_phone` VARCHAR( 15 ) NOT NULL AFTER `user_sex` ;

ALTER TABLE `da_doctor_book` ADD `event_status` VARCHAR( 10 ) NOT NULL AFTER `patient_name` ,
ADD `event_notes` VARCHAR( 256 ) NOT NULL AFTER `event_status` ;

ALTER TABLE `da_todo_list` ADD `todolist_status` VARCHAR( 10 ) NOT NULL AFTER `deadline` ;

ALTER TABLE `da_todo_list` CHANGE `priority` `priority` VARCHAR( 10 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ;

ALTER TABLE `da_todo_list` CHANGE `deadline` `deadline` DATE NOT NULL ;


CREATE TABLE IF NOT EXISTS `da_state` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(3) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `short_code` varchar(10) NOT NULL,
  `ISO_Code` varchar(10) NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `country_id` (`country_id`),
  KEY `country_id_2` (`country_id`),
  KEY `country_id_3` (`country_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `da_country_master` (
  `id` int(3) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `iso_alpha2` varchar(2) CHARACTER SET utf8 DEFAULT NULL,
  `iso_alpha3` varchar(3) CHARACTER SET utf8 DEFAULT NULL,
  `iso_numeric` int(11) DEFAULT NULL,
  `currency_code` char(3) CHARACTER SET utf8 DEFAULT NULL,
  `currency_name` varchar(32) CHARACTER SET utf8 DEFAULT NULL,
  `currrency_symbol` varchar(3) CHARACTER SET utf8 DEFAULT NULL,
  `status` enum('0','1') CHARACTER SET utf8 NOT NULL DEFAULT '1',
  `deleted` enum('0','1') CHARACTER SET utf8 NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `iso_alpha2_2` (`iso_alpha2`),
  KEY `iso_alpha2` (`iso_alpha2`),
  KEY `iso_alpha3` (`iso_alpha3`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=240 ;

ALTER TABLE `da_todo_list` CHANGE `todolist_status` `todolist_status` VARCHAR( 15 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ;

ALTER TABLE `da_patient` ADD `created_by` VARCHAR( 10 ) NOT NULL AFTER `date_modified` ;

ALTER TABLE `da_patient` ADD `user_address` VARCHAR( 255 ) NOT NULL AFTER `user_phone` ,
ADD `user_city` VARCHAR( 100 ) NOT NULL AFTER `user_address` ,
ADD `user_state` VARCHAR( 100 ) NOT NULL AFTER `user_city` ,
ADD `user_zip` VARCHAR( 10 ) NOT NULL AFTER `user_state` ,
ADD `user_contact_method` VARCHAR( 10 ) NOT NULL AFTER `user_zip` ;

CREATE TABLE IF NOT EXISTS `da_patient_insurance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `user_mrn` varchar(25) NOT NULL,
  `user_ssn` varchar(25) NOT NULL,
  `user_insurance_provider` varchar(100) NOT NULL,
  `user_insurance_id` varchar(25) NOT NULL,
  `user_insurance_group` varchar(100) NOT NULL,
  `user_insurance_employer` varchar(100) NOT NULL,
  `user_insurance_employer_phone` varchar(15) NOT NULL,
  `user_insurance_employer_address` varchar(255) NOT NULL,
  `user_insurance_employer_street` varchar(100) NOT NULL,
  `user_insurance_employer_city` varchar(100) NOT NULL,
  `user_insurance_employer_state` varchar(100) NOT NULL,
  `user_insurance_employer_zip` varchar(10) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

ALTER TABLE `da_patient` ADD `user_parent_legal_guardian` VARCHAR( 32 ) NOT NULL AFTER `user_last_name` ;

ALTER TABLE `da_patient` ADD `user_recall_frequency` TINYINT( 2 ) NOT NULL AFTER `user_dob` ;

CREATE TABLE IF NOT EXISTS `da_patient_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `comments` varchar(500) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `status` tinyint(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `da_patient_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `doctor_id` int(11) NOT NULL,
  `subject` varchar(200) NOT NULL,
  `body` text NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `status` tinyint(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

ALTER TABLE `da_patient_comments` ADD `doctor_id` INT NOT NULL AFTER `patient_id` ;

ALTER TABLE `da_procedure` ADD `speciality_id` INT NOT NULL AFTER `id` ;

ALTER TABLE `da_doctor_book` CHANGE `confirm` `confirm` TINYINT( 1 ) NOT NULL COMMENT '0 for Scheduled, 1 for Confirmed, 2 for Cancelled';

CREATE TABLE IF NOT EXISTS `da_contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `resume` varchar(255) NOT NULL,
  `cover_letter` text NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

ALTER TABLE `da_contact` ADD `type` VARCHAR( 10 ) NOT NULL AFTER `id` ;

CREATE TABLE IF NOT EXISTS `da_sms_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(155) NOT NULL,
  `tempalte_body` text NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `status` tinyint(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

ALTER TABLE `da_patient` ADD `user_reminder` VARCHAR( 5 ) NOT NULL AFTER `entry_type` ;

/* 22-05-2014 */
ALTER TABLE `da_doctor_review` ADD `schedul_appoitment_rating` INT( 11 ) NOT NULL AFTER `wait_time_rating` ,
ADD `office_experience_rating` INT( 11 ) NOT NULL AFTER `schedul_appoitment_rating` ,
ADD `spent_time_rating` INT( 11 ) NOT NULL AFTER `office_experience_rating` ;
ALTER TABLE `da_doctor` ADD `lock_profile` TINYINT( 2 ) NOT NULL AFTER `email_verified` ;

CREATE TABLE IF NOT EXISTS `da_email_template` (
`id` int( 11 ) NOT NULL AUTO_INCREMENT ,
`name` varchar( 155 ) NOT NULL ,
`tempalte_body` text NOT NULL ,
`date_created` datetime NOT NULL ,
`date_modified` datetime NOT NULL ,
`status` tinyint( 2 ) NOT NULL ,
PRIMARY KEY ( `id` )
) ENGINE = InnoDB DEFAULT CHARSET = latin1 AUTO_INCREMENT =1;

ALTER TABLE `da_patient` ADD `user_notes` VARCHAR( 255 ) NOT NULL AFTER `user_reminder` ;

CREATE TABLE IF NOT EXISTS `da_doctor_payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `doctor_id` int(11) NOT NULL,
  `cc_first_name` varchar(100) NOT NULL,
  `cc_last_name` varchar(100) NOT NULL,
  `cc_card_no` varchar(20) NOT NULL,
  `cc_security_code` varchar(5) NOT NULL,
  `cc_expdate_year` year(4) NOT NULL,
  `cc_expdate_month` varchar(2) NOT NULL,
  `payment_type` varchar(10) NOT NULL,
  `doctor_plan` varchar(10) NOT NULL,
  `payment_amount` varchar(10) NOT NULL,
  `payment_start_date` date NOT NULL,
  `next_payment_date` date NOT NULL,
  `subscription_id` varchar(25) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `status` tinyint(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

ALTER TABLE `da_doctor_payment` ADD `acount_type` VARCHAR( 10 ) NOT NULL AFTER `doctor_id` ;

ALTER TABLE `da_doctor_payment` ADD `paypal_card_type` VARCHAR( 15 ) NOT NULL AFTER `acount_type` ,
ADD `paypal_email` VARCHAR( 100 ) NOT NULL AFTER `paypal_card_type` ;

ALTER TABLE `da_doctor_payment` ADD `correlation_id` VARCHAR( 25 ) NOT NULL AFTER `subscription_id` ;

ALTER TABLE  `da_doctor` ADD  `slug` VARCHAR( 256 ) NULL AFTER  `lock_profile` ;

CREATE TABLE IF NOT EXISTS `da_reason_for_visit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `speciality_id` int(11) NOT NULL,
  `reason_for_visit` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

ALTER TABLE `da_doctor_book` ADD `reason_for_visit_id` INT NOT NULL AFTER `procedure_id` ;

CREATE TABLE IF NOT EXISTS `da_doctor_reminder` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `doctor_id` int(11) NOT NULL,
  `reminder_for` varchar(10) NOT NULL,
  `reminder_time` tinyint(2) NOT NULL,
  `reminder_period` varchar(20) NOT NULL,
  `reminder_text` text NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `status` tinyint(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `da_doctor_appointment_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `doctor_id` int(11) NOT NULL,
  `appointment_approval_required` varchar(10) NOT NULL,
  `appointment_per_day` tinyint(2) NOT NULL,
  `appointment_canel_period` tinyint(2) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `status` tinyint(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `da_payment_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `paypal_account_type` varchar(20) NOT NULL,
  `paypal_username` varchar(100) NOT NULL,
  `paypal_password` varchar(100) NOT NULL,
  `paypal_signature` varchar(100) NOT NULL,
  `authorizenet_account_type` varchar(20) NOT NULL,
  `authorizenet_api_login_id` varchar(100) NOT NULL,
  `authorizenet_transaction_key` varchar(100) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

ALTER TABLE `da_doctor_payment` ADD `paypal_build` VARCHAR( 25 ) NOT NULL AFTER `correlation_id` ;

CREATE TABLE IF NOT EXISTS `da_doctor_notification_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `doctor_id` int(11) NOT NULL,
  `alert_email` varchar(100) NOT NULL,
  `email_notification` tinyint(2) NOT NULL,
  `cancel_notification` tinyint(2) NOT NULL,
  `confirm_notification` tinyint(2) NOT NULL,
  `weekly_notification` tinyint(2) NOT NULL,
  `monthly_notification` tinyint(2) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `status` tinyint(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

ALTER TABLE `da_doctor_book` ADD `email_reminder` TINYINT( 2 ) NOT NULL AFTER `confirm` ,
ADD `sms_reminder` TINYINT( 2 ) NOT NULL AFTER `email_reminder` ;

ALTER TABLE `da_doctor_book` ADD `notification_weekly` TINYINT( 2 ) NOT NULL AFTER `sms_reminder` ,
ADD `notification_monthly` TINYINT( 2 ) NOT NULL AFTER `notification_weekly` ;