-- phpMyAdmin SQL Dump
-- version 4.0.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 28, 2014 at 06:53 AM
-- Server version: 5.5.8-log
-- PHP Version: 5.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `doctor_appointment`
--

-- --------------------------------------------------------

--
-- Table structure for table `da_condition`
--

CREATE TABLE IF NOT EXISTS `da_condition` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `condition` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `da_condition`
--

INSERT INTO `da_condition` (`id`, `condition`, `description`, `status`) VALUES
(1, 'Arrhythmias', '', '1'),
(2, 'Hemophilia', '', '1'),
(3, 'Abdominal Aortic Aneurysm', '', '1'),
(4, 'Endometrial Ablation', '', '1');

-- --------------------------------------------------------

--
-- Table structure for table `da_conditions`
--

CREATE TABLE IF NOT EXISTS `da_conditions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sp_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `deleted` enum('0','1') NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `da_country_master`
--

CREATE TABLE IF NOT EXISTS `da_country_master` (
  `id` int(3) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `iso_alpha2` varchar(2) DEFAULT NULL,
  `iso_alpha3` varchar(3) DEFAULT NULL,
  `iso_numeric` int(11) DEFAULT NULL,
  `currency_code` char(3) DEFAULT NULL,
  `currency_name` varchar(32) DEFAULT NULL,
  `currrency_symbol` varchar(3) DEFAULT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `deleted` enum('0','1') NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `iso_alpha2_2` (`iso_alpha2`),
  KEY `iso_alpha2` (`iso_alpha2`),
  KEY `iso_alpha3` (`iso_alpha3`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=240 ;

--
-- Dumping data for table `da_country_master`
--

INSERT INTO `da_country_master` (`id`, `name`, `iso_alpha2`, `iso_alpha3`, `iso_numeric`, `currency_code`, `currency_name`, `currrency_symbol`, `status`, `deleted`) VALUES
(1, 'Afghanistan', 'AF', 'AFG', 4, 'AFN', 'Afghani', '؋', '1', '0'),
(2, 'Albania', 'AL', 'ALB', 8, 'ALL', 'Lek', 'Lek', '1', '0'),
(3, 'Algeria', 'DZ', 'DZA', 12, 'DZD', 'Dinar', NULL, '1', '0'),
(4, 'American Samoa', 'AS', 'ASM', 16, 'USD', 'Dollar', '$', '1', '0'),
(5, 'Andorra', 'AD', 'AND', 20, 'EUR', 'Euro', '€', '1', '0'),
(6, 'Angola', 'AO', 'AGO', 24, 'AOA', 'Kwanza', 'Kz', '1', '0'),
(7, 'Anguilla', 'AI', 'AIA', 660, 'XCD', 'Dollar', '$', '1', '0'),
(8, 'Antarctica', 'AQ', 'ATA', 10, '', '', NULL, '1', '0'),
(9, 'Antigua and Barbuda', 'AG', 'ATG', 28, 'XCD', 'Dollar', '$', '1', '0'),
(10, 'Argentina', 'AR', 'ARG', 32, 'ARS', 'Peso', '$', '1', '0'),
(11, 'Armenia', 'AM', 'ARM', 51, 'AMD', 'Dram', NULL, '1', '0'),
(12, 'Aruba', 'AW', 'ABW', 533, 'AWG', 'Guilder', '�'', '1', '0'),
(13, 'Australia', 'AU', 'AUS', 36, 'AUD', 'Dollar', '$', '1', '0'),
(14, 'Austria', 'AT', 'AUT', 40, 'EUR', 'Euro', '€', '1', '0'),
(15, 'Azerbaijan', 'AZ', 'AZE', 31, 'AZN', 'Manat', 'м�', '1', '0'),
(16, 'Bahamas', 'BS', 'BHS', 44, 'BSD', 'Dollar', '$', '1', '0'),
(17, 'Bahrain', 'BH', 'BHR', 48, 'BHD', 'Dinar', NULL, '1', '0'),
(18, 'Bangladesh', 'BD', 'BGD', 50, 'BDT', 'Taka', NULL, '1', '0'),
(19, 'Barbados', 'BB', 'BRB', 52, 'BBD', 'Dollar', '$', '1', '0'),
(20, 'Belarus', 'BY', 'BLR', 112, 'BYR', 'Ruble', 'p.', '1', '0'),
(21, 'Belgium', 'BE', 'BEL', 56, 'EUR', 'Euro', '€', '1', '0'),
(22, 'Belize', 'BZ', 'BLZ', 84, 'BZD', 'Dollar', 'BZ$', '1', '0'),
(23, 'Benin', 'BJ', 'BEN', 204, 'XOF', 'Franc', NULL, '1', '0'),
(24, 'Bermuda', 'BM', 'BMU', 60, 'BMD', 'Dollar', '$', '1', '0'),
(25, 'Bhutan', 'BT', 'BTN', 64, 'BTN', 'Ngultrum', NULL, '1', '0'),
(26, 'Bolivia', 'BO', 'BOL', 68, 'BOB', 'Boliviano', '$b', '1', '0'),
(27, 'Bosnia and Herzegovina', 'BA', 'BIH', 70, 'BAM', 'Marka', 'KM', '1', '0'),
(28, 'Botswana', 'BW', 'BWA', 72, 'BWP', 'Pula', 'P', '1', '0'),
(29, 'Bouvet Island', 'BV', 'BVT', 74, 'NOK', 'Krone', 'kr', '1', '0'),
(30, 'Brazil', 'BR', 'BRA', 76, 'BRL', 'Real', 'R$', '1', '0'),
(31, 'British Indian Ocean Territory', 'IO', 'IOT', 86, 'USD', 'Dollar', '$', '1', '0'),
(32, 'British Virgin Islands', 'VG', 'VGB', 92, 'USD', 'Dollar', '$', '1', '0'),
(33, 'Brunei', 'BN', 'BRN', 96, 'BND', 'Dollar', '$', '1', '0'),
(34, 'Bulgaria', 'BG', 'BGR', 100, 'BGN', 'Lev', 'л�', '1', '0'),
(35, 'Burkina Faso', 'BF', 'BFA', 854, 'XOF', 'Franc', NULL, '1', '0'),
(36, 'Burundi', 'BI', 'BDI', 108, 'BIF', 'Franc', NULL, '1', '0'),
(37, 'Cambodia', 'KH', 'KHM', 116, 'KHR', 'Riels', '៛', '1', '0'),
(38, 'Cameroon', 'CM', 'CMR', 120, 'XAF', 'Franc', 'FCF', '1', '0'),
(39, 'Canada', 'CA', 'CAN', 124, 'CAD', 'Dollar', '$', '1', '0'),
(40, 'Cape Verde', 'CV', 'CPV', 132, 'CVE', 'Escudo', NULL, '1', '0'),
(41, 'Cayman Islands', 'KY', 'CYM', 136, 'KYD', 'Dollar', '$', '1', '0'),
(42, 'Central African Republic', 'CF', 'CAF', 140, 'XAF', 'Franc', 'FCF', '1', '0'),
(43, 'Chad', 'TD', 'TCD', 148, 'XAF', 'Franc', NULL, '1', '0'),
(44, 'Chile', 'CL', 'CHL', 152, 'CLP', 'Peso', NULL, '1', '0'),
(45, 'China', 'CN', 'CHN', 156, 'CNY', 'Yuan Renminbi', '¥', '1', '0'),
(46, 'Christmas Island', 'CX', 'CXR', 162, 'AUD', 'Dollar', '$', '1', '0'),
(47, 'Cocos Islands', 'CC', 'CCK', 166, 'AUD', 'Dollar', '$', '1', '0'),
(48, 'Colombia', 'CO', 'COL', 170, 'COP', 'Peso', '$', '1', '0'),
(49, 'Comoros', 'KM', 'COM', 174, 'KMF', 'Franc', NULL, '1', '0'),
(50, 'Cook Islands', 'CK', 'COK', 184, 'NZD', 'Dollar', '$', '1', '0'),
(51, 'Costa Rica', 'CR', 'CRI', 188, 'CRC', 'Colon', '₡', '1', '0'),
(52, 'Croatia', 'HR', 'HRV', 191, 'HRK', 'Kuna', 'kn', '1', '0'),
(53, 'Cuba', 'CU', 'CUB', 192, 'CUP', 'Peso', '₱', '1', '0'),
(54, 'Cyprus', 'CY', 'CYP', 196, 'CYP', 'Pound', NULL, '1', '0'),
(55, 'Czech Republic', 'CZ', 'CZE', 203, 'CZK', 'Koruna', 'K�?', '1', '0'),
(56, 'Democratic Republic of the Congo', 'CD', 'COD', 180, 'CDF', 'Franc', NULL, '1', '0'),
(57, 'Denmark', 'DK', 'DNK', 208, 'DKK', 'Krone', 'kr', '1', '0'),
(58, 'Djibouti', 'DJ', 'DJI', 262, 'DJF', 'Franc', NULL, '1', '0'),
(59, 'Dominica', 'DM', 'DMA', 212, 'XCD', 'Dollar', '$', '1', '0'),
(60, 'Dominican Republic', 'DO', 'DOM', 214, 'DOP', 'Peso', 'RD$', '1', '0'),
(61, 'East Timor', 'TL', 'TLS', 626, 'USD', 'Dollar', '$', '1', '0'),
(62, 'Ecuador', 'EC', 'ECU', 218, 'USD', 'Dollar', '$', '1', '0'),
(63, 'Egypt', 'EG', 'EGY', 818, 'EGP', 'Pound', '£', '1', '0'),
(64, 'El Salvador', 'SV', 'SLV', 222, 'SVC', 'Colone', '$', '1', '0'),
(65, 'Equatorial Guinea', 'GQ', 'GNQ', 226, 'XAF', 'Franc', 'FCF', '1', '0'),
(66, 'Eritrea', 'ER', 'ERI', 232, 'ERN', 'Nakfa', 'Nfk', '1', '0'),
(67, 'Estonia', 'EE', 'EST', 233, 'EEK', 'Kroon', 'kr', '1', '0'),
(68, 'Ethiopia', 'ET', 'ETH', 231, 'ETB', 'Birr', NULL, '1', '0'),
(69, 'Falkland Islands', 'FK', 'FLK', 238, 'FKP', 'Pound', '£', '1', '0'),
(70, 'Faroe Islands', 'FO', 'FRO', 234, 'DKK', 'Krone', 'kr', '1', '0'),
(71, 'Fiji', 'FJ', 'FJI', 242, 'FJD', 'Dollar', '$', '1', '0'),
(72, 'Finland', 'FI', 'FIN', 246, 'EUR', 'Euro', '€', '1', '0'),
(73, 'France', 'FR', 'FRA', 250, 'EUR', 'Euro', '€', '1', '0'),
(74, 'French Guiana', 'GF', 'GUF', 254, 'EUR', 'Euro', '€', '1', '0'),
(75, 'French Polynesia', 'PF', 'PYF', 258, 'XPF', 'Franc', NULL, '1', '0'),
(76, 'French Southern Territories', 'TF', 'ATF', 260, 'EUR', 'Euro  ', '€', '1', '0'),
(77, 'Gabon', 'GA', 'GAB', 266, 'XAF', 'Franc', 'FCF', '1', '0'),
(78, 'Gambia', 'GM', 'GMB', 270, 'GMD', 'Dalasi', 'D', '1', '0'),
(79, 'Georgia', 'GE', 'GEO', 268, 'GEL', 'Lari', NULL, '1', '0'),
(80, 'Germany', 'DE', 'DEU', 276, 'EUR', 'Euro', '€', '1', '0'),
(81, 'Ghana', 'GH', 'GHA', 288, 'GHC', 'Cedi', '¢', '1', '0'),
(82, 'Gibraltar', 'GI', 'GIB', 292, 'GIP', 'Pound', '£', '1', '0'),
(83, 'Greece', 'GR', 'GRC', 300, 'EUR', 'Euro', '€', '1', '0'),
(84, 'Greenland', 'GL', 'GRL', 304, 'DKK', 'Krone', 'kr', '1', '0'),
(85, 'Grenada', 'GD', 'GRD', 308, 'XCD', 'Dollar', '$', '1', '0'),
(86, 'Guadeloupe', 'GP', 'GLP', 312, 'EUR', 'Euro', '€', '1', '0'),
(87, 'Guam', 'GU', 'GUM', 316, 'USD', 'Dollar', '$', '1', '0'),
(88, 'Guatemala', 'GT', 'GTM', 320, 'GTQ', 'Quetzal', 'Q', '1', '0'),
(89, 'Guinea', 'GN', 'GIN', 324, 'GNF', 'Franc', NULL, '1', '0'),
(90, 'Guinea-Bissau', 'GW', 'GNB', 624, 'XOF', 'Franc', NULL, '1', '0'),
(91, 'Guyana', 'GY', 'GUY', 328, 'GYD', 'Dollar', '$', '1', '0'),
(92, 'Haiti', 'HT', 'HTI', 332, 'HTG', 'Gourde', 'G', '1', '0'),
(93, 'Heard Island and McDonald Islands', 'HM', 'HMD', 334, 'AUD', 'Dollar', '$', '1', '0'),
(94, 'Honduras', 'HN', 'HND', 340, 'HNL', 'Lempira', 'L', '1', '0'),
(95, 'Hong Kong', 'HK', 'HKG', 344, 'HKD', 'Dollar', '$', '1', '0'),
(96, 'Hungary', 'HU', 'HUN', 348, 'HUF', 'Forint', 'Ft', '1', '0'),
(97, 'Iceland', 'IS', 'ISL', 352, 'ISK', 'Krona', 'kr', '1', '0'),
(98, 'India', 'IN', 'IND', 356, 'INR', 'Rupee', '₹', '1', '0'),
(99, 'Indonesia', 'ID', 'IDN', 360, 'IDR', 'Rupiah', 'Rp', '1', '0'),
(100, 'Iran', 'IR', 'IRN', 364, 'IRR', 'Rial', '﷼', '1', '0'),
(101, 'Iraq', 'IQ', 'IRQ', 368, 'IQD', 'Dinar', NULL, '1', '0'),
(102, 'Ireland', 'IE', 'IRL', 372, 'EUR', 'Euro', '€', '1', '0'),
(103, 'Israel', 'IL', 'ISR', 376, 'ILS', 'Shekel', '₪', '1', '0'),
(104, 'Italy', 'IT', 'ITA', 380, 'EUR', 'Euro', '€', '1', '0'),
(105, 'Ivory Coast', 'CI', 'CIV', 384, 'XOF', 'Franc', NULL, '1', '0'),
(106, 'Jamaica', 'JM', 'JAM', 388, 'JMD', 'Dollar', '$', '1', '0'),
(107, 'Japan', 'JP', 'JPN', 392, 'JPY', 'Yen', '¥', '1', '0'),
(108, 'Jordan', 'JO', 'JOR', 400, 'JOD', 'Dinar', NULL, '1', '0'),
(109, 'Kazakhstan', 'KZ', 'KAZ', 398, 'KZT', 'Tenge', 'л�', '1', '0'),
(110, 'Kenya', 'KE', 'KEN', 404, 'KES', 'Shilling', NULL, '1', '0'),
(111, 'Kiribati', 'KI', 'KIR', 296, 'AUD', 'Dollar', '$', '1', '0'),
(112, 'Kuwait', 'KW', 'KWT', 414, 'KWD', 'Dinar', NULL, '1', '0'),
(113, 'Kyrgyzstan', 'KG', 'KGZ', 417, 'KGS', 'Som', 'л�', '1', '0'),
(114, 'Laos', 'LA', 'LAO', 418, 'LAK', 'Kip', '₭', '1', '0'),
(115, 'Latvia', 'LV', 'LVA', 428, 'LVL', 'Lat', 'Ls', '1', '0'),
(116, 'Lebanon', 'LB', 'LBN', 422, 'LBP', 'Pound', '£', '1', '0'),
(117, 'Lesotho', 'LS', 'LSO', 426, 'LSL', 'Loti', 'L', '1', '0'),
(118, 'Liberia', 'LR', 'LBR', 430, 'LRD', 'Dollar', '$', '1', '0'),
(119, 'Libya', 'LY', 'LBY', 434, 'LYD', 'Dinar', NULL, '1', '0'),
(120, 'Liechtenstein', 'LI', 'LIE', 438, 'CHF', 'Franc', 'CHF', '1', '0'),
(121, 'Lithuania', 'LT', 'LTU', 440, 'LTL', 'Litas', 'Lt', '1', '0'),
(122, 'Luxembourg', 'LU', 'LUX', 442, 'EUR', 'Euro', '€', '1', '0'),
(123, 'Macao', 'MO', 'MAC', 446, 'MOP', 'Pataca', 'MOP', '1', '0'),
(124, 'Macedonia', 'MK', 'MKD', 807, 'MKD', 'Denar', 'д�', '1', '0'),
(125, 'Madagascar', 'MG', 'MDG', 450, 'MGA', 'Ariary', NULL, '1', '0'),
(126, 'Malawi', 'MW', 'MWI', 454, 'MWK', 'Kwacha', 'MK', '1', '0'),
(127, 'Malaysia', 'MY', 'MYS', 458, 'MYR', 'Ringgit', 'RM', '1', '0'),
(128, 'Maldives', 'MV', 'MDV', 462, 'MVR', 'Rufiyaa', 'Rf', '1', '0'),
(129, 'Mali', 'ML', 'MLI', 466, 'XOF', 'Franc', NULL, '1', '0'),
(130, 'Malta', 'MT', 'MLT', 470, 'MTL', 'Lira', NULL, '1', '0'),
(131, 'Marshall Islands', 'MH', 'MHL', 584, 'USD', 'Dollar', '$', '1', '0'),
(132, 'Martinique', 'MQ', 'MTQ', 474, 'EUR', 'Euro', '€', '1', '0'),
(133, 'Mauritania', 'MR', 'MRT', 478, 'MRO', 'Ouguiya', 'UM', '1', '0'),
(134, 'Mauritius', 'MU', 'MUS', 480, 'MUR', 'Rupee', '₨', '1', '0'),
(135, 'Mayotte', 'YT', 'MYT', 175, 'EUR', 'Euro', '€', '1', '0'),
(136, 'Mexico', 'MX', 'MEX', 484, 'MXN', 'Peso', '$', '1', '0'),
(137, 'Micronesia', 'FM', 'FSM', 583, 'USD', 'Dollar', '$', '1', '0'),
(138, 'Moldova', 'MD', 'MDA', 498, 'MDL', 'Leu', NULL, '1', '0'),
(139, 'Monaco', 'MC', 'MCO', 492, 'EUR', 'Euro', '€', '1', '0'),
(140, 'Mongolia', 'MN', 'MNG', 496, 'MNT', 'Tugrik', '₮', '1', '0'),
(141, 'Montserrat', 'MS', 'MSR', 500, 'XCD', 'Dollar', '$', '1', '0'),
(142, 'Morocco', 'MA', 'MAR', 504, 'MAD', 'Dirham', NULL, '1', '0'),
(143, 'Mozambique', 'MZ', 'MOZ', 508, 'MZN', 'Meticail', 'MT', '1', '0'),
(144, 'Myanmar', 'MM', 'MMR', 104, 'MMK', 'Kyat', 'K', '1', '0'),
(145, 'Namibia', 'NA', 'NAM', 516, 'NAD', 'Dollar', '$', '1', '0'),
(146, 'Nauru', 'NR', 'NRU', 520, 'AUD', 'Dollar', '$', '1', '0'),
(147, 'Nepal', 'NP', 'NPL', 524, 'NPR', 'Rupee', '₨', '1', '0'),
(148, 'Netherlands', 'NL', 'NLD', 528, 'EUR', 'Euro', '€', '1', '0'),
(149, 'Netherlands Antilles', 'AN', 'ANT', 530, 'ANG', 'Guilder', '�'', '1', '0'),
(150, 'New Caledonia', 'NC', 'NCL', 540, 'XPF', 'Franc', NULL, '1', '0'),
(151, 'New Zealand', 'NZ', 'NZL', 554, 'NZD', 'Dollar', '$', '1', '0'),
(152, 'Nicaragua', 'NI', 'NIC', 558, 'NIO', 'Cordoba', 'C$', '1', '0'),
(153, 'Niger', 'NE', 'NER', 562, 'XOF', 'Franc', NULL, '1', '0'),
(154, 'Nigeria', 'NG', 'NGA', 566, 'NGN', 'Naira', '₦', '1', '0'),
(155, 'Niue', 'NU', 'NIU', 570, 'NZD', 'Dollar', '$', '1', '0'),
(156, 'Norfolk Island', 'NF', 'NFK', 574, 'AUD', 'Dollar', '$', '1', '0'),
(157, 'North Korea', 'KP', 'PRK', 408, 'KPW', 'Won', '₩', '1', '0'),
(158, 'Northern Mariana Islands', 'MP', 'MNP', 580, 'USD', 'Dollar', '$', '1', '0'),
(159, 'Norway', 'NO', 'NOR', 578, 'NOK', 'Krone', 'kr', '1', '0'),
(160, 'Oman', 'OM', 'OMN', 512, 'OMR', 'Rial', '﷼', '1', '0'),
(161, 'Pakistan', 'PK', 'PAK', 586, 'PKR', 'Rupee', '₨', '1', '0'),
(162, 'Palau', 'PW', 'PLW', 585, 'USD', 'Dollar', '$', '1', '0'),
(163, 'Palestinian Territory', 'PS', 'PSE', 275, 'ILS', 'Shekel', '₪', '1', '0'),
(164, 'Panama', 'PA', 'PAN', 591, 'PAB', 'Balboa', 'B/.', '1', '0'),
(165, 'Papua New Guinea', 'PG', 'PNG', 598, 'PGK', 'Kina', NULL, '1', '0'),
(166, 'Paraguay', 'PY', 'PRY', 600, 'PYG', 'Guarani', 'Gs', '1', '0'),
(167, 'Peru', 'PE', 'PER', 604, 'PEN', 'Sol', 'S/.', '1', '0'),
(168, 'Philippines', 'PH', 'PHL', 608, 'PHP', 'Peso', 'Php', '1', '0'),
(169, 'Pitcairn', 'PN', 'PCN', 612, 'NZD', 'Dollar', '$', '1', '0'),
(170, 'Poland', 'PL', 'POL', 616, 'PLN', 'Zloty', 'zł', '1', '0'),
(171, 'Portugal', 'PT', 'PRT', 620, 'EUR', 'Euro', '€', '1', '0'),
(172, 'Puerto Rico', 'PR', 'PRI', 630, 'USD', 'Dollar', '$', '1', '0'),
(173, 'Qatar', 'QA', 'QAT', 634, 'QAR', 'Rial', '﷼', '1', '0'),
(174, 'Republic of the Congo', 'CG', 'COG', 178, 'XAF', 'Franc', 'FCF', '1', '0'),
(175, 'Reunion', 'RE', 'REU', 638, 'EUR', 'Euro', '€', '1', '0'),
(176, 'Romania', 'RO', 'ROU', 642, 'RON', 'Leu', 'lei', '1', '0'),
(177, 'Russia', 'RU', 'RUS', 643, 'RUB', 'Ruble', 'р�', '1', '0'),
(178, 'Rwanda', 'RW', 'RWA', 646, 'RWF', 'Franc', NULL, '1', '0'),
(179, 'Saint Helena', 'SH', 'SHN', 654, 'SHP', 'Pound', '£', '1', '0'),
(180, 'Saint Kitts and Nevis', 'KN', 'KNA', 659, 'XCD', 'Dollar', '$', '1', '0'),
(181, 'Saint Lucia', 'LC', 'LCA', 662, 'XCD', 'Dollar', '$', '1', '0'),
(182, 'Saint Pierre and Miquelon', 'PM', 'SPM', 666, 'EUR', 'Euro', '€', '1', '0'),
(183, 'Saint Vincent and the Grenadines', 'VC', 'VCT', 670, 'XCD', 'Dollar', '$', '1', '0'),
(184, 'Samoa', 'WS', 'WSM', 882, 'WST', 'Tala', 'WS$', '1', '0'),
(185, 'San Marino', 'SM', 'SMR', 674, 'EUR', 'Euro', '€', '1', '0'),
(186, 'Sao Tome and Principe', 'ST', 'STP', 678, 'STD', 'Dobra', 'Db', '1', '0'),
(187, 'Saudi Arabia', 'SA', 'SAU', 682, 'SAR', 'Rial', '﷼', '1', '0'),
(188, 'Senegal', 'SN', 'SEN', 686, 'XOF', 'Franc', NULL, '1', '0'),
(189, 'Serbia and Montenegro', 'CS', 'SCG', 891, 'RSD', 'Dinar', '�"�', '1', '0'),
(190, 'Seychelles', 'SC', 'SYC', 690, 'SCR', 'Rupee', '₨', '1', '0'),
(191, 'Sierra Leone', 'SL', 'SLE', 694, 'SLL', 'Leone', 'Le', '1', '0'),
(192, 'Singapore', 'SG', 'SGP', 702, 'SGD', 'Dollar', '$', '1', '0'),
(193, 'Slovakia', 'SK', 'SVK', 703, 'SKK', 'Koruna', 'Sk', '1', '0'),
(194, 'Slovenia', 'SI', 'SVN', 705, 'EUR', 'Euro', '€', '1', '0'),
(195, 'Solomon Islands', 'SB', 'SLB', 90, 'SBD', 'Dollar', '$', '1', '0'),
(196, 'Somalia', 'SO', 'SOM', 706, 'SOS', 'Shilling', 'S', '1', '0'),
(197, 'South Africa', 'ZA', 'ZAF', 710, 'ZAR', 'Rand', 'R', '1', '0'),
(198, 'South Georgia and the South Sandwich Islands', 'GS', 'SGS', 239, 'GBP', 'Pound', '£', '1', '0'),
(199, 'South Korea', 'KR', 'KOR', 410, 'KRW', 'Won', '₩', '1', '0'),
(200, 'Spain', 'ES', 'ESP', 724, 'EUR', 'Euro', '€', '1', '0'),
(201, 'Sri Lanka', 'LK', 'LKA', 144, 'LKR', 'Rupee', '₨', '1', '0'),
(202, 'Sudan', 'SD', 'SDN', 736, 'SDD', 'Dinar', NULL, '1', '0'),
(203, 'Suriname', 'SR', 'SUR', 740, 'SRD', 'Dollar', '$', '1', '0'),
(204, 'Svalbard and Jan Mayen', 'SJ', 'SJM', 744, 'NOK', 'Krone', 'kr', '1', '0'),
(205, 'Swaziland', 'SZ', 'SWZ', 748, 'SZL', 'Lilangeni', NULL, '1', '0'),
(206, 'Sweden', 'SE', 'SWE', 752, 'SEK', 'Krona', 'kr', '1', '0'),
(207, 'Switzerland', 'CH', 'CHE', 756, 'CHF', 'Franc', 'CHF', '1', '0'),
(208, 'Syria', 'SY', 'SYR', 760, 'SYP', 'Pound', '£', '1', '0'),
(209, 'Taiwan', 'TW', 'TWN', 158, 'TWD', 'Dollar', 'NT$', '1', '0'),
(210, 'Tajikistan', 'TJ', 'TJK', 762, 'TJS', 'Somoni', NULL, '1', '0'),
(211, 'Tanzania', 'TZ', 'TZA', 834, 'TZS', 'Shilling', NULL, '1', '0'),
(212, 'Thailand', 'TH', 'THA', 764, 'THB', 'Baht', '฿', '1', '0'),
(213, 'Togo', 'TG', 'TGO', 768, 'XOF', 'Franc', NULL, '1', '0'),
(214, 'Tokelau', 'TK', 'TKL', 772, 'NZD', 'Dollar', '$', '1', '0'),
(215, 'Tonga', 'TO', 'TON', 776, 'TOP', 'Pa''anga', 'T$', '1', '0'),
(216, 'Trinidad and Tobago', 'TT', 'TTO', 780, 'TTD', 'Dollar', 'TT$', '1', '0'),
(217, 'Tunisia', 'TN', 'TUN', 788, 'TND', 'Dinar', NULL, '1', '0'),
(218, 'Turkey', 'TR', 'TUR', 792, 'TRY', 'Lira', 'YTL', '1', '0'),
(219, 'Turkmenistan', 'TM', 'TKM', 795, 'TMM', 'Manat', 'm', '1', '0'),
(220, 'Turks and Caicos Islands', 'TC', 'TCA', 796, 'USD', 'Dollar', '$', '1', '0'),
(221, 'Tuvalu', 'TV', 'TUV', 798, 'AUD', 'Dollar', '$', '1', '0'),
(222, 'U.S. Virgin Islands', 'VI', 'VIR', 850, 'USD', 'Dollar', '$', '1', '0'),
(223, 'Uganda', 'UG', 'UGA', 800, 'UGX', 'Shilling', NULL, '1', '0'),
(224, 'Ukraine', 'UA', 'UKR', 804, 'UAH', 'Hryvnia', '₴', '1', '0'),
(225, 'United Arab Emirates', 'AE', 'ARE', 784, 'AED', 'Dirham', NULL, '1', '0'),
(226, 'United Kingdom', 'GB', 'GBR', 826, 'GBP', 'Pound', '£', '1', '0'),
(227, 'United States', 'US', 'USA', 840, 'USD', 'Dollar', '$', '1', '0'),
(228, 'United States Minor Outlying Islands', 'UM', 'UMI', 581, 'USD', 'Dollar ', '$', '1', '0'),
(229, 'Uruguay', 'UY', 'URY', 858, 'UYU', 'Peso', '$U', '1', '0'),
(230, 'Uzbekistan', 'UZ', 'UZB', 860, 'UZS', 'Som', 'л�', '1', '0'),
(231, 'Vanuatu', 'VU', 'VUT', 548, 'VUV', 'Vatu', 'Vt', '1', '0'),
(232, 'Vatican', 'VA', 'VAT', 336, 'EUR', 'Euro', '€', '1', '0'),
(233, 'Venezuela', 'VE', 'VEN', 862, 'VEF', 'Bolivar', 'Bs', '1', '0'),
(234, 'Vietnam', 'VN', 'VNM', 704, 'VND', 'Dong', '₫', '1', '0'),
(235, 'Wallis and Futuna', 'WF', 'WLF', 876, 'XPF', 'Franc', NULL, '1', '0'),
(236, 'Western Sahara', 'EH', 'ESH', 732, 'MAD', 'Dirham', NULL, '1', '0'),
(237, 'Yemen', 'YE', 'YEM', 887, 'YER', 'Rial', '﷼', '1', '0'),
(238, 'Zambia', 'ZM', 'ZMB', 894, 'ZMK', 'Kwacha', 'ZK', '1', '0'),
(239, 'Zimbabwe', 'ZW', 'ZWE', 716, 'ZWD', 'Dollar', 'Z$', '1', '0');

-- --------------------------------------------------------

--
-- Table structure for table `da_degree`
--

CREATE TABLE IF NOT EXISTS `da_degree` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `deleted` enum('0','1') NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `da_degree`
--

INSERT INTO `da_degree` (`id`, `name`, `status`, `deleted`) VALUES
(1, 'BMBS', '1', '0'),
(2, 'MBBS', '1', '0'),
(3, 'MBChB', '1', '0'),
(4, 'MB BCh', '1', '0'),
(5, 'BMed', '1', '0'),
(6, 'MD', '1', '0'),
(7, 'MDCM', '1', '0'),
(8, 'Dr.MuD', '1', '0'),
(9, 'Dr.Med', '1', '0'),
(10, 'Cand.med', '1', '0'),
(11, 'Med', '1', '0');

-- --------------------------------------------------------

--
-- Table structure for table `da_doctor`
--

CREATE TABLE IF NOT EXISTS `da_doctor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(32) NOT NULL,
  `first_name` varchar(32) NOT NULL,
  `last_name` varchar(32) NOT NULL,
  `email` varchar(32) NOT NULL,
  `password` varchar(255) NOT NULL,
  `image` varchar(150) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `gender` enum('M','F') NOT NULL DEFAULT 'M',
  `birth_date` date NOT NULL,
  `title` varchar(5) NOT NULL,
  `comments` text NOT NULL,
  `degree` varchar(10) NOT NULL,
  `education` text NOT NULL,
  `medical_school` varchar(150) NOT NULL,
  `medical_school_year` year(4) NOT NULL,
  `residency_training` text NOT NULL,
  `residency_training_year` year(4) NOT NULL,
  `hospital_affiliations` text NOT NULL,
  `board_certifications` text NOT NULL,
  `awards_publications` text NOT NULL,
  `languages_spoken` int(11) NOT NULL,
  `insurances_accepted` text NOT NULL,
  `addr1` text NOT NULL,
  `addr2` text NOT NULL,
  `state` varchar(100) NOT NULL,
  `country` int(10) NOT NULL,
  `zip` varchar(10) NOT NULL,
  `designation` varchar(255) NOT NULL,
  `speciality` int(11) NOT NULL,
  `payment_type` varchar(10) NOT NULL,
  `plan` varchar(5) NOT NULL,
  `visit_price` float NOT NULL,
  `visit_duration` varchar(5) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_modified` datetime NOT NULL,
  `email_verified` tinyint(1) NOT NULL DEFAULT '0',
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `da_doctor`
--

INSERT INTO `da_doctor` (`id`, `username`, `first_name`, `last_name`, `email`, `password`, `image`, `phone`, `gender`, `birth_date`, `title`, `comments`, `degree`, `education`, `medical_school`, `medical_school_year`, `residency_training`, `residency_training_year`, `hospital_affiliations`, `board_certifications`, `awards_publications`, `languages_spoken`, `insurances_accepted`, `addr1`, `addr2`, `state`, `country`, `zip`, `designation`, `speciality`, `payment_type`, `plan`, `visit_price`, `visit_duration`, `date_created`, `date_modified`, `email_verified`, `status`) VALUES
(11, 'jana.sharadindu@gmail.com', 'Sharadindu', 'Jana', 'jana.sharadindu@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '53075061e8d52-Penguins.jpg', '9851520610', 'M', '1986-09-28', 'Mr.', 'kyyt', 'MBBS', '', 'kkgk', 1960, 'No', 1962, 'Goverment Medical Center - Petrie Division ,New York University Elaine A. and Kenneth G., Langone Medical Center ', 'American Board of Internal Medicine', 'Dr. Smith was named in Castle Connolly's "Top Doctors-New York Metro Area" 2006, 2007, 2008 2009, 2010.', 1, '', '', '', '', 0, '721433', 'MD', 0, 'standard', '', 15, '12', '2014-02-05 08:46:21', '0000-00-00 00:00:00', 1, 1),
(13, 'indrojit@banik.com', 'Indrojit', 'Banik', 'indrojit@banik.com', 'e10adc3949ba59abbe56e057f20f883e', '', '9732632272', '', '0000-00-00', '', '', '', '', '', 0000, '', 0000, '', '', '', 0, '', '', '', '', 0, '700065', '', 5, 'advance', 'y', 0, '', '2014-02-05 08:46:21', '0000-00-00 00:00:00', 1, 1),
(14, 'jana.arbsoft@gmail.com', 'Sharadindu', 'Jana', 'jana.arbsoft@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '', '9732632272', '', '0000-00-00', '', '', '', '', '', 0000, '', 0000, '', '', '', 0, '', '', '', '', 0, '700000', '', 2, 'advance', 'm', 0, '', '2014-02-05 08:46:21', '0000-00-00 00:00:00', 1, 1),
(16, 'jana.bcet@gmail.com', 'Sharadindu', 'Jana', 'jana.bcet1@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '', '9851520610', 'M', '1987-09-28', 'Mr.', 'Deatils', 'BMed', 'Medical School - University of Pennsylvania, School of Medicine,\r\nGoverment Medical Center\r\n', '', 0000, 'All', 0000, 'Goverment Medical Center - Petrie Division ,New York University Elaine A. and Kenneth G., Langone Medical Center.', 'American Board of Internal Medicine', 'Dr. Smith was named in Castle Connolly's "Top Doctors-New York Metro Area" 2006, 2007, 2008 2009, 2010.', 0, 'Yes', '49, Subhasnagar, 1st bye lane,', 'dum dum Cantt', 'West Bengal', 0, '700065', '', 3, 'standard', 'y', 0, '', '2014-02-13 03:43:48', '2014-02-13 09:13:48', 1, 1),
(18, 'abc@gmail.com', 'Abc', 'Def', 'abc@gmail.com', '', '', '9732632272', '', '0000-00-00', '', '', '', '', '', 0000, '', 0000, '', '', '', 0, '', '', '', '', 0, '700065', '', 30, '', '', 0, '', '2014-02-16 19:52:01', '2014-02-17 01:22:01', 0, 1),
(19, 'asdfasfasf', 'dadad', 'kflkasfa', 'sdfsdf@wewe.gfdfg', '', '', '9876546588', 'F', '2011-09-05', 'Mrs.', 'fdsgsdgsg', 'BMed', 'sdgsdg', '', 0000, 'sdgsdg', 0000, 'sdgsdg', 'sdgsdgsdg', 'sdgsdgsdg', 0, 'sdgsdgsdgsdg', 'sdgsdgsdgsg', 'sdgsdgsdg', 'sdsdg', 12, '6565', 'sdg sdgs', 4, '', '', 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0),
(20, 'life@science.com', 'life', 'science', 'life@science.com', '', '', '123456', 'M', '0000-00-00', '', '', '', '', '', 0000, '', 0000, '', '', '', 0, '', '', '', '', 0, '100000', '', 11, '', '', 0, '', '2014-02-22 07:21:15', '2014-02-22 12:51:15', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `da_doctor_address`
--

CREATE TABLE IF NOT EXISTS `da_doctor_address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `doctor_id` int(11) NOT NULL,
  `address` varchar(255) NOT NULL,
  `latitude` varchar(15) NOT NULL,
  `longitude` varchar(15) NOT NULL,
  `access` varchar(10) NOT NULL,
  `order_status` tinyint(1) NOT NULL,
  `suite` varchar(125) NOT NULL,
  `city` varchar(125) NOT NULL,
  `state` varchar(125) NOT NULL,
  `zip` varchar(15) NOT NULL,
  `practice_affiliation` varchar(255) NOT NULL,
  `office_name` varchar(125) NOT NULL,
  `office_phone` varchar(25) NOT NULL,
  `office_fax` varchar(25) NOT NULL,
  `default_status` tinyint(1) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `da_doctor_address`
--

INSERT INTO `da_doctor_address` (`id`, `doctor_id`, `address`, `latitude`, `longitude`, `access`, `order_status`, `suite`, `city`, `state`, `zip`, `practice_affiliation`, `office_name`, `office_phone`, `office_fax`, `default_status`, `active`, `date_created`, `date_modified`, `status`) VALUES
(1, 11, '49, subhas nagar, 1st bye lane, dum dum cantt. kolkata,west Bengal,700065', '22.572676', '88.363895', 'Public', 14, 'Suite', 'kolkata', '', '700065', 'Aff', 'Jai Ho', '9732632272', '00145623', 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(2, 11, 'battalla, Natun Bazar, dum dum cantt. kolkata,west Bengal,721433', '40.8168702', '-73.9345842', '1', 2, '', '', '', '', '', '', '', '', 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(3, 11, 'Jagannath Pur, Haipur, contai', '', '', '', 3, '', '', '', '', '', '', '', '', 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(4, 11, 'tet', '', '', '', 2, '', '', '', '', '', '', '', '', 0, 0, '2014-02-26 06:47:27', '2014-02-26 06:47:27', 1);

-- --------------------------------------------------------

--
-- Table structure for table `da_doctor_condition`
--

CREATE TABLE IF NOT EXISTS `da_doctor_condition` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `doctor_id` int(11) NOT NULL,
  `condition_id` int(11) NOT NULL,
  `default_status` tinyint(1) NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `da_doctor_condition`
--

INSERT INTO `da_doctor_condition` (`id`, `doctor_id`, `condition_id`, `default_status`, `status`) VALUES
(1, 11, 1, 0, 0),
(2, 11, 2, 0, 0),
(3, 11, 3, 0, 0),
(4, 11, 4, 0, 0),
(5, 11, 1, 0, 1),
(6, 11, 2, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `da_doctor_insurance`
--

CREATE TABLE IF NOT EXISTS `da_doctor_insurance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `doctor_id` int(11) NOT NULL,
  `insurance_id` int(11) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `da_doctor_insurance`
--

INSERT INTO `da_doctor_insurance` (`id`, `doctor_id`, `insurance_id`, `status`) VALUES
(1, 11, 1, '1'),
(2, 11, 2, '1'),
(3, 11, 3, '1'),
(5, 11, 4, '1');

-- --------------------------------------------------------

--
-- Table structure for table `da_doctor_insurance_plan`
--

CREATE TABLE IF NOT EXISTS `da_doctor_insurance_plan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `doctor_id` int(11) NOT NULL,
  `insurance_id` int(11) NOT NULL,
  `plan_id` int(11) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `da_doctor_insurance_plan`
--

INSERT INTO `da_doctor_insurance_plan` (`id`, `doctor_id`, `insurance_id`, `plan_id`, `status`) VALUES
(1, 11, 1, 1, '1'),
(2, 11, 1, 2, '0'),
(3, 11, 2, 3, '1'),
(4, 11, 2, 4, '1');

-- --------------------------------------------------------

--
-- Table structure for table `da_doctor_language`
--

CREATE TABLE IF NOT EXISTS `da_doctor_language` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `doctor_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `default_status` tinyint(1) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=92 ;

--
-- Dumping data for table `da_doctor_language`
--

INSERT INTO `da_doctor_language` (`id`, `doctor_id`, `language_id`, `default_status`, `status`) VALUES
(1, 11, 1, 0, '0'),
(2, 11, 3, 0, '0'),
(4, 11, 1, 0, '0'),
(5, 11, 2, 0, '0'),
(6, 11, 3, 0, '0'),
(7, 11, 2, 0, '0'),
(8, 11, 1, 0, '0'),
(9, 11, 2, 0, '0'),
(10, 11, 1, 0, '0'),
(11, 11, 2, 0, '0'),
(12, 11, 3, 0, '0'),
(13, 11, 1, 0, '0'),
(14, 11, 2, 0, '0'),
(15, 11, 2, 0, '0'),
(16, 11, 2, 0, '0'),
(17, 11, 3, 0, '0'),
(18, 11, 1, 0, '0'),
(19, 11, 2, 0, '0'),
(20, 11, 3, 0, '0'),
(21, 11, 1, 0, '0'),
(22, 11, 3, 0, '0'),
(23, 11, 1, 0, '0'),
(24, 11, 2, 0, '0'),
(25, 11, 3, 0, '0'),
(26, 11, 1, 0, '0'),
(27, 11, 2, 0, '0'),
(28, 11, 3, 0, '0'),
(29, 11, 1, 0, '0'),
(30, 11, 2, 0, '0'),
(31, 11, 1, 0, '0'),
(32, 11, 2, 0, '0'),
(33, 11, 3, 0, '0'),
(34, 11, 1, 0, '0'),
(35, 11, 2, 0, '0'),
(36, 11, 3, 0, '0'),
(37, 11, 1, 0, '0'),
(38, 11, 2, 0, '0'),
(39, 11, 3, 0, '0'),
(40, 11, 1, 0, '0'),
(41, 11, 2, 0, '0'),
(42, 11, 3, 0, '0'),
(43, 11, 1, 0, '0'),
(44, 11, 2, 0, '0'),
(45, 11, 3, 0, '0'),
(46, 11, 1, 0, '0'),
(47, 11, 2, 0, '0'),
(48, 11, 3, 0, '0'),
(49, 11, 1, 0, '0'),
(50, 11, 2, 0, '0'),
(51, 11, 3, 0, '0'),
(52, 11, 1, 0, '0'),
(53, 11, 2, 0, '0'),
(54, 11, 3, 0, '0'),
(55, 11, 1, 0, '0'),
(56, 11, 2, 0, '0'),
(57, 11, 3, 0, '0'),
(58, 11, 1, 0, '0'),
(59, 11, 2, 0, '0'),
(60, 11, 3, 0, '0'),
(61, 11, 1, 0, '0'),
(62, 11, 2, 0, '0'),
(63, 11, 3, 0, '0'),
(64, 11, 1, 0, '0'),
(65, 11, 2, 0, '0'),
(66, 11, 3, 0, '0'),
(67, 11, 1, 0, '0'),
(68, 11, 2, 0, '0'),
(69, 11, 3, 0, '0'),
(70, 11, 1, 0, '0'),
(71, 11, 2, 0, '0'),
(72, 11, 3, 0, '0'),
(73, 11, 1, 0, '0'),
(74, 11, 3, 0, '0'),
(75, 11, 1, 0, '0'),
(76, 11, 2, 0, '0'),
(77, 11, 3, 0, '0'),
(78, 11, 1, 0, '0'),
(79, 11, 2, 0, '0'),
(80, 11, 3, 0, '0'),
(81, 11, 1, 0, '0'),
(82, 11, 2, 0, '0'),
(83, 11, 3, 0, '0'),
(84, 11, 1, 0, '0'),
(85, 11, 2, 0, '0'),
(86, 11, 3, 0, '0'),
(87, 11, 1, 0, '0'),
(88, 11, 2, 0, '0'),
(89, 11, 3, 0, '0'),
(90, 11, 2, 0, '1'),
(91, 11, 3, 0, '1');

-- --------------------------------------------------------

--
-- Table structure for table `da_doctor_procedure`
--

CREATE TABLE IF NOT EXISTS `da_doctor_procedure` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `doctor_id` int(11) NOT NULL,
  `procedure_id` int(11) NOT NULL,
  `default_status` tinyint(1) NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `da_doctor_procedure`
--

INSERT INTO `da_doctor_procedure` (`id`, `doctor_id`, `procedure_id`, `default_status`, `status`) VALUES
(1, 11, 2, 0, 0),
(2, 11, 3, 0, 0),
(3, 11, 2, 0, 0),
(4, 11, 3, 0, 0),
(5, 11, 4, 0, 0),
(6, 11, 2, 0, 1),
(7, 11, 3, 0, 1),
(8, 11, 4, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `da_doctor_schedule`
--

CREATE TABLE IF NOT EXISTS `da_doctor_schedule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `doctor_id` int(11) NOT NULL,
  `name` varchar(56) NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `da_doctor_schedule`
--

INSERT INTO `da_doctor_schedule` (`id`, `doctor_id`, `name`, `from_date`, `to_date`, `date_created`, `date_modified`, `status`) VALUES
(1, 11, '2014 - Summer', '2013-12-01', '2014-03-31', '2014-02-18 11:20:00', '2014-02-18 09:15:13', 1);

-- --------------------------------------------------------

--
-- Table structure for table `da_doctor_schedule_time`
--

CREATE TABLE IF NOT EXISTS `da_doctor_schedule_time` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `schedule_id` int(11) NOT NULL,
  `address_id` int(11) NOT NULL,
  `day` varchar(10) NOT NULL,
  `from_time` time NOT NULL,
  `to_time` time NOT NULL,
  `from_time_format` varchar(5) NOT NULL,
  `to_time_format` varchar(5) NOT NULL,
  `time_slot` int(11) NOT NULL,
  `laser_slot` int(11) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `da_doctor_schedule_time`
--

INSERT INTO `da_doctor_schedule_time` (`id`, `schedule_id`, `address_id`, `day`, `from_time`, `to_time`, `from_time_format`, `to_time_format`, `time_slot`, `laser_slot`, `date_created`, `date_modified`, `status`) VALUES
(1, 1, 3, 'Wednesday', '10:00:00', '10:00:00', 'AM', 'PM', 25, 30, '2014-02-18 14:34:23', '2014-02-18 15:35:30', 1),
(2, 1, 1, 'Saturday', '10:00:00', '10:00:00', 'AM', 'PM', 20, 10, '2014-02-18 11:32:30', '2014-02-18 11:32:30', 1);

-- --------------------------------------------------------

--
-- Table structure for table `da_doctor_speciality`
--

CREATE TABLE IF NOT EXISTS `da_doctor_speciality` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `doctor_id` int(11) NOT NULL,
  `speciality_id` int(11) NOT NULL,
  `default_status` tinyint(1) NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `da_doctor_speciality`
--

INSERT INTO `da_doctor_speciality` (`id`, `doctor_id`, `speciality_id`, `default_status`, `status`) VALUES
(1, 11, 3, 0, 0),
(2, 11, 5, 1, 0),
(3, 11, 8, 0, 0),
(8, 18, 30, 1, 1),
(9, 20, 11, 1, 1),
(10, 11, 4, 0, 0),
(11, 11, 5, 0, 0),
(12, 11, 6, 0, 0),
(13, 11, 8, 0, 0),
(14, 11, 4, 0, 1),
(15, 11, 5, 0, 1),
(16, 11, 6, 0, 1),
(17, 11, 9, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `da_doctor_timeoff`
--

CREATE TABLE IF NOT EXISTS `da_doctor_timeoff` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `doctor_id` int(11) NOT NULL,
  `from_date` date NOT NULL,
  `from_time` time NOT NULL,
  `from_time_format` varchar(5) NOT NULL,
  `to_date` date NOT NULL,
  `to_time` time NOT NULL,
  `to_time_format` varchar(5) NOT NULL,
  `description` varchar(255) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `da_doctor_timeoff`
--

INSERT INTO `da_doctor_timeoff` (`id`, `doctor_id`, `from_date`, `from_time`, `from_time_format`, `to_date`, `to_time`, `to_time_format`, `description`, `date_created`, `date_modified`, `status`) VALUES
(1, 11, '2014-02-15', '12:00:00', 'AM', '2014-02-22', '12:00:00', 'PM', 'Described', '2014-02-18 09:22:00', '2014-02-18 12:34:23', 1),
(2, 11, '2014-02-05', '12:00:00', 'AM', '2014-02-26', '12:00:00', 'AM', 'Deee', '2014-02-18 12:48:21', '2014-02-18 12:48:21', 1);

-- --------------------------------------------------------

--
-- Table structure for table `da_doctor_video`
--

CREATE TABLE IF NOT EXISTS `da_doctor_video` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `doctor_id` int(11) NOT NULL,
  `name` varchar(125) NOT NULL,
  `description` text NOT NULL,
  `embeded_code` text NOT NULL,
  `default_status` enum('1','0') NOT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `da_doctor_video`
--

INSERT INTO `da_doctor_video` (`id`, `doctor_id`, `name`, `description`, `embeded_code`, `default_status`, `status`) VALUES
(1, 11, 'abc', 'det', '<iframe width="320" height="180" src="//www.youtube.com/embed/uPhZXwed07c?rel=0" frameborder="0" allowfullscreen></iframe>', '0', '0'),
(2, 11, 'def', 'def des', '<iframe width="320" height="180" src="//www.youtube.com/embed/uPhZXwed07c?rel=0" frameborder="0" allowfullscreen></iframe>', '0', '0'),
(7, 11, 'Yii framework 1 ???? ', '', '<iframe width="320" height="180" src="//www.youtube.com/embed/sPPNbGTz_wA?feature=player_detailpage" frameborder="0" allowfullscreen></iframe>', '1', '1');

-- --------------------------------------------------------

--
-- Table structure for table `da_insurance`
--

CREATE TABLE IF NOT EXISTS `da_insurance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `insurance` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `da_insurance`
--

INSERT INTO `da_insurance` (`id`, `insurance`, `description`, `status`) VALUES
(1, 'Aetna ', '', '1'),
(2, 'Blue Cross and Blue Shield', '', '1'),
(3, 'CIGNA', '', '1'),
(4, 'Commercial Insurance Company', '', '1');

-- --------------------------------------------------------

--
-- Table structure for table `da_insurance_plan`
--

CREATE TABLE IF NOT EXISTS `da_insurance_plan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `insurance_id` int(11) NOT NULL,
  `plan` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `da_insurance_plan`
--

INSERT INTO `da_insurance_plan` (`id`, `insurance_id`, `plan`, `description`, `status`) VALUES
(1, 1, 'Aetna - Advantage', '', '1'),
(2, 1, 'Aetna - Choice', '', '1'),
(3, 2, 'BCBS AZ BlueAlliance Network', '', '1'),
(4, 2, 'BCBS MI Community Blue / Blue Preferred PPO', '', '1'),
(5, 3, 'Cigna - Choice Fund', '', '1'),
(6, 3, 'Cigna - Choice Fund PPO', '', '1');

-- --------------------------------------------------------

--
-- Table structure for table `da_language`
--

CREATE TABLE IF NOT EXISTS `da_language` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `da_language`
--

INSERT INTO `da_language` (`id`, `language`, `description`, `status`) VALUES
(1, 'English', '', '1'),
(2, 'Afrikaans', '', '1'),
(3, 'Bengali', '', '1');

-- --------------------------------------------------------

--
-- Table structure for table `da_procedure`
--

CREATE TABLE IF NOT EXISTS `da_procedure` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `procedure` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `da_procedure`
--

INSERT INTO `da_procedure` (`id`, `procedure`, `description`, `status`) VALUES
(1, 'Endometrial Ablation', '', '1'),
(2, 'Heart Rhythm Disorders', '', '1'),
(3, 'Liver Blood Tests', '', '1'),
(4, 'Boils', '', '1');

-- --------------------------------------------------------

--
-- Table structure for table `da_procedures`
--

CREATE TABLE IF NOT EXISTS `da_procedures` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sp_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `deleted` enum('0','1') NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `da_speciality`
--

CREATE TABLE IF NOT EXISTS `da_speciality` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `speciality` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=33 ;

--
-- Dumping data for table `da_speciality`
--

INSERT INTO `da_speciality` (`id`, `speciality`, `description`, `status`) VALUES
(1, 'Cardiologist (Heart Doctor)', '', '1'),
(2, 'Dentist', '', '1'),
(3, 'Dermatologist', 'Dermatologist Description', '1'),
(4, 'Dietitian', '', '1'),
(5, 'Ear, Nose &amp; Throat Doctor (ENT)', 'Ear, Nose &amp; Throat Doctor (ENT) Description', '1'),
(6, 'Endocrinologist', '', '1'),
(7, 'Eye Doctor', '', '1'),
(8, 'Gastroenterologist', 'Gastroenterologist Description', '1'),
(9, 'Hematologist (Blood Specialist)', '', '1'),
(10, 'Infectious Disease Specialist', '', '1'),
(11, 'Neurologist (incl Headache Specialists)', '', '1'),
(12, 'OB-GYN (Obstetrician-Gynecologist)', '', '1'),
(13, 'Ophthalmologist', '', '1'),
(14, 'Optometrist', '', '1'),
(15, 'Orthodontist', '', '1'),
(16, 'Orthopedic Surgeon (Orthopedist)', '', '1'),
(17, 'Pain Management Specialist', '', '1'),
(18, 'Pediatric Dentist', '', '1'),
(19, 'Pediatrician', '', '1'),
(20, 'Physical Therapist (Physical Medicine)', '', '1'),
(21, 'Plastic Surgeon', '', '1'),
(22, 'Podiatrist (Foot Specialist)', '', '1'),
(23, 'Primary Care Doctor (General Practitioner)', '', '1'),
(24, 'Prosthodontist', '', '1'),
(25, 'Psychiatrist', '', '1'),
(26, 'Psychologist', '', '1'),
(27, 'Radiologist', '', '1'),
(28, 'Rheumatologist', '', '1'),
(29, 'Sleep Medicine Specialist', '', '1'),
(30, 'Sports Medicine Specialist', '', '1'),
(31, 'Urologist', '', '1'),
(32, 'Test 1', 'test ! 1', '1');

