<?php
/**
 * Created by PhpStorm.
 * User: xavie
 * Date: 10/16/15
 * Time: 4:07 PM
 */

defined('YII_DEBUG') or define('YII_DEBUG',true);

// including Yii

require_once(dirname(__FILE__) . '/framework/yii.php');
//$yii = dirname(__FILE__).'/core/yii.php';
//require_once($yii);
// we'll use a separate config file
$configFile=dirname(__FILE__).'/protected/config/console.php';

// creating and running console application
Yii::createConsoleApplication($configFile)->run();


