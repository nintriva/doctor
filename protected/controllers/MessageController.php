<?php
class MessageController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    /*public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('index','view'),
                'users'=>array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('create','update'),
                'users'=>array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('admin','delete'),
                'users'=>array('admin'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }*/


    public function actionCompose()
    {

       // print_r($_REQUEST); exit;


        if (!Yii::app()->session['logged_in']) {
            $this->redirect(array('site/index'));
        }

        if (isset($_REQUEST['submit']) && ($_REQUEST['submit'] == "Send")) {

            $mail = $this->MailingData();

            if ($mail) {
               Yii::app()->user->setFlash('sentNewMail', 'Your message is sucessfully sent.');
                $this->redirect(array('message/Compose'));
            }if($mail==0){
                Yii::app()->user->setFlash('sentNewMail', 'Your message is Not  sucessful.');
            }

        }

        $data = array();
        $criteria = new CDbCriteria(array('order'=>'id DESC'));
        $doc = Documents::model()->findAllByAttributes(array('owner' => Yii::app()->session['logged_user_id']),$criteria);
        $shared = SharedDocument::model()->findAllByAttributes(array('user_id' => Yii::app()->session['logged_user_id']));
        $document = array_merge($doc, $shared);

        $this->render('compose_mail', array(
                'data' => $data, 'document' => $document
            ));
    }

    private function MailingData()
    {
        $user_Check = $this->Type();

        $contact_arr = array();
        $contact_emailarr = array();

        if (isset($_REQUEST['contact_r_type']) && $_REQUEST['contact_r_type']) {
            if ($_REQUEST['contact_r_type'] == 'patient') {

                $user_type = 0;
            } elseif ($_REQUEST['contact_r_type'] == 'doctor') {
                $user_type = 1;
            }
            if (isset($_REQUEST['contact_r_id'][0])) {
                $contact_arr = explode(",", $_REQUEST['contact_r_id'][0]);
                $contact_emailarr = explode(",", $_REQUEST['contact_r_email'][0]);
            }
        }
        if (isset($_REQUEST['message_id']) && $_REQUEST['message_id'] != '') {

            $contact_arr = explode(",", $_REQUEST['contact_r_id']);
            $contact_emailarr = explode(",", $_REQUEST['contact_r_email']);

        }
        $emailFromEmail = "";


        if (isset(Yii::app()->session['logged_user_email_address'])) {
            $emailFromEmail = Yii::app()->session['logged_user_email_address'];
        }

        $model = new Message();
        $model->sent_from_user = Yii::app()->session['logged_user_id'];
        $model->sent_from_email = $emailFromEmail;
        $model->subject = $_REQUEST['subject'];
        $model->msg_body = $_REQUEST['msg_body'];
        $model->read_flag = 0;
        $model->created = time();
        $model->department_id= $_REQUEST['department'];
        $model->sent_from_user_type = $user_Check;
        $model->created_by = Yii::app()->session['logged_user_id'];
        if (isset($_REQUEST['thread']) && $_REQUEST['thread'] != '') {
            $model->thread_id = $_REQUEST['thread'];
        } else {

            $model->thread_id = uniqid();
        }
        $model->status = 1;

        if ($model->save()) {
            $message_id = $model->inbox_id;

            //logging the created mssges are done here....
            $log = new Log ;
            $log->user_id = Yii::app()->session['logged_user_id'];
            $log->user_type = Yii::app()->session['logged_user_type'];
            $log->controller = $this->getId();
            $log->action = $this->getAction()->getId();
            $log->created_at = time();
            $log->request =Yii::app()->request->requestUri ;
            $log->param = $message_id;
            $log->save();



            if (isset($_REQUEST['docs_search_id']) && ($_REQUEST['docs_search_id'])) {
                if (Yii::app()->controller->action->id == 'Compose') {
                    $docs_search_id = $_REQUEST['docs_search_id'][0];
                } else {
                    $docs_search_id = $_REQUEST['docs_search_id'];
                }
                $string = urldecode($docs_search_id);

                $arr = explode('&', $string);

                if (isset($arr) && $arr[0] != '') {
                    //foreach(array_slice($arr,1) as $key=>$value)
                    foreach ($arr as $key => $value) {
                        $idarr = explode('=', $value);

                        $idarr = explode('_', $idarr[1]);
                        if (isset($idarr) && $idarr[0] != '') {
                            $id = $idarr[0];

                            $this->SharedDocument($id, $user_type, $message_id, $contact_arr);
                        }
                    }
                }

            }

            $message = new MessageInbox();
            $message->department_id=$_REQUEST['department'];
            $message->message_id = $message_id;
            $message->user_id = Yii::app()->session['logged_user_id'];
            $message->message_type = 0;
            $message->user_type = $user_Check;
            $message->user_email = $emailFromEmail;
            $message->save(false);

            foreach ($contact_arr as $key => $val) {
                if ($contact_arr[$key] != '' || $contact_arr[$key] != null) {
                    $message = new MessageInbox();
                    $message->department_id=$_REQUEST['department'];
                    $message->user_id = $contact_arr[$key];
                    $message->message_id = $message_id;
                    $message->user_type = $user_type;
                    $message->message_type = 1;
                    $message->user_email = $contact_emailarr[$key];
                    if($message->save(false)){
                        $check=1;
                        if($user_type==0){
                            $to_patient=Patient::model()->findByAttributes(array('id'=>$contact_arr[$key]));
                            $to_id=$contact_arr[$key];
                            $to_email= $contact_emailarr[$key];
                            $to_name=$to_patient->user_first_name." ".$to_patient->user_last_name;
                            $fromdoctor=Doctor::model()->findByAttributes(array('id'=> Yii::app()->session['logged_user_id']));
                          //  $this->mailSend($to_id,$to_email,$to_name,$emailFromEmail,$fromdoctor);
                        }
                    }else{
                        $check=0;
                    }
                }
            }
            if($check==0){
                return $check;
            }else{
                return true;
            }


        } else {
            return $model->errors;
        }
    }
    private function getTemplateDataParsing($parse_data_arr, $message)
    {
        foreach ($parse_data_arr as $parse_data_key => $parse_data_val) {
            $message = str_replace($parse_data_key, $parse_data_val, $message);
        }
        return $message;
    }

    public function mailSend($to_id,$to_email,$to_name,$emailFromEmail,$fromdoctor){
            $from_name = $fromdoctor->full_name;
            $temp_id = 17;
            $email_template = EmailTemplate::model()->findByAttributes(array('name'=>'New Message Received'));
            if(!empty($email_template) && $email_template->tempalte_body != ''){
                $message_body = $email_template->tempalte_body;
                $parse_data_arr = array();
                $parse_data_arr['{{$to_name}}'] =  $to_name;
                $parse_data_arr['{{$from_name}}'] = $from_name;
                $message_body = $this->getTemplateDataParsing($parse_data_arr,$message_body);

            }else{
                $message_body = '';
            }
            $to = array($to_email,$to_name);
            $from = array('support@edoctorbook.com','eDoctorBook');
            $subject = isset( $email_template->subject ) ? $email_template->subject : 'eDoctorBook - Request for Join Us.';

            $message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                        <html xmlns="http://www.w3.org/1999/xhtml">
                        <head>
                        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                        <title'.$subject.'</title>
                        </head>
                        <body style="padding:0px; margin:0px; font-size:12px; color:#545454; line-height:18px; font-family:Arial, Helvetica, sans-serif;">
                        <div style="width:450px;  margin:5px auto; border:1px solid #c7c7c7; ">
                        <div style="text-align:left; margin-bottom:15px; -moz-box-shadow:0px 1px 1px 1px #E4E4E4; -webkit-box-shadow:0px 1px 1px 1px #E4E4E4;box-shadow:0px 1px 1px 1px #E4E4E4; background:#54cbc8; font-weight:bold;">

                        &nbsp;
                        </div>
                        <div style="min-height:200px; background:#fff; color:#bbb; margin:10px; padding-top:15px;">
                        <p style="color:#000">
                        '.$message_body.'
                        </p>
                        <br>
                        </div>
                        <p style=" margin-top:10px; padding:8px 5px; color:#000; font-size:13px; margin-bottom:0px; font-weight:bold; line-height:18px; ">eDoctorBook Team</p>
                        <a style="padding-left:5px; padding-right:5px; color:#000; font-size:13px; font-weight:bold; margin:0px; line-height:0px; text-decoration:none" href="">http://www.eDoctorBook.com</a>
                        <div style="background:#54cbc8; padding-top:20px; margin-top:15px; padding-bottom:10px; text-align:center; color:#e2e2e2; font-size:14px;"></div>
                        </div>
                        </body>
                        </html>';

            Helpers::mailsend($to,$from,$subject,$message);
    }


    public function actionReplyMail()
    {
       // print_r('mailed');

        $this->layout = false;
        $mail = $this->MailingData();
        if ($mail) {
            echo $mail;
        }
    }


    public function SharedDocument($id, $type, $mid, $contact_arr)
    {
        foreach ($contact_arr as $key => $val) {
            $shared = new SharedDocument();
            $shared->document_id = $id;
            if ($type == 1) {
                $shared->shared_type = "d";
            }
            if ($type == 0) {
                $shared->shared_type = "p";
            }
            $shared->shared_id = $val;
            $shared->date = date('Y-m-d H:m:s');
            $shared->message_id = $mid;
            $shared->save(false);
        }
        return true;
    }

    public function actionAddQuickMail()
    {
        $user_Check = $this->Type();

        $model = new Quickmessages();
        $model->messages = $_REQUEST['message'];
        $model->user_id = Yii::app()->session['logged_user_id'];
        $model->user_type = $user_Check;
        if ($model->save()) {
            $search_link = CHtml::CheckBox('idList[]', false, array('id' => 'idList[]', 'value' => $model->id . '_' . $model->messages));
            $result = '<li>
                         <span class="active" style="width:25%" >' . $model->messages . '</span>


                         <span class="att txt_align">' . $search_link . '</span></li>';
            echo $result;
        } else {
            echo $model->errors;
        }
    }

    public function actionAddFavouriteMail()
    {
        $user_Check = $this->Type();
        $contact_arr = explode(",", $_REQUEST['contact_id']);
        $contact_email = explode(",", $_REQUEST['contact_email']);
        $contact_name = explode(",", $_REQUEST['contact_name']);
        if ($_REQUEST['contact_type'] == 'patient') {
            $user_type = 0;
        } elseif ($_REQUEST['contact_type'] == 'doctor') {
            $user_type = 1;
        }

        foreach ($contact_arr as $key => $val) {
            $model = new MailsFavorites();
            $model->fav_id = $contact_arr[$key];
            $model->fav_type = $user_type;
            $model->fav_email = $contact_email[$key];
            $model->fav_name = $contact_name[$key];
            $model->user_id = Yii::app()->session['logged_user_id'];
            $model->user_type = $user_Check;
            $model->save();
        }
        $result=$this->FetchFavouriteMails($user_type);
        if ($result == '') echo 'No Result Found!';
        else echo $result;
    }


    public function actionFavouriteSelect()
    {
        if (isset($_REQUEST['send_type']) && $_REQUEST['send_type']) {
            if ($_REQUEST['send_type'] == 'doctor') {
                $fav_type = 1;
            } elseif ($_REQUEST['send_type'] == 'patient') {
                $fav_type = 0;
            }
        }
        $result=$this->FetchFavouriteMails($fav_type);
        if ($result == '') echo 'No Result Found!';
        else echo $result;

    }

    private function FetchFavouriteMails($fav_type){
        $user_Check = $this->Type();

        $criteria = new CDbCriteria();
        $criteria->order = 'id DESC';
        $MailsFavorites = MailsFavorites::model()->findAllByAttributes(array('user_id' => Yii::app()->session['logged_user_id'], 'user_type' => $user_Check, 'fav_type' => $fav_type), $criteria);

        if ($MailsFavorites) {
            $result = '';
            for ($i = 0; $i < count($MailsFavorites); $i++) {
                $search_link = CHtml::CheckBox('idList[]', false, array('id' => 'idList[]', 'value' => $MailsFavorites[$i]['fav_id'] . '--' . $MailsFavorites[$i]['fav_name'] . '--' . $MailsFavorites[$i]['fav_email'] . '--' . $MailsFavorites[$i]['fav_type']));
                $usert=$MailsFavorites[$i]['fav_type'];
                $result .= '<li>
                         <span class="active" style="width:20%" >' . $MailsFavorites[$i]['fav_name'] . '</span>
						
                         <span class="att txt_align">' . $search_link . '</span></li>';
            }
        }
        return $result;
    }
    public function Type()
    {
     //  print_r(Yii::app()->session['logged_user_type'] ); exit;
        if (!Yii::app()->session['logged_in']) {
            $this->redirect(array('site/index'));
        }
        if (Yii::app()->session['logged_user_type'] == "doctor") {
            $user_Check = 1;
        } elseif (Yii::app()->session['logged_user_type'] == 'patient') {
            $user_Check = 0;
        }
        elseif(Yii::app()->session['logged_user_type'] == 'parcticeAdmin'){
            $user_Check = 1;
        }

        return $user_Check;
    }


    public function actiondetails($msg, $msgType, $pgn = '0')
    {

        if (!Yii::app()->session['logged_in']) {
            $this->redirect(array('site/index'));
        }

        $msg_id = $msg;
        $connection = Yii::app()->db;
        $user_Check = $this->Type();
        $idcondition = $this->ThreadDetails($msg_id);
        if ($idcondition != null) {

            $sql3 = "SELECT  da_inbox.*  ,da_message_inbox.user_id,da_message_inbox.user_type,da_message_inbox.message_type FROM da_inbox LEFT JOIN da_message_inbox ON (da_inbox.inbox_id=da_message_inbox.message_id) $idcondition
              ORDER BY  da_inbox.inbox_id DESC  ";
            $command = $connection->createCommand($sql3);
            $message_Touser = $command->queryAll();
            $user_arr = array();

            foreach ($message_Touser as $key => $usermessage) {
                $user_arr[$key]['inbox_id'] = $usermessage['inbox_id'];
                if ($usermessage['sent_from_user_type'] == 0) {

                    $user = Patient::model()->findByAttributes(array('id' => $usermessage['sent_from_user']));
                    if ($user) {
                        $user_arr[$key]['from'][0]['form_name'] = $user->user_first_name . ' ' . $user->user_last_name;
                        $user_arr[$key]['from'][0] ['form_email'] = $user->user_email;
                        $user_arr[$key]['from'][0] ['form_id'] = $user->id;
                        $user_arr[$key]['from'][0]['name'] = $user->user_first_name . ' ' . $user->user_last_name;
                        $user_arr[$key]['from'][0] ['email'] = $user->user_email;
                        $user_arr[$key]['from'][0] ['id'] = $user->id;
                        $user_arr[$key]['from'][0] ['type'] = 'patient';
                        $user_arr[$key]['from'][0]['vic_dob'] = date('d-m-Y', strtotime($user->user_dob));

                    }

                } elseif ($usermessage['sent_from_user_type'] == 1) {
                    $user = Doctor::model()->findByAttributes(array('id' => $usermessage['sent_from_user']));
                    if ($user) {
                        $user_arr[$key]['from'][0]['form_name'] = $user->full_name;
                        $user_arr[$key]['from'][0] ['form_email'] = $user->email;
                        $user_arr[$key]['from'][0] ['form_id'] = $user->id;
                        $user_arr[$key]['from'][0]['name'] = $user->full_name;
                        $user_arr[$key]['from'][0] ['email'] = $user->email;
                        $user_arr[$key]['from'][0] ['id'] = $user->id;
                        $user_arr[$key]['from'][0] ['type'] = 'doctor';

                    }
                }
                if ($usermessage['user_type'] == 0) {
                    $user = Patient::model()->findByAttributes(array('id' => $usermessage['user_id']));

                    if ($user) {
                        if ($usermessage['message_type'] == 1) {
                            $user_arr[$key]['to'][0]['id'] = $user->id;
                            $user_arr[$key]['to'][0]['type'] = 'patient';
                            $user_arr[$key]['to'][0]['name'] = $user->user_first_name . ' ' . $user->user_last_name;
                            $user_arr[$key]['to'][0]['email'] = $user->user_email;
                            $user_arr[$key]['to'][0]['vic_dob'] = date('d-m-Y', strtotime($user->user_dob));

                        }
                    }

                } elseif ($usermessage['user_type'] == 1) {
                    $user = Doctor::model()->findByAttributes(array('id' => $usermessage['user_id']));
                    if ($user) {
                        if ($usermessage['message_type'] == 1) {
                            $user_arr[$key]['to'][0]['id'] = $user->id;
                            $user_arr[$key]['to'][0]['type'] = 'doctor';
                            $user_arr[$key]['to'][0]['name'] = $user->full_name;
                            $user_arr[$key]['to'][0]['email'] = $user->email;

                        }
                    }
                }
            }

            foreach ($user_arr as $array) {
                foreach ($message_Touser as $key => $userMesg) {
                    if ($userMesg['inbox_id'] == $array['inbox_id']) {
                        if (isset($array['from'])) {
                            $message_Touser[$key] = array_merge($message_Touser[$key], $array['from'][0]);
                            $message_Touser[$key]['from'] = $array['from'];
                        }
                        if (isset($array['to'])) {
                            if (!isset($message_Touser[$key]['to'])) {
                                $message_Touser[$key]['to'] = $array['to'];
                            } else {
                                array_push($message_Touser[$key]['to'], $array['to'][0]);
                            }
                        }

                    }

                }

            }

            $userNames = array();
            $array = $message_Touser;
            /* looping through array */
            foreach ($array as $key => $value) {
                if (!empty($userNames) && in_array($value['inbox_id'], $userNames)) unset($array[$key]); //unset from $array if username already exists
                $userNames[] = $value['inbox_id']; // creating username array to compare with main array values
            }


        } else {
            $array = array();
        }

        if (count($array) > 0) {
            if ($msgType) {
                if (isset($msg_id) && $msg_id != 0) {
                    $sql2 = "SELECT da_message_inbox.message_id FROM da_message_inbox LEFT JOIN da_inbox ON (da_inbox.inbox_id=da_message_inbox.message_id) WHERE  da_message_inbox.user_id= '" . Yii::app()->session['logged_user_id'] . "' AND da_message_inbox.user_type='" . $user_Check . "' AND da_inbox.thread_id='" . $msg_id . "'";
                    $command = $connection->createCommand($sql2);
                    $message_ids = $command->queryAll();
                    $messages = [];
                    foreach ($message_ids as $ar) {

                        array_push($messages, $ar['message_id']);
                    }

                    if ($messages) {
                        $ids = join(',', $messages);
                        $idcondition = " da_message_inbox.message_id IN  (" . $ids . ") AND";
                    } else {

                        $idcondition = " ";
                    }

                }
                if (isset($idcondition) && $idcondition != '') {

                    $sqlUpdate = "UPDATE da_message_inbox SET message_read_flag = :message_read_flag WHERE  $idcondition   da_message_inbox.user_id= '" . Yii::app()->session['logged_user_id'] . "' AND da_message_inbox.user_type='" . $user_Check . "' ";
                    $paramsUpdate = array(
                        "message_read_flag" => 1
                    );

                    $connection = Yii::app()->db;
                    $command = $connection->createCommand($sqlUpdate);
                    $command->execute($paramsUpdate);

                    $count = Yii::app()->session['inbox_count'];
                    if ($count > 0) {
                        Yii::app()->session['inbox_count'] = $count - 1;
                    }
                }

            }


            $this->render('msg_details', array(
                    'inbox_arr_details' => $array, 'pgn' => $pgn,
                ));
        } else {
            $this->redirect(array('message/inbox'));
        }


    }


    public function actioninbox()
    {
        if (!Yii::app()->session['logged_in']) {
            $this->redirect(array('site/index'));
        }
        $data['inbox'] = array();
        $searchName="";
        $searchId="";
        $user_Check = $this->Type();
        $searchType="";
        $userType = Yii::app()->session['logged_user_type'];
        $connection = Yii::app()->db;

        $condition = "";
        $limit = 10;
        $end_limit = 10;
        if(isset($_REQUEST['searchtype']))
        {
           $searchType= $_REQUEST['searchtype'];
        }
        if (isset($_REQUEST['msg_limit_drp']) && ($_REQUEST['msg_limit_drp'] != "")) {
            $limit = $_REQUEST['msg_limit_drp'];
            $end_limit = $_REQUEST['msg_limit_drp'];
            $msg_limit_drp = $_REQUEST['msg_limit_drp'];
        }
        if (isset($_REQUEST['page']) && $_REQUEST['page'] > 1) {
            $star_limit = ($limit * ($_REQUEST['page'] - 1));
        } else {
            $star_limit = 0;
        }

        if(isset($_REQUEST['department'])&&$_REQUEST['department']!=''){
            $department=" AND i.`department_id`=" . $_REQUEST['department'];
            $dept=$_REQUEST['department'];
        }
        else{
            $dept="";
            $department = '';
        }

        if(isset($_REQUEST['search']) && $_REQUEST['search']){

            $name=$_REQUEST['search'];
            if($_REQUEST['searchtype']=='patient'){
            $search_name = Patient::model()->findByPk($name);

            $searchId = $name;
            $searchName = $search_name->user_first_name.' '.$search_name->user_last_name;
            }
            else
            {
                $search_name = Doctor::model()->findByPk($name);
                $searchId = $name;
                $searchName = $search_name->full_name;
            }

            if($_REQUEST['searchtype']=='doctor'){
                $nameCheck="AND (d.id = '".$name."')";
            }elseif($_REQUEST['searchtype']=='patient'){
                $nameCheck="AND (p.id = '".$name."')";
            }
        }else{

            $nameCheck='';
        }

        if ($userType == "doctor"||$userType == "parcticeAdmin" ||$userType == "patient") {
            $sql = "select i.*,MAX(i.inbox_id) as in_id ,db_shared_document.shared_id,db_shared_document.shared_type,
           (SELECT da_message_inbox.message_read_flag FROM da_message_inbox   WHERE message_id=MAX(i.inbox_id) AND da_message_inbox.user_id='" . Yii::app()->session['logged_user_id'] . "' AND da_message_inbox.user_type='" . $user_Check . "' AND da_message_inbox.message_type=1
            AND da_message_inbox.is_archive=0 AND da_message_inbox.is_delete=0) as max_read ,
            (SELECT msg_body FROM da_inbox  WHERE inbox_id=MAX(i.inbox_id)) as msg_body,
            IF((i.sent_from_user_type=1),(d.`full_name` ),(concat_ws(' ', p.user_first_name,  p.user_last_name )
            ) )as `vic_name`,IF((i.sent_from_user_type=1),( d.`email`),(
            p.`user_email` ) )  as `vic_email`
            FROM da_inbox as i
            LEFT JOIN `da_message_inbox`  ON(i.inbox_id = da_message_inbox.`message_id`)
            LEFT JOIN `da_patient` as p ON(p.`id` = i.`sent_from_user`) AND i.sent_from_user_type=0
              LEFT JOIN `da_doctor` as d ON((d.`id` = i.`sent_from_user`)) AND i.sent_from_user_type=1
              LEFT JOIN db_shared_document ON (db_shared_document.message_id=i.`inbox_id`) AND db_shared_document.shared_type= 'd'
            WHERE i.`status`=1 $department AND da_message_inbox.user_id='" . Yii::app()->session['logged_user_id'] . "' AND da_message_inbox.user_type='" . $user_Check . "' AND da_message_inbox.message_type=1
            AND da_message_inbox.is_archive=0 AND da_message_inbox.is_delete=0   $nameCheck GROUP BY i.thread_id
             ORDER BY `in_id` DESC LIMIT $star_limit , $end_limit";

        } else {

            $sql = "select i.*,MAX(i.inbox_id) as in_id ,d.`full_name` as `vic_name`, d.`email` as `vic_email`,db_shared_document.shared_id,db_shared_document.shared_type,
            (SELECT da_message_inbox.message_read_flag FROM da_message_inbox   WHERE message_id=MAX(i.inbox_id) AND da_message_inbox.user_id='" . Yii::app()->session['logged_user_id'] . "' AND da_message_inbox.user_type='" . $user_Check . "' AND da_message_inbox.message_type=1
            AND da_message_inbox.is_archive=0 AND da_message_inbox.is_delete=0) as max_read,(SELECT msg_body FROM da_inbox  WHERE inbox_id=MAX(i.inbox_id)) as msg_body  FROM da_inbox as i
            LEFT JOIN `da_message_inbox`  ON(i.inbox_id = da_message_inbox.`message_id`)
            LEFT JOIN `da_doctor` as d ON((d.`id` = i.`sent_from_user`))
               LEFT JOIN db_shared_document ON (db_shared_document.message_id=i.`inbox_id`) AND db_shared_document.shared_type= 'p'
               WHERE i.`status`=1 AND da_message_inbox.user_id='" . Yii::app()->session['logged_user_id'] . "' AND da_message_inbox.message_type=1 AND da_message_inbox.is_delete=0   AND da_message_inbox.is_archive=0 AND da_message_inbox.user_type='" . $user_Check . "' GROUP BY i.thread_id ORDER BY `in_id` DESC LIMIT $star_limit , $end_limit";

        }

        $command = $connection->createCommand($sql);
        $inbox_arr = $command->queryAll();


        $sqlCount = "select * FROM da_inbox LEFT JOIN `da_message_inbox`  ON(da_inbox.inbox_id = da_message_inbox.`message_id`) WHERE da_inbox.status=1 AND da_message_inbox.user_id='" . Yii::app()->session['logged_user_id'] . "' AND da_message_inbox.message_type=1 AND da_message_inbox.is_archive=0 AND da_message_inbox.user_type='" . $user_Check . "' GROUP BY da_inbox.thread_id";
        //$sqlCount="select * FROM da_inbox WHERE status=1 AND `sent_to_user`='".Yii::app()->session['logged_user_id']."'";
        $commandCount = $connection->createCommand($sqlCount);
        $inbox_arr_count = $commandCount->queryAll();
        $count = count($inbox_arr_count);
        $pages = new CPagination($count);
        $criteria = new CDbCriteria(array( //'condition' => $condition,
                                           //'order' => 'category_id DESC'
        ));

        // results per page
        $pages->pageSize = $limit;
        $pages->applyLimit($criteria);
        /*echo '<pre>';
        print_r($inbox_arr); exit;*/

        $this->render('inbox', array(
                'inbox_data' => $inbox_arr, 'pages' => $pages, 'msg_limit_drp' => $limit,'dept'=>$dept,'searchType'=>$searchType,'searchId'=>$searchId,'searchName'=>$searchName ));
    }


    public function actionArchiveMail()
    {
        if (!Yii::app()->session['logged_in']) {
            $this->redirect(array('site/index'));
        }
        $searchName="";
        $searchId="";
        $user_Check = $this->Type();
        $searchType="";
        $userType = Yii::app()->session['logged_user_type'];
        $connection = Yii::app()->db;

        $data['inbox'] = array();
        $user_Check = $this->Type();
        $userType = Yii::app()->session['logged_user_type'];
        $connection = Yii::app()->db;

        $condition = "";
        $limit = 10;
        $end_limit = 10;
        if (isset($_REQUEST['page']) && $_REQUEST['page'] > 1) {
            $star_limit = ($limit * ($_REQUEST['page'] - 1));
        } else {
            $star_limit = 0;
        }

        if(isset($_REQUEST['department'])&&$_REQUEST['department']!=''){
            $department=" AND i.`department_id`=" . $_REQUEST['department'];
            $dept=$_REQUEST['department'];
        }
        else{
            $dept="";
            $department = '';
        }

        if(isset($_REQUEST['search']) && $_REQUEST['search']){

            $name=$_REQUEST['search'];
            if($_REQUEST['searchtype']=='patient'){
                $search_name = Patient::model()->findByPk($name);

                $searchId = $name;
                $searchName = $search_name->user_first_name.' '.$search_name->user_last_name;
            }
            else
            {
                $search_name = Doctor::model()->findByPk($name);
                $searchId = $name;
                $searchName = $search_name->full_name;
            }

            if($_REQUEST['searchtype']=='doctor'){
                $nameCheck="AND (d.id = '".$name."')";
            }elseif($_REQUEST['searchtype']=='patient'){
                $nameCheck="AND (p.id = '".$name."')";
            }
        }else{

            $nameCheck='';
        }

        if ($userType == "doctor"||$userType == "parcticeAdmin" ||$userType == "patient") {
            $sql = "select i.*,MAX(i.inbox_id) as in_id ,db_shared_document.shared_id,db_shared_document.shared_type,
           (SELECT da_message_inbox.message_read_flag FROM da_message_inbox   WHERE message_id=MAX(i.inbox_id) AND da_message_inbox.user_id='" . Yii::app()->session['logged_user_id'] . "' AND da_message_inbox.user_type='" . $user_Check . "'
            AND da_message_inbox.is_archive=1 AND da_message_inbox.is_delete=0) as max_read ,
            (SELECT msg_body FROM da_inbox  WHERE inbox_id=MAX(i.inbox_id)) as msg_body,
            IF((i.sent_from_user_type=1),(d.`full_name` ),(concat_ws(' ', p.user_first_name,  p.user_last_name )
            ) )as `vic_name`,IF((i.sent_from_user_type=1),( d.`email`),(
            p.`user_email` ) )  as `vic_email`
            FROM da_inbox as i
            LEFT JOIN `da_message_inbox`  ON(i.inbox_id = da_message_inbox.`message_id`)
            LEFT JOIN `da_patient` as p ON(p.`id` = i.`sent_from_user`) AND i.sent_from_user_type=0
              LEFT JOIN `da_doctor` as d ON((d.`id` = i.`sent_from_user`)) AND i.sent_from_user_type=1
              LEFT JOIN db_shared_document ON (db_shared_document.message_id=i.`inbox_id`) AND db_shared_document.shared_type= 'd'
            WHERE i.`status`=1 $department AND da_message_inbox.user_id='" . Yii::app()->session['logged_user_id'] . "' AND da_message_inbox.user_type='" . $user_Check . "'
            AND da_message_inbox.is_archive=1 AND da_message_inbox.is_delete=0   $nameCheck GROUP BY i.thread_id
             ORDER BY `in_id` DESC LIMIT $star_limit , $end_limit";

        } else {

            $sql = "select i.*,MAX(i.inbox_id) as in_id ,d.`full_name` as `vic_name`, d.`email` as `vic_email`,db_shared_document.shared_id,db_shared_document.shared_type,
            (SELECT da_message_inbox.message_read_flag FROM da_message_inbox   WHERE message_id=MAX(i.inbox_id) AND da_message_inbox.user_id='" . Yii::app()->session['logged_user_id'] . "' AND da_message_inbox.user_type='" . $user_Check . "'
            AND da_message_inbox.is_archive=0 AND da_message_inbox.is_delete=0) as max_read,(SELECT msg_body FROM da_inbox  WHERE inbox_id=MAX(i.inbox_id)) as msg_body  FROM da_inbox as i
            LEFT JOIN `da_message_inbox`  ON(i.inbox_id = da_message_inbox.`message_id`)
            LEFT JOIN `da_doctor` as d ON((d.`id` = i.`sent_from_user`))
               LEFT JOIN db_shared_document ON (db_shared_document.message_id=i.`inbox_id`) AND db_shared_document.shared_type= 'p'
               WHERE i.`status`=1 AND da_message_inbox.user_id='" . Yii::app()->session['logged_user_id'] . "' AND da_message_inbox.message_type=1 AND da_message_inbox.is_delete=0   AND da_message_inbox.is_archive=0 AND da_message_inbox.user_type='" . $user_Check . "' GROUP BY i.thread_id ORDER BY `in_id` DESC LIMIT $star_limit , $end_limit";

        }




        $command = $connection->createCommand($sql);
        $inbox_arr = $command->queryAll();


        $sqlCount = "select * FROM da_inbox LEFT JOIN `da_message_inbox`  ON(da_inbox.inbox_id = da_message_inbox.`message_id`) WHERE da_inbox.status=1 AND da_message_inbox.user_id='" . Yii::app()->session['logged_user_id'] . "'  AND da_message_inbox.is_archive=1 AND da_message_inbox.is_delete=0 AND da_message_inbox.user_type='" . $user_Check . "' ";
        //$sqlCount="select * FROM da_inbox WHERE status=1 AND `sent_to_user`='".Yii::app()->session['logged_user_id']."'";
        $commandCount = $connection->createCommand($sqlCount);
        $inbox_arr_count = $commandCount->queryAll();
        $count = count($inbox_arr_count);


        $pages = new CPagination($count);
        $criteria = new CDbCriteria(array( //'condition' => $condition,
                                           //'order' => 'category_id DESC'
        ));

        // results per page
        $pages->pageSize = $limit;
        $pages->applyLimit($criteria);
        $this->render('archive_mail', array(
                'inbox_data' => $inbox_arr, 'pages' => $pages, 'msg_limit_drp' => $limit,'dept'=>$dept,'searchType'=>$searchType,'searchId'=>$searchId,'searchName'=>$searchName
        ));
    }


    public function actionsentMail()
    {
        if (!Yii::app()->session['logged_in']) {
            $this->redirect(array('site/index'));

            }
        $limit = 10;
        $end_limit = 10;

        if (isset($_REQUEST['msg_limit_drp']) && ($_REQUEST['msg_limit_drp'] != "")) {
            $limit = $_REQUEST['msg_limit_drp'];
            $end_limit = $_REQUEST['msg_limit_drp'];
            $msg_limit_drp = $_REQUEST['msg_limit_drp'];
        }
        if (isset($_REQUEST['page']) && $_REQUEST['page'] > 1) {
            $star_limit = ($limit * ($_REQUEST['page'] - 1));
        } else {
            $star_limit = 0;
        }

        $user_Check = $this->Type();
        $sql = "
         SELECT `message_id` FROM `da_message_inbox` WHERE `user_id`='" . Yii::app()->session['logged_user_id'] . "' AND `message_type`=0 AND `user_type`='" . $user_Check . "' AND  `message_id`!=0 AND is_archive=0 AND da_message_inbox.is_delete=0 ORDER BY `message_id`  DESC
 ";

        $connection = Yii::app()->db;
        $command = $connection->createCommand($sql);
        $id_arr = $command->queryAll();
        $messages = [];
        foreach ($id_arr as $ar) {

            array_push($messages, $ar['message_id']);
        }

        if ($messages) {
            $ids = join(',', $messages);
            $idcondition = "da_message_inbox.message_id IN  (" . $ids . ")  AND  ";
        }
        $inbox_arr = array();
        if (isset($idcondition) && $idcondition != '') {

            $sql = "SELECT `user_type`,`user_email`,message_id FROM `da_message_inbox` WHERE $idcondition  `message_type`=1 ";
            $command = $connection->createCommand($sql);
            $msg_arr = $command->queryAll();

            $data = array();


            foreach ($msg_arr as $key => $messag_arr) {
                $data[$key]['to'][0]['inbox_id'] = $messag_arr['message_id'];
                if ($messag_arr['user_type'] == 0) {
                    $patient = Patient::model()->findByAttributes(array('user_email' => $messag_arr['user_email']));
                    if ($patient) {
                        //$data[$key]=$patient->attributes;
                        $data[$key]['to'][0]['vic_name'] = $patient->user_first_name . " " . $patient->user_last_name;
                        $data[$key]['to'][0]['vic_email'] = $patient->user_email;
                    }
                } elseif ($messag_arr['user_type'] == 1) {
                    $doctor = Doctor::model()->findByAttributes(array('email' => $messag_arr['user_email']));
                    if ($doctor) {
                        $data[$key]['to'][0]['vic_name'] = $doctor->full_name;
                        $data[$key]['to'][0]['vic_email'] = $doctor->email;
                    }
                }

            }

            $sql = "SELECT `user_type`,`user_email`,`message_id`,is_archive ,da_inbox.*
FROM `da_message_inbox`
LEFT JOIN da_inbox ON da_inbox.inbox_id=da_message_inbox.`message_id`
WHERE `user_id`='" . Yii::app()->session['logged_user_id'] . "' AND `user_type`='" . $user_Check . "'
AND `message_type`=0 AND `message_id` !=0 AND da_message_inbox.is_archive=0 AND da_message_inbox.is_delete=0 GROUP BY da_inbox.thread_id
ORDER BY `id` DESC LIMIT $star_limit , $end_limit ";
            $command = $connection->createCommand($sql);
            $msg_arr = $command->queryAll();
            foreach ($data as $array) {
                foreach ($msg_arr as $key => $userMesg) {
                    if ($userMesg['inbox_id'] == $array['to'][0]['inbox_id']) {
                        if (isset($array['to'])) {
                            if (!isset($msg_arr[$key]['to'])) {
                                $msg_arr[$key]['to'] = $array['to'];
                            } else {
                                array_push($msg_arr[$key]['to'], $array['to'][0]);
                            }
                        }
                    }
                }
            }
            foreach($msg_arr as $key=>$val){
                if(!isset($msg_arr[$key]['to'])){
                    unset($msg_arr[$key]);
                }

            }
            $inbox_arr = $msg_arr;
        }
        $count = count($inbox_arr);
        $pages = new CPagination($count);
        $criteria = new CDbCriteria(array());
        // results per page
        $pages->pageSize = $limit;
        $pages->applyLimit($criteria);

        foreach($inbox_arr as $key=>$arr)
        {

            $ind  = $arr['thread_id'];

            $sq= "SELECT * from da_inbox where thread_id='".$ind."'";
            $connection = Yii::app()->db;
            $command = $connection->createCommand($sq);
            $id_arr = $command->queryAll();
            $sharedcount=0;
            foreach($id_arr as $a)
            {
                $in=$a['inbox_id'];
                $sharedcount = $sharedcount+count(SharedDocument::model()->findAllByAttributes(array('message_id' => $in)));


            }
            $inbox_arr[$key]['attachment_count']=$sharedcount;
        }



        $this->render('sent_mail', array(
                'inbox_data' => $inbox_arr, 'pages' => $pages, 'msg_limit_drp' => $limit,
            ));
    }


    public function actiongetContacts()
    {

        $this->layout = false;
        $keyword = $_REQUEST['keyword'] . '%';
        $sent_to_persion = $_REQUEST['sent_to_persion'];
        $userId = Yii::app()->session['logged_user_id'];
        $userType = Yii::app()->session['logged_user_type'];


        if (Yii::app()->session['logged_in']) {
            $connection = Yii::app()->db;
            $user_seleted_type = "";

            if ($userType == "doctor" ||$userType == "parcticeAdmin") {
                if ($sent_to_persion == "doctor") {
                    $sql = 'select d.`id` as `vic_id`,d.`id` as `id`,d.`full_name` as `vic_name`,d.`full_name` as `name`, d.`email` as `vic_email` FROM `da_doctor` as d WHERE  d.`full_name` LIKE "' . $keyword . '" and payment_type ="standard" GROUP BY d.`id`';

                    $command = $connection->createCommand($sql);
                    $user_details = $command->queryAll();
                    $user_details_res = array();
                    $user_seleted_type = "doctor";
                    /*foreach ($user_details as $key=>$val) {
                        $user_details_res[]
                    }*/
                } else {
                    $sql = 'select db.`patient_id` as pid, p.`id` as `vic_id`,p.`id` as `id`,p.`user_first_name`,p.`user_last_name`,concat_ws(" ", p.user_first_name,  p.user_last_name ) as `vic_name`,concat_ws(" ", p.user_first_name,  p.user_last_name ) as `name` ,p.`user_email` as `vic_email`,DATE_FORMAT(p.`user_dob` ,"%m-%d-%Y") as `vic_dob` FROM `da_doctor_book` as db LEFT JOIN `da_patient` as p ON(p.`id` = db.`patient_id`) WHERE db.`doctor_id`= "' . $userId . '" AND (p.`user_first_name` LIKE "' . $keyword . '" OR p.`user_last_name` LIKE "' . $keyword . '")  GROUP BY p.`id`';

                    $command = $connection->createCommand($sql);
                    $user_details = $command->queryAll();
                    $user_details_res = array();
                    $user_seleted_type = "patient";
                }
            } else if ($userType == "patient") {
                $sql = 'select d.`id` as `vic_id`,d.`id` as `id`,d.`full_name` as `vic_name`,d.`full_name` as `name`, d.`email` as `vic_email`,db.`doctor_id` FROM `da_doctor_book` as db LEFT JOIN `da_doctor` as d ON(d.`id` = db.`doctor_id`) WHERE db.`patient_id`= "' . $userId . '" AND d.`full_name` LIKE "' . $keyword . '" GROUP BY d.`id`';
                $command = $connection->createCommand($sql);
                $user_details = $command->queryAll();
                $user_details_res = array();
                $user_seleted_type = "doctor";

            }


            echo(json_encode($user_details, JSON_NUMERIC_CHECK));



        } else {
            $this->redirect(array('site/index'));
        }
    }

    public function actionUserAjaxInd()
    {
        if (!Yii::app()->session['logged_in']) {
            $this->redirect(array('site/index'));
        }
        $contact_r_id = $_REQUEST['contact_r_id'];
        $contact_r_type = $_REQUEST['contact_r_type'];
        $connection = Yii::app()->db;
        if ($contact_r_type == "doctor") {
            $sql = "select `first_name` as fname,`last_name` as lname,`email` as email,`phone` as phone,`addr1` as address, `city` as city,`state` as state, `zip` as zip FROM da_doctor WHERE status=1 and payment_type ='standard' and id=" . $contact_r_id;
        } elseif ($contact_r_type == "patient") {
            $sql = "select `user_first_name` as fname, `user_last_name` as lname,`user_email` as email,`user_phone` as phone,`user_address` as address, `user_city` as city,`user_state` as state, `user_zip` as zip FROM da_patient WHERE status=1 and id=" . $contact_r_id;
        } else {
            $sql = "";
        }
        $command = $connection->createCommand($sql);
        $user_details = $command->queryRow();

        $_data = array('user_details' => $user_details, 'contact_r_type' => $contact_r_type);
        $json_data = json_encode($_data);
        echo $json_data;
    }

    private function ThreadDetails($msg_id)
    {
        $data = array();
        $connection = Yii::app()->db;
        $userType = Yii::app()->session['logged_user_type'];
        $user_Check = $this->Type();

        if (isset($msg_id) && $msg_id != 0) {
            $sql2 = "SELECT da_message_inbox.message_id FROM da_message_inbox LEFT JOIN da_inbox ON (da_inbox.inbox_id=da_message_inbox.message_id) WHERE  da_message_inbox.user_id= '" . Yii::app()->session['logged_user_id'] . "' AND da_message_inbox.is_delete=0  AND da_message_inbox.user_type='" . $user_Check . "' AND da_inbox.thread_id='" . $msg_id . "'";
            $command = $connection->createCommand($sql2);

            $message_ids = $command->queryAll();
            $messages = [];
            foreach ($message_ids as $ar) {

                array_push($messages, $ar['message_id']);
            }

            if ($messages) {
                $data = $messages;
                $ids = join(',', $messages);
                $idcondition = "WHERE da_message_inbox.message_id IN  (" . $ids . ") ";

            } else {

                $idcondition = null;
            }
        } else {
            $idcondition = null;
        }

        if (Yii::app()->controller->action->id == 'details') {
            return $idcondition;

        } else {
            return $data;
        }
    }


    public function actionArchivemessage()
    {
        $this->layout = false;
        $inboxtblIdArr = explode("|", $_REQUEST['keyword']);

        if ($inboxtblIdArr) {
            for ($i = 0; $i < count($inboxtblIdArr); $i++) {
                $msg_id = $inboxtblIdArr[$i];
                $data = $this->ThreadDetails($msg_id);
                if ($data) {
                    /*print_r($data); exit;*/
                    foreach ($data as $id) {
                        $model = MessageInbox::model()->findByAttributes(array('message_id' => $id, 'user_id' => Yii::app()->session['logged_user_id']));
                         if($model){
                             $model->is_archive = 1;
                             $model->archived_at = time();
                             if ($model->save(false)) {
                                 $log = new Log ;
                                 $log->user_id = Yii::app()->session['logged_user_id'];
                                 $log->user_type = Yii::app()->session['logged_user_type'];
                                 $log->controller = $this->getId();
                                 $log->action = $this->getAction()->getId();
                                 $log->created_at = time();
                                 $log->request =Yii::app()->request->requestUri ;
                                 $log->param = $model->message_id;
                                 $log->save();

                             }
                             else {
                                 echo $model->errors;
                             }
                         }


                    }
                }


            }
            echo 1;
        }
    }


    public function actionDeletemessage()
    {

        $this->layout = false;
        $inboxtblIdArr = explode("|", $_REQUEST['keyword']);
        if ($inboxtblIdArr) {
            for ($i = 0; $i < count($inboxtblIdArr); $i++) {
                $msg_id = $inboxtblIdArr[$i];
                $data = $this->ThreadDetails($msg_id);
                if ($data) {
                    foreach ($data as $id) {
                        $model = MessageInbox::model()->findByAttributes(array('message_id' => $id, 'user_id' => Yii::app()->session['logged_user_id']));
                        $model->is_delete = 1;
                        if ($model->save(false)) {


                        } else {
                            echo $model->errors;
                        }

                    }
                }
            }
            echo 1;
        }
    }


    function decrypt($encrypted_string)
    {
        $dirty = array("+", "/", "=");
        $clean = array("_PLUS_", "_SLASH_", "_EQUALS_");

        $string = base64_decode(str_replace($clean, $dirty, $encrypted_string));

        $decrypted_string = $string;
        return $decrypted_string;
    }

    public function actiongetPatients()
    {
        // print_r($_REQUEST['keyword']);exit;
        $this->layout = false;
        $keyword = $_REQUEST['keyword'] . '%';
        $userId = Yii::app()->session['logged_user_id'];
        $userType = Yii::app()->session['logged_user_type'];

        if (Yii::app()->session['logged_in']) {
            $connection = Yii::app()->db;

            if ($userType == "doctor"||$userType == "parcticeAdmin") {

                $sql = 'select db.`patient_id` as pid, p.`id` as `vic_id`,p.`user_dob` as `vic_dob`,p.`user_first_name`,p.`user_last_name`,concat_ws(" ", p.user_first_name,  p.user_last_name ) as `vic_name` ,p.`user_email` as `vic_email` FROM `da_doctor_book` as db LEFT JOIN `da_patient` as p ON(p.`id` = db.`patient_id`) WHERE db.`doctor_id`= "' . $userId . '" AND (p.`user_first_name` LIKE "' . $keyword . '" OR p.`user_last_name` LIKE "' . $keyword . '")  GROUP BY p.`id`';

                $command = $connection->createCommand($sql);
                $user_details = $command->queryAll();
                $user_details_res = array();


            }

            foreach ($user_details as $rs) {
                // put in bold the written text
                $vic_dob = date("m-d-Y", strtotime($rs['vic_dob']));
                $vic_full_name = str_replace($_REQUEST['keyword'], '<b>' . $_REQUEST['keyword'] . '</b>', $rs['vic_name']);
                $data = $rs['vic_name'] . "|" . $rs['vic_id'];
                echo '<li onclick="set_item(\'' . $data . '\')">' . $vic_full_name . "," . $vic_dob . '</li>';
                // echo '<li onclick="set_item(\''.$data.'\')">'.$vic_full_name.'</li>';
            }


        } else {
            $this->redirect(array('site/index'));
        }
    }

    public function actiongetLists()
    {
        // print_r($_REQUEST['keyword']);exit;
        $this->layout = false;
        $keyword = $_REQUEST['keyword'] . '%';
        $userId = Yii::app()->session['logged_user_id'];
        $userType = Yii::app()->session['logged_user_type'];

        if (Yii::app()->session['logged_in']) {
            $connection = Yii::app()->db;

            if ($userType == "doctor"||$userType == 'parcticeAdmin') {
                $sql = 'select db.`patient_id` as pid, p.`id` as `vic_id`,p.`user_dob` as `vic_dob`,p.`user_first_name`,p.`user_last_name`,concat_ws(" ", p.user_first_name,  p.user_last_name ) as `vic_name` ,p.`user_email` as `vic_email` FROM `da_doctor_book` as db LEFT JOIN `da_patient` as p ON(p.`id` = db.`patient_id`) WHERE db.`doctor_id`= "' . $userId . '" AND (p.`user_first_name` LIKE "' . $keyword . '" OR p.`user_last_name` LIKE "' . $keyword . '")  GROUP BY p.`id`';
                $command = $connection->createCommand($sql);
                $user_details = $command->queryAll();
                $user_details_res = array();
            }
            foreach ($user_details as $rs) {
                // put in bold the written text
                $vic_dob = date("m-d-Y", strtotime($rs['vic_dob']));
                $vic_full_name = str_replace($_REQUEST['keyword'], '<b>' . $_REQUEST['keyword'] . '</b>', $rs['vic_name']);
                $data = $rs['vic_name'] . "|" . $rs['vic_id'] . "|" . $rs['vic_dob'];
                echo '<li onclick="item(\'' . $data . '\')">' . $vic_full_name . "," . $vic_dob . '</li>';
            }


        } else {
            $this->redirect(array('site/index'));
        }
    }


}
