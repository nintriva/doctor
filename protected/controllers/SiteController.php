<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex($id = "")
	{
		if($id){
			$sql='UPDATE da_doctor SET email_verified = :email_verified WHERE id ='.$id;
			$params = array(
				"email_verified" => 1
			);
			$connection = Yii::app()->db;
			$command=$connection->createCommand($sql);
			$command->execute($params);
		}
		$sql='select * FROM da_speciality WHERE status="1"';
		$connection = Yii::app()->db;
		$command = $connection->createCommand($sql);
		$user_speciality = $command->queryAll();
		$user_speciality_res = array();
		foreach ($user_speciality as $key=>$val) {
			$user_speciality_res[$val['id']] = $val['speciality'];
		}
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		
		//Helpers::pre($this->getLangLat('49, subhasnagar, dum dum cantt,kolkata, 700065'));die;
		
		//$ip = $_SERVER['REMOTE_ADDR'];
		//$ip = '61.16.146.185';
		//$api_url = 'http://api.locatorhq.com/?user=sharadindu1549&key=2720abe4a1c89ea2841f415bebbf13a7b642c2cf&ip='.$ip.'&format=json';
		/*$queryString = 'user=sharadindu1549&key=2720abe4a1c89ea2841f415bebbf13a7b642c2cf&ip='.$ip.'&format=json';
		$searchIdJSON = Helpers::requestByCurl('http://api.locatorhq.com/', '', 'GET', $queryString);
		$searchIdObj = json_decode($searchIdJSON);
		$address_arr = $this->getLangLat($searchIdObj->city.','.$searchIdObj->countryName);*/
		
		//$address_arr = $this->getLangLat('49, subhasnagar, dum dum cantt,kolkata, 700065');
		//Helpers::pre($address_arr);die;
		$this->render('index',array(
			'user_speciality' => $user_speciality_res,//'address_arr' => $address_arr,
		));
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		$this->layout = '//layouts/error_layout';
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error',array( 'error'=>$error));
		}
	}
    public function actionSessionexpired()
    {
        $this->layout = '//layouts/main';
        $this->render('sessionexpired');

    }
    public function actionTc()
    {
        $this->layout = '//layouts/error_layout';
        if(!Yii::app()->session['logged_in']){
            $this->redirect(array('site/index'));
        }
         if($_POST){
             if($_POST['checkbox']==1){

                 $connection = Yii::app()->db;
                 $sql="select * FROM tc_acceptance_tracking WHERE `user_id`=".Yii::app()->session['logged_user_id']."";
                 $command = $connection->createCommand($sql);
                 $user = $command->queryAll();
                 if($user){
                     $t = time();
                     $sql='UPDATE tc_acceptance_tracking SET accepted_at = :accepted_at WHERE `user_id` ='.Yii::app()->session['logged_user_id'];
                     $params = array(
                         "accepted_at"=> $t
                     );
                     $connection = Yii::app()->db;
                     $command=$connection->createCommand($sql);
                     $command->execute($params);

                     $this->redirect($this->createAbsoluteUrl('patient/index'));
                 }

             }

        }

        $this->render('tc_pp');

    }

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-Type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		if(isset($_SESSION['logged_user_type']))
			{
				if($_SESSION['logged_user_type']=='patient')
				{
					$this->redirect($this->createAbsoluteUrl('patient/index'));
				}else{
					$this->redirect($this->createAbsoluteUrl('doctor/index'));
				}
			}


		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
	
	public function actionDoctorNameAutocomplete()
	{
		$name = $_REQUEST['term'];
		
		$sql_doctor_name="select id,concat_ws(' ', first_name , last_name) as name FROM da_doctor WHERE status=1 and email_verified=1 and (first_name like '%".$name."%' or last_name  like '%".$name."%') ";
		$connection = Yii::app()->db;
		$command_doctor_name = $connection->createCommand($sql_doctor_name);
		$user_doctor_name = $command_doctor_name->queryAll();
		//print_r($user_doctor_address);
		$doctor_name = array();
		foreach ($user_doctor_name as $key=>$val) {
			//$doctor_name[$val['id']] = $val['name'];
			$doctor_name[] = '{"id": "'.$val['id'].'","value": "'.$val['name'].'","label": "'.$val['name'].'"}';
		}
		//echo json_encode($doctor_name);
		echo '['.implode(',',$doctor_name).']';
	}
	
	function getLangLat($address) {
        $address = urlencode($address);
        $request_url = "http://maps.googleapis.com/maps/api/geocode/xml?address=" . $address . "&sensor=true";
        $xml = simplexml_load_file($request_url);
		$arr = array();
        if ($xml->status && $xml->status == "OK") {
           //return $xml->result->geometry->location;
		   foreach($xml->result->address_component as $a => $b) {
				$arr[] =$b->long_name;
			}
        } else {
            //return (object) array('lat' => '', 'lng' => '');
        }
		return $arr;
    }
    public function action404error(){
        return true;

    }
	
	function actionAjaxLatLong() {
		if( !isset($_GET['latlng']) ) {
			$this->redirect(array('404error'));
		}
        list($lat,$long) = explode(',',htmlentities(htmlspecialchars(strip_tags($_GET['latlng']))));
		$geodata['latitude'] = $lat;
		$geodata['longitude'] = $long;
		//$local_add = str_replace("-",",",$_GET['location_loc_add']);
		$local_add = explode("-",$_GET['location_loc_add']);
		//echo 'Latitude: '.$lat.' Longitude: '.$long;
		Yii::app()->session['visited_user_lat'] = $lat;
		Yii::app()->session['visited_user_long'] = $long;
		//echo 'Latitude: '.Yii::app()->session['visited_user_lat'].' Longitude: '.Yii::app()->session['visited_user_long'];
		$floor_lat = floor($lat);
		$floor_lat_up = $floor_lat+1;
		$floor_lat_down = $floor_lat-1;
/*$connection = Yii::app()->db;
$sql_address="select * FROM da_doctor_address WHERE status='1' and latitude >=".$floor_lat_down." and latitude<=".$floor_lat_up." GROUP BY zip LIMIT 5";
$command_address = $connection->createCommand($sql_address);
$user_address = $command_address->queryAll();*/
		//$user_address_res = array();
		
		//http://maps.googleapis.com/maps/api/geocode/json?latlng=44.4647452,7.3553838&sensor=true
		//$address_lat = Yii::app()->session['visited_user_lat'];
		//$address_long = Yii::app()->session['visited_user_long'];
		$address_lat = $geodata['latitude'];
		$address_long = $geodata['longitude'];
		//$request_url = "http://maps.googleapis.com/maps/api/geocode/json?latlng=" . $address_lat.','.$address_long . "&sensor=true";
//$remoteServer = "http://maps.googleapis.com/maps/api/geocode/json";
//$queryString = "latlng=" . $address_lat.','.$address_long . "&sensor=true";
//$curl_json=json_decode(Helpers::requestByCurl($remoteServer,'','GET',$queryString,''));
		$user_address_res = '';
		//Helpers::pre($curl_json);
		//$curl_json_address = $curl_json->results[0]->formatted_address;
//if( count($curl_json->results) > 0 ) {
//$curl_json_address = $curl_json->results[0]->address_components[5]->short_name;
			//Helpers::pre($curl_json_address);
	
		
	/*if($curl_json_address)
		$user_address_res = '<option value="'.$local_add[0].', '.$curl_json_address.'">'.$local_add[0].', '.$curl_json_address.'</option>';
	else
		$user_address_res = '<option value="'.$_GET['location_loc_add'].'">'.$_GET['location_loc_add'].'</option>';*/
	
			//$user_address_res = '<option value="'.$local_add.'">'.$local_add.'</option>';
//foreach ($user_address as $key=>$val) {
					//$user_address_res[$val['id']] = $val['address'];
					//$user_address_res .= '<option value="'.$val['address'].'">'.$val['address'].'</option>';
					
		/*if($val['state'] != '') $gen_add = $val['city'].', '.$val['state'];
		else $gen_add = $val['city'];
		$user_address_res .= '<option value="'.$gen_add.'">'.$gen_add.'</option>';*/
					
//$gen_add = $val['zip'];
//$user_address_res .= '<option value="'.$gen_add.'">'.$gen_add.'</option>';
//}
//} else {
				$gen_add = '94103';
				$user_address_res .= '<option value="'.$gen_add.'">'.$gen_add.'</option>';
//}	
		echo $user_address_res;
    }
	
	function actionAjaxTimeZone() {
		if( !isset($_GET['latlng']) ) {
			$this->redirect(array('404error'));
		}
        list($lat,$long) = explode(',',htmlentities(htmlspecialchars(strip_tags($_REQUEST['latlng']))));
		$remoteServer = "https://maps.googleapis.com/maps/api/timezone/json";
		$queryString = "timestamp=0&sensor=true&location=".$lat.",".$long."";
		$curl_json=json_decode(Helpers::requestByCurl($remoteServer,'','GET',$queryString,''));
		//Helpers::pre($curl_json);
		//echo $curl_json->timeZoneId;
		if( isset($curl_json->timeZoneId) ) {
			Yii::app()->session['timeZoneId'] = $curl_json->timeZoneId;
		} else {
			Yii::app()->session['timeZoneId'] = date_default_timezone_get();
		}
	}

}