<?php
class PaAdminDoctorsController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public $layout='//layouts/column2';
	
	public function actions() {
		return array(
				// captcha action renders the CAPTCHA image displayed on the contact page
				'captcha'=>array(
						'class'=>'CCaptchaAction',
						'backColor'=>0xFFFFFF,
				),
				// page action renders "static" pages stored under 'protected/views/site/pages'
				// They can be accessed via: index.php?r=site/page&view=FileName
				'page'=>array(
						'class'=>'CViewAction',
				),

		);
	}
	
	public function filters() {
		return array(
				'accessControl', // perform access control for CRUD operations
				'postOnly + delete', // we only allow deletion via POST request
		);
	}
	
	/* ------------- for parctice Admin -------- IB*/
	public function actionLoginDpa() {
		$model=new LoginForm;

		if(isset($_SESSION['logged_user_type'])) {
			if($_SESSION['logged_user_type']=='parcticeAdmin') {
				$this->redirect($this->createAbsoluteUrl('paAdminDoctors/index'));
			}else{
				$this->redirect($this->createAbsoluteUrl('doctor/index'));
			}
		}
		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
		if(isset($_POST['LoginForm'])) {
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login_dpa',array('model'=>$model));
	}
	/* ------------- X -------- IB*/

	/* ------------- for parctice Admin Sign function -------- IB*/
	
	public function actionsignIn() {
		if( !isset($_REQUEST['email']) ) {
			$this->redirect(array('404error'));
		}

		$connection = Yii::app()->db;
		$validationFlag = '';
		$email = Yii::app()->getRequest()->getPost('email');
		$sql="SELECT * FROM `da_pa_admin` WHERE `username` = '".$email."' OR `email` = '".$email."'";
		$command = $connection->createCommand($sql);
		$result = $command->queryAll();

		$validationFlag = "";
		if($result) {
			$validationFlag = 'parcticeAdmin';
		}
	
		$account_type = $validationFlag;
		if($account_type == 'parcticeAdmin') {
			$model = new PaAdmin();
		}
	
		if(Yii::app()->getRequest()->isAjaxRequest){
			$usercriteria = "";
			if($account_type == 'parcticeAdmin'){
				$model->email = Yii::app()->getRequest()->getPost('email');
				$model->password = Yii::app()->getRequest()->getPost('password');
                $criteria=New CDbCriteria;
                $criteria->addCondition('password = '."'md5($model->password)'");
                $criteria->addCondition('email_verified = '. 1,'AND');
                $criteria->addCondition('username = '."'$model->email'");
                $criteria->addCondition('email = '."'$model->email'",'OR');
                $usercriteria =PaAdmin::model()->find($criteria);
			}

			if(isset($usercriteria) && $usercriteria !="" ){
				if($account_type == 'parcticeAdmin'){
					if(isset(Yii::app()->session['logged_in'])){
						Yii::app()->session->destroy();
					}
						
					$sqlDoc="select * FROM da_pa_admin_doctors WHERE status='1' AND `pa_admin_id`='".$usercriteria->id."'";
					$commandDoc = $connection->createCommand($sqlDoc);
					$usercriteriaDoctor = $commandDoc->queryAll();
					$countDoc = count($usercriteriaDoctor);
                    $assDocId = "";
					if($countDoc !=0 ) {

						$firstDoctorId = "";
						$i=0;
						foreach($usercriteriaDoctor as $key=>$data) {
							if($i==0) {
								$assDocId = $data['doctor_id'];
								$firstDoctorId = $data['doctor_id'];
								$i++;
							} else {
								$assDocId = $assDocId.",".$data['doctor_id'];
							}
						}
					}
					Yii::app()->session['logged_in'] = true;
					Yii::app()->session['assigned_doctor'] = $assDocId;
					Yii::app()->session['logged_user_type'] = 'parcticeAdmin';
					Yii::app()->session['logged_user_email'] = $usercriteria->email;
					Yii::app()->session['first_name'] = $usercriteria->first_name;
					Yii::app()->session['middle_name'] = $usercriteria->middle_name;
					Yii::app()->session['last_name'] = $usercriteria->last_name;
					Yii::app()->session['logged_user_id_admin'] = $usercriteria->id;
					Yii::app()->session['logged_user_id'] = $usercriteria->id;
                    if($firstDoctorId != ''){
                        //$docter = Doctor::model()->find("id=\"$firstDoctorId\" and status=1 ");
                        $docter = Doctor::model()->find('id=:id', array(':id'=>$firstDoctorId));
                       // Yii::app()->session['logged_user_email'] = $docter->email;
                        Yii::app()->session['logged_user_id'] = $docter->id;
                        Yii::app()->session['logged_user_email_address'] = $docter->email;

                    }
					
					/*if($firstDoctorId !="" ) {
						$usercriteriaDoctor = Doctor::model()->find("id=\"$firstDoctorId\" and status=1 ");
						Yii::app()->session['logged_user_email'] = $usercriteriaDoctor->username;
						Yii::app()->session['logged_user_id'] = $usercriteriaDoctor->id;
						Yii::app()->session['logged_user_email_address'] = $usercriteriaDoctor->email;
	
						$sqlCount="select * FROM da_inbox WHERE status=1 AND `sent_to_user`='".Yii::app()->session['logged_user_id']."' AND `read_flag`=0";
						$commandCount = $connection->createCommand($sqlCount);
						$inbox_arr_count = $commandCount->queryAll();
						$count = count($inbox_arr_count);
						Yii::app()->session['inbox_count'] = $count;
					}*/
	
					//if($usercriteria->google_account)
					//Yii::app()->session['google_account'] = $usercriteria->google_account;
				}
				echo true;
			}else{
				echo false;
			}
		}
	}

	/* -----------------------------X-----------------------------*/
	
	public function actionDocChange(){
        // print_r(Yii::app()->getRequest()->getPost('doc')); exit;
        //print_r(Yii::app()->getRequest()->getPost('doc')); exit;
		if(isset(Yii::app()->session['logged_user_id'])){
			Yii::app()->session['logged_user_id']=Yii::app()->getRequest()->getPost('doc');
		}
        $id = Yii::app()->getRequest()->getPost('doc');
		Yii::app()->session['logged_in'] = true;
		Yii::app()->session['logged_user_id'] = Yii::app()->getRequest()->getPost('doc');
        $docter = Doctor::model()->find('id=:id', array(':id'=>$id));
        // Yii::app()->session['logged_user_email'] = $docter->email;
        Yii::app()->session['logged_user_id'] = $docter->id;
        Yii::app()->session['logged_user_email_address'] = $docter->email;

		echo 1;
	}
	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		/*$dataProvider=new CActiveDataProvider('Doctor');
			$this->render('index',array(
					'dataProvider'=>$dataProvider,
			));*/

		if(Yii::app()->session['logged_in']){


			if(isset(Yii::app()->session['assigned_doctor'])) {
				$doctorAsigName = explode(",",Yii::app()->session['assigned_doctor']);
				if(count($doctorAsigName) > 0) {	
					$model = new Doctor();
					/*if(Yii::app()->session['logged_user_id'] == 0) {
						Yii::app()->session['logged_user_id'] = $doctorAsigName[0];
					}*/
					$model->id = Yii::app()->session['logged_user_id'];
					//$model->id = 2;
					$usercriteria =Doctor::model()->find("id=\"$model->id\"");
                    /*print_r($usercriteria);
                    exit;*/

				} else {
					$this->redirect(array('site/index'));
				}	
			}

		}else{
			$this->redirect(array('site/index'));
//            print_r(13);
            exit;
		}
	
		$connection = Yii::app()->db;
		$sql='select * FROM da_speciality WHERE status="1"';
		$command = $connection->createCommand($sql);
		$user_speciality = $command->queryAll();
		$user_speciality_res = array();
		foreach ($user_speciality as $key=>$val) {
			$user_speciality_res[$val['id']] = $val['speciality'];
		}


		$sql_multy_speciality='select * FROM da_doctor_speciality WHERE status="1" and doctor_id="'.$model->id.'"';
		$command_multy_speciality = $connection->createCommand($sql_multy_speciality);
		$user_multy_speciality = $command_multy_speciality->queryAll();
		$user_selected_speciality = array();
		foreach ($user_multy_speciality as $key=>$val) {
			$user_selected_speciality[] = $val['speciality_id'];
		}

		$default_data_address = DoctorAddress::model()->findByAttributes(array('doctor_id'=>$model->id,'status'=>1,'default_status'=>1));

		$strt_tm = date('Y-m-d');
		$end_tm = date('Y-m-d',mktime(0,0,0,date('m'),date('d')+30,date('Y')));

		$dashboardAppointment = $this->actionDashboardAppointment($model->id,$strt_tm,$end_tm);

		$sql_procedure='select * FROM da_procedure WHERE status="1"';
		$command_procedure = $connection->createCommand($sql_procedure);
		$user_procedure = $command_procedure->queryAll();
		$user_procedure_res = array();
		foreach ($user_procedure as $key=>$val) {
			$user_procedure_res[$val['id']] = $val['procedure'];
		}
	
		$sql_reason_for_visit='select * FROM da_reason_for_visit WHERE status="1"';
		$command_reason_for_visit = $connection->createCommand($sql_reason_for_visit);
		$user_reason_for_visit = $command_reason_for_visit->queryAll();
		$user_reason_for_visit_res = array();
		foreach ($user_reason_for_visit as $key=>$val) {
			$user_reason_for_visit_res[$val['id']] = $val['reason_for_visit'];
		}
		//$dashboardAppointment_arr = explode('***%%%***',$dashboardAppointment);
		//$dashboardAppointment_arr = array();
		//Helpers::pre($dashboardAppointment[1]);die;
	
		$doctor_id = $model->id;

		$condition = 'status = 1 and doctor_id =' . $doctor_id;
		$criteria = new CDbCriteria(array(
				'condition' => $condition
		));
		//$usercriteria_todo_list =TodoList::model()->findAll($criteria);

		$sql_todo_list='select * FROM da_todo_list WHERE status="1" and doctor_id =' . $doctor_id;
		$command_todo_list = $connection->createCommand($sql_todo_list);
		$usercriteria_todo_list = $command_todo_list->queryAll();
		//Helpers::pre($usercriteria_todo_list);die;

		$doctor_review_condition = 't.status = 1 and doctor_id =' . $doctor_id;
		$doctor_review_criteria = new CDbCriteria(array(
				'condition' => $doctor_review_condition
		));
		$doctor_review=DoctorReview::model()->with('patient_details')->findall($doctor_review_criteria);
		//Helpers::pre($doctor_review);die;

		$this->render('index',array(
				'dataProvider'=>$usercriteria,'user_speciality' => $user_speciality_res,'default_data_address' => $default_data_address,'dashboardAppointment' => $dashboardAppointment[1],'dashboardAppointmentCnt' => $dashboardAppointment[0],'user_app_request' => $dashboardAppointment[2],/*'user_procedure' => $user_procedure_res,*/'todo_list' => $usercriteria_todo_list,'doctor_review' => $doctor_review,'user_selected_speciality' => $user_selected_speciality,'user_reason_for_visit' => $user_reason_for_visit_res,
		));
	}
	
	public function actionDashboardAppointment($id,$strt_tm,$end_tm)
	{
		$strt_tm = $strt_tm;
		$end_tm = $end_tm;
		/*$strt_tm = $_REQUEST['start'];
			$end_tm = $_REQUEST['end'];*/
		if(!Yii::app()->session['logged_in'])
			$this->redirect(array('site/index'));
			
		$start_time = strtotime($strt_tm);
		$to_time = strtotime($end_tm);

		$sql_doctor_address='select db.*,dp.user_first_name as dp_name,da.address as da_address,dpro.procedure as dpro_procedure,concat_ws(" ", dp.user_first_name,  dp.user_last_name ) as dp_patient_name FROM da_doctor_book as db left join da_patient as dp on db.patient_id = dp.id left join da_doctor_address as da on db.address_id = da.id left join da_procedure as dpro on db.procedure_id = dpro.id WHERE db.status=1 and ( book_time <'.$to_time.' and book_time >='.$start_time.') and db.doctor_id='.$id.' order by db.book_time';
		$connection = Yii::app()->db;
		$command_doctor_address = $connection->createCommand($sql_doctor_address);
		$user_patient_book = $command_doctor_address->queryAll();
		$user_calender_res = array();
		$user_app_request = array();
		//$user_calender_cnt = count($user_patient_book);
		$user_calender_cnt = 0;
		$user_tot_arr = array();
		//$user_tot_arr[] = $user_calender_cnt;
		foreach ($user_patient_book as $key=>$val) {
			$st_time = date('Y-m-d h:i',$val['book_time']);
			$end_time = date('Y-m-d h:i',($val['book_time']+($val['book_duration']*60)));
			$cur_date = date('Y-m-d',$val['book_time']);
			if($val['confirm']!=1) ++$user_calender_cnt;
			$user_calender_res[$cur_date][] =array(
					'id' => $val['id'],
					'patient_name' => ($val['dp_patient_name'])?$val['dp_patient_name']:$val['patient_name'],
					'confirm' => $val['confirm'],
					'title' => $val['dp_name'],
					'start' => "$st_time",
					'end' => "$end_time",
					'book_duration' => $val['book_duration'],
					'procedure_id' => $val['procedure_id'],
					'reason_for_visit_id' => $val['reason_for_visit_id'],
					'cur_date' => date('Y-m-d',$val['book_time']),
					'allDay' => false,
					'description' =>"<p><span>Procedure</span>: ".$val['dpro_procedure']."</p><p><span>Address</span>: ".$val['da_address']."</p>",
					"borderColor" => "#1587bd",
					"textColor" => "#000000",
					"color" => "#9fc6e7",
			);
			if($val['confirm']!=1)
				$user_app_request[$cur_date][] = $val['confirm'];
		}
		$user_tot_arr[] = $user_calender_cnt;
		$user_tot_arr[] = $user_calender_res;
		$user_tot_arr[] = $user_app_request;
	
		//return $events_code = json_encode($user_calender_res);
		return $events_code = $user_tot_arr;
	
	}
	
	public function actionEditProfile($id = ""){
		
		if(!Yii::app()->session['logged_in'])
			$this->redirect(array('site/index'));
		if($id!=Yii::app()->session['logged_user_id_admin'])
			$this->redirect(array('PaAdminDoctors/editProfile/'.Yii::app()->session['logged_user_id_admin']));
		
		$model = new PaAdmin();
		$model->id = $id;
		$model =PaAdmin::model()->find("id=\"$model->id\"");
		$connection = Yii::app()->db;
		$error = array();
		if(Yii::app()->getRequest()->isPostRequest){
			
			$model = PaAdmin::model()->findByPk($id);
			$model->scenario = 'edit_profile';
			//$prv_img_path = $model->image;
			if( isset($_REQUEST['resetPwd']) && ( $_REQUEST['resetPwd'] == "Yes" ) ) {
				Yii::app()->session['resetPasword '] = "Yes";
				$model->attributes=Yii::app()->getRequest()->getPost('PaAdmin');
				$old_password = trim($_POST['old_password']);
				$new_password = $_POST['new_password'];
				$confirm_password = $_POST['confirm_password'];
				if( Yii::app()->session['logged_user_id_admin'] == $_POST['hidden_id'] ) {
					$connection = Yii::app()->db;
					$sql="select * FROM da_pa_admin WHERE status=1 and id=".Yii::app()->session['logged_user_id_admin']." and password='".md5($old_password)."'";
					$command = $connection->createCommand($sql);
					$user_arr = $command->queryAll();
					if(empty($user_arr)) $error['old_password'] = 'Password not matched.';
					if($new_password == '') $error['new_password'] = 'Password cannot be blank.';
					if($confirm_password != $new_password) $error['confirm_password'] = 'Password not matched with New Password.';
					if(empty($error)){
						$sql='UPDATE da_pa_admin SET password = :password WHERE status=1 and id ='.Yii::app()->session['logged_user_id_admin'];
						$params = array(
								"password" => md5($confirm_password)
						);
						$connection = Yii::app()->db;
						$command=$connection->createCommand($sql);
						$command->execute($params);
						Yii::app()->user->setFlash('resetPassword','Your Password updated sucessfully.');
						$this->refresh();
					}
				}
			
			} else {	
				Yii::app()->session['resetPasword '] = "";
				$error = array();
				$model->attributes=Yii::app()->getRequest()->getPost('PaAdmin');
				if(Yii::app()->session['yiiadmin__id']){
					if(Yii::app()->getRequest()->getPost('status')=='off')
						$model->status = 0;
					else
						$model->status = 1;
					if(Yii::app()->getRequest()->getPost('lock_profile')==1)
						$model->lock_profile = Yii::app()->getRequest()->getPost('lock_profile');
					else
						$model->lock_profile = 0;
				}
				
				$model->gender=Yii::app()->getRequest()->getPost('gender');
				$model->birth_date=$_REQUEST['yy']."-".$_REQUEST['mm']."-".$_REQUEST['dd'];
				if($model->birth_date == "" || $model->birth_date == '0000-00-00'){
					$model->birth_date = "";
					$birth_date['mm'] = "";
					$birth_date['dd'] = "";
					$birth_date['yy'] = "";
				}else{
					$birth_date_arr =explode("-",$model->birth_date);
					$birth_date['mm'] = $birth_date_arr[1];
					$birth_date['dd'] = $birth_date_arr[2];
					$birth_date['yy'] = $birth_date_arr[0];
				}
				$model->title=Yii::app()->getRequest()->getPost('title');
				$model->date_modified=date('Y-m-d H:i:s');
				$model->status = 1;
				$uniqid = uniqid();  // generate random number between 0-9999
				
				/*$uploadedFile=CUploadedFile::getInstance($model,'image');
				$fileName = "{$uniqid}-{$uploadedFile}";  // random number + file name
				if(!empty($uploadedFile))
					$model->image = $fileName;
				*/
				if($model->save()){
					Yii::app()->user->setFlash('editProfile','Your Profile Updated Sucessfully.');
					$this->refresh();
				}
			}
			
			
			
		} 
		
			$model->scenario = 'edit_profile';
			
			$birthDate = $model->birth_date;
			$title = $model->title;
			if( $birthDate == "" || $birthDate == '0000-00-00'){
				$model->birth_date = "";
				$birth_date['mm'] = "";
				$birth_date['dd'] = "";
				$birth_date['yy'] = "";
			}else{
				$model->birth_date=$birthDate;
				$birth_date_arr =explode("-",$model->birth_date);
				$birth_date['mm'] = $birth_date_arr[1];
				$birth_date['dd'] = $birth_date_arr[2];
				$birth_date['yy'] = $birth_date_arr[0];
			}
			$model->title=$title;
			
			$start_year = date('Y')-70;
			$end_year = date('Y');
			$yearOptions = array();
			$count_year = $start_year;
			for($i=0;$i<=($end_year-$start_year);$i++){
				$yearOptions[$count_year]=$count_year;
				$count_year++;
			}
			
			$condition_country = 'status = "1"';
			$criteria_country = new CDbCriteria(array(
					'condition' => $condition_country
			));
			$data_country =CountryMaster::model()->findAll($criteria_country);

		
		$this->render('edit_profile',array(
				'model' => $model,'birth_date' => $birth_date,'yearOptions' => $yearOptions,'data_country' => $data_country,'error' => $error,
		));
		
	}
	
	function actionResetPwd() {
		
		if(!Yii::app()->session['logged_in'])
			$this->redirect(array('site/index'));
		if(Yii::app()->session['logged_user_id_admin'] == '')
			$this->redirect(array('PaAdminDoctors/editProfile/'.Yii::app()->session['logged_user_id_admin']));
		
		$model = new PaAdmin();
		$model->id = Yii::app()->session['logged_user_id_admin'];
		$model =PaAdmin::model()->find("id=\"$model->id\"");
		$error = array();
		if($_POST){
				
			$old_password = trim($_POST['old_password']);
			$new_password = $_POST['new_password'];
			$confirm_password = $_POST['confirm_password'];
			if( Yii::app()->session['logged_user_id_admin'] == $_POST['hidden_id'] ) {
				$connection = Yii::app()->db;
				$sql="select * FROM da_pa_admin WHERE status=1 and id=".Yii::app()->session['logged_user_id_admin']." and password='".md5($old_password)."'";
				$command = $connection->createCommand($sql);
				$user_arr = $command->queryAll();
				if(empty($user_arr)) $error['old_password'] = 'Password not matched.';
				if($new_password == '') $error['new_password'] = 'Password cannot be blank.';
				if($confirm_password != $new_password) $error['confirm_password'] = 'Password not matched with New Password.';
					
				if(empty($error)){
					$sql='UPDATE da_pa_admin SET password = :password WHERE status=1 and id ='.Yii::app()->session['logged_user_id_admin'];
					$params = array(
							"password" => md5($confirm_password)
					);
					$connection = Yii::app()->db;
					$command=$connection->createCommand($sql);
					$command->execute($params);
					Yii::app()->user->setFlash('resetPassword','Your Password updated sucessfully.');
					$this->refresh();
				}
			}	
		}
		$model->scenario = 'edit_profile';
		$birthDate = $model->birth_date;
		$title = $model->title;
		if( $birthDate == "" || $birthDate == '0000-00-00'){
			$model->birth_date = "";
			$birth_date['mm'] = "";
			$birth_date['dd'] = "";
			$birth_date['yy'] = "";
		}else{
			$model->birth_date=$birthDate;
			$birth_date_arr =explode("-",$model->birth_date);
			$birth_date['mm'] = $birth_date_arr[1];
			$birth_date['dd'] = $birth_date_arr[2];
			$birth_date['yy'] = $birth_date_arr[0];
		}
		$model->title=$title;
			
		$start_year = date('Y')-70;
		$end_year = date('Y');
		$yearOptions = array();
		$count_year = $start_year;
		for($i=0;$i<=($end_year-$start_year);$i++){
			$yearOptions[$count_year]=$count_year;
			$count_year++;
		}
			
		$condition_country = 'status = "1"';
		$criteria_country = new CDbCriteria(array(
				'condition' => $condition_country
		));
		$data_country =CountryMaster::model()->findAll($criteria_country);
		$resetPasword = "Yes";
		
		$this->render('edit_profile',array(
				'model' => $model,'birth_date' => $birth_date,'yearOptions' => $yearOptions,'data_country' => $data_country,'resetPasword'=>$resetPasword,
		));
	}
	
	public function actionForgetpassword()
	{
		$user_email = '';
		$error = '';
		$subject = 'Forget Password';
		if( isset( $_REQUEST['user-email'] ) ) {
			$user_email = $_REQUEST['user-email'];
			$validator = new CEmailValidator;
			if($validator->validateValue($user_email)){
				$usercriteria_doctor =PaAdmin::model()->find("username=\"$user_email\"");
				$password  = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 8);
				//$password = '123456';
				if(!empty($usercriteria_doctor)){
					
					$sql='UPDATE da_pa_admin SET password = :password WHERE id ='.$usercriteria_doctor->id;
					$params = array(
						"password" => md5($password)
					);
					$connection = Yii::app()->db;
					$command=$connection->createCommand($sql);
					$command->execute($params);
					
					$to_email = $usercriteria_doctor->email;
					$to_name = $usercriteria_doctor->title.' '.$usercriteria_doctor->first_name.' '.$usercriteria_doctor->last_name;
					$reg_id = $usercriteria_doctor->id;
					$username = $usercriteria_doctor->username;
					//print $password;
					$this->forgetMail($to_email, $to_name, $password, $subject, $reg_id, $username);
					Yii::app()->user->setFlash('forgetPassword','Your mail send sucessfully. Please check your mail to know your password.');
				}else{
					$error = 'email not valid.';
				}
			}else{
				$error = 'email not valid.';
			}
		}
		$this->render('forget_password',array(
			'user_email'=>$user_email,'error'=>$error,
		));
	}
	
	function forgetMail($to_email, $to_name, $password, $subject, $reg_id, $username) {
			$to_email = $to_email;
			$to_name = $to_name;	
			//print "-".$password;
			//$password  = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 8);
			//$password = '123456';
			//$reg_id = '123456';
			
			$temp_id = 10;
			$email_template = EmailTemplate::model()->find("id=\"$temp_id\"");
			if(!empty($email_template) && $email_template->tempalte_body != ''){
				$message_body = $email_template->tempalte_body;
				//$message_body = $this->getTemplateDataParsing($parse_with,$parse_data,$message_body);
				
				$parse_data_arr = array();
				
				$message_body = $this->getTemplateDataParsing($parse_data_arr,$message_body);
			}else{
				$message_body = '';
			}
				
			$to = array($to_email,$to_name);
			$from = array('info.doctor@gmail.com','eDoctorBook');
			$subject = 'eDoctorBook - '.$subject.'.';
			
				$message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
					<html xmlns="http://www.w3.org/1999/xhtml">
					<head>
					<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
					<title>Doctor Email Template</title>
					</head>
					<body style="padding:0px; margin:0px; font-size:12px; color:#545454; line-height:18px; font-family:Arial, Helvetica, sans-serif;">
					<div style="width:450px;  margin:5px auto; border:1px solid #c7c7c7; "> 
					<div style="text-align:left; margin-bottom:15px; -moz-box-shadow:0px 1px 1px 1px #E4E4E4; -webkit-box-shadow:0px 1px 1px 1px #E4E4E4;box-shadow:0px 1px 1px 1px #E4E4E4; background:#54cbc8; font-weight:bold;">
					<a href="'.$this->createAbsoluteUrl('site/index/'.$reg_id).'"><img src="http://'.$_SERVER['HTTP_HOST'].Yii::app()->getHomeUrl().'/assets/images/small_logo.png" style="padding:3px;" alt="Doctor Appointment Book" /></a>
					&nbsp;
					</div>					
					<div style="min-height:200px; background:#fff; color:#bbb; margin:10px; padding-top:15px;">
					<h2 style="color:#000">
					'.$message_body.'
					</h2>
					<br>
					<table style="width:96%; margin-left:8px; margin-right:8px; border:1px solid #9ddddb;" cellpadding="0" cellspacing="0">
					<tr>
						<th style="width:100; background:#6ad2cf; line-height:22px; text-align:center; text-transform:uppercase; padding-left:5px; color:#fff; 
						border-right:1px solid #5abdba">USER ID</th>
						<th style="width:100; background:#6ad2cf; line-height:22px; text-align:center; text-transform:uppercase; padding-left:5px; color:#fff;  border-right:1px solid #5abdba">PASSWORD</th>
					</tr>
					<tr>
						<td style="width:100; line-height:22px; padding-top:8px; padding-bottom:8px; text-align:center; padding-left:5px; 
						color:#636363; border-right:1px solid #5abdba; font-size:13px;">'.$username.'</td>
						<td style="width:100; line-height:22px;padding-top:8px; padding-bottom:8px; text-align:center; padding-left:5px;
						 color:#636363;  border-right:1px solid #5abdba; font-size:13px;">'.$password.'</td>
					</tr>
					</table>
					</div>
					<p style=" margin-top:10px; padding:8px 5px; color:#000; font-size:13px; margin-bottom:0px; font-weight:bold; line-height:18px; ">eDoctorBook Team</p>
					<a style="padding-left:5px; padding-right:5px; color:#000; font-size:13px; font-weight:bold; margin:0px; line-height:0px; text-decoration:none" href="">http://www.eDoctorBook.com</a>
					<div style="background:#54cbc8; padding-top:20px; margin-top:15px; padding-bottom:10px; text-align:center; color:#e2e2e2; font-size:14px;"></div>
					</div>

					</body>
					</html>';
			
			Helpers::mailsend($to,$from,$subject,$message);
	}
	
	function getTemplateDataParsing($parse_data_arr,$message) {
         foreach($parse_data_arr as $parse_data_key=>$parse_data_val){
		 	$message = str_replace($parse_data_key, $parse_data_val, $message);
		 }
		 return $message;
    }
	
}	