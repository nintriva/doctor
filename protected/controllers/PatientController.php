<?php

class PatientController extends Controller
{
	/*public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view', 'create'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}*/
	
	public function actionIndex()
	{
		/*$dataProvider=new CActiveDataProvider('Doctor');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));*/
		if(Yii::app()->session['logged_in']){
			$model = new Patient();
			//print_r($_REQUEST);
			$model->id = Yii::app()->session['logged_user_id'];
			//$model->password = Yii::app()->getRequest()->getPost('password');
			$usercriteria =Patient::model()->find("id=\"$model->id\"");
		}else{
			$this->redirect(array('site/index'));
		}
		
		$name= Yii::app()->request->getParam('doctor_name');
		$start_date = Yii::app()->request->getParam('start_date');
		$end_date = Yii::app()->request->getParam('end_date');
		$status = Yii::app()->request->getParam('status');
		if($status != ""){
			if($status == "Accepted")	$status_confirm=1;
			if($status == "Pending")	$status_confirm=0;
			if($status == "Cancel")	$status_confirm=2;
			if($status == "Complete")	$status_confirm=3;
		}
		
		$sql_count='select db.*,dp.user_first_name as dp_name,da.address as da_address,da.city as da_city,da.zip as da_zip, dpro.procedure as dpro_procedure,d.title as d_title,d.id as d_id,d.gender as d_gender,d.image as d_image,concat_ws(" ", d.first_name,  d.last_name ) as d_doctor_name FROM da_doctor_book as db left join da_patient as dp on db.patient_id = dp.id  left join da_doctor as d on d.id = db.doctor_id left join da_doctor_address as da on db.address_id = da.id left join da_procedure as dpro on db.procedure_id = dpro.id WHERE db.status=1 and db.patient_id='.$model->id.' ';
		$sql_doctor_address='select db.*,dp.user_first_name as dp_name,da.address as da_address,da.city as da_city,da.zip as da_zip,dpro.procedure as dpro_procedure,d.title as d_title,d.id as d_id,d.gender as d_gender,d.image as d_image,concat_ws(" ", d.first_name,  d.last_name ) as d_doctor_name FROM da_doctor_book as db left join da_patient as dp on db.patient_id = dp.id  left join da_doctor as d on d.id = db.doctor_id left join da_doctor_address as da on db.address_id = da.id left join da_procedure as dpro on db.procedure_id = dpro.id WHERE db.status=1 and db.patient_id='.$model->id.' ';
		
		$input_name = preg_replace("/[^a-zA-Z]+/", "", $name);
		if($name != ""){
			$sql_count .= " and concat_ws( d.first_name, ' ', d.last_name )   like '%".$input_name."%' ";
			$sql_doctor_address .= " and concat_ws( d.first_name, ' ', d.last_name )   like '%".$input_name."%' ";
		}
		if($start_date != ""){
			$sql_count .= " and db.book_time >= '".strtotime($start_date)."' ";
			$sql_doctor_address .= " and db.book_time >= '".strtotime($start_date)."' ";
		}
		if($end_date != ""){
			$sql_count .= " and db.book_time <= '".strtotime($end_date)."' ";
			$sql_doctor_address .= " and db.book_time <= '".(strtotime($end_date)+(24*60*60))."' ";
		}
		if($status != "" && $status != "All"){
			$sql_count .= " and db.confirm = ".$status_confirm;
			$sql_doctor_address .= " and db.confirm = ".$status_confirm;
		}
		
		$sql_count .= " order by db.book_time desc ";
		$sql_doctor_address .= " order by db.book_time desc ";
		
		$limit = 10;
		$end_limit = 10;
		if(isset($_REQUEST['page']) && $_REQUEST['page']>1){
			$star_limit = ($limit*($_REQUEST['page']-1));
		}else{
			$star_limit = 0;
		}
		$sql_doctor_address .= ' limit '.$star_limit.','.$end_limit;
		$sql_count .= ' ';
		//echo $sql_doctor_address;
		
		$connection = Yii::app()->db;
		$command_doctor_address = $connection->createCommand($sql_doctor_address);
		$user_patient_book = $command_doctor_address->queryAll();
		
		$command_count = $connection->createCommand($sql_count);
		$doctor_list_count = $command_count->queryAll();
		$count=count($doctor_list_count);
		$pages=new CPagination($count);
		$criteria = new CDbCriteria(array(
			//'condition' => $condition,
        	//'order' => 'category_id DESC'
		));
	
		// results per page
		$pages->pageSize=$limit;
		$pages->applyLimit($criteria);
		
		//Helpers::pre($user_patient_book);die;
		$sql_speciality='select * FROM da_speciality WHERE status="1"';
		$command_speciality = $connection->createCommand($sql_speciality);
		$user_speciality = $command_speciality->queryAll();
		$user_speciality_res = array();
		foreach ($user_speciality as $key=>$val) {
			$user_speciality_res[$val['id']] = $val['speciality'];
		}
		$sql_procedure='select * FROM da_procedure WHERE status="1"';
		$command_procedure = $connection->createCommand($sql_procedure);
		$user_procedure = $command_procedure->queryAll();
		$user_procedure_res = array();
		foreach ($user_procedure as $key=>$val) {
			$user_procedure_res[$val['id']] = $val['procedure'];
		}
		
		$sql_reason_for_visit='select * FROM da_reason_for_visit WHERE status="1"';
		$command_reason_for_visit = $connection->createCommand($sql_reason_for_visit);
		$user_reason_for_visit = $command_reason_for_visit->queryAll();
		$user_reason_for_visit_res = array();
		foreach ($user_reason_for_visit as $key=>$val) {
			$user_reason_for_visit_res[$val['id']] = $val['reason_for_visit'];
		}
		
		$this->render('index',array(
			'dataProvider'=>$usercriteria,'user_patient_book' => $user_patient_book,'user_procedure' => $user_procedure_res,'user_speciality' => $user_speciality_res,'pages'=>$pages,'user_reason_for_visit' => $user_reason_for_visit_res,
		));
	}
	
	public function actionEditProfile($id = "")
	{
		if(!Yii::app()->session['logged_in'])
			$this->redirect(array('site/index'));
		$model = new stdClass();
		$model->id = Yii::app()->session['logged_user_id'];
		
		$model = Patient::model()->findByPk($model->id);
		$model->scenario = 'edit_profile';
		if($model->user_dob == "" || $model->user_dob == '0000-00-00'){
			$birth_date['mm'] = "";
			$birth_date['dd'] = "";
			$birth_date['yy'] = "";
		}else{
			$birth_date_arr =explode("-",$model->user_dob);
			$birth_date['mm'] = $birth_date_arr[1];
			$birth_date['dd'] = $birth_date_arr[2];
			$birth_date['yy'] = $birth_date_arr[0];
		}
		
		$start_year = date('Y')-70;
		//$end_year = date('Y')-20;
		$end_year = date('Y');
		$yearOptions = array();
		$count_year = $start_year;
		for($i=0;$i<=($end_year-$start_year);$i++){
			$yearOptions[$count_year]=$count_year;
			$count_year++;
		}
		if(Yii::app()->getRequest()->isPostRequest){
			$model->attributes=Yii::app()->getRequest()->getPost('Patient');
			$model->user_sex=Yii::app()->getRequest()->getPost('user_sex');
			$model->user_contact_method=Yii::app()->getRequest()->getPost('user_contact_method');
			$model->user_dob=$_REQUEST['yy']."-".$_REQUEST['mm']."-".$_REQUEST['dd'];
			if($model->save()){
				Yii::app()->user->setFlash('editProfile','Your profile has been updated.');
				$this->refresh();
			}
		}
		$this->render('edit_profile',array(
			'model' => $model,'birth_date' => $birth_date,'yearOptions' => $yearOptions,
		));
	}
	
	public function actionPatientReview() {
		if( !isset($_REQUEST['overall_rating']) ) {
			$this->redirect(array('404error'));
		}
		$overall_rating = $_REQUEST['overall_rating'];
		$bedside_manner_rating = $_REQUEST['bedside_manner_rating'];
		$wait_time_rating = $_REQUEST['wait_time_rating'];
		$scheduling_appointment=$_REQUEST['scheduling_appointment'];
		$office_experience=$_REQUEST['office_experience'];
		$spent_time=$_REQUEST['spent_time'];
		
		//$review_title = $_REQUEST['review_title'];
		$review_message = $_REQUEST['review_message'];
		$patient_id = Yii::app()->session['logged_user_id'];
		$doctor_id = $_REQUEST['doctor_id'];
		$status = 1;
		
		$sql='INSERT INTO da_doctor_review (doctor_id, patient_id, overall_rating, bedside_manner_rating, wait_time_rating,schedul_appoitment_rating,office_experience_rating,spent_time_rating , message, date_created, status) VALUES(:doctor_id,:patient_id,:overall_rating,:bedside_manner_rating,:wait_time_rating,:schedul_appoitment,
		:office_experience,:spent_time,:message,:date_created,:status)';
		$params = array(
			"doctor_id" => $doctor_id,
			"patient_id" => $patient_id,
			"overall_rating" => $overall_rating,
			"bedside_manner_rating" => $bedside_manner_rating,
			"wait_time_rating" => $wait_time_rating,
			"schedul_appoitment"=>$scheduling_appointment, 
			"office_experience"=>$office_experience, 
			"spent_time"=> $spent_time,
			/*"title" => $review_title,*/
			"message" => $review_message,
			"date_created" => date('Y-m-d h:i:s'),
			"status" => $status
		);
		$connection = Yii::app()->db;
		$command=$connection->createCommand($sql);
		$command->execute($params);
    }
	
	public function actionResetpassword()
	{
		if(!Yii::app()->session['logged_in']){
			$this->redirect(array('site/index'));
		}
		$error = array();
		if($_POST){
			
			$old_password = trim($_POST['old_password']);
			$new_password = $_POST['new_password'];
			$confirm_password = $_POST['confirm_password'];
			$connection = Yii::app()->db;
			$sql="select * FROM da_patient WHERE status=1 and id=".Yii::app()->session['logged_user_id']." and password='".md5($old_password)."'";
			$command = $connection->createCommand($sql);
			$user_arr = $command->queryAll();
			if(empty($user_arr)) $error['old_password'] = 'Password not matched.';
			if($new_password == '') $error['new_password'] = 'Password cannot be blank.';
			if($confirm_password != $new_password) $error['confirm_password'] = 'Password not matched with New Password.';
			
			if(empty($error)){
				$sql='UPDATE da_patient SET password = :password WHERE status=1 and id ='.Yii::app()->session['logged_user_id'];
				$params = array(
					"password" => md5($confirm_password)
				);
				$connection = Yii::app()->db;
				$command=$connection->createCommand($sql);
				$command->execute($params);
				Yii::app()->user->setFlash('resetPassword','Your password has been updated.');
				$this->refresh();
			}
		}
		
		$this->render('reset_password',array(
			'error'=>$error,
		));
	}
    public function actionForceresetpassword()
    {
        if(!Yii::app()->session['logged_in'] || Yii::app()->session['force_pw_change']!=1){
            $this->redirect(array('site/index'));
        }
        $error = array();
        if($_POST){
            $new_password = $_POST['new_password'];
            $confirm_password = $_POST['confirm_password'];
            $connection = Yii::app()->db;
            $sql="select * FROM da_patient WHERE status=1 and id=".Yii::app()->session['logged_user_id']."";
            $command = $connection->createCommand($sql);
            $user_arr = $command->queryAll();
            if(empty($user_arr)) $error['new_password'] = 'user not found';
            if($new_password == '') $error['new_password'] = 'Password cannot be blank.';
            if($confirm_password != $new_password) $error['confirm_password'] = 'Password not matched with New Password.';

            if(empty($error)){
                Yii::app()->session['force_pw_change'] = 0;
                $t = time();
                $sql='UPDATE da_patient SET password = :password , force_pw_change = :force_pw_change ,force_pw_changed_at =:force_pw_changed_at WHERE status=1 and id ='.Yii::app()->session['logged_user_id'];
                $params = array(
                    "password" => md5($confirm_password),
                    "force_pw_change" => 0,
                    "force_pw_changed_at"=> $t
                );
                $connection = Yii::app()->db;
                $command=$connection->createCommand($sql);
                $command->execute($params);

                Yii::app()->user->setFlash('resetPassword','Your password has been updated.');

                Yii::app()->session['force_pw_change'] = 0;
                $this->refresh();
            }
        }

        $this->render('force_reset_password',array(
            'error'=>$error,
        ));
    }
	
	public function actionSignout() {
       Yii::app()->session->destroy();
    }
	
	public function actionEdit(){
		$userId = Yii::app()->getRequest()->getParam('id');
		$user = User::model()->findByPk($userId);
		
		$users = User::model()->findAll(array(
			'condition' => 'status = :status',
			'params' => array(':status' => 0),
		));
		
		if(count($users) > 0){
			foreach($users as $user){
				$user->country->count_name;
			}
		}
		
		$user->name = 'Amit';
		$user->status = 0;
		$user->save();
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}
	
	protected function removeCaptchaSession()
	 {
		$session = Yii::app()->session;
		$prefixLen = strlen(CCaptchaAction::SESSION_VAR_PREFIX);
		foreach ($session->keys as $key)
		{
			if (strncmp(CCaptchaAction::SESSION_VAR_PREFIX, $key, $prefixLen) == 0)
			$session->remove($key);
		}
	 }
	 public function actionXyz() {
	 	
	 	$v = Helpers::mailsend('bhaskar.diet@gmail.com','admin@xyz.com','Hi','TEST HI');
	 	echo "<pre>";
	 	print_r($v);
	 }

    public function actionDocument(){

        if((Yii::app()->session['logged_in']) && (Yii::app()->session['logged_user_type'] == "patient") ){
            $model = new Patient();
            $model->id = Yii::app()->session['logged_user_id'];

            $usercriteria =Patient::model()->find("id=\"$model->id\"");
        }

        else{
            $this->redirect(array('site/index'));
        }
        $criteria=new CDbCriteria();
        $criteria->condition = 'owner=:owner';
        $criteria->params = array(':owner'=>Yii::app()->session['logged_user_id']);
        $criteria->addCondition('owner_type ="p"');
        $criteria->order ='id DESC';
        $criteria1 = new CDbCriteria();
        if((Yii::app()->session['logged_in']) && (Yii::app()->session['logged_user_type'] == "patient") ){
            if(isset($_POST['kword']) && ($_POST['kword']!="")){

                $criteria->addCondition("id =".$_POST['kword']);
                $criteria1->addCondition("db_shared_document.document_id =".$_POST['kword']);
            }
        }


        $doc = Documents::model()->findAll($criteria);
        $criteria1->addCondition('status="shared"');
        $criteria1->addCondition('db_shared_document.shared_type="p"');
        $criteria1->addCondition('db_shared_document.shared_id=' . Yii::app()->session['logged_user_id']);
        $criteria1->join = 'LEFT JOIN db_shared_document ON t.id = db_shared_document.document_id';
        $criteria1->order ='id DESC';
        $shared = Documents::model()->findAll($criteria1);
        $document = array_merge($doc,$shared);
        $document = array_unique($document, SORT_REGULAR);

        $count =count($doc);

//pagination
        $pages = new CPagination($count);
        $pages->setPageSize(10);
        $pages->applyLimit($criteria);
        $pages->applyLimit($criteria1);
        $doc = Documents::model()->findAll($criteria);
        $shared = Documents::model()->findAll($criteria1);
        $document = array_merge($doc,$shared);
        $document = array_unique($document, SORT_REGULAR);
        if(isset($_POST['kword'])){

            $this->renderPartial('document_view',array(
                'dataProvider'=>$usercriteria,'document'=>$document,'pages'=>$pages
            ));
        }else{
            $this->render('document',array(
                'dataProvider'=>$usercriteria,'document'=>$document,'pages'=>$pages
            ));
        }

    }

    public function actiongetLists(){
        // print_r($_REQUEST['keyword']);exit;
        $this->layout = false;
        $keyword = $_REQUEST['keyword'].'%';
        $userId = Yii::app()->session['logged_user_id'];
        $userType = Yii::app()->session['logged_user_type'];

        if(Yii::app()->session['logged_in']){
            $connection = Yii::app()->db;

            if( $userType == "doctor" ) {

                $sql='select db.`patient_id` as pid, p.`id` as `vic_id`,p.`user_dob` as `vic_dob`,p.`user_first_name`,p.`user_last_name`,concat_ws(" ", p.user_first_name,  p.user_last_name ) as `vic_name` ,p.`user_email` as `vic_email` FROM `da_doctor_book` as db LEFT JOIN `da_patient` as p ON(p.`id` = db.`patient_id`) WHERE db.`doctor_id`= "'.$userId.'" AND (p.`user_first_name` LIKE "'.$keyword.'" OR p.`user_last_name` LIKE "'.$keyword.'")  GROUP BY p.`id`';

                $command = $connection->createCommand($sql);
                $user_details = $command->queryAll();
                $user_details_res = array();


            }

            foreach ($user_details as $rs) {
                // put in bold the written text
                $vic_dob=date("m-d-Y",strtotime($rs['vic_dob']));
                $vic_full_name = str_replace($_REQUEST['keyword'], '<b>'.$_REQUEST['keyword'].'</b>', $rs['vic_name']);
                $data = $rs['vic_name']."|".$rs['vic_dob']."|".$rs['vic_id'];
                echo '<li onclick="mail_patient(\''.$data.'\')">'.$vic_full_name.",".$vic_dob.'</li>';
                // echo '<li onclick="set_item(\''.$data.'\')">'.$vic_full_name.'</li>';
            }


        }else{
            $this->redirect(array('site/index'));
        }
    }
}