<?php

class RegistrationController extends Controller
{
	/*public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view', 'create'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}*/
	public function actionSignin()
	{
        $defender = new Defender();

        if (!$defender->isSafeIp())
        {
            exit();
        }
        if( !isset($_REQUEST['email']) ) {

			$this->redirect(array('404error'));
            		}
		//print_r($_REQUEST);	
		//====================================================================
		$connection = Yii::app()->db;
		$validationFlag = '';
		$email = Yii::app()->getRequest()->getPost('email');
		$sql="SELECT * FROM `da_doctor` WHERE `username` = '".$email."'" . "OR `email` = '".$email."'";
		$command = $connection->createCommand($sql);
		$result = $command->queryAll();		
		
		
		
		if($result)
		{
			$validationFlag = 'doctor';
		}
		//print $validationFlag;
		$sql2="SELECT * FROM `da_patient` WHERE `user_name` = '".$email."'";
		$command2 = $connection->createCommand($sql2);
		$result2 = $command2->queryAll();	
		//print_r($result2);
		if($result2)
		{
			$validationFlag = 'patient';
		}
		
		//=====================================================================
			$account_type = $validationFlag;
		
		if($account_type == 'doctor')
		{
			$model = new Doctor();
		}else{
			$model = new Patient();
		}
		//echo $account_type;
		
		if(Yii::app()->getRequest()->isAjaxRequest){				
			
			if($account_type == 'doctor'){				
				$model->email = Yii::app()->getRequest()->getPost('email');
				$model->password = Yii::app()->getRequest()->getPost('password');
				$usercriteria =Doctor::model()->find("(username=\"$model->email\" OR email=\"$model->email\")  and (email_verified=1 and password=md5(\"$model->password\"))");
			}else{				
				$model->user_email = Yii::app()->getRequest()->getPost('email');
				if(Yii::app()->getRequest()->getPost('fb_id')!=''){
					$fb_id = Yii::app()->getRequest()->getPost('fb_id');
					$usercriteria =Patient::model()->find("user_name=\"$model->user_email\"");
					
					$sql='UPDATE da_patient SET fb_id = :fb_id WHERE status=1 and id ='.$usercriteria->id;
					$params = array(
						"fb_id" => $fb_id
					);
					$connection = Yii::app()->db;
					$command=$connection->createCommand($sql);
					$command->execute($params);
				}else{
					$model->password = Yii::app()->getRequest()->getPost('password');
					$usercriteria =Patient::model()->find("user_name=\"$model->user_email\" and password=md5(\"$model->password\")");
				}
			}
			/*print_r($usercriteria);
			die;*/
			if(isset($usercriteria)){
                $defender->removeFailedLogins();
				if($account_type == 'doctor'){
                                    
 					if( isset($result[0]['payment_type']) && ( $result[0]['payment_type'] == "free" ) ) {
                                            Yii::app()->session['logged_with_claim'] = '1';
                                    }
                                    
                                    
					Yii::app()->session['logged_in'] = true;
					Yii::app()->session['logged_user_email'] = $usercriteria->username;
					Yii::app()->session['logged_user_id'] = $usercriteria->id;
					Yii::app()->session['logged_user_type'] = 'doctor';
					Yii::app()->session['logged_user_email_address'] = $usercriteria->email;

                    $sqlCount="select * FROM da_inbox  LEFT JOIN `da_message_inbox`  ON(da_inbox.inbox_id = da_message_inbox.`message_id`) WHERE status=1  AND da_message_inbox.user_id='" . Yii::app()->session['logged_user_id'] . "' AND da_message_inbox.user_type=1 AND `message_read_flag`=0";
					$commandCount = $connection->createCommand($sqlCount);
					$inbox_arr_count = $commandCount->queryAll();
					$count = count($inbox_arr_count);
					Yii::app()->session['inbox_count'] = $count;

					
					if($usercriteria->google_account)
						Yii::app()->session['google_account'] = $usercriteria->google_account;
				}else{
					Yii::app()->session['logged_in'] = true;
					Yii::app()->session['logged_time'] = time();
					Yii::app()->session['logged_user_email'] = $usercriteria->user_name;
					Yii::app()->session['logged_user_id'] = $usercriteria->id;
					Yii::app()->session['logged_user_type'] = 'patient';
					Yii::app()->session['logged_user_email_address'] = $usercriteria->user_email;
					Yii::app()->session['force_pw_change'] = $usercriteria->force_pw_change;
					Yii::app()->session['force_pw_change_reason'] = $usercriteria->force_pw_change_reason;



					$sqlCount="select * FROM da_inbox  LEFT JOIN `da_message_inbox`  ON(da_inbox.inbox_id = da_message_inbox.`message_id`) WHERE status=1  AND da_message_inbox.user_id='" . Yii::app()->session['logged_user_id'] . "' AND da_message_inbox.user_type=1 AND `message_read_flag`=0";
					$commandCount = $connection->createCommand($sqlCount);
					$inbox_arr_count = $commandCount->queryAll();
					$count = count($inbox_arr_count);
					Yii::app()->session['inbox_count'] = $count;
					
				}
				echo true;
			}
            else{
                $defender->recordFailedLogin();
                //print_r($defender->failedLogins);
                if($defender->failedLogins > 2){
                    if($validationFlag != ''){


                    }


                    $defender->lockIp();
                    echo 0;
                }
                elseif($defender->failedLogins ==2){
                    echo 2;
                }
                elseif($defender->failedLogins <2){
                    echo 0;
                }
            }

		}
	}
	public function actionSignout() {
       Yii::app()->session->destroy();
    }
	public function actionJoin()
	{
		//$model = new Doctor();
		$sql='select * FROM da_speciality WHERE status="1"';
		$connection = Yii::app()->db;
		$command = $connection->createCommand($sql);
		$user_speciality = $command->queryAll();
		$user_speciality_res = array();
		foreach ($user_speciality as $key=>$val) {
			$user_speciality_res[$val['id']] = $val['speciality'];
		}
		
		if(Yii::app()->getRequest()->isPostRequest && Yii::app()->session['doctor_join_level_1']){
			/*$model->attributes=Yii::app()->getRequest()->getPost('Doctor');
			$model->speciality = Yii::app()->getRequest()->getPost('speciality');*/
			
			$id=Yii::app()->session['logged_user_id'];
			$model = Doctor::model()->findByPk($id);
			$model->attributes=Yii::app()->getRequest()->getPost('Doctor');
			$model->speciality = Yii::app()->getRequest()->getPost('speciality');
			$model->status = 1;
			if($model->save()){
				$this->removeCaptchaSession();
				$speciality_id=Yii::app()->getRequest()->getPost('speciality');

				$sql='UPDATE da_doctor_speciality SET speciality_id = :speciality_id WHERE doctor_id ='.$id;
				$params = array(
					"speciality_id" => $speciality_id
				);
				$connection = Yii::app()->db;
				$command=$connection->createCommand($sql);
				$command->execute($params);
				
				$this->redirect(array('doctorPayment/index'));
			}
		}
		else if(Yii::app()->getRequest()->isPostRequest){
			$model = new Doctor();
			$model->attributes=Yii::app()->getRequest()->getPost('Doctor');
			$model->speciality = Yii::app()->getRequest()->getPost('speciality');
			$model->status = 1;
			$model->date_created = date('Y-m-d h:i:s');
			$model->date_modified = date('Y-m-d h:i:s');
			$model->email_verified = 0;
			//$model->email_verified = 1;
			$password_new_d = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 8);
			$model->password = md5($password_new_d);
			//$model->password = md5('123456');
			$model->username = $_POST['Doctor']['email'];
			$model->full_name = $model->first_name.' '.$model->last_name;
			$model->slug = Helpers::createSlug($model->full_name);
			if($model->save()){
				$this->removeCaptchaSession();
				$newSlag = trim($model->slug."-".$model->id);
				$model->updateByPk($model->id, array('slug' => $newSlag ));
				//$this->redirect(array('join'));
				//Yii::app()->user->setFlash('join','Thank you for joining. We will respond to you as soon as possible.');
				//$this->refresh();
				$speciality_id=Yii::app()->getRequest()->getPost('speciality');
				$default_status=1;

				$sql='INSERT INTO da_doctor_speciality (speciality_id, doctor_id, default_status, status) VALUES(:speciality_id,:doctor_id,:default_status,:status)';
				$params = array(
					"speciality_id" => $speciality_id,
					"doctor_id" => $model->id,
					"default_status" => $default_status,
					"status" => 1
				);
				$connection = Yii::app()->db;
				$command=$connection->createCommand($sql);
				$command->execute($params);
				
				Yii::app()->session['doctor_join_level_1'] = true;
				Yii::app()->session['logged_user_id'] = $model->id;
				
				$doctor_id_temp = Yii::app()->session['logged_user_id'];
				$doctor_det_temp =Doctor::model()->find("id=\"$doctor_id_temp\"");
				$state_det_temp_d = State::model()->find("short_code=\"$doctor_det_temp->state\"");
				$temp_id = 14;
				$email_template = EmailTemplate::model()->find("id=\"$temp_id\"");
				if(!empty($email_template) && $email_template->tempalte_body != ''){
					$message_body = $email_template->tempalte_body;
					//$message_body = $this->getTemplateDataParsing($parse_with,$parse_data,$message_body);
					
					$parse_data_arr = array();
					/*$parse_data_arr['{{doctor.full_name}}'] = $doctor_det_temp->first_name." ".$doctor_det_temp->last_name;
					$parse_data_arr['{{doctor.first_name}}'] = $doctor_det_temp->first_name;
					$parse_data_arr['{{doctor.last_name}}'] = $doctor_det_temp->last_name;
					*/
					$parse_data_arr['{{doctor.full_name}}'] = $doctor_det_temp->full_name;
					$parse_data_arr['{{doctor.first_name}}'] = $doctor_det_temp->first_name;
					$parse_data_arr['{{doctor.last_name}}'] = $doctor_det_temp->last_name;
					$parse_data_arr['{{doctor.address_1}}'] = $doctor_det_temp->addr1;
					$parse_data_arr['{{doctor.city}}'] = $doctor_det_temp->city;
					$parse_data_arr['{{doctor.state}}'] = isset( $state_det_temp_d->name ) ? $state_det_temp_d->name : '';
					$parse_data_arr['{{doctor.zip_code}}'] = $doctor_det_temp->zip;
					$parse_data_arr['{{doctor.phone}}'] = $doctor_det_temp->phone;
					$parse_data_arr['{{doctor.email}}'] = $doctor_det_temp->email;
					$parse_data_arr['{{doctor.parctise_name}}'] = $doctor_det_temp->practice;
					
					
					$message_body = $this->getTemplateDataParsing($parse_data_arr,$message_body);
				}else{
					$message_body = '<h2 style="color:#636363; margin:0px; padding-bottom:10px; padding-left:8px; padding-right:8px; font-size:13px; font-weight:bold;">Your account has now been created.</h2>';
				}
				
				$to_email = $doctor_det_temp->email;
				$reg_id = '';
				$to_name = $doctor_det_temp->first_name." ".$doctor_det_temp->last_name;
				//$to = array($to_email,$to_name);
				$to = $to_email."|".$to_name;
				$from = array('support@edoctorbook.com','eDoctorBook');
				//$subject = 'eDoctorBook - Thank you for registering.';
				$subject = isset( $email_template->subject ) ? $email_template->subject : 'eDoctorBook - Thank you for registering.';
				$message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Doctor Email Template</title>
		</head>
		<body style="padding:0px; margin:0px; font-size:12px; color:#545454; line-height:18px; font-family:Arial, Helvetica, sans-serif;">
		<div style="width:450px;  margin:5px auto; border:1px solid #c7c7c7; "> 
		<div style="text-align:left; margin-bottom:15px; -moz-box-shadow:0px 1px 1px 1px #E4E4E4; -webkit-box-shadow:0px 1px 1px 1px #E4E4E4;box-shadow:0px 1px 1px 1px #E4E4E4; background:#54cbc8; font-weight:bold;">
		<a href="'.$this->createAbsoluteUrl('site/index/'.$reg_id).'"><img src="http://'.$_SERVER['HTTP_HOST'].Yii::app()->getHomeUrl().'/assets/images/small_logo.png" style="padding:3px;" alt="Doctor Appointment Book" /></a>
		&nbsp;
		</div>					
		<div style="min-height:200px; background:#fff; color:#bbb; margin:10px; padding-top:15px;">

		<pre style="color:#000">
		'.$message_body.'
		</pre>
		<br>

		<table style="width:96%; margin-left:8px; margin-right:8px; border:1px solid #9ddddb;" cellpadding="0" cellspacing="0">		
		<tr>
			<td style="width:100; line-height:22px; padding-top:8px; padding-bottom:8px; text-align:center; padding-left:5px; 
			color:#636363; border-right:1px solid #5abdba; font-size:13px;">Username : '.$to_email.'</td>
			<td style="width:100; line-height:22px;padding-top:8px; padding-bottom:8px; text-align:center; padding-left:5px;
			 color:#636363;  border-right:1px solid #5abdba; font-size:13px;">Password : '.$password_new_d.'</td>
		</tr>
		</table>

		<p style="font-size:12px; padding-top:25px; padding-left:8px; padding-right:8px; color:#4c4c4c; display:block;">You can log in by using your provided User Id and password 
		at the following URL:<br />
		 <a style="color:#2593e0; text-decoration:underline; display:block;" href="'.$this->createAbsoluteUrl('ddb/').'">'.$this->createAbsoluteUrl('ddb/').'</a>
		</p>

		</div>
		<p style=" margin-top:10px; padding:8px 5px; color:#000; font-size:13px; margin-bottom:0px; font-weight:bold; line-height:18px; ">eDoctorBook Team</p>
		<a style="padding-left:5px; padding-right:5px; color:#000; font-size:13px; font-weight:bold; margin:0px; line-height:0px; text-decoration:none" href="">http://www.eDoctorBook.com</a>
		<div style="background:#54cbc8; padding-top:20px; margin-top:15px; padding-bottom:10px; text-align:center; color:#e2e2e2; font-size:14px;"></div>
		</div>

		</body>
		</html>';
				
				Yii::app()->session['reg_to'] = $to;
				Yii::app()->session['reg_subject'] = $subject;
				Yii::app()->session['reg_mail_body'] = $message;
				//Helpers::mailsend($to,$from,$subject,$message);				
				$this->redirect(array('doctorPayment/index'));
			}
		}
		else if(Yii::app()->session['doctor_join_level_1']){
			$this->removeCaptchaSession();
			$model = new Doctor();
			$logged_user_id = Yii::app()->session['logged_user_id'];
			$usercriteria =Doctor::model()->find("id=\"$logged_user_id\"");
			$dataProvider = array();
			$model->first_name=$usercriteria->first_name;
			$model->last_name=$usercriteria->last_name;
			$model->email=$usercriteria->email;
			$model->phone=$usercriteria->phone;
			//$model->zip=$usercriteria->zip;
			$model->speciality=$usercriteria->speciality;
		}else{
			$this->removeCaptchaSession();
			$model = new Doctor();
		}
		
		$this->render('join',array(
			'model' => $model,'user_speciality' => $user_speciality_res,
		));
	}
	
	public function actionEdit(){
		$userId = Yii::app()->getRequest()->getParam('id');
		$user = User::model()->findByPk($userId);
		
		$users = User::model()->findAll(array(
			'condition' => 'status = :status',
			'params' => array(':status' => 0),
		));
		
		if(count($users) > 0){
			foreach($users as $user){
				$user->country->count_name;
			}
		}
		
		$user->name = 'Amit';
		$user->status = 0;
		$user->save();
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}
	
	protected function removeCaptchaSession()
	 {
		$session = Yii::app()->session;
		$prefixLen = strlen(CCaptchaAction::SESSION_VAR_PREFIX);
		foreach ($session->keys as $key)
		{
			if (strncmp(CCaptchaAction::SESSION_VAR_PREFIX, $key, $prefixLen) == 0)
			$session->remove($key);
		}
	 }
	
	function getTemplateDataParsing($parse_data_arr,$message) {
         foreach($parse_data_arr as $parse_data_key=>$parse_data_val){
		 	$message = str_replace($parse_data_key, $parse_data_val, $message);
		 }
		 return $message;
    }
	public function actionJoinValidEmail()
	{
		//print_r($_REQUEST);
		$connection = Yii::app()->db;
		$validationFlag = 'ok';
                $email = '';
                if(isset($_REQUEST['Email'])) $email = $_REQUEST['Email'];
                
		$sql="SELECT * FROM `da_doctor` WHERE `username` = '".$email."'";
		$command = $connection->createCommand($sql);
		$result = $command->queryAll();		
		
		
		if($result)
		{
			$validationFlag = 'Availabel';
		}
		
		$sql2="SELECT * FROM `da_patient` WHERE `user_name` = '".$email."'";
		$command2 = $connection->createCommand($sql2);
		$result2 = $command2->queryAll();	
		
		if($result2)
		{
			$validationFlag = 'Availabel';
		}

		print $validationFlag;
	}
	public function actionPasswordValid()
	{
		$candidate = $_REQUEST['password'];	
		preg_match_all('$\S*(?=\S{8,})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])(?=\S*[\W])\S*$', $candidate, $result);
		//print_r($result);
		if($result[0])
		{
			print "Valid";			
		}else{
			print "Invalid";
		}		
		
	}

	public function actionSMS()
	{
		Yii::import('application.vendor.twilio.*');
		spl_autoload_unregister(array('YiiBase','autoload')); 
		//require 'C:\Projects\EMR\webapp\protected\vendors\twilio-php\Services\Twilio.php';
		include_once( dirname(__FILE__) . "/../vendor/twilio/Services/Twilio.php");
		spl_autoload_register(array('YiiBase', 'autoload'));

		
		 // Step 2: set our AccountSid and AuthToken from www.twilio.com/user/account
		$AccountSid = "AC74068ab1383071d146bebb5c905dbcb5";
		$AuthToken = "6ee3bc04ff855fefaefc68d9cd1bd778";
		$AuthPhone = "+16508259090";
		// Step 3: instantiate a new Twilio Rest Client
		$client = new Services_Twilio($AccountSid, $AuthToken);
		//$number = '+919903863458';
		$number = '+919903863458';
		$message = 'Hello This is Test Msg';
		$sms = $client->account->messages->sendMessage("$AuthPhone",$number,$message);
		print "<pre>";
		print_r($sms);
	}
}
