<?php
class DoctorController extends Controller{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';
	/**
	 * @return array action filters
	 */

	public function filters() {
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */

	/*public function accessRules()

	{

		return array(

			array('allow',  // allow all users to perform 'index' and 'view' actions

				'actions'=>array('index','view'),

				'users'=>array('*'),

			),

			array('allow', // allow authenticated user to perform 'create' and 'update' actions

				'actions'=>array('create','update'),

				'users'=>array('@'),

			),

			array('allow', // allow admin user to perform 'admin' and 'delete' actions

				'actions'=>array('admin','delete'),

				'users'=>array('admin'),

			),

			array('deny',  // deny all users

				'users'=>array('*'),

			),

		);

	}*/

	/* -------------- Redious Search get Distance--------------- */

	public function radiusSearch($zipcode='',$distancekm='') {
		$decoded_result = array();
		$short_array = array();
		if($zipcode != '' and $distancekm != '') {
			$url = 'http://zipcodedistanceapi.redline13.com/rest';
			$key = '2odywUYrQJOEEO0vJdlcH5Qd8Lf6EGKG4YfBTd2JXQoCFo7pBsBiysvGdLNSsyzw';
	
			$curl = curl_init();
			curl_setopt_array($curl,
			array( CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_URL => $url.'/'.$key.'/radius.json/'.$zipcode.'/'.$distancekm.'/mile')
			);
	
			$result = curl_exec($curl);
			$decoded_result = json_decode($result, true);
			if(count($decoded_result)>0) {	
				if(!isset($decoded_result['error_code'])) {
					foreach($decoded_result['zip_codes'] as $decoded_results) {
						$short_array[$decoded_results['zip_code']] = $decoded_results['distance'];
		
						$main_Array[$decoded_results['zip_code']]['zip_code'] = $decoded_results['zip_code'];
						$main_Array[$decoded_results['zip_code']]['distance'] = $decoded_results['distance'];
					}
				}
			}	
			asort($short_array);
		}
		return $short_array;
	}
	
	/* -------------- End Redious Search get Distance--------------- */

	public function radiusSearchVal($zipcode1='',$zipcode2='') {

			$decoded_result = array();
			$url = 'http://zipcodeapi.com/rest';
			$key = 'oRHyDEYSEJ7aV00bJXmUsaPqItswA2vYUcxxRvqvfxFaxGmgEIb7S0xBu0842D3X';

			$curl = curl_init();
			curl_setopt_array($curl, 
							   array( CURLOPT_RETURNTRANSFER => 1,
									  CURLOPT_URL => $url.'/'.$key.'/distance.json/'.$zipcode1.'/'.$zipcode2.'/mile')
							);
			$result = curl_exec($curl);
			$decoded_result = json_decode($result, true);
			return $decoded_result;
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	
	public function actionView($id) {
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */

	public function actionCreate() {

		$model=new Doctor;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		if(isset($_POST['Doctor'])) {
			$model->attributes=$_POST['Doctor'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}
		$this->render('create',array(
			'model'=>$model,
		));
	}



	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */

	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		if(isset($_POST['Doctor']))
		{
			$model->attributes=$_POST['Doctor'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}
		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */

	public function actionDelete($id) {
		$this->loadModel($id)->delete();
		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		/*$dataProvider=new CActiveDataProvider('Doctor');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));*/
		if(Yii::app()->session['logged_in']){
			$model = new Doctor();
			//print_r($_REQUEST);
			$model->id = Yii::app()->session['logged_user_id'];
			//$model->password = Yii::app()->getRequest()->getPost('password');
			$usercriteria =Doctor::model()->find("id=\"$model->id\"");
		}else{
			$this->redirect(array('site/index'));
		}
		$connection = Yii::app()->db;
		$sql='select * FROM da_speciality WHERE status="1"';
		$command = $connection->createCommand($sql);
		$user_speciality = $command->queryAll();
		$user_speciality_res = array();
		foreach ($user_speciality as $key=>$val) {
			$user_speciality_res[$val['id']] = $val['speciality'];
		}
		$sql_multy_speciality='select * FROM da_doctor_speciality WHERE status="1" and doctor_id="'.$model->id.'"';
		$command_multy_speciality = $connection->createCommand($sql_multy_speciality);
		$user_multy_speciality = $command_multy_speciality->queryAll();
		$user_selected_speciality = array();
		foreach ($user_multy_speciality as $key=>$val) {
			$user_selected_speciality[] = $val['speciality_id'];
		}
		$default_data_address = DoctorAddress::model()->findByAttributes(array('doctor_id'=>$model->id,'status'=>1,'default_status'=>1));
		$strt_tm = date('Y-m-d');
		$end_tm = date('Y-m-d',mktime(0,0,0,date('m'),date('d')+30,date('Y')));
		$dashboardAppointment = $this->actionDashboardAppointment($model->id,$strt_tm,$end_tm);
		
		$sql_procedure='select * FROM da_procedure WHERE status="1"';
		$command_procedure = $connection->createCommand($sql_procedure);
		$user_procedure = $command_procedure->queryAll();
		$user_procedure_res = array();
		foreach ($user_procedure as $key=>$val) {
			$user_procedure_res[$val['id']] = $val['procedure'];
		}

		$sql_reason_for_visit='select * FROM da_reason_for_visit WHERE status="1"';
		$command_reason_for_visit = $connection->createCommand($sql_reason_for_visit);
		$user_reason_for_visit = $command_reason_for_visit->queryAll();
		$user_reason_for_visit_res = array();
		foreach ($user_reason_for_visit as $key=>$val) {

			$user_reason_for_visit_res[$val['id']] = $val['reason_for_visit'];

		}
		//$dashboardAppointment_arr = explode('***%%%***',$dashboardAppointment);
		//$dashboardAppointment_arr = array();
		//Helpers::pre($dashboardAppointment[1]);die;

		$doctor_id = $model->id;
		$condition = 'status = 1 and doctor_id =' . $doctor_id;
		$criteria = new CDbCriteria(array(

			'condition' => $condition

		));

		//$usercriteria_todo_list =TodoList::model()->findAll($criteria);

		$sql_todo_list='select * FROM da_todo_list WHERE status="1" and doctor_id =' . $doctor_id;
		$command_todo_list = $connection->createCommand($sql_todo_list);
		$usercriteria_todo_list = $command_todo_list->queryAll();
		//Helpers::pre($usercriteria_todo_list);die;

		$doctor_review_condition = 't.status = 1 and doctor_id =' . $doctor_id;
		$doctor_review_criteria = new CDbCriteria(array(

			'condition' => $doctor_review_condition

		));

		$doctor_review=DoctorReview::model()->with('patient_details')->findall($doctor_review_criteria);
		//Helpers::pre($doctor_review);die;
		$this->render('index',array(

			'dataProvider'=>$usercriteria,'user_speciality' => $user_speciality_res,'default_data_address' => $default_data_address,'dashboardAppointment' => $dashboardAppointment[1],'dashboardAppointmentCnt' => $dashboardAppointment[0],'user_app_request' => $dashboardAppointment[2],/*'user_procedure' => $user_procedure_res,*/'todo_list' => $usercriteria_todo_list,'doctor_review' => $doctor_review,'user_selected_speciality' => $user_selected_speciality,'user_reason_for_visit' => $user_reason_for_visit_res,

		));

	}

	

	public function actionResetPasswordPanel() {
		if(!Yii::app()->session['logged_in']){
			$this->redirect(array('site/index'));
		}
		$this->layout = false;
		$error = array();
		if($_POST){
			$old_password = trim($_POST['old_password']);
			$new_password = $_POST['new_password'];
			$confirm_password = $_POST['confirm_password'];
			$connection = Yii::app()->db;
			$sql="select * FROM da_doctor WHERE status=1 and id=".Yii::app()->session['logged_user_id']." and password='".md5($old_password)."'";
			$command = $connection->createCommand($sql);
			$user_arr = $command->queryAll();
			if(empty($user_arr)) $error['old_password'] = 'Password not matched.';
			if($new_password == '') $error['new_password'] = 'Password cannot be blank.';
			if($confirm_password != $new_password) $error['confirm_password'] = 'Password not matched with New Password.';

			if(empty($error)){
				$sql='UPDATE da_doctor SET password = :password WHERE status=1 and id ='.Yii::app()->session['logged_user_id'];
				$params = array(
					"password" => md5($confirm_password)
				);
				$connection = Yii::app()->db;
				$command=$connection->createCommand($sql);
				$command->execute($params);
				Yii::app()->user->setFlash('resetPassword','Your Password updated sucessfully.');
				$this->refresh();
			}
		}
		$this->render('reset_password_panel',array(
			'error'=>$error,
		));
	}

	public function actionResetpassword() {
		if(!Yii::app()->session['logged_in']){
			$this->redirect(array('site/index'));
		}
		$error = array();
		if($_POST){
			$old_password = trim($_POST['old_password']);
			$new_password = $_POST['new_password'];
			$confirm_password = $_POST['confirm_password'];
			$connection = Yii::app()->db;
			$sql="select * FROM da_doctor WHERE status=1 and id=".Yii::app()->session['logged_user_id']." and password='".md5($old_password)."'";
			$command = $connection->createCommand($sql);
			$user_arr = $command->queryAll();
			if(empty($user_arr)) $error['old_password'] = 'Password not matched.';
			if($new_password == '') $error['new_password'] = 'Password cannot be blank.';
			if($confirm_password != $new_password) $error['confirm_password'] = 'Password not matched with New Password.';

			if(empty($error)){
				$sql='UPDATE da_doctor SET password = :password WHERE status=1 and id ='.Yii::app()->session['logged_user_id'];
				$params = array(
					"password" => md5($confirm_password)
				);
				$connection = Yii::app()->db;
				$command=$connection->createCommand($sql);
				$command->execute($params);
				Yii::app()->user->setFlash('resetPassword','Your Password updated sucessfully.');
				$this->refresh();
			}
		}

		$this->render('reset_password',array(
			'error'=>$error,
		));
	}

	public function actionForgetpassword() {
		$user_email = '';
		$error = '';
		$subject = 'Forget Password';
		if( isset( $_REQUEST['user-email'] ) ) {
			$user_email = $_REQUEST['user-email'];
			$validator = new CEmailValidator;
			if($validator->validateValue($user_email)){
				$usercriteria_doctor =Doctor::model()->find("username=\"$user_email\"");
				$password  = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 8);
				//$password = '123456';
				if(!empty($usercriteria_doctor)){
					$sql='UPDATE da_doctor SET password = :password WHERE id ='.$usercriteria_doctor->id;
					$params = array(
						"password" => md5($password)
					);
					$connection = Yii::app()->db;
					$command=$connection->createCommand($sql);
					$command->execute($params);
					$to_email = $usercriteria_doctor->email;
					$to_name = $usercriteria_doctor->title.' '.$usercriteria_doctor->first_name.' '.$usercriteria_doctor->last_name;
					$reg_id = $usercriteria_doctor->id;
					$username = $usercriteria_doctor->username;
					//print $password;
					$this->forgetMail($to_email, $to_name, $password, $subject, $reg_id, $username);
				}else{
					$usercriteria_patient =Patient::model()->find("user_email=\"$user_email\"");
					if(!empty($usercriteria_patient)){
						$sql='UPDATE da_patient SET password = :password WHERE id ='.$usercriteria_patient->id;
						$params = array(
							"password" => md5($password)
						);
						$connection = Yii::app()->db;
						$command=$connection->createCommand($sql);
						$command->execute($params);
						$to_email = $usercriteria_patient->user_email;
						$to_name = $usercriteria_patient->user_first_name.' '.$usercriteria_patient->user_last_name;
						$reg_id = $usercriteria_patient->id;
						$username = $usercriteria_patient->user_name;
						$this->forgetMail($to_email, $to_name, $password, $subject, $reg_id, $username);
					}
				}

				Yii::app()->user->setFlash('forgetPassword','Your mail send sucessfully. Please check your mail to know your password.');
			}else{
				$error = 'email not valid.';
			}
		}

		$this->render('forget_password',array(
			'user_email'=>$user_email,'error'=>$error,
		));
	}

	function forgetMail($to_email, $to_name, $password, $subject, $reg_id, $username) {
			$to_email = $to_email;
			$to_name = $to_name;	
			$temp_id = 10;
			$email_template = EmailTemplate::model()->find("id=\"$temp_id\"");
			if(!empty($email_template) && $email_template->tempalte_body != ''){
				$message_body = $email_template->tempalte_body;
				//$message_body = $this->getTemplateDataParsing($parse_with,$parse_data,$message_body);
				$parse_data_arr = array();
				$message_body = $this->getTemplateDataParsing($parse_data_arr,$message_body);
			}else{
				$message_body = '';
			}
			$to = array($to_email,$to_name);
			$from = array('info.doctor@gmail.com','eDoctorBook');
			$subject = 'eDoctorBook - '.$subject.'.';
				$message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
					<html xmlns="http://www.w3.org/1999/xhtml">
					<head>
					<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
					<title>Doctor Email Template</title>
					</head>
					<body style="padding:0px; margin:0px; font-size:12px; color:#545454; line-height:18px; font-family:Arial, Helvetica, sans-serif;">
					<div style="width:450px;  margin:5px auto; border:1px solid #c7c7c7; "> 
					<div style="text-align:left; margin-bottom:15px; -moz-box-shadow:0px 1px 1px 1px #E4E4E4; -webkit-box-shadow:0px 1px 1px 1px #E4E4E4;box-shadow:0px 1px 1px 1px #E4E4E4; background:#54cbc8; font-weight:bold;">
					<a href="'.$this->createAbsoluteUrl('site/index/'.$reg_id).'"><img src="http://'.$_SERVER['HTTP_HOST'].Yii::app()->getHomeUrl().'/assets/images/small_logo.png" style="padding:3px;" alt="Doctor Appointment Book" /></a>
					&nbsp;
					</div>					
					<div style="min-height:200px; background:#fff; color:#bbb; margin:10px; padding-top:15px;">
					<h2 style="color:#000">
					'.$message_body.'
					</h2>
					<br>
					<table style="width:96%; margin-left:8px; margin-right:8px; border:1px solid #9ddddb;" cellpadding="0" cellspacing="0">
					<tr>
						<th style="width:100; background:#6ad2cf; line-height:22px; text-align:center; text-transform:uppercase; padding-left:5px; color:#fff; 
						border-right:1px solid #5abdba">USER ID</th>
						<th style="width:100; background:#6ad2cf; line-height:22px; text-align:center; text-transform:uppercase; padding-left:5px; color:#fff;  border-right:1px solid #5abdba">PASSWORD</th>
					</tr>
					<tr>
						<td style="width:100; line-height:22px; padding-top:8px; padding-bottom:8px; text-align:center; padding-left:5px; 
						color:#636363; border-right:1px solid #5abdba; font-size:13px;">'.$username.'</td>
						<td style="width:100; line-height:22px;padding-top:8px; padding-bottom:8px; text-align:center; padding-left:5px;
						 color:#636363;  border-right:1px solid #5abdba; font-size:13px;">'.$password.'</td>
					</tr>
					</table>
					</div>
					<p style=" margin-top:10px; padding:8px 5px; color:#000; font-size:13px; margin-bottom:0px; font-weight:bold; line-height:18px; ">eDoctorBook Team</p>
					<a style="padding-left:5px; padding-right:5px; color:#000; font-size:13px; font-weight:bold; margin:0px; line-height:0px; text-decoration:none" href="">http://www.eDoctorBook.com</a>
					<div style="background:#54cbc8; padding-top:20px; margin-top:15px; padding-bottom:10px; text-align:center; color:#e2e2e2; font-size:14px;"></div>
					</div>
					</body>
					</html>';
			Helpers::mailsend($to,$from,$subject,$message);
	}

	public function actionDashboardAppointment($id,$strt_tm,$end_tm) {

		$strt_tm = $strt_tm;
		$end_tm = $end_tm;
		/*$strt_tm = $_REQUEST['start'];

		$end_tm = $_REQUEST['end'];*/
		if(!Yii::app()->session['logged_in'])
			$this->redirect(array('site/index'));

		$start_time = strtotime($strt_tm);
		$to_time = strtotime($end_tm);

		$sql_doctor_address='select db.*,dp.user_first_name as dp_name,da.address as da_address,dpro.procedure as dpro_procedure,concat_ws(" ", dp.user_first_name,  dp.user_last_name ) as dp_patient_name FROM da_doctor_book as db left join da_patient as dp on db.patient_id = dp.id left join da_doctor_address as da on db.address_id = da.id left join da_procedure as dpro on db.procedure_id = dpro.id WHERE db.status=1 and ( book_time <'.$to_time.' and book_time >='.$start_time.') and db.doctor_id='.$id.' order by db.book_time';
		$connection = Yii::app()->db;
		$command_doctor_address = $connection->createCommand($sql_doctor_address);
		$user_patient_book = $command_doctor_address->queryAll();
		$user_calender_res = array();
		$user_app_request = array();
		//$user_calender_cnt = count($user_patient_book);
		$user_calender_cnt = 0;
		$user_tot_arr = array();
		//$user_tot_arr[] = $user_calender_cnt;

		foreach ($user_patient_book as $key=>$val) {
			$st_time = date('Y-m-d h:i',$val['book_time']);
			$end_time = date('Y-m-d h:i',($val['book_time']+($val['book_duration']*60)));
			$cur_date = date('Y-m-d',$val['book_time']);
			if($val['confirm']!=1) ++$user_calender_cnt;
			$user_calender_res[$cur_date][] =array(
								'id' => $val['id'],
								'patient_name' => ($val['dp_patient_name'])?$val['dp_patient_name']:$val['patient_name'],
								'confirm' => $val['confirm'],
								'title' => $val['dp_name'],
								'start' => "$st_time",
								'end' => "$end_time",
								'book_duration' => $val['book_duration'],
								'procedure_id' => $val['procedure_id'],
								'reason_for_visit_id' => $val['reason_for_visit_id'],
								'cur_date' => date('Y-m-d',$val['book_time']),
								'allDay' => false,
								'description' =>"<p><span>Procedure</span>: ".$val['dpro_procedure']."</p><p><span>Address</span>: ".$val['da_address']."</p>",
					 			"borderColor" => "#1587bd",
								"textColor" => "#000000",
								"color" => "#9fc6e7",
							);
			if($val['confirm']!=1)
			$user_app_request[$cur_date][] = $val['confirm'];
		}
		$user_tot_arr[] = $user_calender_cnt;
		$user_tot_arr[] = $user_calender_res;
		$user_tot_arr[] = $user_app_request;
		
		return $events_code = $user_tot_arr;
	}

	

	public function actionAppointmentConfirm() {

		$app_id = $_REQUEST['app_id'];
		$comments = $_REQUEST['comments_text'];
		$connection = Yii::app()->db;
		$sql='UPDATE da_doctor_book SET confirm = :confirm,comments = :comments WHERE id ='.$app_id;
		$params = array(
			"confirm" => 1,
			"comments" => $comments
		);
		$command=$connection->createCommand($sql);
		$command->execute($params);

		$doctor_book_det_temp =DoctorBook::model()->find("id=\"$app_id\"");
		$user_det_temp =Patient::model()->find("id=\"$doctor_book_det_temp->patient_id\"");
		$doctor_id_temp = Yii::app()->session['logged_user_id'];
		$doctor_det_temp =Doctor::model()->find("id=\"$doctor_id_temp\"");
		$address_det_temp_p = DoctorAddress::model()->find("id=\"$doctor_book_det_temp->address_id\"");
		$state_det_temp_p = State::model()->find("short_code=\"$address_det_temp_p->state\"");
		$state_det_temp_d = State::model()->find("short_code=\"$doctor_det_temp->state\"");

		$book_time = $doctor_book_det_temp->book_time;
		if($user_det_temp->user_email != ''){
			$temp_id = 5;
			$email_template = EmailTemplate::model()->find("id=\"$temp_id\"");
				if(!empty($email_template) && $email_template->tempalte_body != ''){
					$message_body = $email_template->tempalte_body;
					//$message_body = $this->getTemplateDataParsing($parse_with,$parse_data,$message_body);
					$parse_data_arr = array();
					$parse_data_arr['{{appointment.actual_format}}'] = date('l, F j - h:i A',$book_time);
					$parse_data_arr['{{appointment.day}}'] = date('l',$book_time);
					$parse_data_arr['{{appointment.month}}'] = date('F',$book_time);
					$parse_data_arr['{{appointment.date}}'] = date('j',$book_time);
					$parse_data_arr['{{appointment.time}}'] = date('h:i A',$book_time);
					$parse_data_arr['{{appointment.address_1}}'] = $address_det_temp_p->address;
					$parse_data_arr['{{appointment.city}}'] = $address_det_temp_p->city;
					$parse_data_arr['{{appointment.state}}'] = $state_det_temp_p->name;
					$parse_data_arr['{{appointment.zip_code}}'] = $address_det_temp_p->zip;

					$parse_data_arr['{{patient.user_full_name}}'] = $user_det_temp->user_first_name." ".$user_det_temp->user_last_name;
					$parse_data_arr['{{patient.user_first_name}}'] = $user_det_temp->user_first_name;
					$parse_data_arr['{{patient.user_last_name}}'] = $user_det_temp->user_last_name;

					$parse_data_arr['{{patient.user_title}}'] = $user_det_temp->patient_name;
					$parse_data_arr['{{patient.user_mobile_number}}'] = isset( $user_det_temp->user_phone ) ? $user_det_temp->user_phone :  '';
					$parse_data_arr['{{patient.user_home_number}}'] = isset( $user_det_temp->user_phone ) ? $user_det_temp->user_phone :  '';
					$parse_data_arr['{{patient.user_work_number}}'] = isset( $user_det_temp->user_phone ) ? $user_det_temp->user_phone :  '';
					$parse_data_arr['{{patient.user_fax_number}}'] = isset( $user_det_temp->user_phone ) ? $user_det_temp->user_phone :  '';
					$parse_data_arr['{{patient.user_other_number}}'] = isset( $user_det_temp->user_phone ) ? $user_det_temp->user_phone :  '';
					$parse_data_arr['{{patient.user_email}}'] = $user_det_temp->patient_email;
					$parse_data_arr['{{patient.user_address_1}}'] = isset( $user_det_temp->user_address ) ? $user_det_temp->user_address :  '';
					$parse_data_arr['{{patient.user_address_2}}'] = isset( $user_det_temp->user_address ) ? $user_det_temp->user_address : '';
					$parse_data_arr['{{patient.user_address_3}}'] = isset( $user_det_temp->user_address ) ? $user_det_temp->user_address :  '';
					$parse_data_arr['{{patient.user_city}}'] = $user_det_temp->user_city;
					$parse_data_arr['{{patient.user_post_code}}'] = $user_det_temp->user_zip;
					$parse_data_arr['{{patient.user_state}}'] = $user_det_temp->user_state;
					$parse_data_arr['{{patient.user_country}}'] = isset( $user_det_temp->patient_country ) ? $user_det_temp->patient_country :  '';
					$parse_data_arr['{{patient.user_dateOfBirth}}'] = isset( $user_det_temp->patient_dob ) ? date('d-m-Y' ,strtotime( $user_det_temp->patient_dob ) ) : '';
					$parse_data_arr['{{patient.user_gender}}'] = Ucfirst( $user_det_temp->patient_sex );

					$parse_data_arr['{firstname}'] = $user_det_temp->user_first_name;
					$parse_data_arr['{user}'] = $doctor_det_temp->first_name." ".$doctor_det_temp->last_name;
					$parse_data_arr['{{doctor.full_name}}'] = $doctor_det_temp->full_name;
					$parse_data_arr['{{doctor.first_name}}'] = $doctor_det_temp->first_name;
					$parse_data_arr['{{doctor.last_name}}'] = $doctor_det_temp->last_name;
					$parse_data_arr['{{doctor.address_1}}'] = $doctor_det_temp->addr1;
					$parse_data_arr['{{doctor.city}}'] = $doctor_det_temp->city;

					//$parse_data_arr['{{doctor.state}}'] = $state_det_temp_d->name;

					$parse_data_arr['{{doctor.zip_code}}'] = $doctor_det_temp->zip;
					$parse_data_arr['{{doctor.phone}}'] = $doctor_det_temp->phone;
					$parse_data_arr['{{doctor.email}}'] = $doctor_det_temp->email;
					$parse_data_arr['{{doctor.parctise_name}}'] = $doctor_det_temp->practice;

					$message_body = $this->getTemplateDataParsing($parse_data_arr,$message_body);

				}else{
					$message_body = '<h2 style="color:#636363; margin:0px; padding-bottom:10px; padding-left:8px; padding-right:8px; font-size:13px; font-weight:bold;">Your Appointment has now been Confirmed.</h2>Your Appointment Time is '.$app_time.'.';
				}
				
				$to_email = $user_det_temp->user_email;
				$to_name = $user_det_temp->user_first_name." ".$user_det_temp->user_last_name;
				$to = array($to_email,$to_name);
				$from = array('info.doctor@gmail.com','eDoctorBook');
				//$subject = 'eDoctorBook - Appointment Confirmed.';
				$subject = isset( $email_template->subject ) ? $email_template->subject : 'eDoctorBook - Appointment Confirmed.';
				$message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
					<html xmlns="http://www.w3.org/1999/xhtml">
					<head>
					<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
					<title>Doctor Email Template</title>
					</head>
					<body style="padding:0px; margin:0px; font-size:12px; color:#545454; line-height:18px; font-family:Arial, Helvetica, sans-serif;">
					<div style="width:450px;  margin:5px auto; border:1px solid #c7c7c7; "> 
					<div style="text-align:left; margin-bottom:15px; -moz-box-shadow:0px 1px 1px 1px #E4E4E4; -webkit-box-shadow:0px 1px 1px 1px #E4E4E4;box-shadow:0px 1px 1px 1px #E4E4E4; background:#54cbc8; font-weight:bold;">
					<a href="'.$this->createAbsoluteUrl('site/index/').'"><img src="http://'.$_SERVER['HTTP_HOST'].Yii::app()->getHomeUrl().'/assets/images/small_logo.png" style="padding:3px;" alt="Doctor Appointment Book" /></a>
					&nbsp;
					</div>	
					<h1 style="text-align:center; display:block; font-weight:normal; margin:0px; padding-bottom:12px; font-size:19px; line-height:26px; color:#4c4c4c;">Thank you '.$to_name.' for using <br /> <font style="color:#6ad2cf">eDoctorBook</font>!</h1>

					<div style="min-height:200px; background:#fff; color:#bbb; margin:10px; padding-top:15px; font-size:12px;"><pre style="color:#000">'.$message_body.'</pre>
					<br>
					</div>
					<p style=" margin-top:10px; padding:8px 5px; color:#000; font-size:13px; margin-bottom:0px; font-weight:bold; line-height:18px; ">eDoctorBook Team</p>
					<a style="padding-left:5px; padding-right:5px; color:#000; font-size:13px; font-weight:bold; margin:0px; line-height:0px; text-decoration:none" href="">http://www.eDoctorBook.com</a>
					<div style="background:#54cbc8; padding-top:20px; margin-top:15px; padding-bottom:10px; text-align:center; color:#e2e2e2; font-size:14px;"></div>
					</div>
					</body>
					</html>';
				
			Helpers::mailsend($to,$from,$subject,$message);

			//================================================================================

				$NotificationSettings = DoctorNotificationSettings::model()->findAllByAttributes(array('cancel_notification'=>1,'status'=>1, 'doctor_id'=>$doctor_id_temp));
				$cancel_notification = 0;
				foreach($NotificationSettings as $NotificationSettings_key=>$NotificationSettings_val) {
					//==================================================================================================
					$cancel_notification = $NotificationSettings_val['cancel_notification'];
					//==================================================================================================
				}
				if($cancel_notification) {
					//==========================================================================================
					Yii::import('application.vendor.twilio.*');
					spl_autoload_unregister(array('YiiBase','autoload')); 
					include_once( dirname(__FILE__) . "/../vendor/twilio/Services/Twilio.php");
					spl_autoload_register(array('YiiBase', 'autoload'));
					
					 // Step 2: set our AccountSid and AuthToken from www.twilio.com/user/account

					$AccountSid = "AC74068ab1383071d146bebb5c905dbcb5";
					$AuthToken = "6ee3bc04ff855fefaefc68d9cd1bd778";
					$AuthPhone = "+16508259090";
					$client = new Services_Twilio($AccountSid, $AuthToken);
					$number = $user_det_temp->user_phone;

					$sms_message_body = 'Hi '.$to_name.', Your Appointment on '.date('l, F j - h:i A',$book_time).' has been Confirmed';
					$sms = $client->account->messages->sendMessage("$AuthPhone",$number,$sms_message_body);
					//==========================================================================

					$sql_doctor_sms_count='INSERT INTO da_doctor_sms_count (doctor_sms_count, doctor_id, patient_id, book_id, time, msg, status)VALUES(:doctor_sms_count, :doctor_id, :patient_id, :book_id, :time, :msg, :status)';
					$params_doctor_sms_count = array(
						"doctor_sms_count" => '',
						"doctor_id" => $doctor_det_temp->id,
						"patient_id" => $user_det_temp->id,
						"book_id" => $app_id,
						"time" => time(),
						"msg" => $sms_message_body,
						"status" => 1
					);

					$command_doctor_sms_count=$connection->createCommand($sql_doctor_sms_count);
					$command_doctor_sms_count->execute($params_doctor_sms_count);					
				}
		}

	}

	

	public function actionAppointmentCancel() {

		$app_id = $_REQUEST['app_id'];
		$mandatory_text = $_REQUEST['mandatory_text'];
		$connection = Yii::app()->db;
		$sql='UPDATE da_doctor_book SET confirm = :confirm,comments = :comments WHERE id ='.$app_id;
		$params = array(
			"confirm" => 2,
			"comments" => $mandatory_text
		);
		$command=$connection->createCommand($sql);
		$command->execute($params);

		$doctor_book_det_temp =DoctorBook::model()->find("id=\"$app_id\"");
		$user_det_temp =Patient::model()->find("id=\"$doctor_book_det_temp->patient_id\"");
		$doctor_id_temp = Yii::app()->session['logged_user_id'];
		$doctor_det_temp =Doctor::model()->find("id=\"$doctor_id_temp\"");
		$address_det_temp_p = DoctorAddress::model()->find("id=\"$doctor_book_det_temp->address_id\"");
		$state_det_temp_p = State::model()->find("short_code=\"$address_det_temp_p->state\"");
		$state_det_temp_d = State::model()->find("short_code=\"$doctor_det_temp->state\"");

		$book_time = $doctor_book_det_temp->book_time;
		if($user_det_temp->user_email != ''){
			$temp_id = 7;
			$email_template = EmailTemplate::model()->find("id=\"$temp_id\"");
				if(!empty($email_template) && $email_template->tempalte_body != ''){
					$message_body = $email_template->tempalte_body;


					$parse_data_arr = array();
					$parse_data_arr['{{appointment.actual_format}}'] = date('l, F j - h:i A',$book_time);
					$parse_data_arr['{{appointment.day}}'] = date('l',$book_time);
					$parse_data_arr['{{appointment.month}}'] = date('F',$book_time);
					$parse_data_arr['{{appointment.date}}'] = date('j',$book_time);
					$parse_data_arr['{{appointment.time}}'] = date('h:i A',$book_time);
					$parse_data_arr['{{appointment.address_1}}'] = $address_det_temp_p->address;
					$parse_data_arr['{{appointment.city}}'] = $address_det_temp_p->city;
					$parse_data_arr['{{appointment.state}}'] = $state_det_temp_p->name;
					$parse_data_arr['{{appointment.zip_code}}'] = $address_det_temp_p->zip;

					$parse_data_arr['{{patient.user_full_name}}'] = $user_det_temp->user_first_name." ".$user_det_temp->user_last_name;
					$parse_data_arr['{{patient.user_first_name}}'] = $user_det_temp->user_first_name;
					$parse_data_arr['{{patient.user_last_name}}'] = $user_det_temp->user_last_name;

					$parse_data_arr['{{patient.user_title}}'] = $user_det_temp->patient_name;
					$parse_data_arr['{{patient.user_mobile_number}}'] = isset( $user_det_temp->user_phone ) ? $user_det_temp->user_phone :  '';
					$parse_data_arr['{{patient.user_home_number}}'] = isset( $user_det_temp->user_phone ) ? $user_det_temp->user_phone :  '';
					$parse_data_arr['{{patient.user_work_number}}'] = isset( $user_det_temp->user_phone ) ? $user_det_temp->user_phone :  '';
					$parse_data_arr['{{patient.user_fax_number}}'] = isset( $user_det_temp->user_phone ) ? $user_det_temp->user_phone :  '';
					$parse_data_arr['{{patient.user_other_number}}'] = isset( $user_det_temp->user_phone ) ? $user_det_temp->user_phone :  '';
					$parse_data_arr['{{patient.user_email}}'] = $user_det_temp->patient_email;
					$parse_data_arr['{{patient.user_address_1}}'] = isset( $user_det_temp->user_address ) ? $user_det_temp->user_address :  '';
					$parse_data_arr['{{patient.user_address_2}}'] = isset( $user_det_temp->user_address ) ? $user_det_temp->user_address : '';
					$parse_data_arr['{{patient.user_address_3}}'] = isset( $user_det_temp->user_address ) ? $user_det_temp->user_address :  '';
					$parse_data_arr['{{patient.user_city}}'] = $user_det_temp->user_city;
					$parse_data_arr['{{patient.user_post_code}}'] = $user_det_temp->user_zip;
					$parse_data_arr['{{patient.user_state}}'] = $user_det_temp->user_state;
					$parse_data_arr['{{patient.user_country}}'] = isset( $user_det_temp->patient_country ) ? $user_det_temp->patient_country :  '';
					$parse_data_arr['{{patient.user_dateOfBirth}}'] = isset( $user_det_temp->patient_dob ) ? date('d-m-Y' ,strtotime( $user_det_temp->patient_dob ) ) : '';
					$parse_data_arr['{{patient.user_gender}}'] = Ucfirst( $user_det_temp->patient_sex );

					$parse_data_arr['{firstname}'] = $user_det_temp->user_first_name;
					$parse_data_arr['{user}'] = $doctor_det_temp->first_name." ".$doctor_det_temp->last_name;
					$parse_data_arr['{{doctor.full_name}}'] = $doctor_det_temp->full_name;
					$parse_data_arr['{{doctor.first_name}}'] = $doctor_det_temp->first_name;
					$parse_data_arr['{{doctor.last_name}}'] = $doctor_det_temp->last_name;
					$parse_data_arr['{{doctor.address_1}}'] = $doctor_det_temp->addr1;
					$parse_data_arr['{{doctor.city}}'] = $doctor_det_temp->city;
					$parse_data_arr['{{doctor.state}}'] = $state_det_temp_d->name;
					$parse_data_arr['{{doctor.zip_code}}'] = $doctor_det_temp->zip;
					$parse_data_arr['{{doctor.phone}}'] = $doctor_det_temp->phone;
					$parse_data_arr['{{doctor.email}}'] = $doctor_det_temp->email;
					$parse_data_arr['{{doctor.parctise_name}}'] = $doctor_det_temp->practice;

					$message_body = $this->getTemplateDataParsing($parse_data_arr,$message_body);
				}else{
					$message_body = '<h2 style="color:#636363; margin:0px; padding-bottom:10px; padding-left:8px; padding-right:8px; font-size:13px; font-weight:bold;">Your Appointment has now been Cancelled.</h2>';
				}

				$to_email = $user_det_temp->user_email;
				$to_name = $user_det_temp->user_first_name." ".$user_det_temp->user_last_name;
				$to = array($to_email,$to_name);
				$from = array('support@edoctorbook.com','eDoctorBook');
				//$subject = 'eDoctorBook - Appointment Cancelled.';
				$subject = isset( $email_template->subject ) ? $email_template->subject : 'eDoctorBook - Appointment Cancelled.';
				$message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
					<html xmlns="http://www.w3.org/1999/xhtml">
					<head>
					<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
					<title>Doctor Email Template</title>
					</head>
					<body style="padding:0px; margin:0px; font-size:12px; color:#545454; line-height:18px; font-family:Arial, Helvetica, sans-serif;">
					<div style="width:450px;  margin:5px auto; border:1px solid #c7c7c7; "> 
					<div style="text-align:left; margin-bottom:15px; -moz-box-shadow:0px 1px 1px 1px #E4E4E4; -webkit-box-shadow:0px 1px 1px 1px #E4E4E4;box-shadow:0px 1px 1px 1px #E4E4E4; background:#54cbc8; font-weight:bold;">
					<a href="'.$this->createAbsoluteUrl('site/index/').'"><img src="http://'.$_SERVER['HTTP_HOST'].Yii::app()->getHomeUrl().'/assets/images/small_logo.png" style="padding:3px;" alt="Doctor Appointment Book" /></a>
					&nbsp;
					</div>
					<h1 style="text-align:center; display:block; font-weight:normal; margin:0px; padding-bottom:12px; font-size:19px; line-height:26px; color:#4c4c4c;">Thank you '.$to_name.' for using <br /> <font style="color:#6ad2cf">eDoctorBook</font>!</h1>

					<div style="min-height:200px; background:#fff; color:#bbb; margin:10px; padding-top:15px; font-size:12px;"><pre style="color:#000">'.$message_body.'</pre>
					<br>
					</div>

					<p style=" margin-top:10px; padding:8px 5px; color:#000; font-size:13px; margin-bottom:0px; font-weight:bold; line-height:18px; ">eDoctorBook Team</p>
					<a style="padding-left:5px; padding-right:5px; color:#000; font-size:13px; font-weight:bold; margin:0px; line-height:0px; text-decoration:none" href="">http://www.eDoctorBook.com</a>
					<div style="background:#54cbc8; padding-top:20px; margin-top:15px; padding-bottom:10px; text-align:center; color:#e2e2e2; font-size:14px;"></div>
					</div>
					</body>
					</html>';

			Helpers::mailsend($to,$from,$subject,$message);

				//================================================================================

				$NotificationSettings = DoctorNotificationSettings::model()->findAllByAttributes(array('cancel_notification'=>1,'status'=>1, 'doctor_id'=>$doctor_id_temp));
				$cancel_notification = 0;
				foreach($NotificationSettings as $NotificationSettings_key=>$NotificationSettings_val) {
					//==================================================================================================
					$cancel_notification = $NotificationSettings_val['cancel_notification'];
					//==================================================================================================
				}

				if($cancel_notification) {
		
					Yii::import('application.vendor.twilio.*');
					spl_autoload_unregister(array('YiiBase','autoload')); 
					include_once( dirname(__FILE__) . "/../vendor/twilio/Services/Twilio.php");
					spl_autoload_register(array('YiiBase', 'autoload'));
					
					 // Step 2: set our AccountSid and AuthToken from www.twilio.com/user/account

					$AccountSid = "AC74068ab1383071d146bebb5c905dbcb5";
					$AuthToken = "6ee3bc04ff855fefaefc68d9cd1bd778";
					$AuthPhone = "+16508259090";
					$client = new Services_Twilio($AccountSid, $AuthToken);
					$number = $user_det_temp->user_phone;

					$sms_message_body = 'Hi '.$to_name.', Your Appointment on '.date('l, F j - h:i A',$book_time).' has now been Cancelled by '.$doctor_det_temp->first_name.' '.$doctor_det_temp->last_name;
					$sms = $client->account->messages->sendMessage("$AuthPhone",$number,$sms_message_body);

					//==========================================================================

					$sql_doctor_sms_count='INSERT INTO da_doctor_sms_count (doctor_sms_count, doctor_id, patient_id, book_id, time, msg, status)VALUES(:doctor_sms_count, :doctor_id, :patient_id, :book_id, :time, :msg, :status)';
					$params_doctor_sms_count = array(
						"doctor_sms_count" => '',
						"doctor_id" => $doctor_det_temp->id,
						"patient_id" => $user_det_temp->id,
						"book_id" => $app_id,
						"time" => time(),
						"msg" => $sms_message_body,
						"status" => 1
					);
					$command_doctor_sms_count=$connection->createCommand($sql_doctor_sms_count);
					$command_doctor_sms_count->execute($params_doctor_sms_count);					

				}

		}

	}

	

	public function actionAppointmentCancelPatient() {

		$app_id = $_REQUEST['app_id'];
		$mandatory_text = $_REQUEST['mandatory_text'];
		$connection = Yii::app()->db;
		$sql='UPDATE da_doctor_book SET confirm = :confirm,comments = :comments WHERE id ='.$app_id;
		$params = array(
			"confirm" => 2,
			"comments" => $mandatory_text
		);
		$command=$connection->createCommand($sql);
		$command->execute($params);

		$doctor_book_det_temp =DoctorBook::model()->find("id=\"$app_id\"");
		$user_det_temp =Patient::model()->find("id=\"$doctor_book_det_temp->patient_id\"");
		
		/*if(Yii::app()->session['logged_user_id'])

		{

			$doctor_id_temp = Yii::app()->session['logged_user_id'];

		}else{

			$doctor_id_temp = DoctorBook::model()->find("id=\"$doctor_id_temp\"");

		}*/

		$doctor_det_temp =Doctor::model()->find("id=\"$doctor_book_det_temp->doctor_id\"");
		$book_time = $doctor_book_det_temp->book_time;
		$address_det_temp_p = DoctorAddress::model()->find("id=\"$doctor_book_det_temp->address_id\"");
		$state_det_temp_p = State::model()->find("short_code=\"$address_det_temp_p->state\"");
		$state_det_temp_d = State::model()->find("short_code=\"$doctor_det_temp->state\"");

		if($user_det_temp->user_email != ''){
			$temp_id = 6;
			$email_template = EmailTemplate::model()->find("id=\"$temp_id\"");
				if(!empty($email_template) && $email_template->tempalte_body != ''){
					$message_body = $email_template->tempalte_body;
					//$message_body = $this->getTemplateDataParsing($parse_with,$parse_data,$message_body);
					$parse_data_arr = array();
					$parse_data_arr['{{appointment.actual_format}}'] = date('l, F j - h:i A',$book_time);
					$parse_data_arr['{{appointment.day}}'] = date('l',$book_time);
					$parse_data_arr['{{appointment.month}}'] = date('F',$book_time);
					$parse_data_arr['{{appointment.date}}'] = date('j',$book_time);
					$parse_data_arr['{{appointment.time}}'] = date('h:i A',$book_time);
					$parse_data_arr['{{appointment.address_1}}'] = $address_det_temp_p->address;
					$parse_data_arr['{{appointment.city}}'] = $address_det_temp_p->city;
					$parse_data_arr['{{appointment.state}}'] = $state_det_temp_p->name;
					$parse_data_arr['{{appointment.zip_code}}'] = $address_det_temp_p->zip;

					$parse_data_arr['{{patient.user_full_name}}'] = $user_det_temp->user_first_name." ".$user_det_temp->user_last_name;
					$parse_data_arr['{{patient.user_first_name}}'] = $user_det_temp->user_first_name;
					$parse_data_arr['{{patient.user_last_name}}'] = $user_det_temp->user_last_name;

					$parse_data_arr['{{patient.user_title}}'] = $user_det_temp->patient_name;
					$parse_data_arr['{{patient.user_mobile_number}}'] = isset( $user_det_temp->user_phone ) ? $user_det_temp->user_phone :  '';
					$parse_data_arr['{{patient.user_home_number}}'] = isset( $user_det_temp->user_phone ) ? $user_det_temp->user_phone :  '';
					$parse_data_arr['{{patient.user_work_number}}'] = isset( $user_det_temp->user_phone ) ? $user_det_temp->user_phone :  '';
					$parse_data_arr['{{patient.user_fax_number}}'] = isset( $user_det_temp->user_phone ) ? $user_det_temp->user_phone :  '';
					$parse_data_arr['{{patient.user_other_number}}'] = isset( $user_det_temp->user_phone ) ? $user_det_temp->user_phone :  '';
					$parse_data_arr['{{patient.user_email}}'] = $user_det_temp->patient_email;
					$parse_data_arr['{{patient.user_address_1}}'] = isset( $user_det_temp->user_address ) ? $user_det_temp->user_address :  '';
					$parse_data_arr['{{patient.user_address_2}}'] = isset( $user_det_temp->user_address ) ? $user_det_temp->user_address : '';
					$parse_data_arr['{{patient.user_address_3}}'] = isset( $user_det_temp->user_address ) ? $user_det_temp->user_address :  '';
					$parse_data_arr['{{patient.user_city}}'] = $user_det_temp->user_city;
					$parse_data_arr['{{patient.user_post_code}}'] = $user_det_temp->user_zip;
					$parse_data_arr['{{patient.user_state}}'] = $user_det_temp->user_state;
					$parse_data_arr['{{patient.user_country}}'] = isset( $user_det_temp->patient_country ) ? $user_det_temp->patient_country :  '';
					$parse_data_arr['{{patient.user_dateOfBirth}}'] = isset( $user_det_temp->patient_dob ) ? date('d-m-Y' ,strtotime( $user_det_temp->patient_dob ) ) : '';
					$parse_data_arr['{{patient.user_gender}}'] = Ucfirst( $user_det_temp->patient_sex );

					$parse_data_arr['{firstname}'] = $user_det_temp->user_first_name;

					$parse_data_arr['{user}'] = $doctor_det_temp->first_name." ".$doctor_det_temp->last_name;
					$parse_data_arr['{{doctor.full_name}}'] = $doctor_det_temp->full_name;
					$parse_data_arr['{{doctor.first_name}}'] = $doctor_det_temp->first_name;
					$parse_data_arr['{{doctor.last_name}}'] = $doctor_det_temp->last_name;
					$parse_data_arr['{{doctor.address_1}}'] = $doctor_det_temp->addr1;
					$parse_data_arr['{{doctor.city}}'] = $doctor_det_temp->city;
					$parse_data_arr['{{doctor.state}}'] = '';
					$parse_data_arr['{{doctor.zip_code}}'] = $doctor_det_temp->zip;
					$parse_data_arr['{{doctor.phone}}'] = $doctor_det_temp->phone;
					$parse_data_arr['{{doctor.email}}'] = $doctor_det_temp->email;
					$parse_data_arr['{{doctor.parctise_name}}'] = $doctor_det_temp->practice;
					
					$message_body = $this->getTemplateDataParsing($parse_data_arr,$message_body);

				}else{
					$message_body = '<h2 style="color:#636363; margin:0px; padding-bottom:10px; padding-left:8px; padding-right:8px; font-size:13px; font-weight:bold;">Your Appointment has now been Cancelled by You.</h2>';
				}
				$to_email = $user_det_temp->user_email;
				$to_name = $user_det_temp->user_first_name." ".$user_det_temp->user_last_name;
				$to = array($to_email,$to_name);
				$from = array('support@edoctorbook.com','eDoctorBook');
				//$subject = 'eDoctorBook - Appointment Cancelled.';
				$subject = isset( $email_template->subject ) ? $email_template->subject : 'eDoctorBook - Appointment Cancelled.';
				$message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

					<html xmlns="http://www.w3.org/1999/xhtml">

					<head>

					<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

					<title>Doctor Email Template</title>

					</head>

					<body style="padding:0px; margin:0px; font-size:12px; color:#545454; line-height:18px; font-family:Arial, Helvetica, sans-serif;">

					<div style="width:450px;  margin:5px auto; border:1px solid #c7c7c7; "> 

					<div style="text-align:left; margin-bottom:15px; -moz-box-shadow:0px 1px 1px 1px #E4E4E4; -webkit-box-shadow:0px 1px 1px 1px #E4E4E4;box-shadow:0px 1px 1px 1px #E4E4E4; background:#54cbc8; font-weight:bold;">

					<a href="'.$this->createAbsoluteUrl('site/index/').'"><img src="http://'.$_SERVER['HTTP_HOST'].Yii::app()->getHomeUrl().'/assets/images/small_logo.png" style="padding:3px;" alt="Doctor Appointment Book" /></a>

					&nbsp;

					</div>	



					<h1 style="text-align:center; display:block; font-weight:normal; margin:0px; padding-bottom:12px; font-size:19px; line-height:26px; color:#4c4c4c;">Thank you '.$to_name.' for using <br /> <font style="color:#6ad2cf">eDoctorBook</font>!</h1>



					<div style="min-height:200px; background:#fff; color:#bbb; margin:10px; padding-top:15px; font-size:12px;"><pre style="color:#000">'.$message_body.'</pre>

					<br>

					

					</div>

					<p style=" margin-top:10px; padding:8px 5px; color:#000; font-size:13px; margin-bottom:0px; font-weight:bold; line-height:18px; ">eDoctorBook Team</p>

					<a style="padding-left:5px; padding-right:5px; color:#000; font-size:13px; font-weight:bold; margin:0px; line-height:0px; text-decoration:none" href="">http://www.eDoctorBook.com</a>

					<div style="background:#54cbc8; padding-top:20px; margin-top:15px; padding-bottom:10px; text-align:center; color:#e2e2e2; font-size:14px;"></div>

					</div>



					</body>

					</html>';

				

			Helpers::mailsend($to,$from,$subject,$message);

			//================================================================================

				$NotificationSettings = DoctorNotificationSettings::model()->findAllByAttributes(array('cancel_notification'=>1,'status'=>1, 'doctor_id'=>$doctor_book_det_temp->doctor_id));
				$cancel_notification = 0;

				foreach($NotificationSettings as $NotificationSettings_key=>$NotificationSettings_val) {

					//==================================================================================================

					$cancel_notification = $NotificationSettings_val['cancel_notification'];

					//==================================================================================================

				}

				if($cancel_notification)

				{

					$temp_id = 12;

					$email_template = EmailTemplate::model()->find("id=\"$temp_id\"");

					if(!empty($email_template) && $email_template->tempalte_body != ''){

						$message_body = $email_template->tempalte_body;

						//$message_body = $this->getTemplateDataParsing($parse_with,$parse_data,$message_body);

						$parse_data_arr = array();

						$parse_data_arr['{{appointment.actual_format}}'] = date('l, F j - h:i A',$book_time);

						$parse_data_arr['{{appointment.day}}'] = date('l',$book_time);

						$parse_data_arr['{{appointment.month}}'] = date('F',$book_time);

						$parse_data_arr['{{appointment.date}}'] = date('j',$book_time);

						$parse_data_arr['{{appointment.time}}'] = date('h:i A',$book_time);

						$parse_data_arr['{{appointment.address_1}}'] = $address_det_temp_p->address;

						$parse_data_arr['{{appointment.city}}'] = $address_det_temp_p->city;

						$parse_data_arr['{{appointment.state}}'] = $state_det_temp_p->name;

						$parse_data_arr['{{appointment.zip_code}}'] = $address_det_temp_p->zip;

							

						$parse_data_arr['{{patient.user_full_name}}'] = $user_det_temp->user_first_name." ".$user_det_temp->user_last_name;

						$parse_data_arr['{{patient.user_first_name}}'] = $user_det_temp->user_first_name;

						$parse_data_arr['{{patient.user_last_name}}'] = $user_det_temp->user_last_name;

							

						$parse_data_arr['{{patient.user_title}}'] = $user_det_temp->patient_name;

						$parse_data_arr['{{patient.user_mobile_number}}'] = isset( $user_det_temp->user_phone ) ? $user_det_temp->user_phone :  '';

						$parse_data_arr['{{patient.user_home_number}}'] = isset( $user_det_temp->user_phone ) ? $user_det_temp->user_phone :  '';

						$parse_data_arr['{{patient.user_work_number}}'] = isset( $user_det_temp->user_phone ) ? $user_det_temp->user_phone :  '';

						$parse_data_arr['{{patient.user_fax_number}}'] = isset( $user_det_temp->user_phone ) ? $user_det_temp->user_phone :  '';

						$parse_data_arr['{{patient.user_other_number}}'] = isset( $user_det_temp->user_phone ) ? $user_det_temp->user_phone :  '';

						$parse_data_arr['{{patient.user_email}}'] = $user_det_temp->patient_email;

						$parse_data_arr['{{patient.user_address_1}}'] = isset( $user_det_temp->user_address ) ? $user_det_temp->user_address :  '';

						$parse_data_arr['{{patient.user_address_2}}'] = isset( $user_det_temp->user_address ) ? $user_det_temp->user_address : '';

						$parse_data_arr['{{patient.user_address_3}}'] = isset( $user_det_temp->user_address ) ? $user_det_temp->user_address :  '';

						$parse_data_arr['{{patient.user_city}}'] = $user_det_temp->user_city;

						$parse_data_arr['{{patient.user_post_code}}'] = $user_det_temp->user_zip;

						$parse_data_arr['{{patient.user_state}}'] = $user_det_temp->user_state;

						$parse_data_arr['{{patient.user_country}}'] = isset( $user_det_temp->patient_country ) ? $user_det_temp->patient_country :  '';

						$parse_data_arr['{{patient.user_dateOfBirth}}'] = isset( $user_det_temp->patient_dob ) ? date('d-m-Y' ,strtotime( $user_det_temp->patient_dob ) ) : '';

						$parse_data_arr['{{patient.user_gender}}'] = Ucfirst( $user_det_temp->patient_sex );

							

						$parse_data_arr['{firstname}'] = $user_det_temp->user_first_name;

							

						$parse_data_arr['{user}'] = $doctor_det_temp->first_name." ".$doctor_det_temp->last_name;

						$parse_data_arr['{{doctor.full_name}}'] = $doctor_det_temp->full_name;

						$parse_data_arr['{{doctor.first_name}}'] = $doctor_det_temp->first_name;

						$parse_data_arr['{{doctor.last_name}}'] = $doctor_det_temp->last_name;

						$parse_data_arr['{{doctor.address_1}}'] = $doctor_det_temp->addr1;

						$parse_data_arr['{{doctor.city}}'] = $doctor_det_temp->city;

						$parse_data_arr['{{doctor.state}}'] = '';

						$parse_data_arr['{{doctor.zip_code}}'] = $doctor_det_temp->zip;

						$parse_data_arr['{{doctor.phone}}'] = $doctor_det_temp->phone;

						$parse_data_arr['{{doctor.email}}'] = $doctor_det_temp->email;

						$parse_data_arr['{{doctor.parctise_name}}'] = $doctor_det_temp->practice;

						

						$message_body = $this->getTemplateDataParsing($parse_data_arr,$message_body);

					}else{

						$message_body = '<h2 style="color:#636363; margin:0px; padding-bottom:10px; padding-left:8px; padding-right:8px; font-size:13px; font-weight:bold;">Your Appointment has now been Cancelled by You.</h2>';

					}

					

					$to_email =  $NotificationSettings_val['alert_email'];

					$to_name = $doctor_det_temp->full_name;

					$to = array($to_email,$to_name);

					$from = array('support@edoctorbook.com','eDoctorBook');

					//$subject = 'eDoctorBook - Appointment Cancelled.';

					$subject = isset( $email_template->subject ) ? $email_template->subject : 'eDoctorBook - Appointment Cancelled.';

					$message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

					<html xmlns="http://www.w3.org/1999/xhtml">

					<head>

					<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

					<title>Doctor Email Template</title>

					</head>

					<body style="padding:0px; margin:0px; font-size:12px; color:#545454; line-height:18px; font-family:Arial, Helvetica, sans-serif;">

					<div style="width:450px;  margin:5px auto; border:1px solid #c7c7c7; ">

					<div style="text-align:left; margin-bottom:15px; -moz-box-shadow:0px 1px 1px 1px #E4E4E4; -webkit-box-shadow:0px 1px 1px 1px #E4E4E4;box-shadow:0px 1px 1px 1px #E4E4E4; background:#54cbc8; font-weight:bold;">

					<a href="'.$this->createAbsoluteUrl('site/index/').'"><img src="http://'.$_SERVER['HTTP_HOST'].Yii::app()->getHomeUrl().'/assets/images/small_logo.png" style="padding:3px;" alt="Doctor Appointment Book" /></a>

					&nbsp;

					</div>

					

					<h1 style="text-align:center; display:block; font-weight:normal; margin:0px; padding-bottom:12px; font-size:19px; line-height:26px; color:#4c4c4c;">Thank you '.$to_name.' for using <br /> <font style="color:#6ad2cf">eDoctorBook</font>!</h1>

					

					<div style="min-height:200px; background:#fff; color:#bbb; margin:10px; padding-top:15px; font-size:12px;"><pre style="color:#000">'.$message_body.'</pre>

					<br>

			

					</div>

					<p style=" margin-top:10px; padding:8px 5px; color:#000; font-size:13px; margin-bottom:0px; font-weight:bold; line-height:18px; ">eDoctorBook Team</p>

					<a style="padding-left:5px; padding-right:5px; color:#000; font-size:13px; font-weight:bold; margin:0px; line-height:0px; text-decoration:none" href="">http://www.eDoctorBook.com</a>

					<div style="background:#54cbc8; padding-top:20px; margin-top:15px; padding-bottom:10px; text-align:center; color:#e2e2e2; font-size:14px;"></div>

					</div>

					

					</body>

					</html>';

					

					Helpers::mailsend($to,$from,$subject,$message);

					

					

					

					//==========================================================================================

					Yii::import('application.vendor.twilio.*');

					spl_autoload_unregister(array('YiiBase','autoload')); 

					include_once( dirname(__FILE__) . "/../vendor/twilio/Services/Twilio.php");

					spl_autoload_register(array('YiiBase', 'autoload'));



					

					 // Step 2: set our AccountSid and AuthToken from www.twilio.com/user/account

					$AccountSid = "AC74068ab1383071d146bebb5c905dbcb5";

					$AuthToken = "6ee3bc04ff855fefaefc68d9cd1bd778";

					$AuthPhone = "+16508259090";

					$client = new Services_Twilio($AccountSid, $AuthToken);

					$number = $user_det_temp->user_phone;



					$sms_message_body = 'Hi '.$to_name.', Your Appointment on '.date('l, F j - h:i A',$book_time).' has now been Cancelled By You';

					$sms = $client->account->messages->sendMessage("$AuthPhone",$number,$sms_message_body);

					//==========================================================================

					$sql_doctor_sms_count='INSERT INTO da_doctor_sms_count (doctor_sms_count, doctor_id, patient_id, book_id, time, msg, status)VALUES(:doctor_sms_count, :doctor_id, :patient_id, :book_id, :time, :msg, :status)';

					$params_doctor_sms_count = array(

						"doctor_sms_count" => '',

						"doctor_id" => $doctor_det_temp->id,

						"patient_id" => $user_det_temp->id,

						"book_id" => $app_id,

						"time" => time(),

						"msg" => $sms_message_body,

						"status" => 1

					);

					$command_doctor_sms_count=$connection->createCommand($sql_doctor_sms_count);

					$command_doctor_sms_count->execute($params_doctor_sms_count);					

					//==========================================================================



					//==========================================================================================

				}

				//================================================================================

		}

	}

	

	public function actionProfile($id = "")

	{

		/*$dataProvider=new CActiveDataProvider('Doctor');

		$this->render('index',array(

			'dataProvider'=>$dataProvider,

		));*/

		if(!Yii::app()->session['logged_in'])

			$this->redirect(array('site/index'));

		

			$model = new Doctor();

			$model->id = $id;

			$usercriteria =Doctor::model()->find("id=\"$model->id\"");

			

			$model->first_name=$usercriteria->first_name;

			$model->last_name=$usercriteria->last_name;

			$model->username=$usercriteria->username;

			$model->email=$usercriteria->email;

			$model->phone=$usercriteria->phone;

			$model->zip=$usercriteria->zip;

			$model->speciality=$usercriteria->speciality;

			$model->addr1=$usercriteria->addr1;

			$model->addr2=$usercriteria->addr2;

			$model->state=$usercriteria->state;

			$model->country=$usercriteria->country;

			$model->gender=$usercriteria->gender;

			$model->birth_date=$usercriteria->birth_date;

			$model->title=$usercriteria->title;

			$model->comments=$usercriteria->comments;

			$model->degree=$usercriteria->degree;

			$model->education=$usercriteria->education;

			$model->residency_training=$usercriteria->residency_training;

			$model->hospital_affiliations=$usercriteria->hospital_affiliations;

			$model->board_certifications=$usercriteria->board_certifications;

			$model->awards_publications=$usercriteria->awards_publications;

			$model->languages_spoken=$usercriteria->languages_spoken;

			$model->insurances_accepted=$usercriteria->insurances_accepted;

			

			$sql='select * FROM da_speciality WHERE status="1"';

			$connection = Yii::app()->db;

			$command = $connection->createCommand($sql);

			$user_speciality = $command->queryAll();

			$user_speciality_res = array();

			foreach ($user_speciality as $key=>$val) {

				$user_speciality_res[$val['id']] = $val['speciality'];

			}

			

			$sql_doctor_speciality='select * FROM da_doctor_speciality WHERE status=1 and doctor_id='.$id;

			$command_doctor_speciality = $connection->createCommand($sql_doctor_speciality);

			$user_doctor_speciality = $command_doctor_speciality->queryAll();

			//echo '<pre>';print_r($user_speciality_res);

			$user_address =DoctorAddress::model()->findAll("doctor_id=$model->id");

			

		$this->render('_view',array(

			//'dataProvider'=>$usercriteria,

			'model' => $model,'address_list' => $user_address,'user_speciality' => $user_speciality_res,'doctor_speciality' => $user_doctor_speciality,

		));

	}

	

	public function actionEditProfileBackUp($id = "")

	{

		/*$dataProvider=new CActiveDataProvider('Doctor');

		$this->render('index',array(

			'dataProvider'=>$dataProvider,

		));*/

		if(!Yii::app()->session['logged_in'])

			$this->redirect(array('site/index'));

		

		if(Yii::app()->getRequest()->isPostRequest){

			//echo "<pre>";print_r($_REQUEST);die;

			//echo $id."jana";die;

			$model = Doctor::model()->findByPk($id);

			$prv_img_path = $model->image;

			$model->attributes=Yii::app()->getRequest()->getPost('Doctor');

			$model->speciality = Yii::app()->getRequest()->getPost('speciality');

			$model->status = 1;

			$model->addr1 = $_REQUEST['Doctor']['addr1'];

			$model->addr2 = $_REQUEST['Doctor']['addr2'];

			$model->state = $_REQUEST['Doctor']['state'];

			$model->country = $_REQUEST['Doctor']['country'];

			$model->gender=Yii::app()->getRequest()->getPost('gender');

			$model->birth_date=$_REQUEST['Doctor']['birth_date'];

			$model->title=Yii::app()->getRequest()->getPost('title');

			$model->comments=$_REQUEST['Doctor']['comments'];

			$model->degree=Yii::app()->getRequest()->getPost('degree');

			$model->education=$_REQUEST['Doctor']['education'];

			$model->residency_training=$_REQUEST['Doctor']['residency_training'];

			$model->hospital_affiliations = $_REQUEST['Doctor']['hospital_affiliations'];

			$model->board_certifications= $_REQUEST['Doctor']['board_certifications'];

			$model->awards_publications= $_REQUEST['Doctor']['awards_publications'];

			$model->languages_spoken= $_REQUEST['Doctor']['languages_spoken'];

			$model->insurances_accepted= $_REQUEST['Doctor']['insurances_accepted'];

			//print_r($_REQUEST['Doctor']);print_r($model->attributes);die;

			$sql='select * FROM da_speciality WHERE status="1"';

			$connection = Yii::app()->db;

			$command = $connection->createCommand($sql);

			$user_speciality = $command->queryAll();

			$user_speciality_res = array();

			foreach ($user_speciality as $key=>$val) {

				$user_speciality_res[$val['id']] = $val['speciality'];

			}

			

			$uniqid = uniqid();  // generate random number between 0-9999

            

            $uploadedFile=CUploadedFile::getInstance($model,'image');

            $fileName = "{$uniqid}-{$uploadedFile}";  // random number + file name

			if(!empty($uploadedFile))

            	$model->image = $fileName;

			

			if($model->save()){

				$this->removeCaptchaSession();

				$image_path = Yii::app()->basePath.'/../assets/upload/doctor/'.$id."/";

				if(!empty($uploadedFile)){

				if(!file_exists($image_path))

					mkdir($image_path, 0777, true);

				$uploadedFile->saveAs($image_path.$fileName);  // image will uplode to rootDirectory/banner/

				

				$previous_path = getcwd().'/assets/upload/doctor/'.$id.'/'.$prv_img_path;

				if($prv_img_path != "" && file_exists($previous_path))

					unlink($previous_path);

				}

				//$this->redirect(array('editProfile'));

				Yii::app()->user->setFlash('editProfile','Your Profile Updated Sucessfully.');

				$this->refresh();

			}

		} else{

			$this->removeCaptchaSession();

			$model = new Doctor();

			$model->id = $id;

			$usercriteria =Doctor::model()->find("id=\"$model->id\"");

			

			$model->first_name=$usercriteria->first_name;

			$model->last_name=$usercriteria->last_name;

			$model->username=$usercriteria->username;

			$model->email=$usercriteria->email;

			$model->phone=$usercriteria->phone;

			$model->zip=$usercriteria->zip;

			$model->speciality=$usercriteria->speciality;

			$model->addr1=$usercriteria->addr1;

			$model->addr2=$usercriteria->addr2;

			$model->state=$usercriteria->state;

			$model->country=$usercriteria->country;

			$model->gender=$usercriteria->gender;

			$model->image=$usercriteria->image;

			if($usercriteria->birth_date == "" || $usercriteria->birth_date == '0000-00-00')

				$model->birth_date = "";

			else

				$model->birth_date=$usercriteria->birth_date;

			$model->title=$usercriteria->title;

			$model->comments=$usercriteria->comments;

			$model->degree=$usercriteria->degree;

			$model->education=$usercriteria->education;

			$model->residency_training=$usercriteria->residency_training;

			$model->hospital_affiliations=$usercriteria->hospital_affiliations;

			$model->board_certifications=$usercriteria->board_certifications;

			$model->awards_publications=$usercriteria->awards_publications;

			$model->languages_spoken=$usercriteria->languages_spoken;

			$model->insurances_accepted=$usercriteria->insurances_accepted;

			$model->date_created=$usercriteria->date_created;

			

			$sql='select * FROM da_speciality WHERE status="1"';

			$connection = Yii::app()->db;

			$command = $connection->createCommand($sql);

			$user_speciality = $command->queryAll();

			$user_speciality_res = array();

			foreach ($user_speciality as $key=>$val) {

				$user_speciality_res[$val['id']] = $val['speciality'];

			}

		}

		$this->render('edit_my_account',array(

			//'dataProvider'=>$usercriteria,

			'model' => $model,'user_speciality' => $user_speciality_res,

		));

	}

	

	public function actionEditProfileAdmin($id = "")

	{

		if(!Yii::app()->session['yiiadmin__id'])

			$this->redirect(array('site/index'));

			

		$yiiadmin__id = Yii::app()->session['yiiadmin__id'];

		$yiiadmin__name = Yii::app()->session['yiiadmin__name'];

		//$yiiadmin__id = 'yiier';

		//echo $id;

		$login_by = $_REQUEST['login_by'];

		$model_name = $_REQUEST['model_name'];

		$model = Doctor::model()->findByPk($id);

		Yii::app()->session['logged_in'] = true;

		Yii::app()->session['logged_user_email'] = $model->username;

		Yii::app()->session['logged_user_id'] = $model->id;

		Yii::app()->session['logged_user_type'] = 'doctor';

		$this->redirect(array('doctor/editProfile/'.$id));

	}

	public function actionEditProfile($id = "")

	{

		/*$dataProvider=new CActiveDataProvider('Doctor');

		$this->render('index',array(

			'dataProvider'=>$dataProvider,

		));*/

		if(!Yii::app()->session['logged_in'])

			$this->redirect(array('site/index'));

		if($id!=Yii::app()->session['logged_user_id'])

			$this->redirect(array('doctor/editProfile/'.Yii::app()->session['logged_user_id']));

		$connection = Yii::app()->db;

		if(Yii::app()->getRequest()->isPostRequest){

			//echo "<pre>";print_r($_REQUEST);die;

			//echo $id."jana";die;

			$model = Doctor::model()->findByPk($id);

			$model->scenario = 'edit_profile';

			$prv_img_path = $model->image;

			$model->attributes=Yii::app()->getRequest()->getPost('Doctor');

			$model->speciality = Yii::app()->getRequest()->getPost('speciality');

			//$model->status = 1;

			if(Yii::app()->session['yiiadmin__id']){

				if(Yii::app()->getRequest()->getPost('status')=='off')

					$model->status = 0;

				else

					$model->status = 1;

				if(Yii::app()->getRequest()->getPost('lock_profile')==1)

			 		$model->lock_profile = Yii::app()->getRequest()->getPost('lock_profile');

				else

					$model->lock_profile = 0;

			}

			/*$model->addr1 = $_REQUEST['Doctor']['addr1'];

			$model->addr2 = $_REQUEST['Doctor']['addr2'];

			$model->state = $_REQUEST['Doctor']['state'];

			$model->country = $_REQUEST['Doctor']['country'];*/

			$model->gender=Yii::app()->getRequest()->getPost('gender');

			//$model->birth_date=$_REQUEST['Doctor']['birth_date'];

			$model->birth_date=$_REQUEST['yy']."-".$_REQUEST['mm']."-".$_REQUEST['dd'];

			if($model->birth_date == "" || $model->birth_date == '0000-00-00'){

				$model->birth_date = "";

				$birth_date['mm'] = "";

				$birth_date['dd'] = "";

				$birth_date['yy'] = "";

			}else{

				$birth_date_arr =explode("-",$model->birth_date);

				$birth_date['mm'] = $birth_date_arr[1];

				$birth_date['dd'] = $birth_date_arr[2];

				$birth_date['yy'] = $birth_date_arr[0];

			}

			$model->title=Yii::app()->getRequest()->getPost('title');

			$model->comments=$_REQUEST['Doctor']['comments'];

			$model->degree=Yii::app()->getRequest()->getPost('degree');

			//$model->education=$_REQUEST['Doctor']['education'];

			$model->npi_no=$_REQUEST['Doctor']['npi_no'];

			$model->residency_training=$_REQUEST['Doctor']['residency_training'];

			$model->hospital_affiliations = $_REQUEST['Doctor']['hospital_affiliations'];

			$model->board_certifications= $_REQUEST['Doctor']['board_certifications'];

			$model->awards_publications= $_REQUEST['Doctor']['awards_publications'];

			$model->languages_spoken= $_REQUEST['languages_spoken'];

			$model->medical_school= $_REQUEST['Doctor']['medical_school'];

			$model->medical_school_year= $_REQUEST['medical_school_year'];

			$model->residency_training_year= $_REQUEST['residency_training_year'];

			$model->visit_price= $_REQUEST['Doctor']['visit_price'];

			$model->visit_duration= $_REQUEST['Doctor']['visit_duration'];

			//$model->insurances_accepted= $_REQUEST['Doctor']['insurances_accepted'];

			//print_r($_REQUEST['Doctor']);print_r($model->attributes);die;

			$sql='select * FROM da_speciality WHERE status="1"';

			$connection = Yii::app()->db;

			$command = $connection->createCommand($sql);

			$user_speciality = $command->queryAll();

			$user_speciality_res = array();

			foreach ($user_speciality as $key=>$val) {

				$user_speciality_res[$val['id']] = $val['speciality'];

			}

			

			$uniqid = uniqid();  // generate random number between 0-9999

            

            $uploadedFile=CUploadedFile::getInstance($model,'image');

            $fileName = "{$uniqid}-{$uploadedFile}";  // random number + file name

			if(!empty($uploadedFile))

            	$model->image = $fileName;

			

			if($model->save()){

				Yii::app()->session['google_account'] = $_REQUEST['Doctor']['google_account'];

				$this->removeCaptchaSession();

				$image_path = Yii::app()->basePath.'/../assets/upload/doctor/'.$id."/";

				if(!empty($uploadedFile)){

				if(!file_exists($image_path))

					mkdir($image_path, 0777, true);

				$uploadedFile->saveAs($image_path.$fileName);  // image will uplode to rootDirectory/banner/

				

				$previous_path = getcwd().'/assets/upload/doctor/'.$id.'/'.$prv_img_path;

				if($prv_img_path != "" && file_exists($previous_path))

					unlink($previous_path);

				}

				//$this->redirect(array('editProfile'));

				Yii::app()->user->setFlash('editProfile','Your Profile Updated Sucessfully.');

				$this->refresh();

			}

		} else{

			$this->removeCaptchaSession();

			$model = new Doctor();

			$model->id = $id;

			$usercriteria =Doctor::model()->find("id=\"$model->id\"");

			$model->scenario = 'edit_profile';

			$model->first_name=$usercriteria->first_name;

			$model->last_name=$usercriteria->last_name;

			$model->username=$usercriteria->username;

			$model->email=$usercriteria->email;

			$model->google_account=$usercriteria->google_account;

			$model->phone=$usercriteria->phone;

			$model->zip=$usercriteria->zip;

			$model->speciality=$usercriteria->speciality;

			$model->addr1=$usercriteria->addr1;

			$model->addr2=$usercriteria->addr2;

			$model->state=$usercriteria->state;

			$model->country=$usercriteria->country;

			$model->gender=$usercriteria->gender;

			$model->image=$usercriteria->image;

			$model->visit_price=$usercriteria->visit_price;

			$model->visit_duration=$usercriteria->visit_duration;

			$model->status = $usercriteria->status;

			$model->lock_profile = $usercriteria->lock_profile;

			if($usercriteria->birth_date == "" || $usercriteria->birth_date == '0000-00-00'){

				$model->birth_date = "";

				$birth_date['mm'] = "";

				$birth_date['dd'] = "";

				$birth_date['yy'] = "";

			}else{

				$model->birth_date=$usercriteria->birth_date;

				$birth_date_arr =explode("-",$model->birth_date);

				$birth_date['mm'] = $birth_date_arr[1];

				$birth_date['dd'] = $birth_date_arr[2];

				$birth_date['yy'] = $birth_date_arr[0];

			}

			$model->title=$usercriteria->title;

			$model->comments=$usercriteria->comments;

			$model->degree=$usercriteria->degree;

			$model->education=$usercriteria->education;

			$model->residency_training=$usercriteria->residency_training;

			$model->hospital_affiliations=$usercriteria->hospital_affiliations;

			$model->board_certifications=$usercriteria->board_certifications;

			$model->awards_publications=$usercriteria->awards_publications;

			$model->languages_spoken=$usercriteria->languages_spoken;

			$model->insurances_accepted=$usercriteria->insurances_accepted;

			$model->date_created=$usercriteria->date_created;

			$model->medical_school=$usercriteria->medical_school;

			$model->medical_school_year=$usercriteria->medical_school_year;

			$model->residency_training_year=$usercriteria->residency_training_year;

			$model->npi_no=$usercriteria->npi_no;

			

			$sql='select * FROM da_speciality WHERE status="1"';

			//$connection = Yii::app()->db;

			$command = $connection->createCommand($sql);

			$user_speciality = $command->queryAll();

			$user_speciality_res = array();

			foreach ($user_speciality as $key=>$val) {

				$user_speciality_res[$val['id']] = $val['speciality'];

			}

		}

		

		$start_year = date('Y')-70;

		//$end_year = date('Y')-20;

		$end_year = date('Y');

		$yearOptions = array();

		$count_year = $start_year;

		for($i=0;$i<=($end_year-$start_year);$i++){

			$yearOptions[$count_year]=$count_year;

			$count_year++;

		}

		

		$sql_doctor_speciality='select * FROM da_doctor_speciality WHERE status=1 and doctor_id='.$id;

		$command_doctor_speciality = $connection->createCommand($sql_doctor_speciality);

		$user_doctor_speciality = $command_doctor_speciality->queryAll();

		

		$default_data_address = DoctorAddress::model()->findByAttributes(array('doctor_id'=>$model->id,'status'=>1,'default_status'=>1));

		

		$condition = 'status = "1" and doctor_id =' . $id;

		$criteria = new CDbCriteria(array(

			'condition' => $condition,

        	'order' => 'id DESC'

		));

		

		$count=DoctorAddress::model()->count($criteria);

		$pages=new CPagination($count);

	

		// results per page

		$pages->pageSize=10;

		$pages->applyLimit($criteria);

		

		$data_address =DoctorAddress::model()->findAll($criteria);

		

		$condition_video = 'status = "1" and doctor_id =' . $id;

		$criteria_video = new CDbCriteria(array(

			'condition' => $condition_video,

        	'order' => 'id DESC'

		));

		$data_video =DoctorVideo::model()->findAll($criteria_video);

		

		$sql_language='select * FROM da_language WHERE status="1"';

		$command_language = $connection->createCommand($sql_language);

		$user_language = $command_language->queryAll();

		$user_language_res = array();

		foreach ($user_language as $key=>$val) {

			$user_language_res[$val['id']] = $val['language'];

		}

		

		$sql_multy_language='select * FROM da_doctor_language WHERE status="1" and doctor_id="'.$id.'"';

		$command_multy_language = $connection->createCommand($sql_multy_language);

		$user_multy_language = $command_multy_language->queryAll();

		$user_selected_language = array();

		foreach ($user_multy_language as $key=>$val) {

			$user_selected_language[] = $val['language_id'];

		}

		

		$sql_speciality='select * FROM da_speciality WHERE status="1"';

		$command_speciality = $connection->createCommand($sql_speciality);

		$user_speciality = $command_speciality->queryAll();

		$user_speciality_res = array();

		foreach ($user_speciality as $key=>$val) {

			$user_speciality_res[$val['id']] = $val['speciality'];

		}

		

		$sql_multy_speciality='select * FROM da_doctor_speciality WHERE status="1" and doctor_id="'.$id.'"';

		$command_multy_speciality = $connection->createCommand($sql_multy_speciality);

		$user_multy_speciality = $command_multy_speciality->queryAll();

		$user_selected_speciality = array();

		foreach ($user_multy_speciality as $key=>$val) {

			$user_selected_speciality[] = $val['speciality_id'];

		}

		

		$sql_condition='select * FROM da_condition WHERE status="1"';

		$command_condition = $connection->createCommand($sql_condition);

		$user_condition = $command_condition->queryAll();

		$user_condition_res = array();

		foreach ($user_condition as $key=>$val) {

			$user_condition_res[$val['id']] = $val['condition'];

		}

		

		$sql_multy_condition='select * FROM da_doctor_condition WHERE status="1" and doctor_id="'.$id.'"';

		$command_multy_condition = $connection->createCommand($sql_multy_condition);

		$user_multy_condition = $command_multy_condition->queryAll();

		$user_selected_condition = array();

		foreach ($user_multy_condition as $key=>$val) {

			$user_selected_condition[] = $val['condition_id'];

		}

		

		//$implode_cond = implode(',',array_keys($user_condition_res));

		$sql_procedure='select * FROM da_procedure WHERE status="1"';

		$command_procedure = $connection->createCommand($sql_procedure);

		$user_procedure = $command_procedure->queryAll();

		$user_procedure_res = array();

		foreach ($user_procedure as $key=>$val) {

			$user_procedure_res[$val['id']] = $val['procedure'];

		}

		

		$sql_multy_procedure='select * FROM da_doctor_procedure WHERE status="1" and doctor_id="'.$id.'"';

		$command_multy_procedure = $connection->createCommand($sql_multy_procedure);

		$user_multy_procedure = $command_multy_procedure->queryAll();

		$user_selected_procedure = array();

		foreach ($user_multy_procedure as $key=>$val) {

			$user_selected_procedure[] = $val['procedure_id'];

		}

		

		$sql_insurance='select * FROM da_insurance WHERE status="1"';

		$command_insurance = $connection->createCommand($sql_insurance);

		$user_insurance = $command_insurance->queryAll();

		$user_insurance_res = array();

		foreach ($user_insurance as $key=>$val) {

			$user_insurance_res[$val['id']] = $val['insurance'];

		}

		

		$sql_multy_insurance='select * FROM da_doctor_insurance WHERE status="1" and doctor_id="'.$id.'"';

		$command_multy_insurance = $connection->createCommand($sql_multy_insurance);

		$user_multy_insurance = $command_multy_insurance->queryAll();

		$user_selected_insurance = array();

		foreach ($user_multy_insurance as $key=>$val) {

			$user_selected_insurance[] = $val['insurance_id'];

		}

		$user_insurance_plan_res = array();

		$user_insurance_plan_res_ins = array();

		$user_selected_insurance_plan_ins = array();

		$user_selected_insurance_plan_inc_name = array();

		foreach ($user_selected_insurance as $insurance_key=>$insurance_val) {

			$sql_insurance_plan='select * FROM da_insurance_plan WHERE status="1" and insurance_id="'.$insurance_val.'"';

			$command_insurance_plan = $connection->createCommand($sql_insurance_plan);

			$user_insurance_plan = $command_insurance_plan->queryAll();

			$user_insurance_plan_res = array();

			foreach ($user_insurance_plan as $key=>$val) {

				$user_insurance_plan_res[$val['id']] = $val['plan'];

			}

			$user_insurance_plan_res_ins[$insurance_val] = $user_insurance_plan_res;

			

			$sql_multy_insurance_plan='select * FROM da_doctor_insurance_plan WHERE status="1" and doctor_id="'.$id.'" and insurance_id="'.$insurance_val.'"';

			$command_multy_insurance_plan = $connection->createCommand($sql_multy_insurance_plan);

			$user_multy_insurance_plan = $command_multy_insurance_plan->queryAll();

			$user_selected_insurance_plan = array();

			$user_selected_insurance_plan_name = array();

			foreach ($user_multy_insurance_plan as $key=>$val) {

				$user_selected_insurance_plan[] = $val['plan_id'];

				$user_selected_insurance_plan_name[] = InsurancePlan::model()->getInsurancePlanNameById( $val['plan_id'] );

			}

			$user_selected_insurance_plan_ins[$insurance_val] = $user_selected_insurance_plan;

			$user_selected_insurance_plan_inc_name[$insurance_val] = $user_selected_insurance_plan_name;

		}

		//echo "<pre>";print_r($user_selected_insurance_plan_ins);

		

		 /*$item_count =32;

	    $page_size =2;

	 

	    $pages =new CPagination($item_count);

	    $pages->setPageSize($page_size);

	 

	    // simulate the effect of LIMIT in a sql query

	    $end =($pages->offset+$pages->limit <= $item_count ? $pages->offset+$pages->limit : $item_count);

	 

	    $sample =range($pages->offset+1, $end);*/

		$sql_address='select * FROM da_doctor_address WHERE status="1" and doctor_id="'.$id.'"';

		$command_address = $connection->createCommand($sql_address);

		$user_address = $command_address->queryAll();

		

		$this->render('edit_my_account',array(

			//'dataProvider'=>$usercriteria,

			'model' => $model,'user_speciality' => $user_speciality_res,'user_selected_speciality' => $user_selected_speciality,'user_condition' => $user_condition_res,'user_selected_condition' => $user_selected_condition,'user_procedure' => $user_procedure_res,'user_selected_procedure' => $user_selected_procedure,'yearOptions' => $yearOptions,'doctor_speciality' => $user_doctor_speciality,'default_data_address' => $default_data_address,'data_address' => $data_address,'birth_date' => $birth_date,'user_language' => $user_language_res,'user_selected_language' => $user_selected_language,'user_insurance' => $user_insurance_res,'user_selected_insurance' => $user_selected_insurance,'user_insurance_plan_res' => $user_insurance_plan_res,'user_insurance_plan_res_ins' => $user_insurance_plan_res_ins,'user_selected_insurance_plan_ins' => $user_selected_insurance_plan_ins,'data_video' => $data_video,'pages'=>$pages,'user_address' => $user_address,'user_selected_insurance_plan_inc_name' => $user_selected_insurance_plan_inc_name,

		));

	}

	

	public function actionDoctorImageDel()

	{

		if(!Yii::app()->session['logged_in'])

			$this->redirect(array('site/index'));

		

		$prv_img_path = $_REQUEST['image_name'];

		if($prv_img_path != ''){

			$id = Yii::app()->session['logged_user_id'];

			$sql='UPDATE da_doctor SET image = :image WHERE id ='.$id;

			$params = array(

				"image" => ''

			);

			$connection = Yii::app()->db;

			$command=$connection->createCommand($sql);

			$command->execute($params);

			

			$previous_path = getcwd().'/assets/upload/doctor/'.$id.'/'.$prv_img_path;

			if($prv_img_path != "" && file_exists($previous_path))

				unlink($previous_path);

			echo 'ok';	

		}

	}

	

	public function actionSpeciatlity($id = "")

	{

		if(!Yii::app()->session['logged_in'])

			$this->redirect(array('site/index'));

		

		$sql='select * FROM da_speciality WHERE status="1"';

		$connection = Yii::app()->db;

		$command = $connection->createCommand($sql);

		$user_speciality = $command->queryAll();

		$user_speciality_res = array();

		foreach ($user_speciality as $key=>$val) {

			$user_speciality_res[$val['id']] = array('speciality'=>$val['speciality'],'description'=>$val['description']);

		}

		

		$model->doctor_id = $id;

		

		$sql_doctor_speciality='select * FROM da_doctor_speciality WHERE status=1 and doctor_id='.$id;

		$command_doctor_speciality = $connection->createCommand($sql_doctor_speciality);

		$user_doctor_speciality = $command_doctor_speciality->queryAll();

		$this->render('speciatlity',array(

			'doctor_speciality' => $user_doctor_speciality,'user_speciality' => $user_speciality_res,

		));

	}

	

	public function actionEditSpeciatlity($id = "")

	{

		if(!Yii::app()->session['logged_in'])

			$this->redirect(array('site/index'));

		

		if(Yii::app()->getRequest()->isPostRequest){

			if($id != ""){

				$speciality_id=Yii::app()->getRequest()->getPost('speciality_id');

				$default_status=$_REQUEST['DoctorAddress']['default_status'];



				$sql='UPDATE da_doctor_speciality SET speciality_id = :speciality_id,default_status = :default_status WHERE id ='.$id;

				$params = array(

					"speciality_id" => $speciality_id,

					"default_status" => $default_status

				);

				$connection = Yii::app()->db;

				$command=$connection->createCommand($sql);

				$command->execute($params);

				

				Yii::app()->user->setFlash('editSpeciatlity','Your Speciality Updated Sucessfully.');

				$this->refresh();

			}else{

				$speciality_id=Yii::app()->getRequest()->getPost('speciality_id');

				$default_status=$_REQUEST['DoctorAddress']['default_status'];



				$sql='INSERT INTO da_doctor_speciality (speciality_id, doctor_id, default_status, status) VALUES(:speciality_id,:doctor_id,:default_status,:status)';

				$params = array(

					"speciality_id" => $speciality_id,

					"doctor_id" => Yii::app()->session['logged_user_id'],

					"default_status" => $default_status,

					"status" => 1

				);

				$connection = Yii::app()->db;

				$command=$connection->createCommand($sql);

				$command->execute($params);

				Yii::app()->user->setFlash('editSpeciatlity','Your Speciality Inserted Sucessfully.');

				$this->refresh();

			}

		} else{

			$sql='select * FROM da_speciality WHERE status="1"';

			$connection = Yii::app()->db;

			$command = $connection->createCommand($sql);

			$user_speciality = $command->queryAll();

			$user_speciality_res = array();

			foreach ($user_speciality as $key=>$val) {

				$user_speciality_res[$val['id']] = $val['speciality'];

			}

		}

		if($id != ""){

			$sql_doctor_speciality='select * FROM da_doctor_speciality WHERE id ='.$id;

			$connection_doctor_speciality = Yii::app()->db;

			$command_doctor_speciality = $connection_doctor_speciality->createCommand($sql_doctor_speciality);

			$user_doctor_speciality = $command_doctor_speciality->queryRow();

		}else{

			$doctor_speciality['speciality_id']=$user_doctor_speciality['speciality_id'];

			$doctor_speciality['speciality_id']=$user_doctor_speciality['speciality_id'];

		}

		

		$this->render('edit_speciatlity',array(

			//'dataProvider'=>$usercriteria,

			'user_speciality' => $user_speciality_res,'doctor_speciality' => $user_doctor_speciality,

		));

	}

	

	public function actionAddress($id = "")

	{

		/*$dataProvider=new CActiveDataProvider('Doctor');

		$this->render('index',array(

			'dataProvider'=>$dataProvider,

		));*/

		//$model = new DoctorAddress();

		if(!Yii::app()->session['logged_in'])

			$this->redirect(array('site/index'));

			

		$model->doctor_id = $id;

		$usercriteria =DoctorAddress::model()->findAll("doctor_id=$model->doctor_id");

		//echo '<pre>';print_r($usercriteria);

		$this->render('address',array(

			//'dataProvider'=>$usercriteria,

			'dataProvider' => $usercriteria,

		));

	}

	

	public function actionEditAddress($id = "")

	{

		/*$dataProvider=new CActiveDataProvider('Doctor');

		$this->render('index',array(

			'dataProvider'=>$dataProvider,

		));*/

		if(!Yii::app()->session['logged_in'])

			$this->redirect(array('site/index'));

		

		if(Yii::app()->getRequest()->isPostRequest){

			//echo $id."jana";die;

			if($id != ""){

				$model = DoctorAddress::model()->findByPk($id);

				$model->attributes=Yii::app()->getRequest()->getPost('DoctorAddress');

				$model->status = 1;

				//print_r($_REQUEST['Doctor']);print_r($model->attributes);die;

				if($model->save()){

					//$this->redirect(array('editProfile'));

					Yii::app()->user->setFlash('editAddress','Your Profile Updated Sucessfully.');

					$this->refresh();

				}

			}else{

				$model = new DoctorAddress();

				$model->attributes=Yii::app()->getRequest()->getPost('DoctorAddress');

				$model->doctor_id = Yii::app()->session['logged_user_id'];

				$model->date_created = date('Y-m-d h:i:s');

				$model->date_modified = date('Y-m-d h:i:s');

				$model->status = 1;

				if($model->save()){

					//$this->redirect(array('editProfile'));

					Yii::app()->user->setFlash('editAddress','Your Profile Updated Sucessfully.');

					$this->refresh();

				}

			}

		} else{

			if($id != "")

				$model = DoctorAddress::model()->findByPk($id);

			else

				$model = new DoctorAddress();

			$model->id = $id;

			$usercriteria =DoctorAddress::model()->find("id=\"$model->id\"");

			

			//print_r($usercriteria);

			$model->doctor_id=$usercriteria->doctor_id;

			$model->address=$usercriteria->address;

			$model->latitude=$usercriteria->latitude;

			$model->longitude=$usercriteria->longitude;

			$model->access=$usercriteria->access;

			$model->order_status=$usercriteria->order_status;

			$model->default_status=$usercriteria->default_status;

			$model->active=$usercriteria->active;

			$model->status=$usercriteria->status;

		}

		$this->render('edit_address',array(

			//'dataProvider'=>$usercriteria,

			'model' => $model,

		));

	}

	

	public function actionEditAddressAjax()

	{

		/*$dataProvider=new CActiveDataProvider('Doctor');

		$this->render('index',array(

			'dataProvider'=>$dataProvider,

		));*/

		$this->layout = false;

		$id = $_REQUEST['id'];

		$type = $_REQUEST['type'];

		if(!Yii::app()->session['logged_in'])

			$this->redirect(array('site/index'));

		//echo '<pre>';print_r($_REQUEST);

		if(Yii::app()->getRequest()->isPostRequest && $type == "submit"){

			//echo $id."jana";die;

			if($id != ""){

				$model = DoctorAddress::model()->findByPk($id);

				$model->attributes=Yii::app()->getRequest()->getPost('DoctorAddress');

				$model->status = 1;

				//$model->suite=$_REQUEST['DoctorAddress']['suite'];

				//$model->default_status=$_REQUEST['DoctorAddress']['default_status'];

				$model->state=$_REQUEST['state'];

				$model->practice_affiliation=$_REQUEST['DoctorAddress']['practice_affiliation'];

				$model->office_name=$_REQUEST['DoctorAddress']['office_name'];

				$model->office_phone=$_REQUEST['DoctorAddress']['office_phone'];

				$model->office_fax=$_REQUEST['DoctorAddress']['office_fax'];

				//$address_arr = $this->getLangLat($model->address);

				//====================================================================

				$address_d = $_REQUEST['DoctorAddress']['address'];

				$city_name_d = $_REQUEST['DoctorAddress']['city']; 

				$state_name_d = $_REQUEST['state'];

				//====================================================================

				$address_arr_d = $address_d.", ".$city_name_d.", ".$state_name_d;

				$address_arr = $this->getLangLat($address_arr_d);



				$model->latitude=$address_arr->lat;

				$model->longitude=$address_arr->lng;

				//$model->access=$_REQUEST['access'];

				//print_r($_REQUEST['Doctor']);print_r($model->attributes);die;

				if($model->save()){

					//Yii::app()->user->setFlash('editAddress','Your Profile Updated Sucessfully.');

					//$this->refresh();

					if($model->default_status == 1){

						$sql='UPDATE da_doctor_address SET default_status = :default_status WHERE id !='.$id;

						$params = array(

							"default_status" => 0

						);

						$connection = Yii::app()->db;

						$command=$connection->createCommand($sql);

						$command->execute($params);

					}

					echo "**##**";die;

				}

			}else{

				$model = new DoctorAddress();

				$model->attributes=Yii::app()->getRequest()->getPost('DoctorAddress');

				$model->doctor_id = Yii::app()->session['logged_user_id'];

				$model->date_created = date('Y-m-d h:i:s');

				$model->date_modified = date('Y-m-d h:i:s');

				$model->status = 1;

				$model->state=$_REQUEST['state'];

				

				$model->practice_affiliation=$_REQUEST['DoctorAddress']['practice_affiliation'];

				$model->office_name=$_REQUEST['DoctorAddress']['office_name'];

				$model->office_phone=$_REQUEST['DoctorAddress']['office_phone'];

				$model->office_fax=$_REQUEST['DoctorAddress']['office_fax'];

				//$address_arr = $this->getLangLat($model->address);

				//====================================================================

				$address_d = $_REQUEST['DoctorAddress']['address'];

				$city_name_d = $_REQUEST['DoctorAddress']['city']; 

				$state_name_d = $_REQUEST['state'];

				//====================================================================

				$address_arr_d = $address_d.", ".$city_name_d.", ".$state_name_d;

				$address_arr = $this->getLangLat($address_arr_d);



				$model->latitude=$address_arr->lat;

				$model->longitude=$address_arr->lng;

				if($model->save()){

					//Yii::app()->user->setFlash('editAddress','Your Profile Updated Sucessfully.');

					//$this->refresh();

					if($model->default_status == 1){

						$sql='UPDATE da_doctor_address SET default_status = :default_status WHERE id !='.$model->id;

						$params = array(

							"default_status" => 0

						);

						$connection = Yii::app()->db;

						$command=$connection->createCommand($sql);

						$command->execute($params);

					}

					echo "**##**";die;

				}

			}

		} else{

			if($id != ""){

				$model = DoctorAddress::model()->findByPk($id);

				

				$model->id = $id;

				$usercriteria =DoctorAddress::model()->find("id=\"$model->id\"");

				

				//print_r($usercriteria);

				$model->doctor_id=$usercriteria->doctor_id;

				$model->address=$usercriteria->address;

				$model->latitude=$usercriteria->latitude;

				$model->longitude=$usercriteria->longitude;

				$model->access=$usercriteria->access;

				$model->order_status=$usercriteria->order_status;

				$model->default_status=$usercriteria->default_status;

				$model->active=$usercriteria->active;

				$model->status=$usercriteria->status;

			}else{

				$model = new DoctorAddress();

			}

		}

		

		$condition = 'status = 1 and country_id =227';

		$criteria = new CDbCriteria(array(

			'condition' => $condition

		));

		$data_city_all = State::model()->findAll($criteria);

		$data_city = array();

		foreach ($data_city_all as $data_city_all_val) {

			$data_city[$data_city_all_val['short_code']] = $data_city_all_val['short_code'];

		}

		//Helpers::pre($data_city);die;

		$this->render('edit_my_address',array(

			//'dataProvider'=>$usercriteria,

			'model' => $model,'data_city' => $data_city,

		));

	}

	

	public function actionEditVideoAjax()

	{

		$this->layout = false;

		$id = $_REQUEST['id'];

		$type = $_REQUEST['type'];

		if(!Yii::app()->session['logged_in'])

			$this->redirect(array('site/index'));

		//echo '<pre>';print_r($_REQUEST);

		$connection = Yii::app()->db;

		if(Yii::app()->getRequest()->isPostRequest && $type == "submit"){

			//echo $id."jana";die;

			if($id != ""){

				$model = DoctorVideo::model()->findByPk($id);

				$model->attributes=Yii::app()->getRequest()->getPost('DoctorVideo');

				$model->status = 1;

				$model->description=$_REQUEST['DoctorVideo']['description'];

				$model->default_status=$_REQUEST['default_status'];

				//$model->default_status=$_REQUEST['DoctorVideo']['default_status'];

				//print_r($_REQUEST['Doctor']);print_r($model->attributes);die;

				if($model->save()){

					//Yii::app()->user->setFlash('editAddress','Your Profile Updated Sucessfully.');

					//$this->refresh();

					if($model->default_status == 1){

						$sql='UPDATE da_doctor_video SET default_status = :default_status WHERE doctor_id = '.$model->doctor_id.' and id !='.$id;

						$params = array(

							"default_status" => 0

						);

						$command=$connection->createCommand($sql);

						$command->execute($params);

					}

					echo "**##**";die;

				}

			}else{

				$model = new DoctorVideo();

				$model->attributes=Yii::app()->getRequest()->getPost('DoctorVideo');

				$model->doctor_id = Yii::app()->session['logged_user_id'];

				//$model->date_created = date('Y-m-d h:i:s');

				//$model->date_modified = date('Y-m-d h:i:s');

				$model->default_status = $_REQUEST['default_status'];

				$model->status = 1;

				if($model->save()){

					//Yii::app()->user->setFlash('editAddress','Your Profile Updated Sucessfully.');

					//$this->refresh();

					if($model->default_status == 1){

						$sql='UPDATE da_doctor_video SET default_status = :default_status WHERE doctor_id = '.$model->doctor_id.' and id !='.$model->id;

						$params = array(

							"default_status" => 0

						);

						$command=$connection->createCommand($sql);

						$command->execute($params);

					}

					echo "**##**";die;

				}

			}

		} else{

			if($id != "")

				$model = DoctorVideo::model()->findByPk($id);

			else

				$model = new DoctorVideo();

			$model->id = $id;

			$usercriteria =DoctorVideo::model()->find("id=\"$model->id\"");

			

			//print_r($usercriteria);

			if($usercriteria){

			$model->doctor_id=$usercriteria->doctor_id;

			$model->status=$usercriteria->status;

			}

		}

		

		$this->render('edit_my_video',array(

			//'dataProvider'=>$usercriteria,

			'model' => $model,

		));

	}

	

	public function actionRemoveVideoAjax()

	{

		

		$connection = Yii::app()->db;

		$id = $_REQUEST['id'];

		$sql='UPDATE da_doctor_video SET status = :status WHERE id ='.$id;

		$params = array(

			"status" => 0

		);

		$command=$connection->createCommand($sql);

		$command->execute($params);

		

	}

	

	public function actionTodolist($id = "")

	{

		if(!Yii::app()->session['logged_in'])

			$this->redirect(array('site/index'));

		$model = new stdClass();

		$model->doctor_id = $id;

		

		$condition = 'status = 1 and doctor_id =' . $model->doctor_id;

		$criteria = new CDbCriteria(array(

			'condition' => $condition,

        	'order' => 'id DESC'

		));

		

		$count=TodoList::model()->count($criteria);

		$pages=new CPagination($count);

	

		// results per page

		$pages->pageSize=10;

		$pages->applyLimit($criteria);

		

		$usercriteria =TodoList::model()->findAll($criteria);

		

		$this->render('todo_list',array(

			'dataProvider' => $usercriteria,'pages'=>$pages,

		));

	}

	

	public function actionEditTodolist($id = "")

	{

		if(!Yii::app()->session['logged_in'])

			$this->redirect(array('site/index'));

		

		if(Yii::app()->getRequest()->isPostRequest){

			if($id != ""){

				$model = TodoList::model()->findByPk($id);

				$model->attributes=Yii::app()->getRequest()->getPost('TodoList');

				$model->todolist_status = $_REQUEST['todolist_status'];

				$model->deadline = $_REQUEST['TodoList']['deadline'];

				$model->status = 1;

				

				/*if(Yii::app()->getRequest()->getPost('from_time_format')=='AM')

					$time_from_nc_hour = Yii::app()->getRequest()->getPost('time_from_nc_hour');

				else

					$time_from_nc_hour = Yii::app()->getRequest()->getPost('time_from_nc_hour')+12;

				$time_from_nc_minute = Yii::app()->getRequest()->getPost('time_from_nc_minute');

				$from_time_format = Yii::app()->getRequest()->getPost('from_time_format');

				

				$model->deadline = date('Y-m-d h:i:s',strtotime(Yii::app()->getRequest()->getPost('deadline').' '.$time_from_nc_hour.':'.$time_from_nc_minute.':00'));*/

				

				if($model->save()){

					Yii::app()->user->setFlash('editTodolist','Your Todo list Updated Sucessfully.');

					//$this->refresh();

					$this->redirect(array('doctor/todolist/'.Yii::app()->session['logged_user_id']));

				}

			}else{

				$model = new TodoList();

				$model->attributes=Yii::app()->getRequest()->getPost('TodoList');

				$model->doctor_id = Yii::app()->session['logged_user_id'];

				$model->date_created = date('Y-m-d h:i:s');

				$model->date_modified = date('Y-m-d h:i:s');

				$model->todolist_status = $_REQUEST['todolist_status'];

				$model->deadline = $_REQUEST['TodoList']['deadline'];

				$model->status = 1;

				

				/*if(Yii::app()->getRequest()->getPost('from_time_format')=='AM')

					$time_from_nc_hour = Yii::app()->getRequest()->getPost('time_from_nc_hour');

				else

					$time_from_nc_hour = Yii::app()->getRequest()->getPost('time_from_nc_hour')+12;

				$time_from_nc_minute = Yii::app()->getRequest()->getPost('time_from_nc_minute');

				$from_time_format = Yii::app()->getRequest()->getPost('from_time_format');

				

				$model->deadline = date('Y-m-d h:i:s',strtotime(Yii::app()->getRequest()->getPost('deadline').' '.$time_from_nc_hour.':'.$time_from_nc_minute.':00'));*/

				

				if($model->save()){

					Yii::app()->user->setFlash('editTodolist','Your Todo list Added Sucessfully.');

					//$this->refresh();

					$this->redirect(array('doctor/todolist/'.Yii::app()->session['logged_user_id']));

				}

			}

		} else{

			if($id != ""){

				$model = TodoList::model()->findByPk($id);

				$model->id = $id;

				$usercriteria =TodoList::model()->find("id=\"$model->id\"");

				

				$model->doctor_id=$usercriteria->doctor_id;

				$model->status=$usercriteria->status;

			}else

				$model = new TodoList();

		}

		

		/*if($model->deadline=='' || $model->deadline == '0000-00-00 00:00:00'){

			$from_time = "";

			$from_date = "";

			$from_format = "";

		}else{

			$time_arr = explode(' ',$model->deadline);

			$from_time = $time_arr[1];

			$from_date = $time_arr[0];

			if(date('A',strtotime($model->deadline))=='AM')

				$from_format = "AM";

			else

				$from_format = "PM";

		}

			

		if($from_time == "" || $from_time == '00:00:00'){

			$from_time_arr['hh'] = "";

			$from_time_arr['mm'] = "";

		}else{

			$from_time_arr_q =explode(":",$from_time);

			if($from_format == "AM")

				$from_time_arr['hh'] = date('h',strtotime($from_date.' '.$from_time_arr_q[0].':00:00'));

			else{

				$date_hr_per = $from_time_arr_q[0]-12;

				$from_time_arr['hh'] = date('h',strtotime($from_date.' '.$date_hr_per.':00:00'));

			}

			$from_time_arr['mm'] = $from_time_arr_q[1];

		}*/

		//print_r($from_time_arr);

		$this->render('edit_todolist',array(

			'model' => $model,//'from_time' => $from_time_arr,'from_date' => $from_date,'from_format' => $from_format,

		));

	}

	

	public function actionTodolistAjaxRemove()

	{

		

		$connection = Yii::app()->db;

		$id = $_REQUEST['id'];

		$sql="UPDATE da_todo_list SET status = :status WHERE id ='".$id."'";

		$params = array(

			"status" => 0

		);

		$command=$connection->createCommand($sql);

		$command->execute($params);

		

	}

	

	public function actionPatientAjaxSearch()

	{

		if(!Yii::app()->session['logged_in'])

			$this->redirect(array('site/index'));

			

		$user_first_name= Yii::app()->request->getParam('user_first_name');

		$user_last_name= Yii::app()->request->getParam('user_last_name');

		$user_phone= Yii::app()->request->getParam('user_phone');

		$patient_mrn= Yii::app()->request->getParam('patient_mrn');		

		

		$sql  = "select p.id as p_id,p.user_first_name,p.user_last_name,p.user_phone FROM da_patient as p left join da_doctor_book db on p.id=db.patient_id ";

		

		if($patient_mrn != ""){

			$sql .= " left join da_patient_insurance as pi on pi.patient_id = p.id ";

		}

		

		$sql .= " WHERE p.status=1 and db.doctor_id=".Yii::app()->session['logged_user_id'];

		

		if($user_first_name != ""){

			$sql .= " and p.user_first_name like '%".$user_first_name."%' ";

		}

		if($user_last_name != ""){

			$sql .= " and p.user_last_name like '%".$user_last_name."%' ";

		}

		if($user_phone != ""){

			$sql .= " and p.user_phone like '%".$user_phone."%' ";

		}

		if($patient_mrn != ""){

			$sql .= " and pi.user_mrn like '%".$patient_mrn."%' ";

		}

		

		$sql .= " group BY db.patient_id order BY p.user_first_name ";

		//echo $sql;

		$connection = Yii::app()->db;

		$command = $connection->createCommand($sql);

		$patient_list = $command->queryAll();

		//Helpers::pre($patient_list);

		$result = '';

		if(!empty($patient_list)){

			for($i=0;$i<count($patient_list);$i++){

				$full_name = $patient_list[$i]['user_first_name'].' '.$patient_list[$i]['user_last_name'];

				$search_link = CHtml::link('<img src="'.Yii::app()->request->baseUrl.'/assets/images/confirm.png" alt=""/>', 'javascript:void(0);',array('onclick'=>"patientSelect('".$full_name."','".$patient_list[$i]['p_id']."');"));

				$result .= '<li><span class="active" style="width:25%" >'.$patient_list[$i]['user_first_name'].'</span>

                         <span class="active" style="width:25%" >'.$patient_list[$i]['user_last_name'].'</span>

                         <span class="active" style="width:25%" >'.$patient_list[$i]['user_phone'].'</span> 

                         <span class="att txt_align">'.$search_link.'</span></li>';

			}

		}

		if($result == '')	echo 'No Result Found!';

		else	echo $result;

	}

	

	public function actionPatient($id = "")

	{

		if(!Yii::app()->session['logged_in'])

			$this->redirect(array('site/index'));

		$model = new stdClass();

		$model->doctor_id = $id; 

		$user_first_name= Yii::app()->request->getParam('user_first_name');

		$user_last_name= Yii::app()->request->getParam('user_last_name');

		$user_dob= Yii::app()->request->getParam('user_dob');

		$user_phone= Yii::app()->request->getParam('user_phone');

		//$patient_id= Yii::app()->request->getParam('patient_id');

		$patient_mrn= Yii::app()->request->getParam('patient_mrn');

		//$appointment_number= Yii::app()->request->getParam('appointment_number');

		

		$sql_count="select count( DISTINCT db.id ) AS d_count FROM da_patient as p left join da_doctor_book db on p.id=db.patient_id ";

		$sql  = "select p.*,db.id as app_book_id,pi.user_mrn as app_mrn FROM da_patient as p left join da_doctor_book db on p.id=db.patient_id ";

		

		//if($patient_mrn != ""){

			$sql_count .= " left join da_patient_insurance as pi on pi.patient_id = p.id ";

			$sql .= " left join da_patient_insurance as pi on pi.patient_id = p.id ";

		//}

		

		$sql_count .= " WHERE p.status=1 and db.doctor_id=".Yii::app()->session['logged_user_id'];

		$sql .= " WHERE p.status=1 and db.doctor_id=".Yii::app()->session['logged_user_id'];

		

		if($user_first_name != ""){

			$sql_count .= " and p.user_first_name like '%".$user_first_name."%' ";

			$sql .= " and p.user_first_name like '%".$user_first_name."%' ";

		}

		if($user_last_name != ""){

			$sql_count .= " and p.user_last_name like '%".$user_last_name."%' ";

			$sql .= " and p.user_last_name like '%".$user_last_name."%' ";

		}

		if($user_dob != ""){

			$sql_count .= " and p.user_dob = '".$user_dob."' ";

			$sql .= " and p.user_dob = '".$user_dob."' ";

		}

		if($user_phone != ""){

			$sql_count .= " and p.user_phone = '".$user_phone."' ";

			$sql .= " and p.user_phone = '".$user_phone."' ";

		}

		/*if($patient_id != ""){

			$sql_count .= " and p.id = '".$patient_id."' ";

			$sql .= " and p.id = '".$patient_id."' ";

		}*/

		if($patient_mrn != ""){

			$sql_count .= " and pi.user_mrn = '.$patient_mrn.' ";

			$sql .= " and pi.user_mrn = '$patient_mrn' ";

		}

		/*if($appointment_number != ""){

			$sql_count .= " and db.id = '".$appointment_number."' ";

			$sql .= " and db.id = '".$appointment_number."' ";

		}*/

		

		$sql_count .= " group BY db.patient_id ";

		$sql .= " group BY db.patient_id ";

		

		$sql_count .= " ";

		$sql .= " order BY p.date_created DESC";

		//echo $sql;

		$limit = 10;

		$end_limit = 10;

		if(isset($_REQUEST['page']) && $_REQUEST['page']>1){

			$star_limit = ($limit*($_REQUEST['page']-1));

		}else{

			$star_limit = 0;

		}

		$sql .= ' limit '.$star_limit.','.$end_limit;

		$sql_count .= ' ';

		//echo $sql_count.'<br>';

		//echo $sql;die;

		$connection = Yii::app()->db;

		$command = $connection->createCommand($sql);

		$patient_list = $command->queryAll();

		

		$command_count = $connection->createCommand($sql_count);

		$patient_list_count = $command_count->queryAll();

		

		$count=count($patient_list_count);

		/*if(isset($patient_list_count[0]['d_count']))

			$count=$patient_list_count[0]['d_count'];

		else

			$count=0;*/

		$pages=new CPagination($count);

		$criteria = new CDbCriteria(array(

			//'condition' => $condition,

        	//'order' => 'category_id DESC'

		));

	

		// results per page

		$pages->pageSize=$limit;

		$pages->applyLimit($criteria);

		//Helpers::pre($patient_list);die;

		$this->render('patient',array(

			'dataProvider' => $patient_list,'pages'=>$pages,

		));

	}

	

	public function actionPatientAjaxInd($id = "")

	{

		if(!Yii::app()->session['logged_in'])

			$this->redirect(array('site/index'));

		

		$app_book_id = $_REQUEST['app_book_id'];

		$patient_id = $_REQUEST['patient_id'];

		$created_by = $_REQUEST['created_by'];

		$connection = Yii::app()->db;

		

		$sql  = "select * FROM da_patient WHERE status=1 and id=".$patient_id;

		$command = $connection->createCommand($sql);

		$patient_list = $command->queryRow();

		

		$sql_app  = "select * FROM da_doctor_book WHERE status=1 and patient_id=".$patient_id." and doctor_id=".Yii::app()->session['logged_user_id'];

		$command_app = $connection->createCommand($sql_app);

		$patient_app_arr = $command_app->queryAll();

		$app_data = array();

		if(!empty($patient_app_arr)){

			$app_data['upcoming_app'] = '';

			$app_data['past_app'] = '';

			foreach ($patient_app_arr as $key=>$val) {

			//==========================================================================

			$status_event = "";

			if($val['confirm']==0) //0 for Scheduled, 1 for Confirmed, 2 for Cancelled

			{

				$status_event = "Scheduled";

			}elseif($val['confirm']==1)

			{

				$status_event = "Confirmed";

			}elseif($val['confirm']==2)

			{

				$status_event = "Cancelled";

			}

			//=========================================================================

				$status = ($val['confirm']==0)?'Pending':($val['confirm']==1)?'Confirmed':'Canceled';

				if($val['book_time']>=time()){

					//$app_data['upcoming_app'] = array('app_time'=>date('m/d/Y h:iA',$val['book_time']),'status'=>$val['confirm']);

					$app_data['upcoming_app'] .= '<div><span><a href="javascript:void(0);" onclick="appointmentShow('.$val['id'].');" style="color:blue;">'.date('m/d/Y h:iA',$val['book_time']).'</a></span><span>'.$status_event.'</span></div>';

				}else{

					//$app_data['past_app'] = array('app_time'=>date('m/d/Y h:iA',$val['book_time']),'status'=>$val['confirm']);

					$app_data['past_app'] .= '<div><span><a href="javascript:void(0);" onclick="appointmentShow('.$val['id'].');" style="color:blue;">'.date('m/d/Y h:iA',$val['book_time']).'</a></span><span>'.$status_event.'</span></div>';

				}

			}

		}

		

		$sql_insurance  = "select * FROM da_patient_insurance WHERE status=1 and patient_id=".$patient_id;

		$command_insurance = $connection->createCommand($sql_insurance);

		$patient_insurance_arr = $command_insurance->queryRow();

		

		$sql_comments  = "select * FROM da_patient_comments WHERE status=1 and patient_id=".$patient_id." and doctor_id=".Yii::app()->session['logged_user_id'];

		$command_comments = $connection->createCommand($sql_comments);

		$patient_comments_arr = $command_comments->queryAll();

		$comments_data = '';

		if(!empty($patient_comments_arr)){

			foreach ($patient_comments_arr as $comments_key=>$comments_val) {

				$link = CHtml::link('<img src="'.Yii::app()->request->baseUrl.'/assets/images/edit_icon.png" alt=""/>', 'javascript:void(0);',array('onclick'=>'commentsEdit('.$comments_val['id'].');'));

				$comments_data .= '<div><span id="edit_comments_data_'.$comments_val['id'].'">'.$comments_val['comments'].'</span><span>'.$link.'</span></div>';

			}

		}

		

		$sql_messages  = "select * FROM da_patient_messages WHERE status=1 and patient_id=".$patient_id." and doctor_id=".Yii::app()->session['logged_user_id'];

		$command_messages = $connection->createCommand($sql_messages);

		$patient_messages_arr = $command_messages->queryAll();

		$messages_data = '';

		if(!empty($patient_messages_arr)){

			foreach ($patient_messages_arr as $messages_key=>$messages_val) {

				$link = CHtml::link($messages_val['subject'], 'javascript:void(0);',array('onclick'=>'messagesEdit('.$messages_val['id'].');','style'=>'color:blue;'));

				$messages_data .= '<div><span>'.date('m/d/Y h:i A',strtotime($messages_val['date_created'])).'</span><span id="edit_messages_data_'.$messages_val['id'].'">'.$link.'</span><span>Sent</span></div>';

			}

		}

		

		$patient_data = array('patient_data'=>$patient_list,'app_data'=>$app_data,'patient_insurance'=>$patient_insurance_arr,'patient_comments'=>$comments_data,'patient_messages'=>$messages_data);

		$json_data = json_encode($patient_data);

		echo $json_data;

		//Helpers::pre($comments_data);die;

	}

	

	public function actionPatientAjaxAppointment($id = "")

	{

		/*$strt_tm = date('Y-m-d',$_REQUEST['start']);

		$end_tm = date('Y-m-d',$_REQUEST['end']);*/

		$app_id = $_REQUEST['app_id'];

		$patient_id = $_REQUEST['patient_id'];

		if(!Yii::app()->session['logged_in'])

			$this->redirect(array('site/index'));

		

		$sql_doctor_address='select db.*,concat_ws(" ", dp.user_first_name,  dp.user_last_name ) as dp_name,da.address as da_address,dpro.procedure as dpro_procedure,dp.created_by as dp_created_by,drv.reason_for_visit as drv_reason_for_visit FROM da_doctor_book as db left join da_patient as dp on db.patient_id = dp.id left join da_doctor_address as da on db.address_id = da.id left join da_procedure as dpro on db.procedure_id = dpro.id left join da_reason_for_visit as drv on db.reason_for_visit_id = drv.id WHERE db.status=1 and db.id='.$app_id;

		$connection = Yii::app()->db;

		$command_doctor_address = $connection->createCommand($sql_doctor_address);

		$user_patient_book = $command_doctor_address->queryAll();

		$user_calender_res = array();

		foreach ($user_patient_book as $key=>$val) {

			$st_time = date('Y-m-d H:i',$val['book_time']);

			$end_time = date('Y-m-d H:i',($val['book_time']+($val['book_duration']*60)));

			//=========================================================================



			if($val['confirm']==0) {

				$status_event = "Scheduled";

			}elseif($val['confirm']==1) {

				$status_event = "Confirmed";

			}elseif($val['confirm']==2) {

				$status_event = "Cancelled";

			}elseif($val['confirm']==3) {

				$status_event = "Complete";

			}elseif($val['confirm']==4) {

				$status_event = "NoShow";

			}

			//=========================================================================

			$user_calender_res =array(

								'id' => $val['id'],

								'title' => ($val['dp_name'])?$val['dp_name']:$val['patient_name'],

								'start' => "$st_time",

								'end' => "$end_time",

								'book_duration' => $val['book_duration'],

								'created_by' => $val['dp_created_by'],

								'patient_id' => $val['patient_id'],

								'patient_name' => $val['patient_name'],

								'event_status' => $status_event,

								'event_notes' => $val['event_notes'],

								'address_id' => $val['address_id'],

								'procedure_id' => $val['procedure_id'],

								'address' => $val['da_address'],

								//'procedure' => $val['dpro_procedure'],

								'procedure' => $val['drv_reason_for_visit'],

								'scheduled' => date('m/d/Y H:iA',$val['book_time']),

							);

		}

		$patient_data = array('app_data'=>$user_calender_res,'return_data'=>'True');

		echo $events_code = json_encode($patient_data);

		

	}

	

	public function actionPatientAjaxMessagesSave()

	{

		if(!Yii::app()->session['logged_in'])

			$this->redirect(array('site/index'));

		

		$patient_id = $_REQUEST['patient_id'];

		$subject = $_REQUEST['subject'];

		$body = $_REQUEST['body'];

		$email = $_REQUEST['email'];

		$doctor_id = Yii::app()->session['logged_user_id'];

		

		$connection = Yii::app()->db;

		$sql_patient='INSERT INTO da_patient_messages (patient_id, doctor_id, subject, body, date_created, status) VALUES(:patient_id,:doctor_id,:subject,:body,:date_created,:status)';

		$params_patient = array(

			"patient_id" => $patient_id,

			"doctor_id" => $doctor_id,

			"subject" => $subject,

			"body" => $body,

			"date_created" => date('Y-m-d h:i:s'),

			"status" => 1

		);

		$command_patient=$connection->createCommand($sql_patient);

		$command_patient->execute($params_patient);

		if($email == 'email'){

			$usercriteria =Patient::model()->findByPk($patient_id);

			$to_email = $usercriteria->user_email;

			$to_name = $usercriteria->user_first_name." ".$usercriteria->user_last_name;

			$to = array($to_email,$to_name);

			$from = array('support@edoctorbook.com','eDoctorBook');

			$subject_mail = 'eDoctorBook - Doctor Message.';

			$message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

					<html xmlns="http://www.w3.org/1999/xhtml">

					<head>

					<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

					<title>Doctor Email Template</title>

					</head>

					<body style="padding:0px; margin:0px; font-size:12px; color:#545454; line-height:18px; font-family:Arial, Helvetica, sans-serif;">

					<div style="width:450px;  margin:5px auto; border:1px solid #c7c7c7; "> 

					<div style="text-align:left; margin-bottom:15px; -moz-box-shadow:0px 1px 1px 1px #E4E4E4; -webkit-box-shadow:0px 1px 1px 1px #E4E4E4;box-shadow:0px 1px 1px 1px #E4E4E4; background:#54cbc8; font-weight:bold;">

					<a href="'.$this->createAbsoluteUrl('site/index/').'"><img src="http://'.$_SERVER['HTTP_HOST'].Yii::app()->getHomeUrl().'/assets/images/small_logo.png" style="padding:3px;" alt="Doctor Appointment Book" /></a>

					&nbsp;

					</div>	



					<h1 style="text-align:center; display:block; font-weight:normal; margin:0px; padding-bottom:12px; font-size:19px; line-height:26px; color:#4c4c4c;">Thank you '.$to_name.' for using <br /> <font style="color:#6ad2cf">eDoctorBook</font>!</h1>



					<div style="min-height:200px; background:#fff; color:#bbb; margin:10px; padding-top:15px; font-size:12px;"><pre style="color:#000">'.$body.'</pre>

					<br>

					

					</div>

					<p style=" margin-top:10px; padding:8px 5px; color:#000; font-size:13px; margin-bottom:0px; font-weight:bold; line-height:18px; ">eDoctorBook Team</p>

					<a style="padding-left:5px; padding-right:5px; color:#000; font-size:13px; font-weight:bold; margin:0px; line-height:0px; text-decoration:none" href="">http://www.eDoctorBook.com</a>

					<div style="background:#54cbc8; padding-top:20px; margin-top:15px; padding-bottom:10px; text-align:center; color:#e2e2e2; font-size:14px;"></div>

					</div>



					</body>

					</html>';

			

			Helpers::mailsend($to,$from,$subject_mail,$message);

		}

		echo 'True';

	}

	

	public function actionPatientAjaxCommentsSave()

	{

		if(!Yii::app()->session['logged_in'])

			$this->redirect(array('site/index'));

		

		$id = $_REQUEST['id'];

		$patient_id = $_REQUEST['patient_id'];

		$comments = $_REQUEST['comments'];

		$doctor_id = Yii::app()->session['logged_user_id'];

		

		$connection = Yii::app()->db;

		if($id!=''){

			$sql_patient='UPDATE da_patient_comments SET comments = :comments WHERE status=1 and id ='.$id;

			$params_patient = array(

				"comments" => $comments

			);

			$command_patient=$connection->createCommand($sql_patient);

			$command_patient->execute($params_patient);

		}else{

			$sql_patient='INSERT INTO da_patient_comments (patient_id, doctor_id, comments, date_created, status) VALUES(:patient_id,:doctor_id,:comments,:date_created,:status)';

			$params_patient = array(

				"patient_id" => $patient_id,

				"doctor_id" => $doctor_id,

				"comments" => $comments,

				"date_created" => date('Y-m-d h:i:s'),

				"status" => 1

			);

			$command_patient=$connection->createCommand($sql_patient);

			$command_patient->execute($params_patient);

		}

		echo 'True';

	}

	

	public function actionPatientAjaxInsuranceSave()

	{

		if(!Yii::app()->session['logged_in'])

			$this->redirect(array('site/index'));

		

		$id = $_REQUEST['id'];

		$patient_id = $_REQUEST['patient_id'];

		$user_mrn = $_REQUEST['user_mrn'];

		$user_ssn = $_REQUEST['user_ssn'];

		$user_insurance_provider = $_REQUEST['user_insurance_provider'];

		$user_insurance_id = $_REQUEST['user_insurance_id'];

		$user_insurance_group = $_REQUEST['user_insurance_group'];

		$user_insurance_employer = $_REQUEST['user_insurance_employer'];

		$user_insurance_employer_phone = $_REQUEST['user_insurance_employer_phone'];

		$user_insurance_employer_address = $_REQUEST['user_insurance_employer_address'];

		$user_insurance_employer_street = $_REQUEST['user_insurance_employer_street'];

		$user_insurance_employer_city = $_REQUEST['user_insurance_employer_city'];

		$user_insurance_employer_state = $_REQUEST['user_insurance_employer_state'];

		$user_insurance_employer_zip = $_REQUEST['user_insurance_employer_zip'];

		

		$connection = Yii::app()->db;

		if($id!=''){

			$sql_patient='UPDATE da_patient_insurance SET user_mrn = :user_mrn,user_ssn = :user_ssn,user_insurance_provider = :user_insurance_provider,user_insurance_id = :user_insurance_id,user_insurance_group = :user_insurance_group,user_insurance_employer = :user_insurance_employer,user_insurance_employer_phone = :user_insurance_employer_phone,user_insurance_employer_address = :user_insurance_employer_address,user_insurance_employer_street = :user_insurance_employer_street,user_insurance_employer_city = :user_insurance_employer_city,user_insurance_employer_state = :user_insurance_employer_state,user_insurance_employer_zip = :user_insurance_employer_zip WHERE status=1 and id ='.$id;

			$params_patient = array(

				"user_mrn" => $user_mrn,

				"user_ssn" => $user_ssn,

				"user_insurance_provider" => $user_insurance_provider,

				"user_insurance_id" => $user_insurance_id,

				"user_insurance_group" => $user_insurance_group,

				"user_insurance_employer" => $user_insurance_employer,

				"user_insurance_employer_phone" => $user_insurance_employer_phone,

				"user_insurance_employer_address" => $user_insurance_employer_address,

				"user_insurance_employer_street" => $user_insurance_employer_street,

				"user_insurance_employer_city" => $user_insurance_employer_city,

				"user_insurance_employer_state" => $user_insurance_employer_state,

				"user_insurance_employer_zip" => $user_insurance_employer_zip

			);

			$command_patient=$connection->createCommand($sql_patient);

			$command_patient->execute($params_patient);

		}else{

			$sql_patient='INSERT INTO da_patient_insurance (patient_id, user_mrn, user_ssn, user_insurance_provider, user_insurance_id, user_insurance_group, user_insurance_employer, user_insurance_employer_phone, user_insurance_employer_address, user_insurance_employer_street, user_insurance_employer_city, user_insurance_employer_state, user_insurance_employer_zip, status) VALUES(:patient_id,:user_mrn,:user_ssn,:user_insurance_provider,:user_insurance_id,:user_insurance_group,:user_insurance_employer,:user_insurance_employer_phone,:user_insurance_employer_address,:user_insurance_employer_street,:user_insurance_employer_city,:user_insurance_employer_state,:user_insurance_employer_zip,:status)';

			$params_patient = array(

				"patient_id" => $patient_id,

				"user_mrn" => $user_mrn,

				"user_ssn" => $user_ssn,

				"user_insurance_provider" => $user_insurance_provider,

				"user_insurance_id" => $user_insurance_id,

				"user_insurance_group" => $user_insurance_group,

				"user_insurance_employer" => $user_insurance_employer,

				"user_insurance_employer_phone" => $user_insurance_employer_phone,

				"user_insurance_employer_address" => $user_insurance_employer_address,

				"user_insurance_employer_street" => $user_insurance_employer_street,

				"user_insurance_employer_city" => $user_insurance_employer_city,

				"user_insurance_employer_state" => $user_insurance_employer_state,

				"user_insurance_employer_zip" => $user_insurance_employer_zip,

				"status" => 1

			);

			$command_patient=$connection->createCommand($sql_patient);

			$command_patient->execute($params_patient);

		}

		echo 'True';

	}

	

	public function actionPatientAjaxIndSave()

	{

		if(!Yii::app()->session['logged_in'])

			$this->redirect(array('site/index'));

		

		$patient_id = $_REQUEST['patient_id'];

		$user_first_name = $_REQUEST['user_first_name'];

		$user_last_name = $_REQUEST['user_last_name'];

		$user_parent_legal_guardian = $_REQUEST['user_parent_legal_guardian'];

		$user_email = $_REQUEST['user_email'];

		$user_phone = $_REQUEST['user_phone'];

		$user_address = $_REQUEST['user_address'];

		$user_city = $_REQUEST['user_city'];

		$user_state = $_REQUEST['user_state'];

		$user_zip = $_REQUEST['user_zip'];

		$user_contact_method = $_REQUEST['user_contact_method'];

		$user_dob = $_REQUEST['user_dob'];

		$user_recall_frequency = $_REQUEST['user_recall_frequency'];

		

		$connection = Yii::app()->db;

		$sql_patient='UPDATE da_patient SET user_first_name = :user_first_name,user_last_name = :user_last_name,user_parent_legal_guardian = :user_parent_legal_guardian,user_email = :user_email,user_phone = :user_phone,user_address = :user_address,user_city = :user_city,user_state = :user_state,user_zip = :user_zip,user_contact_method = :user_contact_method,user_dob = :user_dob,user_recall_frequency = :user_recall_frequency WHERE status=1 and id ='.$patient_id;

		$params_patient = array(

			"user_first_name" => $user_first_name,

			"user_last_name" => $user_last_name,

			"user_parent_legal_guardian" => $user_parent_legal_guardian,

			"user_email" => $user_email,

			"user_phone" => $user_phone,

			"user_address" => $user_address,

			"user_city" => $user_city,

			"user_state" => $user_state,

			"user_zip" => $user_zip,

			"user_contact_method" => $user_contact_method,

			"user_dob" => $user_dob,

			"user_recall_frequency" => $user_recall_frequency

		);

		$command_patient=$connection->createCommand($sql_patient);

		$command_patient->execute($params_patient);

		echo 'True';

	}

	

	public function actionOffers($id = "")

	{

		if(!Yii::app()->session['logged_in'])

			$this->redirect(array('site/index'));

		$model = new stdClass();

		$model->doctor_id = $id;

		

		$condition = 'status = 1 and doctor_id =' . $model->doctor_id;

		$criteria = new CDbCriteria(array(

			'condition' => $condition,

        	'order' => 'id DESC'

		));

		

		$count=DoctorOffers::model()->count($criteria);

		$pages=new CPagination($count);

	

		// results per page

		$pages->pageSize=10;

		$pages->applyLimit($criteria);

		

		$usercriteria =DoctorOffers::model()->findAll($criteria);

		

		$sql_doctor_address='select * FROM da_doctor_address WHERE status=1 and doctor_id='.Yii::app()->session['logged_user_id'];

		$connection = Yii::app()->db;

		$command_doctor_address = $connection->createCommand($sql_doctor_address);

		$user_doctor_address = $command_doctor_address->queryAll();

		//print_r($user_doctor_address);

		$doctor_address = array();

		foreach ($user_doctor_address as $key=>$val) {

			$doctor_address[$val['id']] = $val['address'];

		}

		

		$this->render('offers',array(

			'dataProvider' => $usercriteria,'pages'=>$pages,'doctor_address' => $doctor_address,

		));

	}

	

	public function actionEditOffers($id = "")

	{

		if(!Yii::app()->session['logged_in'])

			$this->redirect(array('site/index'));

		

		if(Yii::app()->getRequest()->isPostRequest){

			if($id != ""){

				$model = DoctorOffers::model()->findByPk($id);

				$model->attributes=Yii::app()->getRequest()->getPost('DoctorOffers');

				$model->description = isset( $_REQUEST['DoctorOffers']['description'] ) ? $_REQUEST['DoctorOffers']['description'] : '';

				$model->address_id = Yii::app()->getRequest()->getPost('address_id');

				$model->status = 1;

				if($model->save()){

					Yii::app()->user->setFlash('editOffers','Your Special Offer Updated Sucessfully.');

					//$this->refresh();

					$this->redirect(array('doctor/offers/'.Yii::app()->session['logged_user_id']));

				}

			}else{

				$model = new DoctorOffers();

				$model->attributes=Yii::app()->getRequest()->getPost('DoctorOffers');

				$model->address_id = Yii::app()->getRequest()->getPost('address_id');

				$model->doctor_id = Yii::app()->session['logged_user_id'];

				$model->description = isset( $_REQUEST['DoctorOffers']['description'] ) ? $_REQUEST['DoctorOffers']['description'] : '';

				$model->date_created = date('Y-m-d h:i:s');

				$model->date_modified = date('Y-m-d h:i:s');

				$model->status = 1;

				if($model->save()){

					Yii::app()->user->setFlash('editOffers','Your Special Offer Added Sucessfully.');

					//$this->refresh();

					$this->redirect(array('doctor/offers/'.Yii::app()->session['logged_user_id']));

				}

			}

		} else{

			if($id != ""){

				$model = DoctorOffers::model()->findByPk($id);

				$model->id = $id;

				$usercriteria =DoctorOffers::model()->find("id=\"$model->id\"");

				

				$model->doctor_id=$usercriteria->doctor_id;

				$model->status=$usercriteria->status;

			}else

				$model = new DoctorOffers();

		}

		

		$sql_doctor_address='select * FROM da_doctor_address WHERE status=1 and doctor_id='.Yii::app()->session['logged_user_id'];

		$connection = Yii::app()->db;

		$command_doctor_address = $connection->createCommand($sql_doctor_address);

		$user_doctor_address = $command_doctor_address->queryAll();

		//print_r($user_doctor_address);

		$doctor_address = array();

		foreach ($user_doctor_address as $key=>$val) {

			$city = ($val['city'])?', '.$val['city']:'';

			$state = ($val['state'])?', '.$val['state']:'';

			$zip = ($val['zip'])?', '.$val['zip']:'';

			$doctor_address[$val['id']] = $val['address'].$city.$state.$zip;

		}

		

		$this->render('edit_offers',array(

			'model' => $model,'doctor_address' => $doctor_address,

		));

	}

	

	public function actionOffersAjaxRemove()

	{

		

		$connection = Yii::app()->db;

		$id = $_REQUEST['id'];

		$sql="UPDATE da_doctor_offers SET status = :status WHERE id ='".$id."'";

		$params = array(

			"status" => 0

		);

		$command=$connection->createCommand($sql);

		$command->execute($params);

		

	}

	

	public function actionAddressAjaxRemove()

	{

		

		$connection = Yii::app()->db;

		$id = $_REQUEST['id'];

		$sql="UPDATE da_doctor_address SET status = :status WHERE id ='".$id."'";

		$params = array(

			"status" => 0

		);

		$command=$connection->createCommand($sql);

		$command->execute($params);

		

	}

	

	public function actionAppointment($id = "")

	{

		if(!Yii::app()->session['logged_in'])

			$this->redirect(array('site/index'));

			

		/*$model->doctor_id = $id;

		$start_time = strtotime(date('Y-m-d'));

		$to_time = strtotime(date('Y-m-d', strtotime(' +1 day')));

		

		//$sql_doctor_address='select db.* FROM da_doctor_book as db WHERE status=1 and ( book_time <'.$to_time.' and book_time >='.$start_time.') and doctor_id='.Yii::app()->session['logged_user_id'];

		$sql_doctor_address='select db.*,dp.user_first_name as dp_name FROM da_doctor_book as db left join da_patient as dp on db.patient_id = dp.id WHERE db.status=1 and ( book_time <'.$to_time.' and book_time >='.$start_time.') and doctor_id='.Yii::app()->session['logged_user_id'];

		$connection = Yii::app()->db;

		$command_doctor_address = $connection->createCommand($sql_doctor_address);

		$user_patient_book = $command_doctor_address->queryAll();

		//Helpers::pre($user_patient_book);die;

		$user_calender_res = array();

		//echo date('Y-m-d h:i:s');

		foreach ($user_patient_book as $key=>$val) {

			$st_time = "2014-03-31 05:40";

			$end_time = "2014-03-31 05:45";

			$st_time_stamp = $val['book_time'];

			$end_time_stamp = $val['book_time']+($val['book_duration']*60);

			$user_calender_res[] =array(

								'id' => $val['id'],

								'title' => $val['dp_name'],

								'start' => "$st_time",

									//'start_time_stamp' => $st_time_stamp,

								'end' => "$end_time",

									//'end_time_stamp' => $end_time_stamp,

								'allDay' => false,

								'description' =>"<p>This is just a test event for source 1.<\/p><p>Nothing to see really.<\/p>",

					 			"borderColor" => "#1587bd",

								"textColor" => "#000000",

								"color" => "#9fc6e7",

							);

		}

		

		$events_code = json_encode($user_calender_res);*/

		$user_patient_book = '';

		$events_code = '';

		$user_calender_res = '';

		

		$connection = Yii::app()->db;

		$sql_address='select * FROM da_doctor_address WHERE status="1" and doctor_id="'.$id.'"';

		$command_address = $connection->createCommand($sql_address);

		$user_address = $command_address->queryAll();

		$user_selected_address = array();

		foreach ($user_address as $key=>$val) {

			$city = ($val['city'])?', '.$val['city']:'';

			$state = ($val['state'])?', '.$val['state']:'';

			$zip = ($val['zip'])?', '.$val['zip']:'';

			$user_selected_address[$val['id']] = $val['address'].$city.$state.$zip;

		}

		

		$sql_procedure='select * FROM da_procedure WHERE status="1"';

		$command_procedure=$connection->createCommand($sql_procedure);

		$procedure_arr = $command_procedure->queryAll();

		$user_procedure_all = array();

		foreach ($procedure_arr as $procedure_arr_key=>$procedure_arr_val) {

			$user_procedure_all[$procedure_arr_val['id']] = $procedure_arr_val['procedure'];

		}

		

		$sql_speciality_procedure='select * FROM da_doctor_procedure WHERE status="1" and doctor_id="'.$id.'"';

		$command_speciality_procedure = $connection->createCommand($sql_speciality_procedure);

		$user_speciality_procedure = $command_speciality_procedure->queryAll();

		$user_procedure = array();

		foreach ($user_speciality_procedure as $key=>$val) {

			$user_procedure[] = $val['procedure_id'];

		}

		

		$sql_multy_speciality='select * FROM da_doctor_speciality WHERE status="1" and doctor_id="'.Yii::app()->session['logged_user_id'].'"';

		$command_multy_speciality = $connection->createCommand($sql_multy_speciality);

		$user_multy_speciality = $command_multy_speciality->queryAll();

		//helpers::pre($user_multy_speciality);

		$speciality_id = 0;

		if( count($user_multy_speciality) > 0 ) {

			$speciality_id = $user_multy_speciality[0]['speciality_id'];

		}

		//print $sql_reason_for_visit='select * FROM da_reason_for_visit WHERE status="1"';

		$sql_reason_for_visit='select * FROM da_reason_for_visit WHERE status="1" and speciality_id='.$speciality_id;

		$command_reason_for_visit = $connection->createCommand($sql_reason_for_visit);

		$user_reason_for_visit = $command_reason_for_visit->queryAll();

		$user_reason_for_visit_res = array();

		foreach ($user_reason_for_visit as $key=>$val) {

			$user_reason_for_visit_res[$val['id']] = $val['reason_for_visit'];

		}

		//helpers::pre($user_reason_for_visit_res);die;

		$this->render('appointment',array(

			'user_patient_book' => $user_patient_book,'events' => $events_code,'user_calender_res' => $user_calender_res,'user_selected_address' => $user_selected_address,'user_selected_procedure' => $user_procedure,'procedure_arr' => $user_procedure_all,'user_reason_for_visit' => $user_reason_for_visit_res,

		));

	}

	public function actionAjaxAppointmentToday($id = "")

	{

		if(!Yii::app()->session['logged_in'])

			$this->redirect(array('site/index'));

		$start_time = strtotime(date('Y-m-d'));

		$to_time = strtotime(date('Y-m-d', strtotime(' +1 day')));

		/*

		$strt_tm = $_REQUEST['start'];

		$end_tm = $_REQUEST['end'];

			

		$start_time = $strt_tm;

		$to_time = $end_tm;

		*/

		

		$sql_doctor_address='select db.*,dp.user_first_name as dp_name,da.address as da_address,dpro.procedure as dpro_procedure FROM da_doctor_book as db left join da_patient as dp on db.patient_id = dp.id left join da_doctor_address as da on db.address_id = da.id left join da_procedure as dpro on db.procedure_id = dpro.id WHERE db.status=1 and ( book_time <'.$to_time.' and book_time >='.$start_time.') and db.doctor_id='.Yii::app()->session['logged_user_id'];

		$connection = Yii::app()->db;

		$command_doctor_address = $connection->createCommand($sql_doctor_address);

		$user_patient_book = $command_doctor_address->queryAll();

		$user_calender_res = array();

		foreach ($user_patient_book as $key=>$val) {

			$st_time = date('Y-m-d h:i',$val['book_time']);

			$end_time = date('Y-m-d h:i',($val['book_time']+($val['book_duration']*60)));

			$user_calender_res[] =array(

								'id' => $val['id'],

								'title' => $val['dp_name'],

								'start' => "$st_time",

								'end' => "$end_time",

								'allDay' => false,

								'description' =>"<p><span>Procedure</span>: ".$val['dpro_procedure']."</p><p><span>Address</span>: ".$val['da_address']."</p>",

					 			"borderColor" => "#1587bd",

								"textColor" => "#000000",

								"color" => "#9fc6e7",

							);

		}

		

		echo $events_code = json_encode($user_calender_res);

		

	}

	public function actionAjaxAppointment($id = "")

	{

		/*$strt_tm = date('Y-m-d',$_REQUEST['start']);

		$end_tm = date('Y-m-d',$_REQUEST['end']);*/

		$strt_tm = $_REQUEST['start'];

		$end_tm = $_REQUEST['end'];

		if(!Yii::app()->session['logged_in'])

			$this->redirect(array('site/index'));

			

		$start_time = $strt_tm;

		$to_time = $end_tm;

		

		$sql_doctor_address='select db.*,concat_ws(" ", dp.user_first_name,  dp.user_last_name ) as dp_name,da.address as da_address,dpro.procedure as dpro_procedure,dp.created_by as dp_created_by FROM da_doctor_book as db left join da_patient as dp on db.patient_id = dp.id left join da_doctor_address as da on db.address_id = da.id left join da_procedure as dpro on db.procedure_id = dpro.id WHERE db.status=1 and ( book_time <'.$to_time.' and book_time >='.$start_time.') and db.doctor_id='.Yii::app()->session['logged_user_id'];

		$connection = Yii::app()->db;

		$command_doctor_address = $connection->createCommand($sql_doctor_address);

		$user_patient_book = $command_doctor_address->queryAll();

		$user_calender_res = array();

		foreach ($user_patient_book as $key=>$val) {

			$st_time = date('Y-m-d H:i',$val['book_time']);

			$end_time = date('Y-m-d H:i',($val['book_time']+($val['book_duration']*60)));

			if($val['confirm'] == 1){ $event_status = 'Confirmed'; $color = '#0000FF'; }

			else if($val['confirm'] == 2){ $event_status = 'Cancelled'; $color = '#ff7e00'; }

			else if($val['confirm'] == 3){ $event_status = 'Complete '; $color = '#0DAC15'; }

			else if($val['confirm'] == 4){ $event_status = 'No Show'; $color = '#E9051F'; }

			else{ $event_status = 'Scheduled'; $color = '#803611'; }

			$user_calender_res[] =array(

								'id' => $val['id'],

								'title' => ($val['dp_name'])?$val['dp_name']:$val['patient_name'],

								'start' => "$st_time",

								'end' => "$end_time",

								'book_duration' => $val['book_duration'],

								'created_by' => $val['dp_created_by'],

								'patient_id' => $val['patient_id'],

								'patient_name' => $val['patient_name'],

								//'event_status' => $val['event_status'],

								'event_status' => $event_status,

								'event_notes' => $val['event_notes'],

								'address_id' => $val['address_id'],

								//'procedure_id' => $val['procedure_id'],

								'procedure_id' => $val['reason_for_visit_id'],

								'allDay' => false,

								'description' =>"<p><span>Procedure</span>: ".$val['dpro_procedure']."</p><p><span>Address</span>: ".$val['da_address']."</p>",

					 			"borderColor" => "#1587bd",

								"textColor" => "#eef6fa",

								"color" => $color,

							);

		}

		

		echo $events_code = json_encode($user_calender_res);

		

	}

	

	public function actionAjaxAppointmentDelete($id = "")

	{

		//Helpers::pre($_POST);

		$id = $_POST['id'];

		$enter_by = $_POST['enter_by'];

		$connection = Yii::app()->db;

		if($id != ''){

			$sql='UPDATE da_doctor_book SET status = :status,created_by = :created_by WHERE status=1 and id ='.$id;

			$params = array(

				"status" => 0,

				"created_by" => $enter_by

			);

			$command=$connection->createCommand($sql);

			$command->execute($params);

			echo 'True';

		}

	}

	

	public function actionAjaxAppointmentUpdate($id = "")

	{

		//Helpers::pre($_POST);

		$id = $_POST['id'];

		$NewEventDate = $_POST['NewEventDate'];

		$NewEventTime = $_POST['NewEventTime'];

		$event_hr_arr = explode(':',$NewEventTime);

		/*if($event_hr_arr[0]>12){

			$event_hr = $event_hr_arr[0]-12;

			$book_time = strtotime($NewEventDate.' '.$event_hr.':'.$event_hr_arr[1].":00");

		}else{*/

			$book_time = strtotime($NewEventDate.' '.$NewEventTime.":00");

		//}

		$NewEventDuration = $_POST['NewEventDuration'];

		$enter_by = $_POST['enter_by'];

		$connection = Yii::app()->db;

		if($id != ''){

			$sql='UPDATE da_doctor_book SET book_time = :book_time,book_duration = :book_duration,date_modified = :date_modified,created_by = :created_by WHERE status=1 and id ='.$id;

			$params = array(

				"book_time" => $book_time,

				"book_duration" => $NewEventDuration,

				"date_modified" => date('Y-m-d h:i:s'),

				"created_by" => $enter_by

			);

			$command=$connection->createCommand($sql);

			$command->execute($params);

			echo 'True';

		}

	}

	

	public function actionAjaxAppointmentSave($id = "")

	{

		//Helpers::pre($_POST);

		$id = $_POST['id'];

		$NewEventDate = $_POST['NewEventDate'];

		$NewEventTime = $_POST['NewEventTime'];

		$event_hr_arr = explode(':',$NewEventTime);

		/*if($event_hr_arr[0]>12){

			$event_hr = $event_hr_arr[0]-12;

			$book_time = strtotime($NewEventDate.' '.$event_hr.':'.$event_hr_arr[1].":00");

		}else{*/

			$book_time = strtotime($NewEventDate.' '.$NewEventTime.":00");

		//}

		$NewEventDuration = $_POST['NewEventDuration'];

		$doctor_id = $_POST['doctor_id'];

		$address_id = $_POST['address_id'];

		$procedure_id = $_POST['procedure_id'];

		$enter_by = $_POST['enter_by'];

		$eventPerson = trim($_POST['eventPerson']);

		$patient_id = $_POST['patient_id'];

		$created_by = $_POST['created_by'];

		$eventStatus = $_POST['eventStatus'];

		$eventNotes = $_POST['eventNotes'];

		

		if($eventStatus == 'Confirmed')	{ $confirm = 1; }

		else if($eventStatus == 'Cancelled') {$confirm = 2; }

		else if($eventStatus == 'Complete')	{ $confirm = 3; }

		else if($eventStatus == 'NoShow') { $confirm = 4; }

		else { $confirm = 0; }

		

		$connection = Yii::app()->db;

		if($id != ''){

			if($created_by!='doctor'){

				$sql='UPDATE da_doctor_book SET book_time = :book_time,book_duration = :book_duration,address_id = :address_id,reason_for_visit_id = :reason_for_visit_id,date_modified = :date_modified,created_by = :created_by,event_status = :event_status,event_notes = :event_notes,confirm = :confirm WHERE status=1 and id ='.$id;

				$params = array(

					"book_time" => $book_time,

					"book_duration" => $NewEventDuration,

					"address_id" => $address_id,

					"reason_for_visit_id" => $procedure_id,

					"date_modified" => date('Y-m-d h:i:s'),

					"created_by" => $enter_by,

					"event_status" => $eventStatus,

					"event_notes" => $eventNotes,

					"confirm" => $confirm

				);

			}else{

				$eventPerson_arr = explode(' ',$eventPerson);

				if(count($eventPerson_arr)==1){

					$user_first_name = $eventPerson;

					$user_last_name = '';

				}else{

					$user_first_name = $eventPerson_arr[0];

					unset($eventPerson_arr[0]);

					$user_last_name = implode(' ',$eventPerson_arr);

				}

				$sql_patient='UPDATE da_patient SET user_first_name = :user_first_name,user_last_name = :user_last_name WHERE status=1 and id ='.$patient_id;

				$params_patient = array(

					"user_first_name" => $user_first_name,

					"user_last_name" => $user_last_name

				);

				$command_patient=$connection->createCommand($sql_patient);

				$command_patient->execute($params_patient);

				$sql='UPDATE da_doctor_book SET book_time = :book_time,book_duration = :book_duration,address_id = :address_id,reason_for_visit_id = :reason_for_visit_id,date_modified = :date_modified,created_by = :created_by,patient_name = :patient_name,event_status = :event_status,event_notes = :event_notes,confirm = :confirm  WHERE status=1 and id ='.$id;

				$params = array(

					"book_time" => $book_time,

					"book_duration" => $NewEventDuration,

					"address_id" => $address_id,

					"reason_for_visit_id" => $procedure_id,

					"date_modified" => date('Y-m-d h:i:s'),

					"created_by" => $enter_by,

					"patient_name" => $eventPerson,

					"event_status" => $eventStatus,

					"event_notes" => $eventNotes,

					"confirm" => $confirm

				);

			}

			$command=$connection->createCommand($sql);

			$command->execute($params);

			

			$user_det_temp =Patient::model()->find("id=\"$patient_id\"");

			$doctor_id_temp = Yii::app()->session['logged_user_id'];

			$doctor_det_temp =Doctor::model()->find("id=\"$doctor_id_temp\"");

			$state_det_temp_d = State::model()->find("short_code=\"$doctor_det_temp->state\"");

			$address_det_temp_p = DoctorAddress::model()->find("id=\"$address_id\"");

			$state_det_temp_p = State::model()->find("short_code=\"$address_det_temp_p->state\"");

			

			if($user_det_temp->user_email != ''){

				$temp_id = '';

				if($eventStatus == 'Scheduled') $temp_id = 16;

				if($eventStatus == 'Confirmed') $temp_id = 5;

				if($eventStatus == 'Cancelled') $temp_id = 7;

				if($temp_id != ''){

					$email_template = EmailTemplate::model()->find("id=\"$temp_id\"");

					if(!empty($email_template) && $email_template->tempalte_body != ''){

						$message_body = $email_template->tempalte_body;

						//$message_body = $this->getTemplateDataParsing($parse_with,$parse_data,$message_body);

						$parse_data_arr = array();

						$parse_data_arr['{{appointment.actual_format}}'] = date('l, F j - h:i A',$book_time);

						$parse_data_arr['{{appointment.day}}'] = date('l',$book_time);

						$parse_data_arr['{{appointment.month}}'] = date('F',$book_time);

						$parse_data_arr['{{appointment.date}}'] = date('j',$book_time);

						$parse_data_arr['{{appointment.time}}'] = date('h:i A',$book_time);

						

						$parse_data_arr['{{appointment.address_1}}'] = isset ( $address_det_temp_p->address ) ? $address_det_temp_p->address : $doctor_det_temp->addr1;

						$parse_data_arr['{{appointment.city}}'] = isset ( $address_det_temp_p->city ) ? $address_det_temp_p->city : $doctor_det_temp->city ;

						$parse_data_arr['{{appointment.state}}'] = isset( $state_det_temp_p->name ) ? $state_det_temp_p->name : '';

						$parse_data_arr['{{appointment.zip_code}}'] = isset ( $address_det_temp_p->zip ) ? $address_det_temp_p->zip : $doctor_det_temp->zip;

						

						$parse_data_arr['{{patient.user_full_name}}'] = $user_det_temp->user_first_name." ".$user_det_temp->user_last_name;

						$parse_data_arr['{{patient.user_first_name}}'] = $user_det_temp->user_first_name;

						$parse_data_arr['{{patient.user_last_name}}'] = $user_det_temp->user_last_name;

						

						$parse_data_arr['{{patient.user_title}}'] = $user_det_temp->patient_name;

						$parse_data_arr['{{patient.user_mobile_number}}'] = isset( $user_det_temp->user_phone ) ? $user_det_temp->user_phone :  '';

						$parse_data_arr['{{patient.user_home_number}}'] = isset( $user_det_temp->user_phone ) ? $user_det_temp->user_phone :  '';

						$parse_data_arr['{{patient.user_work_number}}'] = isset( $user_det_temp->user_phone ) ? $user_det_temp->user_phone :  '';

						$parse_data_arr['{{patient.user_fax_number}}'] = isset( $user_det_temp->user_phone ) ? $user_det_temp->user_phone :  '';

						$parse_data_arr['{{patient.user_other_number}}'] = isset( $user_det_temp->user_phone ) ? $user_det_temp->user_phone :  '';

						$parse_data_arr['{{patient.user_email}}'] = $user_det_temp->patient_email;

						$parse_data_arr['{{patient.user_address_1}}'] = isset( $user_det_temp->user_address ) ? $user_det_temp->user_address :  '';

						$parse_data_arr['{{patient.user_address_2}}'] = isset( $user_det_temp->user_address ) ? $user_det_temp->user_address : '';

						$parse_data_arr['{{patient.user_address_3}}'] = isset( $user_det_temp->user_address ) ? $user_det_temp->user_address :  '';

						$parse_data_arr['{{patient.user_city}}'] = $user_det_temp->user_city;

						$parse_data_arr['{{patient.user_post_code}}'] = $user_det_temp->user_zip;

						$parse_data_arr['{{patient.user_state}}'] = $user_det_temp->user_state;

						$parse_data_arr['{{patient.user_country}}'] = isset( $user_det_temp->patient_country ) ? $user_det_temp->patient_country :  '';

						$parse_data_arr['{{patient.user_dateOfBirth}}'] = isset( $user_det_temp->patient_dob ) ? date('d-m-Y' ,strtotime( $user_det_temp->patient_dob ) ) : '';

						$parse_data_arr['{{patient.user_gender}}'] = Ucfirst( $user_det_temp->patient_sex );

						

						$parse_data_arr['{firstname}'] = $user_det_temp->user_first_name;

						

						$parse_data_arr['{user}'] = $doctor_det_temp->first_name." ".$doctor_det_temp->last_name;

						$parse_data_arr['{{doctor.full_name}}'] = $doctor_det_temp->full_name;

						$parse_data_arr['{{doctor.first_name}}'] = $doctor_det_temp->first_name;

						$parse_data_arr['{{doctor.last_name}}'] = $doctor_det_temp->last_name;

						$parse_data_arr['{{doctor.address_1}}'] = $doctor_det_temp->addr1;

						$parse_data_arr['{{doctor.city}}'] = $doctor_det_temp->city;

						//$parse_data_arr['{{doctor.state}}'] = $state_det_temp_d->name;

						$parse_data_arr['{{doctor.zip_code}}'] = $doctor_det_temp->zip;

						$parse_data_arr['{{doctor.phone}}'] = $doctor_det_temp->phone;

						$parse_data_arr['{{doctor.email}}'] = $doctor_det_temp->email;

						$parse_data_arr['{{doctor.parctise_name}}'] = $doctor_det_temp->practice;

						

						/*

						$parse_data_arr = array();

						$parse_data_arr['{{appointment.actual_format}}'] = date('l, F j - h:i A',$book_time);

						$parse_data_arr['{{appointment.day}}'] = date('l',$book_time);

						$parse_data_arr['{{appointment.month}}'] = date('F',$book_time);

						$parse_data_arr['{{appointment.date}}'] = date('j',$book_time);

						$parse_data_arr['{{appointment.time}}'] = date('h:i A',$book_time);

						$parse_data_arr['{{patient.user_full_name}}'] = $user_det_temp->user_first_name." ".$user_det_temp->user_last_name;

						$parse_data_arr['{{patient.user_first_name}}'] = $user_det_temp->user_first_name;

						$parse_data_arr['{{patient.user_last_name}}'] = $user_det_temp->user_last_name;

						$parse_data_arr['{{doctor.full_name}}'] = $doctor_det_temp->first_name." ".$doctor_det_temp->last_name;

						$parse_data_arr['{{doctor.first_name}}'] = $doctor_det_temp->first_name;

						$parse_data_arr['{{doctor.last_name}}'] = $doctor_det_temp->last_name;

						*/

						$message_body = $this->getTemplateDataParsing($parse_data_arr,$message_body);

					}else{

						if($temp_id == 16){

							$message_body = '<h2 style="color:#636363; margin:0px; padding-bottom:10px; padding-left:8px; padding-right:8px; font-size:13px; font-weight:bold;">Your Appointment has now been Booked.</h2>Your Appointment Time is '.$app_time.'.';

						}

						if($temp_id == 3){

							$message_body = '<h2 style="color:#636363; margin:0px; padding-bottom:10px; padding-left:8px; padding-right:8px; font-size:13px; font-weight:bold;">Your Appointment has now been Confirmed.</h2>Your Appointment Time is '.$app_time.'.';

						}

						if($temp_id == 7){

							$message_body = '<h2 style="color:#636363; margin:0px; padding-bottom:10px; padding-left:8px; padding-right:8px; font-size:13px; font-weight:bold;">Your Appointment has now been Cancelled by Doctor.</h2>';

						}

					}

					

					$to_email = $user_det_temp->user_email;

					$to_name = $user_det_temp->user_first_name." ".$user_det_temp->user_last_name;

					$to = array($to_email,$to_name);

					$from = array('info.doctor@gmail.com','eDoctorBook');

					//$subject = 'eDoctorBook - Appointment Book.';

					$subject = isset( $email_template->subject ) ? $email_template->subject : 'eDoctorBook - Appointment Book.';

					$message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

					<html xmlns="http://www.w3.org/1999/xhtml">

					<head>

					<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

					<title>Doctor Email Template</title>

					</head>

					<body style="padding:0px; margin:0px; font-size:12px; color:#545454; line-height:18px; font-family:Arial, Helvetica, sans-serif;">

					<div style="width:450px;  margin:5px auto; border:1px solid #c7c7c7; "> 

					<div style="text-align:left; margin-bottom:15px; -moz-box-shadow:0px 1px 1px 1px #E4E4E4; -webkit-box-shadow:0px 1px 1px 1px #E4E4E4;box-shadow:0px 1px 1px 1px #E4E4E4; background:#54cbc8; font-weight:bold;">

					<a href="'.$this->createAbsoluteUrl('site/index/').'"><img src="http://'.$_SERVER['HTTP_HOST'].Yii::app()->getHomeUrl().'/assets/images/small_logo.png" style="padding:3px;" alt="Doctor Appointment Book" /></a>

					&nbsp;

					</div>	



					<h1 style="text-align:center; display:block; font-weight:normal; margin:0px; padding-bottom:12px; font-size:19px; line-height:26px; color:#4c4c4c;">Thank you '.$to_name.' for using <br /> <font style="color:#6ad2cf">eDoctorBook</font>!</h1>



					<div style="min-height:200px; background:#fff; color:#bbb; margin:10px; padding-top:15px; font-size:12px;"><pre style="color:#000">'.$message_body.'</pre>

					<br>

					

					</div>

					<p style=" margin-top:10px; padding:8px 5px; color:#000; font-size:13px; margin-bottom:0px; font-weight:bold; line-height:18px; ">eDoctorBook Team</p>

					<a style="padding-left:5px; padding-right:5px; color:#000; font-size:13px; font-weight:bold; margin:0px; line-height:0px; text-decoration:none" href="">http://www.eDoctorBook.com</a>

					<div style="background:#54cbc8; padding-top:20px; margin-top:15px; padding-bottom:10px; text-align:center; color:#e2e2e2; font-size:14px;"></div>

					</div>



					</body>

					</html>';		

					Helpers::mailsend($to,$from,$subject,$message);

				}

			}

			

			

			echo 'True';

		}else{

			$eventPerson_arr = explode(' ',$eventPerson);

			if(count($eventPerson_arr)==1){

				$user_first_name = $eventPerson;

				$user_last_name = '';

			}else{

				$user_first_name = $eventPerson_arr[0];

				unset($eventPerson_arr[0]);

				$user_last_name = implode(' ',$eventPerson_arr);

			}

			//echo count($eventPerson_arr);die;

			/*$sql_patient='INSERT INTO da_patient (user_first_name, user_last_name, date_created, created_by, status) VALUES(:user_first_name,:user_last_name,:date_created,:created_by,:status)';

			$params_patient = array(

				"user_first_name" => $user_first_name,

				"user_last_name" => $user_last_name,

				"date_created" => date('Y-m-d h:i:s'),

				"created_by" => $enter_by,

				"status" => 1

			);

			$command_patient=$connection->createCommand($sql_patient);

			$command_patient->execute($params_patient);

			$patient_inserted_id = Yii::app()->db->getLastInsertId();*/

			$sql='INSERT INTO da_doctor_book (patient_id,book_time, book_duration, doctor_id, address_id,reason_for_visit_id, date_created, created_by, patient_name, status, event_status, event_notes, confirm) VALUES(:patient_id,:book_time,:book_duration,:doctor_id,:address_id,:reason_for_visit_id,:date_created,:created_by,:patient_name,:status,:event_status,:event_notes,:confirm)';

			$params = array(

				//"patient_id" => $patient_inserted_id,

				"patient_id" => $patient_id,

				"book_time" => $book_time,

				"book_duration" => $NewEventDuration,

				"doctor_id" => $doctor_id,

				"address_id" => $address_id,

				"reason_for_visit_id" => $procedure_id,

				"date_created" => date('Y-m-d h:i:s'),

				"created_by" => $enter_by,

				"patient_name" => $eventPerson,

				"status" => 1,

				"event_status" => $eventStatus,

				"event_notes" => $eventNotes,

				"confirm" => $confirm

			);

			$command=$connection->createCommand($sql);

			$command->execute($params);

			

			$user_det_temp =Patient::model()->find("id=\"$patient_id\"");

			$doctor_id_temp = Yii::app()->session['logged_user_id'];

			$doctor_det_temp =Doctor::model()->find("id=\"$doctor_id_temp\"");		

			$state_det_temp_d = State::model()->find("short_code=\"$doctor_det_temp->state\"");

			

			$address_det_temp_p = DoctorAddress::model()->find("id=\"$address_id\"");

			$state_det_temp_p = State::model()->find("short_code=\"$address_det_temp_p->state\"");

			

			if($user_det_temp->user_email != ''){

				$temp_id = '';

				if($eventStatus == 'Scheduled') $temp_id = 16;

				if($eventStatus == 'Confirmed') $temp_id = 3;

				if($eventStatus == 'Cancelled') $temp_id = 7;

				if($temp_id != ''){

					$email_template = EmailTemplate::model()->find("id=\"$temp_id\"");

					if(!empty($email_template) && $email_template->tempalte_body != ''){

						$message_body = $email_template->tempalte_body;

						//$message_body = $this->getTemplateDataParsing($parse_with,$parse_data,$message_body);

						$parse_data_arr = array();

						$parse_data_arr['{{appointment.actual_format}}'] = date('l, F j - h:i A',$book_time);

						$parse_data_arr['{{appointment.day}}'] = date('l',$book_time);

						$parse_data_arr['{{appointment.month}}'] = date('F',$book_time);

						$parse_data_arr['{{appointment.date}}'] = date('j',$book_time);

						$parse_data_arr['{{appointment.time}}'] = date('h:i A',$book_time);

						

						$parse_data_arr['{{appointment.address_1}}'] = isset ( $address_det_temp_p->address ) ? $address_det_temp_p->address : $doctor_det_temp->addr1;

						$parse_data_arr['{{appointment.city}}'] = isset ( $address_det_temp_p->city ) ? $address_det_temp_p->city : $doctor_det_temp->city ;

						$parse_data_arr['{{appointment.state}}'] = isset( $state_det_temp_p->name ) ? $state_det_temp_p->name : $state_det_temp_d->name;

						$parse_data_arr['{{appointment.zip_code}}'] = isset ( $address_det_temp_p->zip ) ? $address_det_temp_p->zip : $doctor_det_temp->zip;

						

						$parse_data_arr['{{patient.user_full_name}}'] = $user_det_temp->user_first_name." ".$user_det_temp->user_last_name;

						$parse_data_arr['{{patient.user_first_name}}'] = $user_det_temp->user_first_name;

						$parse_data_arr['{{patient.user_last_name}}'] = $user_det_temp->user_last_name;

						

						$parse_data_arr['{{patient.user_title}}'] = $user_det_temp->patient_name;

						$parse_data_arr['{{patient.user_mobile_number}}'] = isset( $user_det_temp->user_phone ) ? $user_det_temp->user_phone :  '';

						$parse_data_arr['{{patient.user_home_number}}'] = isset( $user_det_temp->user_phone ) ? $user_det_temp->user_phone :  '';

						$parse_data_arr['{{patient.user_work_number}}'] = isset( $user_det_temp->user_phone ) ? $user_det_temp->user_phone :  '';

						$parse_data_arr['{{patient.user_fax_number}}'] = isset( $user_det_temp->user_phone ) ? $user_det_temp->user_phone :  '';

						$parse_data_arr['{{patient.user_other_number}}'] = isset( $user_det_temp->user_phone ) ? $user_det_temp->user_phone :  '';

						$parse_data_arr['{{patient.user_email}}'] = $user_det_temp->patient_email;

						$parse_data_arr['{{patient.user_address_1}}'] = isset( $user_det_temp->user_address ) ? $user_det_temp->user_address :  '';

						$parse_data_arr['{{patient.user_address_2}}'] = isset( $user_det_temp->user_address ) ? $user_det_temp->user_address : '';

						$parse_data_arr['{{patient.user_address_3}}'] = isset( $user_det_temp->user_address ) ? $user_det_temp->user_address :  '';

						$parse_data_arr['{{patient.user_city}}'] = $user_det_temp->user_city;

						$parse_data_arr['{{patient.user_post_code}}'] = $user_det_temp->user_zip;

						$parse_data_arr['{{patient.user_state}}'] = $user_det_temp->user_state;

						$parse_data_arr['{{patient.user_country}}'] = isset( $user_det_temp->patient_country ) ? $user_det_temp->patient_country :  '';

						$parse_data_arr['{{patient.user_dateOfBirth}}'] = isset( $user_det_temp->patient_dob ) ? date('d-m-Y' ,strtotime( $user_det_temp->patient_dob ) ) : '';

						$parse_data_arr['{{patient.user_gender}}'] = Ucfirst( $user_det_temp->patient_sex );

						

						$parse_data_arr['{firstname}'] = $user_det_temp->user_first_name;

						

						$parse_data_arr['{user}'] = $doctor_det_temp->first_name." ".$doctor_det_temp->last_name;

						$parse_data_arr['{{doctor.full_name}}'] = $doctor_det_temp->full_name;

						$parse_data_arr['{{doctor.first_name}}'] = $doctor_det_temp->first_name;

						$parse_data_arr['{{doctor.last_name}}'] = $doctor_det_temp->last_name;

						$parse_data_arr['{{doctor.address_1}}'] = $doctor_det_temp->addr1;

						$parse_data_arr['{{doctor.city}}'] = $doctor_det_temp->city;

						//$parse_data_arr['{{doctor.state}}'] = $state_det_temp_d->name;

						$parse_data_arr['{{doctor.zip_code}}'] = $doctor_det_temp->zip;

						$parse_data_arr['{{doctor.phone}}'] = $doctor_det_temp->phone;

						$parse_data_arr['{{doctor.email}}'] = $doctor_det_temp->email;

						$parse_data_arr['{{doctor.parctise_name}}'] = $doctor_det_temp->practice;

						

						/*

						$parse_data_arr = array();

						$parse_data_arr['{{appointment.actual_format}}'] = date('l, F j - h:i A',$book_time);

						$parse_data_arr['{{appointment.day}}'] = date('l',$book_time);

						$parse_data_arr['{{appointment.month}}'] = date('F',$book_time);

						$parse_data_arr['{{appointment.date}}'] = date('j',$book_time);

						$parse_data_arr['{{appointment.time}}'] = date('h:i A',$book_time);

						$parse_data_arr['{{patient.user_full_name}}'] = $user_det_temp->user_first_name." ".$user_det_temp->user_last_name;

						$parse_data_arr['{{patient.user_first_name}}'] = $user_det_temp->user_first_name;

						$parse_data_arr['{{patient.user_last_name}}'] = $user_det_temp->user_last_name;

						$parse_data_arr['{{doctor.full_name}}'] = $doctor_det_temp->first_name." ".$doctor_det_temp->last_name;

						$parse_data_arr['{{doctor.first_name}}'] = $doctor_det_temp->first_name;

						$parse_data_arr['{{doctor.last_name}}'] = $doctor_det_temp->last_name;

						*/

						$message_body = $this->getTemplateDataParsing($parse_data_arr,$message_body);

					}else{

						if($temp_id == 16){

							$message_body = '<h2 style="color:#636363; margin:0px; padding-bottom:10px; padding-left:8px; padding-right:8px; font-size:13px; font-weight:bold;">Your Appointment has now been Booked.</h2>Your Appointment Time is '.$app_time.'.';

						}

						if($temp_id == 3){

							$message_body = '<h2 style="color:#636363; margin:0px; padding-bottom:10px; padding-left:8px; padding-right:8px; font-size:13px; font-weight:bold;">Your Appointment has now been Confirmed.</h2>Your Appointment Time is '.$app_time.'.';

						}

						if($temp_id == 7){

							$message_body = '<h2 style="color:#636363; margin:0px; padding-bottom:10px; padding-left:8px; padding-right:8px; font-size:13px; font-weight:bold;">Your Appointment has now been Cancelled by Doctor.</h2>';

						}

					}

					

					$to_email = $user_det_temp->user_email;

					$to_name = $user_det_temp->user_first_name." ".$user_det_temp->user_last_name;

					$to = array($to_email,$to_name);

					$from = array('support@edoctorbook.com','eDoctorBook');

					//$subject = 'eDoctorBook - Appointment Book.';

					$subject = isset( $email_template->subject ) ? $email_template->subject : 'eDoctorBook - Appointment Book.';

					$message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

					<html xmlns="http://www.w3.org/1999/xhtml">

					<head>

					<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

					<title>Doctor Email Template</title>

					</head>

					<body style="padding:0px; margin:0px; font-size:12px; color:#545454; line-height:18px; font-family:Arial, Helvetica, sans-serif;">

					<div style="width:450px;  margin:5px auto; border:1px solid #c7c7c7; "> 

					<div style="text-align:left; margin-bottom:15px; -moz-box-shadow:0px 1px 1px 1px #E4E4E4; -webkit-box-shadow:0px 1px 1px 1px #E4E4E4;box-shadow:0px 1px 1px 1px #E4E4E4; background:#54cbc8; font-weight:bold;">

					<a href="'.$this->createAbsoluteUrl('site/index/').'"><img src="http://'.$_SERVER['HTTP_HOST'].Yii::app()->getHomeUrl().'/assets/images/small_logo.png" style="padding:3px;" alt="Doctor Appointment Book" /></a>

					&nbsp;

					</div>	



					<h1 style="text-align:center; display:block; font-weight:normal; margin:0px; padding-bottom:12px; font-size:19px; line-height:26px; color:#4c4c4c;">Thank you '.$to_name.' for using <br /> <font style="color:#6ad2cf">eDoctorBook</font>!</h1>



					<div style="min-height:200px; background:#fff; color:#bbb; margin:10px; padding-top:15px; font-size:12px;"><pre style="color:#000">'.$message_body.'</pre>

					<br>

					

					</div>

					<p style=" margin-top:10px; padding:8px 5px; color:#000; font-size:13px; margin-bottom:0px; font-weight:bold; line-height:18px; ">eDoctorBook Team</p>

					<a style="padding-left:5px; padding-right:5px; color:#000; font-size:13px; font-weight:bold; margin:0px; line-height:0px; text-decoration:none" href="">http://www.eDoctorBook.com</a>

					<div style="background:#54cbc8; padding-top:20px; margin-top:15px; padding-bottom:10px; text-align:center; color:#e2e2e2; font-size:14px;"></div>

					</div>



					</body>

					</html>';

					

				Helpers::mailsend($to,$from,$subject,$message);

				}

			}

			echo 'True';

		}

	}

	

	public function actionSchedule($id = "")

	{

		if(!Yii::app()->session['logged_in'])

			$this->redirect(array('site/index'));

		

		$model = new stdClass();

		$model->doctor_id = $id;

		

		$condition = 'status = 1 and doctor_id =' . $model->doctor_id;

		$criteria = new CDbCriteria(array(

			'condition' => $condition,

        	'order' => 'id DESC'

		));

		

		$count=DoctorSchedule::model()->count($criteria);

		$pages=new CPagination($count);

	

		// results per page

		$pages->pageSize=10;

		$pages->applyLimit($criteria);

		

		$usercriteria =DoctorSchedule::model()->findAll($criteria);

		

		$sql_doctor_address='select * FROM da_doctor_address WHERE status=1 and doctor_id='.Yii::app()->session['logged_user_id'];

		$connection = Yii::app()->db;

		$command_doctor_address = $connection->createCommand($sql_doctor_address);

		$user_doctor_address = $command_doctor_address->queryAll();

		//print_r($user_doctor_address);

		$doctor_address = array();

		$doctor_address_ = array();

		foreach ($user_doctor_address as $key=>$val) {

			$doctor_address[$val['id']] = $val['address'];

			$doctor_address_[$val['id']] = $val;

		}

		

		$this->render('schedule',array(

			'dataProvider' => $usercriteria,'pages'=>$pages,'doctor_address' => $doctor_address,'doctor_address_' => $doctor_address_,

		));

	}

	

	public function actionEditSchedule($id = "")

	{

		if(!Yii::app()->session['logged_in'])

			$this->redirect(array('site/index'));

		

		if(Yii::app()->getRequest()->isPostRequest){

			if($id != ""){

				$model = DoctorSchedule::model()->findByPk($id);

				$model->attributes=Yii::app()->getRequest()->getPost('DoctorSchedule');

				$model->address_id = Yii::app()->getRequest()->getPost('address_id');

				$model->status = 1;

				if($model->save()){

					Yii::app()->user->setFlash('editSchedules','Your Schedule Updated Sucessfully.');

					//$this->refresh();

					$this->redirect(array('doctor/schedule/'.Yii::app()->session['logged_user_id']));

				}

			}else{

				$model = new DoctorSchedule();

				$model->attributes=Yii::app()->getRequest()->getPost('DoctorSchedule');

				$model->address_id = Yii::app()->getRequest()->getPost('address_id');

				$model->doctor_id = Yii::app()->session['logged_user_id'];

				$model->date_created = date('Y-m-d h:i:s');

				$model->date_modified = date('Y-m-d h:i:s');

				$model->status = 1;

				if($model->save()){

					$connection = Yii::app()->db;

					foreach($model->dayOptions as $dayOptionsVal){

						$sql='INSERT INTO da_doctor_schedule_time (schedule_id, day, on_off, from_time, to_time, from_time_format, to_time_format, time_slot, date_created, status) VALUES(:schedule_id,:day,:on_off,:from_time,:to_time,:from_time_format,:to_time_format,:time_slot,:date_created,:status)';

						$on_off = 1;

						if($dayOptionsVal == 'Saturday' || $dayOptionsVal == 'Sunday')

							$on_off = 0;

						$params = array(

							"schedule_id" => $model->id,

							"day" => $dayOptionsVal,

							"on_off" => $on_off,

							"from_time" => '08:00:00',

							"to_time" => '05:00:00',

							"from_time_format" => 'am',

							"to_time_format" => 'pm',

							"time_slot" => 30,

							"date_created" => date('Y-m-d h:i:s'),

							"status" => 1

						);

						

						$command=$connection->createCommand($sql);

						$command->execute($params);

					}

					Yii::app()->user->setFlash('editSchedules','Your Schedule Added Sucessfully.');

					//$this->refresh();

					$this->redirect(array('doctor/schedule/'.Yii::app()->session['logged_user_id']));

				}

			}

		} else{

			if($id != ""){

				$model = DoctorSchedule::model()->findByPk($id);

				$model->id = $id;

				$usercriteria =DoctorSchedule::model()->find("id=\"$model->id\"");

				

				$model->doctor_id=$usercriteria->doctor_id;

				$model->status=$usercriteria->status;

			}else

				$model = new DoctorSchedule();

		}

		

		$sql_doctor_address='select * FROM da_doctor_address WHERE status=1 and doctor_id='.Yii::app()->session['logged_user_id'];

		$connection = Yii::app()->db;

		$command_doctor_address = $connection->createCommand($sql_doctor_address);

		$user_doctor_address = $command_doctor_address->queryAll();

		/*echo "<pre>";

		print_r($user_doctor_address);*/

		$doctor_address = array();

		foreach ($user_doctor_address as $key=>$val) {

			$doctor_address[$val['id']] = $val['address'].", ".$val['city'].", ".$val['state'];

		}

		

		$this->render('edit_schedule',array(

			'model' => $model,'doctor_address' => $doctor_address,

		));

	}

	

	public function actionScheduleAjaxRemove()

	{

		

		$connection = Yii::app()->db;

		$id = $_REQUEST['id'];

		$sql="UPDATE da_doctor_schedule SET status = :status WHERE id ='".$id."'";

		$params = array(

			"status" => 0

		);

		$command=$connection->createCommand($sql);

		$command->execute($params);

		

	}

	

	/*public function actionScheduleTime($id = "")

	{

		if(!Yii::app()->session['logged_in'])

			$this->redirect(array('site/index'));

			

		$model->schedule_id = $id;

		$condition = 'status = 1 and schedule_id =' . $model->schedule_id;

		$criteria = new CDbCriteria(array(

			'condition' => $condition,

        	'order' => 'id DESC'

		));

		

		$count=DoctorScheduleTime::model()->count($criteria);

		$pages=new CPagination($count);

	

		// results per page

		$pages->pageSize=10;

		$pages->applyLimit($criteria);

		

		$usercriteria =DoctorScheduleTime::model()->findAll($criteria);

		

		$sql_doctor_address='select * FROM da_doctor_address WHERE status=1 and doctor_id='.Yii::app()->session['logged_user_id'];

		$connection = Yii::app()->db;

		$command_doctor_address = $connection->createCommand($sql_doctor_address);

		$user_doctor_address = $command_doctor_address->queryAll();

		//print_r($user_doctor_address);

		$doctor_address = array();

		foreach ($user_doctor_address as $key=>$val) {

			$doctor_address[$val['id']] = $val['address'];

		}

		

		$this->render('schedule_time',array(

			'dataProvider' => $usercriteria,'doctor_address' => $doctor_address,'doctor_schedule_data_id' => $id,'pages'=>$pages,

		));

	}*/

	

	public function actionScheduleTime($id = "")

	{

		if(!Yii::app()->session['logged_in'])

			$this->redirect(array('site/index'));

		$model = new stdClass();

		$model->schedule_id = $id;

		

		$usercriteria =DoctorScheduleTime::model()->findAllByAttributes(array('schedule_id'=>$model->schedule_id,'status'=>1));

		if($_POST){

			$connection = Yii::app()->db;

			for($i=0;$i<7;$i++){

				$sql='UPDATE da_doctor_schedule_time SET on_off = :on_off,from_time = :from_time,to_time = :to_time,from_time_format = :from_time_format,to_time_format = :to_time_format,time_slot = :time_slot,laser_slot = :laser_slot WHERE id ='.$_POST['hidden_id'][$i];

				$on_off = isset($_POST['on_off_'.$i])?1:0;

				

				$from_time_arr =explode(":",$_POST['from_time'][$i]);

				$from_time_arr2 =explode(" ",$from_time_arr[1]);

				$from_time = $from_time_arr[0].':'.$from_time_arr2[0];

				

				$to_time_arr =explode(":",$_POST['to_time'][$i]);

				$to_time_arr2 =explode(" ",$to_time_arr[1]);

				$to_time = $to_time_arr[0].':'.$to_time_arr2[0];

				

				$from_time_format = $from_time_arr2[1];

				$to_time_format = $to_time_arr2[1];

				$params = array(

					"on_off" => $on_off,

					"from_time" => $from_time,

					"to_time" => $to_time,

					"from_time_format" => $from_time_format,

					"to_time_format" => $to_time_format,

					"time_slot" => $_POST['time_slot'][$i],

					"laser_slot" => $_POST['laser_slot'][$i]

				);

				

				$command=$connection->createCommand($sql);

				$command->execute($params);

			}

		//Helpers::pre($_REQUEST);//die;

		Yii::app()->user->setFlash('editSchedulesTime','Your Schedule Time Updated Sucessfully.');

		$this->refresh();

		}

		

		/*if($usercriteria->from_time == "" || $usercriteria->from_time == '00:00:00'){

			$from_time['hh_mm'] = "";

		}else{

			$from_time_arr =explode(":",$usercriteria->from_time);

			$from_time['hh_mm'] = $from_time_arr[0].':'.$from_time_arr[1];

		}

		

		if($usercriteria->to_time == "" || $usercriteria->to_time == '00:00:00'){

			$to_time['hh_mm'] = "";

		}else{

			$to_time_arr =explode(":",$usercriteria->to_time);

			$to_time['hh_mm'] = $to_time_arr[0].':'.$to_time_arr[1];

		}*/

		

		$this->render('schedule_time_week',array(

			'dataProvider' => $usercriteria,'doctor_schedule_data_id' => $id,/*'from_time' => $from_time,'to_time' => $to_time,*/

		));

	}

	

	public function actionAddScheduleTime($id = "")

	{

		if(!Yii::app()->session['logged_in'])

			$this->redirect(array('site/index'));

		$redirect_id = $id;

		$sql_doctor_address='select * FROM da_doctor_address WHERE status=1 and doctor_id='.Yii::app()->session['logged_user_id'];

		$connection = Yii::app()->db;

		$command_doctor_address = $connection->createCommand($sql_doctor_address);

		$user_doctor_address = $command_doctor_address->queryAll();

		//print_r($user_doctor_address);

		$doctor_address = array();

		foreach ($user_doctor_address as $key=>$val) {

			$doctor_address[$val['id']] = $val['address'];

		}

		

		if(Yii::app()->getRequest()->isPostRequest){

				$model = new DoctorScheduleTime();

				$model->attributes=Yii::app()->getRequest()->getPost('DoctorScheduleTime');

				$model->schedule_id = $_POST['doctor_schedule_data_id'];

				$model->address_id = Yii::app()->getRequest()->getPost('address_id');

				$model->day = Yii::app()->getRequest()->getPost('day');

				$model->from_time_format = Yii::app()->getRequest()->getPost('from_time_format');

				$model->to_time_format = Yii::app()->getRequest()->getPost('to_time_format');

				$model->time_slot = Yii::app()->getRequest()->getPost('time_slot');

				$model->laser_slot = Yii::app()->getRequest()->getPost('laser_slot');

				$model->date_created = date('Y-m-d h:i:s');

				$model->date_modified = date('Y-m-d h:i:s');

				$model->status = 1;

				if($model->save()){

					Yii::app()->user->setFlash('editSchedulesTime','Your Schedule Time Inserted Sucessfully.');

					//$this->refresh();

					$this->redirect(array('doctor/scheduleTime/'.$redirect_id));

				}

		} else{

			$model = new DoctorScheduleTime();

		}

		

		if($model->from_time == "" || $model->from_time == '00:00:00'){

			$from_time['hh'] = "";

			$from_time['mm'] = "";

		}else{

			$from_time_arr =explode(":",$model->from_time);

			$from_time['hh'] = $from_time_arr[0];

			$from_time['mm'] = $from_time_arr[1];

		}

		

		if($model->to_time == "" || $model->to_time == '00:00:00'){

			$to_time['hh'] = "";

			$to_time['mm'] = "";

		}else{

			$to_time_arr =explode(":",$model->to_time);

			$to_time['hh'] = $to_time_arr[0];

			$to_time['mm'] = $to_time_arr[1];

		}

		

		$this->render('edit_schedule_time',array(

			'model' => $model,'doctor_address' => $doctor_address,'doctor_schedule_data_id' => $id,'from_time' => $from_time,'to_time' => $to_time,

		));

	}

	

	public function actionEditScheduleTime($id = "")

	{

		if(!Yii::app()->session['logged_in'])

			$this->redirect(array('site/index'));

		

		$sql_doctor_address='select * FROM da_doctor_address WHERE status=1 and doctor_id='.Yii::app()->session['logged_user_id'];

		$connection = Yii::app()->db;

		$command_doctor_address = $connection->createCommand($sql_doctor_address);

		$user_doctor_address = $command_doctor_address->queryAll();

		//print_r($user_doctor_address);

		$doctor_address = array();

		foreach ($user_doctor_address as $key=>$val) {

			$doctor_address[$val['id']] = $val['address'];

		}

		

		if(Yii::app()->getRequest()->isPostRequest){

			if($id != ""){

				$model = DoctorScheduleTime::model()->findByPk($id);

				$model->attributes=Yii::app()->getRequest()->getPost('DoctorScheduleTime');

				$model->address_id = Yii::app()->getRequest()->getPost('address_id');

				$model->day = Yii::app()->getRequest()->getPost('day');

				$model->from_time_format = Yii::app()->getRequest()->getPost('from_time_format');

				$model->to_time_format = Yii::app()->getRequest()->getPost('to_time_format');

				$model->time_slot = Yii::app()->getRequest()->getPost('time_slot');

				$model->laser_slot = Yii::app()->getRequest()->getPost('laser_slot');

				$model->status = 1;

				//print_r($_REQUEST);die;

				if($model->save()){

					Yii::app()->user->setFlash('editSchedulesTime','Your Schedule Time Updated Sucessfully.');

					//$this->refresh();

					$this->redirect(array('doctor/scheduleTime/'.$model->schedule_id));

				}

			}else{

				$model = new DoctorScheduleTime();

				$model->attributes=Yii::app()->getRequest()->getPost('DoctorScheduleTime');

				

				$model->schedule_id = Yii::app()->session['schedule_id'];

				

				$model->address_id = Yii::app()->getRequest()->getPost('address_id');

				$model->day = Yii::app()->getRequest()->getPost('day');

				$model->from_time_format = Yii::app()->getRequest()->getPost('from_time_format');

				$model->to_time_format = Yii::app()->getRequest()->getPost('to_time_format');

				$model->time_slot = Yii::app()->getRequest()->getPost('time_slot');

				$model->laser_slot = Yii::app()->getRequest()->getPost('laser_slot');

				$model->date_created = date('Y-m-d h:i:s');

				$model->date_modified = date('Y-m-d h:i:s');

				$model->status = 1;

				if($model->save()){

					Yii::app()->user->setFlash('editSchedulesTime','Your Schedule Time Updated Sucessfully.');

					$this->refresh();

				}

			}

		} else{

			//if($id != "")

				$model = DoctorScheduleTime::model()->findByPk($id);

			//else

				//$model = new DoctorScheduleTime();

			$model->id = $id;

			$usercriteria =DoctorScheduleTime::model()->find("id=\"$model->id\"");

			

			$model->schedule_id=$usercriteria->schedule_id;

			$model->status=$usercriteria->status;

		}

		

		if($model->from_time == "" || $model->from_time == '00:00:00'){

			$from_time['hh'] = "";

			$from_time['mm'] = "";

		}else{

			$from_time_arr =explode(":",$model->from_time);

			$from_time['hh'] = $from_time_arr[0];

			$from_time['mm'] = $from_time_arr[1];

		}

		

		if($model->to_time == "" || $model->to_time == '00:00:00'){

			$to_time['hh'] = "";

			$to_time['mm'] = "";

		}else{

			$to_time_arr =explode(":",$model->to_time);

			$to_time['hh'] = $to_time_arr[0];

			$to_time['mm'] = $to_time_arr[1];

		}

		

		$this->render('edit_schedule_time',array(

			'model' => $model,'doctor_address' => $doctor_address,'doctor_schedule_data_id' => '','from_time' => $from_time,'to_time' => $to_time,

		));

	}

	

	public function actionScheduleTimeAjaxRemove()

	{

		

		$connection = Yii::app()->db;

		$id = $_REQUEST['id'];

		$sql="UPDATE da_doctor_schedule_time SET status = :status WHERE id ='".$id."'";

		$params = array(

			"status" => 0

		);

		$command=$connection->createCommand($sql);

		$command->execute($params);

		

	}

	

	public function actionTimeoff($id = "")

	{

		if(!Yii::app()->session['logged_in'])

			$this->redirect(array('site/index'));

		$model = new stdClass();

		$model->doctor_id = $id;

		

		$condition = 'status = 1 and doctor_id =' . $model->doctor_id;

		$criteria = new CDbCriteria(array(

			'condition' => $condition,

        	'order' => 'id DESC'

		));

		

		$count=DoctorTimeoff::model()->count($criteria);

		$pages=new CPagination($count);

	

		// results per page

		$pages->pageSize=10;

		$pages->applyLimit($criteria);

		

		$usercriteria =DoctorTimeoff::model()->findAll($criteria);

		//echo '<pre>';print_r($usercriteria);

		$this->render('timeoff',array(

			'dataProvider' => $usercriteria,'pages'=>$pages,

		));

	}

	

	public function actionEditTimeoff($id = "")

	{

		if(!Yii::app()->session['logged_in'])

			$this->redirect(array('site/index'));

		

		if(Yii::app()->getRequest()->isPostRequest){

			if($id != ""){

				$model = DoctorTimeoff::model()->findByPk($id);

				$model->attributes=Yii::app()->getRequest()->getPost('DoctorTimeoff');

				

				$model->from_time_format = Yii::app()->getRequest()->getPost('from_time_format');

				$model->to_time_format = Yii::app()->getRequest()->getPost('to_time_format');

				

				$model->status = 1;

				if($model->save()){

					Yii::app()->user->setFlash('editTimeoff','Your Timeoff Updated Sucessfully.');

					//$this->refresh();

					$this->redirect(array('doctor/timeoff/'.Yii::app()->session['logged_user_id']));

				}

			}else{

				$model = new DoctorTimeoff();

				$model->attributes=Yii::app()->getRequest()->getPost('DoctorTimeoff');

				$model->doctor_id = Yii::app()->session['logged_user_id'];

				

				$model->from_time_format = Yii::app()->getRequest()->getPost('from_time_format');

				$model->to_time_format = Yii::app()->getRequest()->getPost('to_time_format');

				

				$model->date_created = date('Y-m-d h:i:s');

				$model->date_modified = date('Y-m-d h:i:s');

				$model->status = 1;

				if($model->save()){

					Yii::app()->user->setFlash('editTimeoff','Your Timeoff Inserted Sucessfully.');

					//$this->refresh();

					$this->redirect(array('doctor/timeoff/'.Yii::app()->session['logged_user_id']));

				}

			}

		} else{

			if($id != ""){

				$model = DoctorTimeoff::model()->findByPk($id);

				$model->id = $id;

				$usercriteria =DoctorTimeoff::model()->find("id=\"$model->id\"");

				

				$model->status=$usercriteria->status;

			}else

				$model = new DoctorTimeoff();

		}

		

		if($model->from_time == "" || $model->from_time == '00:00:00'){

			$from_time['hh'] = "";

			$from_time['mm'] = "";

		}else{

			$from_time_arr =explode(":",$model->from_time);

			$from_time['hh'] = $from_time_arr[0];

			$from_time['mm'] = $from_time_arr[1];

		}

		

		if($model->to_time == "" || $model->to_time == '00:00:00'){

			$to_time['hh'] = "";

			$to_time['mm'] = "";

		}else{

			$to_time_arr =explode(":",$model->to_time);

			$to_time['hh'] = $to_time_arr[0];

			$to_time['mm'] = $to_time_arr[1];

		}

		

		$this->render('edit_timeoff',array(

			'model' => $model,'from_time' => $from_time,'to_time' => $to_time,

		));

	}

	

	public function actionTimeoffAjaxRemove()

	{

		

		$connection = Yii::app()->db;

		$id = $_REQUEST['id'];

		$sql="UPDATE da_doctor_timeoff SET status = :status WHERE id ='".$id."'";

		$params = array(

			"status" => 0

		);

		$command=$connection->createCommand($sql);

		$command->execute($params);

		

	}



	/**

	 * Manages all models.

	 */

	public function actionAdmin()

	{

		$model=new Doctor('search');

		$model->unsetAttributes();  // clear any default values

		if(isset($_GET['Doctor']))

			$model->attributes=$_GET['Doctor'];



		$this->render('admin',array(

			'model'=>$model,

		));

	}



	/**

	 * Returns the data model based on the primary key given in the GET variable.

	 * If the data model is not found, an HTTP exception will be raised.

	 * @param integer $id the ID of the model to be loaded

	 * @return Doctor the loaded model

	 * @throws CHttpException

	 */

	public function loadModel($id)

	{

		$model=Doctor::model()->findByPk($id);

		if($model===null)

			throw new CHttpException(404,'The requested page does not exist.');

		return $model;

	}



	/**

	 * Performs the AJAX validation.

	 * @param Doctor $model the model to be validated

	 */

	protected function performAjaxValidation($model)

	{

		if(isset($_POST['ajax']) && $_POST['ajax']==='doctor-form')

		{

			echo CActiveForm::validate($model);

			Yii::app()->end();

		}

	}

	

	public function actions()

	{

		return array(

			// captcha action renders the CAPTCHA image displayed on the contact page

			'captcha'=>array(

				'class'=>'CCaptchaAction',

				'backColor'=>0xFFFFFF,

			),

			// page action renders "static" pages stored under 'protected/views/site/pages'

			// They can be accessed via: index.php?r=site/page&view=FileName

			'page'=>array(

				'class'=>'CViewAction',

			),

		);

	}

	

	protected function removeCaptchaSession()

	 {

		$session = Yii::app()->session;

		$prefixLen = strlen(CCaptchaAction::SESSION_VAR_PREFIX);

		foreach ($session->keys as $key)

		{

			if (strncmp(CCaptchaAction::SESSION_VAR_PREFIX, $key, $prefixLen) == 0)

			$session->remove($key);

		}

	 }

	
	 /* ------------------- Doctor Search -------------------------------------------- */
	 
	 public function actionDoctorSearch() {
	 	 
	 	$name= Yii::app()->request->getParam('name_autocomp');
	 	$doctor_id = Yii::app()->request->getParam('doctor_id');
	 	$speciality_id = Yii::app()->request->getParam('speciality_id');
	 	$location = Yii::app()->request->getParam('location');
	 	$office_name = Yii::app()->request->getParam('office_name');
	 	$insurance = Yii::app()->request->getParam('insurance');
	 	$procedure = Yii::app()->request->getParam('procedure');
	 	$reason_for_visit = Yii::app()->request->getParam('reason_for_visit');
	 	$gender = Yii::app()->request->getParam('gender');
	 	$language = Yii::app()->request->getParam('language');
	 	$location_id = Yii::app()->request->getParam('geo_location_id');
	 	 
	 	$radius_array = array();
	 	$radius = array();
	 
	 	if(is_numeric($location_id)){
	 		if(!empty($location_id)){
	 			$distancemile = 10;
	 			$zipcode = trim(Yii::app()->request->getParam('geo_location_id'));
	 			$radius = $this->radiusSearch($zipcode,$distancemile);
	 			foreach($radius as $key=>$val) {
	 				$radius_array[] = TRIM($key);
	 			}
	 		}
	 	}
	 	 
	 	$usercriteria = array();
	 	$usercriteria=$_REQUEST;
	 	 
	 	$input_name = preg_replace("/[^a-zA-Z]+/", "", $name);
	 	 
	 	$searchCriteria = new CDbCriteria();
	 	$searchCriteria->condition = 't.status = :status AND t.email_verified = :email_verified ';
	 	$searchCriteria->params = array( ':status' => 1,':email_verified' => 1);
	 	$searchCriteria->join = ' INNER JOIN `da_doctor_address` `da` ON ( t.id = da.doctor_id AND da.status = 1 )';
	 	 
	 	if (isset($name) && !empty($name)) {
	 		//$searchCriteria->addSearchCondition('concat_ws( t.first_name, " ", t.last_name )', $input_name, true, 'AND');
	 		$searchCriteria->addSearchCondition('t.full_name',$input_name, true, 'AND');
	 	}
	 	if($gender != "" && ($gender == "M" || $gender == "F")){
	 		$searchCriteria->addSearchCondition('t.gender', $gender, true, 'AND');
	 	}
	 	if($speciality_id != ""){
	 		$searchCriteria->join .= ' LEFT JOIN `da_doctor_speciality` `ds` ON ( t.id = ds.doctor_id AND ds.status = 1 )';
	 		$searchCriteria->condition .= ' AND ds.speciality_id = :speciality';
	 		$searchCriteria->params = array_merge(array(':speciality' => $speciality_id ), $searchCriteria->params);
	 	}
	 	if($language != "" && ($language != "Any") ){
	 		$searchCriteria->join .= ' LEFT JOIN `da_doctor_language` `dl` ON ( t.id = dl.doctor_id )';
	 		$searchCriteria->condition .= ' AND dl.language_id = :language';
	 		$searchCriteria->params = array_merge(array(':language' => $language ), $searchCriteria->params);
	 	}
	 	if($insurance != ""){
	 		$searchCriteria->join .= ' LEFT JOIN `da_doctor_insurance` `di` ON ( t.id = di.doctor_id )';
	 		$searchCriteria->condition .= ' AND di.insurance_id = :insurance';
	 		$searchCriteria->params = array_merge(array(':insurance' => $insurance ), $searchCriteria->params);
	 	}
	 	if($procedure != ""){
	 		$searchCriteria->join .= ' LEFT JOIN `da_doctor_procedure` `dp` ON ( t.id=dp.doctor_id and dp.status=1 )';
	 		$searchCriteria->condition .= ' AND dp.procedure_id = :procedure';
	 		$searchCriteria->params = array_merge(array(':procedure' => $procedure ), $searchCriteria->params);
	 	}
	 	if($location_id != ""){
	 		if(count($radius_array)>0) {
	 			$radius_str = implode(', ', $radius_array);
	 			$searchCriteria->condition .= ' AND da.zip IN ('.$radius_str.')';
	 		}else{
	 			$location_id_arr = explode(',',$location_id);
	 			$location_id_arr_imp = array();
	 			if(!empty($location_id_arr)){
	 				if( count ( $location_id_arr ) == 1 ) {
	 					foreach($location_id_arr as $location_id_arr_val){
	 						$searchCriteria->condition .= ' AND ( da.city LIKE :city OR da.state LIKE :state OR da.zip = :zip )';
	 						$searchCriteria->params = array_merge(array( ':city' => trim($location_id_arr_val),':state' => trim($location_id_arr_val),':zip' => trim($location_id_arr_val)), $searchCriteria->params);
	 					}
	 				}else if(  count ( $location_id_arr ) == 2 ) {
	 					$searchCriteria->condition .= ' AND ( da.city LIKE :city AND da.state LIKE :state OR da.zip = :zip )';
	 					$searchCriteria->params = array_merge(array( ':city' => '%'.trim($location_id_arr[0]).'%',':state' => '%'.trim($location_id_arr[1]).'%',':zip' => trim($location_id_arr[1])), $searchCriteria->params);
	 				}else if(  count ( $location_id_arr ) == 3 ) {
	 					$searchCriteria->condition .= ' AND ( da.city LIKE :city AND da.state LIKE :state AND da.zip = :zip )';
	 					$searchCriteria->params = array_merge(array( ':city' => '%'.trim($location_id_arr[0]).'%',':state' => '%'.trim($location_id_arr[1]).'%',':zip' => trim($location_id_arr[2])), $searchCriteria->params);
	 
	 				} else {
	 					foreach($location_id_arr as $location_id_arr_val){
	 						$searchCriteria->condition .= ' AND ( da.address LIKE :address AND da.city LIKE :city OR da.state LIKE :state OR da.zip = :zip )';
	 						$searchCriteria->params = array_merge(array( ':address' => '%'.trim($location_id_arr_val).'%',':city' => '%'.trim($location_id_arr_val).'%',':state' => '%'.trim($location_id_arr_val).'%',':zip' => trim($location_id_arr_val)), $searchCriteria->params);
	 					}
	 				}
	 			}
	 		}
	 	}
	 	 
	 	$limit = 20;
	 	$count = Doctor::model()->count($searchCriteria);
	 	$pages = new CPagination($count);
	 	$pages->setPageSize(20);
	 	$pages->applyLimit($searchCriteria);
	 	$searchCriteria->group = 't.id';
	 	 
	 	//$searchCriteria->order = 'ABS(da.zip) DESC';
	 	 
	 	$doctor_list = Doctor::model()->findAll($searchCriteria);
	 	 
	 	$user_insurance_res = array();
	 	$user_speciality_res = array();
	 	$user_language_res = array();
	 	$user_procedure_res = array();
	 	$user_reason_for_visit_res = array();
	 	 
	 	$user_speciality_all = Speciality::model()->findAll(array(
	 			'condition' => 'status = :status',
	 			'params' => array(':status' => '1'),
	 	));
	 	if (count($user_speciality_all) > 0) {
	 		foreach ($user_speciality_all as $key=>$val) {
	 			$user_speciality_res[$val['id']] = $val['speciality'];
	 		}
	 	}
	 	$user_insurance_all = Insurance::model()->findAll(array(
	 			'condition' => 'status = :status',
	 			'params' => array(':status' => '1'),
	 	));
	 	if (count($user_insurance_all) > 0) {
	 		foreach ($user_insurance_all as $key=>$val) {
	 			$user_insurance_res[$val['id']] = $val['insurance'];
	 		}
	 	}
	 	 
	 	$user_language_all = Language::model()->findAll(array(
	 			'condition' => 'status = :status',
	 			'params' => array(':status' => '1'),
	 	));
	 	if (count($user_language_all) > 0) {
	 		foreach ($user_language_all as $key=>$val) {
	 			$user_language_res[$val['id']] = $val['language'];
	 		}
	 	}
	 	 
	 	$user_procedure_all = Procedure::model()->findAll(array(
	 			'condition' => 'status = :status',
	 			'params' => array(':status' => '1'),
	 	));
	 	if (count($user_procedure_all) > 0) {
	 		foreach ($user_procedure_all as $key=>$val) {
	 			$user_procedure_res[$val['id']] = $val['procedure'];
	 		}
	 	}
	 	 
	 	$user_reason_for_visit_all = ReasonForVisit::model()->findAll(array(
	 			'condition' => 'status = :status',
	 			'params' => array(':status' => '1'),
	 	));
	 	if (count($user_reason_for_visit_all) > 0) {
	 		foreach ($user_reason_for_visit_all as $key=>$val) {
	 			$user_reason_for_visit_res[$val['id']] = $val['reason_for_visit'];
	 		}
	 	}
	 	 
	 	$doc_id = 0;
	 	if( isset($doctor_list[0]['id'])){
	 		$doc_id = $doctor_list[0]['id'];
	 	}
	 	$user_selected_speciality = array();
	 	$user_selected_procedure = array();
	 	 
	 	$connection = Yii::app()->db;
	 	$sql_multy_speciality='select * FROM da_doctor_speciality WHERE status="1" and doctor_id="'.$doc_id.'"';
	 	$command_multy_speciality = $connection->createCommand($sql_multy_speciality);
	 	$user_multy_speciality = $command_multy_speciality->queryAll();
	 	$user_selected_speciality = array();
	 	foreach ($user_multy_speciality as $key=>$val) {
	 		$user_selected_speciality[] = $val['speciality_id'];
	 	}
	 	 
	 	$sql_multy_procedure='select * FROM da_doctor_procedure WHERE status="1" and doctor_id="'.$doc_id.'"';
	 	$command_multy_procedure = $connection->createCommand($sql_multy_procedure);
	 	$user_multy_procedure = $command_multy_procedure->queryAll();
	 	$user_selected_procedure = array();
	 	foreach ($user_multy_procedure as $key=>$val) {
	 		$user_selected_procedure[] = $val['procedure_id'];
	 	}
	 	 
	 	 
	 	$session = Yii::app()->session;
	 	$session['searchresult']=serialize($doctor_list);
	 	$session['user_speciality_res']=serialize($user_speciality_res);
	 	$session['user_selected_speciality']=serialize($user_selected_speciality);
	 	$session['user_selected_procedure']=serialize($user_selected_procedure);
	 	$session['usercriteria']=serialize($usercriteria);
	 	$arrlatlon=array();
	 	if($location_id){
	 		$zipcode =$location_id;
	 		$url="http://maps.googleapis.com/maps/api/geocode/json?address=". $zipcode;
	 		$curl = curl_init();
	 		curl_setopt_array($curl,
	 		array( CURLOPT_RETURNTRANSFER => 1,
	 		CURLOPT_URL => $url)
	 		);
	 		$result = curl_exec($curl);
	 		$decoded_result = json_decode($result, true);
	 		if($decoded_result['status']=="OK"){
	 			$arrlatlon[]=array('tolat'=>$decoded_result['results'][0]['geometry']['location']['lat'],'tolng'=>$decoded_result['results'][0]['geometry']['location']['lng']);
	 			 
	 		}
	 		else {$arrlatlon[]=array('tolat'=>0,'tolng'=>0);}
	 	}
	 	 
	 	$this->render('doctor_search',array(
	 			'dataProvider'=>$usercriteria,'doctor_list'=>$doctor_list,'user_speciality' => $user_speciality_res,'pages'=>$pages,'user_insurance' => $user_insurance_res,'user_language' => $user_language_res,'user_procedure' => $user_procedure_res,'user_reason_for_visit' => $user_reason_for_visit_res,'user_selected_speciality' => $user_selected_speciality,'user_selected_procedure' => $user_selected_procedure, 'radius'=>$radius,'docid'=>$doc_id,'latlon'=>$arrlatlon
	 	));
	 }
	  
	 /*--------------------- End Of Doctor Search -------------------------------------*/
	 
	 /* ------------------- Doctor Search  For Mobile Start-------------------------------------------- */
	 
	 public function actionDoctorSearchMob() {
	 
	 	$name= Yii::app()->request->getParam('name_autocomp');
	 	$speciality_id = Yii::app()->request->getParam('speciality_id');
	 	$office_name = Yii::app()->request->getParam('office_name');
	 	$location_id = Yii::app()->request->getParam('geo_location_id');
	 
	 	$radius_array = array();
	 	$radius = array();
	 
	 	if(is_numeric($location_id)){
	 		if(!empty($location_id)){
	 			$distancemile = 10;
	 			$zipcode = trim(Yii::app()->request->getParam('geo_location_id'));
	 			$radius = $this->radiusSearch($zipcode,$distancemile);
	 			foreach($radius as $key=>$val) {
	 				$radius_array[] = TRIM($key);
	 			}
	 		}
	 	}
	 
	 	$usercriteria = array();
	 	$usercriteria=$_REQUEST;
	 
	 	$input_name = preg_replace("/[^a-zA-Z]+/", "", $name);
	 
	 	$searchCriteria = new CDbCriteria();
	 	$searchCriteria->condition = 't.status = :status AND t.email_verified = :email_verified ';
	 	$searchCriteria->params = array( ':status' => 1,':email_verified' => 1);
	 	$searchCriteria->join = ' INNER JOIN `da_doctor_address` `da` ON ( t.id = da.doctor_id AND da.status = 1 )';
	 
	 	if (isset($name) && !empty($name)) {
	 		//$searchCriteria->addSearchCondition('concat_ws( t.first_name, " ", t.last_name )', $input_name, true, 'AND');
	 		$searchCriteria->addSearchCondition('t.full_name',$input_name, true, 'AND');
	 	}
	 	if($speciality_id != ""){
	 		$searchCriteria->join .= ' LEFT JOIN `da_doctor_speciality` `ds` ON ( t.id = ds.doctor_id AND ds.status = 1 )';
	 		$searchCriteria->condition .= ' AND ds.speciality_id = :speciality';
	 		$searchCriteria->params = array_merge(array(':speciality' => $speciality_id ), $searchCriteria->params);
	 	}
	 	if($location_id != ""){
	 		if(count($radius_array)>0) {
	 			$radius_str = implode(', ', $radius_array);
	 			$searchCriteria->condition .= ' AND da.zip IN ('.$radius_str.')';
	 		}else{
	 			$location_id_arr = explode(',',$location_id);
	 			$location_id_arr_imp = array();
	 			if(!empty($location_id_arr)){
	 				if( count ( $location_id_arr ) == 1 ) {
	 					foreach($location_id_arr as $location_id_arr_val){
	 						$searchCriteria->condition .= ' AND ( da.city LIKE :city OR da.state LIKE :state OR da.zip = :zip )';
	 						$searchCriteria->params = array_merge(array( ':city' => trim($location_id_arr_val),':state' => trim($location_id_arr_val),':zip' => trim($location_id_arr_val)), $searchCriteria->params);
	 					}
	 				}else if(  count ( $location_id_arr ) == 2 ) {
	 					$searchCriteria->condition .= ' AND ( da.city LIKE :city AND da.state LIKE :state OR da.zip = :zip )';
	 					$searchCriteria->params = array_merge(array( ':city' => '%'.trim($location_id_arr[0]).'%',':state' => '%'.trim($location_id_arr[1]).'%',':zip' => trim($location_id_arr[1])), $searchCriteria->params);
	 				}else if(  count ( $location_id_arr ) == 3 ) {
	 					$searchCriteria->condition .= ' AND ( da.city LIKE :city AND da.state LIKE :state AND da.zip = :zip )';
	 					$searchCriteria->params = array_merge(array( ':city' => '%'.trim($location_id_arr[0]).'%',':state' => '%'.trim($location_id_arr[1]).'%',':zip' => trim($location_id_arr[2])), $searchCriteria->params);
	 
	 				} else {
	 					foreach($location_id_arr as $location_id_arr_val){
	 						$searchCriteria->condition .= ' AND ( da.address LIKE :address AND da.city LIKE :city OR da.state LIKE :state OR da.zip = :zip )';
	 						$searchCriteria->params = array_merge(array( ':address' => '%'.trim($location_id_arr_val).'%',':city' => '%'.trim($location_id_arr_val).'%',':state' => '%'.trim($location_id_arr_val).'%',':zip' => trim($location_id_arr_val)), $searchCriteria->params);
	 					}
	 				}
	 			}
	 		}
	 	}
	 
	 	$limit = 20;
	 	$count = Doctor::model()->count($searchCriteria);
	 	$pages = new CPagination($count);
	 	$pages->setPageSize(20);
	 	$pages->applyLimit($searchCriteria);
	 	$searchCriteria->group = 't.id';
	 
	 	//$searchCriteria->order = 'ABS(da.zip) DESC';
	 
	 	$doctor_list = Doctor::model()->findAll($searchCriteria);
	 
	 	$doc_id = 0;
	 	if( isset($doctor_list[0]['id'])){
	 		$doc_id = $doctor_list[0]['id'];
	 	}
	 
	 	$arrlatlon=array();
	 	if($location_id){
	 		$zipcode =$location_id;
	 		$url="http://maps.googleapis.com/maps/api/geocode/json?address=". $zipcode;
	 		$curl = curl_init();
	 		curl_setopt_array($curl,
	 		array( CURLOPT_RETURNTRANSFER => 1,
	 		CURLOPT_URL => $url)
	 		);
	 		$result = curl_exec($curl);
	 		$decoded_result = json_decode($result, true);
	 		if($decoded_result['status']=="OK"){
	 			$arrlatlon[]=array('tolat'=>$decoded_result['results'][0]['geometry']['location']['lat'],'tolng'=>$decoded_result['results'][0]['geometry']['location']['lng']);
	 
	 		}
	 		else {$arrlatlon[]=array('tolat'=>0,'tolng'=>0);}
	 	}
	 	$this->render('doctor_search_mob',array(
	 			'dataProvider'=>$usercriteria,'doctor_list'=>$doctor_list,'pages'=>$pages,'radius'=>$radius,'docid'=>$doc_id,'latlon'=>$arrlatlon
	 	));
	 }
	  
	 /*--------------------- End Of Doctor Search For Mobile-------------------------------------*/
	 
	 
	 public function actionDoctorMapView(){
	 	$session = Yii::app()->session;
	 
	 	$searchresult = unserialize($session['searchresult']);
	 	$user_speciality_res = unserialize($session['user_speciality_res']);
	 	$user_selected_speciality = unserialize($session['user_selected_speciality']);
	 	$user_selected_procedure = unserialize($session['user_selected_procedure']);
	 	$usercriteria=unserialize($session['usercriteria']);
	 
	 	$doc_id = 0;
	 	if( isset($searchresult[0]['id'])){
	 		$doc_id = $searchresult[0]['id'];
	 	}
	 	$this->render('doctor_search_map',array(
	 			'doctor_list'=>$searchresult,'usercriteria'=>$usercriteria,'user_speciality' => $user_speciality_res,'user_selected_speciality' => $user_selected_speciality,'user_selected_procedure' => $user_selected_procedure
	 	));
	 }

	 /* --------------------------- Doctor Schedual Popup --------------------------- */
	  
	 public function actionScheduleTable(){
	 	$indexCount= $_REQUEST['index'];
	 	$docId = $_REQUEST['docId'];
	 	$specialId = $_REQUEST['specialId'];
	 	$docAddId = $_REQUEST['docAddId'];
	 	$procedureId = $_REQUEST['procedureId'];
	 	$arr[]='<input type="hidden" id="speciality_id_" value="'.$specialId.'" />
				<input type="hidden" id="procedure_id" value="'.$procedureId.'" />
				<input type="hidden" id="doctor_id" value="'.$docId.'" />
				<input type="hidden" id="today_date" value="'.date('Y-m-d', strtotime("+ 1 day")).'" />
				<input type="hidden" id="view_date" value="'.date('Y-m-d', strtotime("+ 1 day")).'" />
				<input type="hidden" id="index_id_pop" value="'.$indexCount.'" />
				<input type="hidden" id="address_id" value="'.$docAddId.'" />';
	 
	 	if (!empty($docAddId) && isset($docId)) {
	 		$arr[]=' <div class="calander_menu">
			         	<ul>
			            	<li>'.date('D', (time() + 86400)).'<font style="font-size:12px; display:block; font-weight:normal;">'.date('m-d-Y', time() + 86400).'</font></li>
			                <li>'. date('D', (time() + 86400)+3600*24).'<font style="font-size:12px; display:block; font-weight:normal;">'. date('m-d-Y', (time() + 86400)+3600*24).'</font></li>
			                <li>'.date('D', (time() + 86400)+3600*24*2).'<font style="font-size:12px; display:block; font-weight:normal;">'. date('m-d-Y', (time() + 86400)+3600*24*2).'</font></li>
							<li>'.date('D', (time() + 86400)+3600*24*3).'<font style="font-size:12px; display:block; font-weight:normal;">'. date('m-d-Y', (time() + 86400)+3600*24*3).'</font></li>
							<li>'.date('D', (time() + 86400)+3600*24*4).'<font style="font-size:12px; display:block; font-weight:normal;">'.  date('m-d-Y', (time() + 86400)+3600*24*4).'</font></li>
			            </ul>
			         </div>';
	 		$arr[]= '<div class="nxt_pre_arrow_area">
							<div class="pre_arrow_area" onclick="appointmentSchedule(\'prev\','.$docAddId.');">
								<img src="'.Yii::app()->request->baseUrl.'/assets/images/pre_arrow_gry.png" style="cursor:pointer;"/>
							</div>
							 <div id="schedule_loader_prev_'.$docAddId.'" class="schedule_loader" style="position:absolute; top:11px; z-index:999; display:none;">
								<img src="'.Yii::app()->request->baseUrl.'/assets/images/loader.gif" />
							</div>
							<div class="nxt_arrow_area" onclick="appointmentSchedule(\'next\','.$docAddId.');">
								<img src="'.Yii::app()->request->baseUrl.'/assets/images/nxt_arrow_green.png" style="cursor:pointer;"/>
							</div>
							<div id="schedule_loader_next_'.$docAddId.'" class="schedule_loader" style="position:absolute; top:11px; z-index:999; display:none;">
								<img src="'.Yii::app()->request->baseUrl.'/assets/images/loader.gif" />
							</div>
			         </div>';
	 		$arr[]='<div class="calander_box">';
	 		$view_ary_sch = $this->doctorSchTime($docId, date('Y-m-d', strtotime("+ 1 day")), date('Y-m-d', strtotime(' +5 day')), 'Y-m-d', $docAddId);
	 		$doctorAppTime = $this->doctorAppTime($docId, date('Y-m-d', strtotime("+ 1 day")), date('Y-m-d', strtotime(' +5 day')), 'Y-m-d', $docAddId);
	 		$doctorTimeOff = $this->doctorTimeOff($docId, date('Y-m-d', strtotime("+ 1 day")), date('Y-m-d', strtotime(' +5 day')), 'Y-m-d');
	 		if (!empty($view_ary_sch[$docId])) {
	 			foreach ($view_ary_sch[$docId] as $view_ary_sch_key => $view_ary_sch_val) {
	 				if (!empty($view_ary_sch_val)) {
	 					$view_ary_sch_time_cnt = false;
	 					foreach ($view_ary_sch_val as $view_ary_sch_time) {
	 						if (isset($view_ary_sch_time) && !empty($view_ary_sch_time))
	 							$view_ary_sch_time_cnt = true;
	 					}
	 					if ($view_ary_sch_time_cnt) {
	 						$arr[]=' <div class="sch_time">';
	 						$tot_cnt = 0;
	 						$inner_five = 0;
	 						foreach ($view_ary_sch_val as $view_ary_sch_time_val) {
	 							if ($tot_cnt < 5) {
	 								foreach ($view_ary_sch_time_val as $view_ary_sch_time_val_arr) {
	 									if ($view_ary_sch_time_val_arr['day'] == date('l', strtotime($view_ary_sch_key))) {
	 										if ($view_ary_sch_time_val_arr['on_off'] == 1) {
	 											$view_ary_to_strtotime = $view_ary_sch_time_val_arr['to_strtotime'];
	 											$view_ary_from_strtotime = $view_ary_sch_time_val_arr['from_strtotime'];
	 											$time_diff = $view_ary_to_strtotime - $view_ary_from_strtotime;
	 											$view_ary_time_slot = $view_ary_sch_time_val_arr['time_slot'] * 60;
	 											$view_ary_laser_slot = $view_ary_sch_time_val_arr['laser_slot'] * 60;
	 
	 											for ($iCnt = 0; $iCnt < 5; $iCnt++) {
	 												$added_time = $view_ary_time_slot + $view_ary_laser_slot;
	 												$show_time = $view_ary_from_strtotime + ($added_time * $iCnt);
	 												if ($show_time < $view_ary_to_strtotime) {
	 													$tot_cnt++;
	 													$inner_five++;
	 													if ($tot_cnt <= 5) {
	 														if ($iCnt == 0 && $inner_five < 5) {
	 															if (isset($doctorAppTime[$docId][$view_ary_sch_key]) && in_array($view_ary_sch_time_val_arr['from_strtotime'], $doctorAppTime[$docId][$view_ary_sch_key])) {
	 																$arr[]=  '<span class="innertable disable" ><a class="blank" href="javascript:void(0);">' . date('h:i  a', $view_ary_sch_time_val_arr['from_strtotime']) . '</a></span>';
	 															} else {
	 																if (isset($doctorTimeOff[$docId][$view_ary_sch_key]) && $view_ary_sch_time_val_arr['from_strtotime'] >= $doctorTimeOff[$docId][$view_ary_sch_key][0]['strt_time_off'] && $view_ary_sch_time_val_arr['from_strtotime'] <= $doctorTimeOff[$docId][$view_ary_sch_key][0]['to_time_off']) {
	 																	$arr[]=  '<span class="innertable disable" ><a class="blank" href="javascript:void(0);">' . date('h:i  a', $view_ary_sch_time_val_arr['from_strtotime']) . '</a></span>';
	 																} else {
	 																	$arr[]=  '<span class="innertable"><a href="' . $this->createAbsoluteUrl('doctor/DoctorBookStep1/?start_time=' . $view_ary_sch_time_val_arr['from_strtotime'] . '&time_slot=' . $view_ary_time_slot . '&doctor_id=' . $docId . '&address_id=' . $view_ary_sch_time_val_arr['address_id'] . '&speciality_id=' . $specialId) . '&procedure_id=' . $procedureId . '">' . date('h:i  a', $view_ary_sch_time_val_arr['from_strtotime']) . '</a></span>';
	 																}
	 															}
	 														} elseif ($iCnt > 0 && $iCnt < 4 && $inner_five < 5) {
	 															if ($show_time > $view_ary_to_strtotime) {
	 																$arr[]=  '<span class="innertable disable"><a class="blank"  href="javascript:void(0);">' . date('h:i a', $show_time) . '</a></span>';
	 															} else {
	 																if (isset($doctorAppTime[$docId][$view_ary_sch_key]) && in_array($show_time, $doctorAppTime[$docId][$view_ary_sch_key])) {
	 																	$arr[]=  '<span class="innertable disable"><a class="blank" href="javascript:void(0);">' . date('h:i  a', $show_time) . '</a></span>';
	 																} else {
	 																	if (isset($doctorTimeOff[$docId][$view_ary_sch_key]) && $show_time >= $doctorTimeOff[$docId][$view_ary_sch_key][0]['strt_time_off'] && $show_time <= $doctorTimeOff[$docId][$view_ary_sch_key][0]['to_time_off']) {
	 																		$arr[]=  '<span class="innertable disable"><a class="blank" href="javascript:void(0);">' . date('h:i  a', $show_time) . '</a></span>';
	 																	} else {
	 																		$arr[]=  '<span class="innertable"><a href="' . $this->createAbsoluteUrl('doctor/DoctorBookStep1/?start_time=' . $show_time . '&time_slot=' . $view_ary_time_slot . '&doctor_id=' . $docId . '&address_id=' . $view_ary_sch_time_val_arr['address_id'] . '&speciality_id=' . $specialId) . '&procedure_id=' . $procedureId . '">' . date('h:i a', $show_time) . '</a></span>';
	 																	}
	 																}
	 															}
	 														} elseif ($iCnt == 4) {
	 															//$arr[]=  "<span class=\"innertable\"><a href=\"javascript:void(0);\" class=\"popupApp\" data-id=\"" . $view_ary_sch_time_val_arr['address_id'] . "\" accesskey=\"" . strtotime($view_ary_sch_key) . "\" >more...</a></span>";
	 															$arr[]=  "<span class=\"innertable\"><a href=\"javascript:void(0);\" class=\"popupApp\" data-id=\"".$view_ary_sch_time_val_arr['address_id']."|".$specialId."|".$procedureId."|".$docId."|".$indexCount."\" accesskey=\"".strtotime($view_ary_sch_key)."\">more...</a></span>";
	 														}
	 
	 														if ($inner_five == 5 && $iCnt < 4) {
	 															//$arr[]=  "<span class=\"innertable\"><a href=\"javascript:void(0);\"  class=\"popupApp\" data-id=\"" . $view_ary_sch_time_val_arr['address_id'] . "\" accesskey=\"" . strtotime($view_ary_sch_key) . "\">more...</a></span>";
	 															$arr[]=  "<span class=\"innertable\"><a href=\"javascript:void(0);\" class=\"popupApp\" data-id=\"".$view_ary_sch_time_val_arr['address_id']."|".$specialId."|".$procedureId."|".$docId."|".$indexCount."\" accesskey=\"".strtotime($view_ary_sch_key)."\">more...</a></span>";
	 														}
	 													}
	 												} else {
	 
	 												}
	 											}
	 
	 										}else {
	 											$tot_cnt = 5;
	 											$arr[]=  '<span class="innertable disable"><a class="blank" href="javascript:void(0);">08:00 am</a></span>
															<span class="innertable disable"><a class="blank" href="javascript:void(0);">08:30 am</a></span>
															<span class="innertable disable"><a class="blank" href="javascript:void(0);">09:00 am</a></span>
															<span class="innertable disable"><a class="blank" href="javascript:void(0);">09:30 am</a></span>
															<span class="innertable disable"><a class="blank" href="javascript:void(0);">10:00 am</a></span>';
	 										}
	 									}
	 								}
	 							}else {
	 								break;
	 							}
	 						}
	 						if($inner_five==1){
	 							$arr[]=  '<span class="innertable disable"><a class="blank" href="javascript:void(0);">08:00 am</a></span>
												<span class="innertable disable"><a class="blank" href="javascript:void(0);">08:30 am</a></span>
												<span class="innertable disable"><a class="blank" href="javascript:void(0);">09:00 am</a></span>
									<span class="innertable disable"><a class="blank" href="javascript:void(0);">09:30 am</a></span>';
	 						}else if($inner_five==2){
	 							$arr[]=  '<span class="innertable disable"><a class="blank" href="javascript:void(0);">08:00 am</a></span>
												<span class="innertable disable"><a class="blank" href="javascript:void(0);">08:30 am</a></span>
									<span class="innertable disable"><a class="blank" href="javascript:void(0);">09:00 am</a></span>';
	 						}else if($inner_five==3){
	 							$arr[]=  '<span class="innertable disable"><a class="blank" href="javascript:void(0);">08:00 am</a></span>
									<span class="innertable disable"><a class="blank" href="javascript:void(0);">08:30 am</a></span>';
	 						}else if($inner_five==4){
	 							$arr[]=  '<span class="innertable disable"><a class="blank" href="javascript:void(0);">08:30 am</a></span>';
	 						}
	 						$arr[]='</div>';
	 					}else{
	 						$arr[]= '<div class="sch_time">
			                            <span class="innertable disable"><a class="blank" href="javascript:void(0);">08:00 am</a></span>
			                            <span class="innertable disable"><a class="blank" href="javascript:void(0);">08:30 am</a></span>
			                            <span class="innertable disable"><a class="blank" href="javascript:void(0);">09:00 am</a></span>
			                            <span class="innertable disable"><a class="blank" href="javascript:void(0);">09:30 am</a></span>
			                            <span class="innertable disable"><a class="blank" href="javascript:void(0);">more...</a></span>
			                        </div>';
	 					}
	 				}else{
	 					$arr[]= '<div class="sch_time">
		                            <span class="innertable disable"><a class="blank" href="javascript:void(0);">08:00 am</a></span>
		                            <span class="innertable disable"><a class="blank" href="javascript:void(0);">08:30 am</a></span>
		                            <span class="innertable disable"><a class="blank" href="javascript:void(0);">09:00 am</a></span>
		                            <span class="innertable disable"><a class="blank" href="javascript:void(0);">09:30 am</a></span>
		                            <span class="innertable disable"><a class="blank" href="javascript:void(0);">more...</a></span>
		                        </div>';
	 				}
	 			}
	 			$arr[]= '</div>';
	 		}else{
	 			$arr[]= '<div class="address_list">
			                <div class="calander_box1" style="width:99.9%; font-weight: bold;" >
			                	No availability these days.
			                </div>
						</div>';
	 		}
	 	} else{
	 		$arr[]= '<div class="address_list">
		                <div class="calander_box1" style="width:99.9%; font-weight: bold;" >
		                	No availability these days.
		                </div>
					</div>';
	 	}
	 	echo implode("",$arr);
	 }
	  
	 /* --------------------------- End Doctor Schedual Popup --------------------------- */
	 
	function doctorSchedual($doctorId,$addressId){

		$connection = Yii::app()->db;

		$doctor_sche = array();

		if($addressId != ''){

			$sql_sch='select * FROM da_doctor_schedule WHERE status="1" and doctor_id='.$doctorId.' and address_id='.$addressId;

			$command_sch = $connection->createCommand($sql_sch);

			$doctor_sche = $command_sch->queryAll();

		}

		return $doctor_sche;

	} 

	

	function doctorSchedualWithDateCondition($doctorId,$addressId){

		$connection = Yii::app()->db;

		$doctor_sche = array();

		if($addressId != ''){

			$sql_sch="select * FROM da_doctor_schedule WHERE status='1' and doctor_id='".$doctorId."' and to_date >= '".date('Y-m-d')."' and address_id='".$addressId."'";

			$command_sch = $connection->createCommand($sql_sch);

			$doctor_sche = $command_sch->queryAll();

		}

		return $doctor_sche;

	} 

	

	function checkFav($patientId,$doctorId,$specialityID){

		$connection = Yii::app()->db;

		$doctor_fav = array();		

		$sql_fav="select * FROM da_favourite_doctor WHERE status='1' and doctor_id='".$doctorId."' and patient_id = '".$patientId."' and doctor_speciality = '".$specialityID."'";

		$command_fav = $connection->createCommand($sql_fav);

		$doctor_fav = $command_fav->queryAll();	

		return count($doctor_fav);

	} 

	

	function FavCount($doctorId,$specialityID){

		$connection = Yii::app()->db;

		$doctor_fav = array();		

		$sql_fav="select * FROM da_favourite_doctor WHERE status='1' and doctor_id = '".$doctorId."' and doctor_speciality = '".$specialityID."'";

		$command_fav = $connection->createCommand($sql_fav);

		$doctor_fav = $command_fav->queryAll();	

		return count($doctor_fav);

	} 



	function actionDoctorFavourite()

	{

		if( !isset($_REQUEST['pid']) ) {

			$this->redirect(array('404error'));

		}

		$pid = $_REQUEST['pid'];

		$did = $_REQUEST['did'];

		$spid = $_REQUEST['spid'];

		

		$sql='INSERT INTO da_favourite_doctor (patient_id, doctor_speciality, doctor_id, date) VALUES(:patient_id,:doctor_speciality,:doctor_id, :date)';

		$params = array(

			"patient_id" => $pid,

			"doctor_speciality" => $spid,

			"doctor_id" => $did,

			"date" => date('Y-m-d')

		);

		$connection = Yii::app()->db;

		$command=$connection->createCommand($sql);

		$command->execute($params);





		/*#######  after login my favourite part #######*/

				

			if($this->checkFav($pid,$did,$spid) > 0){ 

			/*####### My favourite part #######*/

				echo "<div class='already_favorite'><a href='javascript:void(0);' title='My Favourite' onclick='delFav(".$pid.",".$did.",".$spid.");'><img src='".Yii::app()->getBaseUrl(true)."/assets/images/active-heart.png' width='16' style='float: left; margin-top: 1px; margin-right: 6px;' /><span style='margin:0'>Favorite</span></a></div>";

			/*####### End My favourite part #######*/

			 } else { 

			/*####### Add to favourite part #######*/

				echo "<div class='add_to_favorite'><a href='javascript:void(0);' title='Add to Favourite' onclick='addFav(".$pid.",".$did.",".$spid.");'><img src='".Yii::app()->getBaseUrl(true)."/assets/images/inactive-heart.png' width='16' style='float: left; margin-top: 1px; margin-right: 6px;' /><span style='margin:0'>Favorite</span></a></div>";

				/*####### End Add to favourite part #######*/

			 }

				/*####### Favourite count part #######*/

				echo "<div class='box'><span style='display:block'>".$this->FavCount($did,$spid)."</span></div>";

				/*####### End Favourite count part #######*/

		/*####### end after login my favourite part #######*/





    }



	function actionDeleteDoctorFavourite(){

		if( !isset($_REQUEST['pid']) ) {

			$this->redirect(array('404error'));

		}

		$pid = $_REQUEST['pid'];

		$did = $_REQUEST['did'];

		$spid = $_REQUEST['spid'];



		$connection = Yii::app()->db;

		$query = "DELETE FROM `da_favourite_doctor` WHERE `patient_id` = :pid AND `status` = :status AND `doctor_speciality` = :spid AND `doctor_id` = :did";

		$command = Yii::app()->db->createCommand($query);

		$command->execute(array('pid' => $pid, 'status' => '1', 'spid' => $spid, 'did' => $did));

		/*#######  after login my favourite part #######*/

				

			if($this->checkFav($pid,$did,$spid) > 0){ 

			/*####### My favourite part #######*/

				echo "<div class='already_favorite'><a href='javascript:void(0);' title='My Favourite' onclick='delFav(".$pid.",".$did.",".$spid.");'><img src='".Yii::app()->getBaseUrl(true)."/assets/images/active-heart.png' width='16' style='float: left; margin-top: 1px; margin-right: 6px;' /><span style='margin:0'>Favorite</span></a></div>";

			/*####### End My favourite part #######*/

			 } else { 

			/*####### Add to favourite part #######*/

				echo "<div class='add_to_favorite'><a href='javascript:void(0);' title='Add to Favourite' onclick='addFav(".$pid.",".$did.",".$spid.");'><img src='".Yii::app()->getBaseUrl(true)."/assets/images/inactive-heart.png' width='16' style='float: left; margin-top: 1px; margin-right: 6px;' /><span style='margin:0'>Favorite</span></a></div>";

				/*####### End Add to favourite part #######*/

			 }

				/*####### Favourite count part #######*/

				echo "<div class='box'><span style='display:block'>".$this->FavCount($did,$spid)."</span></div>";

				/*####### End Favourite count part #######*/

		/*####### end after login my favourite part #######*/





	} 



	



	function doctorAvailableOffer($doc_id,$strDateFrom,$strDateTo,$format,$da_address_id) {

		$connection = Yii::app()->db;

		$aryDates=$this->createRange($strDateFrom,$strDateTo,$format);

		$doctor_sch = array();

		//$doctor_sch_time_arr = array();

		if($da_address_id != ''){

		///foreach($aryDates as $aryDates_val){

			 //$sql_sch='select * FROM da_doctor_offers WHERE status="1" and doctor_id='.$doc_id.' and address_id='.$da_address_id.' and (from_date<=\''.$strDateFrom.'\' and to_date>=\''.$strDateFrom.'\')';

			//$sql_sch='select * FROM da_doctor_offers WHERE status="1" and doctor_id='.$doc_id.' and address_id='.$da_address_id.' and to_date>=\''.$strDateFrom.'\'';

			$sql_sch='select * FROM da_doctor_offers WHERE status="1" and doctor_id=\''.$doc_id.'\' and address_id=\''.$da_address_id.'\' and to_date>=\''.$strDateFrom.'\'';

			$command_sch = $connection->createCommand($sql_sch);

			$doctor_sch = $command_sch->queryAll();

			/*if(!empty($doctor_sch_arr)){

				$doctor_sch = $doctor_sch_arr;

				break;

			}

		}*/

		}

		return $doctor_sch;

	}

	

	function doctorAppTimeMore($doc_id,$strDateFrom,$strDateTo,$format,$address_id = "") {

		$connection = Yii::app()->db;

		$aryDates=$this->createRange($strDateFrom,$strDateTo,$format);

		$doctor_id_sch_time_arr_all = array();

		$doctor_sch_arr = array();

		foreach($aryDates as $aryDates_val){

			 $timestamp_start = strtotime($aryDates_val);

			 $mk_to_time = mktime(0,0,0,date('m',$timestamp_start),date('d',$timestamp_start)+1,date('Y',$timestamp_start));

			 $timestamp_end = $mk_to_time;

			 if($address_id!=''){

				 $sql_sch='select book_time FROM da_doctor_book WHERE status="1" and doctor_id='.$doc_id.' and address_id='.$address_id.' and (book_time<=\''.$timestamp_end.'\' and book_time>=\''.$timestamp_start.'\')';

				 $command_sch = $connection->createCommand($sql_sch);

				 $doctor_sch = $command_sch->queryAll();

			 }else	$doctor_sch = array();

			foreach ($doctor_sch as $doctor_sch_key=>$doctor_sch_val){

				

				$doctor_sch_arr[$aryDates_val][] = $doctor_sch_val['book_time'];

			}

			$doctor_id_sch_time_arr_all[$doc_id] = $doctor_sch_arr;

		}

		return $doctor_id_sch_time_arr_all;

	}

	

	function doctorTimeOffDay($doc_id,$strDateFrom,$strDateTo,$format,$address_id = "") {

		$connection = Yii::app()->db;

		$aryDates=$this->createRange($strDateFrom,$strDateTo,$format);

		$doctor_id_sch_time_arr_all = array();

		$doctor_sch_arr = array();

		foreach($aryDates as $aryDates_val){

			 $timestamp_start = strtotime($aryDates_val);

			 $mk_to_time = mktime(0,0,0,date('m',$timestamp_start),date('d',$timestamp_start)+1,date('Y',$timestamp_start));

			 $timestamp_end = $mk_to_time;

			 //$sql_sch='select * FROM da_doctor_timeoff WHERE status="1" and doctor_id='.$doc_id.' and (\''.$strDateFrom.'\' between from_date and to_date)';

			 $sql_sch='select * FROM da_doctor_timeoff WHERE status="1" and doctor_id='.$doc_id.' and (\''.$aryDates_val.'\' between from_date and to_date)';

			 $command_sch = $connection->createCommand($sql_sch);

			 $doctor_sch = $command_sch->queryAll();

			

			foreach ($doctor_sch as $doctor_sch_key=>$doctor_sch_val){

				$strt_time_off = strtotime($doctor_sch_val['from_date'].' '.$doctor_sch_val['from_time'].' '.$doctor_sch_val['from_time_format']);

				$to_time_off = strtotime($doctor_sch_val['to_date'].' '.$doctor_sch_val['to_time'].' '.$doctor_sch_val['to_time_format']);

				$doctor_sch_arr[$aryDates_val][] = array('strt_time_off'=>$strt_time_off,'to_time_off'=>$to_time_off);

			}

			$doctor_id_sch_time_arr_all[$doc_id] = $doctor_sch_arr;

		}

		return $doctor_id_sch_time_arr_all;

	}

	

	

	function doctorAppTime($doc_id,$strDateFrom,$strDateTo,$format,$address_id = "") {

		$connection = Yii::app()->db;

		$aryDates=$this->createRange($strDateFrom,$strDateTo,$format);

		$doctor_id_sch_time_arr_all = array();

		$doctor_sch_arr = array();

		foreach($aryDates as $aryDates_val){

			 $timestamp_start = strtotime($aryDates_val);

			 $mk_to_time = mktime(0,0,0,date('m',$timestamp_start),date('d',$timestamp_start)+1,date('Y',$timestamp_start));

			 $timestamp_end = $mk_to_time;

			 if($address_id!=''){

				 $sql_sch='select book_time FROM da_doctor_book WHERE status="1" and doctor_id='.$doc_id.' and address_id='.$address_id.' and (book_time<=\''.$timestamp_end.'\' and book_time>=\''.$timestamp_start.'\')';

				 $command_sch = $connection->createCommand($sql_sch);

				 $doctor_sch = $command_sch->queryAll();

			 }else	$doctor_sch = array();

			foreach ($doctor_sch as $doctor_sch_key=>$doctor_sch_val){

				

				$doctor_sch_arr[$aryDates_val][] = $doctor_sch_val['book_time'];

			}

			$doctor_id_sch_time_arr_all[$doc_id] = $doctor_sch_arr;

		}

		return $doctor_id_sch_time_arr_all;

	}

	

	function doctorTimeOffSearch($doc_id,$strDateFrom,$strDateTo,$format,$address_id = "") {

		$connection = Yii::app()->db;

		$aryDates=$this->createRange($strDateFrom,$strDateTo,$format);

		$doctor_id_sch_time_arr_all = array();

		$doctor_sch_arr = array();

		foreach($aryDates as $aryDates_val){

			 $timestamp_start = strtotime($aryDates_val);

			 $mk_to_time = mktime(0,0,0,date('m',$timestamp_start),date('d',$timestamp_start)+1,date('Y',$timestamp_start));

			 $timestamp_end = $mk_to_time;

			 $sql_sch='select * FROM da_doctor_timeoff WHERE status="1" and doctor_id='.$doc_id.' and (\''.$aryDates_val.'\' between from_date and to_date)';

			 $command_sch = $connection->createCommand($sql_sch);

			 $doctor_sch = $command_sch->queryAll();

			

			foreach ($doctor_sch as $doctor_sch_key=>$doctor_sch_val){

				$strt_time_off = strtotime($doctor_sch_val['from_date'].' '.$doctor_sch_val['from_time'].' '.$doctor_sch_val['from_time_format']);

				$to_time_off = strtotime($doctor_sch_val['to_date'].' '.$doctor_sch_val['to_time'].' '.$doctor_sch_val['to_time_format']);

				$doctor_sch_arr[$aryDates_val][] = array('strt_time_off'=>$strt_time_off,'to_time_off'=>$to_time_off);

			}

			$doctor_id_sch_time_arr_all[$doc_id] = $doctor_sch_arr;

		}

		return $doctor_id_sch_time_arr_all;

	}

	

	function doctorTimeOff($doc_id,$strDateFrom,$strDateTo,$format,$address_id = "") {

		$connection = Yii::app()->db;

		$aryDates=$this->createRange($strDateFrom,$strDateTo,$format);

		$doctor_id_sch_time_arr_all = array();

		$doctor_sch_arr = array();

		foreach($aryDates as $aryDates_val){

			 $timestamp_start = strtotime($aryDates_val);

			 $mk_to_time = mktime(0,0,0,date('m',$timestamp_start),date('d',$timestamp_start)+1,date('Y',$timestamp_start));

			 $timestamp_end = $mk_to_time;

			 //$sql_sch='select * FROM da_doctor_timeoff WHERE status="1" and doctor_id='.$doc_id.' and (to_date<=\''.$strDateTo.'\' and from_date>=\''.$strDateFrom.'\')';

			 $sql_sch='select * FROM da_doctor_timeoff WHERE status="1" and doctor_id='.$doc_id.' and (\''.$aryDates_val.'\' between from_date and to_date)';

			 $command_sch = $connection->createCommand($sql_sch);

			 $doctor_sch = $command_sch->queryAll();

			

			foreach ($doctor_sch as $doctor_sch_key=>$doctor_sch_val){

				$strt_time_off = strtotime($doctor_sch_val['from_date'].' '.$doctor_sch_val['from_time'].' '.$doctor_sch_val['from_time_format']);

				$to_time_off = strtotime($doctor_sch_val['to_date'].' '.$doctor_sch_val['to_time'].' '.$doctor_sch_val['to_time_format']);

				$doctor_sch_arr[$aryDates_val][] = array('strt_time_off'=>$strt_time_off,'to_time_off'=>$to_time_off);

			}

			$doctor_id_sch_time_arr_all[$doc_id] = $doctor_sch_arr;

		}

		return $doctor_id_sch_time_arr_all;

	}

	

	function doctorSchTime($doc_id,$strDateFrom,$strDateTo,$format,$address_id = "") {

		$connection = Yii::app()->db;

		$aryDates=$this->createRange($strDateFrom,$strDateTo,$format);

		//print_r($aryDates);

		$doctor_id_sch_time_arr_all = array();

		$doctor_sch_arr = array();

		//$doctor_sch_time_arr = array();

		foreach($aryDates as $aryDates_val){

			$date_arr[$aryDates_val] = array();

			 if($address_id !=''){

			 	$sql_sch='select * FROM da_doctor_schedule WHERE status="1" and doctor_id='.$doc_id.' and (from_date<=\''.$aryDates_val.'\' and to_date>=\''.$aryDates_val.'\')'.' and address_id='.$address_id;

				//$sql_sch='select * FROM da_doctor_schedule WHERE status="1" and doctor_id='.$doc_id.' and (from_date<=\''.$aryDates_val.'\' and to_date>=\''.$aryDates_val.'\')';

			 }else{

				 $sql_sch='select * FROM da_doctor_schedule WHERE status="1" and doctor_id='.$doc_id.' and (from_date<=\''.$aryDates_val.'\' and to_date>=\''.$aryDates_val.'\')';

			 }			 

			$command_sch = $connection->createCommand($sql_sch);

			$doctor_sch = $command_sch->queryAll();

			

			foreach ($doctor_sch as $doctor_sch_key=>$doctor_sch_val){

				//$doctor_sch_arr[$aryDates_val][] = $doctor_sch_val['id'];

				//$doctor_sch_arr[$aryDates_val][$doctor_sch_val['id']] = $doctor_sch_val['id'];

				

				$sql_sch_time='select * FROM da_doctor_schedule_time WHERE status="1" and schedule_id='.$doctor_sch_val['id'];

				$command_sch_time = $connection->createCommand($sql_sch_time);

				$doctor_sch_time = $command_sch_time->queryAll();

				$doctor_sch_time_arr = array();

				foreach ($doctor_sch_time as $doctor_sch_time_key=>$doctor_sch_time_val){

					$doctor_sch_time_arr[$doctor_sch_time_val['id']] = array('from_time'=>$doctor_sch_time_val['from_time'],

																		'to_time'=>$doctor_sch_time_val['to_time'],

																		'from_time_format'=>$doctor_sch_time_val['from_time_format'],

																		'to_time_format'=>$doctor_sch_time_val['to_time_format'],

																		'time_slot'=>$doctor_sch_time_val['time_slot'],

																		'laser_slot'=>$doctor_sch_time_val['laser_slot'],

																		'from_strtotime'=>strtotime($aryDates_val.' '.$doctor_sch_time_val['from_time'].' '.$doctor_sch_time_val['from_time_format']),

																		'to_strtotime'=>strtotime($aryDates_val.' '.$doctor_sch_time_val['to_time'].' '. $doctor_sch_time_val['to_time_format']),

																		'schedule_id'=>$doctor_sch_time_val['schedule_id'],

																		'address_id'=>$doctor_sch_val['address_id'],

																		'day'=>$doctor_sch_time_val['day'],

																		'on_off'=>$doctor_sch_time_val['on_off']

																		);

				

				}

				$doctor_sch_arr[$aryDates_val][$doctor_sch_val['id']] = $doctor_sch_time_arr;

			}

			$final_arr = array_merge($date_arr,$doctor_sch_arr);

			$doctor_id_sch_time_arr_all[$doc_id] = $final_arr;

			

			

		}

		return $doctor_id_sch_time_arr_all;

	}

	

	function createRange($start, $end, $format = 'Y-m-d') {

		$start  = new DateTime($start);

		$end    = new DateTime($end);

		$invert = $start > $end;

	

		$dates = array();

		$dates[] = $start->format($format);

		while ($start != $end) {

			$start->modify(($invert ? '-' : '+') . '1 day');

			$dates[] = $start->format($format);

		}

		return $dates;

	}


	public function actionAppointmentSchedule(){
		if( !isset($_REQUEST['today_date']) ) {
			$this->redirect(array('404error'));
		}
		$today_date  = $_REQUEST['today_date'];
		$view_date  = $_REQUEST['view_date'];
		$type= Yii::app()->request->getParam('type');
		$doctor_id = Yii::app()->request->getParam('doctor_id');
		$speciality_id = Yii::app()->request->getParam('speciality_id');
		$procedure_id = Yii::app()->request->getParam('procedure_id');
		$doctor_address_id = Yii::app()->request->getParam('address_id');
		$indexCount = Yii::app()->request->getParam('index_id_pop');
	
		$usercriteria = array();
		if($type == 'next'){
			$from_view_date = strtotime($view_date);
			$mk_from_time = date("Y-m-d", mktime(0,0,0,date('m',$from_view_date),date('d',$from_view_date)+5,date('Y',$from_view_date)));
			$to_view_date = strtotime($mk_from_time);
			$mk_to_time = date("Y-m-d", mktime(0,0,0,date('m',$to_view_date),date('d',$to_view_date)+4,date('Y',$to_view_date)));
		}else{
			$from_view_date = strtotime($view_date);
			$mk_from_time = date("Y-m-d", mktime(0,0,0,date('m',$from_view_date),date('d',$from_view_date)-intVal(5),date('Y',$from_view_date)));
			$to_view_date = strtotime($mk_from_time);
			$mk_to_time = date("Y-m-d", mktime(0,0,0,date('m',$to_view_date),date('d',$to_view_date)+4,date('Y',$to_view_date)));
		}
	
		$str_str_head = '';
		$str_str_head .= '<ul>
						  <li>';
		$str_str_head .= date('D', strtotime($mk_from_time));
		$str_str_head .= '<font style="font-size:12px; display:block; font-weight:normal;">';
		$str_str_head .= date('m-d-Y', strtotime($mk_from_time));
		$str_str_head .= '</font></li>
						  <li>';
		$str_str_head .= date('D', strtotime($mk_from_time)+3600*24);
		$str_str_head .= '<font style="font-size:12px; display:block; font-weight:normal;">';
		$str_str_head .= date('m-d-Y', strtotime($mk_from_time)+3600*24);
		$str_str_head .= '</font></li>
						  <li>';
		$str_str_head .= date('D', strtotime($mk_from_time)+3600*24*2);
		$str_str_head .= '<font style="font-size:12px; display:block; font-weight:normal;">';
		$str_str_head .= date('m-d-Y', strtotime($mk_from_time)+3600*24*2);
		$str_str_head .= '</font></li>
						  <li>';
		$str_str_head .= date('D', strtotime($mk_from_time)+3600*24*3);
		$str_str_head .= '<font style="font-size:12px; display:block; font-weight:normal;">';
		$str_str_head .= date('m-d-Y', strtotime($mk_from_time)+3600*24*3);
		$str_str_head .= '</font></li>
						  <li>';
		$str_str_head .= date('D', strtotime($mk_from_time)+3600*24*4);
		$str_str_head .= '<font style="font-size:12px; display:block; font-weight:normal;">';
		$str_str_head .= date('m-d-Y', strtotime($mk_from_time)+3600*24*4);
		$str_str_head .= '</font></li>
						  </ul>';
	
		$str_str = '';
		$sch_width = "width: 20%";
		if( Yii::app()->request->getParam('sch_width') ) {
			$sch_width = "width: 14%";
		}
		$view_ary_sch = $this->doctorSchTime($doctor_id,$mk_from_time,$mk_to_time, 'Y-m-d',$doctor_address_id);
		$doctorAppTime = $this->doctorAppTime($doctor_id,$mk_from_time,$mk_to_time, 'Y-m-d',$doctor_address_id);
		$doctorTimeOff = $this->doctorTimeOff($doctor_id,$mk_from_time,$mk_to_time, 'Y-m-d');
		if(!empty($view_ary_sch[$doctor_id])){
			foreach($view_ary_sch[$doctor_id] as $view_ary_sch_key=>$view_ary_sch_val){
				if(!empty($view_ary_sch_val)){
					$view_ary_sch_time_cnt=false;
					foreach($view_ary_sch_val as $view_ary_sch_time){
						if(isset($view_ary_sch_time) && !empty($view_ary_sch_time))
							$view_ary_sch_time_cnt=true;
					}
					if($view_ary_sch_time_cnt){
						$str_str .= '<div class="sch_time" >';
						$tot_cnt=0;$inner_five = 0;
						foreach($view_ary_sch_val as $view_ary_sch_time_val){
							if($tot_cnt<5){
								foreach($view_ary_sch_time_val as $view_ary_sch_time_val_arr){
									if($view_ary_sch_time_val_arr['day']==date('l',strtotime($view_ary_sch_key))){
										if($view_ary_sch_time_val_arr['on_off']==1){
											$view_ary_to_strtotime=$view_ary_sch_time_val_arr['to_strtotime'];
											$view_ary_from_strtotime=$view_ary_sch_time_val_arr['from_strtotime'];
											$time_diff = $view_ary_to_strtotime-$view_ary_from_strtotime;
											$view_ary_time_slot=$view_ary_sch_time_val_arr['time_slot']*60;
											$view_ary_laser_slot=$view_ary_sch_time_val_arr['laser_slot']*60;
	
											for($iCnt=0;$iCnt<5;$iCnt++){
												//$tot_cnt++;
												$added_time = $view_ary_time_slot+$view_ary_laser_slot;
												$show_time=$view_ary_from_strtotime+($added_time*$iCnt);
												if($show_time < $view_ary_to_strtotime){
													$tot_cnt++;$inner_five++;
													if($tot_cnt<=5){
														if($iCnt==0 && $inner_five<5){
															if(isset($doctorAppTime[$doctor_id][$view_ary_sch_key]) && in_array($view_ary_sch_time_val_arr['from_strtotime'],$doctorAppTime[$doctor_id][$view_ary_sch_key])){
																$str_str .= '<span class="innertable disable"><a class="blank" href="javascript:void(0);">'.date('h:i  a',$view_ary_sch_time_val_arr['from_strtotime']).'</a></span>';
															}else{
																if(isset($doctorTimeOff[$doctor_id][$view_ary_sch_key]) && $view_ary_sch_time_val_arr['from_strtotime']>=$doctorTimeOff[$doctor_id][$view_ary_sch_key][0]['strt_time_off'] && $view_ary_sch_time_val_arr['from_strtotime']<=$doctorTimeOff[$doctor_id][$view_ary_sch_key][0]['to_time_off']){
																	$str_str .= '<span class="innertable disable"><a class="blank" href="javascript:void(0);">'.date('h:i  a',$view_ary_sch_time_val_arr['from_strtotime']).'</a></span>';
																}else{
																	$str_str .= '<span class="innertable"><a href="'.$this->createAbsoluteUrl('doctor/DoctorBookStep1/?start_time='.$view_ary_sch_time_val_arr['from_strtotime'].'&time_slot='.$view_ary_time_slot.'&doctor_id='.$doctor_id.'&address_id='.$view_ary_sch_time_val_arr['address_id'].'&speciality_id='.$speciality_id).'&procedure_id='.$procedure_id.'">'.date('h:i  a',$view_ary_sch_time_val_arr['from_strtotime']).'</a></span>';
																}
															}
														}elseif($iCnt>0 && $iCnt<4 && $inner_five<5){
															if($show_time>$view_ary_to_strtotime){
																$str_str .= '<span class="innertable disable"><a href="javascript:void(0);">'.date('h:i a',$show_time).'</a></span>';
															}else{
																if(isset($doctorAppTime[$doctor_id][$view_ary_sch_key]) && in_array($show_time,$doctorAppTime[$doctor_id][$view_ary_sch_key])){
																	$str_str .= '<span class="innertable disable"><a class="blank" href="javascript:void(0);">'.date('h:i  a',$show_time).'</a></span>';
																}else{
																	if(isset($doctorTimeOff[$doctor_id][$view_ary_sch_key]) && $show_time>=$doctorTimeOff[$doctor_id][$view_ary_sch_key][0]['strt_time_off'] && $show_time<=$doctorTimeOff[$doctor_id][$view_ary_sch_key][0]['to_time_off']){
																		$str_str .= '<span class="innertable disable"><a class="blank" href="javascript:void(0);">'.date('h:i  a',$show_time).'</a></span>';
																	}else{
																		$str_str .= '<span class="innertable"><a href="'.$this->createAbsoluteUrl('doctor/DoctorBookStep1/?start_time='.$show_time.'&time_slot='.$view_ary_time_slot.'&doctor_id='.$doctor_id.'&address_id='.$view_ary_sch_time_val_arr['address_id'].'&speciality_id='.$speciality_id).'&procedure_id='.$procedure_id.'">'.date('h:i a',$show_time).'</a></span>';
																	}
																}
															}
														}elseif($iCnt==4){
															//$str_str .= "<span class=\"innertable\"><a href=\"".$this->createAbsoluteUrl("doctor/searchDoctorProfile/id/".$doctor_id."/app_date/".strtotime($view_ary_sch_key))."\" target=\"_blank\">more...</a></span>";
															//$str_str .= "<span class=\"innertable\"><a href=\"javascript:void(0);\"  class=\"popupApp\" data-id=\"".$view_ary_sch_time_val_arr['address_id']."\" accesskey=\"".strtotime($view_ary_sch_key)."\">more...</a></span>";
															$str_str .=  "<span class=\"innertable\"><a href=\"javascript:void(0);\" class=\"popupApp\" data-id=\"".$doctor_address_id."|".$speciality_id."|".$procedure_id."|".$doctor_id."|".$indexCount."\" accesskey=\"".strtotime($view_ary_sch_key)."\">more...</a></span>";
														}
															
														if($inner_five==5 && $iCnt<4){
															//$str_str .= "<span class=\"innertable\"><a href=\"".$this->createAbsoluteUrl("doctor/searchDoctorProfile/id/".$doctor_id."/app_date/".strtotime($view_ary_sch_key))."\" target=\"_blank\">more...</a></span>";
															//$str_str .= "<span class=\"innertable\"><a href=\"javascript:void(0);\"  class=\"popupApp\" data-id=\"".$view_ary_sch_time_val_arr['address_id']."\" accesskey=\"".strtotime($view_ary_sch_key)."\">more...</a></span>";
															$str_str .=  "<span class=\"innertable\"><a href=\"javascript:void(0);\" class=\"popupApp\" data-id=\"".$doctor_address_id."|".$speciality_id."|".$procedure_id."|".$doctor_id."|".$indexCount."\" accesskey=\"".strtotime($view_ary_sch_key)."\">more...</a></span>";
														}
													}
												}else{
	
												}
											}
										}else{
											$tot_cnt = 5;
											$str_str .= '<span class="innertable disable"><a class="blank" href="javascript:void(0);">08:00 am</a></span>
										<span class="innertable disable"><a class="blank" href="javascript:void(0);">08:30 am</a></span>
										<span class="innertable disable"><a class="blank" href="javascript:void(0);">09:00 am</a></span>
										<span class="innertable disable"><a class="blank" href="javascript:void(0);">09:30 am</a></span>
										<span class="innertable disable"><a class="blank" href="javascript:void(0);">10:00 am</a></span>';
										}
									}
								}
							}else{
								break;
							}
						}
						if($inner_five==1){
							$str_str .= '<span class="innertable disable"><a class="blank" href="javascript:void(0);">08:00 am</a></span>
										<span class="innertable disable"><a class="blank" href="javascript:void(0);">08:30 am</a></span>
										<span class="innertable disable"><a class="blank" href="javascript:void(0);">09:00 am</a></span>
							<span class="innertable disable"><a class="blank" href="javascript:void(0);">09:30 am</a></span>';
						}else if($inner_five==2){
							$str_str .= '<span class="innertable disable"><a class="blank" href="javascript:void(0);">08:00 am</a></span>
										<span class="innertable disable"><a class="blank" href="javascript:void(0);">08:30 am</a></span>
							<span class="innertable disable"><a class="blank" href="javascript:void(0);">09:00 am</a></span>';
						}else if($inner_five==3){
							$str_str .= '<span class="innertable disable"><a class="blank" href="javascript:void(0);">08:00 am</a></span>
							<span class="innertable disable"><a class="blank" href="javascript:void(0);">08:30 am</a></span>';
						}else if($inner_five==4){
							$str_str .= '<span class="innertable disable"><a class="blank" href="javascript:void(0);">08:30 am</a></span>';
						}
	
						$str_str .= '</div>';
	
					} else{
						$str_str .= '<div class="sch_time" >
										<span class="innertable disable"><a class="blank" href="javascript:void(0);">08:00 am</a></span>
										<span class="innertable disable"><a class="blank" href="javascript:void(0);">08:30 am</a></span>
										<span class="innertable disable"><a class="blank" href="javascript:void(0);">09:00 am</a></span>
										<span class="innertable disable"><a class="blank" href="javascript:void(0);">09:30 am</a></span>
										<span class="innertable disable"><a class="blank" href="javascript:void(0);">more...</a></span>';
						$str_str .= '</div>';
					}
				}else{
					$str_str .= '<div class="sch_time" >
									<span class="innertable disable"><a class="blank" href="javascript:void(0);">08:00 am</a></span>
									<span class="innertable disable"><a class="blank" href="javascript:void(0);">08:30 am</a></span>
									<span class="innertable disable"><a class="blank" href="javascript:void(0);">09:00 am</a></span>
									<span class="innertable disable"><a class="blank" href="javascript:void(0);">09:30 am</a></span>
									<span class="innertable disable"><a class="blank" href="javascript:void(0);">more...</a></span>';
					$str_str .= '</div>';
	
				}
			}
		}else{
			$str_str .= '<div class="calander_box_lisiting">
							No availability these days.
						</div>';
		}
		echo $str_str_head.'***%%%***'.$str_str.'***%%%***'.$mk_from_time;
	}
	

public function actionAppointmentScheduleProfile(){
		if( !isset($_REQUEST['today_date']) ) {
			$this->redirect(array('404error'));
		}
		$today_date  = $_REQUEST['today_date'];
		$view_date  = $_REQUEST['view_date'];
		//echo $today_date.'|'.$view_date;
		
		$type= Yii::app()->request->getParam('type');
		$doctor_id = Yii::app()->request->getParam('doctor_id');
		$speciality_id = Yii::app()->request->getParam('speciality_id');
		$procedure_id = Yii::app()->request->getParam('procedure_id');
		$doctor_address_id = Yii::app()->request->getParam('address_id');
		
		$usercriteria = array();
		
		if($type == 'next'){
			$from_view_date = strtotime($view_date);
			$mk_from_time = date("Y-m-d", mktime(0,0,0,date('m',$from_view_date),date('d',$from_view_date)+7,date('Y',$from_view_date)));
			$to_view_date = strtotime($mk_from_time);
			$mk_to_time = date("Y-m-d", mktime(0,0,0,date('m',$to_view_date),date('d',$to_view_date)+6,date('Y',$to_view_date)));
		}else{
			$from_view_date = strtotime($view_date);
			$mk_from_time = date("Y-m-d", mktime(0,0,0,date('m',$from_view_date),date('d',$from_view_date)-7,date('Y',$from_view_date)));
			$to_view_date = strtotime($mk_from_time);
			$mk_to_time = date("Y-m-d", mktime(0,0,0,date('m',$to_view_date),date('d',$to_view_date)+6,date('Y',$to_view_date)));
		}
		$str_str_head = '';
		$str_str_head .= '<ul>
						  <li>';
		$str_str_head .= date('D', strtotime($mk_from_time));
		$str_str_head .= '<font style="font-size:12px; display:block; font-weight:normal;">';
		$str_str_head .= date('m-d-Y', strtotime($mk_from_time));
		$str_str_head .= '</font></li>
						  <li>';
		$str_str_head .= date('D', strtotime($mk_from_time)+3600*24);
		$str_str_head .= '<font style="font-size:12px; display:block; font-weight:normal;">';
		$str_str_head .= date('m-d-Y', strtotime($mk_from_time)+3600*24);
		$str_str_head .= '</font></li>
						  <li>';
		$str_str_head .= date('D', strtotime($mk_from_time)+3600*24*2);
		$str_str_head .= '<font style="font-size:12px; display:block; font-weight:normal;">';
		$str_str_head .= date('m-d-Y', strtotime($mk_from_time)+3600*24*2);
		$str_str_head .= '</font></li>
						  <li>';
		$str_str_head .= date('D', strtotime($mk_from_time)+3600*24*3);
		$str_str_head .= '<font style="font-size:12px; display:block; font-weight:normal;">';
		$str_str_head .= date('m-d-Y', strtotime($mk_from_time)+3600*24*3);
		$str_str_head .= '</font></li>
						  <li>';
		$str_str_head .= date('D', strtotime($mk_from_time)+3600*24*4);
		$str_str_head .= '<font style="font-size:12px; display:block; font-weight:normal;">';
		$str_str_head .= date('m-d-Y', strtotime($mk_from_time)+3600*24*4);
		$str_str_head .= '</font></li>
						  <li>';
		$str_str_head .= date('D', strtotime($mk_from_time)+3600*24*5);
		$str_str_head .= '<font style="font-size:12px; display:block; font-weight:normal;">';
		$str_str_head .= date('m-d-Y', strtotime($mk_from_time)+3600*24*5);
		$str_str_head .= '</font></li>
						  <li>';
		$str_str_head .= date('D', strtotime($mk_from_time)+3600*24*6);
		$str_str_head .= '<font style="font-size:12px; display:block; font-weight:normal;">';
		$str_str_head .= date('m-d-Y', strtotime($mk_from_time)+3600*24*6);
		$str_str_head .= '</font></li>
						  </ul>';
		
		$str_str = '';
		$sch_width = "width: 20%";
		if( Yii::app()->request->getParam('sch_width') ) {
			$sch_width = "width: 14%";
		}
			 
			 
			 $view_ary_sch = $this->doctorSchTime($doctor_id,$mk_from_time,$mk_to_time, 'Y-m-d',$doctor_address_id);
			 $doctorAppTime = $this->doctorAppTime($doctor_id,$mk_from_time,$mk_to_time, 'Y-m-d',$doctor_address_id);
			 $doctorTimeOff = $this->doctorTimeOff($doctor_id,$mk_from_time,$mk_to_time, 'Y-m-d');
			 //Helpers::pre($view_ary_sch);
		 if(!empty($view_ary_sch[$doctor_id])){
			 
		
		 $str_str .= '<div class="calander_box_lisiting"><div class="column_bar_left"></div>';
		
		 foreach($view_ary_sch[$doctor_id] as $view_ary_sch_key=>$view_ary_sch_val){
		
		
		   
			if(!empty($view_ary_sch_val)){ 
			 $view_ary_sch_time_cnt=false;
			 foreach($view_ary_sch_val as $view_ary_sch_time){
				 if(isset($view_ary_sch_time) && !empty($view_ary_sch_time))
					$view_ary_sch_time_cnt=true;
			 }
			 if($view_ary_sch_time_cnt){ 
		
				$str_str .= '<div class="sch_time_profile">';
		
					$tot_cnt=0;$inner_five = 0;
					foreach($view_ary_sch_val as $view_ary_sch_time_val){
						if($tot_cnt<5){
						foreach($view_ary_sch_time_val as $view_ary_sch_time_val_arr){
							if($view_ary_sch_time_val_arr['day']==date('l',strtotime($view_ary_sch_key))){
								if($view_ary_sch_time_val_arr['on_off']==1){//if($inner_five>=4)break;
									$view_ary_to_strtotime=$view_ary_sch_time_val_arr['to_strtotime'];
									$view_ary_from_strtotime=$view_ary_sch_time_val_arr['from_strtotime'];
									$time_diff = $view_ary_to_strtotime-$view_ary_from_strtotime;
									$view_ary_time_slot=$view_ary_sch_time_val_arr['time_slot']*60;
									$view_ary_laser_slot=$view_ary_sch_time_val_arr['laser_slot']*60;
									
									for($iCnt=0;$iCnt<5;$iCnt++){
										//$tot_cnt++;
										$added_time = $view_ary_time_slot+$view_ary_laser_slot;
										$show_time=$view_ary_from_strtotime+($added_time*$iCnt);
										if($show_time < $view_ary_to_strtotime){
											$tot_cnt++;$inner_five++;
											if($tot_cnt<=5){
											if($iCnt==0 && $inner_five<5){
												if(isset($doctorAppTime[$doctor_id][$view_ary_sch_key]) && in_array($view_ary_sch_time_val_arr['from_strtotime'],$doctorAppTime[$doctor_id][$view_ary_sch_key])){
													$str_str .= '<span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">'.date('h:i  a',$view_ary_sch_time_val_arr['from_strtotime']).'</a></span>';
												}else{
													if(isset($doctorTimeOff[$doctor_id][$view_ary_sch_key]) && $view_ary_sch_time_val_arr['from_strtotime']>=$doctorTimeOff[$doctor_id][$view_ary_sch_key][0]['strt_time_off'] && $view_ary_sch_time_val_arr['from_strtotime']<=$doctorTimeOff[$doctor_id][$view_ary_sch_key][0]['to_time_off']){
														$str_str .= '<span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">'.date('h:i  a',$view_ary_sch_time_val_arr['from_strtotime']).'</a></span>';
													}else{
														$str_str .= '<span class="innertable_time_first"><a href="'.$this->createAbsoluteUrl('doctor/DoctorBookStep1/?start_time='.$view_ary_sch_time_val_arr['from_strtotime'].'&time_slot='.$view_ary_time_slot.'&doctor_id='.$doctor_id.'&address_id='.$view_ary_sch_time_val_arr['address_id'].'&speciality_id='.$speciality_id).'&procedure_id='.$procedure_id.'">'.date('h:i  a',$view_ary_sch_time_val_arr['from_strtotime']).'</a></span>';
													}
												}
											}elseif($iCnt>0 && $iCnt<4 && $inner_five<5){
												if($show_time>$view_ary_to_strtotime){
													$str_str .= '<span class="innertable_time_first disable"><a href="javascript:void(0);">'.date('h:i a',$show_time).'</a></span>';
												}else{
													if(isset($doctorAppTime[$doctor_id][$view_ary_sch_key]) && in_array($show_time,$doctorAppTime[$doctor_id][$view_ary_sch_key])){
														$str_str .= '<span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">'.date('h:i  a',$show_time).'</a></span>';
													}else{
														if(isset($doctorTimeOff[$doctor_id][$view_ary_sch_key]) && $show_time>=$doctorTimeOff[$doctor_id][$view_ary_sch_key][0]['strt_time_off'] && $show_time<=$doctorTimeOff[$doctor_id][$view_ary_sch_key][0]['to_time_off']){
															$str_str .= '<span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">'.date('h:i  a',$show_time).'</a></span>';
														}else{
															$str_str .= '<span class="innertable_time_first"><a href="'.$this->createAbsoluteUrl('doctor/DoctorBookStep1/?start_time='.$show_time.'&time_slot='.$view_ary_time_slot.'&doctor_id='.$doctor_id.'&address_id='.$view_ary_sch_time_val_arr['address_id'].'&speciality_id='.$speciality_id).'&procedure_id='.$procedure_id.'">'.date('h:i a',$show_time).'</a></span>';
														}
													}
												}
											}elseif($iCnt==4){
												//$str_str .= "<span class=\"innertable_time_first\"><a href=\"".$this->createAbsoluteUrl("doctor/searchDoctorProfile/id/".$doctor_id."/app_date/".strtotime($view_ary_sch_key))."\" target=\"_blank\">more...</a></span>";
												$str_str .= "<span class=\"innertable_time_first\"><a href=\"javascript:void(0);\"  class=\"popupApp\" data-id=\"".$view_ary_sch_time_val_arr['address_id']."\" accesskey=\"".strtotime($view_ary_sch_key)."\">more...</a></span>";
											}
											
											if($inner_five==5 && $iCnt<4){
												//$str_str .= "<span class=\"innertable_time_first\"><a href=\"".$this->createAbsoluteUrl("doctor/searchDoctorProfile/id/".$doctor_id."/app_date/".strtotime($view_ary_sch_key))."\" target=\"_blank\">more...</a></span>";
												$str_str .= "<span class=\"innertable_time_first\"><a href=\"javascript:void(0);\"  class=\"popupApp\" data-id=\"".$view_ary_sch_time_val_arr['address_id']."\" accesskey=\"".strtotime($view_ary_sch_key)."\">more...</a></span>";
											}
											}
										}else{
											
										}
									}
								}else{
								   $tot_cnt = 5;
								   $str_str .= '<span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">08:00 am</a></span>
										<span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">08:30 am</a></span>
										<span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">09:00 am</a></span>
										<span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">09:30 am</a></span>
										<span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">10:00 am</a></span>';
								}
							}
						}
						}else{
							break;	
						}
					}
						if($inner_five==1){
							$str_str .= '<span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">08:00 am</a></span>
										<span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">08:30 am</a></span>
										<span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">09:00 am</a></span>
							<span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">09:30 am</a></span>';
						}else if($inner_five==2){
							$str_str .= '<span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">08:00 am</a></span>
										<span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">08:30 am</a></span>
							<span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">09:00 am</a></span>';
						}else if($inner_five==3){
							$str_str .= '<span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">08:00 am</a></span>
							<span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">08:30 am</a></span>';
						}else if($inner_five==4){
							$str_str .= '<span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">08:30 am</a></span>';
						}
						
				$str_str .= '</div>';
				
				 }
				 else{
					 
				$str_str .= '<div class="sch_time" style="'.$sch_width.'" >
					<span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">08:00 am</a></span>
					<span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">08:30 am</a></span>
					<span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">09:00 am</a></span>
					<span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">09:30 am</a></span>
					<span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">more...</a></span>';
					//$str_str .= "<span class=\"innertable_time_first\"><a href=\"".$this->createAbsoluteUrl("doctor/searchDoctorProfile/id/".$doctor_id."/app_date/".strtotime($view_ary_sch_key))."\" target=\"_blank\">more...</a></span>";
				$str_str .= '</div>';
				 
				 }
				 
			 }
			 else{
			
			$str_str .= '<div class="sch_time" style="'.$sch_width.'" >
				<span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">08:00 am</a></span>
				<span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">08:30 am</a></span>
				<span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">09:00 am</a></span>
				<span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">09:30 am</a></span>
				<span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">more...</a></span>';
				//$str_str .= "<span class=\"innertable_time_first\"><a href=\"".$this->createAbsoluteUrl("doctor/searchDoctorProfile/id/".$doctor_id."/app_date/".strtotime($view_ary_sch_key))."\" target=\"_blank\">more...</a></span>";
			$str_str .= '</div>';
			 
			 }
		
		
		
			}
		
			$str_str .= '<div class="column_bar_right"></div></div>';
		
		 }
		 else{
		
		$str_str .= '<div class="calander_box_lisiting">
			No availability these days.
		</div>';
		 }
		
		 
		 
		echo $str_str_head.'***%%%***'.$str_str.'***%%%***'.$mk_from_time;
	}
	
	public function actionAppointmentScheduleProfileDay(){
		$today_date  = date('Y-m-d',$_REQUEST['today_date']);
		$view_date  = date('Y-m-d',$_REQUEST['view_date']);
		//echo $today_date.'|'.$view_date;
		
		$doctor_id = Yii::app()->request->getParam('doctor_id');
		$speciality_id = Yii::app()->request->getParam('speciality_id');
		$procedure_id = Yii::app()->request->getParam('procedure_id');
		$doctor_address_id = Yii::app()->request->getParam('address_id');
		$countPopId = Yii::app()->request->getParam('countPopId');
		
		$str_str = '';
			 
		 $view_ary_sch = $this->doctorSchTime($doctor_id,$today_date,$view_date, 'Y-m-d',$doctor_address_id);
		 $doctorAppTime = $this->doctorAppTimeMore($doctor_id,$today_date,$view_date, 'Y-m-d',$doctor_address_id);
		 $doctorTimeOff = $this->doctorTimeOffDay($doctor_id,$today_date,$view_date, 'Y-m-d');
		 //Helpers::pre($doctorTimeOff);
		 if(!empty($view_ary_sch[$doctor_id])){
			 
		
		// $str_str .= '<div class="calander_box_lisiting"><div class="column_bar_left"></div>';
		
		 foreach($view_ary_sch[$doctor_id] as $view_ary_sch_key=>$view_ary_sch_val){
		
		
		   
			if(!empty($view_ary_sch_val)){ 
			 $view_ary_sch_time_cnt=false;
			 foreach($view_ary_sch_val as $view_ary_sch_time){
				 if(isset($view_ary_sch_time) && !empty($view_ary_sch_time))
					$view_ary_sch_time_cnt=true;
			 }
			 if($view_ary_sch_time_cnt){ 
		
				//$str_str .= '<div class="sch_time_profile">';
		
					$tot_cnt=0;$inner_five = 0;
					foreach($view_ary_sch_val as $view_ary_sch_time_val){
						foreach($view_ary_sch_time_val as $view_ary_sch_time_val_arr){
							if($view_ary_sch_time_val_arr['day']==date('l',strtotime($view_ary_sch_key))){
								if($view_ary_sch_time_val_arr['on_off']==1){//if($inner_five>=4)break;
									$view_ary_to_strtotime=$view_ary_sch_time_val_arr['to_strtotime'];
									$view_ary_from_strtotime=$view_ary_sch_time_val_arr['from_strtotime'];
									$time_diff = $view_ary_to_strtotime-$view_ary_from_strtotime;
									$view_ary_time_slot=$view_ary_sch_time_val_arr['time_slot']*60;
									$view_ary_laser_slot=$view_ary_sch_time_val_arr['laser_slot']*60;
									
									$time_length = ($view_ary_to_strtotime-$view_ary_from_strtotime)/(($view_ary_sch_time_val_arr['time_slot']+$view_ary_sch_time_val_arr['laser_slot']*60));
									for($iCnt=0;$iCnt<$time_length;$iCnt++){
										//$tot_cnt++;
										$added_time = $view_ary_time_slot+$view_ary_laser_slot;
										$show_time=$view_ary_from_strtotime+($added_time*$iCnt);
										if($show_time < $view_ary_to_strtotime){
											$tot_cnt++;$inner_five++;
											if($iCnt==0){
												if(isset($doctorAppTime[$doctor_id][$view_ary_sch_key])&&in_array($view_ary_sch_time_val_arr['from_strtotime'],$doctorAppTime[$doctor_id][$view_ary_sch_key])){
												$str_str .= '<span class="innertable_time_popup disable"><a class="blank" href="javascript:void(0);">'.date('h:i  a',$view_ary_sch_time_val_arr['from_strtotime']).'</a></span>';
												}else{
													if(isset($doctorTimeOff[$doctor_id][$view_ary_sch_key]) && $view_ary_sch_time_val_arr['from_strtotime']>=$doctorTimeOff[$doctor_id][$view_ary_sch_key][0]['strt_time_off'] && $view_ary_sch_time_val_arr['from_strtotime']<=$doctorTimeOff[$doctor_id][$view_ary_sch_key][0]['to_time_off']){
														$str_str .= '<span class="innertable_time_popup disable"><a class="blank" href="javascript:void(0);">'.date('h:i  a',$view_ary_sch_time_val_arr['from_strtotime']).'</a></span>';
													}else{
														$str_str .= '<span class="innertable_time_popup"><a href="'.$this->createAbsoluteUrl('doctor/DoctorBookStep1/?start_time='.$view_ary_sch_time_val_arr['from_strtotime'].'&doctor_id='.$doctor_id.'&address_id='.$view_ary_sch_time_val_arr['address_id'].'&speciality_id='.$speciality_id).'&time_slot='.$view_ary_time_slot.'&procedure_id='.$procedure_id.'">'.date('h:i  a',$view_ary_sch_time_val_arr['from_strtotime']).'</a></span>';
													}
												}
											}else{
												if(isset($doctorAppTime[$doctor_id][$view_ary_sch_key])&&in_array($show_time,$doctorAppTime[$doctor_id][$view_ary_sch_key])){
												$str_str .= '<span class="innertable_time_popup disable"><a class="blank" href="javascript:void(0);">'.date('h:i  a',$show_time).'</a></span>';
												}else{
													if(isset($doctorTimeOff[$doctor_id][$view_ary_sch_key]) && $show_time>=$doctorTimeOff[$doctor_id][$view_ary_sch_key][0]['strt_time_off'] && $show_time<=$doctorTimeOff[$doctor_id][$view_ary_sch_key][0]['to_time_off']){
														$str_str .= '<span class="innertable_time_popup disable"><a class="blank" href="javascript:void(0);">'.date('h:i  a',$show_time).'</a></span>';
													}else{
														$str_str .= '<span class="innertable_time_popup"><a href="'.$this->createAbsoluteUrl('doctor/DoctorBookStep1/?start_time='.$show_time.'&time_slot='.$view_ary_time_slot.'&doctor_id='.$doctor_id.'&address_id='.$view_ary_sch_time_val_arr['address_id'].'&speciality_id='.$speciality_id).'&procedure_id='.$procedure_id.'">'.date('h:i a',$show_time).'</a></span>';
													}
												}
											}
										}
									}
								}
							}
						}
					}
						
				$str_str .= '</div>';
				
				 }
			 }
			}
		 }
		
		 if($procedure_id == '') {
		 	$procedure_id = 0;
		 }
		 $str_str_head  = date('D m-d-Y',$_REQUEST['today_date'])."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a id='backLink' style='text-decoration:underline;color:#11C06B;font-weight:bold;font-size:14px;cursor:pointer;' onclick='javascript: openSchedual();' >Back</a>";
		echo $str_str_head.'***%%%***'.$str_str;
	}


	/* --------------------- More Time Area Start ---------------------------- */
	
	public function actionAppointmentScheduleProfileDay_(){
		$today_date  = date('Y-m-d',$_REQUEST['today_date']);
		$view_date  = $_REQUEST['view_date'];
	
		$doctor_id = Yii::app()->request->getParam('doctor_id');
		$speciality_id = Yii::app()->request->getParam('speciality_id');
		$procedure_id = Yii::app()->request->getParam('procedure_id');
		$doctor_address_id = Yii::app()->request->getParam('address_id');
		$countPopId = Yii::app()->request->getParam('countPopId');
	
		$str_str = '';
	
		$view_ary_sch = $this->doctorSchTime($doctor_id,$today_date,$view_date, 'Y-m-d',$doctor_address_id);
		$doctorAppTime = $this->doctorAppTimeMore($doctor_id,$today_date,$view_date, 'Y-m-d',$doctor_address_id);
		$doctorTimeOff = $this->doctorTimeOffDay($doctor_id,$today_date,$view_date, 'Y-m-d');
		//Helpers::pre($doctorTimeOff);
		if(!empty($view_ary_sch[$doctor_id])){
	
	
			$str_str .= '<div class="calander_box" id="'.$doctor_id.'" >';
	
			foreach($view_ary_sch[$doctor_id] as $view_ary_sch_key=>$view_ary_sch_val){
	
	
					
				if(!empty($view_ary_sch_val)){
					$view_ary_sch_time_cnt=false;
					foreach($view_ary_sch_val as $view_ary_sch_time){
						if(isset($view_ary_sch_time) && !empty($view_ary_sch_time))
							$view_ary_sch_time_cnt=true;
					}
					if($view_ary_sch_time_cnt){
	
						$str_str .= '<div class="sch_time" style="width:20%;" >';
	
						$tot_cnt=0;$inner_five = 0;
						foreach($view_ary_sch_val as $view_ary_sch_time_val){
							foreach($view_ary_sch_time_val as $view_ary_sch_time_val_arr){
								if($view_ary_sch_time_val_arr['day']==date('l',strtotime($view_ary_sch_key))){
									if($view_ary_sch_time_val_arr['on_off']==1){//if($inner_five>=4)break;
										$view_ary_to_strtotime=$view_ary_sch_time_val_arr['to_strtotime'];
										$view_ary_from_strtotime=$view_ary_sch_time_val_arr['from_strtotime'];
										$time_diff = $view_ary_to_strtotime-$view_ary_from_strtotime;
										$view_ary_time_slot=$view_ary_sch_time_val_arr['time_slot']*60;
										$view_ary_laser_slot=$view_ary_sch_time_val_arr['laser_slot']*60;
											
										$time_length = ($view_ary_to_strtotime-$view_ary_from_strtotime)/(($view_ary_sch_time_val_arr['time_slot']+$view_ary_sch_time_val_arr['laser_slot']*60));
										for($iCnt=0;$iCnt<$time_length;$iCnt++){
											//$tot_cnt++;
											$added_time = $view_ary_time_slot+$view_ary_laser_slot;
											$show_time=$view_ary_from_strtotime+($added_time*$iCnt);
											if($show_time < $view_ary_to_strtotime){
												$tot_cnt++;$inner_five++;
												if($iCnt==0){
													if(isset($doctorAppTime[$doctor_id][$view_ary_sch_key])&&in_array($view_ary_sch_time_val_arr['from_strtotime'],$doctorAppTime[$doctor_id][$view_ary_sch_key])){
														$str_str .= '<span class="innertable disable"><a class="blank" href="javascript:void(0);">'.date('h:i  a',$view_ary_sch_time_val_arr['from_strtotime']).'</a></span>';
													}else{
														if(isset($doctorTimeOff[$doctor_id][$view_ary_sch_key]) && $view_ary_sch_time_val_arr['from_strtotime']>=$doctorTimeOff[$doctor_id][$view_ary_sch_key][0]['strt_time_off'] && $view_ary_sch_time_val_arr['from_strtotime']<=$doctorTimeOff[$doctor_id][$view_ary_sch_key][0]['to_time_off']){
															$str_str .= '<span class="innertable disable"><a class="blank" href="javascript:void(0);">'.date('h:i  a',$view_ary_sch_time_val_arr['from_strtotime']).'</a></span>';
														}else{
															$str_str .= '<span class="innertable"><a href="'.$this->createAbsoluteUrl('doctor/DoctorBookStep1/?start_time='.$view_ary_sch_time_val_arr['from_strtotime'].'&doctor_id='.$doctor_id.'&address_id='.$view_ary_sch_time_val_arr['address_id'].'&speciality_id='.$speciality_id).'&time_slot='.$view_ary_time_slot.'&procedure_id='.$procedure_id.'">'.date('h:i  a',$view_ary_sch_time_val_arr['from_strtotime']).'</a></span>';
														}
													}
												}else{
													if(isset($doctorAppTime[$doctor_id][$view_ary_sch_key])&&in_array($show_time,$doctorAppTime[$doctor_id][$view_ary_sch_key])){
														$str_str .= '<span class="innertable disable"><a class="blank" href="javascript:void(0);">'.date('h:i  a',$show_time).'</a></span>';
													}else{
														if(isset($doctorTimeOff[$doctor_id][$view_ary_sch_key]) && $show_time>=$doctorTimeOff[$doctor_id][$view_ary_sch_key][0]['strt_time_off'] && $show_time<=$doctorTimeOff[$doctor_id][$view_ary_sch_key][0]['to_time_off']){
															$str_str .= '<span class="innertable disable"><a class="blank" href="javascript:void(0);">'.date('h:i  a',$show_time).'</a></span>';
														}else{
															$str_str .= '<span class="innertable"><a href="'.$this->createAbsoluteUrl('doctor/DoctorBookStep1/?start_time='.$show_time.'&time_slot='.$view_ary_time_slot.'&doctor_id='.$doctor_id.'&address_id='.$view_ary_sch_time_val_arr['address_id'].'&speciality_id='.$speciality_id).'&procedure_id='.$procedure_id.'">'.date('h:i a',$show_time).'</a></span>';
														}
													}
												}
											}
										}
									}else{
										$tot_cnt = 5;
										$str_str .= '<span class="innertable disable"><a class="blank" href="javascript:void(0);">08:00 am</a></span>
											<span class="innertable disable"><a class="blank" href="javascript:void(0);">08:30 am</a></span>
											<span class="innertable disable"><a class="blank" href="javascript:void(0);">09:00 am</a></span>
											<span class="innertable disable"><a class="blank" href="javascript:void(0);">09:30 am</a></span>
											<span class="innertable disable"><a class="blank" href="javascript:void(0);">10:00 am</a></span>';
									}
								}
							}
						}
	
						$str_str .= '</div>';
	
					}else{
	
						$str_str .= '<div class="sch_time" ">
						<span class="innertable disable"><a class="blank" href="javascript:void(0);">08:00 am</a></span>
						<span class="innertable disable"><a class="blank" href="javascript:void(0);">08:30 am</a></span>
						<span class="innertable disable"><a class="blank" href="javascript:void(0);">09:00 am</a></span>
						<span class="innertable disable"><a class="blank" href="javascript:void(0);">09:30 am</a></span>
						<span class="innertable disable"><a class="blank" href="javascript:void(0);">more...</a></span>';
						$str_str .= '</div>';
	
					}
				} else{
					$str_str .= '<div class="sch_time" >
						<span class="innertable disable"><a class="blank" href="javascript:void(0);">08:00 am</a></span>
						<span class="innertable disable"><a class="blank" href="javascript:void(0);">08:30 am</a></span>
						<span class="innertable disable"><a class="blank" href="javascript:void(0);">09:00 am</a></span>
						<span class="innertable disable"><a class="blank" href="javascript:void(0);">09:30 am</a></span>
						<span class="innertable disable"><a class="blank" href="javascript:void(0);">more...</a></span>';
					$str_str .= '</div>';
						
				}
			}
		}
		$str_str .= '</div>';
	
		if($procedure_id == '') {
			$procedure_id = 0;
		}
		$str_str_head  = date('D m-d-Y',$_REQUEST['today_date'])."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a style='text-decoration:underline;color:#11C06B;font-weight:bold;font-size:14px;cursor:pointer;' onclick='javascript: openTimeSchedule($countPopId,$speciality_id,$doctor_id,$doctor_address_id,$procedure_id)' >Previews Page</a>";
		echo $str_str_head.'***%%%***'.$str_str;
	}
	
	/* --------------------- More Time Area End ---------------------------- */
	

	public function actionDoctorSearchByName($id = "")

	{

		$name= isset($_REQUEST['search_byname'])?$_REQUEST['search_byname']:'';

		

		$usercriteria = array();

		

		$sql_count="select d.id FROM da_doctor as d ";

		//$sql  = "select d.*,da.address as da_address,da.state as da_state,da.zip as da_zip FROM da_doctor as d ";

		$sql  = "select d.* FROM da_doctor as d ";

		

		/*$sql_count .= " left join da_doctor_address as da on d.id=da.doctor_id ";

		$sql .= " left join da_doctor_address as da on d.id=da.doctor_id ";*/

		

		/*$sql_count .= " WHERE d.status=1 and d.email_verified=1 and da.status=1 ";

		$sql .= " WHERE d.status=1 and d.email_verified=1 and da.status=1 ";*/

		

		$sql_count .= " WHERE d.status=1 and d.email_verified=1 ";

		$sql .= " WHERE d.status=1 and d.email_verified=1 ";

		

		$input_name = preg_replace("/[^a-zA-Z]+/", "", $name);

		

		if($id != ""){

			$sql_count .= " and d.first_name like '".$id."%' ";

			$sql .= " and d.first_name like '".$id."%' ";

		} if($name != ""){

			$sql_count .= " and concat_ws( d.first_name, ' ', d.last_name )   like '%".$input_name."%' ";

			$sql .= " and concat_ws( d.first_name, ' ', d.last_name )   like '%".$input_name."%' ";

		}

		

		/*$sql_count .= "  ";

		$sql .= " GROUP BY d.id ";*/

		$sql_count .= "  ";

		$sql .= "  ";

		

		$limit = 20;  //Till 17-06-2015 this value was 99

		$end_limit = 200; //Till 17-06-2015 this value was 99

		if(isset($_REQUEST['page']) && $_REQUEST['page']>1){

			$star_limit = ($limit*($_REQUEST['page']-1));

		}else{

			$star_limit = 0;

		}

		

		$sql .= ' limit '.$star_limit.','.$end_limit;

		$sql_count .= ' ';

		// $sql;

		$connection = Yii::app()->db;

		$command = $connection->createCommand($sql);

		$doctor_list = $command->queryAll();

		////////////

		$command_count = $connection->createCommand($sql_count);

		$doctor_list_count = $command_count->queryAll();

		

		$count=count($doctor_list_count);

		/*if(isset($doctor_list_count[0]['d_count']))

			$count=$doctor_list_count[0]['d_count'];

		else

			$count=0;*/

		$pages=new CPagination($count);

		$criteria = new CDbCriteria(array(

			//'condition' => $condition,

        	//'order' => 'category_id DESC'

		));

	

		// results per page

		$pages->pageSize=$limit;

		$pages->applyLimit($criteria);

		////////////////////////

		//print_r($doctor_list);

		$sql_spl='select * FROM da_speciality WHERE status="1"';

		$connection = Yii::app()->db;

		$command = $connection->createCommand($sql_spl);

		$user_speciality = $command->queryAll();

		$user_speciality_res = array();

		foreach ($user_speciality as $key=>$val) {

			$user_speciality_res[$val['id']] = $val['speciality'];

		}

		

		$albha_arr =array(''=>"ALL",'A'=>"A",'B'=>"B",'C'=>"C",'D'=>"D",'E'=>"E",'F'=>"F",'G'=>"G",'H'=>"H",'I'=>"I",'J'=>"J",'K'=>"K",'L'=>"L",'M'=>"N",'N'=>"N",'O'=>"O",'P'=>"P",'Q'=>"Q",'R'=>"R",'S'=>"S",'T'=>"T",'U'=>"U",'V'=>"V",'W'=>"W",'X'=>"X",'Y'=>"Y",'Z'=>"Z");

		$this->render('doctor_search_by_name',array(

			'dataProvider'=>$usercriteria,'doctor_list'=>$doctor_list,'user_speciality' => $user_speciality_res,'albha_arr' => $albha_arr,'search_alpha' => $id,'pages'=>$pages,

		));

	}

	

	public function actionDoctorSearchByLanguage($id = "")

	{

		$name= isset($_REQUEST['search_by_language'])?$_REQUEST['search_by_language']:'';

		

		$usercriteria = array();

		//$usercriteria=$_REQUEST;

		

		//$sql="select * FROM da_doctor as d left join da_doctor_speciality as ds ON d.id=ds.doctor_id WHERE d.status=1 and d.email_verified=1 and (ds.speciality_id=".$speciality_id." or (d.first_name like '%".$name."%' or d.last_name  like '%".$name."%')) ";

		$sql_count="select count( DISTINCT d.id ) AS d_count FROM da_doctor as d ";

		$sql  = "select d.* FROM da_doctor as d ";

		

		if($id != "" || $name != ""){

			$sql_count .= " left join da_doctor_language as dl on d.id=dl.doctor_id left join da_language as l on l.id=dl.language_id ";

			$sql .= " left join da_doctor_language as dl on d.id=dl.doctor_id left join da_language as l on l.id=dl.language_id ";

		}

		

		$sql_count .= " WHERE d.status=1 and d.email_verified=1 ";

		$sql .= " WHERE d.status=1 and d.email_verified=1 ";

		

		$input_name = preg_replace("/[^a-zA-Z]+/", "", $name);

		

		if($id != ""){

			$sql_count .= " and l.language like '".$id."%' ";

			$sql .= " and l.language like '".$id."%' ";

		}

		if($name != ""){

			$sql_count .= " and l.language like '%".$input_name."%' ";

			$sql .= " and l.language like '%".$input_name."%' ";

		}

		

		$sql_count .= "  ";

		$sql .= " GROUP BY d.id ";

		

		$limit = 30;

		$end_limit = 30;

		if(isset($_REQUEST['page']) && $_REQUEST['page']>1){

			$star_limit = ($limit*($_REQUEST['page']-1));

		}else{

			$star_limit = 0;

		}

		

		$sql .= ' limit '.$star_limit.','.$end_limit;

		$sql_count .= ' ';

		// $sql;

		$connection = Yii::app()->db;

		$command = $connection->createCommand($sql);

		$doctor_list = $command->queryAll();

		////////////

		$command_count = $connection->createCommand($sql_count);

		$doctor_list_count = $command_count->queryAll();

		

		//$count=count($doctor_list_count);

		if(isset($doctor_list_count[0]['d_count']))

			$count=$doctor_list_count[0]['d_count'];

		else

			$count=0;

		$pages=new CPagination($count);

		$criteria = new CDbCriteria(array(

			//'condition' => $condition,

        	//'order' => 'category_id DESC'

		));

	

		// results per page

		$pages->pageSize=$limit;

		$pages->applyLimit($criteria);

		////////////////////////

		//print_r($doctor_list);

		$sql_spl='select * FROM da_speciality WHERE status="1"';

		$connection = Yii::app()->db;

		$command = $connection->createCommand($sql_spl);

		$user_speciality = $command->queryAll();

		$user_speciality_res = array();

		foreach ($user_speciality as $key=>$val) {

			$user_speciality_res[$val['id']] = $val['speciality'];

		}

		

		$albha_arr =array(''=>"ALL",'A'=>"A",'B'=>"B",'C'=>"C",'D'=>"D",'E'=>"E",'F'=>"F",'G'=>"G",'H'=>"H",'I'=>"I",'J'=>"J",'K'=>"K",'L'=>"L",'M'=>"N",'N'=>"N",'O'=>"O",'P'=>"P",'Q'=>"Q",'R'=>"R",'S'=>"S",'T'=>"T",'U'=>"U",'V'=>"V",'W'=>"W",'X'=>"X",'Y'=>"Y",'Z'=>"Z");

		$this->render('doctor_search_by_language',array(

			'dataProvider'=>$usercriteria,'doctor_list'=>$doctor_list,'user_speciality' => $user_speciality_res,'albha_arr' => $albha_arr,'search_alpha' => $id,'pages'=>$pages,

		));

	}

	

	public function actionDoctorSearchByInsurance($id = "")

	{

		$name= isset($_REQUEST['search_by_insurance'])?$_REQUEST['search_by_insurance']:'';

		

		$usercriteria = array();

		

		$sql_count="select count( DISTINCT d.id ) AS d_count FROM da_doctor as d ";

		$sql  = "select d.* FROM da_doctor as d ";

		

		if($id != "" || $name != ""){

			$sql_count .= " left join da_doctor_insurance as di on d.id=di.doctor_id left join da_insurance as i on i.id=di.insurance_id ";

			$sql .= " left join da_doctor_insurance as di on d.id=di.doctor_id left join da_insurance as i on i.id=di.insurance_id ";

		}

		

		$sql_count .= " WHERE d.status=1 and d.email_verified=1 ";

		$sql .= " WHERE d.status=1 and d.email_verified=1 ";

		

		//$input_name = preg_replace("/[^a-zA-Z]+/", "", $name);

		$input_name = trim($name);

		

		if($id != ""){

			$sql_count .= " and i.insurance like '".$id."%' ";

			$sql .= " and i.insurance like '".$id."%' ";

		}

		if($name != ""){

			$sql_count .= " and i.insurance like '%".$input_name."%' ";

			$sql .= " and i.insurance like '%".$input_name."%' ";

		}

		

		$sql_count .= "  ";

		$sql .= " GROUP BY d.id ";

		

		$limit = 30;

		$end_limit = 30;

		if(isset($_REQUEST['page']) && $_REQUEST['page']>1){

			$star_limit = ($limit*($_REQUEST['page']-1));

		}else{

			$star_limit = 0;

		}

		

		$sql .= ' limit '.$star_limit.','.$end_limit;

		$sql_count .= ' ';

		// $sql;

		$connection = Yii::app()->db;

		$command = $connection->createCommand($sql);

		$doctor_list = $command->queryAll();

		////////////

		$command_count = $connection->createCommand($sql_count);

		$doctor_list_count = $command_count->queryAll();

		

		//$count=count($doctor_list_count);

		if(isset($doctor_list_count[0]['d_count']))

			$count=$doctor_list_count[0]['d_count'];

		else

			$count=0;

		$pages=new CPagination($count);

		$criteria = new CDbCriteria(array(

			//'condition' => $condition,

        	//'order' => 'category_id DESC'

		));

	

		// results per page

		$pages->pageSize=$limit;

		$pages->applyLimit($criteria);

		////////////////////////

		//print_r($doctor_list);

		$sql_spl='select * FROM da_speciality WHERE status="1"';

		$connection = Yii::app()->db;

		$command = $connection->createCommand($sql_spl);

		$user_speciality = $command->queryAll();

		$user_speciality_res = array();

		foreach ($user_speciality as $key=>$val) {

			$user_speciality_res[$val['id']] = $val['speciality'];

		}

		

		$albha_arr =array(''=>"ALL",'A'=>"A",'B'=>"B",'C'=>"C",'D'=>"D",'E'=>"E",'F'=>"F",'G'=>"G",'H'=>"H",'I'=>"I",'J'=>"J",'K'=>"K",'L'=>"L",'M'=>"N",'N'=>"N",'O'=>"O",'P'=>"P",'Q'=>"Q",'R'=>"R",'S'=>"S",'T'=>"T",'U'=>"U",'V'=>"V",'W'=>"W",'X'=>"X",'Y'=>"Y",'Z'=>"Z");

		$this->render('doctor_search_by_insurance',array(

			'dataProvider'=>$usercriteria,'doctor_list'=>$doctor_list,'user_speciality' => $user_speciality_res,'albha_arr' => $albha_arr,'search_alpha' => $id,'pages'=>$pages,

		));

	}

		

	public function actionDoctorSearchByProcedure($id = "")

	{

		$name= isset($_REQUEST['search_by_procedure'])?$_REQUEST['search_by_procedure']:'';

		

		$usercriteria = array();

		//$usercriteria=$_REQUEST;

		

		//$sql="select * FROM da_doctor as d left join da_doctor_speciality as ds ON d.id=ds.doctor_id WHERE d.status=1 and d.email_verified=1 and (ds.speciality_id=".$speciality_id." or (d.first_name like '%".$name."%' or d.last_name  like '%".$name."%')) ";

		$sql_count="select count( DISTINCT d.id ) AS d_count FROM da_doctor as d ";

		$sql  = "select d.* FROM da_doctor as d ";

		

		if($id != "" || $name != ""){

			$sql_count .= " left join da_doctor_procedure as dp on d.id=dp.doctor_id left join da_procedure as p on p.id=dp.procedure_id ";

			$sql .= " left join da_doctor_procedure as dp on d.id=dp.doctor_id left join da_procedure as p on p.id=dp.procedure_id ";

		}

		

		$sql_count .= " WHERE d.status=1 and d.email_verified=1 ";

		$sql .= " WHERE d.status=1 and d.email_verified=1 ";

		

		//$input_name = preg_replace("/[^a-zA-Z]+/", "", $name);

		$input_name = trim($name);

		

		if($id != ""){

			$sql_count .= " and p.procedure like '".$id."%' ";

			$sql .= " and p.procedure like '".$id."%' ";

		}

		if($name != ""){

			$sql_count .= " and p.procedure like '%".$input_name."%' ";

			$sql .= " and p.procedure like '%".$input_name."%' ";

		}

		

		$sql_count .= "  ";

		$sql .= " GROUP BY d.id ";

		

		$limit = 30;

		$end_limit = 30;

		if(isset($_REQUEST['page']) && $_REQUEST['page']>1){

			$star_limit = ($limit*($_REQUEST['page']-1));

		}else{

			$star_limit = 0;

		}

		

		$sql .= ' limit '.$star_limit.','.$end_limit;

		$sql_count .= ' ';

		//echo $sql;

		$connection = Yii::app()->db;

		$command = $connection->createCommand($sql);

		$doctor_list = $command->queryAll();

		////////////

		$command_count = $connection->createCommand($sql_count);

		$doctor_list_count = $command_count->queryAll();

		

		//$count=count($doctor_list_count);

		if(isset($doctor_list_count[0]['d_count']))

			$count=$doctor_list_count[0]['d_count'];

		else

			$count=0;

		$pages=new CPagination($count);

		$criteria = new CDbCriteria(array(

			//'condition' => $condition,

        	//'order' => 'category_id DESC'

		));

	

		// results per page

		$pages->pageSize=$limit;

		$pages->applyLimit($criteria);

		////////////////////////

		//print_r($doctor_list);

		$sql_spl='select * FROM da_speciality WHERE status="1"';

		$connection = Yii::app()->db;

		$command = $connection->createCommand($sql_spl);

		$user_speciality = $command->queryAll();

		$user_speciality_res = array();

		foreach ($user_speciality as $key=>$val) {

			$user_speciality_res[$val['id']] = $val['speciality'];

		}

		

		$albha_arr =array(''=>"ALL",'A'=>"A",'B'=>"B",'C'=>"C",'D'=>"D",'E'=>"E",'F'=>"F",'G'=>"G",'H'=>"H",'I'=>"I",'J'=>"J",'K'=>"K",'L'=>"L",'M'=>"N",'N'=>"N",'O'=>"O",'P'=>"P",'Q'=>"Q",'R'=>"R",'S'=>"S",'T'=>"T",'U'=>"U",'V'=>"V",'W'=>"W",'X'=>"X",'Y'=>"Y",'Z'=>"Z");

		$this->render('doctor_search_by_procedure',array(

			'dataProvider'=>$usercriteria,'doctor_list'=>$doctor_list,'user_speciality' => $user_speciality_res,'albha_arr' => $albha_arr,'search_alpha' => $id,'pages'=>$pages,

		));

	}

	

	public function actionDoctorSearchByHospital($id = "")

	{

		$name= isset($_REQUEST['search_by_hospital'])?$_REQUEST['search_by_hospital']:'';

		

		$usercriteria = array();

		

		$sql_count="select count( DISTINCT d.id ) AS d_count FROM da_doctor as d ";

		$sql  = "select d.* FROM da_doctor as d ";

		

		$sql_count .= " WHERE d.status=1 and d.email_verified=1 ";

		$sql .= " WHERE d.status=1 and d.email_verified=1 ";

		

		//$input_name = preg_replace("/[^a-zA-Z]+/", "", $name);

		$input_name = trim($name);

		

		if($id != ""){

			$sql_count .= " and d.hospital_affiliations like '".$id."%' ";

			$sql .= " and d.hospital_affiliations like '".$id."%' ";

		}

		if($name != ""){

			$sql_count .= " and d.hospital_affiliations like '%".$input_name."%' ";

			$sql .= " and d.hospital_affiliations like '%".$input_name."%' ";

		}

		

		$sql_count .= "  ";

		$sql .= " GROUP BY d.id ";

		

		$limit = 30;

		$end_limit = 30;

		if(isset($_REQUEST['page']) && $_REQUEST['page']>1){

			$star_limit = ($limit*($_REQUEST['page']-1));

		}else{

			$star_limit = 0;

		}

		

		$sql .= ' limit '.$star_limit.','.$end_limit;

		$sql_count .= ' ';

		//echo $sql;

		$connection = Yii::app()->db;

		$command = $connection->createCommand($sql);

		$doctor_list = $command->queryAll();

		////////////

		$command_count = $connection->createCommand($sql_count);

		$doctor_list_count = $command_count->queryAll();

		

		//$count=count($doctor_list_count);

		if(isset($doctor_list_count[0]['d_count']))

			$count=$doctor_list_count[0]['d_count'];

		else

			$count=0;

		$pages=new CPagination($count);

		$criteria = new CDbCriteria(array(

			//'condition' => $condition,

        	//'order' => 'category_id DESC'

		));

	

		// results per page

		$pages->pageSize=$limit;

		$pages->applyLimit($criteria);

		////////////////////////

		//print_r($doctor_list);

		$sql_spl='select * FROM da_speciality WHERE status="1"';

		$connection = Yii::app()->db;

		$command = $connection->createCommand($sql_spl);

		$user_speciality = $command->queryAll();

		$user_speciality_res = array();

		foreach ($user_speciality as $key=>$val) {

			$user_speciality_res[$val['id']] = $val['speciality'];

		}

		

		$albha_arr =array(''=>"ALL",'A'=>"A",'B'=>"B",'C'=>"C",'D'=>"D",'E'=>"E",'F'=>"F",'G'=>"G",'H'=>"H",'I'=>"I",'J'=>"J",'K'=>"K",'L'=>"L",'M'=>"N",'N'=>"N",'O'=>"O",'P'=>"P",'Q'=>"Q",'R'=>"R",'S'=>"S",'T'=>"T",'U'=>"U",'V'=>"V",'W'=>"W",'X'=>"X",'Y'=>"Y",'Z'=>"Z");

		$this->render('doctor_search_by_hospital',array(

			'dataProvider'=>$usercriteria,'doctor_list'=>$doctor_list,'user_speciality' => $user_speciality_res,'albha_arr' => $albha_arr,'search_alpha' => $id,'pages'=>$pages,

		));

	}

	

	public function actionSearchDoctorProfile($slug = "")

	{		

			$addressId = 0;

			if( isset( $slug ) ) {

				$slugArr = explode( "|" ,$slug );

				$slug = $slugArr[0];

				$specialityID = $slugArr[1];

				if( isset( $slugArr[2] ) ) {

					$addressId = $slugArr[2];

				}	

			}

			$modelDoctorData = Doctor::model()->findByAttributes(array('slug' => $slug));

			if($modelDoctorData && $modelDoctorData->count() > 0){

				$id = $modelDoctorData->id;

				$doctor_list =Doctor::model()->find("id=\"$id\"");

				

				$sql='select * FROM da_speciality WHERE status="1"';

				$connection = Yii::app()->db;

				$command = $connection->createCommand($sql);

				$user_speciality = $command->queryAll();//print_r($user_speciality);

				$user_speciality_res = array();

				foreach ($user_speciality as $key=>$val) {

					$user_speciality_res[$val['id']] = $val['speciality'];

				}

				

				$sql_multy_speciality='select * FROM da_doctor_speciality WHERE status="1" and doctor_id="'.$id.'"';

				$command_multy_speciality = $connection->createCommand($sql_multy_speciality);

				$user_multy_speciality = $command_multy_speciality->queryAll();

				$user_selected_speciality = array();

				foreach ($user_multy_speciality as $key=>$val) {

					$user_selected_speciality[] = $val['speciality_id'];

				}

				

				/*$sql_speciality_procedure='select * FROM da_doctor_procedure WHERE status="1" and doctor_id="'.$id.'"';

				$command_speciality_procedure = $connection->createCommand($sql_speciality_procedure);

				$user_speciality_procedure = $command_speciality_procedure->queryAll();

				$user_procedure = array();

				foreach ($user_speciality_procedure as $key=>$val) {

					$user_procedure[] = $val['procedure_id'];

				}*/

				

				$sql_condition='select * FROM da_condition WHERE status="1"';

				$command_condition = $connection->createCommand($sql_condition);

				$user_condition = $command_condition->queryAll();

				$user_condition_res = array();

				foreach ($user_condition as $key=>$val) {

					$user_condition_res[$val['id']] = $val['condition'];

				}

				

				$sql_multy_condition='select * FROM da_doctor_condition WHERE status="1" and doctor_id="'.$id.'"';

				$command_multy_condition = $connection->createCommand($sql_multy_condition);

				$user_multy_condition = $command_multy_condition->queryAll();

				$user_selected_condition = array();

				foreach ($user_multy_condition as $key=>$val) {

					$user_selected_condition[] = $val['condition_id'];

				}

				

				$sql_procedure='select * FROM da_procedure WHERE status="1"';

				$command_procedure = $connection->createCommand($sql_procedure);

				$user_procedure = $command_procedure->queryAll();

				$user_procedure_res = array();

				foreach ($user_procedure as $key=>$val) {

					$user_procedure_res[$val['id']] = $val['procedure'];

				}

				

				$sql_multy_procedure='select * FROM da_doctor_procedure WHERE status="1" and doctor_id="'.$id.'"';

				$command_multy_procedure = $connection->createCommand($sql_multy_procedure);

				$user_multy_procedure = $command_multy_procedure->queryAll();

				$user_selected_procedure = array();

				foreach ($user_multy_procedure as $key=>$val) {

					$user_selected_procedure[] = $val['procedure_id'];

				}

				

				$sql_language='select * FROM da_language WHERE status="1"';

				$command_language = $connection->createCommand($sql_language);

				$user_language = $command_language->queryAll();

				$user_language_res = array();

				foreach ($user_language as $key=>$val) {

					$user_language_res[$val['id']] = $val['language'];

				}

				

				$sql_multy_language='select * FROM da_doctor_language WHERE status="1" and doctor_id="'.$id.'"';

				$command_multy_language = $connection->createCommand($sql_multy_language);

				$user_multy_language = $command_multy_language->queryAll();

				$user_selected_language = array();

				foreach ($user_multy_language as $key=>$val) {

					$user_selected_language[] = $val['language_id'];

				}

				if( $addressId != 0 ) {

					$sql_address='select * FROM da_doctor_address WHERE status="1" and id="'.$addressId.'"';

				} else {

					$sql_address='select * FROM da_doctor_address WHERE status="1" and doctor_id="'.$id.'"';

				}	

				$command_address = $connection->createCommand($sql_address);

				$user_address = $command_address->queryAll();

				$user_selected_address = array();

				foreach ($user_address as $key=>$val) {

					$user_selected_address[] = $val['address'];

				}

				

				$default_data_address = DoctorAddress::model()->findByAttributes(array('doctor_id'=>$id,'status'=>1,'default_status'=>1));

				//Helpers::pre($user_address);die;

				

				$doctor_review_condition = 't.status = 1 and doctor_id =' . $id;

				$doctor_review_criteria = new CDbCriteria(array(

					'condition' => $doctor_review_condition,

					'order' => 't.id DESC',

					'limit' => 5

				));

				$doctor_review=DoctorReview::model()->with('patient_details')->findall($doctor_review_criteria);

				//Helpers::pre($doctor_review);die;

				

				$condition_video = 'status = "1" and default_status = "1" and doctor_id =' . $id;

				$criteria_video = new CDbCriteria(array(

					'condition' => $condition_video,

					'order' => 'id DESC'

				));

				$data_video =DoctorVideo::model()->findAll($criteria_video);

				

				$sql_multy_insurance='select * FROM da_doctor_insurance WHERE status="1" and doctor_id="'.$id.'"';

				$command_multy_insurance = $connection->createCommand($sql_multy_insurance);

				$user_multy_insurance = $command_multy_insurance->queryAll();

				$user_selected_insurance = array();

				foreach ($user_multy_insurance as $key=>$val) {

					$user_selected_insurance[] = $val['insurance_id'];

				}

				$user_insurance_plan_res = array();

				$user_insurance_plan_res_ins = array();

				$user_selected_insurance_plan_ins = array();

				foreach ($user_selected_insurance as $insurance_key=>$insurance_val) {

					$sql_insurance_plan='select * FROM da_insurance_plan WHERE status="1" and insurance_id="'.$insurance_val.'"';

					$command_insurance_plan = $connection->createCommand($sql_insurance_plan);

					$user_insurance_plan = $command_insurance_plan->queryAll();

					$user_insurance_plan_res = array();

					foreach ($user_insurance_plan as $key=>$val) {

						$user_insurance_plan_res[$val['id']] = $val['plan'];

					}

					$user_insurance_plan_res_ins[$insurance_val] = $user_insurance_plan_res;

						

					$sql_multy_insurance_plan='select * FROM da_doctor_insurance_plan WHERE status="1" and doctor_id="'.$id.'" and insurance_id="'.$insurance_val.'"';

					$command_multy_insurance_plan = $connection->createCommand($sql_multy_insurance_plan);

					$user_multy_insurance_plan = $command_multy_insurance_plan->queryAll();

					$user_selected_insurance_plan = array();

					foreach ($user_multy_insurance_plan as $key=>$val) {

						$user_selected_insurance_plan[] = InsurancePlan::model()->getInsurancePlanNameById( $val['plan_id'] );

					}

					$user_selected_insurance_plan_ins[$insurance_val] = $user_selected_insurance_plan;

				}



			//Yii::app()->session['specialityChooseByUser'] = $page_fullUrl;

			$this->render('search_doctor_profile',array(

				'doctor_list' => $doctor_list,'user_speciality' => $user_speciality_res,'user_selected_speciality' => $user_selected_speciality,/*'user_procedure' => $user_procedure,*/'user_condition' => $user_condition_res,'user_selected_condition' => $user_selected_condition,'user_language' => $user_language_res,'user_selected_language' => $user_selected_language,'user_address' => $user_address,'doctor_review' => $doctor_review,'data_video' => $data_video,'user_procedure' => $user_procedure_res,'user_selected_procedure' => $user_selected_procedure,'default_data_address' => $default_data_address,'user_selected_insurance_plan_ins' => $user_selected_insurance_plan_ins,'specialityID' => $specialityID

			));

		} else {

			throw new CHttpException('404', 'Page not found');

		}

	}

	

	public function actionLanguageAjaxUpd()

	{		

		$language_id = $_REQUEST['language_id'];

		$language_arr = explode(",",$language_id);

		$hid_id = $_REQUEST['hid_id'];

		$connection = Yii::app()->db;

		$sql='UPDATE da_doctor_language SET status = :status WHERE doctor_id ='.$hid_id;

		$params = array(

			"status" => 0

		);

		$command=$connection->createCommand($sql);

		$command->execute($params);

		if($language_id != ""){

			$sql_lang='INSERT INTO da_doctor_language (doctor_id, language_id, status) VALUES(:doctor_id,:language_id,:status)';

			

			$command_lang=$connection->createCommand($sql_lang);

			foreach($language_arr as $lan_val){

				$params_lang = array( "doctor_id" => $hid_id, "language_id" => $lan_val, "status" => 1 );

				$command_lang->execute($params_lang);

			}

		}

	}

	

	public function actionLanguageAjaxRemove()

	{		

		$language_id = $_REQUEST['language_id'];

		//$language_arr = explode(",",$language_id);

		$hid_id = $_REQUEST['hid_id'];

		$connection = Yii::app()->db;

		$sql='UPDATE da_doctor_language SET status = :status WHERE language_id = '.$language_id.' and doctor_id ='.$hid_id;

		$params = array(

			"status" => 0

		);

		$command=$connection->createCommand($sql);

		$command->execute($params);

	}

	

	public function actionInsuranceAjaxUpd()

	{		

		$language_id = $_REQUEST['insurance_id'];

		$language_arr = explode(",",$language_id);

		$hid_id = $_REQUEST['hid_id'];

		$connection = Yii::app()->db;

		/*$sql='UPDATE da_doctor_insurance SET status = :status WHERE doctor_id ='.$hid_id;

		$params = array(

			"status" => 0

		);

		$command=$connection->createCommand($sql);

		$command->execute($params);*/

		if($language_id != ""){

			$sql_lang='INSERT INTO da_doctor_insurance (doctor_id, insurance_id, status) VALUES(:doctor_id,:insurance_id,:status)';

			

			$command_lang=$connection->createCommand($sql_lang);

			foreach($language_arr as $lan_val){

				$params_lang = array( "doctor_id" => $hid_id, "insurance_id" => $lan_val, "status" => 1 );

				$command_lang->execute($params_lang);

			}

		}

	}

	

	public function actionInsuranceAjaxRemove()

	{		

		$insurance_id = $_REQUEST['insurance_id'];

		//$language_arr = explode(",",$language_id);

		$hid_id = $_REQUEST['hid_id'];

		$connection = Yii::app()->db;

		$sql='UPDATE da_doctor_insurance SET status = :status WHERE insurance_id = '.$insurance_id.' and doctor_id ='.$hid_id;

		$params = array(

			"status" => 0

		);

		$command=$connection->createCommand($sql);

		$command->execute($params);

	}

	

	public function actionPlanAjaxUpd()

	{		

		$plan_id = $_REQUEST['plan_id'];

		$insurance_id = $_REQUEST['insurance_id'];

		$plan_arr = explode(",",$plan_id);

		$hid_id = $_REQUEST['hid_id'];

		$connection = Yii::app()->db;

		$sql='UPDATE da_doctor_insurance_plan SET status = :status WHERE insurance_id = '.$insurance_id.' and doctor_id ='.$hid_id;

		$params = array(

			"status" => 0

		);

		$command=$connection->createCommand($sql);

		$command->execute($params);

		if($plan_id != ""){

			$sql_plan='INSERT INTO da_doctor_insurance_plan (doctor_id, insurance_id, plan_id, status) VALUES(:doctor_id,:insurance_id,:plan_id,:status)';

			

			$command_plan=$connection->createCommand($sql_plan);

			foreach($plan_arr as $plan_val){

				$params_plan = array( "doctor_id" => $hid_id, "insurance_id" => $insurance_id, "plan_id" => $plan_val, "status" => 1 );

				$command_plan->execute($params_plan);

			}

		}

	}

	

	public function actionPlanAjaxRemove()

	{		

		$plan_id = $_REQUEST['plan_id'];

		//$language_arr = explode(",",$language_id);

		$hid_id = $_REQUEST['hid_id'];

		$connection = Yii::app()->db;

		$sql='UPDATE da_doctor_insurance_plan SET status = :status WHERE plan_id = '.$plan_id.' and doctor_id ='.$hid_id;

		$params = array(

			"status" => 0

		);

		$command=$connection->createCommand($sql);

		$command->execute($params);

	}

	

	public function actionSpecialityAjaxUpd()

	{		

		$speciality_id = $_REQUEST['speciality_id'];

		$speciality_arr = explode(",",$speciality_id);

		$hid_id = $_REQUEST['hid_id'];

		$connection = Yii::app()->db;

		/*$sql='UPDATE da_doctor_speciality SET status = :status WHERE doctor_id ='.$hid_id;

		$params = array(

			"status" => 0

		);

		$command=$connection->createCommand($sql);

		$command->execute($params);*/

		if($speciality_id != ""){

			$sql_speciality='INSERT INTO da_doctor_speciality (doctor_id, speciality_id, status) VALUES(:doctor_id,:speciality_id,:status)';

			

			$command_speciality=$connection->createCommand($sql_speciality);

			foreach($speciality_arr as $speciality_val){

				$params_speciality = array( "doctor_id" => $hid_id, "speciality_id" => $speciality_val, "status" => 1 );

				$command_speciality->execute($params_speciality);

			}

		}

	}

	

	public function actionConditionFind()

	{		

		$speciality_id = $_REQUEST['id'];

		$connection = Yii::app()->db;

		$sql='select * FROM da_condition WHERE status="1" and speciality_id='.$speciality_id;

		$command=$connection->createCommand($sql);

		$condition_arr = $command->queryAll();

		$str = "";

		$str_procedure = "";

		if(!empty($condition_arr)){

			foreach($condition_arr as $val){

				$str .= '<p><input type="checkbox" name="user_selected_condition[]" value="'.$val['id'].'" checked="checked" class="condition_class"  onclick="chooseSCP(\''.'condition'.'\',\''.$val['id'].'\')"/>'.$val['condition'].'</p>';

				

				$sql_procedure='select * FROM da_procedure WHERE status="1" and condition_id='.$val['id'];

				$command_procedure=$connection->createCommand($sql_procedure);

				$condition_procedure_arr = $command_procedure->queryAll();

				if(!empty($condition_procedure_arr)){

				foreach($condition_procedure_arr as $procedure_val){

					$str_procedure .= '<p><input type="checkbox" name="user_selected_procedure[]" value="'.$procedure_val['id'].'" checked="checked" class="procedure_class"/>'.$procedure_val['procedure'].'</p>';

					}

				}

			}

		}

		echo $str.'***###***'.$str_procedure;

		//Helpers::pre($condition_arr);

	}

	

	public function actionProcedureFind()

	{		

		$condition_id = $_REQUEST['id'];

		$condition_arr = explode(",",$condition_id);

		//$condition_id = $_REQUEST['id'];

		$connection = Yii::app()->db;

		$str_procedure = "";

		if($condition_id != ""){

			foreach($condition_arr as $val){

				$sql_procedure='select * FROM da_procedure WHERE status="1" and condition_id='.$val;

				$command_procedure=$connection->createCommand($sql_procedure);

				$condition_procedure_arr = $command_procedure->queryAll();

				if(!empty($condition_procedure_arr)){

				foreach($condition_procedure_arr as $procedure_val){

					$str_procedure .= '<p><input type="checkbox" name="user_selected_procedure[]" value="'.$procedure_val['id'].'" checked="checked" class="procedure_class"/>'.$procedure_val['procedure'].'</p>';

					}

				}

			}

		}

		echo $str_procedure;

		//Helpers::pre($condition_arr);

	}

	

	public function actionSpecialityAjaxRemove()

	{		

		$speciality_id = $_REQUEST['speciality_id'];

		//$speciality_arr = explode(",",$speciality_id);

		$hid_id = $_REQUEST['hid_id'];

		$connection = Yii::app()->db;

		$sql='UPDATE da_doctor_speciality SET status = :status WHERE speciality_id = '.$speciality_id.' and doctor_id ='.$hid_id;

		$params = array(

			"status" => 0

		);

		$command=$connection->createCommand($sql);

		$command->execute($params);

		

		//$sql_condition_find='select id from da_doctor_condition WHERE status = 1 and  doctor_id ='.$hid_id.' and condition_id IN (select id from da_condition where speciality_id = '.$speciality_id.')';

		$sql_condition_find='select id from da_condition where speciality_id = '.$speciality_id;

		$command_condition_find=$connection->createCommand($sql_condition_find);

		$condition_find_arr = $command_condition_find->queryAll();

		$user_condition_find_arr = array();

		foreach ($condition_find_arr as $condition_find_arrval) {

			$user_condition_find_arr[] = $condition_find_arrval['id'];

		}

		

		$sql_condition = 'UPDATE da_doctor_condition SET status = :status WHERE  doctor_id ='.$hid_id.' and condition_id IN (select id from da_condition where speciality_id = '.$speciality_id.')';

		$command_condition=$connection->createCommand($sql_condition);

		$command_condition->execute($params);

		

		//$sql_procedure_find='select id from da_doctor_procedure WHERE status = 1 and doctor_id ='.$hid_id.' and procedure_id IN (select id from da_procedure where condition_id IN  (select id from da_condition where speciality_id = '.$speciality_id.'))';

		$sql_procedure_find='select id from da_procedure where condition_id IN  (select id from da_condition where speciality_id = '.$speciality_id.')';

		$command_procedure_find=$connection->createCommand($sql_procedure_find);

		$procedure_find_arr = $command_procedure_find->queryAll();

		$user_procedure_find_arr = array();

		foreach ($procedure_find_arr as $user_procedure_find_arr_val) {

			$user_procedure_find_arr[] = $user_procedure_find_arr_val['id'];

		}

		

		$sql_procedure = 'UPDATE da_doctor_procedure SET status = :status WHERE  doctor_id ='.$hid_id.' and procedure_id IN (select id from da_procedure where condition_id IN  (select id from da_condition where speciality_id = '.$speciality_id.'))';

		

		$command_procedure=$connection->createCommand($sql_procedure);

		$command_procedure->execute($params);

		echo implode(',',$user_condition_find_arr).'***###***'.implode(',',$user_procedure_find_arr);

	}

	

	public function actionConditionAjaxUpd()

	{		

		$condition_id = $_REQUEST['condition_id'];

		$condition_arr = explode(",",$condition_id);

		$hid_id = $_REQUEST['hid_id'];

		$connection = Yii::app()->db;

		/*$sql='UPDATE da_doctor_condition SET status = :status WHERE doctor_id ='.$hid_id;

		$params = array(

			"status" => 0

		);

		$command=$connection->createCommand($sql);

		$command->execute($params);*/

		if($condition_id != ""){

			$sql_condition='INSERT INTO da_doctor_condition (doctor_id, condition_id, status) VALUES(:doctor_id,:condition_id,:status)';

			

			$command_condition=$connection->createCommand($sql_condition);

			foreach($condition_arr as $condition_val){

				$params_condition = array( "doctor_id" => $hid_id, "condition_id" => $condition_val, "status" => 1 );

				$command_condition->execute($params_condition);

			}

		}

	}

	

	public function actionConditionAjaxRemove()

	{		

		$condition_id = $_REQUEST['condition_id'];

		//$condition_arr = explode(",",$condition_id);

		$hid_id = $_REQUEST['hid_id'];

		$connection = Yii::app()->db;

		$sql='UPDATE da_doctor_condition SET status = :status WHERE condition_id = '.$condition_id.' and doctor_id ='.$hid_id;

		$params = array(

			"status" => 0

		);

		$command=$connection->createCommand($sql);

		$command->execute($params);

		

		$sql_procedure_find='select id from da_procedure where condition_id = '.$condition_id;

		$command_procedure_find=$connection->createCommand($sql_procedure_find);

		$procedure_find_arr = $command_procedure_find->queryAll();

		$user_procedure_find_arr = array();

		foreach ($procedure_find_arr as $user_procedure_find_arr_val) {

			$user_procedure_find_arr[] = $user_procedure_find_arr_val['id'];

		}

		

		$sql_procedure = 'UPDATE da_doctor_procedure SET status = :status WHERE  doctor_id ='.$hid_id.' and procedure_id IN (select id from da_procedure where condition_id = '.$condition_id.')';

		

		$command_procedure=$connection->createCommand($sql_procedure);

		$command_procedure->execute($params);

		

		echo implode(',',$user_procedure_find_arr);

	}

	

	public function actionProcedureAjaxUpd()

	{		

		$procedure_id = $_REQUEST['procedure_id'];

		$procedure_arr = explode(",",$procedure_id);

		$hid_id = $_REQUEST['hid_id'];

		$connection = Yii::app()->db;

		/*$sql='UPDATE da_doctor_procedure SET status = :status WHERE doctor_id ='.$hid_id;

		$params = array(

			"status" => 0

		);

		$command=$connection->createCommand($sql);

		$command->execute($params);*/

		if($procedure_id != ""){

			$sql_procedure='INSERT INTO da_doctor_procedure (doctor_id, procedure_id, status) VALUES(:doctor_id,:procedure_id,:status)';

			

			$command_procedure=$connection->createCommand($sql_procedure);

			foreach($procedure_arr as $procedure_val){

				$params_procedure = array( "doctor_id" => $hid_id, "procedure_id" => $procedure_val, "status" => 1 );

				$command_procedure->execute($params_procedure);

			}

		}

	}

	

	public function actionProcedureAjaxRemove()

	{		

		$procedure_id = $_REQUEST['procedure_id'];

		//$procedure_arr = explode(",",$procedure_id);

		$hid_id = $_REQUEST['hid_id'];

		$connection = Yii::app()->db;

		$sql='UPDATE da_doctor_procedure SET status = :status WHERE procedure_id = '.$procedure_id.' and doctor_id ='.$hid_id;

		$params = array(

			"status" => 0

		);

		$command=$connection->createCommand($sql);

		$command->execute($params);

	}

	

	public function actionPrivacy()

	{

		$this->render('privacy',array(

			//'model'=>$model,

		));

	}

	

	public function actionDoctorBookStep1()

	{

		if(isset($_SESSION['logged_user_type']))

		{

			if($_SESSION['logged_user_type']=='doctor')

			{

				$this->redirect(array('doctor/index'));

			}

		}

		//echo $_REQUEST['doctor_id'];

		unset(Yii::app()->session['app_complete']);

		unset(Yii::app()->session['speciality_id']);

		if(!isset($_REQUEST['doctor_id']) || !isset($_REQUEST['start_time']) || !isset($_REQUEST['time_slot']) || !isset($_REQUEST['address_id']) || !isset($_REQUEST['speciality_id']) || !isset($_REQUEST['procedure_id']))

			$this->redirect(array('site/index'));

		

		Yii::app()->session['session_step1'] = true;

		Yii::app()->session['doctor_id'] = $_REQUEST['doctor_id'];

		$id = Yii::app()->session['doctor_id'];

		Yii::app()->session['start_time'] = $_REQUEST['start_time'];

		$start_time=Yii::app()->session['start_time'];

		Yii::app()->session['time_slot'] = $_REQUEST['time_slot'];

		$time_slot=Yii::app()->session['time_slot'];

		Yii::app()->session['address_id'] = $_REQUEST['address_id'];

		$address_id=Yii::app()->session['address_id'];

		Yii::app()->session['speciality_id'] = $_REQUEST['speciality_id'];

		$speciality_id=Yii::app()->session['speciality_id'];

		Yii::app()->session['procedure_id'] = $_REQUEST['procedure_id'];

		$procedure_id=Yii::app()->session['procedure_id'];

		

		$usercriteria =Doctor::model()->find("id=\"$id\"");

		

		$connection = Yii::app()->db;

		$sql='select * FROM da_speciality WHERE status="1"';

		$command = $connection->createCommand($sql);

		$user_speciality = $command->queryAll();

		$user_speciality_res = array();

		foreach ($user_speciality as $key=>$val) {

			$user_speciality_res[$val['id']] = $val['speciality'];

		}

		

		$sql='select * FROM da_procedure WHERE status="1"';

		$command = $connection->createCommand($sql);

		$user_procedure = $command->queryAll();

		$user_procedure_res = array();

		foreach ($user_procedure as $key=>$val) {

			$user_procedure_res[$val['id']] = $val['procedure'];

		}

		

		$sql_reason_for_visit='select * FROM da_reason_for_visit WHERE status="1" and speciality_id='.Yii::app()->session['speciality_id'];

		$command_reason_for_visit = $connection->createCommand($sql_reason_for_visit);

		$user_reason_for_visit = $command_reason_for_visit->queryAll();

		$user_reason_for_visit_res = array();

		foreach ($user_reason_for_visit as $key=>$val) {

			$user_reason_for_visit_res[$val['id']] = $val['reason_for_visit'];

		}

		

		$sql_multy_procedure='select * FROM da_doctor_procedure WHERE status="1" and doctor_id="'.$id.'"';

		$command_multy_procedure = $connection->createCommand($sql_multy_procedure);

		$user_multy_procedure = $command_multy_procedure->queryAll();

		$user_selected_procedure = array();

		foreach ($user_multy_procedure as $key=>$val) {

			$user_selected_procedure[] = $val['procedure_id'];

		}

		

		$sql_insurance='select * FROM da_insurance WHERE status="1"';

		$command_insurance = $connection->createCommand($sql_insurance);

		$user_insurance = $command_insurance->queryAll();

		$user_insurance_res = array();

		foreach ($user_insurance as $key=>$val) {

			$user_insurance_res[$val['id']] = $val['insurance'];

		}

		

		$sql_multy_insurance='select * FROM da_doctor_insurance WHERE status="1" and doctor_id="'.$id.'"';

		$command_multy_insurance = $connection->createCommand($sql_multy_insurance);

		$user_multy_insurance = $command_multy_insurance->queryAll();

		$user_selected_insurance = array();

		foreach ($user_multy_insurance as $key=>$val) {

			$user_selected_insurance[] = $val['insurance_id'];

		}

		//===============dilip==========================================

		$patient_last_book_time = '';



		if(isset($_SESSION['logged_user_id']))

		{

			$patient_id_d = $_SESSION['logged_user_id'];

			

			$sql_patient_book="SELECT * FROM  `da_doctor_book` WHERE `patient_id` = '".$patient_id_d."' ORDER BY `id` DESC LIMIT 0 , 1";

			$command_patient_book = $connection->createCommand($sql_patient_book);

			$user_speciality_patient_book = $command_patient_book->queryAll();

			//print_r($user_speciality_patient_book);

			if($user_speciality_patient_book)

			{

				$patient_last_book_time = date('jS \of F Y h:i:s A', $user_speciality_patient_book[0]['book_time']);

			}

		}

	

		//==============================================================

		//$default_data_address = DoctorAddress::model()->findByAttributes(array('doctor_id'=>$model->id,'status'=>1,'default_status'=>1));

		$default_data_address = DoctorAddress::model()->find("id=".$address_id);

		

		$this->render('doctor_book_step1',array(

			'dataProvider'=>$usercriteria,'user_speciality' => $user_speciality_res,'user_procedure' => $user_procedure_res,'user_selected_procedure' => $user_selected_procedure,'default_data_address' => $default_data_address,'procedure_id' => $procedure_id,'user_insurance' => $user_insurance_res,'user_selected_insurance' => $user_selected_insurance,'user_reason_for_visit' => $user_reason_for_visit_res, 'patient_last_book_time'=>$patient_last_book_time));

	}

	

	public function actionDoctorBookStep2()

	{

		

		//Helpers::pre($_REQUEST);

		//print_r($_SESSION);

		//die;

		Yii::app()->session['session_step2'] = true;

		$reason_for_visit=0;

		if(!isset($_REQUEST['reason_for_visit']))

		{

			$reason_for_visit="";

		}  else {

			$reason_for_visit=$_REQUEST['reason_for_visit'];

		}



		Yii::app()->session['reason_for_visit'] = $reason_for_visit;



		//if(!isset($_REQUEST['entry_type']) || !(Yii::app()->session['doctor_id']))

			//$this->redirect(array('site/index'));

		if(!Yii::app()->session['session_step1'])

		{

			$this->redirect(array('site/index'));

		}

			

		

		if(Yii::app()->session['logged_in'] && Yii::app()->session['logged_user_type'] == 'patient'){

			$doctor_id_temp = Yii::app()->session['doctor_id'];

			$doctor_det_temp =Doctor::model()->find("id=\"$doctor_id_temp\"");

		

			$user_name = Yii::app()->session['logged_user_email'];

			$usercriteria =Patient::model()->find("user_name=\"$user_name\"");

			$app_time = date('l, F j - h:i A',Yii::app()->session['start_time']);

			$state_d_name = '';

			$state_det_temp_d = State::model()->find("short_code=\"$doctor_det_temp->state\"");

			if( isset( $state_det_temp_d->name ) && (count ($state_det_temp_d) > 0) ) {

				$state_d_name = $state_det_temp_d->name;

			}

			$app_id = 0;

			if( isset( Yii::app()->session['address_id'] ) ) {

				$app_id = Yii::app()->session['address_id'];

			}

			$address_det_temp_p = DoctorAddress::model()->find("id=\"$app_id\"");



			$temp_id = 15;

			$email_template = EmailTemplate::model()->find("id=\"$temp_id\"");

			if(!empty($email_template) && $email_template->tempalte_body != ''){

				$message_body = $email_template->tempalte_body;

				//$message_body = $this->getTemplateDataParsing($parse_with,$parse_data,$message_body);

				

					$parse_data_arr = array();

					$parse_data_arr['{{appointment.actual_format}}'] = date('l, F j - h:i A',Yii::app()->session['start_time']);

					$parse_data_arr['{{appointment.day}}'] = date('l',Yii::app()->session['start_time']);

					$parse_data_arr['{{appointment.month}}'] = date('F',Yii::app()->session['start_time']);

					$parse_data_arr['{{appointment.date}}'] = date('j',Yii::app()->session['start_time']);

					$parse_data_arr['{{appointment.time}}'] = date('h:i A',Yii::app()->session['start_time']);

					$parse_data_arr['{{appointment.address_1}}'] = isset ( $address_det_temp_p->address ) ? $address_det_temp_p->address : $doctor_det_temp->addr1;

					$parse_data_arr['{{appointment.city}}'] = isset ( $address_det_temp_p->city ) ? $address_det_temp_p->city : $doctor_det_temp->city ;

					$parse_data_arr['{{appointment.state}}'] = isset( $address_det_temp_p->state ) ?  $address_det_temp_p->state : $state_d_name;			

					$parse_data_arr['{{appointment.zip_code}}'] = isset ( $address_det_temp_p->zip ) ? $address_det_temp_p->zip : $doctor_det_temp->zip;

					

					$parse_data_arr['{{patient.user_full_name}}'] = $usercriteria->user_first_name." ".$usercriteria->user_last_name;

					$parse_data_arr['{{patient.user_full_name}}'] = $usercriteria->user_first_name." ".$usercriteria->user_last_name;

					$parse_data_arr['{{patient.user_first_name}}'] = $usercriteria->user_first_name;

					$parse_data_arr['{{patient.user_last_name}}'] = $usercriteria->user_last_name;

					

					$parse_data_arr['{{patient.user_title}}'] = $usercriteria->patient_name;

					$parse_data_arr['{{patient.user_mobile_number}}'] = isset( $usercriteria->user_phone ) ? $usercriteria->user_phone :  '';

					$parse_data_arr['{{patient.user_home_number}}'] = isset( $usercriteria->user_phone ) ? $usercriteria->user_phone :  '';

					$parse_data_arr['{{patient.user_work_number}}'] = isset( $usercriteria->user_phone ) ? $usercriteria->user_phone :  '';

					$parse_data_arr['{{patient.user_fax_number}}'] = isset( $usercriteria->user_phone ) ? $usercriteria->user_phone :  '';

					$parse_data_arr['{{patient.user_other_number}}'] = isset( $usercriteria->user_phone ) ? $usercriteria->user_phone :  '';

					$parse_data_arr['{{patient.user_email}}'] = $usercriteria->patient_email;

					$parse_data_arr['{{patient.user_address_1}}'] = isset( $usercriteria->user_address ) ? $usercriteria->user_address :  '';

					$parse_data_arr['{{patient.user_address_2}}'] = isset( $usercriteria->user_address ) ? $usercriteria->user_address : '';

					$parse_data_arr['{{patient.user_address_3}}'] = isset( $usercriteria->user_address ) ? $usercriteria->user_address :  '';

					$parse_data_arr['{{patient.user_city}}'] = $usercriteria->user_city;

					$parse_data_arr['{{patient.user_post_code}}'] = $usercriteria->user_zip;

					$parse_data_arr['{{patient.user_state}}'] = $usercriteria->user_state;

					$parse_data_arr['{{patient.user_country}}'] = isset( $usercriteria->patient_country ) ? $usercriteria->patient_country :  '';

					$parse_data_arr['{{patient.user_dateOfBirth}}'] = isset( $usercriteria->patient_dob ) ? date('d-m-Y' ,strtotime( $usercriteria->patient_dob ) ) : '';

					$parse_data_arr['{{patient.user_gender}}'] = Ucfirst( $usercriteria->patient_sex );

					

					$parse_data_arr['{firstname}'] = $usercriteria->user_first_name;

					

					$parse_data_arr['{user}'] = $doctor_det_temp->first_name." ".$doctor_det_temp->last_name;

					$parse_data_arr['{{doctor.full_name}}'] = $doctor_det_temp->full_name;

					$parse_data_arr['{{doctor.first_name}}'] = $doctor_det_temp->first_name;

					$parse_data_arr['{{doctor.last_name}}'] = $doctor_det_temp->last_name;

					$parse_data_arr['{{doctor.address_1}}'] = $doctor_det_temp->addr1;

					$parse_data_arr['{{doctor.city}}'] = $doctor_det_temp->city;

					$parse_data_arr['{{doctor.state}}'] = isset( $state_det_temp_d->name ) ? $state_det_temp_d->name : '';

					$parse_data_arr['{{doctor.zip_code}}'] = $doctor_det_temp->zip;

					$parse_data_arr['{{doctor.phone}}'] = $doctor_det_temp->phone;

					$parse_data_arr['{{doctor.email}}'] = $doctor_det_temp->email;

					$parse_data_arr['{{doctor.parctise_name}}'] = $doctor_det_temp->practice;



				$message_body = $this->getTemplateDataParsing($parse_data_arr,$message_body);

			}else{

				$message_body = '<h2 style="color:#636363; margin:0px; padding-bottom:10px; padding-left:8px; padding-right:8px; font-size:13px; font-weight:bold;">Your Appointment has now been Booked.</h2>Your Appointment Time is '.$app_time.'.';

			}

			

			$to_email = $usercriteria->user_email;

			$to_name = $usercriteria->user_first_name." ".$usercriteria->user_last_name;

			$to = array($to_email,$to_name);

			$from = array('support@edoctorbook.com','eDoctorBook');

			//$subject = 'eDoctorBook - Appointment Book.';

			$subject = isset( $email_template->subject ) ? $email_template->subject : 'eDoctorBook - Appointment Book.';

			$message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

					<html xmlns="http://www.w3.org/1999/xhtml">

					<head>

					<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

					<title>Doctor Email Template</title>

					</head>

					<body style="padding:0px; margin:0px; font-size:12px; color:#545454; line-height:18px; font-family:Arial, Helvetica, sans-serif;">

					<div style="width:450px;  margin:5px auto; border:1px solid #c7c7c7; "> 

					<div style="text-align:left; margin-bottom:15px; -moz-box-shadow:0px 1px 1px 1px #E4E4E4; -webkit-box-shadow:0px 1px 1px 1px #E4E4E4;box-shadow:0px 1px 1px 1px #E4E4E4; background:#54cbc8; font-weight:bold;">

					<a href="'.$this->createAbsoluteUrl('site/index/').'"><img src="http://'.$_SERVER['HTTP_HOST'].Yii::app()->getHomeUrl().'/assets/images/small_logo.png" style="padding:3px;" alt="Doctor Appointment Book" /></a>

					&nbsp;

					</div>	



					<h1 style="text-align:center; display:block; font-weight:normal; margin:0px; padding-bottom:12px; font-size:19px; line-height:26px; color:#4c4c4c;">Thank you '.$to_name.' for using <br /> <font style="color:#6ad2cf">eDoctorBook</font>!</h1>



					<div style="min-height:200px; background:#fff; color:#bbb; margin:10px; padding-top:15px; font-size:12px;"><pre style="color:#000">'.$message_body.'</pre>

					<br>

					

					</div>

					<p style=" margin-top:10px; padding:8px 5px; color:#000; font-size:13px; margin-bottom:0px; font-weight:bold; line-height:18px; ">eDoctorBook Team</p>

					<a style="padding-left:5px; padding-right:5px; color:#000; font-size:13px; font-weight:bold; margin:0px; line-height:0px; text-decoration:none" href="">http://www.eDoctorBook.com</a>

					<div style="background:#54cbc8; padding-top:20px; margin-top:15px; padding-bottom:10px; text-align:center; color:#e2e2e2; font-size:14px;"></div>

					</div>



					</body>

					</html>';

				

			Helpers::mailsend($to,$from,$subject,$message);

			Yii::app()->session['session_step3'] = true;

			$this->redirect(array('doctor/doctorBookStep4'));

		}

		Yii::app()->session['entry_type'] = $_REQUEST['entry_type'];

		Yii::app()->session['insurance'] = $_REQUEST['insurance'];

		Yii::app()->session['visitor'] = $_REQUEST['visitor'];

		Yii::app()->session['patient_name'] = $_REQUEST['patient_name'];

		Yii::app()->session['patient_email'] = $_REQUEST['patient_email'];

		/*Yii::app()->session['patient_month'] = $_REQUEST['patient_month'];

		Yii::app()->session['patient_date'] = $_REQUEST['patient_date'];

		Yii::app()->session['patient_year'] = $_REQUEST['patient_year'];*/

		if($_REQUEST['patient_dob']){

			$patient_dob_arr = explode('-',$_REQUEST['patient_dob']);

			Yii::app()->session['patient_month'] = $patient_dob_arr[1];

			Yii::app()->session['patient_date'] = $patient_dob_arr[2];

			Yii::app()->session['patient_year'] = $patient_dob_arr[0];

		}

		Yii::app()->session['patient_sex'] = $_REQUEST['patient_sex'];

		

		$id = Yii::app()->session['doctor_id'];

		$start_time=Yii::app()->session['start_time'];

		$time_slot=Yii::app()->session['time_slot'];

		$address_id=Yii::app()->session['address_id'];

		$speciality_id=Yii::app()->session['speciality_id'];

		/*$procedure_id=$_REQUEST['procedure_id'];

		Yii::app()->session['procedure_id'] = $_REQUEST['procedure_id'];*/

		$procedure_id=Yii::app()->session['procedure_id'];

		$insurance=$_REQUEST['insurance'];

		Yii::app()->session['insurance'] = $_REQUEST['insurance'];

		

		$usercriteria =Doctor::model()->find("id=\"$id\"");

		

		$connection = Yii::app()->db;

		$sql='select * FROM da_speciality WHERE status="1"';

		$command = $connection->createCommand($sql);

		$user_speciality = $command->queryAll();

		$user_speciality_res = array();

		foreach ($user_speciality as $key=>$val) {

			$user_speciality_res[$val['id']] = $val['speciality'];

		}

		

		$sql='select * FROM da_procedure WHERE status="1"';

		$command = $connection->createCommand($sql);

		$user_procedure = $command->queryAll();

		$user_procedure_res = array();

		foreach ($user_procedure as $key=>$val) {

			$user_procedure_res[$val['id']] = $val['procedure'];

		}

		

		$sql_insurance='select * FROM da_insurance WHERE status="1"';

		$command_insurance = $connection->createCommand($sql_insurance);

		$user_insurance = $command_insurance->queryAll();

		$user_insurance_res = array();

		foreach ($user_insurance as $key=>$val) {

			$user_insurance_res[$val['id']] = $val['insurance'];

		}

		

		$sql_multy_insurance='select * FROM da_doctor_insurance WHERE status="1" and doctor_id="'.$id.'"';

		$command_multy_insurance = $connection->createCommand($sql_multy_insurance);

		$user_multy_insurance = $command_multy_insurance->queryAll();

		$user_selected_insurance = array();

		foreach ($user_multy_insurance as $key=>$val) {

			$user_selected_insurance[] = $val['insurance_id'];

		}

		

		$default_data_address = DoctorAddress::model()->find("id=".$address_id);

		

		$this->render('doctor_book_step2',array(

			'dataProvider'=>$usercriteria,'user_speciality' => $user_speciality_res,'user_procedure' => $user_procedure_res,'default_data_address' => $default_data_address,'procedure_id' => $procedure_id,'user_insurance' => $user_insurance_res,'user_selected_insurance' => $user_selected_insurance

		));

	}

	

	public function actionPatientCaptcha()

	{

		$captcha=Yii::app()->getController()->createAction("captcha");

		$code = $captcha->verifyCode;

		$captcha = $_REQUEST['captcha'];

		if($code === $_REQUEST['captcha']){ echo true;Yii::app()->session['session_step2'] = true; }

		else	echo false;

	}

	

	public function actionContact()

	{

		$model = new Contact();

		if(Yii::app()->getRequest()->isPostRequest){

			$model->scenario = 'contact';

			$model->attributes=Yii::app()->getRequest()->getPost('Contact');

			$model->cover_letter = $_POST['Contact']['cover_letter'];

			$model->status = 1;

			$model->type = 'contact';

			//$model->resume = 'not.pdf';

			$model->date_created = date('Y-m-d h:i:s');

			$model->date_modified = date('Y-m-d h:i:s');

			

			if($model->save()){

				$this->removeCaptchaSession();

				

				$to_email = 'vijayfll@yahoo.com';

				//$to_email = 'dilip.arb@gmail.com';

				$to_name = 'eDoctorBook';

				$to = array($to_email,$to_name);

				$from = array($model->email,$model->name);

				$subject = 'eDoctorBook - Contact.';

				//$attachment = $resume_path.$fileName;

				$message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

					<html xmlns="http://www.w3.org/1999/xhtml">

					<head>

					<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

					<title>Doctor Email Template</title>

					</head>

					<body style="padding:0px; margin:0px; font-size:12px; color:#545454; line-height:18px; font-family:Arial, Helvetica, sans-serif;">

					<div style="width:450px;  margin:5px auto; border:1px solid #c7c7c7; "> 

					<div style="text-align:left; margin-bottom:15px; -moz-box-shadow:0px 1px 1px 1px #E4E4E4; -webkit-box-shadow:0px 1px 1px 1px #E4E4E4;box-shadow:0px 1px 1px 1px #E4E4E4; background:#54cbc8; font-weight:bold;">

					<a href="'.$this->createAbsoluteUrl('site/index/').'">

					<img src="http://'.$_SERVER['HTTP_HOST'].Yii::app()->getHomeUrl().'/assets/images/small_logo.png" style="padding:3px;" alt="Doctor Appointment Book" /></a>

					&nbsp;

					</div>	



					<div style="min-height:200px; background:#fff; color:#bbb; margin:10px; padding-top:15px; font-size:12px;">

					<strong>Name</strong> : '.$model->name.'<br>

					<strong>Email</strong> : '.$model->email.'<br>

					<strong>Phone</strong> : '.$model->phone.'<br>

					</p>

					<h2 style="color:#636363; margin:0px; padding-bottom:10px; padding-left:8px; padding-right:8px; font-size:13px; font-weight:bold;">Comments</h2>

					<p style="font-size:12px; padding-top:25px; padding-left:8px; padding-right:8px; color:#4c4c4c; display:block;">

					'.$model->cover_letter.'

					</p>

					

					

					</div>

					<p style=" margin-top:10px; padding:8px 5px; color:#000; font-size:13px; margin-bottom:0px; font-weight:bold; line-height:18px; ">eDoctorBook Team</p>

					<a style="padding-left:5px; padding-right:5px; color:#000; font-size:13px; font-weight:bold; margin:0px; line-height:0px; text-decoration:none" href="">http://www.eDoctorBook.com</a>

					<div style="background:#54cbc8; padding-top:20px; margin-top:15px; padding-bottom:10px; text-align:center; color:#e2e2e2; font-size:14px;"></div>

					</div>



					</body>

					</html>';

					

				Helpers::mailsend($to,$from,$subject,$message);

				

				//$this->refresh();

				$this->redirect(array('doctor/contactThanks', 'type'=>'contact'));

			}

			

		}

		

		$this->render('contact',array(

			'model'=>$model,

		));

	}

	

	public function actionSubmitResume()

	{

		$model = new Contact();

		if(Yii::app()->getRequest()->isPostRequest){

			$model->scenario = 'resume';

			$model->attributes=Yii::app()->getRequest()->getPost('Contact');

			$model->cover_letter = $_POST['Contact']['cover_letter'];

			$model->status = 1;

			$model->date_created = date('Y-m-d h:i:s');

			$model->date_modified = date('Y-m-d h:i:s');

			

			$uniqid = uniqid();  // generate random number between 0-9999

			$uploadedFile=CUploadedFile::getInstance($model,'resume');

			$fileName = "{$uniqid}-{$uploadedFile}";  // random number + file name

			if(!empty($uploadedFile))

				$model->resume = $fileName;

					

			if($model->save()){

				$this->removeCaptchaSession();

				

				$resume_path = Yii::app()->basePath.'/../assets/upload/resume/';

				if(!empty($uploadedFile)){

					//@mkdir($resume_path, 0777, true);

					$uploadedFile->saveAs($resume_path.$fileName);  // image will uplode to rootDirectory/banner/

				}

				

				$to_email = 'vijayfll@yahoo.com';

				//$to_email = 'dilip.arb@gmail.com';

				$to_name = 'eDoctorBook';

				$to = array($to_email,$to_name);

				$from = array($model->email,$model->name);

				$subject = 'eDoctorBook - Submit Resume.';

				$attachment = $resume_path.$fileName;

				$message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

					<html xmlns="http://www.w3.org/1999/xhtml">

					<head>

					<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

					<title>Doctor Email Template</title>

					</head>

					<body style="padding:0px; margin:0px; font-size:12px; color:#545454; line-height:18px; font-family:Arial, Helvetica, sans-serif;">

					<div style="width:450px;  margin:5px auto; border:1px solid #c7c7c7; "> 

					<div style="text-align:left; margin-bottom:15px; -moz-box-shadow:0px 1px 1px 1px #E4E4E4; -webkit-box-shadow:0px 1px 1px 1px #E4E4E4;box-shadow:0px 1px 1px 1px #E4E4E4; background:#54cbc8; font-weight:bold;">

					<a href="'.$this->createAbsoluteUrl('site/index/').'"><img src="http://'.$_SERVER['HTTP_HOST'].Yii::app()->getHomeUrl().'/assets/images/small_logo.png" style="padding:3px;" alt="Doctor Appointment Book" /></a>

					&nbsp;

					</div>	

					<div style="min-height:200px; background:#fff; color:#bbb; margin:10px; padding-top:15px; font-size:12px;">

					<p style="font-size:12px; padding-top:25px; padding-left:8px; padding-right:8px; color:#4c4c4c; display:block;">

					<strong>Name</strong> : '.$model->name.'<br>

					<strong>Email</strong> : '.$model->email.'<br>

					<strong>Phone</strong> : '.$model->phone.'<br>

					</p>

					<h2 style="color:#636363; margin:0px; padding-bottom:10px; padding-left:8px; padding-right:8px; font-size:13px; font-weight:bold;">Cover Letter</h2>

					<p style="font-size:12px; padding-top:25px; padding-left:8px; padding-right:8px; color:#4c4c4c; display:block;">

					'.$model->cover_letter.'

					</p>					

					

					</div>

					<p style=" margin-top:10px; padding:8px 5px; color:#000; font-size:13px; margin-bottom:0px; font-weight:bold; line-height:18px; ">eDoctorBook Team</p>

					<a style="padding-left:5px; padding-right:5px; color:#000; font-size:13px; font-weight:bold; margin:0px; line-height:0px; text-decoration:none" href="">http://www.eDoctorBook.com</a>

					<div style="background:#54cbc8; padding-top:20px; margin-top:15px; padding-bottom:10px; text-align:center; color:#e2e2e2; font-size:14px;"></div>

					</div>



					</body>

					</html>';

					

				Helpers::mailsend_($to,$from,$subject,$message,$attachment);

				

				//$this->refresh();

				$this->redirect(array('doctor/contactThanks', 'type'=>'resume'));

			}

			

		}

		

		$this->render('submit_resume',array(

			'model'=>$model,

		));

	}

	

	public function actionContactThanks()

	{

		$this->render('contact_thanks',array(

			//'model'=>$model,

		));

	}

	

	public function actionAbout()

	{

		$this->render('about',array(

			//'model'=>$model,

		));

	}

	

	public function actionCareers()

	{

		$this->render('careers',array(

			//'model'=>$model,

		));

	}

	

	public function actionFaq()

	{

		$this->render('faq',array(

			//'model'=>$model,

		));

	}

	

	public function actionMedia()

	{

		$this->render('media',array(

			//'model'=>$model,

		));

	}

	

	public function actionTermsCondition()

	{

		$this->render('terms_condition',array(

			//'model'=>$model,

		));

	}

	

	public function actionTermsAndCondition()

	{

		$connection = Yii::app()->db;

		$sql='select * FROM da_company WHERE status="1"';

		$command = $connection->createCommand($sql);

		$company = $command->queryAll();

		echo $company[0]['terms'];

	}

	

	public function actionDoctorBookStep3()

	{

		

		//Helpers::pre($_REQUEST);

		//if(!isset($_POST) || !(Yii::app()->session['doctor_id']))

			//$this->redirect(array('site/index'));

		if(!Yii::app()->session['session_step2'])

			$this->redirect(array('site/index'));

			

		if(isset($_REQUEST['user_email'])){

			Yii::app()->session['entry_type'] = $_REQUEST['entry_type'];

			Yii::app()->session['user_email'] = $_REQUEST['user_email'];

			if(isset($_REQUEST['user_reminder']))

				Yii::app()->session['user_reminder'] = 'off';

			else

				Yii::app()->session['user_reminder'] = 'on';

			Yii::app()->session['user_password'] = $_REQUEST['user_password'];

			Yii::app()->session['user_first_name'] = $_REQUEST['user_first_name'];

			Yii::app()->session['user_last_name'] = $_REQUEST['user_last_name'];

			/*Yii::app()->session['user_month'] = $_REQUEST['user_month'];

			Yii::app()->session['user_date'] = $_REQUEST['user_date'];

			Yii::app()->session['user_year'] = $_REQUEST['user_year'];*/

			if($_REQUEST['user_dob']){

				$user_dob_arr = explode('-',$_REQUEST['user_dob']);

				Yii::app()->session['user_month'] = $user_dob_arr[1];

				Yii::app()->session['user_date'] = $user_dob_arr[2];

				Yii::app()->session['user_year'] = $user_dob_arr[0];

			}

			Yii::app()->session['user_sex'] = $_REQUEST['user_sex'];

		}

		$id = Yii::app()->session['doctor_id'];

		$start_time=Yii::app()->session['start_time'];

		$time_slot=Yii::app()->session['time_slot'];

		$address_id=Yii::app()->session['address_id'];

		$speciality_id=Yii::app()->session['speciality_id'];

		$procedure_id=Yii::app()->session['procedure_id'];

		$reason_for_visit=Yii::app()->session['reason_for_visit'];

		$insurance=Yii::app()->session['insurance'];

		

		if(isset($_REQUEST['user_email'])){

			$model = new Patient();

			$model->user_name = Yii::app()->session['user_email'];

			$model->user_email = Yii::app()->session['user_email'];

			$model->password = md5(Yii::app()->session['user_password']);

			$model->user_first_name = Yii::app()->session['user_first_name'];

			$model->user_last_name = Yii::app()->session['user_last_name'];

			$model->user_dob = Yii::app()->session['user_year'].'-'.Yii::app()->session['user_month'].'-'.Yii::app()->session['user_date'];

			$model->user_sex = Yii::app()->session['user_sex'];

			

			$model->entry_type = Yii::app()->session['entry_type'];

			$model->visitor = Yii::app()->session['visitor'];

			$model->patient_name = Yii::app()->session['patient_name'];

			$model->patient_email = Yii::app()->session['patient_email'];

			$model->patient_dob = Yii::app()->session['patient_year'].'-'.Yii::app()->session['patient_month'].'-'.Yii::app()->session['patient_date'];

			$model->patient_sex = Yii::app()->session['patient_sex'];

			$model->entry_type = Yii::app()->session['entry_type'];

			$model->user_reminder = Yii::app()->session['user_reminder'];

			

			$model->status = 0;

			$model->date_created = date('Y-m-d h:i:s');

			$model->date_modified = date('Y-m-d h:i:s');

			

			if($model->save()){

				Yii::app()->session['patient_id'] = $model->id;

				/////////////////

				$usercriteria =Patient::model()->find("id=\"$model->id\"");

				

				$temp_id = 1;

				$email_template = EmailTemplate::model()->find("id=\"$temp_id\"");

				if(!empty($email_template) && $email_template->tempalte_body != ''){

					$message_body = $email_template->tempalte_body;

					//$message_body = $this->getTemplateDataParsing($parse_with,$parse_data,$message_body);

					

					$parse_data_arr = array();

					/*$parse_data_arr['{{patient.user_full_name}}'] = $usercriteria['user_first_name']." ".$usercriteria['user_last_name'];

					$parse_data_arr['{{patient.user_first_name}}'] = $usercriteria['user_first_name'];

					$parse_data_arr['{{patient.user_last_name}}'] = $usercriteria['user_last_name'];

					*/

					$parse_data_arr['{{patient.user_full_name}}'] = $usercriteria['user_first_name']." ".$usercriteria['user_last_name'];

					$parse_data_arr['{{patient.user_first_name}}'] = $usercriteria['user_first_name'];

					$parse_data_arr['{{patient.user_last_name}}'] = $usercriteria['user_last_name'];

					

					$parse_data_arr['{{patient.user_title}}'] =$usercriteria['patient_name'];

					$parse_data_arr['{{patient.user_mobile_number}}'] = isset( $usercriteria['user_phone'] ) ? $usercriteria['user_phone'] :  '';

					$parse_data_arr['{{patient.user_home_number}}'] = isset( $usercriteria['user_phone'] ) ? $usercriteria['user_phone'] :  '';

					$parse_data_arr['{{patient.user_work_number}}'] = isset( $usercriteria['user_phone'] ) ? $usercriteria['user_phone'] :  '';

					$parse_data_arr['{{patient.user_fax_number}}'] = isset( $usercriteria['user_phone'] ) ? $usercriteria['user_phone'] :  '';

					$parse_data_arr['{{patient.user_other_number}}'] = isset( $usercriteria['user_phone'] ) ? $usercriteria['user_phone'] :  '';

					$parse_data_arr['{{patient.user_email}}'] = $usercriteria['patient_email'];

					$parse_data_arr['{{patient.user_address_1}}'] = isset( $usercriteria['user_address'] ) ? $usercriteria['user_address'] :  '';

					$parse_data_arr['{{patient.user_address_2}}'] = isset( $usercriteria['user_address'] ) ? $usercriteria['user_address'] : '';

					$parse_data_arr['{{patient.user_address_3}}'] = isset( $usercriteria['user_address'] ) ? $usercriteria['user_address'] :  '';

					$parse_data_arr['{{patient.user_city}}'] = $usercriteria['user_city'];

					$parse_data_arr['{{patient.user_post_code}}'] = $usercriteria['user_zip'];

					$parse_data_arr['{{patient.user_state}}'] = $usercriteria['user_state'];

					$parse_data_arr['{{patient.user_country}}'] = isset( $usercriteria['patient_country'] ) ? $usercriteria['patient_country'] :  '';

					$parse_data_arr['{{patient.user_dateOfBirth}}'] = isset( $usercriteria['patient_dob'] ) ? date('d-m-Y' ,strtotime( $usercriteria['patient_dob'] ) ) : '';

					$parse_data_arr['{{patient.user_gender}}'] = Ucfirst( $usercriteria['patient_sex'] );

					

					$parse_data_arr['{firstname}'] = $usercriteria['user_first_name'];

					

					$message_body = $this->getTemplateDataParsing($parse_data_arr,$message_body);

				}else{

					$message_body = '<h2 style="color:#636363; margin:0px; padding-bottom:10px; padding-left:8px; padding-right:8px; font-size:13px; font-weight:bold;">Your account has now been created.</h2>';

				}

				

				$to_email = $usercriteria['user_email'];

				$to_name = $usercriteria['user_first_name']." ".$usercriteria['user_last_name'];

				$reg_id = $usercriteria['id'];

				$username = $usercriteria['user_name'];

				//$password = $usercriteria['password'];

				

				$to = array($to_email,$to_name);

				$from = array('info.doctor@gmail.com','eDoctorBook');

				//$subject = 'eDoctorBook - Thank you for registering.';

				$subject = isset( $email_template->subject ) ? $email_template->subject : 'eDoctorBook - Thank you for registering.';

				$message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

					<html xmlns="http://www.w3.org/1999/xhtml">

					<head>

					<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

					<title>Doctor Email Template</title>

					</head>

					<body style="padding:0px; margin:0px; font-size:12px; color:#545454; line-height:18px; font-family:Arial, Helvetica, sans-serif;">

					<div style="width:450px;  margin:5px auto; border:1px solid #c7c7c7; "> 

					<div style="text-align:left; margin-bottom:15px; -moz-box-shadow:0px 1px 1px 1px #E4E4E4; -webkit-box-shadow:0px 1px 1px 1px #E4E4E4;box-shadow:0px 1px 1px 1px #E4E4E4; background:#54cbc8; font-weight:bold;">

					<a href="'.$this->createAbsoluteUrl('site/index/').'"><img src="http://'.$_SERVER['HTTP_HOST'].Yii::app()->getHomeUrl().'/assets/images/small_logo.png" style="padding:3px;" alt="Doctor Appointment Book" /></a>

					&nbsp;

					</div>	



					<h1 style="text-align:center; display:block; font-weight:normal; margin:0px; padding-bottom:12px; font-size:19px; line-height:26px; color:#4c4c4c;">Thank you '.$to_name.' for using <br /> <font style="color:#6ad2cf">eDoctorBook</font>!</h1>



					<div style="min-height:200px; background:#fff; color:#bbb; margin:10px; padding-top:15px; font-size:12px;">

<pre style="color:#000">'.trim($message_body).'</pre>

					<br>

					<p style="font-size:12px; padding-top:25px; padding-left:8px; padding-right:8px; color:#4c4c4c; display:block;">You can log in by using your provided User Id and password 

					at the following URL:<br />

					 <a style="color:#2593e0; text-decoration:underline; display:block;" href="'.$this->createAbsoluteUrl('site/index/').'">'.$this->createAbsoluteUrl('site/index/').'</a>

					</p>

					<br>

					</div>

					<p style=" margin-top:10px; padding:8px 5px; color:#000; font-size:13px; margin-bottom:0px; font-weight:bold; line-height:18px; ">eDoctorBook Team</p>

					<a style="padding-left:5px; padding-right:5px; color:#000; font-size:13px; font-weight:bold; margin:0px; line-height:0px; text-decoration:none" href="">http://www.eDoctorBook.com</a>

					<div style="background:#54cbc8; padding-top:20px; margin-top:15px; padding-bottom:10px; text-align:center; color:#e2e2e2; font-size:14px;"></div>

					</div>



					</body>

					</html>';

				

					Helpers::mailsend($to,$from,$subject,$message);

				////////////////

				$this->refresh();

			}

		}

		

		$usercriteria =Doctor::model()->find("id=\"$id\"");

		

		$connection = Yii::app()->db;

		$sql='select * FROM da_speciality WHERE status="1"';

		$command = $connection->createCommand($sql);

		$user_speciality = $command->queryAll();

		$user_speciality_res = array();

		foreach ($user_speciality as $key=>$val) {

			$user_speciality_res[$val['id']] = $val['speciality'];

		}

		

		$sql='select * FROM da_procedure WHERE status="1"';

		$command = $connection->createCommand($sql);

		$user_procedure = $command->queryAll();

		$user_procedure_res = array();

		foreach ($user_procedure as $key=>$val) {

			$user_procedure_res[$val['id']] = $val['procedure'];

		}

		

		$sql_insurance='select * FROM da_insurance WHERE status="1"';

		$command_insurance = $connection->createCommand($sql_insurance);

		$user_insurance = $command_insurance->queryAll();

		$user_insurance_res = array();

		foreach ($user_insurance as $key=>$val) {

			$user_insurance_res[$val['id']] = $val['insurance'];

		}

		

		$sql_multy_insurance='select * FROM da_doctor_insurance WHERE status="1" and doctor_id="'.$id.'"';

		$command_multy_insurance = $connection->createCommand($sql_multy_insurance);

		$user_multy_insurance = $command_multy_insurance->queryAll();

		$user_selected_insurance = array();

		foreach ($user_multy_insurance as $key=>$val) {

			$user_selected_insurance[] = $val['insurance_id'];

		}

		

		$default_data_address = DoctorAddress::model()->find("id=".$address_id);

		

		$this->render('doctor_book_step3',array(

			'dataProvider'=>$usercriteria,'user_speciality' => $user_speciality_res,'user_procedure' => $user_procedure_res,'default_data_address' => $default_data_address,'procedure_id' => $procedure_id,'user_insurance' => $user_insurance_res,'user_selected_insurance' => $user_selected_insurance

		));

	}

	

	public function actionTwilioPinVerification()

	{

		$verify_pin = $_REQUEST['verify_pin'];

		if(Yii::app()->session['mobile_pin'] == $verify_pin){

			$sql='UPDATE da_patient SET status = :status,user_phone = :user_phone WHERE id ='.Yii::app()->session['patient_id'];

			$params = array(

				"status" => 1,

				"user_phone" => Yii::app()->session['mobile_phone']

			);

			$connection = Yii::app()->db;

			$command=$connection->createCommand($sql);

			$command->execute($params);

			

			$patient_id = Yii::app()->session['patient_id'];

			$usercriteria =Patient::model()->find("id=\"$patient_id\" and status=1");

			Yii::app()->session['logged_in'] = true;

			Yii::app()->session['logged_user_email'] = $usercriteria->user_name;

			Yii::app()->session['logged_user_id'] = $usercriteria->id;

			Yii::app()->session['logged_user_type'] = 'patient';

			

			$app_time = date('l, F j - h:i A',Yii::app()->session['start_time']);

			

			$doctor_id_temp = Yii::app()->session['doctor_id'];

			$doctor_det_temp =Doctor::model()->find("id=\"$doctor_id_temp\"");

			$state_det_temp_d = State::model()->find("short_code=\"$doctor_det_temp->state\"");

				

			$app_id = 0;

			if( isset( Yii::app()->session['address_id'] ) ) {

				$app_id = Yii::app()->session['address_id'];

			}		

			$address_det_temp_p = DoctorAddress::model()->find("id=\"$app_id\"");	

			$state_det_temp_p = State::model()->find("short_code=\"$address_det_temp_p->state\"");

					

			$temp_id = 15;

			$email_template = EmailTemplate::model()->find("id=\"$temp_id\"");

			if(!empty($email_template) && $email_template->tempalte_body != ''){

				$message_body = $email_template->tempalte_body;

				

				$parse_data_arr = array();

				$parse_data_arr['{{appointment.actual_format}}'] = date('l, F j - h:i A',Yii::app()->session['start_time']);

				$parse_data_arr['{{appointment.day}}'] = date('l',Yii::app()->session['start_time']);

				$parse_data_arr['{{appointment.month}}'] = date('F',Yii::app()->session['start_time']);

				$parse_data_arr['{{appointment.date}}'] = date('j',Yii::app()->session['start_time']);

				$parse_data_arr['{{appointment.time}}'] = date('h:i A',Yii::app()->session['start_time']);

				$parse_data_arr['{{appointment.address_1}}'] = isset ( $address_det_temp_p->address ) ? $address_det_temp_p->address : $doctor_det_temp->addr1;

				$parse_data_arr['{{appointment.city}}'] = isset ( $address_det_temp_p->city ) ? $address_det_temp_p->city : $doctor_det_temp->city ;

				$parse_data_arr['{{appointment.state}}'] = isset( $state_det_temp_p->name ) ? $state_det_temp_p->name : $state_det_temp_d->name;

				$parse_data_arr['{{appointment.zip_code}}'] = isset ( $address_det_temp_p->zip ) ? $address_det_temp_p->zip : $doctor_det_temp->zip;

				

				$parse_data_arr['{{patient.user_full_name}}'] = $usercriteria->user_first_name." ".$usercriteria->user_last_name;

				$parse_data_arr['{{patient.user_full_name}}'] = $usercriteria->user_first_name." ".$usercriteria->user_last_name;

				$parse_data_arr['{{patient.user_first_name}}'] = $usercriteria->user_first_name;

				$parse_data_arr['{{patient.user_last_name}}'] = $usercriteria->user_last_name;

				

				$parse_data_arr['{{patient.user_title}}'] = $usercriteria->patient_name;

				$parse_data_arr['{{patient.user_mobile_number}}'] = isset( $usercriteria->user_phone ) ? $usercriteria->user_phone :  '';

				$parse_data_arr['{{patient.user_home_number}}'] = isset( $usercriteria->user_phone ) ? $usercriteria->user_phone :  '';

				$parse_data_arr['{{patient.user_work_number}}'] = isset( $usercriteria->user_phone ) ? $usercriteria->user_phone :  '';

				$parse_data_arr['{{patient.user_fax_number}}'] = isset( $usercriteria->user_phone ) ? $usercriteria->user_phone :  '';

				$parse_data_arr['{{patient.user_other_number}}'] = isset( $usercriteria->user_phone ) ? $usercriteria->user_phone :  '';

				$parse_data_arr['{{patient.user_email}}'] = $usercriteria->patient_email;

				$parse_data_arr['{{patient.user_address_1}}'] = isset( $usercriteria->user_address ) ? $usercriteria->user_address :  '';

				$parse_data_arr['{{patient.user_address_2}}'] = isset( $usercriteria->user_address ) ? $usercriteria->user_address : '';

				$parse_data_arr['{{patient.user_address_3}}'] = isset( $usercriteria->user_address ) ? $usercriteria->user_address :  '';

				$parse_data_arr['{{patient.user_city}}'] = $usercriteria->user_city;

				$parse_data_arr['{{patient.user_post_code}}'] = $usercriteria->user_zip;

				$parse_data_arr['{{patient.user_state}}'] = $usercriteria->user_state;

				$parse_data_arr['{{patient.user_country}}'] = isset( $usercriteria->patient_country ) ? $usercriteria->patient_country :  '';

				$parse_data_arr['{{patient.user_dateOfBirth}}'] = isset( $usercriteria->patient_dob ) ? date('d-m-Y' ,strtotime( $usercriteria->patient_dob ) ) : '';

				$parse_data_arr['{{patient.user_gender}}'] = Ucfirst( $usercriteria->patient_sex );

				

				$parse_data_arr['{firstname}'] = $usercriteria->user_first_name;

				

				$parse_data_arr['{user}'] = $doctor_det_temp->first_name." ".$doctor_det_temp->last_name;

				$parse_data_arr['{{doctor.full_name}}'] = $doctor_det_temp->full_name;

				$parse_data_arr['{{doctor.first_name}}'] = $doctor_det_temp->first_name;

				$parse_data_arr['{{doctor.last_name}}'] = $doctor_det_temp->last_name;

				$parse_data_arr['{{doctor.address_1}}'] = $doctor_det_temp->addr1;

				$parse_data_arr['{{doctor.city}}'] = $doctor_det_temp->city;

				//$parse_data_arr['{{doctor.state}}'] = $state_det_temp_d->name;

				$parse_data_arr['{{doctor.zip_code}}'] = $doctor_det_temp->zip;

				$parse_data_arr['{{doctor.phone}}'] = $doctor_det_temp->phone;

				$parse_data_arr['{{doctor.email}}'] = $doctor_det_temp->email;

				$parse_data_arr['{{doctor.parctise_name}}'] = $doctor_det_temp->practice;

				

				$message_body = $this->getTemplateDataParsing($parse_data_arr,$message_body);

			}else{

				$message_body = '<h2 style="color:#636363; margin:0px; padding-bottom:10px; padding-left:8px; padding-right:8px; font-size:13px; font-weight:bold;">Your Appointment has now been Booked.</h2>Your Appointment Time is '.$app_time.'.';

			}

			

			$to_email = $usercriteria->user_email;

			$to_name = $usercriteria->user_first_name." ".$usercriteria->user_last_name;

			$to = array($to_email,$to_name);

			$from = array('support@edoctorbook.com','eDoctorBook');

			//$subject = 'eDoctorBook - Appointment Book.';

			$subject = isset( $email_template->subject ) ? $email_template->subject : 'eDoctorBook - Appointment Book.';

			$message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

					<html xmlns="http://www.w3.org/1999/xhtml">

					<head>

					<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

					<title>Doctor Email Template</title>

					</head>

					<body style="padding:0px; margin:0px; font-size:12px; color:#545454; line-height:18px; font-family:Arial, Helvetica, sans-serif;">

					<div style="width:450px;  margin:5px auto; border:1px solid #c7c7c7; "> 

					<div style="text-align:left; margin-bottom:15px; -moz-box-shadow:0px 1px 1px 1px #E4E4E4; -webkit-box-shadow:0px 1px 1px 1px #E4E4E4;box-shadow:0px 1px 1px 1px #E4E4E4; background:#54cbc8; font-weight:bold;">

					<a href="'.$this->createAbsoluteUrl('site/index/').'"><img src="http://'.$_SERVER['HTTP_HOST'].Yii::app()->getHomeUrl().'/assets/images/small_logo.png" style="padding:3px;" alt="Doctor Appointment Book" /></a>

					&nbsp;

					</div>	



					<h1 style="text-align:center; display:block; font-weight:normal; margin:0px; padding-bottom:12px; font-size:19px; line-height:26px; color:#4c4c4c;">Thank you '.$to_name.' for using <br /> <font style="color:#6ad2cf">eDoctorBook</font>!</h1>



					<div style="min-height:200px; background:#fff; color:#bbb; margin:10px; padding-top:15px; font-size:12px;">

<pre style="color:#000">'.$message_body.'</pre>

					<br>

					

					</div>

					<p style=" margin-top:10px; padding:8px 5px; color:#000; font-size:13px; margin-bottom:0px; font-weight:bold; line-height:18px; ">eDoctorBook Team</p>

					<a style="padding-left:5px; padding-right:5px; color:#000; font-size:13px; font-weight:bold; margin:0px; line-height:0px; text-decoration:none" href="">http://www.eDoctorBook.com</a>

					<div style="background:#54cbc8; padding-top:20px; margin-top:15px; padding-bottom:10px; text-align:center; color:#e2e2e2; font-size:14px;"></div>

					</div>



					</body>

					</html>';

				

			Helpers::mailsend($to,$from,$subject,$message);

			Yii::app()->session['session_step3'] = true;

			echo 'ok';

		}else	echo 'not';

	}

	

	public function actionTwilioPatientRegistration()

	{

		$patient_id = Yii::app()->session['patient_id'];

		$pin = rand(1000,9999);

		$connection = Yii::app()->db;

		$sql='select * FROM da_patient WHERE id='.$patient_id;

		$command = $connection->createCommand($sql);

		$user_patient = $command->queryAll();

		

		$patient_sms_temp =Patient::model()->find("id=\"$patient_id\"");

		$temp_id = 1;

		$sms_template = SmsTemplate::model()->find("id=\"$temp_id\"");

		if(!empty($sms_template) && $sms_template->tempalte_body != ''){

			$message_body = $sms_template->tempalte_body;

			

			$parse_data_arr = array();

			$parse_data_arr['{{patient.user_full_name}}'] = $patient_sms_temp->user_first_name." ".$patient_sms_temp->user_last_name;

			$parse_data_arr['{{patient.user_first_name}}'] = $patient_sms_temp->user_first_name;

			$parse_data_arr['{{patient.user_last_name}}'] = $patient_sms_temp->user_last_name;

			$parse_data_arr['{{pin.no}}'] = $pin;

			

			$message_body = $this->getTemplateDataParsing($parse_data_arr,$message_body);

		}

		

		$verify_phone = $_REQUEST['verify_phone'];

		$verify_person = $user_patient[0]['user_first_name'].' '.$user_patient[0]['user_last_name'];

		/* Send an SMS using Twilio. You can run this file 3 different ways:

		*

		* - Save it as sendnotifications.php and at the command line, run

		* php sendnotifications.php

		*

		* - Upload it to a web host and load mywebhost.com/sendnotifications.php

		* in a web browser.

		* - Download a local server like WAMP, MAMP or XAMPP. Point the web root

		* directory to the folder containing this file, and load

		* localhost:8888/sendnotifications.php in a web browser.

		*/

		// Step 1: Download the Twilio-PHP library from twilio.com/docs/libraries,

		// and move it into the folder containing this file.

		

		//include_once( dirname(__FILE__) . "/../extensions/twilio/Services/Twilio.php");

		Yii::import('application.vendor.twilio.*');

		spl_autoload_unregister(array('YiiBase','autoload')); 

		//require 'C:\Projects\EMR\webapp\protected\vendors\twilio-php\Services\Twilio.php';

		include_once( dirname(__FILE__) . "/../vendor/twilio/Services/Twilio.php");

		spl_autoload_register(array('YiiBase', 'autoload'));



		

		 // Step 2: set our AccountSid and AuthToken from www.twilio.com/user/account

		$AccountSid = "AC74068ab1383071d146bebb5c905dbcb5";

		$AuthToken = "6ee3bc04ff855fefaefc68d9cd1bd778";

		$AuthPhone = "+16508259090";

		// Step 3: instantiate a new Twilio Rest Client

		$client = new Services_Twilio($AccountSid, $AuthToken);

		// Step 4: make an array of people we know, to send them a message.

		// Feel free to change/add your own phone number and name here.

		$people = array(

		"$verify_phone" => "$verify_person",

		);

		// Step 5: Loop over all our friends. $number is a phone number above, and

		// $name is the name next to it

		foreach ($people as $number => $name) {

		//$message = "Hey $name, Your eDoctorBook account verification pin is $pin";

		if(isset($message_body) && $message_body != '')

			$message = $message_body;

		else

			$message = "Hey $name, Your eDoctorBook account verification pin is $pin";

		Yii::app()->session['mobile_phone'] = $verify_phone;

		Yii::app()->session['mobile_pin'] = $pin;

		/*$sms = $client->account->messages->sendMessage(

		// Step 6: Change the 'From' number below to be a valid Twilio number

		// that you've purchased, or the (deprecated) Sandbox number

		"$AuthPhone",

		// the number we are sending to - Any phone number

		$number,

		// the sms body

		$message

		);*/

		//if($sms = $client->account->messages->sendMessage("$AuthPhone",$number,$message))



		$sms = $client->account->messages->sendMessage("$AuthPhone",$number,$message);

		//echo $sms;
		echo $sms."-".$pin;
		//else throw new ExceptionClass('ExceptionMessage');

		// Display a confirmation message on the screen

		

		//echo "Sent message to $name";

		}

	}

	

	public function actionDoctorBookStep4()

	{

		

		//Helpers::pre($_REQUEST);

		//if(!Yii::app()->session['patient_id'])

			//$this->redirect(array('site/index'));

		if(!Yii::app()->session['session_step3'])

			$this->redirect(array('site/index'));

		//Helpers::pre($model);

		$id = Yii::app()->session['doctor_id'];

		$start_time=Yii::app()->session['start_time'];

		$address_id=Yii::app()->session['address_id'];

		$speciality_id=Yii::app()->session['speciality_id'];

		$procedure_id=Yii::app()->session['procedure_id'];

		

		$usercriteria =Doctor::model()->find("id=\"$id\"");

		

		$connection = Yii::app()->db;

		$sql='select * FROM da_speciality WHERE status="1"';

		$command = $connection->createCommand($sql);

		$user_speciality = $command->queryAll();

		$user_speciality_res = array();

		foreach ($user_speciality as $key=>$val) {

			$user_speciality_res[$val['id']] = $val['speciality'];

		}

		

		$sql='select * FROM da_procedure WHERE status="1"';

		$command = $connection->createCommand($sql);

		$user_procedure = $command->queryAll();

		$user_procedure_res = array();

		foreach ($user_procedure as $key=>$val) {

			$user_procedure_res[$val['id']] = $val['procedure'];

		}

		

		$sql_insurance='select * FROM da_insurance WHERE status="1"';

		$command_insurance = $connection->createCommand($sql_insurance);

		$user_insurance = $command_insurance->queryAll();

		$user_insurance_res = array();

		foreach ($user_insurance as $key=>$val) {

			$user_insurance_res[$val['id']] = $val['insurance'];

		}

		

		$sql_multy_insurance='select * FROM da_doctor_insurance WHERE status="1" and doctor_id="'.$id.'"';

		$command_multy_insurance = $connection->createCommand($sql_multy_insurance);

		$user_multy_insurance = $command_multy_insurance->queryAll();

		$user_selected_insurance = array();

		foreach ($user_multy_insurance as $key=>$val) {

			$user_selected_insurance[] = $val['insurance_id'];

		}

		

		$default_data_address = DoctorAddress::model()->find("id=".$address_id);

		if(!Yii::app()->session['app_complete']){

		$model = new DoctorBook();

		if(Yii::app()->session['logged_in'] && Yii::app()->session['logged_user_type'] == 'patient'){

			$model->patient_id = Yii::app()->session['logged_user_id'];

		}else{

			$model->patient_id = Yii::app()->session['patient_id'];

		}

		$model->book_time = Yii::app()->session['start_time'];

		$model->book_duration=(Yii::app()->session['time_slot']/60);

		$model->doctor_id = Yii::app()->session['doctor_id'];

		$model->address_id = Yii::app()->session['address_id'];

		$model->speciality_id = Yii::app()->session['speciality_id'];

		$model->procedure_id = Yii::app()->session['procedure_id'];

		$model->reason_for_visit_id = Yii::app()->session['reason_for_visit'];

		$model->patient_insurance = Yii::app()->session['insurance'];

		

		$model->status = 1;

		$model->date_created = date('Y-m-d h:i:s');

		$model->date_modified = date('Y-m-d h:i:s');

		

		if($model->save()){

			//Yii::app()->session['patientid'] = $model->id;

			Yii::app()->session['app_complete'] = 'app_complete';

			$this->refresh();

		}

		}

		$this->render('doctor_book_step4',array(

			'dataProvider'=>$usercriteria,'user_speciality' => $user_speciality_res,'user_procedure' => $user_procedure_res,'default_data_address' => $default_data_address,'procedure_id' => $procedure_id,'user_insurance' => $user_insurance_res,'user_selected_insurance' => $user_selected_insurance

		));

	}

	

	public function actionPatientLogin()

	{

		if(Yii::app()->getRequest()->isAjaxRequest){

			//print_r($_REQUEST);

			$user_name = Yii::app()->getRequest()->getPost('patient_signin_email');

			$password = Yii::app()->getRequest()->getPost('patient_signin_password');

			$usercriteria =Patient::model()->find("user_name=\"$user_name\" and password=md5(\"$password\")");

			//print_r($usercriteria);

			if(isset($usercriteria)){

				Yii::app()->session['patient_id'] = $usercriteria->id;

				Yii::app()->session['logged_in'] = true;

				Yii::app()->session['logged_user_email'] = $usercriteria->user_name;

				Yii::app()->session['logged_user_id'] = $usercriteria->id;

				Yii::app()->session['logged_user_type'] = 'patient';

				//$this->redirect(array('doctor/doctorBookStep4'));

				$app_time = date('l, F j - h:i A',Yii::app()->session['start_time']);

				$app_id = 0;

				if( isset( Yii::app()->session['address_id'] ) ) {

					$app_id = Yii::app()->session['address_id'];

				}



				$address_det_temp_p = DoctorAddress::model()->find("id=\"$app_id\"");



				$doctor_id_temp = Yii::app()->session['doctor_id'];

				$doctor_det_temp =Doctor::model()->find("id=\"$doctor_id_temp\"");

				$state_det_temp_d = State::model()->find("short_code=\"$doctor_det_temp->state\"");		

				$temp_id = 15;

				$email_template = EmailTemplate::model()->find("id=\"$temp_id\"");

				if(!empty($email_template) && $email_template->tempalte_body != ''){

					$message_body = $email_template->tempalte_body;

					//$message_body = $this->getTemplateDataParsing($parse_with,$parse_data,$message_body);

					/*

					$parse_data_arr = array();

					$parse_data_arr['{{appointment.actual_format}}'] = date('l, F j - h:i A',Yii::app()->session['start_time']);

					$parse_data_arr['{{appointment.day}}'] = date('l',Yii::app()->session['start_time']);

					$parse_data_arr['{{appointment.month}}'] = date('F',Yii::app()->session['start_time']);

					$parse_data_arr['{{appointment.date}}'] = date('j',Yii::app()->session['start_time']);

					$parse_data_arr['{{appointment.time}}'] = date('h:i A',Yii::app()->session['start_time']);

					$parse_data_arr['{{patient.user_full_name}}'] = $usercriteria->user_first_name." ".$usercriteria->user_last_name;

					$parse_data_arr['{{patient.user_first_name}}'] = $usercriteria->user_first_name;

					$parse_data_arr['{{patient.user_last_name}}'] = $usercriteria->user_last_name;

					$parse_data_arr['{{doctor.full_name}}'] = $doctor_det_temp->first_name." ".$doctor_det_temp->last_name;

					$parse_data_arr['{{doctor.first_name}}'] = $doctor_det_temp->first_name;

					$parse_data_arr['{{doctor.last_name}}'] = $doctor_det_temp->last_name;

					*/

					$parse_data_arr = array();

					$parse_data_arr['{{appointment.actual_format}}'] = date('l, F j - h:i A',Yii::app()->session['start_time']);

					$parse_data_arr['{{appointment.day}}'] = date('l',Yii::app()->session['start_time']);

					$parse_data_arr['{{appointment.month}}'] = date('F',Yii::app()->session['start_time']);

					$parse_data_arr['{{appointment.date}}'] = date('j',Yii::app()->session['start_time']);

					$parse_data_arr['{{appointment.time}}'] = date('h:i A',Yii::app()->session['start_time']);

					$parse_data_arr['{{appointment.address_1}}'] = isset ( $address_det_temp_p->address ) ? $address_det_temp_p->address : $doctor_det_temp->addr1;

					$parse_data_arr['{{appointment.city}}'] = isset ( $address_det_temp_p->city ) ? $address_det_temp_p->city : $doctor_det_temp->city ;

					$parse_data_arr['{{appointment.state}}'] = isset( $address_det_temp_p->state ) ? $address_det_temp_p->state : $state_det_temp_d->name;

					$parse_data_arr['{{appointment.zip_code}}'] = isset ( $address_det_temp_p->zip ) ? $address_det_temp_p->zip : $doctor_det_temp->zip;

					

					$parse_data_arr['{{patient.user_full_name}}'] = $usercriteria->user_first_name." ".$usercriteria->user_last_name;

					$parse_data_arr['{{patient.user_full_name}}'] = $usercriteria->user_first_name." ".$usercriteria->user_last_name;

					$parse_data_arr['{{patient.user_first_name}}'] = $usercriteria->user_first_name;

					$parse_data_arr['{{patient.user_last_name}}'] = $usercriteria->user_last_name;

					

					$parse_data_arr['{{patient.user_title}}'] = $usercriteria->patient_name;

					$parse_data_arr['{{patient.user_mobile_number}}'] = isset( $usercriteria->user_phone ) ? $usercriteria->user_phone :  '';

					$parse_data_arr['{{patient.user_home_number}}'] = isset( $usercriteria->user_phone ) ? $usercriteria->user_phone :  '';

					$parse_data_arr['{{patient.user_work_number}}'] = isset( $usercriteria->user_phone ) ? $usercriteria->user_phone :  '';

					$parse_data_arr['{{patient.user_fax_number}}'] = isset( $usercriteria->user_phone ) ? $usercriteria->user_phone :  '';

					$parse_data_arr['{{patient.user_other_number}}'] = isset( $usercriteria->user_phone ) ? $usercriteria->user_phone :  '';

					$parse_data_arr['{{patient.user_email}}'] = $usercriteria->patient_email;

					$parse_data_arr['{{patient.user_address_1}}'] = isset( $usercriteria->user_address ) ? $usercriteria->user_address :  '';

					$parse_data_arr['{{patient.user_address_2}}'] = isset( $usercriteria->user_address ) ? $usercriteria->user_address : '';

					$parse_data_arr['{{patient.user_address_3}}'] = isset( $usercriteria->user_address ) ? $usercriteria->user_address :  '';

					$parse_data_arr['{{patient.user_city}}'] = $usercriteria->user_city;

					$parse_data_arr['{{patient.user_post_code}}'] = $usercriteria->user_zip;

					$parse_data_arr['{{patient.user_state}}'] = $usercriteria->user_state;

					$parse_data_arr['{{patient.user_country}}'] = isset( $usercriteria->patient_country ) ? $usercriteria->patient_country :  '';

					$parse_data_arr['{{patient.user_dateOfBirth}}'] = isset( $usercriteria->patient_dob ) ? date('d-m-Y' ,strtotime( $usercriteria->patient_dob ) ) : '';

					$parse_data_arr['{{patient.user_gender}}'] = Ucfirst( $usercriteria->patient_sex );

					

					$parse_data_arr['{firstname}'] = $usercriteria->user_first_name;

					

					$parse_data_arr['{user}'] = $doctor_det_temp->first_name." ".$doctor_det_temp->last_name;

					$parse_data_arr['{{doctor.full_name}}'] = $doctor_det_temp->full_name;

					$parse_data_arr['{{doctor.first_name}}'] = $doctor_det_temp->first_name;

					$parse_data_arr['{{doctor.last_name}}'] = $doctor_det_temp->last_name;

					$parse_data_arr['{{doctor.address_1}}'] = $doctor_det_temp->addr1;

					$parse_data_arr['{{doctor.city}}'] = $doctor_det_temp->city;

					//$parse_data_arr['{{doctor.state}}'] = $state_det_temp_d->name;

					$parse_data_arr['{{doctor.zip_code}}'] = $doctor_det_temp->zip;

					$parse_data_arr['{{doctor.phone}}'] = $doctor_det_temp->phone;

					$parse_data_arr['{{doctor.email}}'] = $doctor_det_temp->email;

					$parse_data_arr['{{doctor.parctise_name}}'] = $doctor_det_temp->practice;

					

					$message_body = $this->getTemplateDataParsing($parse_data_arr,$message_body);

				}else{

					$message_body = '<h2 style="color:#636363; margin:0px; padding-bottom:10px; padding-left:8px; padding-right:8px; font-size:13px; font-weight:bold;">Your Appointment has now been Booked.</h2>Your Appointment Time is '.$app_time.'.';

				}

				

				$to_email = $usercriteria->user_email;

				$to_name = $usercriteria->user_first_name." ".$usercriteria->user_last_name;

				$to = array($to_email,$to_name);

				$from = array('support@edoctorbook.com','eDoctorBook');

				//$subject = 'eDoctorBook - Appointment Book.';

				$subject = isset( $email_template->subject ) ? $email_template->subject : 'eDoctorBook - Appointment Book.';

				$message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

					<html xmlns="http://www.w3.org/1999/xhtml">

					<head>

					<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

					<title>Doctor Email Template</title>

					</head>

					<body style="padding:0px; margin:0px; font-size:12px; color:#545454; line-height:18px; font-family:Arial, Helvetica, sans-serif;text-align: justify; ">

					<div style="width:450px;  margin:5px auto; border:1px solid #c7c7c7; "> 

					<div style="text-align:left; margin-bottom:15px; -moz-box-shadow:0px 1px 1px 1px #E4E4E4; -webkit-box-shadow:0px 1px 1px 1px #E4E4E4;box-shadow:0px 1px 1px 1px #E4E4E4; background:#54cbc8; font-weight:bold;">

					<a href="'.$this->createAbsoluteUrl('site/index/').'"><img src="http://'.$_SERVER['HTTP_HOST'].Yii::app()->getHomeUrl().'/assets/images/small_logo.png" style="padding:3px;" alt="Doctor Appointment Book" /></a>

					&nbsp;

					</div>	



					<h1 style="text-align:center; display:block; font-weight:normal; margin:0px; padding-bottom:12px; font-size:19px; line-height:26px; color:#4c4c4c;">Thank you '.$to_name.' for using <br /> <font style="color:#6ad2cf">eDoctorBook</font>!</h1>



					<div style="min-height:200px; background:#fff; color:#bbb; margin:10px; padding-top:15px; font-size:12px;">

<pre style="color:#000">'.$message_body.'</pre>

					<br>

					

					</div>

					<p style=" margin-top:10px; padding:8px 5px; color:#000; font-size:13px; margin-bottom:0px; font-weight:bold; line-height:18px; ">eDoctorBook Team</p>

					<a style="padding-left:5px; padding-right:5px; color:#000; font-size:13px; font-weight:bold; margin:0px; line-height:0px; text-decoration:none" href="">http://www.eDoctorBook.com</a>

					<div style="background:#54cbc8; padding-top:20px; margin-top:15px; padding-bottom:10px; text-align:center; color:#e2e2e2; font-size:14px;"></div>

					</div>



					</body>

					</html>';

				

			Helpers::mailsend($to,$from,$subject,$message);

			Yii::app()->session['session_step3'] = true;

				echo true;

			}else{

				echo false;

			}

		}

	}

	

	public function actionPatientHave()

	{

		if(Yii::app()->getRequest()->isAjaxRequest){

			//print_r($_REQUEST);

			$user_name = Yii::app()->getRequest()->getPost('user_email');

			$usercriteria =Patient::model()->find("user_name=\"$user_name\"");

			//print_r($usercriteria);

			if(isset($usercriteria)){

				echo true;

			}else{

				echo false;

			}

		}

	}

	

	function getLangLat($address) {

        $address = urlencode($address);

        $request_url = "http://maps.googleapis.com/maps/api/geocode/xml?address=" . $address . "&sensor=true";

        $xml = simplexml_load_file($request_url);

		$arr = array();

        if ($xml->status && $xml->status == "OK") {

           return $xml->result->geometry->location;

        } else {

            return (object) array('lat' => '', 'lng' => '');

        }

    }

	

	function getTemplateDataParsing($parse_data_arr,$message) {

         foreach($parse_data_arr as $parse_data_key=>$parse_data_val){

		 	$message = str_replace($parse_data_key, $parse_data_val, $message);

		 }

		 return $message;

    }

	

	public function actionSettingTab()

	{

		if(!Yii::app()->session['logged_in'])

			$this->redirect(array('site/index'));

		

		$doctor_id = Yii::app()->session['logged_user_id'];

		//$model->password = Yii::app()->getRequest()->getPost('password');

		$doctor_data =Doctor::model()->find("id=\"$doctor_id\"");

		

		$connection = Yii::app()->db;

		

		$sql_doctor_reminder='select * FROM da_doctor_reminder WHERE status=1 and doctor_id='.Yii::app()->session['logged_user_id'];

		$command_doctor_reminder = $connection->createCommand($sql_doctor_reminder);

		$user_doctor_reminder = $command_doctor_reminder->queryAll();

		

		$sql_doctor_other_settings='select * FROM da_doctor_appointment_settings WHERE status=1 and doctor_id='.Yii::app()->session['logged_user_id'];

		$command_doctor_other_settings = $connection->createCommand($sql_doctor_other_settings);

		$user_doctor_other_settings = $command_doctor_other_settings->queryRow();

		

		$sql_doctor_notification_settings='select * FROM da_doctor_notification_settings WHERE status=1 and doctor_id='.Yii::app()->session['logged_user_id'];

		$command_doctor_notification_settings = $connection->createCommand($sql_doctor_notification_settings);

		$user_doctor_notification_settings = $command_doctor_notification_settings->queryRow();

		//Helpers::pre($user_doctor_notification_settings);die;

		$this->render('setting_tab',array(

			'data_reminder'=>$user_doctor_reminder,'data_other_settings'=>$user_doctor_other_settings,'data_notification_settings'=>$user_doctor_notification_settings,'doctor_data'=>$doctor_data,

		));

	}

	

	public function actionReminderData($id = "")

	{

		if(!Yii::app()->session['logged_in'])

			$this->redirect(array('site/index'));

		

		$id = $_REQUEST['id'];

		$type = $_REQUEST['type'];

		$view_type = $_REQUEST['view_type'];

		if($view_type == 'view')	$view_name = 'john';

		else	$view_name = '{firstname}';

		$user_doctor_reminder = array();

		if($id != ''){

			$sql_doctor_reminder='select * FROM da_doctor_reminder WHERE status=1 and id='.$id;

			$connection = Yii::app()->db;

			$command_doctor_reminder = $connection->createCommand($sql_doctor_reminder);

			$user_doctor_reminder = $command_doctor_reminder->queryRow();

		}

		if($type == 'Email'){

			if(!empty($user_doctor_reminder) && $user_doctor_reminder['reminder_text'] != ''){

				if($user_doctor_reminder['reminder_for'] == 'Email'){

					$message_body = $user_doctor_reminder['reminder_text'];

					if($view_type == 'view'){

						$parse_data_arr = array();

						$parse_data_arr['{firstname}'] = 'john';

						$message_body = $this->getTemplateDataParsing($parse_data_arr,$message_body);

					}

					echo $message_body;

				}else{

					echo 'Hello '.ucfirst ( $view_name ).',

	

	This is a friendly reminder to confirm your scheduled upcoming appointment with {user} at {company}. Please take a moment to click the green confirm button to confirm your appointment.

	

	We are looking forward to seeing you for your appointment. Please be sure to arrive at least 10 minutes early. If you have any questions, please contact the office directly. Thank you and have a great day.';

				}

			}else{

				echo 'Hello '.ucfirst ( $view_name ).',

	

	This is a friendly reminder to confirm your scheduled upcoming appointment with {user} at {company}. Please take a moment to click the green confirm button to confirm your appointment.

	

	We are looking forward to seeing you for your appointment. Please be sure to arrive at least 10 minutes early. If you have any questions, please contact the office directly. Thank you and have a great day.';

			}

		}else{

			if(!empty($user_doctor_reminder) && $user_doctor_reminder['reminder_text'] != ''){

				if($user_doctor_reminder['reminder_for'] == 'SMS'){

					$message_body = $user_doctor_reminder['reminder_text'];

					if($view_type == 'view'){

						$parse_data_arr = array();

						$parse_data_arr['{firstname}'] = 'john';

						$message_body = $this->getTemplateDataParsing($parse_data_arr,$message_body);

					}

					echo $message_body;

				}else{

					echo 'Hi '.$view_name.'. You have an appointment with {user} on {date} at {time}. Text "yes" to accept or "no" to cancel.';	

				}

			}else{

				echo 'Hi '.$view_name.'. You have an appointment with {user} on {date} at {time}. Text "yes" to accept or "no" to cancel.';

			}

		}

	}

	

	public function actionReminderSet($id = "")

	{

		if(!Yii::app()->session['logged_in'])

			$this->redirect(array('site/index'));

		

		$id = $_REQUEST['id'];

		$type = $_REQUEST['type'];

		$reminder_text = $_REQUEST['reminder_text'];

		$connection = Yii::app()->db;

		if($id != ''){

			$sql="UPDATE da_doctor_reminder SET reminder_for = :reminder_for,reminder_text = :reminder_text WHERE id ='".$id."'";

			$params = array(

				"reminder_for" => $type,

				"reminder_text" => $reminder_text

			);

			$command=$connection->createCommand($sql);

			$command->execute($params);

			echo 'ok';

		}else{

			$sql='INSERT INTO da_doctor_reminder (doctor_id, reminder_for, reminder_text, date_created, status) VALUES(:doctor_id,:reminder_for,:reminder_text,:date_created,:status)';

			$params = array(

				"doctor_id" => Yii::app()->session['logged_user_id'],

				"reminder_for" => $type,

				"reminder_text" => $reminder_text,

				"date_created" => date('Y-m-d h:i:s'),

				"status" => 1

			);

			$command=$connection->createCommand($sql);

			$command->execute($params);

			echo Yii::app()->db->getLastInsertId();

		}

	}

	

	public function actionReminderSave($id = "")

	{            

		if(!Yii::app()->session['logged_in'])

			$this->redirect(array('site/index'));

		//Helpers::pre($_REQUEST);

		if(!empty($_REQUEST)){                  

                   

			$connection = Yii::app()->db;

			$sql="UPDATE da_doctor_reminder SET status = :status WHERE doctor_id ='".Yii::app()->session['logged_user_id']."'";

			$params = array(

				"status" => 0

			);

                       

			$command=$connection->createCommand($sql);

			$command->execute($params);

                        

			for( $i=0; $i<count($_REQUEST['reminder_id']); $i++){

				$id = $_REQUEST['reminder_id'][$i];

				$reminder_for = $_REQUEST['reminder_for'][$i];

				$reminder_time = $_REQUEST['reminder_time'][$i];

				$reminder_period = $_REQUEST['reminder_period'][$i];

				

				if($id != ''){                                        

					$sql="UPDATE da_doctor_reminder SET reminder_for = :reminder_for,reminder_time = :reminder_time,reminder_period = :reminder_period,status = :status WHERE id ='".$id."'";

					$params = array(

						"reminder_for" => $reminder_for,

						"reminder_time" => $reminder_time,

						"reminder_period" => $reminder_period,

						"status" => 1

					);

					$command=$connection->createCommand($sql);

					$command->execute($params);

                                         

				}else{

                                     

					$sql='INSERT INTO da_doctor_reminder (doctor_id, reminder_for, reminder_time, reminder_period, date_created, status) VALUES(:doctor_id,:reminder_for,:reminder_time,:reminder_period,:date_created,:status)';

					$params = array(

						"doctor_id" => Yii::app()->session['logged_user_id'],

						"reminder_for" => $reminder_for,

						"reminder_time" => $reminder_time,

						"reminder_period" => $reminder_period,

						"date_created" => date('Y-m-d h:i:s'),

						"status" => 1

					);

					$command=$connection->createCommand($sql);

					$command->execute($params);

				}                               

			}

		}

                else

                {         

                    $connection = Yii::app()->db;                      

                //  To set the sattus of the last reminder to 0                        

                    $sql="UPDATE da_doctor_reminder SET status = :status WHERE doctor_id ='".Yii::app()->session['logged_user_id']."'";

                //  $sql="DELETE FROM da_doctor_reminder WHERE doctor_id ='".Yii::app()->session['logged_user_id']."'";

                    $params = array(

                            "status" => 0

                    );



                    $command=$connection->createCommand($sql);

                    $command->execute($params);  

                    echo 'removed';

                    die();

                }

		echo 'ok';

	}

	

	public function actionOtherSettingSave($id = "")

	{

		if(!Yii::app()->session['logged_in'])

			$this->redirect(array('site/index'));

		

		$id = $_REQUEST['id'];

		$appointment_approval_required = $_REQUEST['appointment_approval_required'];

		$appointment_per_day = $_REQUEST['appointment_per_day'];

		$appointment_canel_period = $_REQUEST['appointment_canel_period'];

		$connection = Yii::app()->db;

		if($id != ''){

			$sql="UPDATE da_doctor_appointment_settings SET appointment_approval_required = :appointment_approval_required,appointment_per_day = :appointment_per_day,appointment_canel_period = :appointment_canel_period WHERE id ='".$id."'";

			$params = array(

				"appointment_approval_required" => $appointment_approval_required,

				"appointment_per_day" => $appointment_per_day,

				"appointment_canel_period" => $appointment_canel_period

			);

			$command=$connection->createCommand($sql);

			$command->execute($params);

			echo 'ok';

		}else{

			$sql='INSERT INTO da_doctor_appointment_settings (doctor_id, appointment_approval_required, appointment_per_day, appointment_canel_period, date_created, status) VALUES(:doctor_id,:appointment_approval_required,:appointment_per_day,:appointment_canel_period,:date_created,:status)';

			$params = array(

				"doctor_id" => Yii::app()->session['logged_user_id'],

				"appointment_approval_required" => $appointment_approval_required,

				"appointment_per_day" => $appointment_per_day,

				"appointment_canel_period" => $appointment_canel_period,

				"date_created" => date('Y-m-d h:i:s'),

				"status" => 1

			);

			$command=$connection->createCommand($sql);

			$command->execute($params);

			echo Yii::app()->db->getLastInsertId();

		}

	}

	

	public function actionNotificationSettingSave($id = "")

	{

		if(!Yii::app()->session['logged_in'])

			$this->redirect(array('site/index'));

		

		$id = $_REQUEST['id'];

		$alert_email = $_REQUEST['alert_email'];

		$email_notification = $_REQUEST['email_notification'];

		$cancel_notification = $_REQUEST['cancel_notification'];

		$confirm_notification = $_REQUEST['confirm_notification'];

		$weekly_notification = $_REQUEST['weekly_notification'];

		$monthly_notification = $_REQUEST['monthly_notification'];

		$connection = Yii::app()->db;

		if($id != ''){

			$sql="UPDATE da_doctor_notification_settings SET alert_email = :alert_email,email_notification = :email_notification,cancel_notification = :cancel_notification,confirm_notification = :confirm_notification,weekly_notification = :weekly_notification,monthly_notification = :monthly_notification,date_modified = :date_modified WHERE id ='".$id."'";

			$params = array(

				"alert_email" => $alert_email,

				"email_notification" => $email_notification,

				"cancel_notification" => $cancel_notification,

				"confirm_notification" => $confirm_notification,

				"weekly_notification" => $weekly_notification,

				"monthly_notification" => $monthly_notification,

				"date_modified" => date('Y-m-d h:i:s')

			);

			$command=$connection->createCommand($sql);

			$command->execute($params);

			echo 'ok';

		}else{

			$sql='INSERT INTO da_doctor_notification_settings (doctor_id, alert_email, email_notification, cancel_notification, confirm_notification, weekly_notification, monthly_notification, date_created, status) VALUES(:doctor_id,:alert_email,:email_notification,:cancel_notification,:confirm_notification,:weekly_notification,:monthly_notification,:date_created,:status)';

			$params = array(

				"doctor_id" => Yii::app()->session['logged_user_id'],

				"alert_email" => $alert_email,

				"email_notification" => $email_notification,

				"cancel_notification" => $cancel_notification,

				"confirm_notification" => $confirm_notification,

				"weekly_notification" => $weekly_notification,

				"monthly_notification" => $monthly_notification,

				"date_created" => date('Y-m-d h:i:s'),

				"status" => 1

			);

			$command=$connection->createCommand($sql);

			$command->execute($params);

			echo Yii::app()->db->getLastInsertId();

		}

	}

	//=================================================================================

	public function actionSpecialOffer($id = "")

	{

		//print_r($_REQUEST);

		if( !isset($_REQUEST['id']) ) {

			$this->redirect(array('404error'));

		}

		$view_ary_offer = array();

		$view_ary_offer = $this->doctorAvailableOffer($_REQUEST['id'],date('Y-m-d'),date('Y-m-d', strtotime(' +2 day')), 'Y-m-d',$_REQUEST['a_id']);

		$_SESSION['fun_var']['Offer'] = $view_ary_offer;

		$_SESSION['fun_var']['val_show'] = 0;

		?>

		<script type="text/javascript">

			<!--

				function fetchValueOffer(val)

					{

						//alert(val);

						url = '<?php echo $this->createAbsoluteUrl("doctor/SpecialOfferPOST/");?>';

						$.post(url, {page:val})

							.done(function(data)

							{

								if(data)

								{

									

									var myArray = data.split('D!l!p');

									//alert(myArray[0]);

									//location.reload();									

									$('#title_val').html(myArray[0]);

									$('#desc_val').html(myArray[1]);

									$('#date_val').html(myArray[2]);

									$('#menu_cont').html(myArray[3]);

								}

							});

					}

			//-->

			</script>



			<span class="button b-close"></span>

			<div class="profilesec_timetable_offerbox">

				

				<?php

					if($view_ary_offer)

					{	

					$total_count = count($view_ary_offer);

				?>

			<!-- <ul id="pagination3"> -->

			<?php

			//for($i=0;$i<=count($view_ary_offer)-1;$i++)

						//{

			?>				

				<!-- <li> -->

				

					<div class="offer1">

						<h2 id="title_val"><?=$view_ary_offer[0]['name']?></h2>

						<p id="desc_val"><?=$view_ary_offer[0]['description']?></p>

						<p style="text-align:center;font-weight:bold;" id="date_val">

							Valid From :

						<?php

						 	/*if(date('j') < date('j',strtotime($view_ary_offer[0]['from_date']))){

								echo date('M j',strtotime($view_ary_offer[0]['from_date'])); ?> - <?php echo date('M j',strtotime($view_ary_offer[0]['to_date']));

							 }else{

								 echo 'Upto '.date('M j',strtotime($view_ary_offer[0]['to_date']));

							 }*/

							echo date('M dS, Y',strtotime($view_ary_offer[0]['from_date'])); ?> - To : <?php echo date('M dS, Y',strtotime($view_ary_offer[0]['to_date']));

						?>

						</p>					

						</div>

					<!-- </li> -->

					

					<div id="menu_cont">						

						<?php

							if($total_count>1)

							{

								?>

								<span class="right">

									<a href="javascript:void(0);" onclick="fetchValueOffer('next');">Next</a>

								</span>

								<?php

							}

						?>

					</div>

				<?php

				//}

				?>

				<!-- </ul> -->

				

				<?php

					}else{

						?>

							<div class="offer1">

								<h2>No Special Offer Available.</h2>

							</div>

						<?php

					}

				?>

					

			</div>

			

			<?

	}



	public function actionSpecialOfferPOST($id = "")

	{

		//print_r($_REQUEST);

		$key = $_SESSION['fun_var']['val_show'];

		//===========================================================================

		if($_REQUEST['page']=='next')

		{

			if($_SESSION['fun_var']['val_show']!=count($_SESSION['fun_var']['Offer'])-1)

			{

				$key = $_SESSION['fun_var']['val_show'] + 1;

			}

			

		}else{

			if($_SESSION['fun_var']['val_show']!=0)

			{

				$key = $_SESSION['fun_var']['val_show'] - 1;

			}

		}

		$_SESSION['fun_var']['val_show'] = $key;

		$date_field = '';

		//==========================================================



		

		//==========================================================

		print $_SESSION['fun_var']['Offer'][$key]['name'];

		print "D!l!p";

		print $_SESSION['fun_var']['Offer'][$key]['description'];

		print "D!l!p";

		print $date_field;

		echo "Valid From : ".date('M dS, Y',strtotime($_SESSION['fun_var']['Offer'][$key]['from_date'])); ?> - To : <?php echo date('M dS, Y',strtotime($_SESSION['fun_var']['Offer'][$key]['from_date']));

		print "D!l!p";

		if ( count($_SESSION['fun_var']['Offer'])-1 > $key ) {

			if( $key == 0 )  {

				print '

					<span class="left">

						&nbsp;

					</span>

					<span class="right">

						<a href="javascript:void(0);" onclick="fetchValueOffer(\'next\');">Next</a>

					</span>

				';

			} else {

				print '

					<span class="left">

						<a href="javascript:void(0);" onclick="fetchValueOffer(\'previous\');">Previous</a>

					</span>

					<span class="right">

						<a href="javascript:void(0);" onclick="fetchValueOffer(\'next\');">Next</a>

					</span>

				';

			}	

		} else if ( count($_SESSION['fun_var']['Offer'])-1 == $key ) {

			print '

					<span class="left">

						<a href="javascript:void(0);" onclick="fetchValueOffer(\'previous\');">Previous</a>

					</span>

					<!--

					<span class="right">

						<a href="javascript:void(0);" onclick="javascript: void(0);" >Next</a>

					</span>

					-->

				';

		}

		//===========================================================================

		

	}

	public function actionEmailSurvey() {

		$connection = Yii::app()->db;

		$overall_rating = $_REQUEST['overall_rating'];

		$bedside_manner_rating = $_REQUEST['bedside_manner_rating'];

		$wait_time_rating = $_REQUEST['wait_time_rating'];

		$scheduling_appointment=$_REQUEST['scheduling_appointment'];

		$office_experience=$_REQUEST['office_experience'];

		$spent_time=$_REQUEST['spent_time'];

		

		$review_message = $_REQUEST['review_message'];

		$patient_id = $_REQUEST['patient_id'];

		$doctor_id = $_REQUEST['doctor_id'];

		$ri = $_REQUEST['ri'];

		$status = 1;

		

		$sqlSurvey='select * FROM da_doctor_review WHERE `appointment_id`="'.$ri.'" AND `doctor_id`="'.$doctor_id.'" AND `patient_id`="'.$patient_id.'"';

		$commandSurvey = $connection->createCommand($sqlSurvey);

		$user_surveyReview = $commandSurvey->queryAll();

		if( count( $user_surveyReview ) == 0 ) {

			$sql='INSERT INTO da_doctor_review (doctor_id, patient_id, appointment_id, overall_rating, bedside_manner_rating, wait_time_rating,schedul_appoitment_rating,office_experience_rating,spent_time_rating , message, date_created, status) VALUES(:doctor_id,:patient_id,:appointment_id,:overall_rating,:bedside_manner_rating,:wait_time_rating,:schedul_appoitment,

					:office_experience,:spent_time,:message,:date_created,:status)';

			$params = array(

					"doctor_id" => $doctor_id,

					"patient_id" => $patient_id,

					"appointment_id" => $ri,

					"overall_rating" => $overall_rating,

					"bedside_manner_rating" => $bedside_manner_rating,

					"wait_time_rating" => $wait_time_rating,

					"schedul_appoitment"=>$scheduling_appointment,

					"office_experience"=>$office_experience,

					"spent_time"=> $spent_time,

					"message" => $review_message,

					"date_created" => date('Y-m-d h:i:s'),

					"status" => $status

			);

			

			$command=$connection->createCommand($sql);

			$command->execute($params);

			echo 1;

		} else {

			echo 0;

		}		

	}

	

	public function actionSurveyEmailRating() {

		$doctor_details = array();

		$doctor_det = $model = Doctor::model()->findByPk( $_GET['doc_id'] );

		if( count( $doctor_det ) > 0 ) {

			$doctor_details = array('doctor_id' => $doctor_det->id,'doctor_full_name'=> $doctor_det->full_name ,'doctor_slug' => $doctor_det->slug,'doctor_title' => $doctor_det->title);

		}

			

		$rating_val = array('ratingVal' => $_GET['rating'],'doctor_id' => $_GET['doc_id'], 'patient_id' => $_GET['p_id'],'appointment_id' => $_GET['ri']);

		$data = array();

		$this->render('survey_email_rating',array(

			'data'=>$data, 'rating_val' => $rating_val,'doctor_details' => $doctor_details,

		));

	}

	

	public function actionCompose() {

		$data = array();

		$this->render('compose_mail',array(

			'data'=>$data,

		));

	}

	

	public function actioninbox() {

		$data = array();

		$this->render('inbox',array(

			'data'=>$data,

		));

	}

	

	public function actionsentMail() {

		$data = array();

		$this->render('sent_mail',array(

			'data'=>$data,

		));

	}


	/*-- 26.5.2015 : Ne method for search filed drop down --*/
	public function actiongetAllInsurance(){
		$connection = Yii::app()->db;
		$sql_insurance='select * FROM da_insurance WHERE status="1"';
		$command_insurance = $connection->createCommand($sql_insurance);
		$user_insurance = $command_insurance->queryAll();
		$user_insurance_res = array();
		foreach ($user_insurance as $key=>$val) {
			$user_insurance_res[$val['id']] = $val['insurance'];
		}
	
		echo json_encode($user_insurance_res);
	
	}
	
	
	public function actiongetAllSpeciality(){
		$sql_spl='select * FROM da_speciality WHERE status="1"';
		$connection = Yii::app()->db;
		$command = $connection->createCommand($sql_spl);
		$user_speciality = $command->queryAll();
		$user_speciality_res = array();
		foreach ($user_speciality as $key=>$val) {
			$user_speciality_res[$val['id']] = $val['speciality'];
		}
		echo json_encode($user_speciality_res);
	}
	 
	public function actiongetAllLanguage (){
		$connection = Yii::app()->db;
		$sql_language='select * FROM da_language WHERE status="1"';
		$command_language = $connection->createCommand($sql_language);
		$user_language = $command_language->queryAll();
		$user_language_res = array();
		foreach ($user_language as $key=>$val) {
			$user_language_res[$val['id']] = $val['language'];
		}
		echo json_encode($user_language_res);
	}
	
	 
	public  function actiongetAllProcedure ($docid=""){
		$connection = Yii::app()->db;
		$condition="";
		if(!empty($docid)) {$condition=" and doctor_id='".$doc_id."'";}
		$sql_procedure="select * FROM da_procedure WHERE status='1'".$condition;
		$command_procedure = $connection->createCommand($sql_procedure);
		$user_procedure = $command_procedure->queryAll();
		$user_procedure_res = array();
		foreach ($user_procedure as $key=>$val) {
			$user_procedure_res[$val['id']] = $val['procedure'];
		}
	
		echo json_encode($user_procedure_res);
	}
	public  function actiongetAllReasonVisit (){
		$connection = Yii::app()->db;
		$sql_reason_for_visit='select * FROM da_reason_for_visit WHERE status="1"';
		$command_reason_for_visit = $connection->createCommand($sql_reason_for_visit);
		$user_reason_for_visit = $command_reason_for_visit->queryAll();
		$user_reason_for_visit_res = array();
		foreach ($user_reason_for_visit as $key=>$val) {
			$user_reason_for_visit_res[$val['id']] = $val['reason_for_visit'];
		}
	
		echo json_encode($user_reason_for_visit_res);
	}
	 
	public  function actionCheckZip (){
		$zipcode =Yii::app()->request->getPost('zip');
		 
		 
		$url="http://maps.googleapis.com/maps/api/geocode/json?address=". $zipcode;
		 
		$curl = curl_init();
		curl_setopt_array($curl,
		array( CURLOPT_RETURNTRANSFER => 1,
		CURLOPT_URL => $url)
		);
	
		$result = curl_exec($curl);
		$decoded_result = json_decode($result, true);
			
		if($decoded_result['status']=="OK"){
			echo "0";
	
		}
		else {echo "1";}
		/*  $url = 'http://www.zipcodeapi.com/rest/';
		 $key = 'v1Eq0UIV4lhIfA2qENPNDovIqs816OfBQU1QOKMT54ClEEMjsB750COq5XsbLIMB';
	
	
		$curl = curl_init();
		curl_setopt_array($curl,
				array( CURLOPT_RETURNTRANSFER => 1,
						CURLOPT_URL => $url.'/'.$key.'/info.json/'.$zipcode.'/degrees')
		);
	
		$result = curl_exec($curl);
	
		$decoded_result = json_decode($result, true);
		//print_r($decoded_result);
		if(array_key_exists('error_code',$decoded_result))
		{
		echo "0";
		}
		else {
		echo "1";
		}*/
	}
	 
	 
	/*public function doctorSchedualByPlace($docid){
	 $connection = Yii::app()->db;
	$sql='SELECT sc.doctor_id, sc.address_id, sc.to_date, sc.from_date, sc.id AS scdid, scd . *
	FROM da_doctor_schedule sc
	INNER JOIN  `da_doctor_schedule_time` scd ON ( sc.id = scd.schedule_id )
	WHERE sc.status =  "1"
	AND sc.doctor_id =185
	AND sc.address_id =131
	AND scd.on_off =1';
	$command = $connection->createCommand($sql);
	$result = $command->queryAll();
	
	}*/
	/*-- 26.5.2015 : Ne method for search filed drop down --*/
	
	public function getSpecialIdName($id){
		$user_speciality = Speciality::model()->findAll(array(
				'condition' => 'status = :status AND id = :id',
				'params' => array(':status' => '1', 'id' => $id),
		));
		$spcialityName = "";
		if (count($user_speciality) > 0) {
			$spcialityName = $user_speciality[0]->speciality;
		}
		return $spcialityName;
	}
	
	public function actionScheduleTableMob() {
		$indexCount= $_REQUEST['index'];
		
		/*$docId = $_REQUEST['docId'];
		$specialId = $_REQUEST['specialId'];
		$docAddId = $_REQUEST['docAddId'];
		$procedureId = $_REQUEST['procedureId'];
		$today = date('Y-m-d');*/
		
		$today_date  = date('Y-m-d', strtotime("+ 1 day"));
		$view_date  = date('Y-m-d', strtotime("+ 1 day"));		
		$doctor_id = $_REQUEST['docId'];
		$speciality_id = $_REQUEST['specialId'];
		$procedure_id =  $_REQUEST['procedureId'];
		$doctor_address_id = $_REQUEST['docAddId'];
		$str_str = '';
	
		$arr[]='<input type="hidden" id="speciality_id_'.$indexCount.'" value="'.$speciality_id.'" />
				<input type="hidden" id="procedure_id'.$indexCount.'" value="'.$procedure_id.'" />
				<input type="hidden" id="doctor_id'.$indexCount.'" value="'.$doctor_id.'" />
				<input type="hidden" id="today_date'.$indexCount.'" value="'.date('Y-m-d', strtotime("+ 1 day")).'" />
				<input type="hidden" id="view_date'.$indexCount.'" value="'.date('Y-m-d', strtotime("+ 1 day")).'" />
				<input type="hidden" id="index_id_pop'.$indexCount.'" value="'.$indexCount.'" />
				<input type="hidden" id="address_id'.$indexCount.'" value="'.$doctor_address_id.'" />';
		
		$arr[]=' <div class="calander_menu" id="cAreaMenu_'.$indexCount.'" >
		         	<ul>
		            	<li><font style="font-weight:700; font-size:12px;">'.date('l', time() + 86400).', '.date('F d,Y', time() + 86400).'</font></li>
		            </ul>
		         </div>';
		$arr[]= '<div class="nxt_pre_arrow_area">
						<div class="pre_arrow_area" onclick="appointmentScheduleMob(\'prev\','.$doctor_address_id.','.$indexCount.');">
							<img src="'.Yii::app()->request->baseUrl.'/assets/images/pre_arrow_mob-light.png" style="cursor:pointer;"/>
						</div>
						 <div id="schedule_loader_prev_'.$doctor_address_id.'" class="schedule_loader" style="position:absolute; top:11px; z-index:999; display:none;">
							<img src="'.Yii::app()->request->baseUrl.'/assets/images/loader.gif" />
						</div>
						<div class="nxt_arrow_area" onclick="appointmentScheduleMob(\'next\','.$doctor_address_id.','.$indexCount.');">
							<img src="'.Yii::app()->request->baseUrl.'/assets/images/nxt_arrow_mob-dark.png" style="cursor:pointer;"/>
						</div>
						<div id="schedule_loader_next_'.$doctor_address_id.'" class="schedule_loader" style="position:absolute; top:11px; z-index:999; display:none;">
							<img src="'.Yii::app()->request->baseUrl.'/assets/images/loader.gif" />
						</div>
		         </div>';
		
		$view_ary_sch = $this->doctorSchTime($doctor_id,$today_date,$view_date, 'Y-m-d',$doctor_address_id);
		$doctorAppTime = $this->doctorAppTimeMore($doctor_id,$today_date,$view_date, 'Y-m-d',$doctor_address_id);
		$doctorTimeOff = $this->doctorTimeOffDay($doctor_id,$today_date,$view_date, 'Y-m-d');
		//Helpers::pre($view_ary_sch);
		if(!empty($view_ary_sch[$doctor_id])){
			
			foreach($view_ary_sch[$doctor_id] as $view_ary_sch_key=>$view_ary_sch_val){
				 
				if(!empty($view_ary_sch_val)){
					$view_ary_sch_time_cnt=false;
					foreach($view_ary_sch_val as $view_ary_sch_time){
						if(isset($view_ary_sch_time) && !empty($view_ary_sch_time))
							$view_ary_sch_time_cnt=true;
					}
					if($view_ary_sch_time_cnt){
		
						$arr[]='<div class="calander_box_lisiting_popup" id="cArea_'.$indexCount.'" >';
		
						$tot_cnt=0;$inner_five = 0;
						foreach($view_ary_sch_val as $view_ary_sch_time_val){
							foreach($view_ary_sch_time_val as $view_ary_sch_time_val_arr){
								if($view_ary_sch_time_val_arr['day']==date('l',strtotime($view_ary_sch_key))){
									//if($view_ary_sch_time_val_arr['on_off']==1){
										$view_ary_to_strtotime=$view_ary_sch_time_val_arr['to_strtotime'];
										$view_ary_from_strtotime=$view_ary_sch_time_val_arr['from_strtotime'];
										$time_diff = $view_ary_to_strtotime-$view_ary_from_strtotime;
										$view_ary_time_slot=$view_ary_sch_time_val_arr['time_slot']*60;
										$view_ary_laser_slot=$view_ary_sch_time_val_arr['laser_slot']*60;
											
										$time_length = ($view_ary_to_strtotime-$view_ary_from_strtotime)/(($view_ary_sch_time_val_arr['time_slot']+$view_ary_sch_time_val_arr['laser_slot']*60));
									if($view_ary_sch_time_val_arr['on_off']==1){
										for($iCnt=0;$iCnt<$time_length;$iCnt++){
											$added_time = $view_ary_time_slot+$view_ary_laser_slot;
											$show_time=$view_ary_from_strtotime+($added_time*$iCnt);
											if($show_time < $view_ary_to_strtotime){
												$tot_cnt++;$inner_five++;
												if($iCnt==0){
													if(isset($doctorAppTime[$doctor_id][$view_ary_sch_key])&&in_array($view_ary_sch_time_val_arr['from_strtotime'],$doctorAppTime[$doctor_id][$view_ary_sch_key])){
														$arr[]='<span class="innertable_time_popup disable"><a class="blank" href="javascript:void(0);">'.date('h:i  a',$view_ary_sch_time_val_arr['from_strtotime']).'</a></span>';
													}else{
														if(isset($doctorTimeOff[$doctor_id][$view_ary_sch_key]) && $view_ary_sch_time_val_arr['from_strtotime']>=$doctorTimeOff[$doctor_id][$view_ary_sch_key][0]['strt_time_off'] && $view_ary_sch_time_val_arr['from_strtotime']<=$doctorTimeOff[$doctor_id][$view_ary_sch_key][0]['to_time_off']){
															$arr[]='<span class="innertable_time_popup disable"><a class="blank" href="javascript:void(0);">'.date('h:i  a',$view_ary_sch_time_val_arr['from_strtotime']).'</a></span>';
														}else{
															$arr[]='<span class="innertable_time_popup"><a href="'.$this->createAbsoluteUrl('doctor/DoctorBookStep1/?start_time='.$view_ary_sch_time_val_arr['from_strtotime'].'&doctor_id='.$doctor_id.'&address_id='.$view_ary_sch_time_val_arr['address_id'].'&speciality_id='.$speciality_id).'&time_slot='.$view_ary_time_slot.'&procedure_id='.$procedure_id.'">'.date('h:i  a',$view_ary_sch_time_val_arr['from_strtotime']).'</a></span>';
														}
													}
												}else{
													if(isset($doctorAppTime[$doctor_id][$view_ary_sch_key])&&in_array($show_time,$doctorAppTime[$doctor_id][$view_ary_sch_key])){
														$arr[]='<span class="innertable_time_popup disable"><a class="blank" href="javascript:void(0);">'.date('h:i  a',$show_time).'</a></span>';
													}else{
														if(isset($doctorTimeOff[$doctor_id][$view_ary_sch_key]) && $show_time>=$doctorTimeOff[$doctor_id][$view_ary_sch_key][0]['strt_time_off'] && $show_time<=$doctorTimeOff[$doctor_id][$view_ary_sch_key][0]['to_time_off']){
															$arr[]='<span class="innertable_time_popup disable"><a class="blank" href="javascript:void(0);">'.date('h:i  a',$show_time).'</a></span>';
														}else{
															$arr[]='<span class="innertable_time_popup"><a href="'.$this->createAbsoluteUrl('doctor/DoctorBookStep1/?start_time='.$show_time.'&time_slot='.$view_ary_time_slot.'&doctor_id='.$doctor_id.'&address_id='.$view_ary_sch_time_val_arr['address_id'].'&speciality_id='.$speciality_id).'&procedure_id='.$procedure_id.'">'.date('h:i a',$show_time).'</a></span>';
														}
													}
												}
											}
										}
									}else{
										/* -------  Disable Part ----------- */
										for($iCnt=0;$iCnt<$time_length;$iCnt++){	
											$added_time = $view_ary_time_slot+$view_ary_laser_slot;
											$show_time=$view_ary_from_strtotime+($added_time*$iCnt);
											if($show_time < $view_ary_to_strtotime){
												if($iCnt==0){
													$arr[]='<span class="innertable_time_popup disable" style="background:#cdcdcd;" ><a class="blank" href="javascript:void(0);">'.date('h:i  a',$view_ary_sch_time_val_arr['from_strtotime']).'</a></span>';
												} else {
													$arr[]='<span class="innertable_time_popup disable" style="background:#cdcdcd;" ><a class="blank" href="javascript:void(0);">'.date('h:i  a',$show_time).'</a></span>';
												}		
											}										
										}
										/* --------------- Disable Part End --------------- */
									}
								}
							}
						}
		
						$arr[]='</div>';
		
					}
				}
			}
		}
		echo implode("",$arr);
		
	}
	
	public function actionAppointmentScheduleMob(){
		if( !isset($_REQUEST['today_date']) ) {
			$this->redirect(array('404error'));
		}
		//$today_date  = $_REQUEST['today_date'];
		$today_date  = $_REQUEST['view_date'];
		$view_date  = $_REQUEST['view_date'];
		$type= Yii::app()->request->getParam('type');
		$doctor_id = Yii::app()->request->getParam('doctor_id');
		$speciality_id = Yii::app()->request->getParam('speciality_id');
		$procedure_id = Yii::app()->request->getParam('procedure_id');
		$doctor_address_id = Yii::app()->request->getParam('address_id');
		$indexCount = Yii::app()->request->getParam('index_id_pop');
	
		$usercriteria = array();
		
		if($type == 'next'){
			$from_view_date = strtotime($view_date);
			$mk_from_time = date("Y-m-d", mktime(0,0,0,date('m',$from_view_date),date('d',$from_view_date)+1,date('Y',$from_view_date)));
			$to_view_date = strtotime($mk_from_time);
			$mk_to_time = date("Y-m-d", mktime(0,0,0,date('m',$to_view_date),date('d',$to_view_date)+intVal(0),date('Y',$to_view_date)));
		}else{
			$from_view_date = strtotime($view_date);
			$mk_from_time = date("Y-m-d", mktime(0,0,0,date('m',$from_view_date),date('d',$from_view_date)-intVal(1),date('Y',$from_view_date)));
			$to_view_date = strtotime($mk_from_time);
			$mk_to_time = date("Y-m-d", mktime(0,0,0,date('m',$to_view_date),date('d',$to_view_date)+intVal(0),date('Y',$to_view_date)));
		}
	
		$str_str_head = '';
		$str_str_head .= '<ul>
						  <li>';
		$str_str_head .= '<span style="font-weight:700; font-size:12px;">';
		$str_str_head .= date('l', strtotime($mk_from_time));
		$str_str_head .= ',';
		$str_str_head .= date('F d,Y', strtotime($mk_from_time));
		$str_str_head .= '</span></li>
						  <li></ul>';
	
		$str_str = '';
		$sch_width = "width: 20%";
		if( Yii::app()->request->getParam('sch_width') ) {
			$sch_width = "width: 14%";
		}

		$view_ary_sch = $this->doctorSchTime($doctor_id,$mk_from_time,$mk_to_time, 'Y-m-d',$doctor_address_id);
		$doctorAppTime = $this->doctorAppTime($doctor_id,$mk_from_time,$mk_to_time, 'Y-m-d',$doctor_address_id);
		$doctorTimeOff = $this->doctorTimeOff($doctor_id,$mk_from_time,$mk_to_time, 'Y-m-d');
		
		//Helpers::pre($view_ary_sch);
		if(!empty($view_ary_sch[$doctor_id])){
			
			foreach($view_ary_sch[$doctor_id] as $view_ary_sch_key=>$view_ary_sch_val){
				 
				if(!empty($view_ary_sch_val)){
					$view_ary_sch_time_cnt=false;
					foreach($view_ary_sch_val as $view_ary_sch_time){
						if(isset($view_ary_sch_time) && !empty($view_ary_sch_time))
							$view_ary_sch_time_cnt=true;
					}
					if($view_ary_sch_time_cnt){
		
						$str_str .='<div class="" id="calander_box_lisiting_popup_mob" >';
		
						$tot_cnt=0;$inner_five = 0;
						foreach($view_ary_sch_val as $view_ary_sch_time_val){
							foreach($view_ary_sch_time_val as $view_ary_sch_time_val_arr){
								if($view_ary_sch_time_val_arr['day']==date('l',strtotime($view_ary_sch_key))){
									//if($view_ary_sch_time_val_arr['on_off']==1){
										$view_ary_to_strtotime=$view_ary_sch_time_val_arr['to_strtotime'];
										$view_ary_from_strtotime=$view_ary_sch_time_val_arr['from_strtotime'];
										$time_diff = $view_ary_to_strtotime-$view_ary_from_strtotime;
										$view_ary_time_slot=$view_ary_sch_time_val_arr['time_slot']*60;
										$view_ary_laser_slot=$view_ary_sch_time_val_arr['laser_slot']*60;
											
										$time_length = ($view_ary_to_strtotime-$view_ary_from_strtotime)/(($view_ary_sch_time_val_arr['time_slot']+$view_ary_sch_time_val_arr['laser_slot']*60));
									if($view_ary_sch_time_val_arr['on_off']==1){
										for($iCnt=0;$iCnt<$time_length;$iCnt++){
											$added_time = $view_ary_time_slot+$view_ary_laser_slot;
											$show_time=$view_ary_from_strtotime+($added_time*$iCnt);
											if($show_time < $view_ary_to_strtotime){
												$tot_cnt++;$inner_five++;
												if($iCnt==0){
													if(isset($doctorAppTime[$doctor_id][$view_ary_sch_key])&&in_array($view_ary_sch_time_val_arr['from_strtotime'],$doctorAppTime[$doctor_id][$view_ary_sch_key])){
														$str_str .='<span class="innertable_time_popup disable"><a class="blank" href="javascript:void(0);">'.date('h:i  a',$view_ary_sch_time_val_arr['from_strtotime']).'</a></span>';
													}else{
														if(isset($doctorTimeOff[$doctor_id][$view_ary_sch_key]) && $view_ary_sch_time_val_arr['from_strtotime']>=$doctorTimeOff[$doctor_id][$view_ary_sch_key][0]['strt_time_off'] && $view_ary_sch_time_val_arr['from_strtotime']<=$doctorTimeOff[$doctor_id][$view_ary_sch_key][0]['to_time_off']){
															$str_str .='<span class="innertable_time_popup disable"><a class="blank" href="javascript:void(0);">'.date('h:i  a',$view_ary_sch_time_val_arr['from_strtotime']).'</a></span>';
														}else{
															$str_str .='<span class="innertable_time_popup"><a href="'.$this->createAbsoluteUrl('doctor/DoctorBookStep1/?start_time='.$view_ary_sch_time_val_arr['from_strtotime'].'&doctor_id='.$doctor_id.'&address_id='.$view_ary_sch_time_val_arr['address_id'].'&speciality_id='.$speciality_id).'&time_slot='.$view_ary_time_slot.'&procedure_id='.$procedure_id.'">'.date('h:i  a',$view_ary_sch_time_val_arr['from_strtotime']).'</a></span>';
														}
													}
												}else{
													if(isset($doctorAppTime[$doctor_id][$view_ary_sch_key])&&in_array($show_time,$doctorAppTime[$doctor_id][$view_ary_sch_key])){
														$str_str .='<span class="innertable_time_popup disable"><a class="blank" href="javascript:void(0);">'.date('h:i  a',$show_time).'</a></span>';
													}else{
														if(isset($doctorTimeOff[$doctor_id][$view_ary_sch_key]) && $show_time>=$doctorTimeOff[$doctor_id][$view_ary_sch_key][0]['strt_time_off'] && $show_time<=$doctorTimeOff[$doctor_id][$view_ary_sch_key][0]['to_time_off']){
															$str_str .='<span class="innertable_time_popup disable"><a class="blank" href="javascript:void(0);">'.date('h:i  a',$show_time).'</a></span>';
														}else{
															$str_str .='<span class="innertable_time_popup"><a href="'.$this->createAbsoluteUrl('doctor/DoctorBookStep1/?start_time='.$show_time.'&time_slot='.$view_ary_time_slot.'&doctor_id='.$doctor_id.'&address_id='.$view_ary_sch_time_val_arr['address_id'].'&speciality_id='.$speciality_id).'&procedure_id='.$procedure_id.'">'.date('h:i a',$show_time).'</a></span>';
														}
													}
												}
											}
										}
									}else{
										/* -------  Disable Part ----------- */
										for($iCnt=0;$iCnt<$time_length;$iCnt++){	
											$added_time = $view_ary_time_slot+$view_ary_laser_slot;
											$show_time=$view_ary_from_strtotime+($added_time*$iCnt);
											if($show_time < $view_ary_to_strtotime){
												if($iCnt==0){
													$str_str .='<span class="innertable_time_popup disable" style="background:#cdcdcd;" ><a class="blank" href="javascript:void(0);">'.date('h:i  a',$view_ary_sch_time_val_arr['from_strtotime']).'</a></span>';
												} else {
													$str_str .='<span class="innertable_time_popup disable" style="background:#cdcdcd;" ><a class="blank" href="javascript:void(0);">'.date('h:i  a',$show_time).'</a></span>';
												}
												//$str_str .='<span class="innertable_time_popup disable" style="background:#cdcdcd;" ><a class="blank" href="javascript:void(0);">'.date('h:i  a',$view_ary_sch_time_val_arr['from_strtotime']).'</a></span>';
											}										
										}
										/* --------------- Disable Part End --------------- */
									}
								}
							}
						}
		
						$str_str .= '</div>';
		
					}
				}
			}
		}
		echo $str_str_head.'***%%%***'.$str_str.'***%%%***'.$mk_from_time;
	}
	
/* ----------------------- Doctor Profile For Mob ----------------------------------- */
	
	public function actionSearchDoctorProfileMob($slug = "") {
		$addressId = 0;
		if( isset( $slug ) ) {
			$slugArr = explode( "|" ,$slug );
			$slug = $slugArr[0];
			$specialityID = $slugArr[1];
			if( isset( $slugArr[2] ) ) {
				$addressId = $slugArr[2];
			}
		}
		$activeTab = "viewPf";
		if(in_array("reviewPf",$slugArr)) {
			$activeTab = "reviewPf";
		}
		$modelDoctorData = Doctor::model()->findByAttributes(array('slug' => $slug));
		if($modelDoctorData && $modelDoctorData->count() > 0){
			$id = $modelDoctorData->id;
			$doctor_list =Doctor::model()->find("id=\"$id\"");
			$sql='select * FROM da_speciality WHERE status="1"';
			$connection = Yii::app()->db;
			$command = $connection->createCommand($sql);
			$user_speciality = $command->queryAll();//print_r($user_speciality);
			$user_speciality_res = array();
			foreach ($user_speciality as $key=>$val) {
				$user_speciality_res[$val['id']] = $val['speciality'];
			}

			$sql_multy_speciality='select * FROM da_doctor_speciality WHERE status="1" and doctor_id="'.$id.'"';
			$command_multy_speciality = $connection->createCommand($sql_multy_speciality);
			$user_multy_speciality = $command_multy_speciality->queryAll();
			$user_selected_speciality = array();
			foreach ($user_multy_speciality as $key=>$val) {
				$user_selected_speciality[] = $val['speciality_id'];
			}
	
			$sql_condition='select * FROM da_condition WHERE status="1"';
			$command_condition = $connection->createCommand($sql_condition);
			$user_condition = $command_condition->queryAll();
			$user_condition_res = array();
			foreach ($user_condition as $key=>$val) {
				$user_condition_res[$val['id']] = $val['condition'];
			}
	
			$sql_multy_condition='select * FROM da_doctor_condition WHERE status="1" and doctor_id="'.$id.'"';
			$command_multy_condition = $connection->createCommand($sql_multy_condition);
			$user_multy_condition = $command_multy_condition->queryAll();
			$user_selected_condition = array();
			foreach ($user_multy_condition as $key=>$val) {
				$user_selected_condition[] = $val['condition_id'];
			}
	
			$sql_procedure='select * FROM da_procedure WHERE status="1"';
			$command_procedure = $connection->createCommand($sql_procedure);
			$user_procedure = $command_procedure->queryAll();
			$user_procedure_res = array();
			foreach ($user_procedure as $key=>$val) {
				$user_procedure_res[$val['id']] = $val['procedure'];
			}
	
			$sql_multy_procedure='select * FROM da_doctor_procedure WHERE status="1" and doctor_id="'.$id.'"';
			$command_multy_procedure = $connection->createCommand($sql_multy_procedure);
			$user_multy_procedure = $command_multy_procedure->queryAll();
			$user_selected_procedure = array();
			foreach ($user_multy_procedure as $key=>$val) {
				$user_selected_procedure[] = $val['procedure_id'];
			}
	
			$sql_language='select * FROM da_language WHERE status="1"';
			$command_language = $connection->createCommand($sql_language);
			$user_language = $command_language->queryAll();
			$user_language_res = array();
			foreach ($user_language as $key=>$val) {
				$user_language_res[$val['id']] = $val['language'];
			}
		
			$sql_multy_language='select * FROM da_doctor_language WHERE status="1" and doctor_id="'.$id.'"';
			$command_multy_language = $connection->createCommand($sql_multy_language);
			$user_multy_language = $command_multy_language->queryAll();
			$user_selected_language = array();
			foreach ($user_multy_language as $key=>$val) {
				$user_selected_language[] = $val['language_id'];
			}
			if( $addressId != 0 ) {
				$sql_address='select * FROM da_doctor_address WHERE status="1" and id="'.$addressId.'"';
			} else {
				$sql_address='select * FROM da_doctor_address WHERE status="1" and doctor_id="'.$id.'"';
			}
	
			$command_address = $connection->createCommand($sql_address);
			$user_address = $command_address->queryAll();
			$user_selected_address = array();
			foreach ($user_address as $key=>$val) {
				$user_selected_address[] = $val['address'];
			}
	
			$default_data_address = DoctorAddress::model()->findByAttributes(array('doctor_id'=>$id,'status'=>1,'default_status'=>1));
			$doctor_review_condition = 't.status = 1 and doctor_id =' . $id;
			$doctor_review_criteria = new CDbCriteria(array(
					'condition' => $doctor_review_condition,
					'order' => 't.id DESC',
					'limit' => 5
			));
	
			$doctor_review=DoctorReview::model()->with('patient_details')->findall($doctor_review_criteria);
			//Helpers::pre($doctor_review);die;
			$condition_video = 'status = "1" and default_status = "1" and doctor_id =' . $id;
			$criteria_video = new CDbCriteria(array(
					'condition' => $condition_video,
					'order' => 'id DESC'
			));
	
			$data_video =DoctorVideo::model()->findAll($criteria_video);
	
			$sql_multy_insurance='select * FROM da_doctor_insurance WHERE status="1" and doctor_id="'.$id.'"';
			$command_multy_insurance = $connection->createCommand($sql_multy_insurance);
			$user_multy_insurance = $command_multy_insurance->queryAll();
			$user_selected_insurance = array();
			foreach ($user_multy_insurance as $key=>$val) {
				$user_selected_insurance[] = $val['insurance_id'];
			}
			$user_insurance_plan_res = array();
			$user_insurance_plan_res_ins = array();
			$user_selected_insurance_plan_ins = array();
			foreach ($user_selected_insurance as $insurance_key=>$insurance_val) {
				$sql_insurance_plan='select * FROM da_insurance_plan WHERE status="1" and insurance_id="'.$insurance_val.'"';
				$command_insurance_plan = $connection->createCommand($sql_insurance_plan);
				$user_insurance_plan = $command_insurance_plan->queryAll();
				$user_insurance_plan_res = array();
				foreach ($user_insurance_plan as $key=>$val) {
					$user_insurance_plan_res[$val['id']] = $val['plan'];
				}
				$user_insurance_plan_res_ins[$insurance_val] = $user_insurance_plan_res;
	
				$sql_multy_insurance_plan='select * FROM da_doctor_insurance_plan WHERE status="1" and doctor_id="'.$id.'" and insurance_id="'.$insurance_val.'"';
				$command_multy_insurance_plan = $connection->createCommand($sql_multy_insurance_plan);
				$user_multy_insurance_plan = $command_multy_insurance_plan->queryAll();
				$user_selected_insurance_plan = array();
				foreach ($user_multy_insurance_plan as $key=>$val) {
					$user_selected_insurance_plan[] = InsurancePlan::model()->getInsurancePlanNameById( $val['plan_id'] );
				}
				$user_selected_insurance_plan_ins[$insurance_val] = $user_selected_insurance_plan;
			}
			//Yii::app()->session['specialityChooseByUser'] = $page_fullUrl;
			$this->render('search_doctor_profile_mob',array(
					'doctor_list' => $doctor_list,'user_speciality' => $user_speciality_res,'user_selected_speciality' => $user_selected_speciality,'user_condition' => $user_condition_res,'user_selected_condition' => $user_selected_condition,'user_language' => $user_language_res,'user_selected_language' => $user_selected_language,'user_address' => $user_address,'doctor_review' => $doctor_review,'data_video' => $data_video,'user_procedure' => $user_procedure_res,'user_selected_procedure' => $user_selected_procedure,'default_data_address' => $default_data_address,'user_selected_insurance_plan_ins' => $user_selected_insurance_plan_ins,'specialityID' => $specialityID,'activeTab' => $activeTab
			));
		} else{
			throw new CHttpException('404', 'Page not found');
		}
	
	}
	
	
}

