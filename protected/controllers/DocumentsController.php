<?php
class DocumentsController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view', 'compose', 'download','link', 'search','list', 'document', 'update','getLists'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {


        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model = new Documents;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Documents'])) {
            $model->attributes = $_POST['Documents'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */

    public function actionComposer()
    {
        $model = new Documents();



        if (isset($_POST)) {
            if(Yii::app()->session['logged_user_type'] == "patient" ){
                $model->owner_type="p";
            }
            if(Yii::app()->session['logged_user_type'] == "doctor"||Yii::app()->session['logged_user_type'] == "parcticeAdmin" ){
                $model->owner_type="d";
                $model->status="shared";
            }
            if(!isset($_POST['patient_id'])){
                $model->patient_id= Yii::app()->session['logged_user_id'];
                $user = Patient::model()->findByPk(Yii::app()->session['logged_user_id']);
                $model->patient_name= $user->user_first_name;
            }

            $model->attributes = $_POST;

            $savemodel=false;
            if (isset($_FILES['image'])) {
                foreach ($_FILES['image']['name'] as $key => $name) {
                    if ($name != '') {
                        if (!defined('awsAccessKey')) define('awsAccessKey', Yii::app()->params['awsAccessKey']);
                        if (!defined('awsSecretKey')) define('awsSecretKey', Yii::app()->params['awsSecretKey']);
                        //$s3 = new S3(awsAccessKey, awsSecretKey);
                        S3::setAuth(Yii::app()->params['awsAccessKey'], Yii::app()->params['awsSecretKey']);
                        S3::$endpoint=Yii::app()->params['awsEndPoint'];
                        $doc_file = new DocumentFiles();
                        $fileName = $_FILES['image']['name'][$key];
                        $fileTempName = $_FILES['image']['tmp_name'][$key];
                        $exts = explode('.', $fileName);
                        $names=current($exts);
                        $ext = end($exts);
                        $fileName = $names."_".uniqid() . '.' . $ext;

                        ///////encryption works from here ....
                        $securityManager = new CSecurityManager;
                        $securityManager->setEncryptionKey(Yii::app()->params['fileEncryptKey']);
                       // $fileData = file_get_contents($_FILES["image"]["tmp_name"][$key]);
                        $fileData = $securityManager->encrypt(base64_encode(file_get_contents($_FILES["image"]["tmp_name"][$key])));
                        $pathNew = Yii::app()->basePath.'/../images/uploads/';
                        file_put_contents($pathNew.$fileName,$fileData);
                        /////uploads to s3 bucket........
                        if (S3::putObject(S3::inputFile($pathNew.$fileName),Yii::app()->params['awsBucket'],$fileName,S3::ACL_PRIVATE,array(),$_FILES['image']['type'][$key],S3::STORAGE_CLASS_STANDARD,S3::SSE_AES256)) {
                            $furl =  $fileName;
                            $savemodel = $model->save();
                            $doc_file->document_id = $model->id;
                            $doc_file->file_url = $furl;
                            $doc_file->save();
                        }
                    }

                }
            }
            if ($savemodel) {

                $log = new Log ;
                $log->user_id = Yii::app()->session['logged_user_id'];
                $log->user_type = Yii::app()->session['logged_user_type'];
                $log->controller = $this->getId();
                $log->action = $this->getAction()->getId();
                $log->created_at = time();
                $log->request =Yii::app()->request->requestUri ;
                $log->param = $model->id;
                $log->save();

                return $model = 1;

            }
        }

       return $model->errors;
    }


    public function actionCompose()
    {
        $model = $this->actionComposer();
        if ($model==1) {
            echo 1;
        } else {
            echo 0;
        }
    }

    public function actionDocument()
    {
        $model = $this->actionComposer();
        if ($model) {
            echo json_encode($model->attributes);
        } else {
            echo 0;
        }
    }
    public function accessCheck(){

        if (isset(Yii::app()->session['logged_user_type']) && (Yii::app()->session['logged_user_type'] == "doctor" ||Yii::app()->session['logged_user_type']=="parcticeAdmin")) {
            $user_type = 1;
            $owner_type = "d";
        } elseif ((Yii::app()->session['logged_user_type'] == "patient")) {
            $user_type = 0;
            $owner_type = "p";
        }
        $doc= Documents::model()->findByAttributes(array('owner'=>Yii::app()->session['logged_user_id'],'owner_type'=>$owner_type));
        if($doc){
            return true;
        }else{
            $shared=SharedDocument::model()->findByAttributes(array('shared_id'=>Yii::app()->session['logged_user_id'],'shared_type'=>$owner_type));
            if($shared){
                return true;
            }else{
                return false;
            }
        }

    }

    public function actionDownload($url)
    {
       // print_r($url);exit;
        $this->layout=false;
        if($this->accessCheck()){
            $url = DocumentFiles::model()->findByPk($url);
            $file = $url->file_url;

            S3::setAuth(Yii::app()->params['awsAccessKey'], Yii::app()->params['awsSecretKey']);
            //$result = $s3->getObject("nintrivatestapp",$file);
            $time = Yii::app()->params['time'];
            $bucket = Yii::app()->params['awsBucket'];
            S3::$endpoint=Yii::app()->params['awsEndPoint'];
            $obj = S3::getObject($bucket,$file);
            /*echo '<pre>';
            print_r($obj); exit;*/
           $signedUrl = S3::getAuthenticatedURL($bucket,$file,"+{$time} minutes",false,true);
          // print_r($signedUrl); exit;

            $securityManager = new CSecurityManager;
            $securityManager->setEncryptionKey(Yii::app()->params['fileEncryptKey']);
           //
           // print_r(file_get_contents($signedUrl)); exit;
           // $temp = (file_get_contents());
//
            $fileData =  base64_decode($securityManager->decrypt($obj->body));
           // $fileData = $securityManager->decrypt((file_get_contents($signedUrl)));
           /* print_r(Yii::app()->basePath) ; exit;*/
            $fileName = uniqid() . '_' . $file;
            $pathNew =Yii::app()->basePath.'/../images/uploads/'.$fileName;

            file_put_contents($pathNew,$fileData);

            //print_r($fileName); exit;


            $this->render('doc_view', array(
                'signedUrl' =>$fileName ,
            ));
        }else{

            throw new CHttpException(403, 'Access Denied');
        }


//        header('Location: '.$signedUrl);
    }

      public function actionLink($url)
      {
          if($this->accessCheck()){
              $url = DocumentFiles::model()->findByPk($url);
              $file = $url->file_url;
              //$file = explode('/', $url);
              $exts = explode('.', $file);
              $ext = end($exts);
              S3::setAuth(Yii::app()->params['awsAccessKey'], Yii::app()->params['awsSecretKey']);
              $bucket = Yii::app()->params['awsBucket'];
              $time = Yii::app()->params['time'];
              S3::$endpoint=Yii::app()->params['awsEndPoint'];
              $signedUrl = S3::getAuthenticatedURL($bucket,$file,"+{$time} minutes");

              $obj = S3::getObject($bucket,$file);
              //print_r($obj); exit;
              $securityManager = new CSecurityManager;
              $securityManager->setEncryptionKey(Yii::app()->params['fileEncryptKey']);
            //  $temp = (file_get_contents($signedUrl));
              $fileData =  base64_decode($securityManager->decrypt($obj->body));
              // $fileData = $securityManager->decrypt((file_get_contents($signedUrl)));
              // print_r($fileData); exit;
             /* $pathNew = Yii::app()->basePath.'/../images/uploads/';*/
              $fileName = uniqid() . '_' . $file;
              $pathNew = Yii::app()->basePath.'/../images/uploads/'.$fileName;
              file_put_contents($pathNew,$fileData);
              if (file_exists($pathNew)){
                  header('Content-Type: application/'.$ext);
                  header('Content-Disposition: attachment; filename='.$fileName);
                  header('Pragma: no-cache');
                  readfile($pathNew);
                  return true;
              }

          }else{
              throw new CHttpException(403, 'Access Denied');
          }


    }

    public function actionSearch()
    {

        $criteria = new CDbCriteria;

        $criteria->condition = 'owner=:owner';
        $criteria->params = array(':owner'=>Yii::app()->session['logged_user_id']);
        $criteria->addCondition('owner_type ="d"');
        $criteria1 = new CDbCriteria();
        if((Yii::app()->session['logged_in']) && (Yii::app()->session['logged_user_type'] == "doctor") ){
            if(isset($_POST['kword']) && ($_POST['kword']!="")){

                $criteria->addCondition("patient_id =".$_POST['kword']);
                $criteria1->addCondition("db_shared_document.document_id =".$_POST['kword']);
            }
        }

        $doc = Documents::model()->findAll($criteria);

        $criteria1->addCondition('status="shared"');
        $criteria1->addCondition('db_shared_document.shared_type="d"');
        $criteria1->addCondition('db_shared_document.shared_id=' . Yii::app()->session['logged_user_id']);
        $criteria1->join = 'LEFT JOIN db_shared_document ON t.id = db_shared_document.document_id';
        $shared = Documents::model()->findAll($criteria1);
        $doc = array_merge($doc, $shared);
        $doc = array_unique($doc, SORT_REGULAR);

        $result = '';
        if (!empty($doc)) {
            for ($i = 0; $i < count($doc); $i++) {
                $search_link = CHtml::CheckBox('idList[]',false,array('id' => 'idList[]','value'=>$doc[$i]['id'].'_'.$doc[$i]['document_name']));

                $result .= '<li>
                         <span class="active" style="width:25%" >' . $doc[$i]['document_name'] . '</span>
                         <span class="active" style="width:20%" >' . $doc[$i]['label'] . '</span>
                         <span class="active" style="width:20%" >' . $doc[$i]['date'] . '</span>
                         <span class="att txt_align">' . $search_link . '</span></li>';
            }
        }
        if ($result == '') echo 'No Result Found!';
        else echo $result;

    }


    public function actionList()
    {

        $criteria = new CDbCriteria;

        $criteria->condition = 'owner=:owner';
        $criteria->params = array(':owner'=>Yii::app()->session['logged_user_id']);
        $criteria->addCondition('owner_type ="p"');
        $criteria1 = new CDbCriteria;
        if((Yii::app()->session['logged_in']) && (Yii::app()->session['logged_user_type'] == "patient") ){
            if(isset($_POST['kword']) && ($_POST['kword']!="")){

                $criteria->addCondition("id =".$_POST['kword']);
                $criteria1->addCondition("db_shared_document.document_id =".$_POST['kword']);
            }
        }


        $doc = Documents::model()->findAll($criteria);

        $criteria1->addCondition('status="shared"');
        $criteria1->addCondition('db_shared_document.shared_type="p"');
        $criteria1->addCondition('db_shared_document.shared_id=' . Yii::app()->session['logged_user_id']);
        $criteria1->join = 'LEFT JOIN db_shared_document ON t.id = db_shared_document.document_id';
        $shared = Documents::model()->findAll($criteria1);
        $doc = array_merge($doc,$shared);
        $doc = array_unique($doc, SORT_REGULAR);

        $result = '';
        if (!empty($doc)) {
            for ($i = 0; $i < count($doc); $i++) {
                $search_link = CHtml::CheckBox('idList[]',false,array('id' => 'idList[]','value'=>$doc[$i]['id'].'_'.$doc[$i]['document_name']));
                $result .= '<li>
                         <span class="active" style="width:25%" >' . $doc[$i]['document_name'] . '</span>
                         <span class="active" style="width:20%" >' . $doc[$i]['label'] . '</span>
                         <span class="active" style="width:20%" >' . $doc[$i]['date'] . '</span>

                         <span class="att txt_align">' . $search_link . '</span></li>';
            }
        }
        if ($result == '') echo 'No Result Found!';
        else echo $result;

    }

    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);
        $owner = $model->owner;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Documents'])) {
            if (isset($_POST['label']) && $_POST['label'])
                $_POST['Documents']['label'] = $_POST['label'];
            if (isset($_POST['status']) && $_POST['status'])
                $_POST['Documents']['status'] = $_POST['status'];
            $model->attributes = $_POST['Documents'];

            $model->owner=$owner;
           // print_r($model->owner);exit;
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $dataProvider = new CActiveDataProvider('Documents');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new Documents('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['Documents']))
            $model->attributes = $_GET['Documents'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Documents the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Documents::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Documents $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'documents-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actiongetLists(){
        $this->layout = false;
        if(Yii::app()->session['logged_in']){
        $criteria = new CDbCriteria;
//        $criteria->condition = 'owner=:owner';
//        $criteria->params = array(':owner'=>Yii::app()->session['logged_user_id']);
        $criteria->addCondition('owner='.Yii::app()->session['logged_user_id']);
        $criteria->addCondition('document_name LIKE "'.$_REQUEST['keyword'].'%"');
        $criteria->addCondition('owner_type ="p"');
        $doc = Documents::model()->findAll($criteria);
            $shared = array();
            if(!$_REQUEST['keyword']){
                $criteria1 = new CDbCriteria();
                $criteria1->addCondition('status="shared"');

                $criteria1->addCondition('db_shared_document.shared_type="p"');
                $criteria1->addCondition('db_shared_document.shared_id=' . Yii::app()->session['logged_user_id']);
                $criteria1->join = 'LEFT JOIN db_shared_document ON t.id = db_shared_document.document_id';
                $shared = Documents::model()->findAll($criteria1);
            }


            $document = array_merge($doc,$shared);



        foreach ($document as $rs) {
            // put in bold the written text
            $vic_date=date("m-d-Y",strtotime($rs['date']));
            $vic_full_name = str_replace($_REQUEST['keyword'], '<b>'.$_REQUEST['keyword'].'</b>', $rs['document_name']);
            $data = $rs['document_name']."|".$rs['date']."|".$rs['id'];
            echo '<li onclick="docs(\''.$data.'\')">'.$vic_full_name.",".$vic_date.'</li>';
        }
        }
        else{
            $this->redirect(array('site/index'));
        }
    }
}
