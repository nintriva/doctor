<?php


class Force_pw_resetCommand extends CConsoleCommand {
    public function run($args) {

        $connection = Yii::app()->db;
        $sql="select * FROM da_patient WHERE status=1";
        $command = $connection->createCommand($sql);
        $user_arr = $command->queryAll();

        if($user_arr){
            foreach($user_arr as $user){
                $current_time = time();
                $last_pw_change = $user['force_pw_changed_at'];
                $duration = $current_time - $last_pw_change ;
                $reason = 'password is older than 60 days';
                if($duration > 5184000){
                    $sql='UPDATE da_patient SET  force_pw_change = :force_pw_change ,force_pw_change_reason =:force_pw_change_reason WHERE status=1 and id ='.$user['id'];
                    $params = array(
                        "force_pw_change" => 1,
                        "force_pw_change_reason"=> $reason
                    );
                    $connection = Yii::app()->db;
                    $command=$connection->createCommand($sql);
                    $command->execute($params);
                }

            }

        }

        }

}