<?php


class Unblock_ipCommand extends CConsoleCommand {
    public function run($args) {
        $connection = Yii::app()->db;
        $sql="select * FROM locked_ips";
        $command = $connection->createCommand($sql);
        $array = $command->queryAll();
        if($array){
            foreach($array as $adress){
                $time = time();
                $differnce = $time -$adress['time']  ;
                if($differnce >86400){
                    $command = Yii::app()->db->createCommand();
                    $command->delete('locked_ips', 'id=:id', array(':id'=>$adress["id"]));

                }
            }
        }
    }
}