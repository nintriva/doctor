<?php

/**
 * This is the model class for table "da_doctor_book".
 *
 * The followings are the available columns in table 'da_doctor_book':
 * @property integer $id
 * @property integer $patient_id
 * @property string $book_time
 * @property integer $doctor_id
 * @property integer $address_id
 * @property integer $speciality_id
 * @property integer $procedure_id
 * @property string $date_created
 * @property string $date_modified
 * @property integer $status
 */
class DoctorBook extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'da_doctor_book';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('patient_id, book_time, doctor_id, address_id, speciality_id, procedure_id, date_created, date_modified, status', 'required'),
			array('patient_id', 'required'),
			array('patient_id, doctor_id, address_id, speciality_id, procedure_id, status', 'numerical', 'integerOnly'=>true),
			array('book_time', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, patient_id, book_time, doctor_id, address_id, speciality_id, procedure_id, date_created, date_modified, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'patient_id' => 'Patient',
			'book_time' => 'Book Time',
			'doctor_id' => 'Doctor',
			'address_id' => 'Address',
			'speciality_id' => 'Speciality',
			'procedure_id' => 'Procedure',
			'date_created' => 'Date Created',
			'date_modified' => 'Date Modified',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('patient_id',$this->patient_id);
		$criteria->compare('book_time',$this->book_time,true);
		$criteria->compare('doctor_id',$this->doctor_id);
		$criteria->compare('address_id',$this->address_id);
		$criteria->compare('speciality_id',$this->speciality_id);
		$criteria->compare('procedure_id',$this->procedure_id);
		$criteria->compare('date_created',$this->date_created,true);
		$criteria->compare('date_modified',$this->date_modified,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DoctorBook the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
