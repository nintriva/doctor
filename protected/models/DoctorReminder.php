<?php

/**
 * This is the model class for table "da_doctor_reminder".
 *
 * The followings are the available columns in table 'da_doctor_reminder':
 * @property integer $id
 * @property integer $doctor_id
 * @property string $reminder_for
 * @property integer $reminder_time
 * @property string $reminder_period
 * @property string $reminder_text
 * @property string $date_created
 * @property string $date_modified
 * @property integer $status
 */
class DoctorReminder extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'da_doctor_reminder';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('doctor_id, reminder_for, reminder_time, reminder_period, reminder_text, date_created, date_modified, status', 'required'),
			array('doctor_id, reminder_time, status', 'numerical', 'integerOnly'=>true),
			array('reminder_for', 'length', 'max'=>10),
			array('reminder_period', 'length', 'max'=>20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, doctor_id, reminder_for, reminder_time, reminder_period, reminder_text, date_created, date_modified, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'doctor_id' => 'Doctor',
			'reminder_for' => 'Reminder For',
			'reminder_time' => 'Reminder Time',
			'reminder_period' => 'Reminder Period',
			'reminder_text' => 'Reminder Text',
			'date_created' => 'Date Created',
			'date_modified' => 'Date Modified',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('doctor_id',$this->doctor_id);
		$criteria->compare('reminder_for',$this->reminder_for,true);
		$criteria->compare('reminder_time',$this->reminder_time);
		$criteria->compare('reminder_period',$this->reminder_period,true);
		$criteria->compare('reminder_text',$this->reminder_text,true);
		$criteria->compare('date_created',$this->date_created,true);
		$criteria->compare('date_modified',$this->date_modified,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DoctorReminder the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
