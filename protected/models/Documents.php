<?php

/**
 * This is the model class for table "db_document".
 *
 * The followings are the available columns in table 'db_document':
 * @property integer $id
 * @property string $owner
 * @property string $document_name
 * @property string $label
 * @property string $status
 * @property string $date
 * @property string $note
 * @property string $file_url
 * @property string $is_private
 * @property string $is_share
 * @property string $key
 */
class Documents extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'db_document';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('document_name, label, note,date,patient_name','required'),
			array('owner', 'length', 'max'=>500),
			array('document_name', 'length', 'max'=>1000),
			array('label', 'length', 'max'=>100),
			array('note', 'length', 'max'=>2000),
			array('key', 'length', 'max'=>200),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, owner, document_name, label, status, date, note, is_private,owner_type, is_share, key,patient_name', 'safe', 'on'=>'search'),
			array('owner,date,patient_name,patient_id,owner_type,status', 'safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'docFiles' => array(self::HAS_MANY, 'DocumentFiles', 'document_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'owner' => 'Owner',
			'document_name' => 'Document Name',
			'label' => 'Label',
			'status' => 'Status',
			'date' => 'Date',
			'note' => 'Note',
			'is_private' => 'Is Private',
			'is_share' => 'Is Share',
			'key' => 'Key',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('owner',$this->owner,true);
		$criteria->compare('document_name',$this->document_name,true);
		$criteria->compare('label',$this->label,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('note',$this->note,true);
		$criteria->compare('file_url',$this->file_url,true);
		$criteria->compare('is_private',$this->is_private,true);
		$criteria->compare('is_share',$this->is_share,true);
		$criteria->compare('key',$this->key,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Documents the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
    public function beforeSave(){


        if ($this->isNewRecord){
            $this->owner = Yii::app()->session['logged_user_id'];
            $this->created_date = date('Y-m-d H:m:s');
            $this->updated_date = date('Y-m-d H:m:s');
        }else{
            $this->updated_date = date('Y-m-d H:m:s');
        }

        return true;
    }
}
