<?php

/**
 * This is the model class for table "da_doctor".
 *
 * The followings are the available columns in table 'da_doctor':
 * @property integer $id
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $phone
 * @property string $zip
 * @property integer $speciality
 * @property integer $status
 */
class PaAdminDoctors extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return Yii::app()->params['dbPrefix'] . 'pa_admin_doctors';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		return array();
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			//'doctor' => array(self::BILONGSTO, 'Doctor', 'doctor_id','condition'=>'Doctor.status=1'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array();
	}

	public function doctorsName($id){
		return $this->findAll(array(
            'condition' => 'doctor_id = :doctor_id',
            'params' => array(':doctor_id' => $id),
        ));
	}
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Doctor the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
}
