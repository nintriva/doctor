<?php

/**
 * This is the model class for table "da_patient".
 *
 * The followings are the available columns in table 'da_patient':
 * @property integer $id
 * @property string $user_name
 * @property string $user_email
 * @property string $password
 * @property string $user_first_name
 * @property string $user_last_name
 * @property string $user_dob
 * @property string $user_sex
 * @property string $entry_type
 * @property string $visiter
 * @property string $patient_name
 * @property string $patient_email
 * @property string $patient_dob
 * @property integer $patient_sex
 * @property string $date_created
 * @property string $date_modified
 * @property integer $status
 */
class Patient extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'da_patient';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('user_name, user_email, password, user_first_name, user_last_name, user_dob, user_sex, entry_type, visiter, patient_name, patient_email, patient_dob, patient_sex, date_created, date_modified, status', 'required'),
			array('user_name, status', 'required'),
			array('status,force_pw_change', 'numerical', 'integerOnly'=>true),
			array('user_name,force_pw_change_resaon, user_email, password, user_first_name, user_last_name, patient_name, patient_email', 'length', 'max'=>32),
			array('user_sex', 'length', 'max'=>10),
			array('entry_type, visitor', 'length', 'max'=>5),
			array('user_first_name,user_last_name, user_phone, user_address, user_city, user_state, user_zip, user_contact_method', 'required', 'on'=>'edit_profile'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, user_name, user_email, password,force_pw_change_resaon, user_first_name, user_last_name, user_dob, user_sex, entry_type, visiter, patient_name, patient_email, patient_dob, patient_sex, date_created, date_modified, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label) 
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_name' => 'User Name',
			'user_email' => 'User Email',
			'password' => 'Password',
			'user_first_name' => 'First Name',
			'user_last_name' => 'Last Name',
			'user_dob' => 'Date of Birth',
			'user_sex' => 'Sex',
			'user_phone' => 'Phone Number',
			'user_address' => 'Address',
			'user_city' => 'City',
			'user_state' => 'State',
			'user_zip' => 'Zip',
			'user_contact_method' => 'Preferred Contact Method',
			'entry_type' => 'Entry Type',
			'visiter' => 'Visiter',
			'patient_name' => 'Patient Name',
			'patient_email' => 'Patient Email',
			'patient_dob' => 'Patient Dob',
			'patient_sex' => 'Patient Sex',
			'date_created' => 'Date Created',
			'date_modified' => 'Date Modified',
			'status' => 'Status',
            'force_pw_change'=>'Force PW change',
            'force_pw_change_resaon'=>'Force PW change Reason'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_name',$this->user_name,true);
		$criteria->compare('user_email',$this->user_email,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('user_first_name',$this->user_first_name,true);
		$criteria->compare('user_last_name',$this->user_last_name,true);
		$criteria->compare('user_dob',$this->user_dob,true);
		$criteria->compare('user_sex',$this->user_sex,true);
		$criteria->compare('entry_type',$this->entry_type,true);
		$criteria->compare('visiter',$this->visiter,true);
		$criteria->compare('patient_name',$this->patient_name,true);
		$criteria->compare('patient_email',$this->patient_email,true);
		$criteria->compare('patient_dob',$this->patient_dob,true);
		$criteria->compare('patient_sex',$this->patient_sex);
		$criteria->compare('date_created',$this->date_created,true);
		$criteria->compare('date_modified',$this->date_modified,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('force_pw_change',$this->force_pw_change);
		$criteria->compare('force_pw_change_resaon',$this->force_pw_change_resaon);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Patient the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function getGenderOptions(){
		return array('male' => 'Male', 'female' => 'Female');
	}

	public function getContactMethod(){
		return array('Email' => 'Email', 'Phone' => 'Phone', 'Sms' => 'Sms');
	}

	public function getMonthOptions(){
		return array('01' => 'January', '02' => 'February', '03' => 'March', '94' => 'April', '05' => 'May', '06' => 'June', '07' => 'July', '08' => 'August', '09' => 'September', '10' => 'October', '11' => 'November', '12' => 'December');
	}
	
	public function getDateOptions(){
		return array('01' => '01', '02' => '02', '03' => '03', '04' => '04', '05' => '05', '06' => '06', '07' => '07', '08' => '08', '09' => '09', '10' => '10', '11' => '12', '13' => '13', '14' => '14', '15' => '15', '16' => '16', '17' => '17', '18' => '18', '19' => '19', '20' => '20', '21' => '21', '22' => '23', '24' => '24', '25' => '25', '26' => '26', '27' => '27', '28' => '28', '29' => '29', '30' => '30', '31' => '31');
	}
}
