<?php

/**
 * This is the model class for table "da_doctor_appointment_settings".
 *
 * The followings are the available columns in table 'da_doctor_appointment_settings':
 * @property integer $id
 * @property integer $doctor_id
 * @property string $appointment_approval_required
 * @property integer $appointment_per_day
 * @property integer $appointment_canel_period
 * @property string $date_created
 * @property string $date_modified
 * @property integer $status
 */
class DoctorAppointmentSettings extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'da_doctor_appointment_settings';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('doctor_id, appointment_approval_required, appointment_per_day, appointment_canel_period, date_created, date_modified, status', 'required'),
			array('doctor_id, appointment_per_day, appointment_canel_period, status', 'numerical', 'integerOnly'=>true),
			array('appointment_approval_required', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, doctor_id, appointment_approval_required, appointment_per_day, appointment_canel_period, date_created, date_modified, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'doctor_id' => 'Doctor',
			'appointment_approval_required' => 'Appointment Approval Required',
			'appointment_per_day' => 'Appointment Per Day',
			'appointment_canel_period' => 'Appointment Canel Period',
			'date_created' => 'Date Created',
			'date_modified' => 'Date Modified',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('doctor_id',$this->doctor_id);
		$criteria->compare('appointment_approval_required',$this->appointment_approval_required,true);
		$criteria->compare('appointment_per_day',$this->appointment_per_day);
		$criteria->compare('appointment_canel_period',$this->appointment_canel_period);
		$criteria->compare('date_created',$this->date_created,true);
		$criteria->compare('date_modified',$this->date_modified,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DoctorAppointmentSettings the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
