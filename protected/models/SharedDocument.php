<?php

/**
 * This is the model class for table "db_shared_document".
 *
 * The followings are the available columns in table 'db_shared_document':
 * @property integer $id
 * @property integer $document_id
 * @property integer $user_id
 * @property integer $shared_id
 * @property string $date
 * @property string $notes
 * @property string $key
 */
class SharedDocument extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'db_shared_document';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('document_id,  shared_id, date', 'required'),
			array('document_id, user_id, shared_id', 'numerical', 'integerOnly'=>true),
			array('notes', 'length', 'max'=>2000),
			array('key', 'length', 'max'=>500),
            array('user_id,notes,key,shared_type,message_id', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, document_id, user_id, shared_id, date, notes, key', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'document_id' => 'Document',
			'user_id' => 'User',
			'shared_id' => 'Shared',
			'date' => 'Date',
			'notes' => 'Notes',
			'key' => 'Key',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('document_id',$this->document_id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('shared_id',$this->shared_id);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('notes',$this->notes,true);
		$criteria->compare('key',$this->key,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SharedDocument the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
