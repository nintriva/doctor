<?php

/**
 * This is the model class for table "da_doctor_address".
 *
 * The followings are the available columns in table 'da_doctor_address':
 * @property integer $id
 * @property integer $doctor_id
 * @property string $address
 * @property string $latitude
 * @property string $longitude
 * @property string $access
 * @property integer $order_status
 * @property integer $default_status
 * @property integer $active
 * @property integer $status
 */
class DoctorAddress extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return Yii::app()->params['dbPrefix'] . 'doctor_address';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('doctor_id, address, order_status, default_status, active, status', 'required'),
			array('doctor_id, address, city, zip, status', 'required'),
			array('doctor_id, order_status, default_status, active, status', 'numerical', 'integerOnly'=>true),
			array('address', 'length', 'max'=>255),
			array('latitude, longitude', 'length', 'max'=>15),
			array('access', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, doctor_id, address, latitude, longitude, access, order_status, default_status, active, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'doctor_id' => 'Doctor',
			'address' => 'Address<br>(max: 255 chars)',
			'latitude' => 'Latitude',
			'longitude' => 'Longitude',
			'access' => 'Access',
			'order_status' => 'Order',
			'default_status' => 'Selected as default address',
			'active' => 'Active',
			'status' => 'Status',
			'city' => 'City',
			'state' => 'State',
			'zip' => 'Zip',
			'practice_affiliation' => 'Practice',
			'office_name' => 'Office Name',
			'office_phone' => 'Phone',
			'office_fax' => 'Fax',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('doctor_id',$this->doctor_id);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('latitude',$this->latitude,true);
		$criteria->compare('longitude',$this->longitude,true);
		$criteria->compare('access',$this->access,true);
		$criteria->compare('order_status',$this->order_status);
		$criteria->compare('default_status',0);
		$criteria->compare('active',$this->active);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DoctorAddress the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function getAccessOptions(){
		return array('Public' => 'Public', 'Registered' => 'Registered');
	}
}
