<?php

/**
 * This is the model class for table "log_table".
 *
 * The followings are the available columns in table 'log_table':
 * @property integer $id
 * @property integer $user_id
 * @property integer $user_type
 * @property integer $fav_id
 * @property integer $fav_type
 * @property string $fav_email
 */
class Log extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'log_table';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(


            array('id,user_id,  category, created_at', 'numerical', 'integerOnly'=>true),
            array('controller,action,user_type,ip', 'length', 'max'=>300),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, user_id, user_type,request, category, created_at, controller,action', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'user_id' => 'User',
            'patient_id' => 'Patient ID',
            'category' => 'Category',
            'created_at' => 'Created_at',
            'controller' => 'Controller',
            'action' => 'Action',
            'ip' => 'ip',
        );
    }


    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('user_id',$this->user_id);
        $criteria->compare('patient_id',$this->patient_id);
        $criteria->compare('category',$this->category);
        $criteria->compare('created_at',$this->created_at);
        $criteria->compare('controller',$this->controller);
        $criteria->compare('action',$this->action);
        $criteria->compare('ip',$this->ip);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return MailsFavorites the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}
