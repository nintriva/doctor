<?php

/**
 * This is the model class for table "da_doctor".
 *
 * The followings are the available columns in table 'da_doctor':
 * @property integer $id
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $phone
 * @property string $zip
 * @property integer $speciality
 * @property integer $status
 */
class Message extends CActiveRecord
{
	
	/**
	 * @return string the associated database table name
	 */
    public function tableName()
    {
        return 'da_inbox';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('sent_from_email, subject, msg_body, attachments_path, thread_id, created', 'safe'),
            array('sent_from_user, sent_to_user,  created, created_by, read_flag, status,department_id', 'numerical', 'integerOnly'=>true),
            array('sent_from_email, sent_to_email, attachments_path', 'length', 'max'=>201),
            array('subject', 'length', 'max'=>101),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('inbox_id,department_id, sent_from_user, sent_from_email, sent_to_user, sent_to_email, subject, msg_body, attachments_path, thread_id, created, created_by, read_flag, status', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'inbox_id' => 'Inbox',
            'sent_from_user' => 'Sent From User',
            'sent_from_email' => 'Sent From Email',
            'sent_to_user' => 'Sent To User',
            'sent_to_email' => 'Sent To Email',
            'subject' => 'Subject',
            'msg_body' => 'Msg Body',
            'attachments_path' => 'Attachments Path',
            'thread_id' => 'Thread',
            'created' => 'Created',
            'created_by' => 'Created By',
            'read_flag' => 'Read Flag',
            'status' => 'Status',
            'department_id' => 'Department Id',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('inbox_id',$this->inbox_id);
        $criteria->compare('sent_from_user',$this->sent_from_user);
        $criteria->compare('sent_from_email',$this->sent_from_email,true);
        $criteria->compare('sent_to_user',$this->sent_to_user);
        $criteria->compare('sent_to_email',$this->sent_to_email,true);
        $criteria->compare('subject',$this->subject,true);
        $criteria->compare('msg_body',$this->msg_body,true);
        $criteria->compare('attachments_path',$this->attachments_path,true);
        $criteria->compare('thread_id',$this->thread_id);
        $criteria->compare('created',$this->created);
        $criteria->compare('created_by',$this->created_by);
        $criteria->compare('read_flag',$this->read_flag);
        $criteria->compare('status',$this->status);
        $criteria->compare('department_id',$this->department_id);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
	
	public function encrypt($pure_string) {
        	$dirty = array("+", "/", "=");
        	$clean = array("_PLUS_", "_SLASH_", "_EQUALS_");
        	$iv_size = mcrypt_get_iv_size(MCRYPT_BLOWFISH, MCRYPT_MODE_ECB);
        	//$_SESSION['iv'] = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        	$encrypted_string = utf8_encode($pure_string);
        	$encrypted_string = base64_encode($encrypted_string);
        	return str_replace($dirty, $clean, $encrypted_string);
        }      
}
