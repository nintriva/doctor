<?php

/**
 * This is the model class for table "da_doctor".
 *
 * The followings are the available columns in table 'da_doctor':
 * @property integer $id
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $phone
 * @property string $zip
 * @property integer $speciality
 * @property integer $status
 */
class Doctor extends CActiveRecord
{
	public $verifyCode;public $email;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return Yii::app()->params['dbPrefix'] . 'doctor';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('first_name, last_name, email, phone, speciality', 'required',
                  'message'=>'Please enter a value for {attribute}.'),
			array('status', 'numerical', 'integerOnly'=>true),
			array('first_name, last_name, email, zip', 'length', 'max'=>32),
			array('addr1', 'length', 'max'=>155),
			array('phone', 'length', 'max'=>15),
			array('zip', 'length', 'max'=>10),
			// email has to be a valid email address
			array('email,google_account', 'email', 'message'=>'Email is not valid'),
     		array('email', 'uniqueEmail', 'on'=>'insert'),
			//array('speciality', 'required', 'on'=>'insert'),
			// verifyCode needs to be entered correctly
			array('verifyCode', 'captcha', 'allowEmpty'=>!CCaptcha::checkRequirements(), 'on'=>'insert'),
			//array('visit_price,visit_duration', 'required', 'on'=>'edit_profile'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, first_name, last_name, email, phone, zip, speciality, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			//'country' => array(self::BELONGS_TO, 'Country', 'country_id'),
			'doctorSpeciality' => array(self::HAS_ONE, 'DoctorSpeciality', 'doctor_id','condition'=>'doctorSpeciality.status=1'),
			'doctorAddress' => array(self::HAS_MANY, 'DoctorAddress', 'doctor_id','condition'=>'doctorAddress.status=1'),
			'doctorOffers' => array(self::HAS_ONE, 'DoctorOffers', 'doctor_id','condition'=>'doctorOffers.status=1'),
			'doctorLanguage' => array(self::HAS_ONE, 'DoctorLanguage', 'doctor_id','condition'=>'doctorLanguage.status=1'),
			'doctorInsurance' => array(self::HAS_ONE, 'DoctorInsurance', 'doctor_id','condition'=>'doctorInsurance.status=1'),
			'doctorProcedure' => array(self::HAS_ONE, 'DoctorProcedure', 'doctor_id','condition'=>'doctorProcedure.status=1'),
				
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'first_name' => 'First Name',
			'last_name' => 'Last Name',
			'email' => 'Email',
			'phone' => 'Phone',
			'zip' => 'Practice ZIP Code',
			'speciality' => 'Speciality',
			'status' => 'Status',
			'verifyCode'=>'Verification Code',
			'addr1' => 'Address',
			'addr2' => 'Address 2',
			'state' => 'State',
			'country' => 'Country',
			'gender' => 'Gender',
			'birth_date' => 'Birth Date',
			'title' => 'Title',
			'comments' => 'Comments',
			'degree' => 'Degree',
			'education' => 'Education',
			'residency_training' => 'Residency Training',
			'hospital_affiliations' => 'Hospital Affiliations',
			'board_certifications' => 'Board Certifications',
			'awards_publications' => 'Awards and Publications',
			'languages_spoken' => 'Preferred Language',
			'insurances_accepted' => 'Insurances Accepted',
			'date_created' => 'Date Created',
			'image' => 'Image',
			'visit_price' => 'Default Price per Visit ($)',
			'visit_duration' => 'Default Visit Duration (min)',
			'medical_school' => 'Medical School',
			'medical_school_year' => 'Year',
			'residency_training_year' => 'Year',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('first_name',$this->first_name,true);
		$criteria->compare('first_name',$this->middle_name,true);
		$criteria->compare('last_name',$this->last_name,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('zip',$this->zip,true);
		$criteria->compare('speciality',$this->speciality);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Doctor the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function getSpecialtyOptions(){
		return array('1' => 'Primary Care Doctors', '2' => 'Chiropractors', '3' => 'Dentists', '4' => 'Dermatologists', '5' => 'Eye Doctors', '6' => 'Gynecologists', '7' => 'Psychiatrists');
	}
	
	public function getTitleOptions(){
		return array('Dr.' => 'Dr.', 'Mr.' => 'Mr.', 'Ms.' => 'Ms.', 'Mrs.' => 'Mrs.', 'Miss' => 'Miss');
	}
	
	public function getGenderOptions(){
		return array('M' => 'Male', 'F' => 'Female');
	}
	
	public function getDegreeOptions(){
		return array('BMBS' => 'BMBS', 'MBBS' => 'MBBS', 'MBChB' => 'MBChB', 'MB BCh' => 'MB BCh', 'BMed' => 'BMed', 'MD' => 'MD', 'MDCM' => 'MDCM', 'Dr.MuD' => 'Dr.MuD', 'Dr.Med' => 'Dr.Med', 'Cand.med' => 'Cand.med', 'Med' => 'Med');
	}
	
	public function getMonthOptions(){
		return array('01' => 'January', '02' => 'February', '03' => 'March', '94' => 'April', '05' => 'May', '06' => 'June', '07' => 'July', '08' => 'August', '09' => 'September', '10' => 'October', '11' => 'November', '12' => 'December');
	}
	
	public function getDateOptions(){
		return array('01' => '01', '02' => '02', '03' => '03', '04' => '04', '05' => '05', '06' => '06', '07' => '07', '08' => '08', '09' => '09', '10' => '10', '11' => '11', '12' => '12', '13' => '13', '14' => '14', '15' => '15', '16' => '16', '17' => '17', '18' => '18', '19' => '19', '20' => '20', '21' => '21', '22' => '22', '23' => '23', '24' => '24', '25' => '25', '26' => '26', '27' => '27', '28' => '28', '29' => '29', '30' => '30', '31' => '31');
	}
	
	public function uniqueEmail($attribute, $params){
		 // Set $emailExist variable true or false by using your custom query on checking in database table if email exist or not.
		// You can user $this->{$attribute} to get attribute value.
	
		 if(Doctor::model()->exists('username=:email',array('email'=>$this->email)))
             $this->addError('email','Email already exists.');
	}
}
