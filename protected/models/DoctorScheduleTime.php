<?php

/**
 * This is the model class for table "da_doctor_schedule_time".
 *
 * The followings are the available columns in table 'da_doctor_schedule_time':
 * @property integer $id
 * @property integer $schedule_id
 * @property integer $address_id
 * @property string $day
 * @property string $from_time
 * @property string $to_time
 * @property string $from_time_format
 * @property integer $time_slot
 * @property integer $laser_slot
 * @property string $date_created
 * @property string $date_modified
 * @property integer $status
 */
class DoctorScheduleTime extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'da_doctor_schedule_time';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('schedule_id, address_id, day, from_time, to_time, from_time_format, to_time_format, time_slot, laser_slot, status', 'required'),
			array('schedule_id, address_id, time_slot, laser_slot, status', 'numerical', 'integerOnly'=>true),
			array('day', 'length', 'max'=>10),
			array('from_time_format', 'length', 'max'=>5),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, schedule_id, address_id, day, from_time, to_time, from_time_format, time_slot, laser_slot, date_created, date_modified, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'schedule_id' => 'Schedule',
			'address_id' => 'Address',
			'day' => 'Day',
			'from_time' => 'From Time',
			'to_time' => 'To Time',
			'from_time_format' => 'From Time Format',
			'time_slot' => 'Time Slot',
			'laser_slot' => 'Laser Slot',
			'date_created' => 'Date Created',
			'date_modified' => 'Date Modified',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('schedule_id',$this->schedule_id);
		$criteria->compare('address_id',$this->address_id);
		$criteria->compare('day',$this->day,true);
		$criteria->compare('from_time',$this->from_time,true);
		$criteria->compare('to_time',$this->to_time,true);
		$criteria->compare('from_time_format',$this->from_time_format,true);
		$criteria->compare('time_slot',$this->time_slot);
		$criteria->compare('laser_slot',$this->laser_slot);
		$criteria->compare('date_created',$this->date_created,true);
		$criteria->compare('date_modified',$this->date_modified,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DoctorScheduleTime the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function getDayOptions(){
		return array('Monday' => 'Monday', 'Tuesday' => 'Tuesday', 'Wednesday' => 'Wednesday', 'Thursday' => 'Thursday', 'Friday' => 'Friday', 'Saturday' => 'Saturday', 'Sunday' => 'Sunday');
	}
	
	public function getTimeFormatOptions(){
		return array('AM' => 'AM', 'PM' => 'PM');
	}
	
	public function getTimeSlotOptions(){
		return array('10' => '10', '15' => '15', '20' => '20', '25' => '25', '30' => '30');
	}
	
	public function getLaserSlotOptions(){
		return array('10' => '10', '15' => '15', '20' => '20', '25' => '25', '30' => '30');
	}
	
	public function getTimeHourOptions(){
		return array('01' => '01', '02' => '02', '03' => '03', '04' => '04', '05' => '05', '06' => '06', '07' => '07', '08' => '08', '09' => '09', '10' => '10', '11' => '11', '12' => '12');
	}
	
	public function getTimeMinuteOptions(){
		return array('00' => '00', '05' => '05', '10' => '10', '15' => '15', '20' => '20', '25' => '25', '30' => '30', '35' => '35', '40' => '40', '45' => '45', '50' => '50', '55' => '55');
	}
	
	public function getTimeOptions(){
		return array('01:00 am' => '01:00 am', '01:30 am' => '01:30 am', '02:00 am' => '02:00 am', '02:30 am' => '02:30 am', '03:00 am' => '03:00 am', '03:30 am' => '03:30 am', '04:00 am' => '04:00 am', '04:30 am' => '04:30 am', '05:00 am' => '05:00 am', '05:30 am' => '05:30 am', '06:00 am' => '06:00 am', '06:30 am' => '06:30 am', '07:00 am' => '07:00 am', '07:30 am' => '07:30 am', '08:00 am' => '08:00 am', '08:30 am' => '08:30 am', '09:00 am' => '09:00 am', '09:30 am' => '09:30 am', '10:00 am' => '10:00 am', '10:30 am' => '10:30 am', '11:00 am' => '11:00 am', '11:30 am' => '11:30 am', '12:00 am' => '12:00 am', '12:30 am' => '12:30 am', 
		'01:00 pm' => '01:00 pm', '01:30 pm' => '01:30 pm', '02:00 pm' => '02:00 pm', '02:30 pm' => '02:30 pm', '03:00 pm' => '03:00 pm', '03:30 pm' => '03:30 pm', '04:00 pm' => '04:00 pm', '04:30 pm' => '04:30 pm', '05:00 pm' => '05:00 pm', '05:30 pm' => '05:30 pm', '06:00 pm' => '06:00 pm', '06:30 pm' => '06:30 pm', '07:00 pm' => '07:00 pm', '07:30 pm' => '07:30 pm', '08:00 pm' => '08:00 pm', '08:30 pm' => '08:30 pm', '09:00 pm' => '09:00 pm', '09:30 pm' => '09:30 pm', '10:00 pm' => '10:00 pm', '10:30 pm' => '10:30 pm', '11:00 pm' => '11:00 pm', '11:30 pm' => '11:30 pm', '12:00 pm' => '12:00 pm', '12:30 pm' => '12:30 pm');
	}
}
