<?php

/**
 * This is the model class for table "da_doctor".
 *
 * The followings are the available columns in table 'da_doctor':
 * @property integer $id
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $phone
 * @property string $zip
 * @property integer $speciality
 * @property integer $name_prefix
 * @property integer $status
 */
class PaAdmin extends CActiveRecord
{
	public $verifyCode;public $email;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return Yii::app()->params['dbPrefix'] . 'pa_admin';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		return array(
			array('first_name,last_name,address1,email,phone,city,state', 'required'),
			array('zip', 'numerical', 'integerOnly'=>true),
			array('email, phone', 'length', 'max'=>100),
			array('email', 'email', 'message'=>'Email is not valid'),
			array('middle_name,name_prefix,name_suffix,address2', 'safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			//'doctor' => array(self::BILONGSTO, 'Doctor', 'doctor_id','condition'=>'Doctor.status=1'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'first_name' => 'First Name',
			'last_name' => 'Last Name',
			'middle_name' => 'Middle Name',
			'name_prefix' => 'Name Prefix',	
			'email' => 'Email',
			'phone' => 'Phone',
			'zip' => 'Practice ZIP Code',
			'speciality' => 'Speciality',
			'status' => 'Status',
			'verifyCode'=>'Verification Code',
			'address1' => 'Address',
			'address2' => 'Address 2',
			'state' => 'State',
			'country' => 'Country',
			'gender' => 'Gender',
			'birth_date' => 'Birth Date',
			'title' => 'Title',
		);
	}

	public function getTitleOptions(){
		return array('Dr.' => 'Dr.', 'Mr.' => 'Mr.', 'Ms.' => 'Ms.', 'Mrs.' => 'Mrs.', 'Miss' => 'Miss');
	}
	public function getGenderOptions(){
		return array('M' => 'Male', 'F' => 'Female');
	}
	
	public function getDegreeOptions(){
		return array('BMBS' => 'BMBS', 'MBBS' => 'MBBS', 'MBChB' => 'MBChB', 'MB BCh' => 'MB BCh', 'BMed' => 'BMed', 'MD' => 'MD', 'MDCM' => 'MDCM', 'Dr.MuD' => 'Dr.MuD', 'Dr.Med' => 'Dr.Med', 'Cand.med' => 'Cand.med', 'Med' => 'Med');
	}
	
	public function getMonthOptions(){
		return array('01' => 'January', '02' => 'February', '03' => 'March', '94' => 'April', '05' => 'May', '06' => 'June', '07' => 'July', '08' => 'August', '09' => 'September', '10' => 'October', '11' => 'November', '12' => 'December');
	}
	
	public function getDateOptions(){
		return array('01' => '01', '02' => '02', '03' => '03', '04' => '04', '05' => '05', '06' => '06', '07' => '07', '08' => '08', '09' => '09', '10' => '10', '11' => '11', '12' => '12', '13' => '13', '14' => '14', '15' => '15', '16' => '16', '17' => '17', '18' => '18', '19' => '19', '20' => '20', '21' => '21', '22' => '22', '23' => '23', '24' => '24', '25' => '25', '26' => '26', '27' => '27', '28' => '28', '29' => '29', '30' => '30', '31' => '31');
	}
	public function uniqueEmail($attribute, $params){
		// Set $emailExist variable true or false by using your custom query on checking in database table if email exist or not.
		// You can user $this->{$attribute} to get attribute value.
	
		if(Doctor::model()->exists('username=:email',array('email'=>$this->email))){
			$this->addError('email','Email already exists.');
		}if(Patient::model()->exists('user_name=:email',array('user_email'=>$this->email))){
			$this->addError('email','Email already exists.');
		}if(PaAdmin::model()->exists('username=:email',array('email'=>$this->email))){
			$this->addError('email','Email already exists.');
		}	
	}
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Doctor the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
}
