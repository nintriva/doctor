<?php

/**
 * This is the model class for table "da_payment_account".
 *
 * The followings are the available columns in table 'da_payment_account':
 * @property integer $id
 * @property string $paypal_account_type
 * @property string $paypal_username
 * @property string $paypal_password
 * @property string $paypal_signature
 * @property string $authorizenet_account_type
 * @property string $authorizenet_api_login_id
 * @property string $authorizenet_transaction_key
 * @property string $date_created
 * @property string $date_modified
 * @property integer $status
 */
class PaymentAccount extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'da_payment_account';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('paypal_account_type, paypal_username, paypal_password, paypal_signature, authorizenet_account_type, authorizenet_api_login_id, authorizenet_transaction_key, date_created, date_modified, status', 'required'),
			array('status', 'numerical', 'integerOnly'=>true),
			array('paypal_account_type, authorizenet_account_type', 'length', 'max'=>20),
			array('paypal_username, paypal_password, paypal_signature, authorizenet_api_login_id, authorizenet_transaction_key', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, paypal_account_type, paypal_username, paypal_password, paypal_signature, authorizenet_account_type, authorizenet_api_login_id, authorizenet_transaction_key, date_created, date_modified, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'paypal_account_type' => 'Paypal Account Type',
			'paypal_username' => 'Paypal Username',
			'paypal_password' => 'Paypal Password',
			'paypal_signature' => 'Paypal Signature',
			'authorizenet_account_type' => 'Authorizenet Account Type',
			'authorizenet_api_login_id' => 'Authorizenet Api Login',
			'authorizenet_transaction_key' => 'Authorizenet Transaction Key',
			'date_created' => 'Date Created',
			'date_modified' => 'Date Modified',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('paypal_account_type',$this->paypal_account_type,true);
		$criteria->compare('paypal_username',$this->paypal_username,true);
		$criteria->compare('paypal_password',$this->paypal_password,true);
		$criteria->compare('paypal_signature',$this->paypal_signature,true);
		$criteria->compare('authorizenet_account_type',$this->authorizenet_account_type,true);
		$criteria->compare('authorizenet_api_login_id',$this->authorizenet_api_login_id,true);
		$criteria->compare('authorizenet_transaction_key',$this->authorizenet_transaction_key,true);
		$criteria->compare('date_created',$this->date_created,true);
		$criteria->compare('date_modified',$this->date_modified,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PaymentAccount the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
