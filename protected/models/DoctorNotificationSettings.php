<?php

/**
 * This is the model class for table "da_doctor_notification_settings".
 *
 * The followings are the available columns in table 'da_doctor_notification_settings':
 * @property integer $id
 * @property integer $doctor_id
 * @property string $alert_email
 * @property integer $email_notification
 * @property integer $cancel_notification
 * @property integer $confirm_notification
 * @property integer $weekly_notification
 * @property integer $monthly_notification
 * @property string $date_created
 * @property string $date_modified
 * @property integer $status
 */
class DoctorNotificationSettings extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'da_doctor_notification_settings';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('doctor_id, alert_email, email_notification, cancel_notification, confirm_notification, weekly_notification, monthly_notification, date_created, date_modified, status', 'required'),
			array('doctor_id, email_notification, cancel_notification, confirm_notification, weekly_notification, monthly_notification, status', 'numerical', 'integerOnly'=>true),
			array('alert_email', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, doctor_id, alert_email, email_notification, cancel_notification, confirm_notification, weekly_notification, monthly_notification, date_created, date_modified, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'doctor_id' => 'Doctor',
			'alert_email' => 'Alert Email',
			'email_notification' => 'Email Notification',
			'cancel_notification' => 'Cancel Notification',
			'confirm_notification' => 'Confirm Notification',
			'weekly_notification' => 'Weekly Notification',
			'monthly_notification' => 'Monthly Notification',
			'date_created' => 'Date Created',
			'date_modified' => 'Date Modified',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('doctor_id',$this->doctor_id);
		$criteria->compare('alert_email',$this->alert_email,true);
		$criteria->compare('email_notification',$this->email_notification);
		$criteria->compare('cancel_notification',$this->cancel_notification);
		$criteria->compare('confirm_notification',$this->confirm_notification);
		$criteria->compare('weekly_notification',$this->weekly_notification);
		$criteria->compare('monthly_notification',$this->monthly_notification);
		$criteria->compare('date_created',$this->date_created,true);
		$criteria->compare('date_modified',$this->date_modified,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DoctorNotificationSettings the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
