<?php

/**
 * This is the model class for table "da_contact".
 *
 * The followings are the available columns in table 'da_contact':
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property string $resume
 * @property string $cover_letter
 * @property string $date_created
 * @property string $date_modified
 * @property integer $status
 */
class Contact extends CActiveRecord
{
	public $verifyCode;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'da_contact';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, email', 'required'),
			array('status', 'numerical', 'integerOnly'=>true),
			array('name, email, phone', 'length', 'max'=>100),
			array('resume', 'length', 'max'=>255),
			// email has to be a valid email address
			array('email', 'email', 'message'=>'Email is not valid'),
			// verifyCode needs to be entered correctly
			array('verifyCode', 'captcha', 'allowEmpty'=>!CCaptcha::checkRequirements()),
			//file type rule
			array('resume', 'file', 'types'=>'pdf,doc,docx', 'on'=>'resume'),
			array('cover_letter', 'required', 'on'=>'contact'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, email, phone, resume, cover_letter, date_created, date_modified, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'email' => 'Email',
			'to_name' => 'To Name',
			'to_email' => 'To Email',
			'phone' => 'Phone',
			'resume' => 'Resume',
			'cover_letter' => 'Comments',
			'date_created' => 'Date Created',
			'date_modified' => 'Date Modified',
			'status' => 'Status',
			'verifyCode'=>'Verification Code',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('to_name',$this->to_name,true);
		$criteria->compare('to_email',$this->to_email,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('resume',$this->resume,true);
		$criteria->compare('cover_letter',$this->cover_letter,true);
		$criteria->compare('date_created',$this->date_created,true);
		$criteria->compare('date_modified',$this->date_modified,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Contact the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
