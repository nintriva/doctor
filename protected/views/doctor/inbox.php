<?php

$this->breadcrumbs=array(
	'Messaging Center'=>array('inbox'),
	'Inbox',
);
?>
<div class="main">
  <div id="breadcrumb" class="fk-lbreadbcrumb newvd">
    <!--<span><a href="">Home</a></span> >  
    <span>Dashboard</span>--> 
    <?php $this->widget('zii.widgets.CBreadcrumbs', array(
      'links'=>$this->breadcrumbs,
    ));
  ?>
  </div>
  <div class="dashboard_mainarea">
    <div class="leftmenu">
    	<?php $this->renderPartial('//layouts/navigation'); ?>
    </div>
    <div class="rightarea_dashboard">
    	<div class="dashboardcont_leftbox">
    		<h1>Inbox</h1>
        	<ul id="example2" class="accordion">
                <li>
                    <h3>
                      <span class="sender-name">Bhaskar Poddar</span>
                      <span class="sender-sub">With toggling enabled, the user is able to close down all items.</span>
                      <span class="sender-date">oct 30 2014</span>
                    </h3>
                    <div class="panel loading">
                        <p>1 Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
                        It has survived not only five centuries, but also the leap into electronic</p>
                    </div>
                </li>
                <li>
                    <h3>
                    	<span class="sender-name">Supriyo Saha</span>
                      <span class="sender-sub">With toggling enabled, the user is able to close down all items.</span>
                      <span class="sender-date">oct 30 2014</span>
                    </h3>
                    <div class="panel loading">
                        <p>2 Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of</p>
                    </div>
                </li>
                
                
            </ul>
      </div>
    </div>
  </div>
</div>