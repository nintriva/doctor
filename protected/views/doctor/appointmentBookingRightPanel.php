<div class="rightpanel">
    <div class="rightcont_sect">
        <div class="doctor">
            <?php
            //if($dataProvider->image){
            $filePath = 'assets/upload/doctor/' . $dataProvider->id . "/" . $dataProvider->image;
            if (@file_get_contents($filePath, 0, NULL, 0, 1)) {
                ?>
                <?php echo CHtml::image(Yii::app()->request->baseUrl . '/assets/upload/doctor/' . $dataProvider->id . "/" . $dataProvider->image, "image", array("width" => 74/* ,"height"=>111 */, 'class' => 'doctor-image')); ?>
            <?php } else { ?>
                <?php if ($dataProvider->gender == 'M') { ?>
                    <?php echo CHtml::image(Yii::app()->request->baseUrl . '/assets/images/avatar.png', "image", array("width" => 74, "height" => 111, 'class' => 'doctor-image')); ?>
                <?php } else { ?>
                    <a href="javascript:void(0);"><?php echo CHtml::image(Yii::app()->request->baseUrl . '/assets/images/avtar_female.png', "image", array("width" => 74, "height" => 111, 'class' => 'doctor-image')); ?></a>
                <?php } ?>
            <?php } ?>  
            <span class="doctor-info"> 
                <span class="doctor-name">
                    <font style="font-size:16px; display:block; padding-bottom:10px; color:#979797; ">Appointment With:</font>
                    <?php echo $dataProvider->title; ?> <?php echo $dataProvider->first_name; ?> <?php echo $dataProvider->last_name; ?><em>&nbsp;<?php echo ($dataProvider->degree) ? ', ' . $dataProvider->degree : ''; ?></em></span> <span class="doctor-specialty"><?php echo isset($user_speciality[$dataProvider->speciality]) ? $user_speciality[$dataProvider->speciality] : ''; ?>
                </span>
                <div class="rating rating-4_5"></div>
                <?php
                if ($default_data_address) {
                    ?>
                    <span class="doctor-address"> <span class="doctor-address-line">
                            <font style="font-size:17px; display:block; padding-bottom:10px; color:#979797; font-weight:bold; ">Appointment Where:</font>
                            <?php
                            echo $default_data_address->address;
                            ?>
                        </span>
                        <span class="doctor-address-line">
                            <?php
                            echo $default_data_address->city . ', ' . $default_data_address->zip;
                            ?>
                        </span>
                    </span>
                <?php } ?>  
            </span>
        </div>
        <div class="status-card-lower">
            <div class="status-date">
                <?php if (Yii::app()->session['start_time']) { ?>
                    <font style="font-size:17px; color:#979797;  display:block; padding-bottom:10px; "> Appointment When:</font>
                    <?php //echo date('M j, Y',Yii::app()->session['start_time']).' at '.date('h:i A (l)',Yii::app()->session['start_time']);  ?>
                    <?php echo date('l, F j - h:i A', Yii::app()->session['start_time']); ?>
                <?php } ?>
            </div>

        </div>
    </div>
</div>