<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'reset_password',
)); ?>
	<div class="dashboardcont_leftbox">
		<?php if(Yii::app()->user->hasFlash('resetPassword')): ?>
			<span class="flash-success">
				<?php echo Yii::app()->user->getFlash('resetPassword'); ?>
			</span>
		<?php endif; ?>
		 <h1>Reset Password</h1>
		 <div class="box_content">
			<div class="fld_area">
			   <div class="fld_name">Old Password</div>
			   <div class="name_fld">
			   <input id="old_password" class="fld_class" type="password" name="old_password" placeholder="Old Password" autocomplete="off" size="32" value="<?php if(isset($_REQUEST['old_password'])) echo $_REQUEST['old_password']; ?>">
			   <div class="errorMessage"><?php if(isset($error['old_password'])) echo $error['old_password']; ?></div>
			   </div>
			   <div class="clear"></div>             
			</div>
			<div class="fld_area">
			   <div class="fld_name fld_name_hight">New Password</div>
			   <div class="name_fld">
			   <input id="new_password" class="fld_class" type="password" name="new_password" placeholder="New Password" autocomplete="off" size="32" value="<?php if(isset($_REQUEST['new_password'])) echo $_REQUEST['new_password']; ?>">
			   <div class="errorMessage"><?php if(isset($error['new_password'])) echo $error['new_password']; ?></div>
			   </div>
			   <div class="clear"></div>
			</div>
			<div class="fld_area">
			   <div class="fld_name fld_name_hight">Confirm Password</div>
			   <div class="name_fld">
			   <input id="confirm_password" class="fld_class" type="password" name="confirm_password" placeholder="Confirm Password" autocomplete="off" size="32" value="<?php if(isset($_REQUEST['confirm_password'])) echo $_REQUEST['confirm_password']; ?>">
			   <div class="errorMessage"><?php if(isset($error['confirm_password'])) echo $error['confirm_password']; ?></div>
			   </div>
			   <div class="clear"></div>
			</div>
		</div>
	</div>
	<div>
		<span>
		<?php //echo CHtml::submitButton('Update',array('class'=>'registbt')); ?>
        <input class="registbt" type="button" value="Update" name="yt0" onclick="return passwordSubmit();">
		<?php echo CHtml::link('Cancel', $this->createAbsoluteUrl('doctor/index'),array('class'=>'registbt')); ?>
		</span>
	</div>
<?php $this->endWidget(); ?>