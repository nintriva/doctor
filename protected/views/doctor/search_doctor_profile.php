<?php
$this->breadcrumbs = array(
    "Doctor's Profile",
);
?>
<script src='<?php echo Yii::app()->getBaseUrl(true); ?>/assets/js/dashboard_script.js'></script>
<script type="text/javascript" src="<?php echo Yii::app()->getBaseUrl(true); ?>/assets/js/rating.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->getBaseUrl(true); ?>/assets/js/jquery.barrating.js"></script>
<link rel="stylesheet" href="<?php echo Yii::app()->getBaseUrl(true); ?>/assets/css/rating.css" type="text/css" media="screen" title="Rating CSS">
<link rel="stylesheet" href="<?php echo Yii::app()->getBaseUrl(true); ?>/assets/css/barrating.css" type="text/css" media="screen" title="Rating CSS">
<script type="text/javascript">
    $(function () {
        $('#example-0').barrating('show', {
            readonly: true
        });
        $('#example-1').barrating('show', {
            readonly: true
        });
        $('#example-2').barrating('show', {
            readonly: true
        });
        $('#example-3').barrating('show', {
            readonly: true
        });
        $('#example-4').barrating('show', {
            readonly: true
        });
    });
</script>

<div class="main">
    <div id="breadcrumb" class="fk-lbreadbcrumb newvd">
        <?php
        $this->widget('zii.widgets.CBreadcrumbs', array(
            'links' => $this->breadcrumbs,
        ));
        ?>
    </div>
    <div class="profilepage_contain">

        <!--Row #1 starts for photo, name and video-->
        <div class="profilePageRow">

            <!-- Left Column for Doctor Picture-->

            <div class="leftcol">
                <span class="photos">
                    <?php
                    $filePath = 'assets/upload/doctor/' . $doctor_list->id . "/" . $doctor_list->image;
                    if (@file_get_contents($filePath, 0, NULL, 0, 1)) {
                        ?>
                        <a href="javascript:void(0);"><?php echo CHtml::image(Yii::app()->request->baseUrl . '/assets/upload/doctor/' . $doctor_list->id . "/" . $doctor_list->image, "image", array('width' => 200/* ,'height'=>280 */)); ?></a>
                    <?php } else { ?>
                        <?php if ($doctor_list->gender == 'M') { ?>
                            <a href="javascript:void(0);"><?php echo CHtml::image(Yii::app()->request->baseUrl . '/assets/images/avatar.png', "image", array("width" => 200)); ?></a>
                        <?php } else { ?>
                            <a href="javascript:void(0);"><?php echo CHtml::image(Yii::app()->request->baseUrl . '/assets/images/avtar_female.png', "image", array("width" => 200)); ?></a>
                        <?php } ?>
                    <?php } ?>
                </span>
            </div>


            <!-- right Column for Doctor's name, address, and videos -->
            <div class="rightcol">
                <div class="locations" >
                    <h2><?php echo $doctor_list->title . " " . $doctor_list->first_name . " " . $doctor_list->last_name; ?>
                    </h2>
                    <?php echo $doctor_list->degree; ?>
                    <br>
                    <?php echo isset($user_selected_speciality[0]) ? $user_speciality[$user_selected_speciality[0]] : ""; ?>
                    <br> 
                    <br>

                    <?php if (isset($user_address[0]['practice_affiliation']) && !empty($user_address[0]['practice_affiliation'])) { ?>
                        <h3><?php echo $user_address[0]['practice_affiliation']; ?></h3>
                    <?php } ?>
                </div>
                <?php
                if (isset($user_address) && !empty($user_address[0])) {
                    $city = ($user_address[0]['city']) ? $user_address[0]['city'] : '';
                    $state = ($user_address[0]['state']) ? ', ' . $user_address[0]['state'] : '';
                    $zip = ($user_address[0]['zip']) ? ', ' . $user_address[0]['zip'] : '';
                    echo $user_address[0]['address'] . '<br>' . $city . $state . $zip;
                } else {
                    $city = ($default_data_address->city) ? $default_data_address->city : '';
                    $state = ($default_data_address->state) ? ', ' . $default_data_address->state : '';
                    $zip = ($default_data_address->zip) ? ', ' . $default_data_address->zip : '';
                    echo $default_data_address->address . '<br>' . $city . $state . $zip;
                }
                ?>

                <div class="locations">
                    <!--<h3>Average Rating</h3>-->
                    <?php
                    $overall_rating = 0;
                    $total_review = count($doctor_review);
                    foreach ($doctor_review as $doctor_review_key => $doctor_review_val) {
                        $overall_rating += $doctor_review_val->overall_rating;
                    }
                    if ($total_review == 0)
                        $average_rating = 0;
                    else {
                        $average_rating = round($overall_rating / $total_review);
                    }

					 $connection = Yii::app()->db;
					 $sqlCount="select * FROM da_doctor_speciality WHERE default_status = '1' AND doctor_id='".$doctor_list->id."'";
					 $commandCount = $connection->createCommand($sqlCount);
					 $spclArr = $commandCount->queryAll();
                    ?>                    

                    <div class="rating rating_pos_<?php echo $average_rating; ?>">
                    </div>
					
					<?php 
					//echo "<pre>";
					//print_r($user_speciality);


					if(Yii::app()->session['logged_in'] == '1' && Yii::app()->session['logged_user_type'] == 'patient') {
						?>
						<div id='favDiv'>
					<!--#######  after login my favourite part #######-->
						
						<?php
						if($this->checkFav(Yii::app()->session['logged_user_id'],$doctor_list->id,$specialityID) > 0){ ?>
						<!--####### My favourite part #######-->
							<div class="already_favorite"><a href='javascript:void(0);' title='My Favourite' onclick='delFav("<?php echo Yii::app()->session['logged_user_id']; ?>","<?php echo $doctor_list->id; ?>","<?php echo $specialityID; ?>");'><img src="<?php echo Yii::app()->getBaseUrl(true); ?>/assets/images/active-heart.png" width="16" style="float: left; margin-top: 1px; margin-right: 6px;" /><span style="margin:0">Favorite</span></a> </div>
						<!--####### End My favourite part #######-->
						<?php } else { ?>
						<!--####### Add to favourite part #######-->

							 <div class="add_to_favorite"><a href='javascript:void(0);' title='Add to Favourite' onclick='addFav("<?php echo Yii::app()->session['logged_user_id']; ?>","<?php echo $doctor_list->id; ?>","<?php echo $specialityID; ?>");'><img src="<?php echo Yii::app()->getBaseUrl(true); ?>/assets/images/inactive-heart.png" width="16" style="float: left; margin-top: 1px; margin-right: 6px;" /> <span style="margin:0">Favorite</span></a></div>
							<!--####### End Add to favourite part #######-->
						<?php } ?> 
							<!--####### Favourite count part #######-->
							<div class="box"><span style="display:block"><?php echo $this->FavCount($doctor_list->id,$specialityID); ?></span></div>
							<!--####### End Favourite count part #######-->
					<!--####### end after login my favourite part #######-->
					</div>
					<?php }else{?>
					<!--#######  Before login my favourite part #######-->
						<?php if($this->FavCount($doctor_list->id,$specialityID) > 0){ ?>
						<!--#######  Before login favourite Exist part #######-->
						<div class="already_favorite"><a id='pop_log' href='javascript:void(0)' title='Add to Favourites'><img src="<?php echo Yii::app()->getBaseUrl(true); ?>/assets/images/active-heart.png" width="16" style="float: left; margin-top: 1px; margin-right: 6px;" /> <span style="margin:0">Favorite</span></a></div>
						<!--#######  End Before login favourite Exist part #######-->
						<?php }else{?>
						<!--#######  Before login favourite not Exist part #######-->
						
						<div class="add_to_favorite"><a id='pop_log' href='javascript:void(0)' title='Add to Favourites'><img src="<?php echo Yii::app()->getBaseUrl(true); ?>/assets/images/inactive-heart.png" width="16" style="float: left; margin-top: 1px; margin-right: 6px;" /> <span style="margin:0">Favorite</span></a></div>
						<!--#######  Before login favourite not Exist part #######-->
						<?php }?>
						<!--####### Favourite count part #######-->
						<div class="box"><span style="display:block"><?php echo $this->FavCount($doctor_list->id,$specialityID); ?></span></div>
						<!--####### End Favourite count part #######-->
					<!--#######  Before login my favourite part #######-->
					<?php }?>
					</p>
                </div>



            </div>

        </div>
		<div id="popup" style="display: none; text-align: center; font-size: 16px" class="popup1">
				<div style="margin-top:70px; line-height:28px;">
					Please Login to add	<h2 style="font-weight:bold;"><?php echo $doctor_list->title . " " . $doctor_list->first_name . " " . $doctor_list->last_name; ?>
                    </h2> as your favourite doctor
				</div>
		</div>
		<script type="text/javascript">
							
								$(function(){
									$('#pop_log').click(function(){
										$('#popup').bPopup({
											contentContainer:'.content',
										});
									});
								});
							
							</script>
        <!--Row #1 ends -->
        <div class="divider"></div>
        
        <!--Row #2 starts -->
        <div class="profilePageRow">
            <!-- Specialties listing -->
            <div class="leftcol">
                <div class="locations">
                    <h3>Specialties</h3>
                    <?php
                    $speciality_count = 0;
                    $speciality = "";
                    foreach ($user_speciality as $speciality_key => $speciality_val) {
                        ?>
                        <?php
                        if (in_array($speciality_key, $user_selected_speciality)):
                            ?>
                            <span>
                                <?php
                                if ($speciality_count == 0) {
                                    $speciality = $speciality_val;
                                    $speciality_count ++;
                                } else {
                                    $speciality = $speciality . " ," . $speciality_val;
                                }
                                ?></span>
                            <?php
                        endif;
                    }
                    echo $speciality;
                    ?>
                </div>

                <!--Insurance listing-->
                <div class="locations">
                    <?php if (count($user_selected_insurance_plan_ins) > 0) { ?>
                        <h3 style="padding: 5px 0;" >Insurance Accepted</h3>
                        <?php
                        foreach ($user_selected_insurance_plan_ins as $insurance_plan_key => $insurance_plan_val) {
                            // if( count( $insurance_plan_val ) > 0 ) {
                            ?>
                            <div style="width:100%; float: left; padding: 5px 0;" >
                                <?php
                                $insurance_plan_name = Insurance::model()->getInsuranceNameById($insurance_plan_key);
                                echo '<span style="font-weight: bold; float: left; width:auto; padding-right: 10px; ">' . $insurance_plan_name . '</span>';
                                if (count($insurance_plan_val) > 0) {
                                    $plan_count = 0;
                                    $plan_name_str = "";
                                    foreach ($insurance_plan_val as $plan_key => $planVal) {
                                        if ($plan_count == 0) {
                                            $plan_name_str = $planVal;
                                            $plan_count ++;
                                        } else {
                                            $plan_name_str = $plan_name_str . " ," . $planVal;
                                        }
                                    }
                                    echo '<span style="font-weight: bold;" >: </span>' . $plan_name_str;
                                }
                                ?>
                            </div>
                            <?php
                            // }
                        }
                    }
                    ?>
                </div>


                <h3>Professional Statement</h3>
                <p><?php echo $doctor_list->comments; ?></p>

            </div>

            <div class="rightcol">
                <!--<h3 class="detailsHeader">Details</span></h3>-->
                <div class="link-column">
                    <h2>Education</h2>
                    <p><?php echo $doctor_list->medical_school; ?> <?php echo ($doctor_list->medical_school_year) ? '<strong style="font-weight:bold;">Graduation Year </strong>' . $doctor_list->medical_school_year : ''; ?></p>
                </div>
                <div class="link-column">
                    <h2>Residency Training</h2>
                    <p><?php echo $doctor_list->residency_training; ?> <?php if ($doctor_list->residency_training_year != "0000") { ?><?php echo ($doctor_list->residency_training_year) ? '<strong style="font-weight:bold;">Completion Year </strong>' . $doctor_list->residency_training_year : ''; ?><?php } ?></p>
                </div>
                <?php if (count($user_selected_language) > 0) { ?>
                    <div class="link-column">
                        <h2>Languages Spoken</h2>
                        <p>
                            <?php //echo "<pre>";print_r($user_selected_language);$user_language ?>
                            <?php //$languages_spoken = $doctor_list->languages_spoken; ?>
                            <?php
                            $lang = "";
                            $iv = 0;
                            ?>
                            <?php foreach ($user_language as $lang_key => $lang_val) { ?>
                                <?php
                                if (in_array($lang_key, $user_selected_language)) {
                                    if ($iv == 0) {
                                        $lang = $lang_val;
                                        $iv++;
                                    } else {
                                        $lang = $lang . " ," . $lang_val;
                                    }
                                }
                            }
                            ?>
                            <span><?php echo $lang; ?></span>
                        </p>
                    </div>
                <?php } ?>
                <?php if ($doctor_list->board_certifications != "") { ?>
                    <div class="link-column">
                        <h2>Board Certifications</h2>
                        <p><?php echo $doctor_list->board_certifications; ?></p>
                    </div>
                <?php } if ($doctor_list->hospital_affiliations != "") { ?>
                    <div class="link-column">
                        <h2>Hospital</h2>
                        <p><?php echo $doctor_list->hospital_affiliations; ?></p>
                    </div>
                <?php } if ($doctor_list->awards_publications != "") { ?>
                    <div class="link-column">
                        <h2>Awards and Publications</h2>
                        <p>
                            <?php echo $doctor_list->awards_publications; ?>
                        </p>
                    </div>
                <?php } ?>

                <?php if (count($user_selected_condition) > 0) { ?>
                    <h3 class="detailsHeader">Conditions Treated</h3>
                    <div class="doctor_condition">
                        <ul>
                            <?php foreach ($user_condition as $condition_key => $condition_val) { ?>
                                <?php if (in_array($condition_key, $user_selected_condition)): ?>
                                    <li><a href=""><?php echo $condition_val; ?></a></li>
                                    <?php
                                /* else:
                                  ?>
                                  <li class="inactivetag"><?php echo $condition_val; ?></li>
                                  <?php
                                 */endif;
                            }
                            ?>

                        </ul>
                    </div>
                <?php } ?>
                <div class="clear"> &nbsp;</div>
                <?php if (count($user_selected_procedure) > 0 && ( $user_selected_procedure[0] != 0 )) { ?>
                    <div style="float:left">
                        <h3 class="detailsHeader">Procedures</h3>
                        <div class="doctor_condition">
                            <ul>
                                <?php foreach ($user_procedure as $procedure_key => $procedure_val) { ?>
                                    <?php if (in_array($procedure_key, $user_selected_procedure)): ?>
                                        <li><a href=""><?php echo $procedure_val; ?></a></li>
                                        <?php
                                    endif;
                                }
                                ?>
                            </ul>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
        <!--Row #2 ends--> 
        <div class="divider"></div>
        
        
        <!--Row #3 start for map and schedule-->

            <div class="doctorprofile_appoint">
                <div class="leftpanel">

                    <div style="width:100%; height:280px; border:1px solid #ccc;" id="profile_map">
                    </div>

                </div>
                <div class="rightpanel">
                    <?php
                    if (isset($user_selected_procedure) && count($user_selected_procedure) == 0) {
                        $user_selected_procedure[0] = 0;
                    }
                    if (!empty($user_address) && isset($user_selected_speciality[0]) && isset($user_selected_procedure[0])) {
                        foreach ($user_address as $user_address_val) {
                            //print $user_address_val['id'];
                            $view_ary_sch = $this->doctorSchTime($doctor_list->id, date('Y-m-d', strtotime("+ 1 day")), date('Y-m-d', strtotime(' +7 day')), 'Y-m-d', $user_address_val['id']);
                            $doctorAppTime = $this->doctorAppTime($doctor_list->id, date('Y-m-d', strtotime("+ 1 day")), date('Y-m-d', strtotime(' +7 day')), 'Y-m-d', $user_address_val['id']);
                            $doctorTimeOff = $this->doctorTimeOff($doctor_list->id, date('Y-m-d', strtotime("+ 1 day")), date('Y-m-d', strtotime(' +7 day')), 'Y-m-d');
                            //Helpers::pre($doctorTimeOff);
                            if (!empty($view_ary_sch[$doctor_list->id])) {
                                ?>
                                <div class="address_list address_list2">
                                    <h4><img src="<?php echo Yii::app()->request->baseUrl; ?>/assets/images/address_icon.png" />
                                        <?php
                                        if (isset($user_address_val)) {
                                            $city = ($user_address_val['city']) ? $user_address_val['city'] : '';
                                            $state = ($user_address_val['state']) ? ', ' . $user_address_val['state'] : '';
                                            $zip = ($user_address_val['zip']) ? '-' . $user_address_val['zip'] : '';
                                            echo $user_address_val['address'] . ' ' . $city . $state . $zip;
                                        }
                                        ?>
                                    </h4>

                                    <div class="navarrow pre_arrow_area_prof" id="pre_arrow_area_prof_<?php echo $user_address_val['id']; ?>" onclick="appointmentSchedule('prev',<?php echo $user_address_val['id']; ?>);"><a href="javascript:void(0);"><img src="<?php echo Yii::app()->request->baseUrl; ?>/assets/images/pre_arrow_gry.png" alt="Previous" height="34" width="34" style="padding-right:6px;"></a></div>
                                    <div id="schedule_loader_prev_<?php echo $user_address_val['id']; ?>" class="schedule_loader" style="position: absolute; margin: 32px 0; display:none;">
                                        <img src="<?php echo Yii::app()->request->baseUrl; ?>/assets/images/loader.gif" />
                                    </div>
                                    <div class="calander_menu_big" id="calander_menu_big_<?php echo $user_address_val['id']; ?>">
                                        <ul>

                                            <li><?php echo date('D', (time() + 86400)); ?><font style="font-size:11px; display:block; font-weight:normal;"><?php echo date('m-d-Y', time()+86400); ?></font></li>
                                            <li><?php echo date('D', (time() + 86400) + 3600 * 24); ?><font style="font-size:11px; display:block; font-weight:normal;"><?php echo date('m-d-Y', (time()+86400) + 3600 * 24); ?></font></li>
                                            <li><?php echo date('D', (time() + 86400) + 3600 * 24 * 2); ?><font style="font-size:11px; display:block; font-weight:normal;"><?php echo date('m-d-Y', (time()+86400) + 3600 * 24 * 2); ?></font></li>
                                            <li><?php echo date('D', (time() + 86400) + 3600 * 24 * 3); ?><font style="font-size:11px; display:block; font-weight:normal;"><?php echo date('m-d-Y', (time()+86400) + 3600 * 24 * 3); ?></font></li>
                                            <li><?php echo date('D', (time() + 86400) + 3600 * 24 * 4); ?><font style="font-size:11px; display:block; font-weight:normal;"><?php echo date('m-d-Y', (time()+86400) + 3600 * 24 * 4); ?></font></li>
                                            <li><?php echo date('D', (time() + 86400) + 3600 * 24 * 5); ?><font style="font-size:11px; display:block; font-weight:normal;"><?php echo date('m-d-Y', (time()+86400) + 3600 * 24 * 5); ?></font></li>
                                            <li><?php echo date('D', (time() + 86400) + 3600 * 24 * 6); ?><font style="font-size:11px; display:block; font-weight:normal;"><?php echo date('m-d-Y', (time()+86400) + 3600 * 24 * 6); ?></font></li>

                                        </ul>
                                    </div>
                                    <div class="navarrow nxt_arrow_area_prof" onclick="appointmentSchedule('next',<?php echo $user_address_val['id']; ?>);"><a href="javascript:void(0);"><img src="<?php echo Yii::app()->request->baseUrl; ?>/assets/images/nxt_arrow_green.png" alt="Next" height="34" width="34" style="padding-left:7px;"></a></div>
                                    <div id="schedule_loader_next_<?php echo $user_address_val['id']; ?>" class="schedule_loader" style="position: absolute; margin: 32px 611px;display:none;">
                                        <img src="<?php echo Yii::app()->request->baseUrl; ?>/assets/images/loader.gif" />
                                    </div>
                                    <div class="profilesec_timetable_leftcalnd_div" id="profilesec_timetable_leftcalnd_<?php echo $user_address_val['id']; ?>">
                                        <div class="calander_box_lisiting calander_box_lisiting2">
                                            <div class="column_bar_left"></div>
                                            <?php
                                            foreach ($view_ary_sch[$doctor_list->id] as $view_ary_sch_key => $view_ary_sch_val) {
                                                ?>

                                                <?php
                                                if (!empty($view_ary_sch_val)) {
                                                    $view_ary_sch_time_cnt = false;
                                                    foreach ($view_ary_sch_val as $view_ary_sch_time) {
                                                        if (isset($view_ary_sch_time) && !empty($view_ary_sch_time))
                                                            $view_ary_sch_time_cnt = true;
                                                    }
                                                    if ($view_ary_sch_time_cnt) {
                                                        ?>
                                                        <div class="sch_time_profile">
                                                            <?php
                                                            $tot_cnt = 0;
                                                            $inner_five = 0;
                                                            foreach ($view_ary_sch_val as $view_ary_sch_time_val) {
                                                                if ($tot_cnt < 5) {
                                                                    foreach ($view_ary_sch_time_val as $view_ary_sch_time_val_arr) {
                                                                        if ($view_ary_sch_time_val_arr['day'] == date('l', strtotime($view_ary_sch_key))) {
                                                                            if ($view_ary_sch_time_val_arr['on_off'] == 1) {
                                                                                $view_ary_to_strtotime = $view_ary_sch_time_val_arr['to_strtotime'];
                                                                                $view_ary_from_strtotime = $view_ary_sch_time_val_arr['from_strtotime'];
                                                                                $time_diff = $view_ary_to_strtotime - $view_ary_from_strtotime;
                                                                                $view_ary_time_slot = $view_ary_sch_time_val_arr['time_slot'] * 60;
                                                                                $view_ary_laser_slot = $view_ary_sch_time_val_arr['laser_slot'] * 60;

                                                                                for ($iCnt = 0; $iCnt < 5; $iCnt++) {
                                                                                    $added_time = $view_ary_time_slot + $view_ary_laser_slot;
                                                                                    $show_time = $view_ary_from_strtotime + ($added_time * $iCnt);
                                                                                    if ($show_time < $view_ary_to_strtotime) {
                                                                                        $tot_cnt++;
                                                                                        $inner_five++;
                                                                                        if ($tot_cnt <= 5) {
                                                                                            if ($iCnt == 0 && $inner_five < 5) {
                                                                                                if (isset($doctorAppTime[$doctor_list->id][$view_ary_sch_key]) && in_array($view_ary_sch_time_val_arr['from_strtotime'], $doctorAppTime[$doctor_list->id][$view_ary_sch_key])) {
                                                                                                    echo '<span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">' . date('h:i  a', $view_ary_sch_time_val_arr['from_strtotime']) . '</a></span>';
                                                                                                } else {
                                                                                                    if (isset($doctorTimeOff[$doctor_list->id][$view_ary_sch_key]) && $view_ary_sch_time_val_arr['from_strtotime'] >= $doctorTimeOff[$doctor_list->id][$view_ary_sch_key][0]['strt_time_off'] && $view_ary_sch_time_val_arr['from_strtotime'] <= $doctorTimeOff[$doctor_list->id][$view_ary_sch_key][0]['to_time_off']) {
                                                                                                        echo '<span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">' . date('h:i  a', $view_ary_sch_time_val_arr['from_strtotime']) . '</a></span>';
                                                                                                    } else {
                                                                                                        echo '<span class="innertable_time_first"><a href="' . $this->createAbsoluteUrl('doctor/DoctorBookStep1/?start_time=' . $view_ary_sch_time_val_arr['from_strtotime'] . '&time_slot=' . $view_ary_time_slot . '&doctor_id=' . $doctor_list->id . '&address_id=' . $view_ary_sch_time_val_arr['address_id'] . '&speciality_id=' . $user_selected_speciality[0]) . '&procedure_id=' . $user_selected_procedure[0] . '">' . date('h:i  a', $view_ary_sch_time_val_arr['from_strtotime']) . '</a></span>';
                                                                                                    }
                                                                                                }
                                                                                            } elseif ($iCnt > 0 && $iCnt < 4 && $inner_five < 5) {
                                                                                                if ($show_time > $view_ary_to_strtotime) {
                                                                                                    echo '<span class="innertable_time_first disable"><a class="blank"  href="javascript:void(0);">' . date('h:i a', $show_time) . '</a></span>';
                                                                                                } else {
                                                                                                    if (isset($doctorAppTime[$doctor_list->id][$view_ary_sch_key]) && in_array($show_time, $doctorAppTime[$doctor_list->id][$view_ary_sch_key])) {
                                                                                                        echo '<span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">' . date('h:i  a', $show_time) . '</a></span>';
                                                                                                    } else {
                                                                                                        if (isset($doctorTimeOff[$doctor_list->id][$view_ary_sch_key]) && $show_time >= $doctorTimeOff[$doctor_list->id][$view_ary_sch_key][0]['strt_time_off'] && $show_time <= $doctorTimeOff[$doctor_list->id][$view_ary_sch_key][0]['to_time_off']) {
                                                                                                            echo '<span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">' . date('h:i  a', $show_time) . '</a></span>';
                                                                                                        } else {
                                                                                                            echo '<span class="innertable_time_first"><a href="' . $this->createAbsoluteUrl('doctor/DoctorBookStep1/?start_time=' . $show_time . '&time_slot=' . $view_ary_time_slot . '&doctor_id=' . $doctor_list->id . '&address_id=' . $view_ary_sch_time_val_arr['address_id'] . '&speciality_id=' . $user_selected_speciality[0]) . '&procedure_id=' . $user_selected_procedure[0] . '">' . date('h:i a', $show_time) . '</a></span>';
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                            } elseif ($iCnt == 4) {
                                                                                                echo "<span class=\"innertable_time_first\"><a href=\"javascript:void(0);\" class=\"popupApp\" data-id=\"" . $view_ary_sch_time_val_arr['address_id'] . "\" accesskey=\"" . strtotime($view_ary_sch_key) . "\" >more...</a></span>";
                                                                                            }

                                                                                            if ($inner_five == 5 && $iCnt < 4) {
                                                                                                echo "<span class=\"innertable_time_first\"><a href=\"javascript:void(0);\"  class=\"popupApp\" data-id=\"" . $view_ary_sch_time_val_arr['address_id'] . "\" accesskey=\"" . strtotime($view_ary_sch_key) . "\">more...</a></span>";
                                                                                            }
                                                                                        }
                                                                                    } else {
                                                                                        
                                                                                    }
                                                                                }
                                                                            } else {
                                                                                $tot_cnt = 5;
                                                                                echo '<span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">08:00 am</a></span>
												<span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">08:30 am</a></span>
												<span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">09:00 am</a></span>
												<span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">09:30 am</a></span>
												<span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">10:00 am</a></span>';
                                                                            }
                                                                        }
                                                                    }
                                                                } else {
                                                                    break;
                                                                }
                                                            }
                                                            if ($inner_five == 1) {
                                                                echo '<span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">08:00 am</a></span>
												<span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">08:30 am</a></span>
												<span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">09:00 am</a></span>
									<span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">09:30 am</a></span>';
                                                            } else if ($inner_five == 2) {
                                                                echo '<span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">08:00 am</a></span>
												<span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">08:30 am</a></span>
									<span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">09:00 am</a></span>';
                                                            } else if ($inner_five == 3) {
                                                                echo '<span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">08:00 am</a></span>
									<span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">08:30 am</a></span>';
                                                            } else if ($inner_five == 4) {
                                                                echo '<span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">08:30 am</a></span>';
                                                            }
                                                            ?>
                                                        </div>
                                                        <?php
                                                    } else {
                                                        ?>
                                                        <div class="sch_time">
                                                            <span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">08:00 am</a></span>
                                                            <span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">08:30 am</a></span>
                                                            <span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">09:00 am</a></span>
                                                            <span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">09:30 am</a></span>
                                                            <span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">more...</a></span>
                                                            <?php //echo "<span class=\"innertable_time_first\"><a href=\"".$this->createAbsoluteUrl("doctor/searchDoctorProfile/id/".$doctor_list->id."/app_date/".strtotime($view_ary_sch_key))."\" target=\"_blank\">more...</a></span>";   ?>
                                                        </div>
                                                        <?php
                                                    }
                                                    ?>
                                                    <?php
                                                } else {
                                                    ?>
                                                    <div class="sch_time">
                                                        <span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">08:00 am</a></span>
                                                        <span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">08:30 am</a></span>
                                                        <span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">09:00 am</a></span>
                                                        <span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">09:30 am</a></span>
                                                        <span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">more...</a></span>
                                                        <?php //echo "<span class=\"innertable_time_first\"><a href=\"".$this->createAbsoluteUrl("doctor/searchDoctorProfile/id/".$doctor_list->id."/app_date/".strtotime($view_ary_sch_key))."\" target=\"_blank\">more...</a></span>";    ?>
                                                    </div>
                                                    <?php
                                                }
                                                ?>

                                                <?php
                                            }
                                            ?>
                                            <div class="column_bar_right"></div>
                                        </div>
                                    </div>
                                </div>
                            <?php } else { ?>
                                <div class="address_list address_list2">
                                    <h4><img src="<?php echo Yii::app()->request->baseUrl; ?>/assets/images/address_icon.png" />
                                        <?php //echo $user_address_val['address']; ?>
                                        <?php
                                        if (isset($user_address_val)) {
                                            $city = ($user_address_val['city']) ? $user_address_val['city'] : '';
                                            $state = ($user_address_val['state']) ? ', ' . $user_address_val['state'] : '';
                                            $zip = ($user_address_val['zip']) ? '-' . $user_address_val['zip'] : '';
                                            echo $user_address_val['address'] . ' ' . $city . $state . $zip;
                                        }
                                        ?>
                                    </h4>
                                    <div class="navarrow pre_arrow_area_prof"><a href="javascript:void(0);"><img src="<?php echo Yii::app()->request->baseUrl; ?>/assets/images/pre_arrow_gry.png" alt="Previous" height="34" width="34" style="padding-right:6px;"></a></div>
                                    <div class="calander_menu_big">
                                        <ul>
                                            <li><?php echo date('D', (time() + 86400)); ?><font style="font-size:11px; display:block; font-weight:normal;"><?php echo date('m-d-Y', time()); ?></font></li>
                                            <li><?php echo date('D', (time() + 86400) + 3600 * 24); ?><font style="font-size:11px; display:block; font-weight:normal;"><?php echo date('m-d-Y', time() + 3600 * 24); ?></font></li>
                                            <li><?php echo date('D', (time() + 86400) + 3600 * 24 * 2); ?><font style="font-size:11px; display:block; font-weight:normal;"><?php echo date('m-d-Y', time() + 3600 * 24 * 2); ?></font></li>
                                            <li><?php echo date('D', (time() + 86400) + 3600 * 24 * 3); ?><font style="font-size:11px; display:block; font-weight:normal;"><?php echo date('m-d-Y', time() + 3600 * 24 * 3); ?></font></li>
                                            <li><?php echo date('D', (time() + 86400) + 3600 * 24 * 4); ?><font style="font-size:11px; display:block; font-weight:normal;"><?php echo date('m-d-Y', time() + 3600 * 24 * 4); ?></font></li>
                                            <li><?php echo date('D', (time() + 86400) + 3600 * 24 * 5); ?><font style="font-size:11px; display:block; font-weight:normal;"><?php echo date('m-d-Y', time() + 3600 * 24 * 5); ?></font></li>
                                            <li><?php echo date('D', (time() + 86400) + 3600 * 24 * 6); ?><font style="font-size:11px; display:block; font-weight:normal;"><?php echo date('m-d-Y', time() + 3600 * 24 * 6); ?></font></li>
                                        </ul>
                                    </div>
                                    <div class="navarrow nxt_arrow_area_prof"><a href="javascript:void(0);"><img src="<?php echo Yii::app()->request->baseUrl; ?>/assets/images/nxt_arrow_gry.png" alt="Next" height="34" width="34" style="padding-left:7px;"></a></div>
                                    <div class="calander_box_lisiting calander_box_lisiting2">
                                        <div class="column_bar_left"></div>
                                        <div class="sch_time_profile">
                                            <span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">09:00AM</a></span>
                                            <span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">09:30AM</a></span>
                                            <span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">10:00AM</a></span>
                                            <span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">10:30AM</a></span>
                                            <span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">11:00AM</a></span>
                                        </div>
                                        <div class="sch_time_profile">
                                            <span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">09:00AM</a></span>
                                            <span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">09:30AM</a></span>
                                            <span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">10:00AM</a></span>
                                            <span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">10:30AM</a></span>
                                            <span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">11:00AM</a></span>
                                        </div>
                                        <div class="sch_time_profile">
                                            <span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">09:00AM</a></span>
                                            <span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">09:30AM</a></span>
                                            <span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">10:00AM</a></span>
                                            <span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">10:30AM</a></span>
                                            <span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">11:00AM</a></span>
                                        </div>
                                        <div class="sch_time_profile">
                                            <span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">09:00AM</a></span>
                                            <span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">09:30AM</a></span>
                                            <span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">10:00AM</a></span>
                                            <span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">10:30AM</a></span>
                                            <span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">11:00AM</a></span>
                                        </div>
                                        <div class="sch_time_profile">
                                            <span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">09:00AM</a></span>
                                            <span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">09:30AM</a></span>
                                            <span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">10:00AM</a></span>
                                            <span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">10:30AM</a></span>
                                            <span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">11:00AM</a></span>
                                        </div>
                                        <div class="sch_time_profile">
                                            <span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">09:00AM</a></span>
                                            <span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">09:30AM</a></span>
                                            <span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">10:00AM</a></span>
                                            <span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">10:30AM</a></span>
                                            <span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">11:00AM</a></span>
                                        </div>
                                        <div class="sch_time_profile">
                                            <span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">09:00AM</a></span>
                                            <span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">09:30AM</a></span>
                                            <span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">10:00AM</a></span>
                                            <span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">10:30AM</a></span>
                                            <span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">11:00AM</a></span>
                                        </div>
                                        <div class="column_bar_right"></div>
                                    </div>
                                </div>
                            <?php } ?>
                        <?php } ?>
                    <?php } else { ?>
                        <div class="address_list address_list2">
                            <h4>
                                <img src="<?php echo Yii::app()->request->baseUrl; ?>/assets/images/address_icon.png" />
                                <?php
                                if (isset($user_address) && !empty($user_address[0])) {
                                    $city = ($user_address[0]['city']) ? $user_address[0]['city'] : '';
                                    $state = ($user_address[0]['state']) ? ', ' . $user_address[0]['state'] : '';
                                    $zip = ($user_address[0]['zip']) ? '-' . $user_address[0]['zip'] : '';
                                    echo $user_address[0]['address'] . ' ' . $city . $state . $zip;
                                } else {
                                    $city = ($default_data_address->city) ? $default_data_address->city : '';
                                    $state = ($default_data_address->state) ? ', ' . $default_data_address->state : '';
                                    $zip = ($default_data_address->zip) ? ', ' . $default_data_address->zip : '';
                                    echo $default_data_address->address . ' ' . $city . $state . $zip;
                                }
                                ?>
                            </h4>
                            <div class="navarrow pre_arrow_area_prof"><a href="javascript:void(0);"><img src="<?php echo Yii::app()->request->baseUrl; ?>/assets/images/pre_arrow_gry.png" alt="Previous" height="34" width="34" style="padding-right:6px;"></a></div>
                            <div class="calander_menu_big">
                                <ul>
                                    <li><?php echo date('D', (time() + 86400)); ?><font style="font-size:11px; display:block; font-weight:normal;"><?php echo date('m-d-Y', time()); ?></font></li>
                                    <li><?php echo date('D', (time() + 86400) + 3600 * 24); ?><font style="font-size:11px; display:block; font-weight:normal;"><?php echo date('m-d-Y', time() + 3600 * 24); ?></font></li>
                                    <li><?php echo date('D', (time() + 86400) + 3600 * 24 * 2); ?><font style="font-size:11px; display:block; font-weight:normal;"><?php echo date('m-d-Y', time() + 3600 * 24 * 2); ?></font></li>
                                    <li><?php echo date('D', (time() + 86400) + 3600 * 24 * 3); ?><font style="font-size:11px; display:block; font-weight:normal;"><?php echo date('m-d-Y', time() + 3600 * 24 * 3); ?></font></li>
                                    <li><?php echo date('D', (time() + 86400) + 3600 * 24 * 4); ?><font style="font-size:11px; display:block; font-weight:normal;"><?php echo date('m-d-Y', time() + 3600 * 24 * 4); ?></font></li>
                                    <li><?php echo date('D', (time() + 86400) + 3600 * 24 * 5); ?><font style="font-size:11px; display:block; font-weight:normal;"><?php echo date('m-d-Y', time() + 3600 * 24 * 5); ?></font></li>
                                    <li><?php echo date('D', (time() + 86400) + 3600 * 24 * 6); ?><font style="font-size:11px; display:block; font-weight:normal;"><?php echo date('m-d-Y', time() + 3600 * 24 * 6); ?></font></li>
                                </ul>
                            </div>
                            <div class="navarrow nxt_arrow_area_prof"><a href="javascript:void(0);"><img src="<?php echo Yii::app()->request->baseUrl; ?>/assets/images/nxt_arrow_gry.png" alt="Next" height="34" width="34" style="padding-left:7px;"></a></div>
                            <div class="calander_box_lisiting calander_box_lisiting2">
                                <div class="column_bar_left"></div>
                                <div class="sch_time_profile">
                                    <span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">09:00AM</a></span>
                                    <span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">09:30AM</a></span>
                                    <span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">10:00AM</a></span>
                                    <span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">10:30AM</a></span>
                                    <span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">11:00AM</a></span>
                                </div>
                                <div class="sch_time_profile">
                                    <span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">09:00AM</a></span>
                                    <span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">09:30AM</a></span>
                                    <span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">10:00AM</a></span>
                                    <span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">10:30AM</a></span>
                                    <span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">11:00AM</a></span>
                                </div>
                                <div class="sch_time_profile">
                                    <span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">09:00AM</a></span>
                                    <span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">09:30AM</a></span>
                                    <span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">10:00AM</a></span>
                                    <span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">10:30AM</a></span>
                                    <span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">11:00AM</a></span>
                                </div>
                                <div class="sch_time_profile">
                                    <span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">09:00AM</a></span>
                                    <span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">09:30AM</a></span>
                                    <span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">10:00AM</a></span>
                                    <span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">10:30AM</a></span>
                                    <span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">11:00AM</a></span>
                                </div>
                                <div class="sch_time_profile">
                                    <span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">09:00AM</a></span>
                                    <span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">09:30AM</a></span>
                                    <span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">10:00AM</a></span>
                                    <span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">10:30AM</a></span>
                                    <span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">11:00AM</a></span>
                                </div>
                                <div class="sch_time_profile">
                                    <span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">09:00AM</a></span>
                                    <span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">09:30AM</a></span>
                                    <span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">10:00AM</a></span>
                                    <span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">10:30AM</a></span>
                                    <span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">11:00AM</a></span>
                                </div>
                                <div class="sch_time_profile">
                                    <span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">09:00AM</a></span>
                                    <span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">09:30AM</a></span>
                                    <span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">10:00AM</a></span>
                                    <span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">10:30AM</a></span>
                                    <span class="innertable_time_first disable"><a class="blank" href="javascript:void(0);">11:00AM</a></span>
                                </div>
                                <div class="column_bar_right"></div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>

        <!--Row #3 ends-->
        
        <!--Row #4 start for reviews-->
        <div class="profilePageRow">

            <div class="reviewsColumn">

                <h2 style="color: #333333;font-size: 16px;font-weight: bold;padding-bottom: 16px;">Reviews</h2>


                <?php if (empty($doctor_review)) { ?>
                    No reviews available for the doctor yet. <BR><BR>
                    <?php if (Yii::app()->session['logged_user_type'] == 'patient') { ?>
                        Be the first one to share a review for the doctor!!
                    <?php } else { ?>
                        We are working hard to obtain reviews from users like yourself. 
                        If you have a review to share, login with your eDoctorBook credentials.
                        <?php } ?>
                <?php } ?>

                <?php if (!empty($doctor_review)) { ?>

                    <?php foreach ($doctor_review as $doctor_review_key => $doctor_review_val) { ?>
                        
                        <div class="divider"></div>
                        <br>
                        <div class="Profile_ReviewsBox">
                            <!--
            <h2>Patient Reviews <span>for <?php //echo $doctor_list->title." ".$doctor_list->first_name." ".$doctor_list->last_name;                  ?>, <?php //echo $doctor_list->degree;                  ?> ,<?php //echo ($doctor_list->speciality)?$user_speciality[$doctor_list->speciality]:"";                  ?></span></h2>
                            -->
                            <div class="whenWho"> Review by <?php echo $doctor_review_val->patient_details->user_first_name . ' ' . $doctor_review_val->patient_details->user_last_name; ?> on <?php echo date('F d, Y', strtotime($doctor_review_val->date_created)); ?> </div>
                            <div class="rec">
                                <div class="rating rating_pos_<?php echo $doctor_review_val->schedul_appoitment_rating; ?>"></div>
                                <div class="explanation">Appointment Scheduling</div>
                            </div>
                            <div class="rec bedman">
                                <div class="rating rating_pos_<?php echo $doctor_review_val->office_experience_rating; ?>"></div>
                                <div class="explanation"> Office Experience  </div>
                            </div>
                            <div class="rec bedman nobdrlast">
                                <div class="rating rating_pos_<?php echo $doctor_review_val->bedside_manner_rating; ?>"></div>
                                <div class="explanation"> Listening and Answering Questions </div>
                            </div>
                            <div class="rec">
                                <div class="rating rating_pos_<?php echo $doctor_review_val->spent_time_rating; ?>"></div>
                                <div class="explanation"> Time Spent with you </div>
                            </div>
                            <div class="rec bedman">
                                <div class="rating rating_pos_<?php echo $doctor_review_val->overall_rating; ?>"></div>
                                <div class="explanation"> Overall Rating  </div>
                            </div>
                            <div class="rec bedman nobdrlast">
                                <div id="slider-range-min_<?php echo $doctor_review_key; ?>"></div>
                                <?php
                                $doctor_review_val->wait_time_rating;
                                if ($doctor_review_val->wait_time_rating == 0) {
                                    $msg = "Exact";
                                } else if ($doctor_review_val->wait_time_rating > 0 && $doctor_review_val->wait_time_rating < 16) {
                                    $msg = "Under 15 Min";
                                } else if ($doctor_review_val->wait_time_rating > 15 && $doctor_review_val->wait_time_rating < 31) {
                                    $msg = "15-30 Min";
                                } else if ($doctor_review_val->wait_time_rating > 30 && $doctor_review_val->wait_time_rating < 46) {
                                    $msg = "30-45 Min";
                                } else {
                                    $msg = "More Then 45 Min";
                                }
                                ?>
                                <div class="explanation_<?php echo $doctor_review_key; ?>"><?php echo $msg; ?></div>
                                <div class="explanation"> Wait Time</div>
                            </div>
                            <script>
                                $("#slider-range-min_<?php echo $doctor_review_key; ?>").slider({
                                    range: "min",
                                    value: <?php echo $doctor_review_val->wait_time_rating; ?>,
                                    min: 0,
                                    max: 60,
                                    slide: function (event, ui) {
                                    }
                                }).slider({disabled: true});
                                $("a.ui-slider-handle").attr('href', 'javascript:void(0)');

                            </script>
                            <!--
            <div class="rec bedman nobdrlast">
                                    <div class="input select rating-b">
                                            <select id="example-<?php //echo $doctor_review_key;                  ?>" name="rating">
                                                    <option value="&nbsp;" <?php //if($doctor_review_val->wait_time_rating ==0 ){ echo "selected"; }                  ?> >&nbsp;</option>
                                                    <option value="Under 15 Min" <?php //if($doctor_review_val->wait_time_rating >0&&$doctor_review_val->wait_time_rating <15 ){ echo "selected"; }                  ?> >&nbsp;</option>
                                                    <option value="15-30 Min" <?php //if($doctor_review_val->wait_time_rating >14 && $doctor_review_val->wait_time_rating <31){ echo "selected"; }                  ?>>&nbsp;</option>
                                                    <option value="15-30 Min" <?php //if($doctor_review_val->wait_time_rating >30 && $doctor_review_val->wait_time_rating <46){ echo "selected"; }                  ?> >&nbsp;</option>
                                                    <option value="More Then 45 Min" <?php //if($doctor_review_val->wait_time_rating >45){ echo "selected"; }                  ?> >&nbsp;</option>
                                            </select>
                                    </div>
                                    <div class="clear"></div>
                <div class="explanation"> Wait Time </div>
            </div>
                            -->
                            <div class="comments" style="padding-left: 6px;margin-top:3%;display:inline-block;" >
                                <b>Comments:</b> <?php echo $doctor_review_val->message; ?>
                                <p>&nbsp;
                                <p>
                            </div>
                        </div>
                        
                    <?php } ?>
                <?php } ?>

                <div style="float:left; display:none" id="div1">
                    <h3>Rating</h3>
                    <form name="rating" method="post">
                        <span class="review">
                            <div class="title">Ease of Scheduling appointment</div>
                            <div class="star_img">
                                <div class="star_container">
                                    <section class="container" id="contaner_scheduling_appointment" >
                                        <input type="radio" name="example" class="rating_star" value="1" />
                                        <input type="radio" name="example" class="rating_star" value="2" />
                                        <input type="radio" name="example" class="rating_star" value="3" />
                                        <input type="radio" name="example" class="rating_star" value="4" />
                                        <input type="radio" name="example" class="rating_star" value="5" />
                                    </section>
                                </div>
                                <div class="star_value">
                                    <div id="rating_html_scheduling_appointment"></div>
                                    <input type="hidden" id="rating_html_hidden_scheduling_appointment">
                                </div>
                            </div>
                        </span>
                        <span class="review">
                            <div class="title">office experience (Comfort,<br>Friendliness,Cleanliness)</div>
                            <div class="star_img">
                                <div class="star_container">
                                    <section class="container" id="contaner_office_experience" >
                                        <input type="radio" name="example" class="rating_star" value="1" />
                                        <input type="radio" name="example" class="rating_star" value="2" />
                                        <input type="radio" name="example" class="rating_star" value="3" />
                                        <input type="radio" name="example" class="rating_star" value="4" />
                                        <input type="radio" name="example" class="rating_star" value="5" />
                                    </section>
                                </div>
                                <div class="star_value">
                                    <div id="rating_html_office_experience"></div>
                                    <input type="hidden" id="rating_html_hidden_office_experience">
                                </div>
                            </div>
                        </span>
                        <span class="review">
                            <div class="title">How well practitioner listened<br>and answered your questions</div>
                            <div class="star_img">
                                <div class="star_container">
                                    <section class="container" id="contaner_beside_manner_rating" >
                                        <input type="radio" name="example" class="rating_star" value="1" />
                                        <input type="radio" name="example" class="rating_star" value="2" />
                                        <input type="radio" name="example" class="rating_star" value="3" />
                                        <input type="radio" name="example" class="rating_star" value="4" />
                                        <input type="radio" name="example" class="rating_star" value="5" />
                                    </section>
                                </div>
                                <div class="star_value">
                                    <div id="rating_html_beside_manner_rating"></div>
                                    <input type="hidden" id="rating_html_hidden_beside_manner_rating">
                                </div>
                            </div>
                        </span>
                        <span class="review">
                            <div class="title">Spent Appropriate time<br>with you</div>
                            <div class="star_img">
                                <div class="star_container">
                                    <section class="container" id="contaner_spent_time_rating" >
                                        <input type="radio" name="example" class="rating_star" value="1" />
                                        <input type="radio" name="example" class="rating_star" value="2" />
                                        <input type="radio" name="example" class="rating_star" value="3" />
                                        <input type="radio" name="example" class="rating_star" value="4" />
                                        <input type="radio" name="example" class="rating_star" value="5" />
                                    </section>
                                </div>
                                <div class="star_value">
                                    <div id="rating_html_spent_time_rating"></div>
                                    <input type="hidden" id="rating_html_hidden_spent_time_rating">
                                </div>
                            </div>
                        </span>
                        <span class="review">
                            <div class="title">Overall rating for the<br>practitioner</div>
                            <div class="star_img">
                                <div class="star_container">
                                    <section class="container" id="contaner_overall_rating" >
                                        <input type="radio" name="example" class="rating_star" value="1" />
                                        <input type="radio" name="example" class="rating_star" value="2" />
                                        <input type="radio" name="example" class="rating_star" value="3" />
                                        <input type="radio" name="example" class="rating_star" value="4" />
                                        <input type="radio" name="example" class="rating_star" value="5" />
                                    </section>
                                </div>
                                <div class="star_value">
                                    <div id="rating_html_overall_rating"></div>
                                    <input type="hidden" id="rating_html_hidden_overall_rating">
                                </div>
                            </div>
                        </span>
                        <!--
   <span class="review">
       <label>Wait Time</label>
       <select name="wait_time_rating" id="wait_time_rating">
           <option value="0" selected="selected">Rate Now</option>
           <option value="1">1</option>
           <option value="2">2</option>
           <option value="3">3</option>
           <option value="4">4</option>
           <option value="5">5</option>
       </select>
   </span>
   <span class="review">
       <label>Title</label>
       <input type="text" name="title" id="review_title" placeholder="Title">
   </span>
                        -->

                        <span class="review">
                            <div class="title">Total Wait Time</div>
                            <div class="star_img">
                                <div id="slider-range-min"></div>
                                <div id="total_wait_time_rating" style="border:0; color:#f6931f; font-weight:bold;">Slide for wait times</div>
                                <input type="hidden" id="total_wait_time" >
                            </div>
                        </span>
                        <span class="review">
                            <label>Comments:</label>
                            <textarea name="message" id="review_message" cols="" rows="" style="width: 83%; height: 10%; resize: none;" ></textarea>
                        </span>
                        <span class="review"><input onclick="patientReview();" type="button" name="review" value="Click to submit your review" class="registbt"></span>
                    </form>
                </div>
                <?php if (Yii::app()->session['logged_user_type'] == 'patient') { ?>
                    <div id="send_rate_shw_hid">
                        <br>
                        <span class="review">
                            <input onclick="show2();" type="button" name="review" value="Click to submit your review" class="registbt">
                        </span>
                    </div>
                <?php } ?>
            </div>
        </div>
        <!--Row #4 ends-->
        <div class="divider"></div>

        <!-- Row #5 starts for social media icons -->
        <!--<div class="profilePageRow">
            <div class="leftcol">
                <br>
                <img src="<?php //echo Yii::app()->request->baseUrl; ?>/assets/images/share_icon.png" alt="">
                <br>
            </div>
        </div>--> 
        <!--Row #5 ends--> 
    </div>
</div>




<script>
    var user_location;
    function getLatLong(address) {
        var geocoder = new google.maps.Geocoder();
        var result = [];
        geocoder.geocode({'address': address/*, 'region': 'uk'*/}, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                user_location = results[0].geometry.location;
            } else {
                alert("Unable to find address: " + status);
            }
        });
    }
</script>
<script>
    function addslashes(string) {
        return string.replace(/\\/g, '\\\\').
                replace(/\u0008/g, '\\b').
                replace(/\t/g, '\\t').
                replace(/\n/g, '\\n').
                replace(/\f/g, '\\f').
                replace(/\r/g, '\\r').
                replace(/'/g, '\\\'').
                replace(/"/g, '\\"');
    }
    function initialize() {

        var bounds = new google.maps.LatLngBounds();
        var loc;
        var mapOptions = {
            zoom: 13,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            mapTypeControl: false, //map change
            streetViewControl: false,
            center: new google.maps.LatLng(<?php echo isset($user_address[0]['latitude']) && ($user_address[0]['latitude'] != '') ? $user_address[0]['latitude'] : 22.4871067; ?>,<?php echo isset($user_address[0]['longitude']) && ($user_address[0]['longitude'] != '') ? $user_address[0]['longitude'] : 88.3131307; ?>)
        };
        var map = new google.maps.Map(document.getElementById('profile_map'), mapOptions);//map change
<?php
if ($user_address) {
    for ($i = 0; $i < count($user_address); $i++) {
        ?>
                var pinIcon_<?php echo $i + 1; ?> = new google.maps.MarkerImage(
                        "http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=<?php echo $i + 1; ?>|ccc|000000",
                        new google.maps.Size(35, 35)
                        );
                var pinIcon_over_<?php echo $i + 1; ?> = new google.maps.MarkerImage(
                        "http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=<?php echo $i + 1; ?>|aec|000000",
                        new google.maps.Size(35, 35)
                        );
                var user_location_lat_<?php echo $user_address[$i]['id']; ?> = <?php echo ($user_address[$i]['latitude'] != '') ? $user_address[$i]['latitude'] : 22.4871067; ?>;
                var user_location_lng_<?php echo $user_address[$i]['id']; ?> = <?php echo ($user_address[$i]['longitude'] != '') ? $user_address[$i]['longitude'] : 88.3131307; ?>;

                loc = new google.maps.LatLng(user_location_lat_<?php echo $user_address[$i]['id']; ?>, user_location_lng_<?php echo $user_address[$i]['id']; ?>);
                var marker_id_<?php echo $user_address[$i]['id']; ?> = new google.maps.Marker({
                    map: map,
                    streetViewControl: false,
                    icon: pinIcon_<?php echo $i + 1; ?>,
                    title: "<?php //echo $user_address[$i]['address'];                     ?>",
                    position: new google.maps.LatLng(user_location_lat_<?php echo $user_address[$i]['id']; ?>, user_location_lng_<?php echo $user_address[$i]['id']; ?>),
                    draggable: false
                });
                var str_user_add = addslashes("<?php echo $user_address[$i]['address']; ?>");
                var str_user_img = '';
        <?php
        $filePath = 'assets/upload/doctor/' . $doctor_list->id . "/" . $doctor_list->image;
        if (@file_get_contents($filePath, 0, NULL, 0, 1)) {
            ?>
                    str_user_img = '<?php echo CHtml::image(Yii::app()->request->baseUrl . '/assets/upload/doctor/' . $doctor_list->id . "/" . $doctor_list->image, "image", array("width" => 80)); ?>';
        <?php } else { ?>
            <?php if ($doctor_list->gender == 'M') { ?>
                        str_user_img = '<?php echo CHtml::image(Yii::app()->request->baseUrl . '/assets/images/avatar.png', "image", array("width" => 80)); ?>';
            <?php } else { ?>
                        str_user_img = '<?php echo CHtml::image(Yii::app()->request->baseUrl . '/assets/images/avtar_female.png', "image", array("width" => 80)); ?>';
            <?php } ?>
        <?php } ?>
                var str_user_booking = '<a href="<?php echo $this->createAbsoluteUrl('doctor/searchDoctorProfile/id/' . $doctor_list->id); ?>" target="_blank" class="mapbook">Book Online</a>';
                var contentString_click_<?php echo $user_address[$i]['id']; ?> = '<div><div style="width:250px; overflow:hidden; border-radius:3px;"><span style="display: block; float: left; width:40%;">' + str_user_img + '</span><span class="profile_brief"><?php echo $doctor_list->title . " " . $doctor_list->first_name . " " . $doctor_list->last_name; ?>, <font style="font-family: cursive,Courier,monospace;"><?php echo $doctor_list->degree; ?></font><br><p><span style="font-size: 14px;"> <?php echo ($doctor_list->speciality) ? $user_speciality[$doctor_list->speciality] : ""; ?><?php //echo isset($user_speciality[$doctor_list[$i]['ds_speciality_id']])?$user_speciality[$doctor_list[$i]['ds_speciality_id']]:'';                     ?>.</span><br><br>' + str_user_add + '</span></div></div>';
                var infowindow_content_<?php echo $user_address[$i]['id']; ?> = new google.maps.InfoWindow({
                    content: contentString_click_<?php echo $user_address[$i]['id']; ?>
                });
                google.maps.event.addListener(marker_id_<?php echo $user_address[$i]['id']; ?>, 'click', function () {
                    infowindow_content_<?php echo $user_address[$i]['id']; ?>.open(map, marker_id_<?php echo $user_address[$i]['id']; ?>);
                    marker_id_<?php echo $user_address[$i]['id']; ?>.setIcon(pinIcon_over_<?php echo $i + 1; ?>);
                });
                google.maps.event.addListener(infowindow_content_<?php echo $user_address[$i]['id']; ?>, 'closeclick', function () {
                    marker_id_<?php echo $user_address[$i]['id']; ?>.setIcon(pinIcon_<?php echo $i + 1; ?>);
                });
        <?php
    }
}
?>
    }
    $(function () {
        var script = document.createElement("script");
        script.type = "text/javascript";
        script.src = "https://maps.googleapis.com/maps/api/js?key=AIzaSyCFPo3KmF9lRvTl8mNsUW02gKKFTosnHhI&sensor=true&" + "callback=initialize" + '&libraries=places';
        document.body.appendChild(script);
    });
</script>
<script>
    function appointmentSchedule(type, address_id) {
        var today_date = $('#today_date').val();
        var view_date = $('#view_date').val();
        var speciality_id = $('#speciality_id').val();//alert(speciality_id);
        var procedure = $('#procedure_id').val();//alert(procedure);
        var doctor_id = $('#doctor_id').val();
        //var address_id = address_id;
        if (type == 'prev' && today_date == view_date) {
            //$('.pre_arrow_area img').attr('src','/doctorappointments/assets/images/pre_arrow_gry.png');
            return false;
        } else {
            //$('#schedule_body').css('display','block');
            //$('#schedule_loader').css('display','block');
            if (type == 'next') {
                $('#schedule_loader_next_' + address_id).css('display', 'block');
            } else {
                $('#schedule_loader_prev_' + address_id).css('display', 'block');
            }
            var sch_width = "14";
            $.post("<?php echo Yii::app()->request->baseUrl; ?>/doctor/appointmentScheduleProfile",
                    {type: type, today_date: today_date, view_date: view_date, speciality_id: speciality_id, procedure_id: procedure, doctor_id: doctor_id, address_id: address_id, sch_width: sch_width},
            function (response) {
                //alert(response);
                var res = response.split("***%%%***");
                $('#calander_menu_big_' + address_id).html(res[0]);
                $('#profilesec_timetable_leftcalnd_' + address_id).html(res[1]);
                $('#view_date').val(res[2]);
                if (today_date == res[2])
                    $('#pre_arrow_area_prof_' + address_id + ' img').attr('src', '<?php echo Yii::app()->request->baseUrl; ?>/assets/images/pre_arrow_gry.png');
                else
                    $('#pre_arrow_area_prof_' + address_id + ' img').attr('src', '<?php echo Yii::app()->request->baseUrl; ?>/assets/images/pre_arrow_green.png');
                //$('#schedule_body').css('display','none');
                $('.schedule_loader').css('display', 'none');
            });
        }
    }
</script>
<script>
    function profileAppList(today_date, address_id) {
        //alert('ok');

        var speciality_id = $('#speciality_id').val();//alert(speciality_id);
        var procedure = $('#procedure_id').val();//alert(procedure);
        var doctor_id = $('#doctor_id').val();

        $.post("<?php echo Yii::app()->request->baseUrl; ?>/doctor/appointmentScheduleProfileDay",
                {today_date: today_date, view_date: today_date, speciality_id: speciality_id, procedure_id: procedure, doctor_id: doctor_id, address_id: address_id},
        function (response) {
            //alert(response);
            var res = response.split("***%%%***");
            $('#app_popup_heading').html(res[0]);
            $('#popup_content_val').html(res[1]);
        });
    }
</script>
<style>
    .calander_box_lisiting_popup{ background:#fff; float:left; width:100%; /*min-height:209px;*/ border:1px solid #CCCCCC; margin-top:5px;
                                  box-shadow:0 2px 1px 0 #DEDEDE;  -moz-box-shadow: 0 2px 1px 0 #DEDEDE; -webkit-box-shadow: 0 2px 1px 0 #DEDEDE;}
    .calander_box_lisiting_popup .innertable_time_popup{ border-right:1px solid #ccc; border-bottom:1px solid #ccc; display:block; float:left; width:24.7%;}
    .calander_box_lisiting_popup .innertable_time_popup a{ display:block; color:#1bc1bd; font-size:16px; font-weight:bold; line-height:43.5px; height:43.5px;  text-align:center; }
    .calander_box_lisiting_popup .innertable_time_popup a.blank{ cursor:default;}
    .calander_box_lisiting_popup .innertable_time_popup a.blank:hover{ background:none;}
    .calander_box_lisiting_popup .innertable_time_popup a:hover{ background:#1fd3fa; color:#fff;}


    .calander_box_lisiting_popup .innertable_time_popup_first{ border-right:1px solid #ccc; border-bottom:1px solid #ccc; display:block; float:left; width:20%;}
    .calander_box_lisiting_popup .innertable_time_popup_first a{ display:block; color:#1bc1bd; font-size:16px; font-weight:bold; line-height:43.5px; height:43.5px;  text-align:center; }
    .calander_box_lisiting_popup .innertable_time_popup_first a.blank{ cursor:default;}
    .calander_box_lisiting_popup .innertable_time_popup_first a.blank:hover{ background:none;}
    .calander_box_lisiting_popup .innertable_time_popup_first a:hover{ background:#1fd3fa; color:#fff;}
    .address_list{ width:100%; float:left; margin-bottom:30px;}
    .address_list h4{ color:#727272; font-size:13px; font-weight:bold; padding-bottom:2px; float:left; display:block; width:100%; margin:0px; line-height:30px;}
</style>
<input type="hidden" id="speciality_id" value="<?php echo isset($user_selected_speciality[0]) ? $user_selected_speciality[0] : ''; ?>" />
<input type="hidden" id="procedure_id" value="<?php echo isset($user_procedure[0]) ? $user_procedure[0] : ''; ?>" />
<input type="hidden" id="doctor_id" value="<?php echo $doctor_list->id; ?>" />
<input type="hidden" id="today_date" value="<?php echo date('Y-m-d', strtotime("+ 1 day")); ?>" />
<input type="hidden" id="view_date" value="<?php echo date('Y-m-d', strtotime("+ 1 day")); ?>" />
<div style="position:fixed; width:100%; height:100%; background:#fff; opacity:0.8; display:none;" id="schedule_body">
</div>


<div id="toPopup">
    <div class="close_view"></div>
    <div id="popup_content">
        <h4 id="app_popup_heading">Comments</h4>
        <p>

        <div class="calander_box_lisiting_popup" id="popup_content_val">
        </div>

        </p>
    </div>
</div>

<div id="backgroundPopup"></div>
<script type="text/javascript">
    function show1() {
        document.getElementById('div1').style.display = 'none';
    }
    function show2() {
        document.getElementById('send_rate_shw_hid').style.display = 'none';
        document.getElementById('div1').style.display = 'block';
    }
    function patientReview() {
        var overall_rating = $('#rating_html_hidden_overall_rating').val();
        var bedside_manner_rating = $('#rating_html_hidden_beside_manner_rating').val();
        var wait_time_rating = $('#total_wait_time').val();
        var scheduling_appointment_rating = $('#rating_html_hidden_scheduling_appointment').val();
        var office_experience_rating = $('#rating_html_hidden_office_experience').val();
        var spent_time_rating = $('#rating_html_hidden_spent_time_rating').val();
        //var review_title = $('#review_title').val();
        var review_message = $('#review_message').val();
        var doctor_id = $('#doctor_id').val();

        //alert(review_rating);alert(review_title);alert(review_message);
        var jqXHR = $.ajax({
            type: "POST",
            url: "<?php echo Yii::app()->request->baseUrl; ?>/patient/patientReview",
            data: {overall_rating: overall_rating, bedside_manner_rating: bedside_manner_rating, wait_time_rating: wait_time_rating, scheduling_appointment: scheduling_appointment_rating, office_experience: office_experience_rating, spent_time: spent_time_rating, review_message: review_message, doctor_id: doctor_id},
            async: false,
            success: function (result) {
                document.getElementById('send_rate_shw_hid').style.display = 'block';
                document.getElementById('div1').style.display = 'none';
                location.reload();
            }
        });
    }
</script>
<!--rating start-->

<script type="text/javascript">
    function voteRatingMsg(vote) {
        msg = "";
        if (vote == 1) {
            msg = "Poor";
        } else if (vote == 2) {
            msg = "Fair";
        } else if (vote == 3) {
            msg = "Good";
        } else if (vote == 4) {
            msg = "Very&nbsp;Good";
        } else if (vote == 5) {
            msg = "Excellent";
        }
        return msg;
    }
    $(function () {
        hidden_val1 = "";
        hidden_val2 = "";
        hidden_val3 = "";
        hidden_val4 = "";
        hidden_val5 = "";
        msg = "Slide for wait times";
        // $('#contaner_bedside_manner').rating();
        $('#contaner_scheduling_appointment').rating(function (vote, event) {
            var msg = voteRatingMsg(vote);
            $('#rating_html_scheduling_appointment').html(msg);
            $('#rating_html_hidden_scheduling_appointment').val(vote);
        });
        $('#contaner_office_experience').rating(function (vote, event) {
            var msg = voteRatingMsg(vote);
            $('#rating_html_office_experience').html(msg);
            $('#rating_html_hidden_office_experience').val(vote);
        });
        $('#contaner_beside_manner_rating').rating(function (vote, event) {
            var msg = voteRatingMsg(vote);
            $('#rating_html_beside_manner_rating').html(msg);
            $('#rating_html_hidden_beside_manner_rating').val(vote);
        });
        $('#contaner_spent_time_rating').rating(function (vote, event) {
            var msg = voteRatingMsg(vote);
            $('#rating_html_spent_time_rating').html(msg);
            $('#rating_html_hidden_spent_time_rating').val(vote);
        });
        $('#contaner_overall_rating').rating(function (vote, event) {
            var msg = voteRatingMsg(vote);
            $('#rating_html_overall_rating').html(msg);
            $('#rating_html_hidden_overall_rating').val(vote);
        });

        /*$('.container').rating(function(vote, event){
         // write your ajax code here
         // For example;
         // $.get(document.URL, {vote: vote});
         });*/
        $('#contaner_scheduling_appointment a.star').mouseover(function () {
            var eq_no = $("#contaner_scheduling_appointment a.star").index(this);
            var rat_val = $('#contaner_scheduling_appointment .rating_star').eq(eq_no).val();//alert(eq_no);
            var msg = voteRatingMsg(rat_val);
            $('#rating_html_scheduling_appointment').html(msg);
        });
        $('#contaner_scheduling_appointment a.star').mouseout(function () {
            if ($('#rating_html_hidden_scheduling_appointment').val() == '') {
                $('#rating_html_scheduling_appointment').html('');
            } else
                hidden_val1 = $('#rating_html_hidden_scheduling_appointment').val();
            var msg = voteRatingMsg(hidden_val1);
            $('#rating_html_scheduling_appointment').html(msg);
        });

        $('#contaner_office_experience a.star').mouseover(function () {
            var eq_no = $("#contaner_office_experience a.star").index(this);
            var rat_val = $('#contaner_office_experience .rating_star').eq(eq_no).val();//alert(eq_no);
            var msg = voteRatingMsg(rat_val);
            $('#rating_html_office_experience').html(msg);
        });
        $('#contaner_office_experience a.star').mouseout(function () {
            if ($('#rating_html_hidden_office_experience').val() == '') {
                $('#rating_html_office_experience').html('');
            } else
                hidden_val2 = $('#rating_html_hidden_office_experience').val();
            var msg = voteRatingMsg(hidden_val2);
            $('#rating_html_office_experience').html(msg);
        });

        $('#contaner_beside_manner_rating a.star').mouseover(function () {
            var eq_no = $("a.star").index(this);
            var rat_val = $('.rating_star').eq(eq_no).val();//alert(eq_no);
            var msg = voteRatingMsg(rat_val);
            $('#rating_html_beside_manner_rating').html(msg);
        });
        $('#contaner_beside_manner_rating a.star').mouseout(function () {
            if ($('#rating_html_hidden_beside_manner_rating').val() == '') {
                $('#rating_html_beside_manner_rating').html('');
            } else
                hidden_val3 = $('#rating_html_hidden_beside_manner_rating').val();
            var msg = voteRatingMsg(hidden_val3);
            $('#rating_html_beside_manner_rating').html(msg);
        });

        $('#contaner_spent_time_rating a.star').mouseover(function () {
            var eq_no = $("a.star").index(this);
            var rat_val = $('.rating_star').eq(eq_no).val();//alert(eq_no);
            var msg = voteRatingMsg(rat_val);
            $('#rating_html_spent_time_rating').html(msg);
        });
        $('#contaner_spent_time_rating a.star').mouseout(function () {
            if ($('#rating_html_hidden_spent_time_rating').val() == '') {
                $('#rating_html_spent_time_rating').html('');
            } else
                hidden_val4 = $('#rating_html_hidden_spent_time_rating').val();
            var msg = voteRatingMsg(hidden_val4);
            $('#rating_html_spent_time_rating').html(msg);
        });

        $('#contaner_overall_rating a.star').mouseover(function () {
            var eq_no = $("a.star").index(this);
            var rat_val = $('.rating_star').eq(eq_no).val();//alert(eq_no);
            var msg = voteRatingMsg(rat_val);
            $('#rating_html_overall_rating').html(msg);
        });
        $('#contaner_overall_rating a.star').mouseout(function () {
            if ($('#rating_html_hidden_overall_rating').val() == '') {
                $('#rating_html_overall_rating').html('');
            } else
                hidden_val5 = $('#rating_html_hidden_overall_rating').val();
            var msg = voteRatingMsg(hidden_val5);
            $('#rating_html_overall_rating').html(msg);
        });
        $("#slider-range-min").slider({
            range: "min",
            value: 0,
            min: 0,
            max: 60,
            slide: function (event, ui) {
                if (ui.value == 0) {
                    msg = "Slide for wait times";
                } else if (ui.value > 0 && ui.value < 16) {
                    msg = "Under 15 Min";
                } else if (ui.value > 15 && ui.value < 31) {
                    msg = "15-30 Min";
                } else if (ui.value > 30 && ui.value < 46) {
                    msg = "30-45 Min";
                } else {
                    msg = "More Then 45 Min";
                }
                $("#total_wait_time_rating").html(msg);
                $("#total_wait_time").val(ui.value);
            }
        });
        $("#total_wait_time_rating").html(msg);
        $("#total_wait_time").val($("#slider-range-min").slider("value"));

    });

	function addFav(pid,did,spid)
	{
		$.post("<?php echo Yii::app()->request->baseUrl; ?>/doctor/doctorFavourite",{pid:pid,did:did,spid:spid},function(data){ 
			$("#favDiv").html(data);
			//location.reload(); 
		});
	}
	function delFav(pid,did,spid)
	{
		$.post("<?php echo Yii::app()->request->baseUrl; ?>/doctor/deleteDoctorFavourite",{pid:pid,did:did,spid:spid},function(data){ 
			
				$("#favDiv").html(data);
				//location.reload(); 
			
		});
	}
	
</script>
<!--rating end-->
<style>
.calander_box_lisiting_popup{ background:#fff; float:left; width:100%; /*min-height:209px;*/ border:1px solid #CCCCCC; margin-top:5px;
 box-shadow:0 2px 1px 0 #DEDEDE;  -moz-box-shadow: 0 2px 1px 0 #DEDEDE; -webkit-box-shadow: 0 2px 1px 0 #DEDEDE;}
.calander_box_lisiting_popup .innertable_time_popup{ border-right:1px solid #ccc; border-bottom:1px solid #ccc; display:block; float:left; width:24.7%;}
.calander_box_lisiting_popup .innertable_time_popup a{ display:block; color:#1bc1bd; font-size:16px; font-weight:bold; line-height:43.5px; height:43.5px;  text-align:center; }
.calander_box_lisiting_popup .innertable_time_popup a.blank{ cursor:default;}
.calander_box_lisiting_popup .innertable_time_popup a.blank:hover{ background:none;}
.calander_box_lisiting_popup .innertable_time_popup a:hover{ background:#1fd3fa; color:#fff;}


.calander_box_lisiting_popup .innertable_time_popup_first{ border-right:1px solid #ccc; border-bottom:1px solid #ccc; display:block; float:left; width:20%;}
.calander_box_lisiting_popup .innertable_time_popup_first a{ display:block; color:#1bc1bd; font-size:16px; font-weight:bold; line-height:43.5px; height:43.5px;  text-align:center; }
.calander_box_lisiting_popup .innertable_time_popup_first a.blank{ cursor:default;}
.calander_box_lisiting_popup .innertable_time_popup_first a.blank:hover{ background:none;}
.calander_box_lisiting_popup .innertable_time_popup_first a:hover{ background:#1fd3fa; color:#fff;}
.address_list{ width:100%; float:left; margin-bottom:30px;}
.address_list h4{ color:#727272; font-size:13px; font-weight:bold; padding-bottom:2px; float:left; display:block; width:100%; margin:0px; line-height:30px;}
</style>
<div id="toPopup" style="z-index: 999;"> 
    <div class="close_view"></div> 
    <div id="popup_content">
    <h4 id="app_popup_heading"></h4>
    <p>
    	<div class="calander_box_lisiting_popup" id="popup_content_val"></div>
    </p>
    </div>
</div>