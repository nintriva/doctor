<?php
/* @var $this DoctorController */
/* @var $model Doctor */

$this->breadcrumbs=array(
	'Dashboard'=>array('index'),
	'Edit Profile',
);

/*$this->menu=array(
	array('label'=>'List Doctor', 'url'=>array('index')),
	array('label'=>'Manage Doctor', 'url'=>array('admin')),
);*/
?>

<!--<h1>Create Doctor</h1>-->

<?php //$this->renderPartial('_form', array('model'=>$model)); ?>

<div class="main">
    <div id="breadcrumb" class="fk-lbreadbcrumb newvd">
        <!--<span><a href="">Home</a></span> >  
        <span>Dashboard</span>--> 
        <?php $this->widget('zii.widgets.CBreadcrumbs', array(
				  'links'=>$this->breadcrumbs,
			  ));
		?>
    </div>
  	  <div class="dashboard_mainarea">
     	<div class="leftmenu">
       		 <?php /*?><h2>Doctor control panel</h2>
             <ul>
            	 <li><?php echo CHtml::link('Dashboard', $this->createAbsoluteUrl('index')); ?></li>
                 <li>
                 <!--<a href="#">Edit My Account</a>-->
                 <?php echo CHtml::link('Edit My Account', $this->createAbsoluteUrl('doctor/editProfile/'.Yii::app()->session['logged_user_id'])); ?>
                 </li>
                 <li class="active"><?php echo CHtml::link('My Addresses', $this->createAbsoluteUrl('doctor/address/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li><?php echo CHtml::link('My Specialities', $this->createAbsoluteUrl('doctor/speciatlity/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li><?php echo CHtml::link('View Profile', $this->createAbsoluteUrl('doctor/profile/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li><a href="#">Appointments</a></li>
                 <li><?php echo CHtml::link('Schedules', $this->createAbsoluteUrl('doctor/schedule/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li><?php echo CHtml::link('Timeoff', $this->createAbsoluteUrl('doctor/timeoff/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li><?php echo CHtml::link('Todo List', $this->createAbsoluteUrl('doctor/todolist/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li><?php echo CHtml::link('Patients', $this->createAbsoluteUrl('doctor/patient/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li><?php echo CHtml::link('Setting Tab', $this->createAbsoluteUrl('doctor/settingTab/'.Yii::app()->session['logged_user_id'])); ?></li>
             </ul><?php */?>
              <?php $this->renderPartial('//layouts/navigation'); ?>
        </div>
        
        <div class="rightarea_dashboard">
        	<div class="dashboard_content1">
            	<div class="add_area">
                	<span class="add_new_btn"><?php echo CHtml::link('Add new', $this->createAbsoluteUrl('doctor/editAddress/'),array('class'=>'grn_btn')); ?></span>
                    <span class="refresh"><a href="javascript:void(0)"><img src="<?php echo Yii::app()->request->baseUrl; ?>/assets/images/refresh_iocn.png" alt="" /></a></span>
                    <div class="clear"></div>
                </div>
           	  <div class="dashboardcont_leftbox2">
                	<ul>
                        <li class="heading">
                         <span class="add">Address</span> 
                         <span class="att">Latitude</span> 
                         <span class="att">Longitude</span>
                         <!--<span class="att">Access</span>-->
                         <span class="order">Order</span>
                         <span class="active">Default</span>
                         <span class="active">Active</span>
                         <span class="att">Action</span>
                        </li>
                        <?php
						if($dataProvider):
						for($i=0;$i<count($dataProvider);$i++){
						?>
                        <li>
                         <span class="add"><?php echo $dataProvider[$i]['address']; ?></span> 
                         <span class="att"><?php echo $dataProvider[$i]['latitude']; ?></span> 
                         <span class="att"><?php echo $dataProvider[$i]['longitude']; ?></span>
                         <!--<span class="att">Public</span>-->
                         <span class="order"><?php echo $dataProvider[$i]['order_status']; ?></span>
                         <span class="active"><?php echo $dataProvider[$i]['default_status']; ?></span>
                         <span class="active"><?php echo $dataProvider[$i]['active']; ?></span>
                         <span class="att"><?php echo CHtml::link('Edit', $this->createAbsoluteUrl('doctor/editAddress/'.$dataProvider[$i]['id'])); ?></span>
                        </li>
                        <?php
						 }
					    endif;
					    ?>
                    </ul>
                </div>
                <div class="pagination_area">
                    	<div class="pagination_area_lft">
                       	  <div id="demo5" class="jPaginate" style="padding-left: 64px;">
                            	<div class="jPag-control-back">
                                	<a class="jPag-first" style="color: rgb(255, 255, 255);background-color: rgb(3, 191, 145);border: 1px solid rgb(255, 255, 255);">
                                    	First
                                    </a>
									<span class="jPag-sprevious">«</span>
                                </div>
                                <div style="overflow: hidden; width: 163px;">
                                	<ul class="jPag-pages" style="width: 237px;">
                                    <li>
                                    	<span class="jPag-current" style="color: rgb(3, 191, 145); background-color: rgb(255, 255, 255); border: 1px solid rgb(204, 204, 204);">1</span>
                                    </li>
                                    <li>
                                    <a style="color: rgb(255, 255, 255); background-color: rgb(3, 191, 145); border: 1px solid rgb(255, 255, 255);">2</a>
                                    </li>
                                    <li>
                                    <a style="color: rgb(255, 255, 255); background-color: rgb(3, 191, 145); border: 1px solid rgb(255, 255, 255);">3</a>
                                    </li>
                                    <li>
                                    <a style="color: rgb(255, 255, 255); background-color: rgb(3, 191, 145); border: 1px solid rgb(255, 255, 255);">4</a>
                                    </li>
                                    <li>
                                    <a style="color: rgb(255, 255, 255); background-color: rgb(3, 191, 145); border: 1px solid rgb(255, 255, 255);">5</a>
                                    </li>
                                    <li>
                                    <a style="color: rgb(255, 255, 255); background-color: rgb(3, 191, 145); border: 1px solid rgb(255, 255, 255);">6</a>
                                    </li>
                                    <li>
                                    <a style="color: rgb(255, 255, 255); background-color: rgb(3, 191, 145); border: 1px solid rgb(255, 255, 255);">7</a>
                                    </li>
                                    <li>
                                    <a style="color: rgb(255, 255, 255); background-color: rgb(3, 191, 145); border: 1px solid rgb(255, 255, 255);">8</a>
                                    </li>
                                    <li>
                                    <a style="color: rgb(255, 255, 255); background-color: rgb(3, 191, 145); border: 1px solid rgb(255, 255, 255);">9</a>
                                    </li>
                                    <li>
                                    <a style="color: rgb(255, 255, 255); background-color: rgb(3, 191, 145); border: 1px solid rgb(255, 255, 255);">10</a>
                                    </li>
                                    </ul>
                                </div>
                            <div class="jPag-control-front" style="left: 231px;"><span class="jPag-snext">»</span><a class="jPag-last" style="color: rgb(255, 255, 255); background-color: rgb(3, 191, 145); border: 1px solid rgb(255, 255, 255);">Last</a></div>
                            </div>
                        </div>
                  <div class="pagination_area_rht">Total: 1 - 3 / 3</div>
                        <div class="clear"></div>
              </div>
            </div>
		</div>
         
      </div>
</div>