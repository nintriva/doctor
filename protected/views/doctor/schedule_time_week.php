<?php
/* @var $this DoctorController */
/* @var $model Doctor */

$this->breadcrumbs=array(
	'Dashboard'=>array('index'),
	'Edit Profile',
);

/*$this->menu=array(
	array('label'=>'List Doctor', 'url'=>array('index')),
	array('label'=>'Manage Doctor', 'url'=>array('admin')),
);*/
?>

<!--<h1>Create Doctor</h1>-->

<?php //$this->renderPartial('_form', array('model'=>$model)); ?>

<div class="main">
    <div id="breadcrumb" class="fk-lbreadbcrumb newvd">
        <!--<span><a href="">Home</a></span> >  
        <span>Dashboard</span>--> 
        <?php $this->widget('zii.widgets.CBreadcrumbs', array(
				  'links'=>$this->breadcrumbs,
			  ));
		?>
    </div>
  	  <div class="dashboard_mainarea">
     	<div class="leftmenu">
       		 <?php /*?><h2>Doctor control panel</h2>
             <ul>
            	 <li><?php echo CHtml::link('Dashboard', $this->createAbsoluteUrl('index')); ?></li>
                 <li><?php echo CHtml::link('Edit My Account', $this->createAbsoluteUrl('doctor/editProfile/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li><?php echo CHtml::link('Special Offers', $this->createAbsoluteUrl('doctor/offers/'.Yii::app()->session['logged_user_id'])); ?></li><?php */?>
                 <?php /*?><li><?php echo CHtml::link('My Addresses', $this->createAbsoluteUrl('doctor/address/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li><?php echo CHtml::link('My Specialities', $this->createAbsoluteUrl('doctor/speciatlity/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li><?php echo CHtml::link('View Profile', $this->createAbsoluteUrl('doctor/profile/'.Yii::app()->session['logged_user_id'])); ?></li><?php */?>
                <?php /*?> <li><?php echo CHtml::link('Appointments', $this->createAbsoluteUrl('doctor/appointment/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li class="active"><?php echo CHtml::link('Schedules', $this->createAbsoluteUrl('doctor/schedule/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li><?php echo CHtml::link('Timeoff', $this->createAbsoluteUrl('doctor/timeoff/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li><?php echo CHtml::link('Todo List', $this->createAbsoluteUrl('doctor/todolist/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li><?php echo CHtml::link('Patients', $this->createAbsoluteUrl('doctor/patient/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li><?php echo CHtml::link('Setting Tab', $this->createAbsoluteUrl('doctor/settingTab/'.Yii::app()->session['logged_user_id'])); ?></li>
             </ul><?php */?>
             <?php $this->renderPartial('//layouts/navigation'); ?>
        </div>
        
        <div class="rightarea_dashboard">
        	<div class="dashboard_content1">
            	
           	  <div class="dashboardcont_leftbox2">
                	<?php if(Yii::app()->user->hasFlash('editSchedulesTime')): ?>
                        <span class="flash-success">
                            <?php echo Yii::app()->user->getFlash('editSchedulesTime'); ?>
                        </span>
                    <?php endif; ?>
                    <?php $form=$this->beginWidget('CActiveForm', array(
						'id'=>'edit_schedules_time',
					)); ?>
                    <ul>
                        <li class="heading">
                         <span class="order">Day</span> 
                         <span class="order">On/Off</span> 
                         <span class="active_sch_tim">Start Time</span> 
                         <span class="active_sch_tim">End Time</span>
                         <span class="active_sch_tim">Time Slots(min)</span>
                         <span class="active_sch_tim_rest">Rest Time Slots(min)</span>
                        </li>
                        
                        <?php
						if($dataProvider):
						for($i=0;$i<count($dataProvider);$i++){
						?>
                        <li>
                         <span class="order"><?php echo $dataProvider[$i]['day']; ?><input type="hidden" name="hidden_id[]" value="<?php echo $dataProvider[$i]['id']; ?>" /></span> 
                         <span class="order" style="vertical-align:middle; padding-left:10px;" >
                         <input type="checkbox" name="on_off_<?php echo $i ; ?>" value="1" <?php if($dataProvider[$i]['on_off']==1){ ?>checked="checked"<?php } ?> />
                         </span> 
                         <span class="active_sch_tim">
                         <?php /*?><input type="text" name="from_time[]" value="<?php echo $dataProvider[$i]['from_time']; ?>" /><?php */?>
                         <?php
						 	if($dataProvider[$i]->from_time == "" || $dataProvider[$i]->from_time == '00:00:00'){
								$from_time['hh_mm'] = "08:00";
								 $selected_time_from_nc_hour = $from_time['hh_mm'].' am';
							}else{
								$from_time_arr =explode(":",$dataProvider[$i]->from_time);
								$from_time['hh_mm'] = $from_time_arr[0].':'.$from_time_arr[1];
								$selected_time_from_nc_hour = $from_time['hh_mm'].' '.strtolower($dataProvider[$i]->from_time_format);
							}
		
							  $selected_time_from_nc_hour = $selected_time_from_nc_hour;
							  echo CHtml::dropDownList('from_time[]', $selected_time_from_nc_hour, 
							  DoctorScheduleTime::model()->getTimeOptions(),
							  array(/*'empty' => 'Select Start Time Format',*/'style'=>'width: auto; padding-right:5px;','onchange'=>"timeFun('hr','from',this.value);"));
						?>
                         </span> 
                         <span class="active_sch_tim">
                         <?php /*?><input type="text" name="to_time[]" value="<?php echo $dataProvider[$i]['to_time']; ?>" /><?php */?>
                         <?php
						 	if($dataProvider[$i]->to_time == "" || $dataProvider[$i]->to_time == '00:00:00'){
								$to_time['hh_mm'] = "05:00";
								$selected_time_to_nc_hour = $to_time['hh_mm'].' pm';
							}else{
								$to_time_arr =explode(":",$dataProvider[$i]->to_time);
								$to_time['hh_mm'] = $to_time_arr[0].':'.$to_time_arr[1];
								$selected_time_to_nc_hour = $to_time['hh_mm'].' '.strtolower($dataProvider[$i]->to_time_format);
							}
		
							  $selected_time_to_nc_hour = $selected_time_to_nc_hour;
							  echo CHtml::dropDownList('to_time[]', $selected_time_to_nc_hour, 
							  DoctorScheduleTime::model()->getTimeOptions(),
							  array(/*'empty' => 'Select End Time Format',*/'style'=>'width: auto; padding-left:5px;','onchange'=>"timeFun('hr','to',this.value);"));
						?>
                         </span>
                         <span class="active_sch_tim">
                        <?php /*?> <input type="text" name="time_slot[]" value="<?php echo $dataProvider[$i]['time_slot']; ?>" /><?php */?>
                         <?php
							  $selected_time_slot = $dataProvider[$i]->time_slot;
							  echo CHtml::dropDownList('time_slot[]', $selected_time_slot, 
							  DoctorScheduleTime::model()->getTimeSlotOptions(),
							  array('empty' => 'Select','class'=>'fld_class'));
						?>
                         </span>
                         <span class="active_sch_tim">
                         <?php /*?><input type="text" name="laser_slot[]" value="<?php echo $dataProvider[$i]['laser_slot']; ?>" /><?php */?>
                         <?php
							  $selected_laser_slot = $dataProvider[$i]->laser_slot;
							  echo CHtml::dropDownList('laser_slot[]', $selected_laser_slot, 
							  DoctorScheduleTime::model()->getLaserSlotOptions(),
							  array('empty' => 'Select','class'=>'fld_class'));
						?>
                         </span>
                        </li>
                        <?php
						 }
					    endif;
					    ?>
                        
                    </ul>
                    <div>
                    <span>
					<?php echo CHtml::submitButton('Update',array('class'=>'grn_btn')); ?>
                    <?php echo CHtml::link('Cancel', $this->createAbsoluteUrl('doctor/schedule/'.Yii::app()->session['logged_user_id']),array('class'=>'registbt_new')); ?>
                    </span>
                    </div>
                    <?php $this->endWidget(); ?>
                </div>
            </div>
		</div>
         
      </div>
</div>
<script>
function removeScheduleTime(id){
	if(confirm('Are you sure ?'))
	$.post("<?php echo Yii::app()->request->baseUrl; ?>/doctor/scheduleTimeAjaxRemove", {id:id},function(response) {
			location.reload();
		});
}
</script>