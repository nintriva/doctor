<?php
$this->breadcrumbs=array(
	'Dashboard'=>array('index'),
	'Patients',
);
?>
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/assets/css/tabs.css">
<style>
	.fc-event-inner{
		cursor:pointer;
	}
	.ui-widget-overlay {
		background: 50% 50% #AAAAAA;
	}
	.ui-widget-header{
		background-color:#54cbc8;
		background-image: none;	
	}
</style>
<div class="main">
    <div id="breadcrumb" class="fk-lbreadbcrumb newvd">
        <!--<span><a href="">Home</a></span> >  
        <span>Dashboard</span>--> 
        <?php $this->widget('zii.widgets.CBreadcrumbs', array(
				  'links'=>$this->breadcrumbs,
			  ));
		?>
    </div>
  	  <div class="dashboard_mainarea">
     	<div class="leftmenu">
       		 <?php /*?><h2>Doctor control panel</h2>
             <ul>
            	 <li><?php echo CHtml::link('Dashboard', $this->createAbsoluteUrl('index')); ?></li>
                 <li><?php echo CHtml::link('My Account', $this->createAbsoluteUrl('doctor/editProfile/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li><?php echo CHtml::link('Special Offers', $this->createAbsoluteUrl('doctor/offers/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li><?php echo CHtml::link('Appointments', $this->createAbsoluteUrl('doctor/appointment/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li><?php echo CHtml::link('Schedules', $this->createAbsoluteUrl('doctor/schedule/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li><?php echo CHtml::link('Timeoff', $this->createAbsoluteUrl('doctor/timeoff/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li><?php echo CHtml::link('Todo List', $this->createAbsoluteUrl('doctor/todolist/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li class="active"><?php echo CHtml::link('Patients', $this->createAbsoluteUrl('doctor/patient/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li><?php echo CHtml::link('Setting Tab', $this->createAbsoluteUrl('doctor/settingTab/'.Yii::app()->session['logged_user_id'])); ?></li>
             </ul><?php */?>
             <?php $this->renderPartial('//layouts/navigation'); ?>
        </div>
        
        <div class="rightarea_dashboard">
         <div class="dashboard_content1">
         	   <div class="dashboardcont_leftbox">
              <h1>Search Your Patients</h1>
            	<div class="add_area">
                	<span class="add_calender">
                    	<div class="filter_search_area_calender">
                            
                            <form name="refine search" method="get" action="<?php echo Yii::app()->request->baseUrl; ?>/doctor/patient">
                            <div class="filter_apdate_calender_new">
                            	 
                               <span> 
                                <label>First Name</label>
                                <input type="text" name="user_first_name" id="user_first_name" class="filter_txtfld_calender" placeholder="First Name" value="<?php if(isset($_REQUEST['user_first_name'])) echo $_REQUEST['user_first_name']; ?>"/>
                               </span>
                               <span> 
                                <label>Last Name</label>
                                <input type="text" name="user_last_name" id="user_last_name" class="filter_txtfld_calender" placeholder="Last Name" value="<?php if(isset($_REQUEST['user_last_name'])) echo $_REQUEST['user_last_name']; ?>"/>
                               </span>
                               <span> 
                                <label>Birth Date</label>
                                <input type="text" name="user_dob" id="search_date" class="filter_txtfld_calender datepicker" placeholder="yyyy-mm-dd" value="<?php if(isset($_REQUEST['user_dob'])) echo $_REQUEST['user_dob']; ?>"/>
                               </span>
                               <span > 
                                <label>Phone Number</label>
                                <input type="text" name="user_phone" id="user_phone" class="filter_txtfld_calender" placeholder="Phone Number" value="<?php if(isset($_REQUEST['user_phone'])) echo $_REQUEST['user_phone']; ?>"/>
                                </span>
                                <span > 
                                <label>Patient MRN</label>
                                <input type="text" name="patient_mrn" id="patient_mrn" class="filter_txtfld_calender" placeholder="Patient MRN" value="<?php if(isset($_REQUEST['patient_mrn'])) echo $_REQUEST['patient_mrn']; ?>"/>
                               </span>
                               <?php /*?><span> 
                                <label>Appointment Number</label>
                                <input type="text" name="appointment_number" id="appointment_number" class="filter_txtfld_calender" placeholder="Appointment Number" value="<?php if(isset($_REQUEST['appointment_number'])) echo $_REQUEST['appointment_number']; ?>"/>
                               </span><?php */?>
                            </div>
                            <div class="filter_apbtn_new"><input type="submit" class="search_btn" name="" value="Search"/></div>
                            <div class="clear"></div>
                            </form>
                        </div>
                    </span>
                   <!-- <span class="refresh"><a href="javascript:void(0)"><img src="<?php echo Yii::app()->request->baseUrl; ?>/assets/images/refresh_iocn.png" alt="" /></a></span>-->
                    <div class="clear"></div>
                </div>
              </div>   
           	  <div class="dashboardcont_leftbox2">
              		<?php if(Yii::app()->user->hasFlash('editPatient')): ?>
                        <span class="flash-success">
                            <?php echo Yii::app()->user->getFlash('editPatient'); ?>
                        </span>
                    <?php endif; ?>
                	<ul>
                        <li class="heading">
                         <span class="active">First Name</span> 
                         <span class="active">Last Name</span>
                         <span class="active">Phone Number</span> 
                         <span class="active">Birth Date</span>
                         <span class="active">Patient MRN</span>
                         <span class="att txt_align">Action</span>
                        </li>
                        <?php
						if($dataProvider):
						for($i=0;$i<count($dataProvider);$i++){
						?>
                        <li>
                         <span class="active" id="patient_first_name_<?php echo $dataProvider[$i]['app_book_id']; ?>"><?php echo $dataProvider[$i]['user_first_name']; ?></span>
                         <span class="active" id="patient_last_name_<?php echo $dataProvider[$i]['app_book_id']; ?>"><?php echo $dataProvider[$i]['user_last_name']; ?></span>
                         <span class="active"><?php echo $dataProvider[$i]['user_phone']; ?></span> 
                         <span class="active"><?php echo ($dataProvider[$i]['user_dob']=='' || $dataProvider[$i]['user_dob']=='0000-00-00')?'':$dataProvider[$i]['user_dob']; ?></span>
                         <span class="active"><?php echo $dataProvider[$i]['app_mrn']; ?></span>
                         <span class="att txt_align">
                            <?php echo CHtml::link('<img src="'.Yii::app()->request->baseUrl.'/assets/images/edit_icon.png" alt=""/>', 'javascript:void(0);',array('onclick'=>'patientDetail('.$dataProvider[$i]['app_book_id'].','.$dataProvider[$i]['id'].',\''.$dataProvider[$i]['created_by'].'\');')); ?>
                            <?php /*?><a href="javascript:void(0);" ><img src="<?php echo Yii::app()->request->baseUrl; ?>/assets/images/delete_icon.png" alt=""/></a><?php */?>
                         </span>
                        </li>
                        <?php
						 }
						 else:
						 	echo '<li style="text-align: center;"><br>No patients have been added to your practice.<br>Contact eDoctorBook support for assistance with uploading your patients list.<br> </li>';
					    endif;
					    ?>
                    </ul>
                </div>
                <?php $this->widget('CLinkPager', array(
                        'pages' => $pages,'header'=>'','prevPageLabel'=>'&lt;&lt;','nextPageLabel'=>'&gt;&gt;',
                    ))
				?>
            </div>
        
		</div>
         
      </div>
</div>
<div id="patient_detail" style="display:none;">
	<div class="tabBox_new">
            	<ul class="tabs">
                	<li><a href="#tab1">Contact Information</a></li>
                    <li><a href="#tab2">Insurance</a></li>
                    <li><a href="#tab3">Comments</a></li>
                    <li><a href="#tab4">Appointments</a></li>
                    <li><a href="#tab5">Messages</a></li>
                </ul>
                <div class="tabContainer">
                	<div id="tab1" class="tabContent_new">
 						<form method="post" action="">
                        <div class="dashboardcont_leftbox">
                            <div class="box_content_new">
        							<div class="fld_area_new1">
                                        <div class="fld_name">First Name </div>
                                        <div class="name_fld">
                                        <input type="text" id="popup_user_first_name" name="user_first_name" placeholder="First Name" class="fld_class_new popup_edit">
                                        <div id="popup_user_first_name_html" class="popup_non_edit"></div>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="fld_area_new1">
                                        <div class="fld_name">Last Name</div>
                                        <div class="name_fld">
                                        <input type="text" id="popup_user_last_name" name="user_last_name" placeholder="Last Name" class="fld_class_new popup_edit">
                                        <div id="popup_user_last_name_html" class="popup_non_edit"></div>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="fld_area_new1">
                                        <div class="fld_name">Parent/Legal Guardian</div>
                                        <div class="name_fld">
                                        <input type="text" id="popup_user_parent_legal_guardian" name="user_parent_legal_guardian" placeholder="Parent/Legal Guardian" class="fld_class_new popup_edit">
                                        <div id="popup_user_parent_legal_guardian_html" class="popup_non_edit"></div>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="fld_area_new1">
                                        <div class="fld_name">Email</div>
                                        <div class="name_fld">
                                        <input type="text" id="popup_user_email" name="user_email" placeholder="Email" class="fld_class_new popup_edit">
                                        <div id="popup_user_email_html" class="popup_non_edit"></div>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="fld_area_new1">
                                        <div class="fld_name">Phone</div>
                                        <div class="name_fld">
                                        <input type="text" id="popup_user_phone" name="user_phone" placeholder="Phone" class="fld_class_new popup_edit">
                                        <div id="popup_user_phone_html" class="popup_non_edit"></div>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="fld_area_new1">
                                        <div class="fld_name">Address</div>
                                        <div class="name_fld">
                                        <input type="text" id="popup_user_address" name="user_address" placeholder="Address" class="fld_class_new popup_edit">
                                        <div id="popup_user_address_html" class="popup_non_edit"></div>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="fld_area_new1">
                                        <div class="fld_name">City</div>
                                        <div class="name_fld">
                                        <input type="text" id="popup_user_city" name="user_city" placeholder="City" class="fld_class_new popup_edit">
                                        <div id="popup_user_city_html" class="popup_non_edit"></div>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="fld_area_new1">
                                        <div class="fld_name">State</div>
                                        <div class="name_fld">
                                        <input type="text" id="popup_user_state" name="user_state" placeholder="State" class="fld_class_new popup_edit">
                                         <div id="popup_user_state_html" class="popup_non_edit"></div>
                                         </div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="fld_area_new1">
                                        <div class="fld_name">Zip</div>
                                        <div class="name_fld">
                                        <input type="text" id="popup_user_zip" name="user_zip" placeholder="Zip" class="fld_class_new popup_edit">
                                        <div id="popup_user_zip_html" class="popup_non_edit"></div>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="fld_area_new1">
                                        <div class="fld_name">Preferred contact method</div>
                                        <div class="name_fld">
                                        <select class="select_fld_class popup_edit" id="popup_user_contact_method" name="user_contact_method">
                                        	<option value="Email">Email</option>
                                            <option value="SMS">SMS</option>
                                        </select>
                                        <div id="popup_user_contact_method_html" class="popup_non_edit"></div>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="fld_area_new1">
                                        <div class="fld_name">Birth Date</div>
                                        <div class="name_fld">
                                            <input type="text" id="popup_user_dob" name="user_dob" placeholder="" readonly="readonly" class="fld_class_new popup_edit datepicker">
                                            <div id="popup_user_dob_html" class="popup_non_edit"></div>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="fld_area_new1">
                                        <div class="fld_name">Recall Frequency(Months)</div>
                                        <div class="name_fld">
                                        <select class="select_fld_class popup_non_edit" id="popup_user_recall_frequency" name="user_recall_frequency">
                                        	<option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                            <option value="7">7</option>
                                            <option value="8">8</option>
                                            <option value="9">9</option>
                                            <option value="10">10</option>
                                            <option value="11">11</option>
                                            <option value="12">12</option>
                                        </select>
                                        <div id="popup_user_recall_frequency_html" class="popup_edit"></div>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                            </div>
                        </div>
                        <div>
                        <span>
                        <!--<input class="registbt patient_data_save" type="button" value="Update" name="yt0" onClick="patientAjaxIndSave();">-->
                        <input class="registbt" type="button" value="Update" name="yt0" onClick="patientAjaxIndSave();">
                        <input class="registbt" type="button" value="Close" name="yt1" onClick='$("#patient_detail").dialog("close");'>
                        </span>
                        </div>
                        </form>
                    </div>
                    <div id="tab2" class="tabContent_new">
                    	<form method="post" action="">
                        <input type="hidden" id="popup_insurance_id" />
                        <div class="dashboardcont_leftbox">
                            <div class="box_content_new">
        							<div class="fld_area_new1">
                                        <div class="fld_name">MRN</div>
                                        <div class="name_fld">
                                        <input type="text" id="popup_user_mrn" name="user_mrn" placeholder="MRN" class="fld_class_new">
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="fld_area_new1">
                                        <div class="fld_name">SSN</div>
                                        <div class="name_fld">
                                        <input type="text" id="popup_user_ssn" name="user_ssn" placeholder="SSN" class="fld_class_new">
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="fld_area_new1">
                                        <div class="fld_name">Insurance provider</div>
                                        <div class="name_fld">
                                        <input type="text" id="popup_user_insurance_provider" name="user_insurance_provider" placeholder="Insurance provider" class="fld_class_new">
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="fld_area_new1">
                                        <div class="fld_name">Insurance ID</div>
                                        <div class="name_fld">
                                        <input type="text" id="popup_user_insurance_id" name="user_insurance_id" placeholder="Insurance ID" class="fld_class_new">
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="fld_area_new1">
                                        <div class="fld_name">Insurance group</div>
                                        <div class="name_fld">
                                        <input type="text" id="popup_user_insurance_group" name="user_insurance_group" placeholder="Insurance Group" class="fld_class_new">
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="fld_area_new1">
                                        <div class="fld_name">Employer</div>
                                        <div class="name_fld">
                                        <input type="text" id="popup_user_insurance_employer" name="user_insurance_employer" placeholder="Employer" class="fld_class_new">
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="fld_area_new1">
                                        <div class="fld_name">Employer phone</div>
                                        <div class="name_fld">
                                        <input type="text" id="popup_user_insurance_employer_phone" name="user_insurance_employer_phone" placeholder="Employer phone" class="fld_class_new">
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="fld_area_new1">
                                        <div class="fld_name">Employer address</div>
                                        <div class="name_fld">
                                        <input type="text" id="popup_user_insurance_employer_address" name="user_insurance_employer_address" placeholder="Employer address" class="fld_class_new">
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="fld_area_new1">
                                        <div class="fld_name">Street</div>
                                        <div class="name_fld">
                                        <input type="text" id="popup_user_insurance_employer_street" name="user_insurance_employer_street" placeholder="Street" class="fld_class_new">
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="fld_area_new1">
                                        <div class="fld_name">City</div>
                                        <div class="name_fld">
                                        <input type="text" id="popup_user_insurance_employer_city" name="user_insurance_employer_city" placeholder="City" class="fld_class_new">
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="fld_area_new1">
                                        <div class="fld_name">State</div>
                                        <div class="name_fld">
                                        <input type="text" id="popup_user_insurance_employer_state" name="user_insurance_employer_state" placeholder="State" class="fld_class_new">
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="fld_area_new1">
                                        <div class="fld_name">Zip</div>
                                        <div class="name_fld">
                                        <input type="text" id="popup_user_insurance_employer_zip" name="user_insurance_employer_zip" placeholder="Zip" class="fld_class_new">
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                            </div>
                        </div>
                        <div>
                        <span>
                        <!--<a class="registbt" href="#">Update</a> <a class="registbt" href="">Reset</a>-->
                        <input class="registbt" type="button" value="Update" name="insurance_update" onClick="patientAjaxInsuranceSave();">
                        <input class="registbt" type="button" value="Close" name="yt1" onClick='$("#patient_detail").dialog("close");'>
                        </span>
                        </div>
                        </form>
                    </div>                 
                    <div id="tab3" class="tabContent_new">
                    	<div class="dashboardcont_leftbox_new">
                            <div class="new_comments">
                                <input type="hidden" id="popup_comments_id" />
                                <label>(500 characters maximum)</label>
                                <textarea id="popup_comments" name="comments" class="textarea_length"></textarea>
                                <div style="float:left; width:100%;">
                                	<input class="registbt" type="button" value="Save" name="Save" onClick="patientAjaxCommentsSave();">
                                    <input class="registbt" type="button" value="Cancel" name="Cancel" onClick='$("#patient_detail").dialog("close");'>
                                </div>
                            </div>
                            <div class="view_profile_area_newtab">
                            <div class="view_infoColumn_new">
                                <div class="topRow_new">
                                    <div class="nameDiv_new">
                                         <div class="past_app_msg">
                                         <div><span class="bold">Comments</span><span class="bold">Action</span></div>
                                         <div id="patient_comments_all" class="past_app_new">No Comments</div>
                                         </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>
                        </div>
                    </div>
                    <div id="tab4" class="tabContent_new">
                    	<div class="view_profile_area_newtab">
                            <div class="view_infoColumn_new">
                                <div class="topRow_new">
                                    <div class="nameDiv_new">
                                         <h2>Upcoming appointments</h2>
                                         <div id="upcoming_app" class="past_app_new"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="view_profile_area_newtab">
                            <div class="view_profile_area_newtab">
                                <div class="topRow_new">
                                    <div class="nameDiv_new">
                                         <h2>Past appointments</h2>
                                         <div id="past_app" class="past_app_new"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <div id="tab5" class="tabContent_new">
                    	<div class="box_content_new">
                            <div class="fld_area_new1">
                                <div class="fld_name">Subject</div>
                                <div class="name_fld">
                                <input type="text" id="popup_subject" name="subject" placeholder="Subject" class="fld_class_new">
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="fld_area_new1">
                                <div class="fld_name">Message</div>
                                <div class="name_fld">
                                <div class="new_comments">
                                <textarea id="popup_messages" name="messages" class="textarea_length" style=" width: 93%;" placeholder="Messages"></textarea>
                                </div>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="fld_area_new1">
                                <div class="fld_name">To</div>
                                <div class="name_fld" id="popup_messages_email">
                                
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div>
                        <span>
                        <!--<a class="registbt" href="#">Update</a> <a class="registbt" href="">Reset</a>-->
                        <input class="registbt" type="button" value="Send" name="message_send" onClick="patientAjaxMessagesEmailSave();">
                        <!--<input class="registbt" type="button" value="Send" name="message_send" onClick="patientAjaxMessagesSave();">-->
                        <!--<input class="registbt" type="button" value="Email" name="message_email" onClick="patientAjaxMessagesEmailSave();">-->
                        <input class="registbt" type="button" value="Close" name="yt1" onClick='$("#patient_detail").dialog("close");'>
                        </span>
                        </div>
                        <div class="view_profile_area_newtab">
                            <div class="view_infoColumn_new">
                                <div class="topRow_new">
                                    <div class="nameDiv_new">
                                         <div class="past_app_msg">
                                         <div><span class="bold">Date</span><span class="bold">Subject</span><span class="bold">Status</span></div>
                                         <div id="patient_msg" class="past_app_new">No messages</div>
                                         </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>  
            </div>
</div>
<script type="text/javascript">
	  $(".tabContent_new").hide(); 
	  $("ul.tabs li:first").addClass("active").show(); 
	  $(".tabContent_new:first").show(); 
	 
	  $("ul.tabs li").click(function () {
		$("ul.tabs li").removeClass("active"); 
		$(this).addClass("active"); 
		$(".tabContent_new").hide(); 
		var activeTab = $(this).find("a").attr("href"); 
		$(activeTab).fadeIn(); 
		return false;
	  });
</script>    
<script>
function removeOffers(id){
	if(confirm('Are you sure ?'))
	$.post("<?php echo Yii::app()->request->baseUrl; ?>/doctor/offersAjaxRemove", {id:id},function(response) {
			location.reload();
		});
}
</script>
<script>
function patientDetail(app_book_id,patient_id,created_by){
	$('#hidden_patient_id').val(patient_id);
	var title = $('#patient_first_name_'+app_book_id).html()+' '+$('#patient_last_name_'+app_book_id).html();
	$.post("<?php echo Yii::app()->request->baseUrl; ?>/doctor/patientAjaxInd", {app_book_id:app_book_id,patient_id:patient_id,created_by:created_by},function(response) {
		var obj = jQuery.parseJSON(response);
		if(created_by == 'doctor'){$('.popup_edit').show();$('.popup_non_edit').hide();$('.patient_data_save').show();}
		else{$('.popup_edit').hide();$('.popup_non_edit').show();$('.patient_data_save').hide();}
		$('#popup_user_first_name').val(obj.patient_data.user_first_name);
			$('#popup_user_first_name_html').html(obj.patient_data.user_first_name);
		$('#popup_user_last_name').val(obj.patient_data.user_last_name);
			$('#popup_user_last_name_html').html(obj.patient_data.user_last_name);
		$('#popup_user_parent_legal_guardian').val(obj.patient_data.user_parent_legal_guardian);
			$('#popup_user_parent_legal_guardian_html').html(obj.patient_data.user_parent_legal_guardian);
		$('#popup_user_email').val(obj.patient_data.user_email);
			$('#popup_user_email_html').html(obj.patient_data.user_email);
		$('#popup_user_phone').val(obj.patient_data.user_phone);
			$('#popup_user_phone_html').html(obj.patient_data.user_phone);
		$('#popup_user_address').val(obj.patient_data.user_address);
			$('#popup_user_address_html').html(obj.patient_data.user_address);
		$('#popup_user_city').val(obj.patient_data.user_city);
			$('#popup_user_city_html').html(obj.patient_data.user_city);
		$('#popup_user_state').val(obj.patient_data.user_state);
			$('#popup_user_state_html').html(obj.patient_data.user_state);
		$('#popup_user_zip').val(obj.patient_data.user_zip);
			$('#popup_user_zip_html').html(obj.patient_data.user_zip);
		if(obj.patient_data.user_contact_method == ''){
			$('#popup_user_contact_method').val('Email');
			$('#popup_user_contact_method_html').html('Email');
		}else{
			$('#popup_user_contact_method').val(obj.patient_data.user_contact_method);
			$('#popup_user_contact_method_html').html(obj.patient_data.user_contact_method);
		}
		if(obj.patient_data.user_dob=='' || obj.patient_data.user_dob=='0000-00-00'){
			$('#popup_user_dob').val('');
			$('#popup_user_dob_html').html('');
		}else{
			$('#popup_user_dob').val(obj.patient_data.user_dob);
			$('#popup_user_dob_html').html(obj.patient_data.user_dob);
		}
		if(obj.patient_data.user_recall_frequency == '' || obj.patient_data.user_recall_frequency == 0){
			$('#popup_user_recall_frequency').val(1);
			$('#popup_user_recall_frequency_html').html(1);
		}else{
			$('#popup_user_recall_frequency').val(obj.patient_data.user_recall_frequency);
			$('#popup_user_recall_frequency_html').html(obj.patient_data.user_recall_frequency);
		}
		/* app book data */
		if(obj.app_data.upcoming_app=='')
			$('#upcoming_app').html('No appointments');
		else
			$('#upcoming_app').html(obj.app_data.upcoming_app);
		if(obj.app_data.past_app=='')
			$('#past_app').html('No appointments');
		else
			$('#past_app').html(obj.app_data.past_app);
		
		/* insurance data */
		$('#popup_insurance_id').val('');
		$('#popup_insurance_id').val(obj.patient_insurance.id);
		$('#popup_user_mrn').val(obj.patient_insurance.user_mrn);
		$('#popup_user_ssn').val(obj.patient_insurance.user_ssn);
		$('#popup_user_insurance_provider').val(obj.patient_insurance.user_insurance_provider);
		$('#popup_user_insurance_id').val(obj.patient_insurance.user_insurance_id);
		$('#popup_user_insurance_group').val(obj.patient_insurance.user_insurance_group);
		$('#popup_user_insurance_employer').val(obj.patient_insurance.user_insurance_employer);
		$('#popup_user_insurance_employer_phone').val(obj.patient_insurance.user_insurance_employer_phone);
		$('#popup_user_insurance_employer_address').val(obj.patient_insurance.user_insurance_employer_address);
		$('#popup_user_insurance_employer_street').val(obj.patient_insurance.user_insurance_employer_street);
		$('#popup_user_insurance_employer_city').val(obj.patient_insurance.user_insurance_employer_city);
		$('#popup_user_insurance_employer_state').val(obj.patient_insurance.user_insurance_employer_state);
		$('#popup_user_insurance_employer_zip').val(obj.patient_insurance.user_insurance_employer_zip);
		
		/* comments data */
		if(obj.patient_comments=='')
			$('#patient_comments_all').html('No Comments');
		else
			$('#patient_comments_all').html(obj.patient_comments);
		
		/* messages data */
		if(obj.patient_messages=='')
			$('#patient_msg').html('No Messages');
		else
			$('#patient_msg').html(obj.patient_messages);
		
		$('#popup_messages_email').html(obj.patient_data.user_email);
		$('#popup_comments_id').val('');
		$('#popup_comments').val('');
		$('#popup_subject').val('');
		$('#popup_messages').val('');
		
		$("#patient_detail input,#patient_detail textarea").removeClass('red_class');
		$("#patient_detail").dialog({ modal: true, title: title, width:800 });
	});	
}
function commentsEdit(id){
	$('#popup_comments_id').val(id);
	var comments_val = $('#edit_comments_data_'+id).html();
	$('#popup_comments').val(comments_val);
}
function patientAjaxIndSave(){
	var dataRow = {
        'patient_id': $('#hidden_patient_id').val(),
        'user_first_name': $('#popup_user_first_name').val(),
        'user_last_name': $('#popup_user_last_name').val(),
        'user_parent_legal_guardian': $('#popup_user_parent_legal_guardian').val(),
        'user_email': $('#popup_user_email').val(),
        'user_phone': $('#popup_user_phone').val(),
        'user_address': $('#popup_user_address').val(),
        'user_city': $('#popup_user_city').val(),
        'user_state': $('#popup_user_state').val(),
        'user_zip': $('#popup_user_zip').val(),
        'user_contact_method': $('#popup_user_contact_method').val(),
        'user_dob': $('#popup_user_dob').val(),
        'user_recall_frequency': $('#popup_user_recall_frequency').val()
    }
	var error = false;
	if($.trim($('#popup_user_first_name').val()) == ''){ error = true;$('#popup_user_first_name').addClass('red_class');$('#popup_user_first_name').val(''); }
	if($.trim($('#popup_user_last_name').val()) == ''){ error = true;$('#popup_user_last_name').addClass('red_class');$('#popup_user_last_name').val(''); }
	if(error == true){
		$('#patient_detail input[type="text"]').keyup(function(){
			$(this).removeClass('red_class');
		});
	}
	if(error == false){
    $.ajax({
        type: 'POST',
        url: "<?php echo Yii::app()->request->baseUrl; ?>/doctor/patientAjaxIndSave",
        data: dataRow,
        success: function (response) {
            if (response == 'True') {
                $("#patient_detail").dialog("close");
				location.reload();
            }
        }
    });
	}
}
function patientAjaxInsuranceSave(){
	var dataRow = {
        'id': $('#popup_insurance_id').val(),
        'patient_id': $('#hidden_patient_id').val(),
        'user_mrn': $('#popup_user_mrn').val(),
        'user_ssn': $('#popup_user_ssn').val(),
        'user_insurance_provider': $('#popup_user_insurance_provider').val(),
        'user_insurance_id': $('#popup_user_insurance_id').val(),
        'user_insurance_group': $('#popup_user_insurance_group').val(),
        'user_insurance_employer': $('#popup_user_insurance_employer').val(),
        'user_insurance_employer_phone': $('#popup_user_insurance_employer_phone').val(),
        'user_insurance_employer_address': $('#popup_user_insurance_employer_address').val(),
        'user_insurance_employer_street': $('#popup_user_insurance_employer_street').val(),
        'user_insurance_employer_city': $('#popup_user_insurance_employer_city').val(),
        'user_insurance_employer_state': $('#popup_user_insurance_employer_state').val(),
        'user_insurance_employer_zip': $('#popup_user_insurance_employer_zip').val()
    }
	var error = false;
	/*if($.trim($('#popup_user_first_name').val()) == ''){ error = true;$('#popup_user_first_name').addClass('red_class');$('#popup_user_first_name').val(''); }
	if($.trim($('#popup_user_last_name').val()) == ''){ error = true;$('#popup_user_last_name').addClass('red_class');$('#popup_user_last_name').val(''); }
	if(error == true){
		$('#patient_detail input[type="text"]').keyup(function(){
			$(this).removeClass('red_class');
		});
	}*/
	if(error == false){
    $.ajax({
        type: 'POST',
        url: "<?php echo Yii::app()->request->baseUrl; ?>/doctor/patientAjaxInsuranceSave",
        data: dataRow,
        success: function (response) {
            if (response == 'True') {
                $("#patient_detail").dialog("close");
				//location.reload();
            }
        }
    });
	}
}
function patientAjaxCommentsSave(){
	var dataRow = {
        'id': $('#popup_comments_id').val(),
        'patient_id': $('#hidden_patient_id').val(),
        'comments': $('#popup_comments').val()
    }
	var error = false;
	if($.trim($('#popup_comments').val()) == ''){ error = true;$('#popup_comments').addClass('red_class');$('#popup_comments').val(''); }
	/*if($.trim($('#popup_user_last_name').val()) == ''){ error = true;$('#popup_user_last_name').addClass('red_class');$('#popup_user_last_name').val(''); }*/
	if(error == true){
		$('#patient_detail textarea').keyup(function(){
			$(this).removeClass('red_class');
		});
	}
	if(error == false){
    $.ajax({
        type: 'POST',
        url: "<?php echo Yii::app()->request->baseUrl; ?>/doctor/patientAjaxCommentsSave",
        data: dataRow,
        success: function (response) {
            if (response == 'True') {
                $('#popup_comments').val('');
				$('#popup_comments_id').val('');
				$("#patient_detail").dialog("close");
				//location.reload();
            }
        }
    });
	}
}
function patientAjaxMessagesSave(){
	var dataRow = {
        'patient_id': $('#hidden_patient_id').val(),
        'subject': $('#popup_subject').val(),
        'body': $('#popup_messages').val(),
        'email': ''
    }
	var error = false;
	if($.trim($('#popup_subject').val()) == ''){ error = true;$('#popup_subject').addClass('red_class');$('#popup_subject').val(''); }
	if($.trim($('#popup_messages').val()) == ''){ error = true;$('#popup_messages').addClass('red_class');$('#popup_messages').val(''); }
	if(error == true){
		$('#patient_detail textarea').keyup(function(){
			$(this).removeClass('red_class');
		});
		$('#patient_detail input[type="text"]').keyup(function(){
			$(this).removeClass('red_class');
		});
	}
	if(error == false){
    $.ajax({
        type: 'POST',
        url: "<?php echo Yii::app()->request->baseUrl; ?>/doctor/patientAjaxMessagesSave",
        data: dataRow,
        success: function (response) {
            if (response == 'True') {
                $('#popup_subject').val('');
				$('#popup_messages').val('');
				$("#patient_detail").dialog("close");
				//location.reload();
            }
        }
    });
	}
}
function patientAjaxMessagesEmailSave(){
	var dataRow = {
        'patient_id': $('#hidden_patient_id').val(),
        'subject': $('#popup_subject').val(),
        'body': $('#popup_messages').val(),
        'email': 'email'
    }
	var error = false;
	if($.trim($('#popup_subject').val()) == ''){ error = true;$('#popup_subject').addClass('red_class');$('#popup_subject').val(''); }
	if($.trim($('#popup_messages').val()) == ''){ error = true;$('#popup_messages').addClass('red_class');$('#popup_messages').val(''); }
	if(error == true){
		$('#patient_detail textarea').keyup(function(){
			$(this).removeClass('red_class');
		});
		$('#patient_detail input[type="text"]').keyup(function(){
			$(this).removeClass('red_class');
		});
	}
	if(error == false){
		if($.trim($('#popup_user_email_html').html())!=''){
			$.ajax({
				type: 'POST',
				url: "<?php echo Yii::app()->request->baseUrl; ?>/doctor/patientAjaxMessagesSave",
				data: dataRow,
				success: function (response) {
					if (response == 'True') {
						$('#popup_subject').val('');
						$('#popup_messages').val('');
						$("#patient_detail").dialog("close");
						//location.reload();
					}
				}
			});
		}else{
			alert("Patient don't have email id!");
		}
	}
}
</script>
<input type="hidden" id="hidden_patient_id" name="hidden_patient_id" />
<input type="hidden" id="hidden_app_book_id" name="hidden_app_book_id" />
<script>
function appointmentShow(app_id){
	var patient_id = $('#hidden_patient_id').val();
	var dataRow = {
        'app_id': app_id,
        'patient_id': patient_id
    }
	$.ajax({
		type: 'POST',
		url: "<?php echo Yii::app()->request->baseUrl; ?>/doctor/patientAjaxAppointment",
		data: dataRow,
		success: function (response) {
			var obj = jQuery.parseJSON(response);
			if (obj.return_data == 'True') {
				$('#app_procedure').html(obj.app_data.procedure);
				$('#app_address').html(obj.app_data.address);
				$('#app_scheduled').html(obj.app_data.scheduled);
				$('#app_book_duration').html(obj.app_data.book_duration);
				$('#app_title').html(obj.app_data.title);
				$('#app_event_status').html(obj.app_data.event_status);
				$('#app_event_notes').html(obj.app_data.event_notes);
				$("#eventContent").dialog({ modal: true, title: 'Appointment', width:400 });
			}
		}
	});
}
</script>
<div id="eventContent" title="Event Details" style="display:none;" class="dailog_popup_text">
    <label>Reason for Visit : </label>
   <!-- <input type="text" id="eventProcedure" placeholder="" value="">-->
    <span id="app_procedure"></span><br>
    <label>Address : </label>
    <span id="app_address"></span><br>
    <label>Scheduled : </label>
    <span id="app_scheduled"></span><br>
    <label>Appointment length (min) : </label>
    <span id="app_book_duration"></span><br>
    <label>Person : </label>
    <span id="app_title"></span><br>
    <label>Status : </label>
    <span id="app_event_status"></span><br>
    <label>Notes : </label>
    <span id="app_event_notes"></span>
    <div class="modal-footer">
    <button type="button" id="btnPopupCancel" data-dismiss="modal" class="btn mapbook" onClick='$("#eventContent").dialog("close");'>Close</button>
    <!--<button type="button" id="btnPopupSave" data-dismiss="modal" class="btn btn-primary mapbook">Save</button>
    <button type="button" id="btnPopupDelete" data-dismiss="modal" class="btn mapbook mapbook">Delete</button>-->
  </div>
</div>