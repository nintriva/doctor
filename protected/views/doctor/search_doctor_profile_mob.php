
<script type="text/javascript" src="<?php echo Yii::app()->getBaseUrl(true); ?>/assets/js/rating.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->getBaseUrl(true); ?>/assets/js/jquery.barrating.js"></script>

<script type="text/javascript">
jQuery(document).ready(function() {
    jQuery('.tabs .tab-links a').on('click', function(e)  {
        var currentAttrValue = jQuery(this).attr('href');
 
        // Show/Hide Tabs
        jQuery('.tabs ' + currentAttrValue).show().siblings().hide();
 
        // Change/remove current tab to active
        jQuery(this).parent('li').addClass('active').siblings().removeClass('active');
 
        e.preventDefault();
    });
});

</script>

<link rel="stylesheet" href="<?php echo Yii::app()->getBaseUrl(true); ?>/assets/css/rating.css" type="text/css" media="screen" title="Rating CSS">
<link rel="stylesheet" href="<?php echo Yii::app()->getBaseUrl(true); ?>/assets/css/barrating.css" type="text/css" media="screen" title="Rating CSS">

<div class="profColumn">
    <div class="photoColumn_left">
    
    	<!-- --------------------Fist Area---------------------------- -->   	
    		<div class="leftcolof3">
                <span class="photos">
                    <?php
                    $filePath = 'assets/upload/doctor/' . $doctor_list->id . "/" . $doctor_list->image;
                    if (@file_get_contents($filePath, 0, NULL, 0, 1)) {
                        ?>
                        <a href="javascript:void(0);"><?php echo CHtml::image(Yii::app()->request->baseUrl . '/assets/upload/doctor/' . $doctor_list->id . "/" . $doctor_list->image, "image", array('width' => 200/* ,'height'=>280 */)); ?></a>
                    <?php } else { ?>
                        <?php if ($doctor_list->gender == 'M') { ?>
                            <a href="javascript:void(0);"><?php echo CHtml::image(Yii::app()->request->baseUrl . '/assets/images/avatar.png', "image", array("width" => 200)); ?></a>
                        <?php } else { ?>
                            <a href="javascript:void(0);"><?php echo CHtml::image(Yii::app()->request->baseUrl . '/assets/images/avtar_female.png', "image", array("width" => 200)); ?></a>
                        <?php } ?>
                    <?php } ?>
                </span>
            </div>   
    <!-- ------------ END ----------------- -->    
    </div>
	<div class="profpicx_containerbox">
   		 <div class="midcolof3">
                <div class="locations" >
                    <h2><?php echo $doctor_list->title . " " . $doctor_list->first_name . " " . $doctor_list->last_name; ?>
                    </h2>
                    <?php echo $doctor_list->degree; ?>
                    <br>
                    <div class="divider80"></div>
                    <br>
                    <?php if (isset($user_address[0]['practice_affiliation']) && !empty($user_address[0]['practice_affiliation'])) { ?>
                        <h3><?php echo $user_address[0]['practice_affiliation']; ?></h3>
                    <?php } ?>
                </div>
                <?php
                if (isset($user_address) && !empty($user_address[0])) {
                    $city = ($user_address[0]['city']) ? $user_address[0]['city'] : '';
                    $state = ($user_address[0]['state']) ? ', ' . $user_address[0]['state'] : '';
                    $zip = ($user_address[0]['zip']) ? ', ' . $user_address[0]['zip'] : '';
                    echo $user_address[0]['address'] . '<br>' . $city . $state . $zip;
                } else {
                    $city = ($default_data_address->city) ? $default_data_address->city : '';
                    $state = ($default_data_address->state) ? ', ' . $default_data_address->state : '';
                    $zip = ($default_data_address->zip) ? ', ' . $default_data_address->zip : '';
                    echo $default_data_address->address . '<br>' . $city . $state . $zip;
                }
                ?>

                <div class="locations">
                    <?php
                    $overall_rating = 0;
                    $total_review = count($doctor_review);
                    foreach ($doctor_review as $doctor_review_key => $doctor_review_val) {
                        $overall_rating += $doctor_review_val->overall_rating;
                    }
                    if ($total_review == 0)
                        $average_rating = 0;
                    else {
                        $average_rating = round($overall_rating / $total_review);
                    }
                    ?>                    

                    <div class="rating rating_pos_<?php echo $average_rating; ?>"></div>

	                    <?php
	                    if (Yii::app()->session['logged_in'] == '1' && Yii::app()->session['logged_user_type'] == 'patient') {
	                        ?>
	                        <div id='favDiv'>
	                            <!--#######  after login my favourite part #######-->
	
	                            <?php if ($this->checkFav(Yii::app()->session['logged_user_id'], $doctor_list->id, $specialityID) > 0) { ?>
	                                <!--####### My favourite part #######-->
	                                <div class="already_favorite"><a href='javascript:void(0);' title='My Doctor' onclick='delFav("<?php echo Yii::app()->session['logged_user_id']; ?>", "<?php echo $doctor_list->id; ?>", "<?php echo $specialityID; ?>");'><img src="<?php echo Yii::app()->getBaseUrl(true); ?>/assets/images/active-heart.png" width="16" style="float: left; margin-top: 1px; margin-right: 6px;" /><span style="margin:0">My Doctor</span></a> </div>
	                                <!--####### End My favourite part #######-->
	                            <?php } else { ?>
	                                <!--####### Add to favourite part #######-->
	
	                                <div class="add_to_favorite"><a href='javascript:void(0);' title='Add to My Doctors' onclick='addFav("<?php echo Yii::app()->session['logged_user_id']; ?>", "<?php echo $doctor_list->id; ?>", "<?php echo $specialityID; ?>");'><img src="<?php echo Yii::app()->getBaseUrl(true); ?>/assets/images/inactive-heart.png" width="16" style="float: left; margin-top: 1px; margin-right: 6px;" /> <span style="margin:0">My Doctor</span></a></div>
	                                <!--####### End Add to favourite part #######-->
	                            <?php } ?> 
	                            <!--####### Favourite count part #######-->
	                            <div class="box"><span style="display:block"><?php echo $this->FavCount($doctor_list->id, $specialityID); ?></span></div>
	                            <!--####### End Favourite count part #######-->
	                            <!--####### end after login my favourite part #######-->
	                        </div>
	                    <?php } else { ?>
	                        <!--#######  Before login my favourite part #######-->
	                        <?php if ($this->FavCount($doctor_list->id, $specialityID) > 0) { ?>
	                            <!--#######  Before login favourite Exist part #######-->
	                            <div class="already_favorite"><a id='pop_log' href='javascript:void(0)' title='Add to My Doctors'><img src="<?php echo Yii::app()->getBaseUrl(true); ?>/assets/images/active-heart.png" width="16" style="float: left; margin-top: 1px; margin-right: 6px;" /> <span style="margin:0">My Doctor</span></a></div>
	                            <!--#######  End Before login favourite Exist part #######-->
	                        <?php } else { ?>
	                            <!--#######  Before login favourite not Exist part #######-->
	
	                            <div class="add_to_favorite"><a id='pop_log' href='javascript:void(0)' title='Add to My Doctors'><img src="<?php echo Yii::app()->getBaseUrl(true); ?>/assets/images/inactive-heart.png" width="16" style="float: left; margin-top: 1px; margin-right: 6px;" /> <span style="margin:0">My Doctor</span></a></div>
	                            <!--#######  Before login favourite not Exist part #######-->
	                        <?php } ?>
	                        <!--####### Favourite count part #######-->
	                        <div class="box"><span style="display:block"><?php echo $this->FavCount($doctor_list->id, $specialityID); ?></span></div>
	                        <!--####### End Favourite count part #######-->
	                        <!--#######  Before login my favourite part #######-->
	                    <?php } ?>
	                    </p>
						<div id="popup" style="display: none; text-align: center; font-size: 16px" class="popup1">
							<span class="button b-close"></span>
							<div style="margin-top:70px; line-height:28px;">
								Please Login to add	<h2 style="font-weight:bold;"><?php echo $doctor_list->title . " " . $doctor_list->first_name . " " . $doctor_list->last_name; ?>
		                    	</h2> as your favourite doctor
							</div>
						</div>
	                    <script type="text/javascript">
	
	                        $(function () {
	                            $('#pop_log').click(function () {
	                                $('#popup').bPopup({
	                                    contentContainer: '.content',
	                                });
	                            });
	                        });
	
	                    </script>
	                </div>
	            </div>
    </div>
    <div class="book-button-section">
    </div>

    <div class="profile-review-tab">
    	<div class="tabs">

    <ul class="tab-links">
        <li <?php if( $activeTab == "viewPf" ) { ?>class="active" <?php } ?> ><a href="#tab1">Profile</a></li>
        <li <?php if( $activeTab == "reviewPf" ) { ?>class="active" <?php } ?> ><a href="#tab2">Review</a></li>
    </ul>
 
    <div class="tab-content">
        <div id="tab1" <?php if( $activeTab == "viewPf" ) { ?> class="tab active" <?php } else { ?> class="tab" <?php }?> >
            <?php ?>             
               <!-- --Profile Area Start ------------------------- -->
	                <div class="locations">
	                    <?php if ($doctor_list->comments != "") { ?>
	                        <h3>Professional Statement</h3>
	                        <p><?php echo $doctor_list->comments; ?></p>
	                    <?php } ?>
	                </div>
               <!--R1.1S-->    
	        <?php if ($data_video) { ?>
	            <div class="profilePageRow">
	                <div class="leftcol">
	                    <div class="photos">
	                        <?php echo $data_video[0]['embeded_code']; ?>
	                    </div>
	                    <div id="popup" style="display: none; text-align: center; font-size: 16px" class="popup1">
	                        <div style="margin-top:70px; line-height:28px;">
	                            Please Login to add	<h2 style="font-weight:bold;"><?php echo $doctor_list->title . " " . $doctor_list->first_name . " " . $doctor_list->last_name; ?>
	                            </h2> as your doctor's list
	                        </div>     
	                        <div class="rightcol">
	                            <div class="photos">
	                                <?php // echo $data_video[0]['embeded_code'];  ?>
	                                <?php // echo $data_video[1]['embeded_code'];  ?>
	                            </div>
	                        </div>  
	                    </div>
	                </div>
	            </div>
	        <?php } ?>
	        
	        <!--R2S -->
	        <div class="profilePageRow">
	            <div class="leftcol">
	                <div class="locations">
	                    <h3>Specialties</h3>
	                    <?php
	                    $speciality_count = 0;
	                    $speciality = "";
	                    foreach ($user_speciality as $speciality_key => $speciality_val) {
	                        ?>
	                        <?php
	                        if (in_array($speciality_key, $user_selected_speciality)):
	                            ?>
	                            <span>
	                                <?php
	                                if ($speciality_count == 0) {
	                                    $speciality = $speciality_val;
	                                    $speciality_count ++;
	                                } else {
	                                    $speciality = $speciality . "<BR>" . $speciality_val;
	                                }
	                                ?></span>
	                            <?php
	                        endif;
	                    }
	                    echo $speciality;
	                    ?>
	
	                    <BR><BR>
	                    <?php if (count($user_selected_language) > 0) { ?>
	                        <div class="link-column">
	                            <h3>Languages Spoken</h3>
	                            <p>
	                                <?php //echo "<pre>";print_r($user_selected_language);$user_language  ?>
	                                <?php //$languages_spoken = $doctor_list->languages_spoken;  ?>
	                                <?php
	                                $lang = "";
	                                $iv = 0;
	                                ?>
	                                <?php foreach ($user_language as $lang_key => $lang_val) { ?>
	                                    <?php
	                                    if (in_array($lang_key, $user_selected_language)) {
	                                        if ($iv == 0) {
	                                            $lang = $lang_val;
	                                            $iv++;
	                                        } else {
	                                            $lang = $lang . ", " . $lang_val;
	                                        }
	                                    }
	                                }
	                                ?>
	                                <span><?php echo $lang; ?></span>
	                            </p>
	                        </div>
	                    <?php } ?>
	                </div>
	            </div>
	
	            <div class="rightcol">
	                <!--<h3 class="detailsHeader">Details</span></h3>-->
	                <div class="link-column">
	                    <h2>Education</h2>
	                    <p><?php echo $doctor_list->medical_school; ?> <?php echo ($doctor_list->medical_school_year) ? '(Graduation - ' . $doctor_list->medical_school_year . ')': ''; ?></p>
	                </div>
	                <div class="link-column">
	                    <h2>Residency Training</h2>
	                    <p><?php echo $doctor_list->residency_training; ?> <?php if ($doctor_list->residency_training_year != "0000") { ?><?php echo ($doctor_list->residency_training_year) ? '(Completed - ' . $doctor_list->residency_training_year . ')': ''; ?><?php } ?></p>
	                </div>
	
	                <?php if ($doctor_list->board_certifications != "") { ?>
	                    <div class="link-column">
	                        <h2>Board Certifications</h2>
	                        <p><?php echo $doctor_list->board_certifications; ?></p>
	                    </div>
	                <?php } if ($doctor_list->hospital_affiliations != "") { ?>
	                    <div class="link-column">
	                        <h2>Hospital</h2>
	                        <p><?php echo $doctor_list->hospital_affiliations; ?></p>
	                    </div>
	                <?php } if ($doctor_list->awards_publications != "") { ?>
	                    <div class="link-column">
	                        <h2>Awards and Publications</h2>
	                        <p>
	                            <?php echo $doctor_list->awards_publications; ?>
	                        </p>
	                    </div>
	                <?php } ?>
	            </div>
	        </div> 
	        <!--R2E-->
	        <div class="locations">
                    <?php if (count($user_selected_condition) > 0) { ?>
                        <h3>Conditions Treated</h3>
                        <div class="doctor_condition">
                            <ul>
                                <?php foreach ($user_condition as $condition_key => $condition_val) { ?>
                                    <?php if (in_array($condition_key, $user_selected_condition)): ?>
                                        <li><a href=""><?php echo $condition_val; ?></a></li>
                                        <?php
												endif;
                                			}
                                ?>
                            </ul>
                        </div>
                    <?php } ?>
              </div>
	        	<div class="locations">
                    <?php if (count($user_selected_procedure) > 0 && ( $user_selected_procedure[0] != 0 )) { ?>
                        <div style="float:left">
                            <h3>Procedures</h3>
                            <div class="doctor_condition">
                                <ul>
                                    <?php foreach ($user_procedure as $procedure_key => $procedure_val) { ?>
                                        <?php if (in_array($procedure_key, $user_selected_procedure)): ?>
                                            <li><a href=""><?php echo $procedure_val; ?></a></li>
                                            <?php
                                        endif;
                                    }
                                    ?>
                                </ul>
                            </div>
                        </div>
                    <?php } ?> 
                </div>

                <!--Insurance listing-->
                <div class="locations">
                    <?php if (count($user_selected_insurance_plan_ins) > 0) { ?>
                        <h3>Insurance Accepted</h3>
                        <div class="doctor_condition">
                            <ul>
                                <?php
                                foreach ($user_selected_insurance_plan_ins as $insurance_plan_key => $insurance_plan_val) {
                                    ?>
                                    <?php
                                    $insurance_plan_name = Insurance::model()->getInsuranceNameById($insurance_plan_key);
                                    echo '<li><a href="">' . $insurance_plan_name . '</a></li>';
                                    if (count($insurance_plan_val) > 0) {
                                        $plan_count = 0;
                                        $plan_name_str = "";
                                        foreach ($insurance_plan_val as $plan_key => $planVal) {
                                            if ($plan_count == 0) {
                                                $plan_name_str = $planVal;
                                                $plan_count ++;
                                            } else {
                                                $plan_name_str = $plan_name_str . " ," . $planVal;
                                            }
                                        }
                                        echo '<li><a href="">' . $plan_name_str . '</a></li>';
                                    }
                                    ?>

                                <?php }
                                ?>

                            </ul>
                        </div>

                    <?php }
                    ?>
                </div>   
 <?php ?> 
        </div>
 
        <div id="tab2" <?php if( $activeTab == "reviewPf" ) { ?> class="tab active" <?php } else { ?> class="tab" <?php } ?> >           
                <!--R4S-->
		        <div class="profilePageRow" id="Reviews">
		
		            <div class="reviewsColumn">
		
		                <h2 style="color: #333333;font-size: 16px;font-weight: bold;padding-bottom: 16px;">Reviews</h2>
		
		
		                <?php if (empty($doctor_review)) { ?>
		                    No reviews available for the doctor yet. <BR><BR>
		                    <?php if (Yii::app()->session['logged_user_type'] == 'patient') { ?>
		                        Be the first one to share a review for the doctor!!
		                    <?php } else { ?>
		                        We are working hard to obtain reviews from users like yourself. 
		                        If you have a review to share, login with your eDoctorBook credentials.
		                    <?php } ?>
		                <?php } ?>
		
		                <?php if (!empty($doctor_review)) { ?>
		
		                    <?php foreach ($doctor_review as $doctor_review_key => $doctor_review_val) { ?>
		
		                        <div class="divider"></div>
		                        <br>
		                        <div class="Profile_ReviewsBox">
		                            <div class="whenWho"> Review by <?php echo $doctor_review_val->patient_details->user_first_name . ' ' . $doctor_review_val->patient_details->user_last_name; ?> on <?php echo date('F d, Y', strtotime($doctor_review_val->date_created)); ?> </div>
		                            <div class="rec">
		                                <div class="rating rating_pos_<?php echo $doctor_review_val->schedul_appoitment_rating; ?>"></div>
		                                <div class="explanation">Appointment Scheduling</div>
		                            </div>
		                            <div class="rec bedman">
		                                <div class="rating rating_pos_<?php echo $doctor_review_val->office_experience_rating; ?>"></div>
		                                <div class="explanation"> Office Experience  </div>
		                            </div>
		                            <div class="rec bedman nobdrlast">
		                                <div class="rating rating_pos_<?php echo $doctor_review_val->bedside_manner_rating; ?>"></div>
		                                <div class="explanation"> Listening and Answering Questions </div>
		                            </div>
		                            <div class="rec">
		                                <div class="rating rating_pos_<?php echo $doctor_review_val->spent_time_rating; ?>"></div>
		                                <div class="explanation"> Time Spent with you </div>
		                            </div>
		                            <div class="rec bedman">
		                                <div class="rating rating_pos_<?php echo $doctor_review_val->overall_rating; ?>"></div>
		                                <div class="explanation"> Overall Rating  </div>
		                            </div>
		                            <div class="rec bedman nobdrlast">
		                                <div id="slider-range-min_<?php echo $doctor_review_key; ?>"></div>
		                                <?php
		                                $doctor_review_val->wait_time_rating;
		                                if ($doctor_review_val->wait_time_rating == 0) {
		                                    $msg = "Exact";
		                                } else if ($doctor_review_val->wait_time_rating > 0 && $doctor_review_val->wait_time_rating < 16) {
		                                    $msg = "Under 15 Min";
		                                } else if ($doctor_review_val->wait_time_rating > 15 && $doctor_review_val->wait_time_rating < 31) {
		                                    $msg = "15-30 Min";
		                                } else if ($doctor_review_val->wait_time_rating > 30 && $doctor_review_val->wait_time_rating < 46) {
		                                    $msg = "30-45 Min";
		                                } else {
		                                    $msg = "More Then 45 Min";
		                                }
		                                ?>
		                                <div class="explanation_<?php echo $doctor_review_key; ?>"><?php echo $msg; ?></div>
		                                <div class="explanation"> Wait Time</div>
		                            </div>
		                            <script>
		                                $("#slider-range-min_<?php echo $doctor_review_key; ?>").slider({
		                                    range: "min",
		                                    value: <?php echo $doctor_review_val->wait_time_rating; ?>,
		                                    min: 0,
		                                    max: 60,
		                                    slide: function (event, ui) {
		                                    }
		                                }).slider({disabled: true});
		                                $("a.ui-slider-handle").attr('href', 'javascript:void(0)');
		
		                            </script>
		                            <div class="comments" style="padding-left: 6px;margin-top:3%;display:inline-block;" >
		                                <b>Comments:</b> <?php echo $doctor_review_val->message; ?>
		                                <p>&nbsp;
		                                <p>
		                            </div>
		                        </div>
		
		                    <?php } ?>
		                <?php } ?>

	                <div style="float:left; display:none" id="div1">
	                    <h3>Rating</h3>
	                    <form name="rating" method="post">
	                        <span class="review">
	                            <div class="title">Ease of Scheduling appointment</div>
	                            <div class="star_img">
	                                <div class="star_container">
	                                    <section class="container" id="contaner_scheduling_appointment" >
	                                        <input type="radio" name="example" class="rating_star" value="1" />
	                                        <input type="radio" name="example" class="rating_star" value="2" />
	                                        <input type="radio" name="example" class="rating_star" value="3" />
	                                        <input type="radio" name="example" class="rating_star" value="4" />
	                                        <input type="radio" name="example" class="rating_star" value="5" />
	                                    </section>
	                                </div>
	                                <div class="star_value">
	                                    <div id="rating_html_scheduling_appointment"></div>
	                                    <input type="hidden" id="rating_html_hidden_scheduling_appointment">
	                                </div>
	                            </div>
	                        </span>
	                        <span class="review">
	                            <div class="title">office experience (Comfort,<br>Friendliness,Cleanliness)</div>
	                            <div class="star_img">
	                                <div class="star_container">
	                                    <section class="container" id="contaner_office_experience" >
	                                        <input type="radio" name="example" class="rating_star" value="1" />
	                                        <input type="radio" name="example" class="rating_star" value="2" />
	                                        <input type="radio" name="example" class="rating_star" value="3" />
	                                        <input type="radio" name="example" class="rating_star" value="4" />
	                                        <input type="radio" name="example" class="rating_star" value="5" />
	                                    </section>
	                                </div>
	                                <div class="star_value">
	                                    <div id="rating_html_office_experience"></div>
	                                    <input type="hidden" id="rating_html_hidden_office_experience">
	                                </div>
	                            </div>
	                        </span>
	                        <span class="review">
	                            <div class="title">How well practitioner listened<br>and answered your questions</div>
	                            <div class="star_img">
	                                <div class="star_container">
	                                    <section class="container" id="contaner_beside_manner_rating" >
	                                        <input type="radio" name="example" class="rating_star" value="1" />
	                                        <input type="radio" name="example" class="rating_star" value="2" />
	                                        <input type="radio" name="example" class="rating_star" value="3" />
	                                        <input type="radio" name="example" class="rating_star" value="4" />
	                                        <input type="radio" name="example" class="rating_star" value="5" />
	                                    </section>
	                                </div>
	                                <div class="star_value">
	                                    <div id="rating_html_beside_manner_rating"></div>
	                                    <input type="hidden" id="rating_html_hidden_beside_manner_rating">
	                                </div>
	                            </div>
	                        </span>
	                        <span class="review">
	                            <div class="title">Spent Appropriate time<br>with you</div>
	                            <div class="star_img">
	                                <div class="star_container">
	                                    <section class="container" id="contaner_spent_time_rating" >
	                                        <input type="radio" name="example" class="rating_star" value="1" />
	                                        <input type="radio" name="example" class="rating_star" value="2" />
	                                        <input type="radio" name="example" class="rating_star" value="3" />
	                                        <input type="radio" name="example" class="rating_star" value="4" />
	                                        <input type="radio" name="example" class="rating_star" value="5" />
	                                    </section>
	                                </div>
	                                <div class="star_value">
	                                    <div id="rating_html_spent_time_rating"></div>
	                                    <input type="hidden" id="rating_html_hidden_spent_time_rating">
	                                </div>
	                            </div>
	                        </span>
	                        <span class="review">
	                            <div class="title">Overall rating for the<br>practitioner</div>
	                            <div class="star_img">
	                                <div class="star_container">
	                                    <section class="container" id="contaner_overall_rating" >
	                                        <input type="radio" name="example" class="rating_star" value="1" />
	                                        <input type="radio" name="example" class="rating_star" value="2" />
	                                        <input type="radio" name="example" class="rating_star" value="3" />
	                                        <input type="radio" name="example" class="rating_star" value="4" />
	                                        <input type="radio" name="example" class="rating_star" value="5" />
	                                    </section>
	                                </div>
	                                <div class="star_value">
	                                    <div id="rating_html_overall_rating"></div>
	                                    <input type="hidden" id="rating_html_hidden_overall_rating">
	                                </div>
	                            </div>
	                        </span>
	                        <span class="review">
	                            <div class="title">Total Wait Time</div>
	                            <div class="star_img">
	                                <div id="slider-range-min"></div>
	                                <div id="total_wait_time_rating" style="border:0; color:#f6931f; font-weight:bold;">Slide for wait times</div>
	                                <input type="hidden" id="total_wait_time" >
	                            </div>
	                        </span>
	                        <span class="review">
	                            <label>Comments:</label>
	                            <textarea name="message" id="review_message" cols="" rows="" style="width: 83%; height: 10%; resize: none;" ></textarea>
	                        </span>
	                        <span class="review"><input onclick="patientReview();" type="button" name="review" value="Click to submit your review" class="registbt"></span>
	                    </form>
	                </div>
	                <?php if (Yii::app()->session['logged_user_type'] == 'patient') { ?>
	                    <div id="send_rate_shw_hid">
	                        <br>
	                        <span class="review">
	                            <input onclick="show2();" type="button" name="review" value="Click to submit your review" class="registbt">
	                        </span>
	                    </div>
	                <?php } ?>
	            </div>
	        </div>
	        <!--R4E-->

        </div>

    </div>
</div>
	</div>

	<div class="mob-book-area">
    <?php 
    	if (isset($user_selected_procedure) && count($user_selected_procedure) == 0) {
    		$user_selected_procedure[0] = 0;
    	}
    	$indexCount = 0;
    	if (!empty($user_address) && isset($user_selected_speciality[0]) && isset($user_selected_procedure[0])) {
			foreach ($user_address as $user_address_val) {
				$view_ary_sch = $this->doctorSchTime($doctor_list->id,date('Y-m-d', strtotime("+ 1 day")),date('Y-m-d', strtotime("+ 1 day")), 'Y-m-d',$user_address_val['id']);
				$doctorAppTime = $this->doctorAppTimeMore($doctor_list->id,date('Y-m-d', strtotime("+ 1 day")),date('Y-m-d', strtotime("+ 1 day")), 'Y-m-d',$user_address_val['id']);
				$doctorTimeOff = $this->doctorTimeOffDay($doctor_list->id,date('Y-m-d', strtotime("+ 1 day")),date('Y-m-d', strtotime("+ 1 day")), 'Y-m-d');
				
				$doctor_id = $doctor_list->id;
				$speciality_id = $user_selected_speciality[0];
				$procedure_id =  $user_selected_procedure[0];
				$doctor_address_id = $user_address_val['id'];
				if (!empty($view_ary_sch[$doctor_list->id])) {	
			?>
					<h4><img src="<?php echo Yii::app()->request->baseUrl; ?>/assets/images/address_icon.png" />
					<input type="hidden" id="speciality_id_<?php echo $indexCount; ?>" value="<?php echo $speciality_id; ?>" />
					<input type="hidden" id="procedure_id<?php echo $indexCount; ?>" value="<?php echo $procedure_id; ?>" />
					<input type="hidden" id="doctor_id<?php echo $indexCount; ?>" value="<?php echo $doctor_id; ?>" />
					<input type="hidden" id="today_date<?php echo $indexCount; ?>" value="<?php echo date('Y-m-d', strtotime("+ 1 day")); ?>" />
					<input type="hidden" id="view_date<?php echo $indexCount; ?>" value="<?php echo date('Y-m-d', strtotime("+ 1 day")); ?>" />
					<input type="hidden" id="index_id_pop<?php echo $indexCount; ?>" value="<?php echo $indexCount; ?>" />
					<input type="hidden" id="address_id<?php echo $indexCount; ?>" value="<?php echo $doctor_address_id; ?>" />
					<input type="hidden" id="view_date_<?php echo $indexCount; ?>" value="<?php echo date('Y-m-d', strtotime("+ 1 day")); ?>" /> 
			<?php 	
					if (isset($user_address_val)) {
						$city = ($user_address_val['city']) ? $user_address_val['city'] : '';
						$state = ($user_address_val['state']) ? ', ' . $user_address_val['state'] : '';
						$zip = ($user_address_val['zip']) ? '-' . $user_address_val['zip'] : '';
						echo $user_address_val['address'] . ' ' . $city . $state . $zip;
					}
			?>
                    </h4>
                    <div class="profile-schedule-arrow-control">
                        <div class="navarrow pre_arrow_area_prof" id="pre_arrow_area_prof_<?php echo $user_address_val['id']; ?>" onclick="appointmentScheduleMob('prev','<?php echo $doctor_address_id; ?>','<?php echo $indexCount;?>');"><a href="javascript:void(0);"><img src="<?php echo Yii::app()->request->baseUrl; ?>/assets/images/pre_arrow_mob-light.png" alt="Previous" height="17" width="15" style="padding-right:6px;"></a></div>
                        <div id="schedule_loader_prev_<?php echo $user_address_val['id']; ?>" class="schedule_loader" style="position: absolute; margin: 32px 0; display:none;">
                              <img src="<?php echo Yii::app()->request->baseUrl; ?>/assets/images/loader.gif" />
                        </div>
                        <div class="calander_menu" id="cAreaMenu_<?php echo $indexCount; ?>" >
							<ul>
								<li><?php echo date('l', time()+ 86400 ); ?><span><?php echo date('F d,Y', time() + 86400); ?></span></li>
							</ul>
						</div>
						<div class="navarrow nxt_arrow_area_prof" onclick="appointmentScheduleMob('next','<?php echo $doctor_address_id; ?>','<?php echo $indexCount;?>');"><a href="javascript:void(0);"><img src="<?php echo Yii::app()->request->baseUrl; ?>/assets/images/nxt_arrow_mob-dark.png" alt="Next" height="17" width="15" style="padding-left:7px;"></a></div>
                     </div>
						<div id="schedule_loader_next_<?php echo $user_address_val['id']; ?>" class="schedule_loader" style="position: absolute; margin: 32px 611px;display:none;">
							<img src="<?php echo Yii::app()->request->baseUrl; ?>/assets/images/loader.gif" />
						</div>
			<?php 
					foreach ($view_ary_sch[$doctor_list->id] as $view_ary_sch_key => $view_ary_sch_val) {
						if (!empty($view_ary_sch_val)) {
							$view_ary_sch_time_cnt = false;
							foreach ($view_ary_sch_val as $view_ary_sch_time) {
								if (isset($view_ary_sch_time) && !empty($view_ary_sch_time))
									$view_ary_sch_time_cnt = true;
							}
							if ($view_ary_sch_time_cnt) {
								if($view_ary_sch_time_cnt){

									echo '<div class="calander_box_lisiting_popup" id="cArea_'.$indexCount.'" >';
								
									$tot_cnt=0;$inner_five = 0;
									foreach($view_ary_sch_val as $view_ary_sch_time_val){
										foreach($view_ary_sch_time_val as $view_ary_sch_time_val_arr){
											if($view_ary_sch_time_val_arr['day']==date('l',strtotime($view_ary_sch_key))){
												//if($view_ary_sch_time_val_arr['on_off']==1){
												$view_ary_to_strtotime=$view_ary_sch_time_val_arr['to_strtotime'];
												$view_ary_from_strtotime=$view_ary_sch_time_val_arr['from_strtotime'];
												$time_diff = $view_ary_to_strtotime-$view_ary_from_strtotime;
												$view_ary_time_slot=$view_ary_sch_time_val_arr['time_slot']*60;
												$view_ary_laser_slot=$view_ary_sch_time_val_arr['laser_slot']*60;
													
												$time_length = ($view_ary_to_strtotime-$view_ary_from_strtotime)/(($view_ary_sch_time_val_arr['time_slot']+$view_ary_sch_time_val_arr['laser_slot']*60));
												if($view_ary_sch_time_val_arr['on_off']==1){
													for($iCnt=0;$iCnt<$time_length;$iCnt++){
														$added_time = $view_ary_time_slot+$view_ary_laser_slot;
														$show_time=$view_ary_from_strtotime+($added_time*$iCnt);
														if($show_time < $view_ary_to_strtotime){
															$tot_cnt++;$inner_five++;
															if($iCnt==0){
																if(isset($doctorAppTime[$doctor_id][$view_ary_sch_key])&&in_array($view_ary_sch_time_val_arr['from_strtotime'],$doctorAppTime[$doctor_id][$view_ary_sch_key])){
																	echo '<span class="innertable_time_popup disable"><a class="blank" href="javascript:void(0);">'.date('h:i  a',$view_ary_sch_time_val_arr['from_strtotime']).'</a></span>';
																}else{
																	if(isset($doctorTimeOff[$doctor_id][$view_ary_sch_key]) && $view_ary_sch_time_val_arr['from_strtotime']>=$doctorTimeOff[$doctor_id][$view_ary_sch_key][0]['strt_time_off'] && $view_ary_sch_time_val_arr['from_strtotime']<=$doctorTimeOff[$doctor_id][$view_ary_sch_key][0]['to_time_off']){
																		echo '<span class="innertable_time_popup disable"><a class="blank" href="javascript:void(0);">'.date('h:i  a',$view_ary_sch_time_val_arr['from_strtotime']).'</a></span>';
																	}else{
																		echo '<span class="innertable_time_popup"><a href="'.$this->createAbsoluteUrl('doctor/DoctorBookStep1/?start_time='.$view_ary_sch_time_val_arr['from_strtotime'].'&doctor_id='.$doctor_id.'&address_id='.$view_ary_sch_time_val_arr['address_id'].'&speciality_id='.$speciality_id).'&time_slot='.$view_ary_time_slot.'&procedure_id='.$procedure_id.'">'.date('h:i  a',$view_ary_sch_time_val_arr['from_strtotime']).'</a></span>';
																	}
																}
															}else{
																if(isset($doctorAppTime[$doctor_id][$view_ary_sch_key])&&in_array($show_time,$doctorAppTime[$doctor_id][$view_ary_sch_key])){
																	echo '<span class="innertable_time_popup disable"><a class="blank" href="javascript:void(0);">'.date('h:i  a',$show_time).'</a></span>';
																}else{
																	if(isset($doctorTimeOff[$doctor_id][$view_ary_sch_key]) && $show_time>=$doctorTimeOff[$doctor_id][$view_ary_sch_key][0]['strt_time_off'] && $show_time<=$doctorTimeOff[$doctor_id][$view_ary_sch_key][0]['to_time_off']){
																		echo '<span class="innertable_time_popup disable"><a class="blank" href="javascript:void(0);">'.date('h:i  a',$show_time).'</a></span>';
																	}else{
																		echo '<span class="innertable_time_popup"><a href="'.$this->createAbsoluteUrl('doctor/DoctorBookStep1/?start_time='.$show_time.'&time_slot='.$view_ary_time_slot.'&doctor_id='.$doctor_id.'&address_id='.$view_ary_sch_time_val_arr['address_id'].'&speciality_id='.$speciality_id).'&procedure_id='.$procedure_id.'">'.date('h:i a',$show_time).'</a></span>';
																	}
																}
															}
														}
													}
												}else{
													/* -------  Disable Part ----------- */
													for($iCnt=0;$iCnt<$time_length;$iCnt++){
														$added_time = $view_ary_time_slot+$view_ary_laser_slot;
														$show_time=$view_ary_from_strtotime+($added_time*$iCnt);
														if($show_time < $view_ary_to_strtotime){
															echo '<span class="innertable_time_popup disable" style="background:#cdcdcd;" ><a class="blank" href="javascript:void(0);">'.date('h:i  a',$view_ary_sch_time_val_arr['from_strtotime']).'</a></span>';
														}
													}
													/* --------------- Disable Part End --------------- */
												}
											}
										}
									}
								
									echo '</div>';
								
								}

							}
						} else {
			?>
						<div class="calander_box_lisiting_popup">
							<span class="innertable_time_first disable" style="background:#cdcdcd;" ><a class="blank" href="javascript:void(0);">08:00 am</a></span>
							<span class="innertable_time_first disable" style="background:#cdcdcd;" ><a class="blank" href="javascript:void(0);">08:30 am</a></span>
							<span class="innertable_time_first disable" style="background:#cdcdcd;" ><a class="blank" href="javascript:void(0);">09:00 am</a></span>
							<span class="innertable_time_first disable" style="background:#cdcdcd;" ><a class="blank" href="javascript:void(0);">09:30 am</a></span>
							<span class="innertable_time_first disable" style="background:#cdcdcd;" ><a class="blank" href="javascript:void(0);">more...</a></span>
							
						</div>
			<?php
						}
					}
					
				} else {
			?>
			
								<h4><img src="<?php echo Yii::app()->request->baseUrl; ?>/assets/images/address_icon.png" />
                                    <?php //echo $user_address_val['address']; ?>
                                    <?php
                                    if (isset($user_address_val)) {
                                        $city = ($user_address_val['city']) ? $user_address_val['city'] : '';
                                        $state = ($user_address_val['state']) ? ', ' . $user_address_val['state'] : '';
                                        $zip = ($user_address_val['zip']) ? '-' . $user_address_val['zip'] : '';
                                        echo $user_address_val['address'] . ' ' . $city . $state . $zip;
                                    }
                                    ?>
                                </h4>
                                <div class="navarrow pre_arrow_area_prof"><a href="javascript:void(0);"><img src="<?php echo Yii::app()->request->baseUrl; ?>/assets/images/pre_arrow_mob-light.png" alt="Previous" height="17" width="15" style="padding-right:6px;"></a></div>
                                <div class="calander_menu_big">
                                    <ul>
                                        <li><?php echo date('D', time()); ?> <span><?php echo date('m-d-Y', time()); ?></span></li>
                                     </ul>
                                </div>
                                <div class="navarrow nxt_arrow_area_prof"><a href="javascript:void(0);"><img src="<?php echo Yii::app()->request->baseUrl; ?>/assets/images/nxt_arrow_mob-light.png" alt="Next" height="17" width="15" style="padding-left:7px;"></a></div>
	                                <div class="calander_box_lisiting_popup">
	                                    <div class="column_bar_left"></div>
	                                    <div class="sch_time_profile">
	                                        <span class="innertable_time_popup disable"><a class="blank" href="javascript:void(0);">09:00AM</a></span>
	                                        <span class="innertable_time_popup disable"><a class="blank" href="javascript:void(0);">09:30AM</a></span>
	                                        <span class="innertable_time_popup disable"><a class="blank" href="javascript:void(0);">10:00AM</a></span>
	                                        <span class="innertable_time_popup disable"><a class="blank" href="javascript:void(0);">10:30AM</a></span>
	                                        <span class="innertable_time_popup disable"><a class="blank" href="javascript:void(0);">11:00AM</a></span>
	                                    </div>
	                                    <div class="column_bar_right"></div>
	                                </div>
			<?php 
				}
				$indexCount++;
			}	
		} 
	?>
    </div>
</div>
<script>
function appointmentScheduleMob(type,addId,iCount) {
    var today_date = $('#today_date'+iCount).val();
    var view_date = $('#view_date_'+iCount).val();
    var speciality_id = $('#speciality_id_'+iCount).val();
    var procedure = $('#procedure_id'+iCount).val();
    var doctor_id = $('#doctor_id'+iCount).val();
    var address_id = $('#address_id'+iCount).val();
 	var index_id_pop = $('#index_id_pop'+iCount).val(); 	 
    if (type == 'prev' && today_date == view_date) {
        return false;
    } else {
    	$('.schedule_loader').css('display','none');
        if (type == 'next') {
            $('#schedule_loader_next_' + address_id).show();
        } else {
            $('#schedule_loader_prev_' + address_id).show();
        }
        var sch_width = "14";
        $.post("<?php echo Yii::app()->request->baseUrl; ?>/doctor/appointmentScheduleMob",
                {type: type, today_date: today_date, view_date: view_date, speciality_id: speciality_id, procedure_id: procedure, doctor_id: doctor_id, address_id: address_id, sch_width: sch_width,index_id_pop: index_id_pop},
        function (response) {

                	var res = response.split("***%%%***");
        			$('#cAreaMenu_'+iCount).html(res[0]);
        			//$('#cArea_'+address_id).html(res[1]);
        			var areaId = "cArea_"+iCount;
        			document.getElementById(areaId).innerHTML = res[1];
        			document.getElementById("view_date_"+iCount).value=res[2];
        			//$('#view_date').val(res[2]);    			
        			if(today_date == res[2])
        				$('.pre_arrow_area img').attr('src','<?php echo Yii::app()->request->baseUrl; ?>/assets/images/pre_arrow_mob-light.png');
        			else
        				$('.pre_arrow_area img').attr('src','<?php echo Yii::app()->request->baseUrl; ?>/assets/images/pre_arrow_mob-dark.png');
        			$('.schedule_loader').css('display','none');
        			
           
        });
    }
}

</script>