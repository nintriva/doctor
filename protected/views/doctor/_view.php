<?php
/* @var $this DoctorController */
/* @var $model Doctor */

$this->breadcrumbs=array(
	'Dashboard'=>array('index'),
	'View Profile',
);

/*$this->menu=array(
	array('label'=>'List Doctor', 'url'=>array('index')),
	array('label'=>'Manage Doctor', 'url'=>array('admin')),
);*/
?>

<!--<h1>Create Doctor</h1>-->

<?php //$this->renderPartial('_form', array('model'=>$model)); ?>

<div class="main">
    <div id="breadcrumb" class="fk-lbreadbcrumb newvd">
        <!--<span><a href="">Home</a></span> >  
        <span>Dashboard</span>--> 
        <?php $this->widget('zii.widgets.CBreadcrumbs', array(
				  'links'=>$this->breadcrumbs,
			  ));
		?>
    </div>
  	  <div class="dashboard_mainarea">
     	<div class="leftmenu">
     	<?php /*
       		 <h2>Doctor control panel</h2>
       	*/ ?>	 
             <ul>
            	 <li><?php echo CHtml::link('Dashboard', $this->createAbsoluteUrl('index')); ?></li>
                 <li><a href="#">Appointments/Calender</a></li>
                 <li><?php echo CHtml::link('Patients', $this->createAbsoluteUrl('doctor/patient/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li><?php echo CHtml::link('Task Manager', $this->createAbsoluteUrl('doctor/todolist/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li><?php echo CHtml::link('Time-Off/Holidays Setup', $this->createAbsoluteUrl('doctor/timeoff/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li>
                 <!--<a href="#">Edit My Account</a>-->
                 <?php echo CHtml::link('Profile Setup', $this->createAbsoluteUrl('doctor/editProfile/'.Yii::app()->session['logged_user_id'])); ?>
                 </li>
                 <li><?php echo CHtml::link('My Addresses', $this->createAbsoluteUrl('doctor/address/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li><?php echo CHtml::link('My Specialities', $this->createAbsoluteUrl('doctor/speciatlity/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li class="active"><?php echo CHtml::link('View Profile', $this->createAbsoluteUrl('doctor/profile/'.Yii::app()->session['logged_user_id'])); ?></li>
                 
                 <li><?php echo CHtml::link('Schedules Setup', $this->createAbsoluteUrl('doctor/schedule/'.Yii::app()->session['logged_user_id'])); ?></li>
                 
                 
                 
                 <li><?php echo CHtml::link('Setting Tab', $this->createAbsoluteUrl('doctor/settingTab/'.Yii::app()->session['logged_user_id'])); ?></li>
             </ul>
        </div>
        <div class="rightarea_dashboard">
        	<div class="view_profile_area">
            	<div class="view_profileLeftColumn">
                     <span class="photos"><img style="height:280px; width:180px;" alt="" src="<?php echo Yii::app()->request->baseUrl; ?>/assets/images/dr_bigpicx.jpg"></span>
                </div>
                <div class="view_infoColumn">
                	<div class="topRow">
                        <div class="nameDiv">
                             <h2><?php echo $model->title." ".$model->first_name." ".$model->last_name; ?></h2>
                             <p><?php echo $model->degree; ?></p>
                        </div>
                    </div>
                    <div class="bottomRow">
                    	<div class="col3">
                            <div class="specialties"> 
                                 <h3>Specialties</h3>
                                 <?php
								 if($doctor_speciality):
								 $cunt_spe = count($doctor_speciality);
								 for($i=0;$i<$cunt_spe;$i++){
								 ?>
                                     <a href=""><?php echo  $user_speciality[$doctor_speciality[$i]['speciality_id']]; ?></a>
                                 <?php
								 if(($cunt_spe-1)!=$i){	echo ","; }
								 }
								 endif;
								 ?>
                            </div>
                            <div class="locations"> 
                                <h3>Professional Statement</h3>
                                <p><?php echo $model->comments; ?></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
            
            <div class="view_profile_area">
            	<div class="view_detailsColumn">
                	<h3 class="detailsHeader">Details for <span><?php echo $model->title." ".$model->first_name." ".$model->last_name; ?>, <?php echo $model->degree; ?>, FAAD</span></h3>
                    <?php if($model->education): ?>
                    <div class="link-column">
                        <h2>Education</h2>
                        <p><?php echo $model->education; ?> </p>
                    </div>
                    <?php endif; ?>
                    <?php if($model->hospital_affiliations): ?>
                    <div class="link-column">
                        <h2>Hospital Affiliations</h2>
                        <p><?php echo $model->hospital_affiliations; ?> </p>
                    </div>
                    <?php endif; ?>
                    <?php if($model->board_certifications): ?>
                    <div class="link-column">
                        <h2>Board Certifications</h2>
                        <p><?php echo $model->board_certifications; ?> </p>
                    </div>
                    <?php endif; ?>
                    <?php if($model->awards_publications): ?>
                    <div class="link-column">
                        <h2>Awards and Publications</h2>
                        <p><?php echo $model->awards_publications; ?> </p>
                    </div>
                    <?php endif; ?>
                    <?php if($model->languages_spoken): ?>
                    <div class="link-column">
                        <h2>Languages Spoken</h2>
                        <p><?php echo $model->languages_spoken; ?> </p>
                    </div>
                    <?php endif; ?>
                </div>
                <?php if($address_list && count($address_list)>0): ?>
                <div class="reviewsColumn">
                    <div class="link-column">
                        <h2>Addresses</h2>
                        <?php for($add_cnt=0 ; $add_cnt<count($address_list); $add_cnt++){ ?>
                        <p><?php echo $address_list[$add_cnt]->address;//print_r($address_list); ?> </p>
                        <?php } ?>
                    </div>
                    
                	<div style="width:100%; height:280px;">
                    <iframe width="100%" height="100%" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.co.in/?ie=UTF8&amp;ll=<?php echo $address_list[1]->latitude; ?>,<?php echo $address_list[1]->longitude; ?>&amp;spn=1.79388,2.688904&amp;t=m&amp;z=9&amp;output=embed"></iframe>
                    </div>
                </div>
                <?php endif; ?>
                <div class="clear"></div>
            </div>
		</div> 
      </div>
</div>
<script>
$(function() {
	$( "#Doctor_birth_date" ).datepicker({
		changeMonth: true,changeYear: true, yearRange : '-80:-20',dateFormat: 'yy-mm-dd'
	});
});
</script>