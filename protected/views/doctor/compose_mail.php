<?php

$this->breadcrumbs=array(
	'Messaging Center'=>array('compose'),
	'Compose Mail',
);
?>
<div class="main">
  <div id="breadcrumb" class="fk-lbreadbcrumb newvd">
    <!--<span><a href="">Home</a></span> >  
    <span>Dashboard</span>--> 
    <?php $this->widget('zii.widgets.CBreadcrumbs', array(
      'links'=>$this->breadcrumbs,
    ));
  ?>
  </div>
  <div class="dashboard_mainarea">
    <div class="leftmenu">
    	<?php $this->renderPartial('//layouts/navigation'); ?>
    </div>
    <div class="rightarea_dashboard">
    	<div class="dashboardcont_leftbox">
    		<h1>Compose Mail</h1>
        	<div class="box_content">
            <div class="fld_area">
              <label class="fld_name required">To</label>                                        
              <div class="name_fld">
              	<input type="text" class="fld_class"  maxlength="32" size="32">                                                                                
              </div>
              <div class="clear"></div>
            </div>
            <div class="fld_area">
              <label class="fld_name required">Subject</label>                                        
              <div class="name_fld">
              	<input type="text"  class="fld_class" maxlength="32" size="32">                                                                                
              </div>
              <div class="clear"></div>
            </div>
            <div class="fld_area">
              <div class="fld_name fld_name_hight">Message</div>
              <div class="name_fld">
              	<textarea class="txtarea_class" maxlength="256" size="32"></textarea>                                        
              </div>
              <div class="clear"></div>
            </div>
            <div>
            	<label class="fld_name required">&nbsp;</label>  
              <span>
              	<input type="submit" value="Submit" class="registbt">                            
              	<input type="reset" value="Reset" class="registbt">                            
              </span>
            </div>
          </div>
      </div>
    </div>
  </div>
</div>