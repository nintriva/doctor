<?php
$this->breadcrumbs=array(
	'ContactDoctor',
);
?>
<div class="main">
    <div class="regis_contentarea">
    	<div class="wrapper">
		<!-- Form -->
		<?php $form=$this->beginWidget('CActiveForm', array(
				'id'=>'contact-form',
				'htmlOptions' => array(
					'enctype' => 'multipart/form-data',
				),
			)); ?>
			
		<!--
				<style type="text/css">
				   body {
						   font-family: sans-serif;
						   font-size: 14px;
				   }
				</style>
       
			   <script src="http://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places" type="text/javascript"></script>
				<script type="text/javascript">
					   function initialize() {
					            var options = {
										types: ['(locality)'],
										componentRestrictions: {country: "US"}
								};
							   var input = document.getElementById('searchTextField');
							   var autocomplete = new google.maps.places.Autocomplete(input);
					   }
					   google.maps.event.addDomListener(window, 'load', initialize);
			   </script>         
               <div>
                       <input id="searchTextField" type="text" size="50" placeholder="Enter a location" autocomplete="on">
               </div>
         -->
		 	
			<h3>Tell about eDoctorbook to your doctor</h3>
			<h4>We would love you to drop note to your doctor about edoctorbook and ask them to join.</h4>
			<div>
				<label>
					<span>Your Name: *</span>
					<?php echo $form->textField($model,'name',array('placeholder'=> 'Please enter your name')); ?>
                    <?php echo $form->error($model,'name'); ?>
				</label>
			</div>
			<div>
				<label>
					<span>Your Email: *</span>
					<?php echo $form->textField($model,'email',array('placeholder'=> 'Please enter your email address')); ?>
                    <?php echo $form->error($model,'email'); ?>
				</label>
			</div>
			<div>
				<label>
					<span>Doctor's Name: *</span>
					<?php echo $form->textField($model,'to_name',array('placeholder'=> 'Please enter doctor\'s name')); ?>
                    <?php echo $form->error($model,'to_name'); ?>
				</label>
			</div>
			<div>
				<label>
					<span>Doctor's email: *</span>
					<?php echo $form->textField($model,'to_email',array('placeholder'=> 'Please enter doctor\'s email')); ?>
                    <?php echo $form->error($model,'to_email'); ?>
				</label>
			</div>
			<div>
				<label>
					<span>Your message: *</span>
					<?php echo $form->textArea($model,'cover_letter',array('placeholder'=> 'Include few lines ')); ?>
                    <?php echo $form->error($model,'cover_letter'); ?><?php echo $form->error($model,'resume'); ?>
				</label>
			</div>
            <div>
            	<label id="label">
                	<?php if(CCaptcha::checkRequirements()): ?>
                    	<span>Verification Code:</span>
                        <?php $this->widget('CCaptcha'); ?>
                        <?php echo $form->textField($model,'verifyCode'); ?>
                        <div class="hint">Please enter the letters as shown in the image above.
						<br/>Letters are not case-sensitive.</div>
                        <?php echo $form->error($model,'verifyCode'); ?>
                    <?php endif; ?>
                </label>
            </div>
			<div style="margin-top:15px;">
				<button name="submit" type="submit" id="contact-submit">Submit</button>
			</div>
		<?php $this->endWidget(); ?>
		<!-- /Form -->
	</div>
		
    </div>
    </div>