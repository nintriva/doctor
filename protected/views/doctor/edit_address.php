<?php
/* @var $this DoctorController */
/* @var $model Doctor */

$this->breadcrumbs=array(
	'Dashboard'=>array('index'),
	'Edit Profile',
);

/*$this->menu=array(
	array('label'=>'List Doctor', 'url'=>array('index')),
	array('label'=>'Manage Doctor', 'url'=>array('admin')),
);*/
?>

<!--<h1>Create Doctor</h1>-->

<?php //$this->renderPartial('_form', array('model'=>$model)); ?>

<div class="main">
    <div id="breadcrumb" class="fk-lbreadbcrumb newvd">
        <!--<span><a href="">Home</a></span> >  
        <span>Dashboard</span>--> 
        <?php $this->widget('zii.widgets.CBreadcrumbs', array(
				  'links'=>$this->breadcrumbs,
			  ));
		?>
    </div>
  	  <div class="dashboard_mainarea">
     	<div class="leftmenu">
        	 <p>
			 <?php if(Yii::app()->user->hasFlash('editAddress')): ?>
                <span class="flash-success">
                    <?php echo Yii::app()->user->getFlash('editAddress'); ?>
                </span>
             </p>
            <?php endif; ?>
       		 <?php /*?><h2>Doctor control panel</h2>
             <ul>
            	 <li><?php echo CHtml::link('Dashboard', $this->createAbsoluteUrl('index')); ?></li>
                 <li>
                 <!--<a href="#">Edit My Account</a>-->
                 <?php echo CHtml::link('Edit My Account', $this->createAbsoluteUrl('doctor/editProfile/'.Yii::app()->session['logged_user_id'])); ?>
                 </li>
                 <li class="active"><?php echo CHtml::link('My Addresses', $this->createAbsoluteUrl('doctor/address/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li><?php echo CHtml::link('My Specialities', $this->createAbsoluteUrl('doctor/speciatlity/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li><?php echo CHtml::link('View Profile', $this->createAbsoluteUrl('doctor/profile/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li><a href="#">Appointments</a></li>
                 <li><?php echo CHtml::link('Schedules', $this->createAbsoluteUrl('doctor/schedule/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li><?php echo CHtml::link('Timeoff', $this->createAbsoluteUrl('doctor/timeoff/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li><?php echo CHtml::link('Todo List', $this->createAbsoluteUrl('doctor/todolist/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li><?php echo CHtml::link('Patients', $this->createAbsoluteUrl('doctor/patient/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li><?php echo CHtml::link('Setting Tab', $this->createAbsoluteUrl('doctor/settingTab/'.Yii::app()->session['logged_user_id'])); ?></li>
             </ul><?php */?>
             <?php $this->renderPartial('//layouts/navigation'); ?>
        </div>
        <div class="rightarea_dashboard">
          <div class="dashboard_content1">
          	<?php if(Yii::app()->user->hasFlash('editProfile')): ?>
            <span class="flash-success">
                <?php echo Yii::app()->user->getFlash('editProfile'); ?>
            </span>
        <?php endif; ?>
          	<h1 class="h1"><?php echo $model->isNewRecord ? 'Insert' : 'Update'; ?> Your Address</h1>
             <?php $form=$this->beginWidget('CActiveForm', array(
				'id'=>'edit_address',
			)); ?>
            	<span>
                	<?php echo $form->labelEx($model,'address'); ?>
                    <div class="name_fld">
					<?php echo $form->textArea($model,'address',array('size'=>32,'maxlength'=>255,'placeholder'=> 'Address','class'=>'fld_class')); ?>
					<?php echo $form->error($model,'address'); ?>
                    </div>
                </span>
                <span>
                	<?php echo $form->labelEx($model,'latitude'); ?>
                    <div class="name_fld">
					<?php echo $form->textField($model,'latitude',array('size'=>32,'maxlength'=>32,'placeholder'=>'Latitude','class'=>'fld_class')); ?>
                    <?php echo $form->error($model,'latitude'); ?>
                    </div>
                </span>
                <span>
                	<?php echo $form->labelEx($model,'longitude'); ?>
                    <div class="name_fld">
					<?php echo $form->textField($model,'longitude',array('size'=>32,'maxlength'=>32,'placeholder'=>'Longitude','class'=>'fld_class')); ?>
					<?php echo $form->error($model,'longitude'); ?>
                    </div>
                </span>
                <span>
                	<?php echo $form->labelEx($model,'order_status'); ?>
                    <div class="name_fld">
					<?php echo $form->textField($model,'order_status',array('size'=>32,'maxlength'=>2,'placeholder'=>'Order','class'=>'fld_class')); ?>
                    <?php echo $form->error($model,'order_status'); ?>
                    </div>
                </span>
                <span>
                	<?php echo $form->labelEx($model,'default_status'); ?>
					<?php //echo $form->textField($model,'default_status',array('size'=>32,'maxlength'=>32,'placeholder'=>'Default')); ?>
                    <div class="name_fld">
                    <?php echo $form->checkBox($model,'default_status',  array()); ?>
                    <?php echo $form->error($model,'default_status'); ?>
                    </div>
                </span>
                <span>
                	<?php echo $form->labelEx($model,'active'); ?>
					<?php //echo $form->textField($model,'active',array('size'=>32,'maxlength'=>32,'placeholder'=>'Active')); ?>
                    <div class="name_fld">
                    <?php echo $form->checkBox($model,'active',  array()); ?>
                    <?php echo $form->error($model,'active'); ?>
                    </div>
                </span>
                <span>
                <?php echo CHtml::submitButton($model->isNewRecord ? 'Save' : 'Update',array('class'=>'grn_btn')); ?>
                </span>
            <?php $this->endWidget(); ?>
          </div>
        </div> 
      </div>
</div>