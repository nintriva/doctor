<script src="<?php echo Yii::app()->request->baseUrl; ?>/assets/js/script.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/assets/js/jquery.prettyPhoto.js" type="text/javascript"></script>
<link href="<?php echo Yii::app()->request->baseUrl; ?>/assets/css/prettyPhoto.css" rel="stylesheet" type="text/css" />
<?php
$this->breadcrumbs=array(
	'Dashboard'=>array('index'),
	'Edit Profile',
);
?>

<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/assets/js/jquery.aw-showcase.js"></script>
<script type="text/javascript">
$(document).ready(function(){
  function add() {
    if($(this).val() === ''){
      $(this).val($(this).attr('placeholder')).addClass('placeholder');
    }
  }

  function remove() {
    if($(this).val() === $(this).attr('placeholder')){
      $(this).val('').removeClass('placeholder');
    }
  }
  

  // Create a dummy element for feature detection
  if (!('placeholder' in $('<input>')[0])) {

    // Select the elements that have a placeholder attribute
    $('input[placeholder], textarea[placeholder]').blur(add).focus(remove).each(add);

    // Remove the placeholder text before the form is submitted
    $('form').submit(function(){
      $(this).find('input[placeholder], textarea[placeholder]').each(remove);
    });
  }
});
</script>

<!--for tabs start-->
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/assets/css/tabs.css">
<!--for tabs end-->

<!--for field open start-->
<script language="javascript">
function displayTextField()
{
	/*$("#existItemText").hide();
	$("#existItem").show();
	$("#existItem").focus();
	event.stopPropagation();*/	
}
function hideTextField()
{
	$("#existItemText").show();
	$("#existItem").hide();
}
function displayTextField2()
{
	/*$("#existItemText2").hide();
	$("#existItem2").show();
	$("#existItem2").focus();	*/
}
function hideTextField2()
{
	$("#existItemText2").show();
	$("#existItem2").hide();
}
function displayTextField3()
{
	/*$("#existItemText2").hide();
	$("#existItem2").show();
	$("#existItem2").focus();	*/
}
function hideTextField3()
{
	$("#existItemText3").show();
	$("#existItem3").hide();
}
</script>
<!--for field open end-->

<!--for popup start-->
<link href="<?php echo Yii::app()->request->baseUrl; ?>/assets/css/popup.css" rel="stylesheet" type="text/css" media="all" />
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/assets/js/popupscript.js"></script>
<!--for popup end -->

<!--tooltip start-->
<script type="text/javascript">
var offsetfromcursorX=12 //Customize x offset of tooltip
var offsetfromcursorY=10 //Customize y offset of tooltip

var offsetdivfrompointerX=10 //Customize x offset of tooltip DIV relative to pointer image
var offsetdivfrompointerY=14 //Customize y offset of tooltip DIV relative to pointer image. Tip: Set it to (height_of_pointer_image-1).

document.write('<div id="dhtmltooltip"></div>') //write out tooltip DIV
document.write('<img id="dhtmlpointer" src="<?php echo Yii::app()->request->baseUrl; ?>/assets/images/arrow2.gif">') //write out pointer image

var ie=document.all
var ns6=document.getElementById && !document.all
var enabletip=false
if (ie||ns6)
var tipobj=document.all? document.all["dhtmltooltip"] : document.getElementById? document.getElementById("dhtmltooltip") : ""

var pointerobj=document.all? document.all["dhtmlpointer"] : document.getElementById? document.getElementById("dhtmlpointer") : ""

function ietruebody(){
return (document.compatMode && document.compatMode!="BackCompat")? document.documentElement : document.body
}

function ddrivetip(thetext, thewidth, thecolor){
if (ns6||ie){
if (typeof thewidth!="undefined") tipobj.style.width=thewidth+"px"
if (typeof thecolor!="undefined" && thecolor!="") tipobj.style.backgroundColor=thecolor
tipobj.innerHTML=thetext
enabletip=true
return false
}
}

function positiontip(e){
if (enabletip){
var nondefaultpos=false
var curX=(ns6)?e.pageX : event.clientX+ietruebody().scrollLeft;
var curY=(ns6)?e.pageY : event.clientY+ietruebody().scrollTop;
//Find out how close the mouse is to the corner of the window
var winwidth=ie&&!window.opera? ietruebody().clientWidth : window.innerWidth-20
var winheight=ie&&!window.opera? ietruebody().clientHeight : window.innerHeight-20

var rightedge=ie&&!window.opera? winwidth-event.clientX-offsetfromcursorX : winwidth-e.clientX-offsetfromcursorX
var bottomedge=ie&&!window.opera? winheight-event.clientY-offsetfromcursorY : winheight-e.clientY-offsetfromcursorY

var leftedge=(offsetfromcursorX<0)? offsetfromcursorX*(-1) : -1000

//if the horizontal distance isn't enough to accomodate the width of the context menu
if (rightedge<tipobj.offsetWidth){
//move the horizontal position of the menu to the left by it's width
tipobj.style.left=curX-tipobj.offsetWidth+"px"
nondefaultpos=true
}
else if (curX<leftedge)
tipobj.style.left="5px"
else{
//position the horizontal position of the menu where the mouse is positioned
tipobj.style.left=curX+offsetfromcursorX-offsetdivfrompointerX+"px"
pointerobj.style.left=curX+offsetfromcursorX+"px"
}

//same concept with the vertical position
if (bottomedge<tipobj.offsetHeight){
tipobj.style.top=curY-tipobj.offsetHeight-offsetfromcursorY+"px"
nondefaultpos=true
}
else{
tipobj.style.top=curY+offsetfromcursorY+offsetdivfrompointerY+"px"
pointerobj.style.top=curY+offsetfromcursorY+"px"
}
tipobj.style.visibility="visible"
if (!nondefaultpos)
pointerobj.style.visibility="visible"
else
pointerobj.style.visibility="hidden"
}
}

function hideddrivetip(){
if (ns6||ie){
enabletip=false
tipobj.style.visibility="hidden"
pointerobj.style.visibility="hidden"
tipobj.style.left="-1000px"
tipobj.style.backgroundColor=''
tipobj.style.width=''
}
}

document.onmousemove=positiontip
</script>

<div class="main">
        <div id="breadcrumb" class="fk-lbreadbcrumb newvd">
            <?php $this->widget('zii.widgets.CBreadcrumbs', array(
					  'links'=>$this->breadcrumbs,
				  ));
			?>
        </div>
  	  <div class="dashboard_mainarea">
     	<div class="leftmenu lftmenu_hight">
       		 <?php /*?><h2>Doctor control panel</h2>
             <ul>
            	 <li><?php echo CHtml::link('Dashboard', $this->createAbsoluteUrl('index')); ?></li>
                 <li class="active"><?php echo CHtml::link('My Account', $this->createAbsoluteUrl('doctor/editProfile/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li><?php echo CHtml::link('Special Offers', $this->createAbsoluteUrl('doctor/offers/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li><?php echo CHtml::link('Appointments', $this->createAbsoluteUrl('doctor/appointment/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li><?php echo CHtml::link('Schedules', $this->createAbsoluteUrl('doctor/schedule/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li><?php echo CHtml::link('Timeoff', $this->createAbsoluteUrl('doctor/timeoff/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li><?php echo CHtml::link('Todo List', $this->createAbsoluteUrl('doctor/todolist/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li><?php echo CHtml::link('Patients', $this->createAbsoluteUrl('doctor/patient/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li><?php echo CHtml::link('Setting Tab', $this->createAbsoluteUrl('doctor/settingTab/'.Yii::app()->session['logged_user_id'])); ?></li>
             </ul><?php */?>
             <?php $this->renderPartial('//layouts/navigation'); ?>
        </div>
        <div class="rightarea_dashboard">
        	<div class="tabBox">
            	<ul class="tabs">
                	<li><a href="#tab1">Personal Info</a></li>
                    <li><a href="#tab2">Addresses</a></li>
                    <li><a href="#tab4">Conditions &amp; Procedures</a></li>
                    <li><a href="#tab5">View Profile </a></li>
                    <li><a href="#tab6">Video</a></li>
                    <li><a href="#tab7">Reset Password</a></li>
                </ul>
                <div class="tabContainer">
                	<div id="tab1" class="tabContent">
 						<!--<form method="post" action="">-->
                        <?php $form=$this->beginWidget('CActiveForm', array(
							'id'=>'registration',
							'htmlOptions' => array(
								'enctype' => 'multipart/form-data',
							),
						)); ?>
                        <input type="hidden" name="hidden_id" id="hidden_id" value="<?php echo $model->id; ?>" />
                        <div class="dashboardcont_leftbox">
                            <?php if(Yii::app()->user->hasFlash('editProfile')): ?>
                            <span class="flash-success">
                                <?php echo Yii::app()->user->getFlash('editProfile'); ?>
                            </span>
                            <?php endif; ?>
                            <h1>Personal Information</h1>
                            <div class="box_content">
        							<!--<div class="fld_area">
                                        <div class="fld_name">Title</div>
                                        <div class="name_fld"><select class="select_fld_class"><option>Mr.</option><option>Ms.</option></select></div>
                                        <div class="clear"></div>
                                    </div>-->
                                    <div class="fld_area">
                                        <?php echo $form->labelEx($model,'title',array('class'=>'fld_name')); ?>
                                        <div class="name_fld">
                                        <?php
											  $selected_title = $model->title;
											  echo CHtml::dropDownList('title', $selected_title, 
											  $model->titleOptions,
											  array(/*'empty' => 'Select your title',*/'class'=>'select_fld_class'));
										?>
                                        <?php echo $form->error($model,'title'); ?>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="fld_area">
                                        <?php echo $form->labelEx($model,'first_name',array('class'=>'fld_name')); ?>
                                        <div class="name_fld">
                                        <?php echo $form->textField($model,'first_name',array('size'=>32,'maxlength'=>32,'placeholder'=> 'First Name','class'=>'fld_class')); ?>
                                        <?php echo $form->error($model,'first_name'); ?>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <!--<div class="fld_area">
                                        <div class="fld_name">Middle Name</div>
                                        <div class="name_fld"><input type="text" name="lnme" placeholder="Middle Name" class="fld_class"></div>
                                        <div class="clear"></div>
                                    </div>-->
                                    <div class="fld_area">
                                        <?php echo $form->labelEx($model,'last_name',array('class'=>'fld_name')); ?>
                                        <div class="name_fld">
                                        <?php echo $form->textField($model,'last_name',array('size'=>32,'maxlength'=>32,'placeholder'=> 'Last Name','class'=>'fld_class')); ?>
                                        <?php echo $form->error($model,'last_name'); ?>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="fld_area">
                                        <?php echo $form->labelEx($model,'Gender',array('class'=>'fld_name')); ?>
                                        <div class="name_fld">
                                        <?php
											  $selected_gender = $model->gender;

											  echo CHtml::dropDownList('gender', $selected_gender, 
											  $model->genderOptions,
											  array('class'=>'select_fld_class'));
										?>
										<?php echo $form->error($model,'gender'); ?>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="fld_area">
                                        <?php echo $form->labelEx($model,'birth_date',array('class'=>'fld_name')); ?>
                                        <div class="name_fld">
                                            <div class="select_option">
                                                <?php
													  $birth_date_mm = $birth_date['mm'];
													  echo CHtml::dropDownList('mm', $birth_date_mm, 
													  $model->monthOptions,
													  array('class'=>'fld_class2'));
												?>
                                            </div>
                                            <div class="select_option2">
                                                <?php
													  $birth_date_dd = $birth_date['dd'];
													  echo CHtml::dropDownList('dd', $birth_date_dd, 
													  $model->dateOptions,
													  array('class'=>'fld_class2'));
												?>
                                            </div>
                                            <div class="select_option3">
                                            	<?php
													  $birth_date_yy = $birth_date['yy'];
													  echo CHtml::dropDownList('yy', $birth_date_yy, 
													  $yearOptions,
													  array('class'=>'fld_class2'));
												?>
                                            </div>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                   
        
                            </div>
                        </div>
                        
                        <div class="dashboardcont_leftbox">
                            <h1>Profile Details</h1>
                            <div class="box_content">
        
                                    <div class="fld_area">
                                        <?php echo $form->labelEx($model,'image',array('class'=>'fld_name')); ?>
                                        <div class="name_fld">
                                        	<input type="hidden" id="doctor_ind_image_hidden" value="<?php echo $model->image; ?>" />
                                            <span id="doctor_ind_image">
                                            <?php
											//if($model->image){
											$filePath = 'assets/upload/doctor/'.$model->id."/".$model->image;
											if(@file_get_contents($filePath, 0, NULL, 0, 1)){ 
											?>
                                            	<a class="image-zoom" href="<?php echo Yii::app()->request->baseUrl.'/assets/upload/doctor/'.$model->id."/".$model->image.'?myVar=text&iframe=true&width=65%&height=80%'; ?>" rel="prettyPhoto[gallery]" title="<?php echo $model->title; ?> <?php echo $model->first_name; ?> <?php echo $model->last_name; ?>"><?php echo CHtml::image(Yii::app()->request->baseUrl.'/assets/upload/doctor/'.$model->id."/".$model->image,"image",array("width"=>100)); ?> </a>
                                            <?php }else{ ?>
                                                <?php if($model->gender=='M'){ ?>
                                                <a href="javascript:void(0);"><?php echo CHtml::image(Yii::app()->request->baseUrl.'/assets/images/avatar.png',"image",array("width"=>90)); ?></a>
                                                <?php }else{ ?>
                                                <a href="javascript:void(0);"><?php echo CHtml::image(Yii::app()->request->baseUrl.'/assets/images/avtar_female.png',"image",array("width"=>90)); ?></a>
                                                <?php } ?>
                                            <?php } ?>
                                            </span>
                                            <span class="browse_area">
                                            	<!--<input type="file" name="" />-->
                                                <?php echo CHtml::activeFileField($model, 'image'); ?> 
                                                <a href="javascript:void(0);" onclick="doctorImageDel();"><img src="<?php echo Yii::app()->request->baseUrl; ?>/assets/images/delete_icon.png" alt="Delete" title="Delete" /></a>
                                                <?php echo $form->error($model,'image'); ?>
                                                <div><a href="javascript:void(0);" onmouseout="hideddrivetip()" ;="" onmouseover="ddrivetip('please upload correct width (100 to 300)px and height (100 to 300)px image.')">Photo Guideline</a></div>
                                            </span>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="fld_area">
                                        <?php echo $form->labelEx($model,'phone',array('class'=>'fld_name')); ?>
                                        <div class="name_fld">
                                        <?php echo $form->textField($model,'phone',array('size'=>32,'maxlength'=>32,'placeholder'=>'Phone','class'=>'fld_class')); ?>
                                		<?php echo $form->error($model,'phone'); ?>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <!--<div class="fld_area">
                                        <div class="fld_name">Mobile Phone</div>
                                        <div class="name_fld"><input type="text" name="lnme" placeholder="Mobile Phone" class="fld_class"></div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="fld_area">
                                        <div class="fld_name">Fax</div>
                                        <div class="name_fld"><input type="text" name="lnme" placeholder="Fax" class="fld_class"></div>
                                        <div class="clear"></div>
                                    </div>-->
                                    <div class="fld_area">
                                        <?php echo $form->labelEx($model,'email',array('class'=>'fld_name')); ?>
                                        <div class="name_fld">
                                        <?php echo $form->textField($model,'email',array('size'=>32,'maxlength'=>32,'placeholder'=>'Email','class'=>'fld_class')); ?>
                                        <?php echo $form->error($model,'email'); ?>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="fld_area">
                                        <label class="fld_name required" for="Doctor_google_account">Google Account<span class="required"> (Auto synchronization for Appointment)</span></label>
                                        <div class="name_fld">
                                        <?php echo $form->textField($model,'google_account',array('size'=>32,'maxlength'=>32,'placeholder'=>'Google Email Address','class'=>'fld_class')); ?>
                                        <?php echo $form->error($model,'google_account'); ?>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <?php /*?><div class="fld_area">
                                        <?php echo $form->labelEx($model,'addr1',array('class'=>'fld_name')); ?>
                                        <div class="name_fld">
                                        <?php echo $form->textArea($model,'addr1',array('size'=>32,'maxlength'=>155,'placeholder'=>'Address','class'=>'txtarea_class')); ?>
                                		<?php echo $form->error($model,'addr1'); ?>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="fld_area">
                                        <?php echo $form->labelEx($model,'zip',array('class'=>'fld_name')); ?>
                                        <div class="name_fld">
                                        <?php echo $form->textField($model,'zip',array('size'=>32,'maxlength'=>32,'placeholder'=>'ZIP Code','class'=>'fld_class')); ?>
                                        <?php echo $form->error($model,'zip'); ?>
                                        </div>
                                        <div class="clear"></div>
                                    </div><?php */?>
                                    <div class="fld_area">
                                        <?php echo $form->labelEx($model,'speciality',array('class'=>'fld_name')); ?>
                                        <div class="name_fld">
                                        <?php 
										$selected = $model->speciality;
										  echo CHtml::dropDownList('speciality', $selected, 
										  $user_speciality,
										  array('empty' => 'Specialty','class'=>'select_fld_class'));
										  ?>
                                        <?php echo $form->error($model,'speciality'); ?>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="fld_area">
                                        <?php echo $form->labelEx($model,'visit_price',array('class'=>'fld_name')); ?>
                                        <div class="name_fld">
                                        <?php echo $form->textField($model,'visit_price',array('size'=>32,'maxlength'=>32,'placeholder'=>'e.g. 10','class'=>'fld_class')); ?>
                                        <?php echo $form->error($model,'visit_price'); ?>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="fld_area">
                                        <?php echo $form->labelEx($model,'visit_duration',array('class'=>'fld_name')); ?>
                                        <div class="name_fld">
                                        <?php echo $form->textField($model,'visit_duration',array('size'=>32,'maxlength'=>32,'placeholder'=>'e.g. 30','class'=>'fld_class')); ?>
                                        <?php echo $form->error($model,'visit_duration'); ?>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
        							<div class="fld_area">
                                        <div class="fld_name">Professional Statement</div>
                                        <div class="name_fld">
                                        <?php echo $form->textArea($model,'comments',array('size'=>32,'maxlength'=>800,'placeholder'=> 'Comments','class'=>'txtarea_class')); ?>
                                		<?php echo $form->error($model,'comments'); ?>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                            </div>
                        </div>
                        
                        <div class="dashboardcont_leftbox">
                            <h1>Account Details</h1>
                            <div class="box_content">
        
                                    <div class="fld_area">
                                        <?php echo $form->labelEx($model,'username',array('class'=>'fld_name')); ?>
                                    <div class="name_fld">
                                    <?php echo $form->textField($model,'username',array('size'=>32,'maxlength'=>32,'placeholder'=> 'Username','class'=> 'readonly fld_class','readonly'=> 'readonly')); ?>
                                    <?php echo $form->error($model,'username'); ?>
                                    </div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="fld_area">
                                        <?php echo $form->labelEx($model,'languages_spoken',array('class'=>'fld_name')); ?>
                                        <div class="name_fld">
                                        <?php
											  $selected_languages_spoken = $model->languages_spoken;
											  echo CHtml::dropDownList('languages_spoken', $selected_languages_spoken, 
											  $user_language,
											  array('empty' => 'Select your preferred language','class'=>'select_fld_class'));
										?>
                                        <?php echo $form->error($model,'languages_spoken'); ?>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                            </div>
                        </div>
                        
                        <div class="dashboardcont_leftbox">
                            <h1>Professional Information</h1>
                            <div class="box_content">
        
                                    <div class="fld_area">
                                         <?php echo $form->labelEx($model,'npi_no',array('class'=>'fld_name')); ?>
                                        <div class="name_fld">
                                            <?php echo $form->textField($model,'npi_no',array('size'=>32,'maxlength'=>255,'placeholder'=> 'NPI No.','class'=>'fld_class','value'=>$model->npi_no)); ?>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="fld_area">
                                        <?php echo $form->labelEx($model,'degree',array('class'=>'fld_name')); ?>
                                        <div class="name_fld">
                                        <?php
                                              $selected_degree = $model->degree;
                                              echo CHtml::dropDownList('degree', $selected_degree, 
                                              $model->degreeOptions,
                                              array('empty' => 'Degree','class'=>'select_fld_class'));
                                        ?>
                                        <?php echo $form->error($model,'degree'); ?>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="fld_area">
                                         <?php echo $form->labelEx($model,'university',array('class'=>'fld_name')); ?>
                                        <div class="name_fld">
                                            <?php echo $form->textField($model,'medical_school',array('size'=>32,'maxlength'=>255,'placeholder'=> 'University','class'=>'fld_class')); ?>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="fld_area">
                                         <?php echo $form->labelEx($model,'Graduation Year',array('class'=>'fld_name')); ?>
                                        <div class="name_fld">
                                            	<?php
													  $selected_medical_school_year = $model->medical_school_year;
													  echo CHtml::dropDownList('medical_school_year', $selected_medical_school_year, $yearOptions,array('class'=>'fld_class2'));
												?>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="fld_area">
                                        <?php echo $form->labelEx($model,'residency_training',array('class'=>'fld_name fld_name_hight')); ?>
                                        <div class="name_fld">
                                            <?php echo $form->textField($model,'residency_training',array('size'=>32,'maxlength'=>255,'placeholder'=> 'Residency Training','class'=>'fld_class')); ?>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="fld_area">
                                        <?php echo $form->labelEx($model,'Completion Year',array('class'=>'fld_name fld_name_hight')); ?>
                                        <div class="name_fld">
                                            	<?php
													  $selected_residency_training_year = $model->residency_training_year;
													  echo CHtml::dropDownList('residency_training_year', $selected_residency_training_year, $yearOptions,array('class'=>'fld_class2'));
												?>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="fld_area">
                                        <?php echo $form->labelEx($model,'hospital',array('class'=>'fld_name fld_name_hight')); ?>
                                        <div class="name_fld">
                                        <?php echo $form->textField($model,'hospital_affiliations',array('size'=>32,'maxlength'=>255,'placeholder'=> 'Hospital','class'=>'fld_class')); ?>
                                		<?php echo $form->error($model,'hospital_affiliations'); ?>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="fld_area">
                                        <div class="fld_name fld_name_hight">Certification<br>(max: 256 chars)</div>
                                        <div class="name_fld">
                                        <?php echo $form->textArea($model,'board_certifications',array('size'=>32,'maxlength'=>256,'placeholder'=> 'Certification','class'=>'txtarea_class')); ?>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="fld_area">
                                        <div class="fld_name fld_name_hight">Awards and Publications<br>(max: 256 chars)</div>
                                        <div class="name_fld">
                                        <?php echo $form->textArea($model,'awards_publications',array('size'=>32,'maxlength'=>255,'placeholder'=> 'Awards and Publications','class'=>'txtarea_class')); ?>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="fld_area">
                                        <div class="fld_name fld_name_hight">Languages Spoken</div>
                                        <input type="hidden" id="hidden_language" value="<?php echo implode(",",$user_selected_language); ?>" />
                                        <div class="name_fld">
                                        	<div class="insurancePlanList clearfix" id="language_html">
                                                <?php foreach($user_language as $lang_key => $lang_val){ ?>
												<?php if(in_array($lang_key,$user_selected_language)): ?>
                                                <div class="insurancePlan" id="language_html_id_<?php echo $lang_key; ?>"><span><?php echo $lang_val; ?></span><a class="removePlan hgIconX" title="Delete" onclick="langRemove('<?php echo $lang_key; ?>');"></a></div>
                                                <?php
												endif;
												}
												?>
                                            </div>
                                            <div><a href="javascript:void(0);" class="langPopup" onclick="languageChecked();">Add Language</a></div>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                            </div>
                        </div>
                        <div class="dashboardcont_leftbox">
                            <h1>Insurances Accepted</h1>
                            <div class="box_content">
        						<div>Edit the list of insurance carriers you accept. 
                                Keep the list up to date to ensure that your profile is not excluded when patients and referring 
                                providers filter their search results by specific insurance carriers.
                                </div><input type="hidden" id="hidden_insurance" value="<?php echo implode(",",$user_selected_insurance); ?>" />
                                <div class="insurance_area">
                                	<?php foreach($user_insurance as $insurance_key => $insurance_val){ ?>
                                    <?php if(in_array($insurance_key,$user_selected_insurance)): ?>
                                    <div class="main_insurance_list" id="main_insurance_list_id_<?php echo $insurance_key; ?>">
                                    	<div class="mainBucketListItemRow existingItem">
                                            <div class="errorContainer">
                                                <p style="display: none;" class="errorLabel"></p>
                                            </div>
                                            <label>
                                            <?php echo $insurance_val; ?>
                                            <div class="addPlan">
                                                <a href="javascript:void(0);" class="choosePlan plan_popup_click" id="AETNA" onclick="planChecked('<?php echo $insurance_key; ?>');">Add <span><?php echo $insurance_val; ?></span> Plan</a>
                                            </div>
                                            </label>
                                            
                                            <a class="removeItem hgIconX" title="Delete" onclick="insuranceRemove('<?php echo $insurance_key; ?>');"></a>
                                        </div>
                                        <input type="hidden" class="hidden_plan" id="hidden_plan_<?php echo $insurance_key; ?>" value="<?php echo implode(",",$user_selected_insurance_plan_ins[$insurance_key]); ?>" />
                                        <div class="insurancePlanList clearfix" id="plan_html_<?php echo $insurance_key; ?>">
                                            <?php foreach($user_insurance_plan_res_ins[$insurance_key] as $insurance_plan_key => $insurance_plan_val){ ?>
                                    		<?php if(in_array($insurance_plan_key,$user_selected_insurance_plan_ins[$insurance_key])): ?>
                                            <div class="insurancePlan" id="insurancePlan_del_<?php echo $insurance_plan_key; ?>"><span><?php echo $insurance_plan_val; ?></span><a class="removePlan hgIconX" title="Delete" onclick="planRemove('<?php echo $insurance_plan_key; ?>');"></a></div>
                                            
											<?php
                                            endif;
                                            }
                                            ?>
                                        </div>
                                    </div>
                                    
                                    <!-- -->
                                    <div id="planOfPopup_<?php echo $insurance_key; ?>" class="planOfPopup" style="height:370px; " > 
                                    	<div class="close"></div>
                                        <span class="ecs_tooltip">Press Esc to close <span class="arrow"></span></span>
                                        <div id="popup_content" class="planPopContent" >
                                            <h2 class="popup_heading">Add Plans</h2>
                                            <div class="popup_subheading">You may select more than one option below.</div>
                                            <div class="total_fld"  style="margin-top:10px; margin-bottom:10px;">
                                                <?php foreach($user_insurance_plan_res_ins[$insurance_key] as $user_insurance_plan_key => $user_insurance_plan_val){ ?>
                                                <p><input type="checkbox" name="user_selected_language[]" value="<?php echo $user_insurance_plan_key; ?>" <?php if(in_array($user_insurance_plan_key,$user_selected_insurance_plan_ins[$insurance_key])) echo 'checked="checked"'; ?> class="plan_class_<?php echo $insurance_key; ?>"/> <?php echo $user_insurance_plan_val; ?></p>
                                                <?php } ?>
                                            </div>
                                            <div>
                                                <input type="button" value="Save" class="registbt plan_save_button"> <input type="button" value="Cancel" class="registbt cancel_button">
                                            </div>
                                        </div> 
                                </div>
                                <!-- -->
                                    <?php
									endif;
									}
									?>
                                    
                                </div>
                               <label>
                                <div class="addPlan_p4">
                                  <a href="javascript:void(0);" class="choosePlan plan4Popup" id="AETNA" onclick="insuranceChecked();">Choose from Top 20 Nationwide Carriers</a>
                                </div>
                           	   </label>
                            </div>
                        </div>
                        <?php if(Yii::app()->session['yiiadmin__id']){ ?>
                        <div class="dashboardcont_leftbox">
                            <h1>Status Information</h1>
                            <div class="box_content">
        							<div class="fld_area">
                                        <?php echo $form->labelEx($model,'status',array('class'=>'fld_name')); ?>
                                        <div class="name_fld">
                                        <?php
											  /*$selected_status = $model->status;
											  echo CHtml::dropDownList('status', $selected_status, 
											  array(1=>'Active',0=>'inactive'),
											  array('class'=>'select_fld_class'));*/
										?>
                                        <?php $selected_status = $model->status; ?>
                                        <input type="checkbox" name="status" value="off" <?php echo ($selected_status==0)?'checked':''; ?>/> Inactive
                                        <?php echo $form->error($model,'title'); ?>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="fld_area">
                                        <?php echo $form->labelEx($model,'lock_profile',array('class'=>'fld_name')); ?>
                                        <div class="name_fld">
                                        <?php $selected_lock_profile = $model->lock_profile; ?>
                                        <input type="checkbox" name="lock_profile" value="1" <?php echo ($selected_lock_profile==1)?'checked':''; ?>/> 
                                        <?php echo $form->error($model,'lock_profile'); ?>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                            </div>
                        </div>
                        <?php } ?>
                        <?php if(!Yii::app()->session['yiiadmin__id'] && $model->lock_profile==0){ ?>
                            <div><span>
                            <?php echo CHtml::submitButton($model->isNewRecord ? 'Update' : 'Save',array('class'=>'registbt')); ?>
                            <?php echo CHtml::resetButton($model->isNewRecord ? 'Reset' : 'Reset',array('class'=>'registbt')); ?>
                            </span></div>
                        <?php } ?>
                         <?php if(Yii::app()->session['yiiadmin__id']){ ?>
                            <div><span>
                            <?php echo CHtml::submitButton($model->isNewRecord ? 'Update' : 'Save',array('class'=>'registbt')); ?>
                            <?php echo CHtml::resetButton($model->isNewRecord ? 'Reset' : 'Reset',array('class'=>'registbt')); ?>
                            </span></div>
                        <?php } ?>
                        <?php $this->endWidget(); ?>
                    </div>
                    <div id="tab2" class="tabContent">
                    	<div class="dashboard_content1">
            	<div class="add_area">
                	<span class="add_new_btn"><?php echo CHtml::link('Add new', $this->createAbsoluteUrl('doctor/editAddress/'),array('class'=>'grn_btn addPopup','onclick'=>'address(\'\');')); ?></span>
                    <?php /*?><span class="refresh"><a href="javascript:void(0)"><img src="<?php echo Yii::app()->request->baseUrl; ?>/assets/images/refresh_iocn.png" alt="" /></a></span><?php */?>
                    <div class="clear"></div>
                </div>
           	  <div class="dashboardcont_leftbox2">
                	<ul>
                        <li class="heading">
                         <span class="add">Address</span>
                         <span class="att txt_align">Phone</span>
                         <span class="active txt_align">City</span>
                         <span class="active txt_align">State</span>
                         <span class="att txt_align">Zip</span>
                         <span class="att txt_align">Action</span>
                        </li>
                        <?php
						if($data_address):
						for($i=0;$i<count($data_address);$i++){
						?>
                        <li>
                         <span class="add"><?php echo $data_address[$i]['address']; ?></span>
                         <span class="att txt_align"><?php echo $data_address[$i]['office_phone']; ?></span>  
                         <span class="active txt_align"><?php echo $data_address[$i]['city']; ?></span>
                         <span class="active txt_align"><?php echo $data_address[$i]['state']; ?></span> 
                         <span class="att txt_align"><?php echo $data_address[$i]['zip']; ?></span> 
                         <span class="att txt_align">
                         <?php echo CHtml::link('<img src="'.Yii::app()->request->baseUrl.'/assets/images/edit_icon.png" alt="" />', $this->createAbsoluteUrl('doctor/editAddress/'.$data_address[$i]['id']),array("class"=>"addPopup",'onclick'=>'address('.$data_address[$i]['id'].');')); ?>
                         <a href="javascript:void(0);" onclick="removeAddress(<?php echo $data_address[$i]['id']; ?>);"><img src="<?php echo Yii::app()->request->baseUrl; ?>/assets/images/delete_icon.png" alt=""/></a>
                         </span>
                        </li>
                        <?php
						 }
						 else:
						 	echo '<li style="text-align: center;">No Address Yet.</li>';
					     endif;
					    ?>
                    </ul>
                </div>
                <?php $this->widget('CLinkPager', array(
                        'pages' => $pages,'header'=>'','prevPageLabel'=>'&lt;&lt;','nextPageLabel'=>'&gt;&gt;',
                    ))
				?>
            </div>
                    </div>
                  	<div id="tab4" class="tabContent">
                    	<div class="dashboardcont_leftbox">
                            <h1>Conditions &amp; Procedures</h1>
                            <div class="fld_area">
                                <div class="fld_name fld_name_hight">Specialties</div>
                                <input type="hidden" id="hidden_speciality" value="<?php echo implode(",",$user_selected_speciality); ?>" />
                                <div class="name_fld">
                                    <div class="insurancePlanList clearfix" id="speciality_html">
                                        <?php foreach($user_speciality as $speciality_key => $speciality_val){ ?>
                                        <?php if(in_array($speciality_key,$user_selected_speciality)): ?>
                                        <div class="insurancePlan" id="speciality_html_id_<?php echo $speciality_key; ?>"><span><?php echo $speciality_val; ?></span><a class="removePlan hgIconX" title="Delete" onclick="specialityRemove('<?php echo $speciality_key; ?>');"></a></div>
                                        <?php
                                        endif;
                                        }
                                        ?>
                                    </div>
                                    <div><a href="javascript:void(0);" class="specialityPopup" onclick="specialityChecked();">Add speciality</a></div>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <script>
							function chooseSC(type,id){
								var fileType='';
								if(type=='speciality')	fileType='condition';
								if(type=='condition')	fileType='procedure';
								$.post("<?php echo Yii::app()->request->baseUrl; ?>/doctor/"+fileType+"Find", {id:id},function(response) {
									if(type=='speciality'){
										var response_arr = response.split('***###***');
										$('#adv_condition_id').html(response_arr[0]);
										$('#adv_procedure_id').html(response_arr[1]);
									}
								});
							}
							function chooseSCP(type,id){
								var hidval = [];
								$(".condition_class").each(function(){
									if($(this).is(":checked")){
										var ind_condition_id = $(this).val();
										hidval.push(ind_condition_id);
									}
								});
								var hidstr = hidval.join(',');
								$.post("<?php echo Yii::app()->request->baseUrl; ?>/doctor/procedureFind", {id:hidstr},function(response) {
									$('#adv_procedure_id').html(response);
									//alert(response);
								});
							}
							</script>
                            <div id="specialityPopup"> 
    							<div class="close"></div>
                                <span class="ecs_tooltip">Press Esc to close <span class="arrow"></span></span>
                                
                                <div id="popup_content" class="specialityPopup_column">
                                    <h2 class="popup_heading" style="font-weight: bold;">Please select your Speciality</h2>
                                    <div class="popup_subheading">You may select one option below.</div>
                                    <div class="total_fld" id="adv_speciality_id">
                                        <?php /*foreach($user_speciality as $speciality_key => $speciality_val){ ?>
                                        <?php if(!in_array($speciality_key,$user_selected_speciality)){ ?>
                                        <p><input type="radio" name="user_selected_speciality[]" id="user_selected_speciality_id_<?php echo $speciality_key; ?>" onclick="chooseSC('speciality','<?php echo $speciality_key; ?>')" value="<?php echo $speciality_key; ?>" <?php if(in_array($speciality_key,$user_selected_speciality)) echo 'checked="checked"'; ?> class="speciality_class"/> <?php echo $speciality_val; ?></p>
                                        <?php } ?>
										<?php }*/ ?>
                                        
                                        <select onchange="chooseSC('speciality',this.value)">
                                        <option>Select</option>
                                        <?php foreach($user_speciality as $speciality_key => $speciality_val){ ?>
                                        <?php if(!in_array($speciality_key,$user_selected_speciality)){ ?>
                                        <option class="speciality_class" value="<?php echo $speciality_key; ?>"><?php echo $speciality_val; ?></option>
                                        <?php } ?>
										<?php } ?>
                                        </select>
                                        
                                    </div>
                                    <div>
                                    </div>
                                </div>
                                
                                <div id="popup_content" class="specialityPopup_column" style="max-height:120px; overflow-y:scroll;">
                                    <h2 class="popup_heading" style="font-weight: bold;">Please select your Conditions Treated</h2>
                                    <div class="popup_subheading">You may select more than one option below.</div>
                                    <div class="total_fld" id="adv_condition_id">
                                        <?php /*foreach($user_condition as $condition_key => $condition_val){ ?>
                                        <p><input type="checkbox" name="user_selected_condition[]" value="<?php echo $condition_key; ?>" <?php if(in_array($condition_key,$user_selected_condition)) echo 'checked="checked"'; ?> class="condition_class"/> <?php echo $condition_val; ?></p>
                                        <?php }*/ ?>
                                    </div>
                                    <div>
                                    </div>
                                </div>
                                
                                <div id="popup_content" class="specialityPopup_column" style="max-height:120px; overflow-y:scroll;">
                                    <h2 class="popup_heading" style="font-weight: bold;">Please select your Procedures</h2>
                                    <div class="popup_subheading">You may select more than one option below.</div>
                                    <div class="total_fld" id="adv_procedure_id">
                                        <?php /*foreach($user_procedure as $procedure_key => $procedure_val){ ?>
                                        <p><input type="checkbox" name="user_selected_procedure[]" value="<?php echo $procedure_key; ?>" <?php if(in_array($procedure_key,$user_selected_procedure)) echo 'checked="checked"'; ?> class="procedure_class"/> <?php echo $procedure_val; ?></p>
                                        <?php }*/ ?>
                                    </div>
                                    <div>
                                    </div>
                                </div>
                                <div style="text-align: center;">
                                <input type="button" value="Save" class="registbt speciality_save_button"> <input type="button" value="Cancel" class="registbt cancel_button_scp">
                                </div>
                        </div>

                        <div class="fld_area">
                                <div class="fld_name fld_name_hight">Conditions Treated</div>
                                <input type="hidden" id="hidden_condition" value="<?php echo implode(",",$user_selected_condition); ?>" />
                                <div class="name_fld">
                                    <div class="insurancePlanList clearfix" id="condition_html">
                                        <?php foreach($user_condition as $condition_key => $condition_val){ ?>
                                        <?php if(in_array($condition_key,$user_selected_condition)): ?>
                                        <div class="insurancePlan" id="condition_html_id_<?php echo $condition_key; ?>"><span><?php echo $condition_val; ?></span><a class="removePlan hgIconX" title="Delete" onclick="conditionRemove('<?php echo $condition_key; ?>');"></a></div>
                                        <?php
                                        endif;
                                        }
                                        ?>
                                    </div>
                                    <!--<div><a href="javascript:void(0);" class="conditionPopup" onclick="conditionChecked();">Add condition</a></div>-->
                                </div>
                                <div class="clear"></div>
                            </div>
                            
                            <div id="conditionPopup"> 
    							<div class="close"></div>
                                <span class="ecs_tooltip">Press Esc to close <span class="arrow"></span></span>
                                <div id="popup_content" style="height:250px; overflow-y:scroll;">
                                    <h2 class="popup_heading">Please select your condition</h2>
                                    <div class="popup_subheading">You may select more than one option below.</div>
                                    <div class="total_fld">
                                        <?php /*foreach($user_condition as $condition_key => $condition_val){ ?>
                                        <p><input type="checkbox" name="user_selected_condition[]" value="<?php echo $condition_key; ?>" <?php if(in_array($condition_key,$user_selected_condition)) echo 'checked="checked"'; ?> class="condition_class"/> <?php echo $condition_val; ?></p>
                                        <?php }*/ ?>
                                    </div>
                                    <div>
                                        <input type="button" value="Save" class="registbt condition_save_button"> <input type="button" value="Cancel" class="registbt cancel_button">
                                    </div>
                                </div> 
                        </div>
                        
                        <div class="fld_area">
                                <div class="fld_name fld_name_hight">Procedures Performed</div>
                                <input type="hidden" id="hidden_procedure" value="<?php echo implode(",",$user_selected_procedure); ?>" />
                                <div class="name_fld">
                                    <div class="insurancePlanList clearfix" id="procedure_html">
                                        <?php foreach($user_procedure as $procedure_key => $procedure_val){ ?>
                                        <?php if(in_array($procedure_key,$user_selected_procedure)): ?>
                                        <div class="insurancePlan" id="procedure_html_id_<?php echo $procedure_key; ?>"><span><?php echo $procedure_val; ?></span><a class="removePlan hgIconX" title="Delete" onclick="procedureRemove('<?php echo $procedure_key; ?>');"></a></div>
                                        <?php
                                        endif;
                                        }
                                        ?>
                                    </div>
                                    <!--<div><a href="javascript:void(0);" class="procedurePopup" onclick="procedureChecked();">Add procedure</a></div>-->
                                </div>
                                <div class="clear"></div>
                            </div>
                            
                            <div id="procedurePopup"> 
    							<div class="close"></div>
                                <span class="ecs_tooltip">Press Esc to close <span class="arrow"></span></span>
                                <div id="popup_content" style="height:250px; overflow-y:scroll;">
                                    <h2 class="popup_heading">Please select your procedure</h2>
                                    <div class="popup_subheading">You may select more than one option below.</div>
                                    <div class="total_fld">
                                        <?php /*foreach($user_procedure as $procedure_key => $procedure_val){ ?>
                                        <p><input type="checkbox" name="user_selected_procedure[]" value="<?php echo $procedure_key; ?>" <?php if(in_array($procedure_key,$user_selected_procedure)) echo 'checked="checked"'; ?> class="procedure_class"/> <?php echo $procedure_val; ?></p>
                                        <?php }*/ ?>
                                    </div>
                                    <div>
                                        <input type="button" value="Save" class="registbt procedure_save_button"> <input type="button" value="Cancel" class="registbt cancel_button">
                                    </div>
                                </div> 
                        </div>
                        
                        </div>
                    </div>
                    <div id="tab5" class="tabContent">
                    	<div class="profilepage_contain">
      	<div class="doctorprofile_header">
        	<div class="profileLeftColumn">
           		 <span class="photos_new">
                 <!--<img src="<?php echo Yii::app()->request->baseUrl; ?>/assets/images/dr_bigpicx.jpg" alt="" style="height:280px; width:180px;">-->
                 <?php
				//if($model->image){
				$filePath = 'assets/upload/doctor/'.$model->id."/".$model->image;
				if(@file_get_contents($filePath, 0, NULL, 0, 1)){ 
				?>
                    <a href=""><?php echo CHtml::image(Yii::app()->request->baseUrl.'/assets/upload/doctor/'.$model->id."/".$model->image,"image",array('width'=>180/*,'height'=>280*/)); ?></a>
                <?php }else{ ?>
                    <?php echo CHtml::image(Yii::app()->request->baseUrl.'/assets/images/avatar.png',"image",array("width"=>200)); ?>
                <?php } ?>
                 </span>
            </div>
            <div class="infoColumn_new">
            	<div class="topRow">
                	<div class="nameDiv">
                   		 <h2><?php echo $model->title." ".$model->first_name." ".$model->last_name; ?></h2>
                         <p><?php echo $model->degree; ?> <br><?php //echo ($model->speciality)?$user_speciality[$model->speciality]:""; ?><?php echo isset($user_selected_speciality[0])?$user_speciality[$user_selected_speciality[0]]:""; ?></p>
                    </div>
                    <div class="mapColumn_new">
                        <div class="photos_new">
                        <!--<img src="<?php echo Yii::app()->request->baseUrl; ?>/assets/images/video.jpg" alt="" >-->
                        
                        <?php
                        if($data_video){
                        for($i=0;$i<count($data_video);$i++){
                            if($data_video[$i]['default_status'] == 1){
                        ?>
                        <?php
                        echo $data_video[$i]['embeded_code'];
                        ?>
                        <?php
                            }
                         }
                        }
                        ?>
                        
                        </div>
                     </div>
                </div>
                
            </div>
            
            <div class="infoColumn_newII">
            	<div>
                    	
                        <div class="locations"><h3>Practice Name</h3> <?php echo isset( $user_address[0]['practice_affiliation'] ) ? $user_address[0]['practice_affiliation'] : ''; ?></div>
                </div>
                <div>
                		<div class="locations"> 
                       		 <h3>Specialties</h3>
                             <!--<a href=""><?php //echo ($model->speciality)?$user_speciality[$model->speciality]:""; ?></a>-->
                            <?php 
		                     $speciality_count = 0;
		                     $speciality = "";
		                     foreach($user_speciality as $speciality_key => $speciality_val){ ?>
		                    <?php
		                    	if(in_array($speciality_key,$user_selected_speciality)): 
		                    ?>
		                    <span>
		                    	<?php 
		                    		if( $speciality_count == 0 ) {
										$speciality = $speciality_val; 
										$speciality_count ++;
									} else {
										$speciality = $speciality. " ," .$speciality_val;
									}
									
								?></span>
		                    <?php
		                    		endif;
		                    }
		                    echo $speciality;
		                    ?>
                        </div>
                        
                        <div class="locations">
		                	<?php if( count( $user_selected_insurance_plan_inc_name ) > 0 ) { ?> 
		                     		<h3 style="padding: 5px 0;" >Insurance Accepted</h3>
		                     <?php   
		                     		foreach( $user_selected_insurance_plan_inc_name as $insurance_plan_key => $insurance_plan_val ){ 
										//if( count( $insurance_plan_val ) > 0 ) {
								?>
										<div style="width:100%; float: left; padding: 5px 0;" >
		                     			<?php 
		                     				$insurance_plan_name = Insurance::model()->getInsuranceNameById($insurance_plan_key);
		                     				echo '<span style="font-weight: bold; float: left; width:auto; padding-right: 10px; ">'. $insurance_plan_name .'</span>'; 
		                     				if ( count( $insurance_plan_val ) > 0 ) {
												$plan_count = 0;
												$plan_name_str = "";
												foreach( $insurance_plan_val as $plan_key=>$planVal) {
						                    		if( $plan_count == 0 ) {
														$plan_name_str = $planVal; 
														$plan_count ++;
													} else {
														$plan_name_str = $plan_name_str. " ," .$planVal;
													}
		                    				}
		                    ?>
		                    					
		                    <?php 				
		                    						echo '<span style="font-weight: bold;" >: </span>'. $plan_name_str;
		                    ?>
		                    					
		                    <?php 					
		                    				}
		                    			//}
		                    ?>
		                    			</div>
		                    <?php 			
		                    		}
		                    	}	
		                    ?>
		                </div>
                        
                        <div class="locations"> 
                         	<h3>Professional Statement</h3>
                            <p><?php echo $model->comments; ?></p>
                        </div>
                </div>
            </div>
        </div>
        <div class="doctorprofile_appoint">
        <div class="leftpanel_new" <?php if(!$data_video){ echo 'style="width:100%;"'; } ?>>
        	
            <div style="width:100%; height:215px; border:1px solid #ccc;" id="profile_map">
        		
                	
            </div>
           <div class="shared-social-header-text">Share <?php echo $model->title." ".$model->first_name." ".$model->last_name; ?>, <?php echo $model->degree; ?>, <?php //echo ($model->speciality)?$user_speciality[$model->speciality]:""; ?><?php echo isset($user_selected_speciality[0])?$user_speciality[$user_selected_speciality[0]]:""; ?> with your network:<br>
				<img src="<?php echo Yii::app()->request->baseUrl; ?>/assets/images/share_icon.png" alt="">
            </div>

        	</div>
            
        </div>
        <div class="doctorprofile_header">
        	<div class="detailsColumn1">
            	<div style="float:left; width:45%; margin-right:4%;">
                    <h3 class="detailsHeader" style="padding-bottom: 10px;">Details for <span><?php echo $model->title." ".$model->first_name." ".$model->last_name; ?>, <?php echo $model->degree; ?>, <?php //echo ($model->speciality)?$user_speciality[$model->speciality]:""; ?><?php echo isset($user_selected_speciality[0])?$user_speciality[$user_selected_speciality[0]]:""; ?></span></h3>
                    <div class="link-column">
                        <h2>Education</h2>
                        <p><?php echo $model->medical_school; ?> <?php echo ($model->medical_school_year)?'<strong style="font-weight:bold;">Graduation Year </strong>'.$model->medical_school_year:''; ?></p>
                    </div>
                    <div class="link-column">
                        <h2>Residency Training</h2>
                        <p><?php echo $model->residency_training; ?> <?php echo ($model->residency_training_year)?'<strong style="font-weight:bold;">Completion Year </strong>'.$model->residency_training_year:''; ?></p>
                    </div>
                    <div class="link-column">
                        <h2>Languages Spoken</h2>
                        <p>
						<?php //echo $model->languages_spoken; ?>
                        <?php foreach($user_language as $lang_key => $lang_val){ ?>
						<?php if(in_array($lang_key,$user_selected_language)): ?>
                        <span><?php echo $lang_val; ?></span>
                        <?php
                        endif;
                        }
                        ?>
                        </p>
                    </div>
                    <?php if($model->board_certifications) { ?>
                    <div class="link-column">
                        <h2>Board Certifications</h2>
                        <p><?php echo $model->board_certifications; ?></p>
                    </div>
                    <?php } ?>
                    <?php if($model->hospital_affiliations) { ?>
                    <div class="link-column">
                        <h2>Professional Memberships</h2>
                        <p><?php echo $model->hospital_affiliations; ?> </p>
                    </div>
                    <?php } ?>
                    <?php if($model->awards_publications) { ?>
                    <div class="link-column">
                        <h2>Awards and Publications</h2>
                        <p>
							<?php echo $model->awards_publications; ?>
                        </p>
                    </div>
                    <?php } ?>
                </div>
                <div style="float:left; width:50%;">
                	<h3 class="detailsHeader" style="padding-bottom:16px;">Conditions Treated</h3>
                    <div class="doctor_condition">
                    	<ul>
                     	   <?php foreach($user_condition as $condition_key => $condition_val){ ?>
							<?php if(in_array($condition_key,$user_selected_condition)): ?>
                                 <li><a href="javascript:void(0);"><?php echo $condition_val; ?></a></li>
                            <?php
                            /*else:
                            ?>
                                <li class="inactivetag"><?php echo $condition_val; ?></li>
                            <?php
                            */endif;
                            }
                            ?>
                        </ul>
                    </div>
                </div>
                <div style="float:left; width:50%;">
                	<h3 class="detailsHeader" style="padding-bottom:16px;">Procedures</h3>
                    <div class="doctor_condition">
                    	<ul>
                     	   <?php foreach($user_procedure as $procedure_key => $procedure_val){ ?>
							<?php if(in_array($procedure_key,$user_selected_procedure)): ?>
                                 <li><a href="javascript:void(0);"><?php echo $procedure_val; ?></a></li>
                            <?php
                            /*else:
                            ?>
                                <li class="inactivetag"><?php echo $procedure_val; ?></li>
                            <?php
                            */endif;
                            }
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
            
        </div>
      </div>
                    </div>
                    <div id="tab6" class="tabContent">
                    	<div class="dashboard_content1">
                                <div class="add_area">
                                    <!--<span class="add_new_btn"><a href="edit_my_address.html" class="grn_btn">Add new</a></span>-->
                                    <div class="clear"></div>
                                </div>
                                <div class="dashboardcont_leftbox2">
                                    <ul>
                                        <li class="heading">
                                         <span class="vid">Video</span> 
                                         <span class="att txt_align">Status</span> 
                                         <span class="att txt_align">Action</span>
                                        </li>
                                        <?php
										if($data_video):
										for($i=0;$i<count($data_video);$i++){
										?>
                                        <li>
                                         <span class="vid">
                                         	<!--<iframe width="320" height="180" src="//www.youtube.com/embed/uPhZXwed07c?rel=0" frameborder="0" allowfullscreen></iframe>-->
                                            <?php echo $data_video[$i]['embeded_code']; ?>
                                         </span> 
                                         <span class="att txt_align"><?php echo ($data_video[$i]['default_status']==1)?'Active':'Inactive'; ?></span> 
                                         <span class="att txt_align">
                                            <?php echo CHtml::link('<img src="'.Yii::app()->request->baseUrl.'/assets/images/edit_icon.png" alt="" />', $this->createAbsoluteUrl('doctor/editVideo/'.$data_video[$i]['id']),array("class"=>"videoPopup",'onclick'=>'video('.$data_video[$i]['id'].');','title'=>'Edit')); ?>
                                            <a href="javascript:void(0);" onclick="videoRemove('<?php echo $data_video[$i]['id']; ?>');"><img src="<?php echo Yii::app()->request->baseUrl; ?>/assets/images/delete_icon.png" alt="" title="Delete"/></a>
                                         </span>
                                        </li>
                                        <?php
										 }
										 else:
						 					echo '<li style="text-align: center;">No Video Yet.</li>';
										endif;
										?>
                                       </ul>
                                       <?php
										if(count($data_video)<2):
										?>
									   <?php echo CHtml::link('Add Video', $this->createAbsoluteUrl('doctor/editVideo/'),array('class'=>'videoPopup','onclick'=>'video(\'\');')); ?>
                                       <?php
										 endif;
									   ?>
                                </div>
                                
                                <div id="videoPopup">
                                    <div class="close"></div>
                                    <div id="addVideoPopup"></div>
                                </div>
                            </div>
                    </div>
                    
                                        <div id="tab7" class="tabContent">
                    	<?php $form=$this->beginWidget('CActiveForm', array(
							'id'=>'reset_password',
						)); ?>
							<div class="dashboardcont_leftbox">
								<?php if(Yii::app()->user->hasFlash('reset_password')): ?>
									<span class="flash-success">
										<?php echo Yii::app()->user->getFlash('reset_password'); ?>
									</span>
								<?php endif; ?>
								 <h1>Reset Password</h1>
								 <div class="box_content">
									<div class="fld_area">
									   <div class="fld_name">Old Password</div>
									   <div class="name_fld">
									   <input id="old_password" class="fld_class" type="password" name="old_password" placeholder="Old Password" autocomplete="off" size="32" value="<?php if(isset($_REQUEST['old_password'])) echo $_REQUEST['old_password']; ?>">
									   <div class="errorMessage"><?php if(isset($error['old_password'])) echo $error['old_password']; ?></div>
									   </div>
									   <div class="clear"></div>             
									</div>
									<div class="fld_area">
									   <div class="fld_name fld_name_hight">New Password</div>
									   <div class="name_fld">
									   <input id="new_password" class="fld_class" type="password" name="new_password" placeholder="New Password" autocomplete="off" size="32" value="<?php if(isset($_REQUEST['new_password'])) echo $_REQUEST['new_password']; ?>">
									   <div class="errorMessage"><?php if(isset($error['new_password'])) echo $error['new_password']; ?></div>
									   </div>
									   <div class="clear"></div>
									</div>
									<div class="fld_area">
									   <div class="fld_name fld_name_hight">Confirm Password</div>
									   <div class="name_fld">
									   <input id="confirm_password" class="fld_class" type="password" name="confirm_password" placeholder="Confirm Password" autocomplete="off" size="32" value="<?php if(isset($_REQUEST['confirm_password'])) echo $_REQUEST['confirm_password']; ?>">
									   <div class="errorMessage"><?php if(isset($error['confirm_password'])) echo $error['confirm_password']; ?></div>
									   </div>
									   <div class="clear"></div>
									</div>
								</div>
							</div>
							<div>
								<span>								
								<input class="registbt" type="button" value="Update" name="yt0" onclick="return passwordSubmit();">
								<?php echo CHtml::link('Cancel', $this->createAbsoluteUrl('doctor/index'),array('class'=>'registbt')); ?>
								</span>
							</div>
						<?php $this->endWidget(); ?>
                    </div>
                    
                </div>
                <script type="text/javascript">// < ![CDATA[
// < ![CDATA[
// < ![CDATA[
function show1()
{ document.getElementById('div1').style.display ='none'; } 
function show2()
{ document.getElementById('div1').style.display = 'block'; }
// ]]>

</script>	

                <script type="text/javascript">
					  $(".tabContent").hide(); 
					  $("ul.tabs li:first").addClass("active").show(); 
					  $(".tabContent:first").show(); 
					 
					  $("ul.tabs li").click(function () {
						$("ul.tabs li").removeClass("active"); 
						$(this).addClass("active"); 
						$(".tabContent").hide(); 
						var activeTab = $(this).find("a").attr("href"); 
						$(activeTab).fadeIn(); 
						return false;
					  });
			    </script>      
            </div> 
      	</div>
    </div>
    </div>


<div class="loader"></div>
<div id="backgroundPopup"></div>

<div class="loader"></div>
<div id="backgroundPopup"></div>
<!--popup end-->

<!--popup start-->
<div id="addPopup"> 
    	<div class="close"></div>
       	<div  id="addPopupAddress"></div>
</div>
<div class="loader"></div>
<div id="backgroundPopup"></div>
<!--popup end-->

<!--languge popup start-->
<div id="langPopup"> 
    	
        <div class="close"></div>
       	<span class="ecs_tooltip">Press Esc to close <span class="arrow"></span></span>
		<div id="popup_content">
        	<h2 class="popup_heading">Please select your language</h2>
            <div class="popup_subheading">You may select more than one option below.</div>
			<div class="total_fld">
            	<?php foreach($user_language as $lang_key => $lang_val){ ?>
            	<p><input type="checkbox" name="user_selected_language[]" value="<?php echo $lang_key; ?>" <?php if(in_array($lang_key,$user_selected_language)) echo 'checked="checked"'; ?> class="language_class"/> <?php echo $lang_val; ?></p>
                <?php } ?>
            </div>
            <div>
            	<input type="button" value="Save" class="registbt language_save_button"> <input type="button" value="Cancel" class="registbt cancel_button">
            </div>
        </div> 
</div>
<div class="loader"></div>
<div id="backgroundPopup"></div>
<!--languge popup end-->


<div class="loader"></div>
<div id="backgroundPopup"></div>
<!--plan1 popup end-->

<div class="loader"></div>
<div id="backgroundPopup"></div>
<!--plan2 popup end-->

<div class="loader"></div>
<div id="backgroundPopup"></div>
<!--plan3 popup end-->

<!--plan4 popup start-->
<div id="plan4Popup"> 
    	 <div class="close"></div>
       	<span class="ecs_tooltip">Press Esc to close<span class="arrow"></span></span>
		<div id="popup_content">
        	<h2 class="popup_heading">Top 20 Nationwide Carriers</h2>
            <div class="popup_subheading">You may select more than one option below.</div>
			<div class="total_fld">
            	<?php foreach($user_insurance as $user_insurance_key => $user_insurance_val){ ?>
            	<p><input type="checkbox" name="user_selected_language[]" value="<?php echo $user_insurance_key; ?>" <?php if(in_array($user_insurance_key,$user_selected_insurance)) echo 'checked="checked" disabled="disabled"'; ?> class="insurance_class"/> <?php echo $user_insurance_val; ?></p>
                <?php } ?>
            </div>
            <div>
            	<input type="button" value="Save" class="registbt insurance_save_button"> <input type="button" value="Cancel" class="registbt cancel_button">
            </div>
        </div> 
</div>
<div class="loader"></div>
<div id="backgroundPopup"></div>
<!--plan4 popup end-->

<script language="javascript">

$('#existItemText').click(function(event) {
	$("#existItemText").hide();
	$("#existItem").show();
	$("#existItem").focus();
	event.stopPropagation();
});
$('html').click(function() {
	$("#existItem").hide();
	$("#existItemText").show();
});

$('#existItemText2').click(function(event) {
	$("#existItemText2").hide();
	$("#existItem2").show();
	$("#existItem2").focus();
	event.stopPropagation();
});
$('html').click(function() {
	$("#existItem2").hide();
	$("#existItemText2").show();
});
$('#existItemText3').click(function(event) {
	$("#existItemText3").hide();
	$("#existItem3").show();
	$("#existItem3").focus();
	event.stopPropagation();
});
$('html').click(function() {
	$("#existItem3").hide();
	$("#existItemText3").show();
});


</script>
<script>
$(function() {
	$( "#Doctor_birth_date" ).datepicker({
		changeMonth: true,changeYear: true, yearRange : '-80:-20',dateFormat: 'yy-mm-dd'
	});
	//unset_cookie('tab_visible_id', '');
	//alert(get_cookie('tab_visible_id'));
	if(get_cookie('tab_visible_id') == 'tab2'){
		 $(".tabContent").hide(); 
		 $('ul.tabs li').removeClass('active');
		 $("ul.tabs li").eq(1).addClass("active").show(); 
		 $(".tabContent").eq(1).show(); 
		 
		 $("ul.tabs li").click(function () {
			$("ul.tabs li").removeClass("active"); 
			$(this).addClass("active"); 
			$(".tabContent").hide(); 
			var activeTab = $(this).find("a").attr("href"); 
			$(activeTab).fadeIn(); 
			return false;
		 });
		unset_cookie('tab_visible_id', '');
	}
	if(get_cookie('tab_visible_id') == 'tab6'){
		 $(".tabContent").hide(); 
		 $('ul.tabs li').removeClass('active');
		 $("ul.tabs li").eq(4).addClass("active").show(); 
		 $(".tabContent").eq(4).show(); 
		 
		 $("ul.tabs li").click(function () {
			$("ul.tabs li").removeClass("active"); 
			$(this).addClass("active"); 
			$(".tabContent").hide(); 
			var activeTab = $(this).find("a").attr("href"); 
			$(activeTab).fadeIn(); 
			return false;
		 });
		unset_cookie('tab_visible_id', '');
	}
});
function languageChecked(){
	var hidden_language = $('#hidden_language').val().split(',');
	$('.language_class').attr('checked', false);
	$(".language_class").each(function(){
		if($.inArray($(this).val(), hidden_language)!==-1){
			$(this).attr('checked', true);
		}
	});
}
function languageUpdate(){
	var hidval = [];
	var hidval_lang = [];
	$(".language_class").each(function(){
		if($(this).is(":checked")){
			var ind_lang_id = $(this).val();
			hidval.push(ind_lang_id);
			var ind_lang = $(this).parent().text();
			hidval_lang.push('<div class="insurancePlan" id="language_html_id_'+ind_lang_id+'"><span>'+ind_lang+'</span><a class="removePlan hgIconX" title="Delete" onclick="langRemove(\''+ind_lang_id+'\');"></a></div>');
		}
	});
	var hidstr = hidval.join(',');
	var hidval_lang_str = hidval_lang.join('');
	$.post("<?php echo Yii::app()->request->baseUrl; ?>/doctor/languageAjaxUpd", {language_id:hidstr,hid_id:$('#hidden_id').val()},function(response) {
		$('#language_html').html(hidval_lang_str);
		$('#hidden_language').val(hidstr);
	});
}

function langRemove(id){
	$.post("<?php echo Yii::app()->request->baseUrl; ?>/doctor/languageAjaxRemove", {language_id:id,hid_id:$('#hidden_id').val()},function(response) {
		$('#language_html_id_'+id).remove();
		var hidden_language_val = $('#hidden_language').val();
		var hidden_language = hidden_language_val.split(',');
		var index = hidden_language.indexOf(id);//alert(hidden_language);alert(index);
		if(index!=-1){
			hidden_language.splice(index, 1);
		}
		$('#hidden_language').val(hidden_language.join(','));
	});
}

function insuranceChecked(){
	var hidden_insurance = $('#hidden_insurance').val().split(',');
	$('.insurance_class').attr('checked', false);
	$('.insurance_class').attr('disabled', false);
	$(".insurance_class").each(function(){
		if($.inArray($(this).val(), hidden_insurance)!==-1){
			$(this).attr('checked', true);
			$(this).attr('disabled', true);
		}
	});
}

function insuranceUpdate(){
	var hidval = [];
	var hidval_lang = [];
	$(".insurance_class").each(function(){
		if($(this).is(":checked") && !$(this).is(":disabled")){
			var ind_lang_id = $(this).val();
			hidval.push(ind_lang_id);
			var ind_lang = $(this).parent().text();
			//hidval_lang.push('<div class="insurancePlan" id="language_html_id_'+ind_lang_id+'"><span>'+ind_lang+'</span><a class="removePlan hgIconX" title="Delete" onclick="langRemove(\''+ind_lang_id+'\');"></a></div>');
			hidval_lang.push('<div class="main_insurance_list" id="main_insurance_list_id_'+ind_lang_id+'"><div class="mainBucketListItemRow existingItem"><div class="errorContainer"><p style="display: none;" class="errorLabel"></p></div><label>'+ind_lang+'<div class="addPlan"><a href="javascript:void(0);" class="choosePlan plan2Popup" id="AETNA">Add '+ind_lang+' Plan</a></div></label><a class="removeItem hgIconX" title="Delete"></a></div><input type="hidden" class="hidden_plan" id="hidden_plan_'+ind_lang+'" value="" /><div class="insurancePlanList clearfix" id="plan_html_'+ind_lang+'"></div></div>');
		}
	});
	var hidstr = hidval.join(',');
	var hidval_lang_str = hidval_lang.join('');//alert(hidstr);alert(hidval_lang_str);
	$.post("<?php echo Yii::app()->request->baseUrl; ?>/doctor/insuranceAjaxUpd", {insurance_id:hidstr,hid_id:$('#hidden_id').val()},function(response) {
		//$('#language_html').html(hidval_lang_str);
		$('.insurance_area').append(hidval_lang_str);
		if($('#hidden_insurance').val()!="")
			$('#hidden_insurance').val($('#hidden_insurance').val()+","+hidstr);
		else
			$('#hidden_insurance').val(hidstr);
	});
}

function insuranceRemove(id){
	$.post("<?php echo Yii::app()->request->baseUrl; ?>/doctor/insuranceAjaxRemove", {insurance_id:id,hid_id:$('#hidden_id').val()},function(response) {
		$('#main_insurance_list_id_'+id).remove();
		var hidden_insurance_val = $('#hidden_insurance').val();
		var hidden_insurance = hidden_insurance_val.split(',');
		var index = hidden_insurance.indexOf(id);//alert(hidden_insurance);alert(index);
		if(index!=-1){
			hidden_insurance.splice(index, 1);
		}
		$('#hidden_insurance').val(hidden_insurance.join(','));
	});
}

function planChecked(id){
	var hidden_plan = $('#hidden_plan_'+id).val().split(',');
	$('.plan_class_'+id).attr('checked', false);
	$(".plan_class_"+id).each(function(){
		if($.inArray($(this).val(), hidden_plan)!==-1){
			$(this).attr('checked', true);
		}
	});
}

function planUpdate(id){
	var hid_id_id = $(".hidden_plan").eq(id).attr('id');
	var hid_id_id_arr = hid_id_id.split('_');
	var arr_ind = hid_id_id_arr.length-1;
	var hid_id_id_no = hid_id_id_arr[arr_ind];
	
	var hidval = [];
	var hidval_lang = [];
	$(".plan_class_"+hid_id_id_no).each(function(){
		if($(this).is(":checked")){
			var ind_lang_id = $(this).val();
			hidval.push(ind_lang_id);
			var ind_lang = $(this).parent().text();
			hidval_lang.push('<div class="insurancePlan" id="insurancePlan_del_'+ind_lang_id+'"><span>'+ind_lang+'</span><a class="removePlan hgIconX" title="Delete" onclick="planRemove(\''+ind_lang_id+'\');"></a></div>');
		}
	});
	var hidstr = hidval.join(',');
	var hidval_lang_str = hidval_lang.join('');
	$.post("<?php echo Yii::app()->request->baseUrl; ?>/doctor/planAjaxUpd", {insurance_id:hid_id_id_no,plan_id:hidstr,hid_id:$('#hidden_id').val()},function(response) {
		$('#plan_html_'+hid_id_id_no).html(hidval_lang_str);
		$('#hidden_plan_'+hid_id_id_no).val(hidstr);
	});
}

function planRemove(id){
	$.post("<?php echo Yii::app()->request->baseUrl; ?>/doctor/planAjaxRemove", {plan_id:id,hid_id:$('#hidden_id').val()},function(response) {
		var ins_id = $('#insurancePlan_del_'+id).parent().siblings('input.hidden_plan').eq(0).attr('id');
		
		$('#insurancePlan_del_'+id).remove();
		var hidden_language_val = $('#'+ins_id).val();
		var hidden_language = hidden_language_val.split(',');
		var index = hidden_language.indexOf(id);//alert(hidden_language);alert(index);
		if(index!=-1){
			hidden_language.splice(index, 1);
		}
		$('#'+ins_id).val(hidden_language.join(','));
	});
}
function address(id){
	$.post("<?php echo $this->createAbsoluteUrl('doctor/editAddressAjax/'); ?>", {id:id,type:"ajax"},function(response) {
		$('#addPopupAddress').html(response);
	});
}
function addressSubmit(id){
	var form_ind = $("form").index($("form#edit_address"));
	var formData = new FormData($('form')[form_ind]);
	formData.append("id", id);
	formData.append("type", "submit");
	$.ajax({
		url:'<?php echo $this->createAbsoluteUrl('doctor/editAddressAjax/'); ?>',  
		type: 'POST',
		xhr: function() { 
		myXhr = $.ajaxSettings.xhr();
		if(myXhr.upload){}
		return myXhr;
		},
		success: function(response){
			if(response == "**##**"){
				set_cookie('tab_visible_id', 'tab2');
				location.reload();
			}else
				$('#addPopupAddress').html(response);
				$('#addPopupAddress input,#addPopupAddress textarea').keyup(function(){
						$(this).parent('div.name_fld').children('div.errorMessage').hide();
					});
		},
		data: formData,
		cache: false,
		contentType: false,
		processData: false
	});
}

function video(id){
	$.post("<?php echo $this->createAbsoluteUrl('doctor/editVideoAjax/'); ?>", {id:id,type:"ajax"},function(response) {
		//$('#videoPopup').html(response);
		$('#addVideoPopup').html(response);
	});
}
function videoSubmit(id){
	//var formno = $("form").length;alert(formno);
	var form_ind = $("form").index($("form#edit_video"));
	//var formData = new FormData(document.getElementById("edit_video"));
	var formData = new FormData($('form')[form_ind]);
	/*$.post("<?php echo $this->createAbsoluteUrl('doctor/editVideoAjax/'); ?>", {id:id,type:"submit",formData:formData},function(response) {
		$('#addPopup').html(response);
	});*/
	//alert(id);
	formData.append("id", id);
	formData.append("type", "submit");
	$.ajax({
		url:'<?php echo $this->createAbsoluteUrl('doctor/editVideoAjax/'); ?>',  
		type: 'POST',
		xhr: function() { 
		myXhr = $.ajaxSettings.xhr();
		if(myXhr.upload){}
		return myXhr;
		},
		success: function(response){
			if(response == "**##**"){
				set_cookie('tab_visible_id', 'tab6');
				location.reload();
			}else
				//$('#videoPopup').html(response);
				$('#addVideoPopup').html(response);
				$('#edit_video input,#edit_video textarea').keyup(function(){
						$(this).removeClass('error');
						$(this).parent('div.name_fld').children('div.errorMessage').hide();
					});
			//$('form')[form_ind].reset();
		},
		// error: errorHandler,
		data: formData,
		cache: false,
		contentType: false,
		processData: false
	});
}

function videoRemove(id){
	if(confirm("Are You Sure to Delete ?"))
	$.post("<?php echo $this->createAbsoluteUrl('doctor/removeVideoAjax/'); ?>", {id:id},function(response) {
		set_cookie('tab_visible_id', 'tab6');
		location.reload();
	});
}

function set_cookie(name, value){
	var today = new Date();
	today.setTime(today.getTime());
	var expires_date = new Date(today.getTime() + (1000 * 60 * 60 * 24 * 90));
	document.cookie = name + "=" + escape(value) + ";expires=" + expires_date.toGMTString();
}
function unset_cookie(name, value){
	var today = new Date();
	today.setTime(today.getTime());
	var expires_date = new Date(today.getTime() + (-(1000 * 60 * 60 * 24 * 90)));
	document.cookie = name + "=" + escape(value) + ";expires=" + expires_date.toGMTString();
}
function get_cookie(name){
	var a = null;
	if(document.cookie){
		var b = document.cookie.split((escape(name) + '='));
		if(b.length >= 2){
			var c = b[1].split(';');
			a = unescape(c[0]);
		}
	}
	return a;
}

function specialityChecked(){
	var hidden_speciality = $('#hidden_speciality').val().split(',');
	$('.speciality_class').attr('selected', false);
	$(".speciality_class").each(function(){
		if($.inArray($(this).val(), hidden_speciality)!==-1){
			//$(this).attr('checked', true);
			//$(this).parent().remove();
			$(this).remove();
		}
	});
	removeSCP();
}

function specialityConProUpdate(){
	specialityUpdate();
	conditionUpdate();
	procedureUpdate();
}
function removeSCP(){
	$('#adv_condition_id').html('');
	$('#adv_procedure_id').html('');
}
function specialityUpdate(){
	var hidval = [];
	var hidval_speciality = [];
	$(".speciality_class").each(function(){
		if($(this).is(":selected")){
			var ind_speciality_id = $(this).val();
			hidval.push(ind_speciality_id);
			//var ind_speciality = $(this).parent().text();
			var ind_speciality = $(this).text();
			hidval_speciality.push('<div class="insurancePlan" id="speciality_html_id_'+ind_speciality_id+'"><span>'+ind_speciality+'</span><a class="removePlan hgIconX" title="Delete" onclick="specialityRemove(\''+ind_speciality_id+'\');"></a></div>');
		}
	});
	var hidstr = hidval.join(',');
	var hidval_speciality_str = hidval_speciality.join('');
	$.post("<?php echo Yii::app()->request->baseUrl; ?>/doctor/specialityAjaxUpd", {speciality_id:hidstr,hid_id:$('#hidden_id').val()},function(response) {
		/*$('#speciality_html').html(hidval_speciality_str);*/
		$('#speciality_html').append(hidval_speciality_str);
		$('#hidden_speciality').val(hidstr);
	});
}


function specialityRemove(id){
	$.post("<?php echo Yii::app()->request->baseUrl; ?>/doctor/specialityAjaxRemove", {speciality_id:id,hid_id:$('#hidden_id').val()},function(response) {
		$('#speciality_html_id_'+id).remove();
		var hidden_speciality_val = $('#hidden_speciality').val();
		var hidden_speciality = hidden_speciality_val.split(',');
		var index = hidden_speciality.indexOf(id);//alert(hidden_speciality);alert(index);
		if(index!=-1){
			hidden_speciality.splice(index, 1);
		}
		$('#hidden_speciality').val(hidden_speciality.join(','));
		
		var response_arr = response.split('***###***');
		if(response_arr[0] != ''){
			var response_a = response_arr[0].split(',');
			$.each(response_a, function (i, ob) {
				$('#condition_html_id_'+ob).remove();
			});
		}
		if(response_arr[1] != ''){
			var response_b = response_arr[1].split(',');
			$.each(response_b, function (i, ob) {
				$('#procedure_html_id_'+ob).remove();
			});
		}
	});
}

function conditionChecked(){
	var hidden_condition = $('#hidden_condition').val().split(',');
	$('.condition_class').attr('checked', false);
	$(".condition_class").each(function(){
		if($.inArray($(this).val(), hidden_condition)!==-1){
			$(this).attr('checked', true);
		}
	});
}
function conditionUpdate(){
	var hidval = [];
	var hidval_condition = [];
	$(".condition_class").each(function(){
		if($(this).is(":checked")){
			var ind_condition_id = $(this).val();
			hidval.push(ind_condition_id);
			var ind_condition = $(this).parent().text();
			hidval_condition.push('<div class="insurancePlan" id="condition_html_id_'+ind_condition_id+'"><span>'+ind_condition+'</span><a class="removePlan hgIconX" title="Delete" onclick="conditionRemove(\''+ind_condition_id+'\');"></a></div>');
		}
	});
	var hidstr = hidval.join(',');
	var hidval_condition_str = hidval_condition.join('');
	$.post("<?php echo Yii::app()->request->baseUrl; ?>/doctor/conditionAjaxUpd", {condition_id:hidstr,hid_id:$('#hidden_id').val()},function(response) {
		$('#condition_html').append(hidval_condition_str);
	});
}

function conditionRemove(id){
	$.post("<?php echo Yii::app()->request->baseUrl; ?>/doctor/conditionAjaxRemove", {condition_id:id,hid_id:$('#hidden_id').val()},function(response) {
		$('#condition_html_id_'+id).remove();
		var hidden_condition_val = $('#hidden_condition').val();
		var hidden_condition = hidden_condition_val.split(',');
		var index = hidden_condition.indexOf(id);//alert(hidden_condition);alert(index);
		if(index!=-1){
			hidden_condition.splice(index, 1);
		}
		$('#hidden_condition').val(hidden_condition.join(','));
		
		if(response != ''){
			var response_b = response.split(',');
			$.each(response_b, function (i, ob) {
				$('#procedure_html_id_'+ob).remove();
			});
		}
	});
}

function procedureChecked(){
	var hidden_procedure = $('#hidden_procedure').val().split(',');
	$('.procedure_class').attr('checked', false);
	$(".procedure_class").each(function(){
		if($.inArray($(this).val(), hidden_procedure)!==-1){
			$(this).attr('checked', true);
		}
	});
}
function procedureUpdate(){
	var hidval = [];
	var hidval_procedure = [];
	$(".procedure_class").each(function(){
		if($(this).is(":checked")){
			var ind_procedure_id = $(this).val();
			hidval.push(ind_procedure_id);
			var ind_procedure = $(this).parent().text();
			hidval_procedure.push('<div class="insurancePlan" id="procedure_html_id_'+ind_procedure_id+'"><span>'+ind_procedure+'</span><a class="removePlan hgIconX" title="Delete" onclick="procedureRemove(\''+ind_procedure_id+'\');"></a></div>');
		}
	});
	var hidstr = hidval.join(',');
	var hidval_procedure_str = hidval_procedure.join('');
	$.post("<?php echo Yii::app()->request->baseUrl; ?>/doctor/procedureAjaxUpd", {procedure_id:hidstr,hid_id:$('#hidden_id').val()},function(response) {
		$('#procedure_html').append(hidval_procedure_str);
	});
}

function procedureRemove(id){
	$.post("<?php echo Yii::app()->request->baseUrl; ?>/doctor/procedureAjaxRemove", {procedure_id:id,hid_id:$('#hidden_id').val()},function(response) {
		$('#procedure_html_id_'+id).remove();
		var hidden_procedure_val = $('#hidden_procedure').val();
		var hidden_procedure = hidden_procedure_val.split(',');
		var index = hidden_procedure.indexOf(id);//alert(hidden_procedure);alert(index);
		if(index!=-1){
			hidden_procedure.splice(index, 1);
		}
		$('#hidden_procedure').val(hidden_procedure.join(','));
	});
}

function passwordSubmit(id){
	var form_ind = $("form").index($("form#reset_password"));
	var formData = new FormData($('form')[form_ind]);
	formData.append("id", id);
	formData.append("type", "submit");
        $.ajax({
		url:'<?php echo $this->createAbsoluteUrl('doctor/resetPasswordPanel'); ?>',  
		type: 'POST',
		xhr: function() { 
		myXhr = $.ajaxSettings.xhr();
		if(myXhr.upload){}
		return myXhr;
		},
		success: function(response){
			if(response){
				//set_cookie('tab_visible_id', 'tab2');
				//location.reload();
				$('#tab7').html(response);
			}else
				$('#addPopupAddress').html(response);
				$('input').keyup(function(){
						$(this).parent('div.name_fld').children('div.errorMessage').hide();
					});
		},
		data: formData,
		cache: false,
		contentType: false,
		processData: false
	});
}

</script>

<script>
function removeAddress(id){
	if(confirm('Are you sure ?'))
	$.post("<?php echo Yii::app()->request->baseUrl; ?>/doctor/addressAjaxRemove", {id:id},function(response) {
			set_cookie('tab_visible_id', 'tab2');
			location.reload();
		});
}
</script>
<script>
	function addslashes(string) {
		return string.replace(/\\/g, '\\\\').
			replace(/\u0008/g, '\\b').
			replace(/\t/g, '\\t').
			replace(/\n/g, '\\n').
			replace(/\f/g, '\\f').
			replace(/\r/g, '\\r').
			replace(/'/g, '\\\'').
			replace(/"/g, '\\"');
	}
	function initialize() {
		
		  var bounds = new google.maps.LatLngBounds ();
  		  var loc;
		  var mapOptions = {
		   zoom: 13,
		   mapTypeControl : false,//map change
		   streetViewControl: false,   
		   center: new google.maps.LatLng(<?php echo isset($user_address[0]['latitude'])&&($user_address[0]['latitude']!='')?$user_address[0]['latitude']:22.4871067; ?>,<?php echo isset($user_address[0]['longitude'])&&($user_address[0]['longitude']!='')?$user_address[0]['longitude']:88.3131307; ?>)  
		   };  
		  var map = new google.maps.Map(document.getElementById('profile_map'),mapOptions);//map change
		  
		 
		 <?php
		 if($user_address){
		 for($i=0;$i<count($user_address);$i++){
		 ?>
		 
		 var pinIcon_<?php echo $i+1; ?> = new google.maps.MarkerImage(   
		   "http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=<?php echo $i+1; ?>|ccc|000000",
		   new google.maps.Size(350, 350)
		  );
		 var pinIcon_over_<?php echo $i+1; ?> = new google.maps.MarkerImage(   
		   "http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=<?php echo $i+1; ?>|aec|000000",
		   new google.maps.Size(350, 350)
		  );
		  var user_location_lat_<?php echo $user_address[$i]['id']; ?> = <?php echo ($user_address[$i]['latitude']!='')?$user_address[$i]['latitude']:22.4871067; ?>;
		  var user_location_lng_<?php echo $user_address[$i]['id']; ?> = <?php echo ($user_address[$i]['longitude']!='')?$user_address[$i]['longitude']:88.3131307; ?>;
		  
		   loc = new google.maps.LatLng(user_location_lat_<?php echo $user_address[$i]['id']; ?>,user_location_lng_<?php echo $user_address[$i]['id']; ?>);
  		  
   
		  var marker_id_<?php echo $user_address[$i]['id']; ?> = new google.maps.Marker({
		  map: map,
		  streetViewControl: false,
		  icon:pinIcon_<?php echo $i+1; ?>,
		  title:"<?php //echo $user_address[$i]['address']; ?>",
		  position: new google.maps.LatLng(user_location_lat_<?php echo $user_address[$i]['id']; ?>,user_location_lng_<?php echo $user_address[$i]['id']; ?>),
		  draggable: false
		  }); 
		
		  var str_user_add = addslashes("<?php echo $user_address[$i]['address']; ?>");
		  var str_user_img = '';
		  <?php
		  $filePath = 'assets/upload/doctor/'.$model->id."/".$model->image;
		  if(@file_get_contents($filePath, 0, NULL, 0, 1)){ 
		  ?>
				 str_user_img = '<?php echo CHtml::image(Yii::app()->request->baseUrl.'/assets/upload/doctor/'.$model->id."/".$model->image,"image",array("width"=>80)); ?>';
		  <?php }else{ ?>
				 <?php if($model->gender=='M'){ ?>
				str_user_img = '<?php echo CHtml::image(Yii::app()->request->baseUrl.'/assets/images/avatar.png',"image",array("width"=>80)); ?>';
				<?php }else{ ?>
				str_user_img = '<?php echo CHtml::image(Yii::app()->request->baseUrl.'/assets/images/avtar_female.png',"image",array("width"=>80)); ?>';
				<?php } ?>
		  <?php } ?>
		  var str_user_booking = '<a href="<?php echo $this->createAbsoluteUrl('doctor/searchDoctorProfile/id/'.$model->id); ?>" target="_blank" class="mapbook">Book Online</a>';
		  var contentString_click_<?php echo $user_address[$i]['id']; ?> = '<div><div style="width:250px; overflow:hidden; border-radius:3px;"><span style="display: block; float: left; width:40%;">'+str_user_img+'</span><span class="profile_brief_account"><?php echo $model->title." ".$model->first_name." ".$model->last_name; ?>, <font style="font-family: cursive,Courier,monospace;"><?php echo $model->degree; ?></font><br><p><span style="font-size: 14px;"> <?php //echo ($model->speciality)?$user_speciality[$model->speciality]:""; ?><?php //echo isset($user_speciality[$doctor_list[$i]['ds_speciality_id']])?$user_speciality[$doctor_list[$i]['ds_speciality_id']]:''; ?>.</span><br><br>'+str_user_add+'</span></div></div>';
		  //var contentString_click = '';
		  var infowindow_content_<?php echo $user_address[$i]['id']; ?> = new google.maps.InfoWindow({
			 content: contentString_click_<?php echo $user_address[$i]['id']; ?>
		  }); 
		  google.maps.event.addListener(marker_id_<?php echo $user_address[$i]['id']; ?>, 'click', function() {
		  		infowindow_content_<?php echo $user_address[$i]['id']; ?>.open(map,marker_id_<?php echo $user_address[$i]['id']; ?>);
				marker_id_<?php echo $user_address[$i]['id']; ?>.setIcon(pinIcon_over_<?php echo $i+1; ?>);
		  });
		  google.maps.event.addListener(infowindow_content_<?php echo $user_address[$i]['id']; ?>,'closeclick',function(){
			   marker_id_<?php echo $user_address[$i]['id']; ?>.setIcon(pinIcon_<?php echo $i+1; ?>);
		  });
		  
		  google.maps.event.trigger( map, 'resize' );
		 /* bounds.extend(loc);
		  map.fitBounds (bounds);
   		  map.panToBounds(bounds);*/
		 /*----------------- --------------------*/
		 <?php
		 }
		 }
		 ?>
	 }
	 $(function() {
		 setTimeout('initialize()', 5000);
		 var script = document.createElement("script");
					script.type = "text/javascript";
		script.src = "https://maps.googleapis.com/maps/api/js?key=AIzaSyCFPo3KmF9lRvTl8mNsUW02gKKFTosnHhI&sensor=true&" +"callback=initialize"+'&libraries=places';
			document.body.appendChild(script);
	});
</script>
<script>
function readImage(file,id_obj) {
	var reader = new FileReader();
	var image  = new Image();
  
	reader.readAsDataURL(file);  
	reader.onload = function(_file) {
		image.src    = _file.target.result;              // url.createObjectURL(file);
		image.onload = function() {
          var w = this.width,
           h = this.height,
           t = file.type,                           // ext only: // file.type.split('/')[1],
           n = file.name,
           s = ~~(file.size/1024) +'KB';
          //$('#uploadPreview').append(w+'x'+h+' '+s+' '+t+' '+n+'<br>');
          var thumb = 0;

          if(w<100)
          {
           thumb = 1;
          }
          if(w>300)
          {
           thumb = 1;
          }

          if(h<100)
          {
           thumb = 1;
          }
          if(h>300)
          {
           thumb = 1;
          }

          if(thumb==1)
          {
           alert('please upload correct width (100 to 300)px and height (100 to 300)px image.');
           id_obj.focus();
           id_obj.val('');
           return false;
          }

         };
		image.onerror= function() {
			//alert('Invalid file type: '+ file.type);
			alert('File not an image !');
			id_obj.focus();
			id_obj.val('');
			return false;
		};      
	};
	
}
$(document).ready(function(){
	$("#Doctor_image").change(function (e) {
		if(this.disabled) return alert('File upload not supported!');
		var F = this.files;
		if(F && F[0]) for(var i=0; i<F.length; i++) readImage( F[i] ,$(this));
	});
});
function doctorImageDel(){
	var doctor_ind_image_hidden = $.trim($('#doctor_ind_image_hidden').val());
	if(doctor_ind_image_hidden != '')
	if(confirm('are you sure?'))
		$.post("<?php echo Yii::app()->request->baseUrl; ?>/doctor/doctorImageDel", {image_name:doctor_ind_image_hidden},function(response) {
			if(response == 'ok'){
				location.reload();
			}
		});
}


</script>