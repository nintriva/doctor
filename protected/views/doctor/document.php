<script src="<?php echo Yii::app()->getBaseUrl(true); ?>/assets/js/script.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->getBaseUrl(true); ?>/assets/js/jquery.prettyPhoto.js" type="text/javascript"></script>
<link href="<?php echo Yii::app()->getBaseUrl(true); ?>/assets/css/prettyPhoto.css" rel="stylesheet" type="text/css" />

<link href='<?php echo Yii::app()->getBaseUrl(true); ?>/assets/calender/fullcalendar/fullcalendar.css' rel='stylesheet' />
<link href='<?php echo Yii::app()->getBaseUrl(true); ?>/assets/calender/fullcalendar/fullcalendar.print.css' rel='stylesheet' media='print' />
<script src='<?php echo Yii::app()->getBaseUrl(true); ?>/assets/calender/fullcalendar/fullcalendar.min.js'></script>
<script src='<?php echo Yii::app()->getBaseUrl(true); ?>/assets/calender/fullcalendar/gcal.js'></script>
<script src='<?php echo Yii::app()->getBaseUrl(true); ?>/assets/calender/fullcalendar/moment.min.js'></script>
<script src='<?php echo Yii::app()->getBaseUrl(true); ?>/assets/js/dashboard_script.js'></script>
<link href='<?php echo Yii::app()->getBaseUrl(true); ?>/assets/css/modal_popup.css' rel='stylesheet' />
<link href='<?php echo Yii::app()->getBaseUrl(true); ?>/assets/css/style.css' rel='stylesheet' />
<style>
    a.cross{background:url(../assets/images/cross.png) no-repeat; display:block; width:19px; height:19px; float:left;}
    a.cross:hover{background:url(../assets/images/cross_hover.png) no-repeat;}
</style>
<?php

$this->breadcrumbs = array(
    'Documents',
);

?>
<div class="main">
    <div id="breadcrumb" class="fk-lbreadbcrumb newvd">
        <!--<span><a href="">Home</a></span> >  
        <span>Dashboard</span>--> 
        <?php
        $this->widget('zii.widgets.CBreadcrumbs', array(
            'links' => $this->breadcrumbs,
        ));
        ?>
    </div>
    <div class="dashboard_mainarea">
        <div class="leftmenu" id="left_id">
          
<?php $this->renderPartial('//layouts/navigation'); ?>
        </div>
        <div class="rightarea_dashboard" id="right_id">
            <div class="dashboard_content">
                <?php if( isset(Yii::app()->session['logged_user_type']) && (Yii::app()->session['logged_user_type'] == "patient" ) ) {?>
                <span class="profile_brief">
                    <?php echo $dataProvider->user_first_name; ?> <?php echo $dataProvider->user_last_name; ?> <br>
                </span><?php }?>
                <?php if( isset(Yii::app()->session['logged_user_type']) && (Yii::app()->session['logged_user_type'] == "doctor"|| Yii::app()->session['logged_user_type'] == "parcticeAdmin") ) {?>
                    <span class="profile_brief">
                    <?php echo $dataProvider->first_name; ?> <?php echo $dataProvider->last_name; ?> <br>
                    </span><?php }?>
                <span class="member_box">Member since <b style="font-weight:bold;"><?php echo date('M, Y', strtotime($dataProvider->date_created)); ?></b></span>
            </div>
        <div id="document_partial">

            <?php $this->renderPartial('document_view', array('document'=>$document,'pages'=>$pages)); ?>
</div>
        </div> 
    </div>
</div>


<div id="toPopup"> 
    <div class="close_view"></div> 
    <div id="popup_content">
        <h4>Comments</h4>
        <p id="popup_content_val"></p>
    </div>
</div>

<div id="popup" class="modal-box" style="height: 425px !important;
    width: 40%!important;    padding: 10px !important;">

    <header style="width: 90%;">
        <a href="#" class="js-modal-close close">×</a>
        <h3>Create New Document</h3>
    </header>
    <div class="modal-body">
        <div class="dashboardcont_leftbox">


                <?php $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'compose_document',
                    'enableAjaxValidation'=>true,
                    'enableClientValidation' => true,
                    'htmlOptions' => array('enctype' => 'multipart/form-data'),
                )); ?>
                <div class="box_content">

                    <div class="fld_area">
                        <label class="fld_name required ">Document Name:</label>
                        <div class="name_fld">
                            <input type="text" class="fld_class doccustom" id="contact_id" name="document_name" required>

                            <ul id="contact_list_id" style="display:none;"></ul>
                        </div>

                        <div class="clear"></div>
                    </div>
                    <div class="fld_area">
                        <label class="fld_name required">Label</label>
                        <div class="name_fld doccustom" style="margin-left: -4px;">
                            <div>
                           <?php if( isset(Yii::app()->session['logged_user_type']) && (Yii::app()->session['logged_user_type'] == "doctor" ||Yii::app()->session['logged_user_type'] == "parcticeAdmin") ) {?>
                           <select name="label" style="border: 1px solid #A6AFAF;" name="label">

                            <option disabled selected>Please Select</option>
                            <option>Advanced Directive</option>
                            <option >Authorization</option>
                            <option >Cardiopulmonary Diagnosis</option>
                            <option >Clinical Summary</option>
                            <option >Consent for Treatment</option>
                            <option >Consultation</option>
                            <option >CT Report</option>
                            <option >Demographics</option>
                            <option >Diagnostic Ultrasound</option>
                            <option >Disability Info</option>
                            <option >Discharge Summary</option>
                            <option >Dismissals</option>
                            <option >Drivers License</option>
                            <option >ECHO</option>
                            <option >EEG-EMG</option>
                            <option >EKG</option>
                            <option >Eligibility</option>
                            <option>Emergency Department</option>
                            <option>Explanation of Benefits</option>
                            <option>History and Physical</option>
                            <option>Hospital History</option>
                            <option>Immunization History</option>
                            <option>Imported Document</option>
                            <option>Insurance Card</option>
                            <option>Insurance Correspondence</option>
                            <option>Insurance Referral</option>
                            <option>Letter of Medical Necessity</option>
                            <option>Mammography Document</option>
                            <option>Medical Record Request</option>
                            <option>Medical Report</option>
                            <option>Misc. Doc</option>
                            <option>Misc. Hospital Document</option>
                            <option>Misc. Lab Result</option>
                            <option>Missed Appointment</option>
                            <option>Nursing Home Visit/Home Care</option>
                            <option>OB Documentation</option>
                            <option>OB Non-Stress Test</option>
                            <option>Office Consult Note</option>
                            <option>Office History and Physical</option>
                            <option>Office Procedure Report</option>
                            <option>Other</option>
                            <option>Other Office Letter</option>
                            <option>Other Office Note</option>
                            <option>Pathology Report</option>
                            <option>Patient Authorization/Referral</option>
                            <option>Patient Correspondence</option>
                            <option>Patient Demographics</option>
                            <option>Patient Driver&#39;s License</option>
                            <option>Patient Insurance Card</option>
                            <option>Patient Letter</option>
                            <option>Physician Established Patient Note</option>
                            <option>Physician New Patient Note</option>
                            <option>Prescriptions</option>
                            <option>Radiology Diagnostic</option>
                            <option>Radiology/Oncology</option>
                            <option>Referral Letter</option>
                            <option>Release of Information</option>
                            <option>Request for Amendment</option>
                            <option>Summary of Care Received</option>
                            <option>Surgery/Procedure Report</option>
                            <option>Tests</option>
                            <option>Therapy Note</option>
                            <option>Treadmill Report</option>
                            <option>Ultrasound</option>
                            <option>Ultrasound Document</option>
                            <option>Urgent Care Visit Note</option>
                            </select>
<?php }?>
                            <?php if( isset(Yii::app()->session['logged_user_type']) && (Yii::app()->session['logged_user_type'] == "patient" ) ) {?>
                            <select name="label" style="border: 1px solid #A6AFAF;">
                            <option disabled selected>Please Select</option>
                            <option>Advanced Directive</option>
                            <option>Authorization</option>
                            <option>Consent for Treatment</option>
                            <option>Consultation</option>
                            <option>X-Ray</option>
                            <option>Demographics</option>
                            <option>Diagnostic Ultrasound</option>
                            <option>Disability Info</option>
                            <option>Drivers License</option>
                            <option>ECHO</option>
                            <option>EEG-EMG</option>
                            <option>EKG</option>
                            <option>Eligibility</option>
                            <option>History and Physical</option>
                            <option>Hospital History</option>
                            <option>Immunization History</option>
                            <option>Imported Document</option>
                            <option>Insurance Card</option>
                            <option>Insurance Correspondence</option>
                            <option>Letter of Medical Necessity</option>
                            <option>Mammography Document</option>
                            <option>Medical Record Request</option>
                            <option>Medical Report</option>
                            <option>Misc. Doc</option>
                            <option>Other</option>
                            <option>Pathology Report</option>
                            <option>Prescriptions</option>
                            <option>Release of Information</option>
                            <option>Request for Amendment</option>
                            <option>Summary of Care Received</option>
                            <option>Surgery/Procedure Report</option>
                            <option>Tests</option>
                            <option>Therapy Note</option>
                            <option>Treadmill Report</option>
                            <option>Ultrasound Document</option>
                            <option>Urgent Care Visit Note</option>
                            </select>
                            </div>
                            <?php }?>
                        </div>
                        <div class="clear"></div>
                    </div>


                    <?php if( isset(Yii::app()->session['logged_user_type']) && (Yii::app()->session['logged_user_type'] == "doctor"||Yii::app()->session['logged_user_type'] == "parcticeAdmin" ) ) {?>
                    <div class="fld_area dashboardcont_leftbox">
                        <div class="fld_name fld_name_hight">Patient</div>
                        <div class="name_fld">

                            <input type="text" style="width: 50%" id="patient_name_search" class="filter_txtfld_calender fld_class doccustom" placeholder="Type your patient name.." onkeyup="autosearch();"  value=""/>
                            <input type="hidden" name="patient_id" id="patient_name_id" value="" />
                            <input type="hidden" name="patient_name" id="patient_name_save" value="" />
                            <ul id="patients_list" style="display:none;"></ul>
                        </div>
                        <div class="clear"></div>
                    </div>

<?php }?>

                    <div class="fld_area">
                        <div class="fld_name fld_name_hight">Date</div>
                        <div class="name_fld" style="position:inherit">

                    <input type="text" name="date"  class="filter_txtfld_calender datepicker small doccustom" placeholder="select date" value=""  required />
                        </div>
                        <div class="clear"></div>
                    </div>

                    <div class="fld_area">
                        <div class="fld_name fld_name_hight">Description</div>
                        <div class="name_fld" style="position:inherit">
                            <textarea class="txtarea_class" maxlength="256" size="32" id="msg_body" name="note" ></textarea>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="fld_area">
                        <label class="fld_name">Attach Files</label>
                        <input id="upload1" name="image[]" type="file" multiple accept=".jpg,.doc,.docx,.pdf,.png,.txt,.zip" />


                        <div class="clear"></div>
                    </div>
                    <div class="fld_area">
                        <span style="margin-top: 10px;    margin-left: 142px;color: #9C2727;">Only jpg,doc,docx,pdf,png,txt,zip files are allowed </span>
                    </div>
                    <div>
                        <label class="fld_name ">&nbsp;</label>

                       <span>
                            <?php echo CHtml::button('Save', array( 'class' => 'registbt', 'id'=>'sub')); ?>
                            <?php echo CHtml::button('Cancel', array('class' => "registbt",'data-dismiss'=>"modal",'id'=>'close_popup')); ?>
                         </span>
                    </div>
                    <div id="loadmodal">
                        <img id="loader" style="display: none; position:absolute;
    top:35%;width: 15%;
    left:50%;" src="<?php echo Yii::app()->request->baseUrl; ?>/assets/images/loading83.gif" />
                    </div>
                </div>
                <?php $this->endWidget(); ?>


        </div>
    </div>
    <!-- <footer style="margin-top: -19px;!important">
        <a href="#" class="js-modal-close">Close Button</a>
    </footer>-->
</div>



<div id="toPopup2"> 
    <div class="close"></div>
    <div id="popup_content"> <!--your content start-->
        <input type="hidden" id="cancel_hidden_app_id" value="" />
        <h4>Cancel Appointment</h4>
        <p id="cancel_app_title_id">Support | 9:00AM-9:30AM | Thursday , April 1st </p>
        <form name="" method="post">
            <textarea name="" placeholder="Send an optional note to your doctor..." id="mandatory_text"></textarea>
            <input type="button" name="cancel" value="Cancel" class="cancelbt" />
            <input type="button" name="accept" value="Send Message & Cancel" class="acceptbtcancel" />
        </form>
    </div>
</div>

<div id="backgroundPopup"></div>
<script type="text/javascript">
    $(function () {
        equalHeights($(".leftmenu"), $('.rightarea_dashboard'));
    });
    function appointmentCancel(app_id) {
        var app_id = $('#cancel_hidden_app_id').val();
        //if(confirm('are you sure?')){
        if ($('#mandatory_text').val() != '') {
            $.post("<?php echo Yii::app()->request->baseUrl; ?>/doctor/appointmentCancelPatient", {app_id: app_id, mandatory_text: $('#mandatory_text').val()}, function (response) {
                //$('#app_cancel_'+app_id).remove();
                //alert($('#booking_'+app_id).closest().html());
                location.reload();
            });
        } else {
            $('#mandatory_text').css('border', '1px solid red');
            $('#mandatory_text').keyup(function () {
                $(this).css('border', 'none');
            });
            return false;
        }
        //}
    }
</script>
<script>
    $(function() {
        $("#datepicker").datepicker("option", "dateFormat", "mm-dd-yy ");
    });
</script>
<script>

    $(document).ready(function(){

    $("#sub").click(function(){


        var obj = $('#compose_document')[0];
        var formData = new FormData(obj);
        console.log('ok');
       console.log((obj));
        $.ajax({
            url: '<?php echo Yii::app()->createUrl('documents/compose');?>',
            type: 'POST',
            data: formData,
            async: false,
            beforeSend: function () {
                $("#loader").show();
                $("#sub").attr("disabled", true);
            },
            success: function (data) {

                if(data==1){
                    location.reload();
                    $("#sub").attr("disabled", false);
                }
                else{
                    $("#loader").hide();
                    alert("please fill all fields.")
                    $("#sub").attr("disabled", false);
                }

            },
            error:function(data){

                $("#loader").hide();
                alert(data.statusText);
                $("#sub").attr("disabled", false);
            },
            cache: false,
            contentType: false,
            processData: false
        });

        return false;
    });
    });
</script>
<style>
    #page-link{
font-size: 12px;
    }
</style>

<script>
    var name = 0;
    $("#upload1").change(function(){
        var inp = document.getElementById('upload1');
        console.log(inp);
        for (var i = 0; i < inp.files.length; ++i) {
            //name+= ','+ inp.files.item(i).name;
            //  name ++;*/
            if(/.*\.(png)|(jpeg)|(jpg)|(doc)|(docx)|(txt)|(zip)|(pdf)$/.test(inp.files.item(i).name.toLowerCase()))
            {
                return true;
            }
            else{
                alert('file type not allowed');
                //  inp.files.item(i).name.focus();
                $("#upload1").val('');
                return false;
            }
            // alert("here is a file name: " + name;
        }

    });



</script>
<script>
    function autosearch() {

        var keyword = $('#patient_name_search').val();
        //var sent_to_persion = $('#sent_to_persion').val();
        //keyword != "" || keyword != null && (strlen($keyword) > 3)
        if( keyword != ""  || keyword != null) {
            if( keyword.length > 1 ){
            $.ajax({
                url: '<?php echo Yii::app()->request->baseUrl; ?>/Message/getLists',
                type: 'POST',
                data: {keyword:keyword},
                success:function(data){
                    $('#patients_list').show();
                    $('#patients_list').html(data);
                    if(keyword.length<1)
                    {
                        $('#patients_list').hide();
                    }
                }
            });
        }
            if(keyword.length<1)
            {
                $('#patients_list').hide();
            }
        }
    }
    function item(item) {
        var itm = item.split("|");
        var date = new Date(itm[2]);
        var date1 =(date.getMonth() + 1) + '-' + date.getDate() + '-' +  date.getFullYear();
       var itmData =itm[0]+" "+ date1;
        $('#patient_name_search').val(itmData);
        $('#patient_name_save').val(itm[0]);
        $('#patient_name_id').val(itm[1]);
        $('#patients_list').hide();

        //alert(itm[0]+"--"+itm[1]+"--"+itm[2]);
    }


</script>