<?php
/* @var $this DoctorController */
/* @var $model Doctor */

$this->breadcrumbs=array(
	'Dashboard'=>array('index'),
	'Edit Speciality',
);

/*$this->menu=array(
	array('label'=>'List Doctor', 'url'=>array('index')),
	array('label'=>'Manage Doctor', 'url'=>array('admin')),
);*/
?>

<!--<h1>Create Doctor</h1>-->

<?php //$this->renderPartial('_form', array('model'=>$model)); ?>

<div class="main">
    <div id="breadcrumb" class="fk-lbreadbcrumb newvd">
        <!--<span><a href="">Home</a></span> >  
        <span>Dashboard</span>--> 
        <?php $this->widget('zii.widgets.CBreadcrumbs', array(
				  'links'=>$this->breadcrumbs,
			  ));
		?>
    </div>
  	  <div class="dashboard_mainarea">
     	<div class="leftmenu">
        	 <p>
			 <?php if(Yii::app()->user->hasFlash('editAddress')): ?>
                <span class="flash-success">
                    <?php echo Yii::app()->user->getFlash('editAddress'); ?>
                </span>
             </p>
            <?php endif; ?>
       		 <?php /*?><h2>Doctor control panel</h2>
             <ul>
            	 <li><?php echo CHtml::link('Dashboard', $this->createAbsoluteUrl('index')); ?></li>
                 <li>
                 <!--<a href="#">Edit My Account</a>-->
                 <?php echo CHtml::link('Edit My Account', $this->createAbsoluteUrl('doctor/editProfile/'.Yii::app()->session['logged_user_id'])); ?>
                 </li>
                 <li><?php echo CHtml::link('My Addresses', $this->createAbsoluteUrl('doctor/address/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li class="active"><?php echo CHtml::link('My Specialities', $this->createAbsoluteUrl('doctor/speciatlity/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li><?php echo CHtml::link('View Profile', $this->createAbsoluteUrl('doctor/profile/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li><a href="#">Appointments</a></li>
                 <li><?php echo CHtml::link('Schedules', $this->createAbsoluteUrl('doctor/schedule/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li><?php echo CHtml::link('Timeoff', $this->createAbsoluteUrl('doctor/timeoff/'.Yii::app()->session['logged_user_id'])); ?></li>    
                 <li><?php echo CHtml::link('Todo List', $this->createAbsoluteUrl('doctor/todolist/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li><?php echo CHtml::link('Patients', $this->createAbsoluteUrl('doctor/patient/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li><?php echo CHtml::link('Setting Tab', $this->createAbsoluteUrl('doctor/settingTab/'.Yii::app()->session['logged_user_id'])); ?></li>           
             </ul><?php */?>
             <?php $this->renderPartial('//layouts/navigation'); ?>
        </div>
        <div class="rightarea_dashboard">
          <div class="dashboard_content1">
          	<?php if(Yii::app()->user->hasFlash('editSpeciatlity')): ?>
            <span class="flash-success">
                <?php echo Yii::app()->user->getFlash('editSpeciatlity'); ?>
            </span>
        <?php endif; ?>
          	<h1 class="h1"><?php //echo $model->isNewRecord ? 'Insert' : 'Update'; ?> Your Speciality </h1>
             <?php $form=$this->beginWidget('CActiveForm', array(
				'id'=>'edit_speciality ',
			)); ?>
            	<span>
                	<label>Speciality <span class="required">*</span> </label>
                    <div class="name_fld">
					<?php
						  $selected_doctor_speciality = $doctor_speciality['speciality_id'];
						  echo CHtml::dropDownList('speciality_id', $selected_doctor_speciality, 
						  $user_speciality,
						  array('empty' => 'Select your speciality','class'=>'fld_class'));
					?>
					<?php //echo $form->error($model,'address'); ?>
                    </div>
                </span>
                <span>
                	<label>Default </label>
                    <div class="name_fld">
                    <input id="DoctorAddress_default_status" type="hidden" value="0" name="DoctorAddress[default_status]">
                    <input id="DoctorAddress_default_status" type="checkbox" value="1" name="DoctorAddress[default_status]" <?php echo ($doctor_speciality['default_status'] == 1)?'checked="checked"':''; ?>>
                    </div>
                </span>
                <span>
                <?php echo CHtml::submitButton($doctor_speciality['id'] ? 'Update' : 'Save',array('class'=>'grn_btn')); ?>
                </span>
            <?php $this->endWidget(); ?>
          </div>
        </div> 
      </div>
</div>