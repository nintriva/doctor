<?php
/* @var $this DoctorController */
/* @var $model Doctor */

$this->breadcrumbs=array(
	'Dashboard'=>array('index'),
	'Edit Profile',
);

/*$this->menu=array(
	array('label'=>'List Doctor', 'url'=>array('index')),
	array('label'=>'Manage Doctor', 'url'=>array('admin')),
);*/
?>

<!--<h1>Create Doctor</h1>-->

<?php //$this->renderPartial('_form', array('model'=>$model)); ?>

<div class="main">
    <div id="breadcrumb" class="fk-lbreadbcrumb newvd">
        <!--<span><a href="">Home</a></span> >  
        <span>Dashboard</span>--> 
        <?php $this->widget('zii.widgets.CBreadcrumbs', array(
				  'links'=>$this->breadcrumbs,
			  ));
		?>
    </div>
  	  <div class="dashboard_mainarea">
     	<div class="leftmenu lftmenu_hight">
       		 <?php /*?><h2>Doctor control panel</h2>
             <ul>
            	 <li><?php echo CHtml::link('Dashboard', $this->createAbsoluteUrl('index')); ?></li>
                 <li class="active">
                 <!--<a href="#">Edit My Account</a>-->
                 <?php echo CHtml::link('Edit My Account', $this->createAbsoluteUrl('doctor/editProfile/'.Yii::app()->session['logged_user_id'])); ?>
                 </li>
                 <li><?php echo CHtml::link('My Addresses', $this->createAbsoluteUrl('doctor/address/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li><?php echo CHtml::link('My Specialities', $this->createAbsoluteUrl('doctor/speciatlity/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li><?php echo CHtml::link('View Profile', $this->createAbsoluteUrl('doctor/profile/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li><a href="#">Appointments</a></li>
                 <li><?php echo CHtml::link('Schedules', $this->createAbsoluteUrl('doctor/schedule/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li><?php echo CHtml::link('Timeoff', $this->createAbsoluteUrl('doctor/timeoff/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li><?php echo CHtml::link('Todo List', $this->createAbsoluteUrl('doctor/todolist/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li><?php echo CHtml::link('Patients', $this->createAbsoluteUrl('doctor/patient/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li><?php echo CHtml::link('Setting Tab', $this->createAbsoluteUrl('doctor/settingTab/'.Yii::app()->session['logged_user_id'])); ?></li>
             </ul><?php */?>
             <?php $this->renderPartial('//layouts/navigation'); ?>
        </div>
        <div class="rightarea_dashboard">
          <div class="dashboard_content1">
          	<?php if(Yii::app()->user->hasFlash('editProfile')): ?>
            <span class="flash-success">
                <?php echo Yii::app()->user->getFlash('editProfile'); ?>
            </span>
        	<?php endif; ?>
          	<?php $form=$this->beginWidget('CActiveForm', array(
				'id'=>'registration',
				'htmlOptions' => array(
					'enctype' => 'multipart/form-data',
				),
			)); ?>
            
            	<div class="dashboardcont_leftbox">
                	<h1>Personal Information</h1>
                	<div class="box_content">

                           <!-- <div class="fld_area">
                                <div class="fld_name">First Name </div>
                                <div class="name_fld"><input type="text" name="lnme" placeholder="First Name" class="fld_class"></div>
                                <div class="clear"></div>
                            </div>-->
                            <span>
								<?php echo $form->labelEx($model,'first_name'); ?>
                                <div class="name_fld">
                                <?php echo $form->textField($model,'first_name',array('size'=>32,'maxlength'=>32,'placeholder'=> 'First Name','class'=>'fld_class')); ?>
                                <?php echo $form->error($model,'first_name'); ?>
                                </div>
                            </span>
                            <!--<div class="fld_area">
                                <div class="fld_name">Middle Name</div>
                                <div class="name_fld"><input type="text" name="lnme" placeholder="Middle Name" class="fld_class"></div>
                                <div class="clear"></div>
                            </div>-->
                            <span>
								<?php echo $form->labelEx($model,'last_name'); ?>
                                <div class="name_fld">
                                <?php echo $form->textField($model,'last_name',array('size'=>32,'maxlength'=>32,'placeholder'=>'Last Name','class'=>'fld_class')); ?>
                                <?php echo $form->error($model,'last_name'); ?>
                                </div>
                            </span>
                            <span>
								<?php echo $form->labelEx($model,'gender'); ?>
                                <div class="name_fld">
                                <?php
                                      $selected_gender = $model->gender;
                                      echo CHtml::dropDownList('gender', $selected_gender, 
                                      $model->genderOptions,
                                      array('empty' => 'Select your gender','class'=>'fld_class'));
                                ?>
                                <?php echo $form->error($model,'gender'); ?>
                                </div>
                            </span>
                            <!--<div class="fld_area">
                                <div class="fld_name">Birth Date</div>
                                <div class="name_fld">
                                	<div class="select_option">
                                        <select class="fld_class2">
                                            <option>Febuary</option>
                                            <option>March</option>
                                        </select>
                                    </div>
                                    <div class="select_option2">
                                    <select class="fld_class2">
                                        <option>12</option>
                                        <option>13</option>
                                    </select>
                                    </div>
                                    <div class="select_option3">
                                    <select class="fld_class2">
                                        <option>2014</option>
                                        <option>2013</option>
                                    </select>
                                    </div>
                                </div>
                                <div class="clear"></div>
                            </div>-->
                            <span>
								<?php echo $form->labelEx($model,'birth_date'); ?>
                                <div class="name_fld">
                                <?php echo $form->textField($model,'birth_date',array('readonly'=>'readonly','class'=>'readonly fld_class')); ?>
                                <?php echo $form->error($model,'birth_date'); ?>
                                </div>
                            </span>
                            <span>
								<?php echo $form->labelEx($model,'title'); ?>
                                <div class="name_fld">
                                <?php
                                      $selected_title = $model->title;
                                      echo CHtml::dropDownList('title', $selected_title, 
                                      $model->titleOptions,
                                      array('empty' => 'Select your title','class'=>'fld_class'));
                                ?>
                                <?php echo $form->error($model,'title'); ?>
                                </div>
                            </span>

                    </div>
                </div>
                <div class="dashboardcont_leftbox">
                	<h1>Profile Details</h1>
                	<div class="box_content">

                            <span>
                                <?php echo $form->labelEx($model,'image'); ?>
                                <div class="name_fld">
                                <!--<img alt="" src="<?php //echo Yii::app()->request->baseUrl; ?>/assets/images/doctor_picx.jpg">-->
                                <?php if($model->image){ ?>
                                <div class="row">
                                     <?php echo CHtml::image(Yii::app()->request->baseUrl.'/assets/upload/doctor/'.$model->id."/".$model->image,"image",array("width"=>200)); ?>
                                </div>
                                <?php }else{ ?>
                                	<div class="row">
									<?php echo CHtml::image(Yii::app()->request->baseUrl.'/assets/images/avatar.png',"image",array("width"=>200)); ?>
                                </div>
                                <?php } ?>
                                <?php echo CHtml::activeFileField($model, 'image'); ?> 
                                <?php echo $form->error($model,'image'); ?>
                                </div>
                            </span>
                            <span>
								<?php echo $form->labelEx($model,'phone'); ?>
                                <div class="name_fld">
                                <?php echo $form->textField($model,'phone',array('size'=>32,'maxlength'=>32,'placeholder'=>'e.g. XXX-XXX-XXXX','class'=>'fld_class')); ?>
                                <?php echo $form->error($model,'phone'); ?>
                                </div>
                            </span>
                            <span>
								<?php echo $form->labelEx($model,'email'); ?>
                                <div class="name_fld">
                                <?php echo $form->textField($model,'email',array('size'=>32,'maxlength'=>32,'placeholder'=>'Email Address','class'=>'fld_class')); ?>
                                <?php echo $form->error($model,'email'); ?>
                                </div>
                            </span>
                            <span>
								<?php echo $form->labelEx($model,'addr1'); ?>
                                <div class="name_fld">
                                <?php echo $form->textField($model,'addr1',array('size'=>32,'maxlength'=>32,'placeholder'=>'Address 1','class'=>'fld_class')); ?>
                                </div>
                            </span>
                            <span>
                                <?php echo $form->labelEx($model,'addr2'); ?>
                                <div class="name_fld">
                                <?php echo $form->textField($model,'addr2',array('size'=>32,'maxlength'=>32,'placeholder'=>'Address 2','class'=>'fld_class')); ?>
                                </div>
                            </span>
                            <span>
                                <?php echo $form->labelEx($model,'state'); ?>
                                <div class="name_fld">
                                <?php echo $form->textField($model,'state',array('size'=>32,'maxlength'=>32,'placeholder'=>'State','class'=>'fld_class')); ?>
                                </div>
                            </span>
                            <span>
                                <?php echo $form->labelEx($model,'country'); ?>
                                <div class="name_fld">
                                <?php echo $form->textField($model,'country',array('size'=>32,'maxlength'=>32,'placeholder'=>'Country','class'=>'fld_class')); ?>
                                </div>
                            </span>
                            <span>
                                <?php echo $form->labelEx($model,'zip'); ?>
                                <div class="name_fld">
                                <?php echo $form->textField($model,'zip',array('size'=>32,'maxlength'=>32,'placeholder'=>'ZIP','class'=>'fld_class')); ?>
                                <?php echo $form->error($model,'zip'); ?>
                                </div>
                            </span>
                            <span>
								<?php echo $form->labelEx($model,'comments'); ?>
                                <div class="name_fld">
                                <?php echo $form->textArea($model,'comments',array('size'=>32,'maxlength'=>255,'placeholder'=> 'Comments','class'=>'fld_class')); ?>
                                <?php echo $form->error($model,'comments'); ?>
                                </div>
                            </span>
                    </div>
                </div>
                <div class="dashboardcont_leftbox">
                	<h1>Account Details</h1>
                	<div class="box_content">
							<span>
								<?php echo $form->labelEx($model,'username'); ?>
                                <div class="name_fld">
                                <?php echo $form->textField($model,'username',array('size'=>32,'maxlength'=>32,'placeholder'=> 'Username','class'=> 'readonly fld_class','readonly'=> 'readonly')); ?>
                                <?php echo $form->error($model,'username'); ?>
                                </div>
                            </span>
                            <!--<div class="fld_area">
                                <div class="fld_name">Preferred Language</div>
                                <div class="name_fld"><select class="fld_class"><option>English</option><option>Spanish</option></select></div>
                                <div class="clear"></div>
                            </div>-->
                            <span>
								<?php echo $form->labelEx($model,'date_created'); ?>
                                <div class="name_fld">
                                <?php echo date('M d, Y h:i',strtotime($model->date_created)); ?>
                                </div>
                            </span>
                    </div>
                </div>
                <div class="dashboardcont_leftbox">
                	<h1>Professional Information</h1>
                	<div class="box_content">

                            <span>
								<?php echo $form->labelEx($model,'degree'); ?>
                                <div class="name_fld">
                                <?php
                                      $selected_degree = $model->degree;
                                      echo CHtml::dropDownList('degree', $selected_degree, 
                                      $model->degreeOptions,
                                      array('empty' => 'Select your degree','class'=>'fld_class'));
                                ?>
                                <?php echo $form->error($model,'speciality'); ?>
                                </div>
                            </span>
                            <span>
								<?php echo $form->labelEx($model,'speciality'); ?>
                                <div class="name_fld">
                                <?php
                                      $selected = $model->speciality;
                                      echo CHtml::dropDownList('speciality', $selected, 
                                      $user_speciality,
                                      array('empty' => 'Select your specialty','class'=>'fld_class'));
                                ?>
                                <?php echo $form->error($model,'speciality'); ?>
                                </div>
                            </span>
                            <span>
								<?php echo $form->labelEx($model,'education'); ?>
                                <div class="name_fld">
                                <?php echo $form->textArea($model,'education',array('size'=>32,'maxlength'=>255,'placeholder'=> 'Education','class'=>'fld_class')); ?>
                                <?php echo $form->error($model,'education'); ?>
                                </div>
                            </span>
                            <span>
								<?php echo $form->labelEx($model,'residency_training'); ?>
                                <div class="name_fld">
                                <?php echo $form->textArea($model,'residency_training',array('size'=>32,'maxlength'=>255,'placeholder'=> 'Residency','class'=>'fld_class')); ?>
                                <?php echo $form->error($model,'residency_training'); ?>
                                </div>
                            </span>
                            <span>
								<?php echo $form->labelEx($model,'hospital_affiliations'); ?>
                                <div class="name_fld">
                                <?php echo $form->textArea($model,'hospital_affiliations',array('size'=>32,'maxlength'=>255,'placeholder'=> 'Hospital','class'=>'fld_class')); ?>
                                <?php echo $form->error($model,'hospital_affiliations'); ?>
                                </div>
                            </span>
                            <span>
								<?php echo $form->labelEx($model,'board_certifications'); ?>
                                <div class="name_fld">
                                <?php echo $form->textArea($model,'board_certifications',array('size'=>32,'maxlength'=>255,'placeholder'=> 'Board Certifications','class'=>'fld_class')); ?>
                                <?php echo $form->error($model,'board_certifications'); ?>
                                </div>
                            </span>
                            <span>
								<?php echo $form->labelEx($model,'awards_publications'); ?>
                                <div class="name_fld">
                                <?php echo $form->textArea($model,'awards_publications',array('size'=>32,'maxlength'=>255,'placeholder'=> 'Awards and Publications','class'=>'fld_class')); ?>
                                <?php echo $form->error($model,'awards_publications'); ?>
                                </div>
                            </span>
                            <span>
								<?php echo $form->labelEx($model,'languages_spoken'); ?>
                                <div class="name_fld">
                                <?php echo $form->textArea($model,'languages_spoken',array('size'=>32,'maxlength'=>255,'placeholder'=> 'Languages Spoken','class'=>'fld_class')); ?>
                                <?php echo $form->error($model,'languages_spoken'); ?>
                                </div>
                            </span>
                            <span>
								<?php echo $form->labelEx($model,'insurances_accepted'); ?>
                                <div class="name_fld">
                                <?php echo $form->textArea($model,'insurances_accepted',array('size'=>32,'maxlength'=>255,'placeholder'=> 'Insurances Accepted','class'=>'fld_class')); ?>
                                <?php echo $form->error($model,'insurances_accepted'); ?>
                                </div>
                            </span>
                           <span>
							<?php if(CCaptcha::checkRequirements()): ?>
                                <?php echo $form->labelEx($model,'verifyCode'); ?>
                                <div class="name_fld">
                                <?php $this->widget('CCaptcha'); ?>
                                <?php echo $form->textField($model,'verifyCode',array('class'=>'fld_class')); ?>
                                <div class="hint">Please enter the letters as shown in the image above.
                                <br/>Letters are not case-sensitive.</div>
                                </div>
                           <?php endif; ?>
                          </span>
                        <span>
                        <?php echo CHtml::submitButton($model->isNewRecord ? 'Update' : 'Save',array('class'=>'grn_btn')); ?>
                        </span>

                    </div>
                </div>
            <?php $this->endWidget(); ?>
          </div>
        </div> 
      </div>
</div>
<script>
$(function() {
	$( "#Doctor_birth_date" ).datepicker({
		changeMonth: true,changeYear: true, yearRange : '-80:-20',dateFormat: 'yy-mm-dd'
	});
});
</script>