<?php
/* @var $this DoctorController */
/* @var $model Doctor */

$this->breadcrumbs=array(
	'Dashboard'=>array('index'),
	'Edit Profile',
);

/*$this->menu=array(
	array('label'=>'List Doctor', 'url'=>array('index')),
	array('label'=>'Manage Doctor', 'url'=>array('admin')),
);*/
?>

<!--<h1>Create Doctor</h1>-->

<?php //$this->renderPartial('_form', array('model'=>$model)); ?>

<div class="main">
    <div id="breadcrumb" class="fk-lbreadbcrumb newvd">
        <!--<span><a href="">Home</a></span> >  
        <span>Dashboard</span>--> 
        <?php $this->widget('zii.widgets.CBreadcrumbs', array(
				  'links'=>$this->breadcrumbs,
			  ));
		?>
    </div>
  	  <div class="dashboard_mainarea">
     	<div class="leftmenu">
        	 
       		<?php /*?> <h2>Doctor control panel</h2>
             <ul>
            	 <li><?php echo CHtml::link('Dashboard', $this->createAbsoluteUrl('index')); ?></li>
                 <li><?php echo CHtml::link('Edit My Account', $this->createAbsoluteUrl('doctor/editProfile/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li><?php echo CHtml::link('Special Offers', $this->createAbsoluteUrl('doctor/offers/'.Yii::app()->session['logged_user_id'])); ?></li><?php */?>
                 <?php /*?><li><?php echo CHtml::link('My Addresses', $this->createAbsoluteUrl('doctor/address/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li><?php echo CHtml::link('My Specialities', $this->createAbsoluteUrl('doctor/speciatlity/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li><?php echo CHtml::link('View Profile', $this->createAbsoluteUrl('doctor/profile/'.Yii::app()->session['logged_user_id'])); ?></li><?php */?>
                 <?php /*?><li><?php echo CHtml::link('Appointments', $this->createAbsoluteUrl('doctor/appointment/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li class="active"><?php echo CHtml::link('Schedules', $this->createAbsoluteUrl('doctor/schedule/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li><?php echo CHtml::link('Timeoff', $this->createAbsoluteUrl('doctor/timeoff/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li><?php echo CHtml::link('Todo List', $this->createAbsoluteUrl('doctor/todolist/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li><?php echo CHtml::link('Patients', $this->createAbsoluteUrl('doctor/patient/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li><?php echo CHtml::link('Setting Tab', $this->createAbsoluteUrl('doctor/settingTab/'.Yii::app()->session['logged_user_id'])); ?></li>
             </ul><?php */?>
             <?php $this->renderPartial('//layouts/navigation'); ?>
        </div>
        <div class="rightarea_dashboard">
          <div class="dashboard_content1">
          	<?php if(Yii::app()->user->hasFlash('editSchedules')): ?>
            <span class="flash-success">
                <?php echo Yii::app()->user->getFlash('editSchedules'); ?>
            </span>
        <?php endif; ?>
          	<h1 class="h1"><?php echo $model->isNewRecord ? 'Insert' : 'Update'; ?> Your Schedule</h1>
             <?php $form=$this->beginWidget('CActiveForm', array(
				'id'=>'edit_schedule',
             		'htmlOptions'=>array('onsubmit'=>'return date_validate();'),
			)); ?>
            	<span>
                	<?php echo $form->labelEx($model,'name', array('class'=>'fld_name')); ?>
                    <div class="name_fld">
					<?php echo $form->textField($model,'name',array('size'=>32,'maxlength'=>255,'placeholder'=> 'Name','class'=>'fld_class')); ?>
					<?php echo $form->error($model,'name'); ?>
                    </div>
                </span>
                <span>
					<?php echo $form->labelEx($model,'address_id',array('class'=>'fld_name')); ?>
                    <div class="name_fld">
                    <?php
                          $selected_address = $model->address_id;
                          echo CHtml::dropDownList('address_id', $selected_address, 
                          $doctor_address,
                          array('empty' => 'Select your address','class'=>'fld_class'));
                    ?>
                    <?php echo $form->error($model,'address_id'); ?>
                    </div>
                </span>
                <span>
                	<?php echo $form->labelEx($model,'from_date',array('class'=>'fld_name')); ?>
                    <div class="name_fld">
					<?php echo $form->textField($model,'from_date',array('size'=>32,'maxlength'=>32,'placeholder'=>'Valid From Date','class'=>'fld_class datepicker readonly','readonly'=>'readonly')); ?>
                    <?php echo $form->error($model,'from_date'); ?>
                    </div>
                </span>
                <span>
                	<?php echo $form->labelEx($model,'to_date', array('class'=>'fld_name')); ?>
                    <div class="name_fld">
					<?php echo $form->textField($model,'to_date',array('size'=>32,'maxlength'=>32,'placeholder'=>'Valid To Date','class'=>'fld_class datepicker readonly','readonly'=>'readonly')); ?>
					<?php echo $form->error($model,'to_date'); ?>
                    </div>
                </span>
                <span>
                <?php echo CHtml::submitButton($model->isNewRecord ? 'Save' : 'Update',array('class'=>'grn_btn')); ?>
                </span>
            <?php $this->endWidget(); ?>
          </div>
        </div> 
      </div>
</div>
<script>
$(function(){
	$('#edit_schedule input').keyup(function(){
		$(this).removeClass('error');
		$(this).parent('div.name_fld').children('div.errorMessage').hide();
	});
	$('#edit_schedule input, #edit_schedule select').change(function(){
		$(this).removeClass('error');
		$(this).parent('div.name_fld').children('div.errorMessage').hide();
	});
});
function date_validate() {
    var startDate = document.getElementById("DoctorSchedule_from_date").value;
    var endDate = document.getElementById("DoctorSchedule_to_date").value;
    if ((Date.parse(startDate) > Date.parse(endDate))) {
        alert("End date should be greater than Start date");
        return false;
    }
}
</script>