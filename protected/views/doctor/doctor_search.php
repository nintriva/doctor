<?php Yii::app()->session['logged_in']; ?>

<script src='<?php echo Yii::app()->getBaseUrl(true); ?>/assets/js/dashboard_script.js'></script>
<div class="main"> 
    <div class="searchpage_bg">
        <div class="fixeddiv">
            <input type="hidden" name="page" id="page" placeholder="page" value="<?php
            if (isset($_REQUEST['page']))
                echo $_REQUEST['page'];
            else
                echo 0;
            ?>">
            <form name="refine search"  id="searchform" method="get" action="<?php echo Yii::app()->request->baseUrl; ?>/doctor/doctorSearch" onsubmit="return callSearchSubmit();">
                <input type="hidden" name="office_name" id="office_name" placeholder="office name" value="<?php if (isset($_REQUEST['office_name'])) echo $_REQUEST['office_name']; ?>">
                <?php /* ?><input type="hidden" name="name_autocomp" id="name_autocomp" placeholder="name" value="<?php if(isset($_REQUEST['name_autocomp'])) echo $_REQUEST['name_autocomp']; ?>"><?php */ ?>
                <div>
                    <span class="rfine_search">
                        <label>Speciality</label>
                        <?php
                        $selected_doctor_speciality = isset($dataProvider['speciality_id']) ? $dataProvider['speciality_id'] : "";

                        echo CHtml::dropDownList('speciality_id', $selected_doctor_speciality, $user_speciality, array('empty' => 'Select speciality', 'class' => 'speciality_cls'));
                        ?>
                    </span>
                    <span class="rfine_search">
                        <label>Location<font style="font-weight:normal;">(Zip or City, State)</font></label>
                        <input type="text" name="geo_location_id" id="geo_location_id" placeholder="Location" value="<?php if (isset($_REQUEST['geo_location_id'])) echo $_REQUEST['geo_location_id']; ?>">
                    </span>
                    <span class="rfine_search">
                        <label>Doctor</label>
                        <input type="text" name="name_autocomp" id="name_autocomp" placeholder="Name" value="<?php if (isset($_REQUEST['name_autocomp'])) echo $_REQUEST['name_autocomp']; ?>">
                        <select name="insurance" id="insurance" style="display:none;"> 
                            <option value="">I'll choose my insurance later</option>
                        </select>
                    </span>

                </div>
                <div>
                    <span class="rfine_search">
                        <label>Insurance</label>
                        <?php
                        $selected_doctor_insurance = isset($dataProvider['insurance']) ? $dataProvider['insurance'] : "";
                        echo CHtml::dropDownList('insurance', $selected_doctor_insurance, $user_insurance, array('empty' => 'Select Insurance', 'class' => 'insurance_cls'));
                        ?>
                    </span>
                    <span class="rfine_search">
                        <?php
                        $selected_doctor_language = isset($dataProvider['language']) ? $dataProvider['language'] : "";
                        ?>
                        <label>Language</label>
                        <select id="language" name="language" class="language_cls">
                            <option value="Any" >Any </option>
                            <?php if (count($user_language) > 0) { ?>
                                <?php foreach ($user_language as $key => $data) { ?>
                                    <option value="<?php echo $key; ?>" <?php
                                    if ($selected_doctor_language == $key) {
                                        echo "selected";
                                    }
                                    ?> ><?php echo $data; ?></option>
                                        <?php } ?>
                                    <?php } ?>
                        </select>
                    </span>
                    <span class="rfine_search">
                        <label>Doctor Gender</label>
                        <select name="gender" id="gender"> 
                            <option value="All" <?php if (isset($_REQUEST['gender']) && ('All' == $_REQUEST['gender'])) echo 'selected="selected"'; ?>>Any </option>
                            <option value="M" <?php if (isset($_REQUEST['gender']) && ('M' == $_REQUEST['gender'])) echo 'selected="selected"'; ?>>Male </option>
                            <option value="F" <?php if (isset($_REQUEST['gender']) && ('F' == $_REQUEST['gender'])) echo 'selected="selected"'; ?>>Female </option>
                        </select>
                    </span>
                </div>
                <?php
                if (isset($_GET['schedualOnly']) && $_GET['schedualOnly'] == 1) {
                    if (isset(Yii::app()->session['page_fullUrl'])) {
                        $page_fullUrl = Yii::app()->session['page_fullUrl'];
                        unset(Yii::app()->session['page_fullUrl']);
                    } else {
                        $fullUrl = explode('&schedualOnly', "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI']);
                        $page_fullUrl = $fullUrl[0];
                    }
                } else {
                    $page_fullUrl = "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
                    Yii::app()->session['page_fullUrl'] = $page_fullUrl;
                }
                ?>
                <div style="display:none;">            
                    <span class="rfine_search">
                        <label style="padding-top:5px;"><input type='checkbox' id='scheduleOnly' <?php if (isset($_GET['schedualOnly']) && $_GET['schedualOnly'] == 1) { ?>checked<?php } ?> onclick='scheduleOnlyCall();'> Schedule Only</label>
                        <input type='hidden' id='fullUrl' value='<?php echo $page_fullUrl; ?>'>
                    </span>
                </div><!---->
                <input type="hidden" name="zip" value="" />

                <div style="margin-top:15px; float:left;">
                    <span class="rfine_search_last"><input type="submit" value="Refine Search" class="registbt" ></span>
                    <span class="rfine_search_last">
                        <a href="<?php echo Yii::app()->request->baseUrl; ?>/doctor/doctorMapView?<?php echo $_SERVER['QUERY_STRING']; ?>">
                            <img src="<?php echo Yii::app()->request->baseUrl; ?>/assets/images/mapviewicon.png" /><font style="color:#3B7FCA; font-size:12px; font-weight:bold;">
                            <span id="map_view_button">
                                Map View</span>
                            </font>
                        </a>
                    </span>
                </div>
            </form>
        </div>
        <input type="hidden" name="scroll_height" id="scroll_height" value="419" />
        <script>
            $(document).ready(function () {
                if (get_cookie('map_view') != 'yes') {
                    $('.profilelist_bg').css('padding', '0px 0px');
                    $('#schedule_map').hide();
                    $('#scroll_height').val(160);
                } else {
                    $("#map_view_button").html("Hide Map");
                }
            });
            $(window).bind('load', function () {
                unset_cookie('map_view', 'no');
            });
            function mapView() {
                if ($("#map_view_button").html() == "Map View") {
                    if (get_cookie('map_view') == 'yes') {
                        unset_cookie('map_view', 'no');
                        $('.profilelist_bg').css('padding', '0px 0px');
                        $('#schedule_map').hide();
                        $('#scroll_height').val(160);
                    } else {
                        set_cookie('map_view', 'yes');
                        location.reload();
                    }
                }
                if ($("#map_view_button").html() == "Hide Map") {
                    unset_cookie('map_view', 'no');
                    $('.profilelist_bg').css('padding', '0px 0px');
                    $('#schedule_map').hide();
                    $('#scroll_height').val(160);
                    $("#map_view_button").html("Map View");
                }
            }
            function set_cookie(name, value) {
                var today = new Date();
                today.setTime(today.getTime());
                var expires_date = new Date(today.getTime() + (1000 * 60 * 60 * 24 * 90));
                document.cookie = name + "=" + escape(value) + ";expires=" + expires_date.toGMTString();
            }
            function unset_cookie(name, value) {
                var today = new Date();
                today.setTime(today.getTime());
                var expires_date = new Date(today.getTime() + (-(1000 * 60 * 60 * 24 * 90)));
                document.cookie = name + "=" + escape(value) + ";expires=" + expires_date.toGMTString();
            }
            function get_cookie(name) {
                var a = null;
                if (document.cookie) {
                    var b = document.cookie.split((escape(name) + '='));
                    if (b.length >= 2) {
                        var c = b[1].split(';');
                        a = unescape(c[0]);
                    }
                }
                return a;
            }
        </script>

        <div class="profilelist_bg">

            <div class="page-navigation-top">
                 <span id="searchcounter"></span><?php
                $this->widget('CLinkPager', array(
                    'pages' => $pages, 'header' => '', 'prevPageLabel' => '&lt;&lt;', 'nextPageLabel' => '&gt;&gt;',
                ))
                ?>

                <!--	<select name="sort" id="sort" onchange="callZip();"> <option value=""> select</option> <option value="zip" <?php
                if (!empty($_REQUEST['zip']) && $_REQUEST['zip'] == "zip") {
                    echo "selected='selected'";
                }
                ?>> zip</option></select> -->

            </div>

            <?php if (!empty($doctor_list)) { ?>

                <div class="profilesec_right">
                    <div id="paginationdemo" class="demo">
                        <div id="p1" class="pagedemo _current" style="">
                            <?php
                            $count_d = 1;
                            $notmatchdistance = 0;
                            if ($doctor_list) {

                                for ($i = 0; $i < count($doctor_list); $i++) {
                                    foreach ($doctor_list[$i]->doctorAddress as $key2) {


                                        $getDoctorSchedual = $this->doctorSchedual($doctor_list[$i]->id, $key2->id);
                                        $getdoctorSchedualWithDateCondition = $this->doctorSchedualWithDateCondition($doctor_list[$i]->id, $key2->id);

                                        $condition = 0;
                                        $nxt = 0;
                                        if (isset($_GET['schedualOnly']) && ($_GET['schedualOnly'] == 1)) {

                                            if ($doctor_list[$i]->id == $key2->doctor_id) {
                                                if (count($getdoctorSchedualWithDateCondition) > 0) {
                                                    $condition = 1;
                                                }
                                            }
                                            $nxt = count($getdoctorSchedualWithDateCondition);
                                            $cnd = 'yes';
                                        } else {

                                            if ($doctor_list[$i]->id == $key2->doctor_id) {
                                                if (count($getDoctorSchedual) > 0) {
                                                    $condition = 1;
                                                }
                                            }
                                            $nxt = count($getDoctorSchedual);
                                            $cnd = 'no';
                                        }
                                        //echo count($getDoctorSchedual);
                                        //if( $condition == 1) {
                                        if (($doctor_list[$i]->id == $key2->doctor_id) && (($cnd == 'yes' && $nxt > 0 ) || ($cnd == 'no'))) {

                                            //	if( $doctor_list[$i]->id == $key2->doctor_id ) {
                                            ?>

                                            <div class="profColumn" id="div<?php $count_d; ?>">
                                                <div class="photoColumn_left">
                                                    <!--<a href=""><img src="<?php echo Yii::app()->request->baseUrl; ?>/assets/images/doctor_picx.jpg" alt=""></a>-->
                                                    <?php
                                                    //if($doctor_list[$i]['image']){
                                                    $filePath = 'assets/upload/doctor/' . $doctor_list[$i]->id . "/" . $doctor_list[$i]->image;
                                                    if (@file_get_contents($filePath, 0, NULL, 0, 1)) {
                                                        ?>
                                                        <a href="javascript:void(0);"><?php echo CHtml::image(Yii::app()->request->baseUrl . '/assets/upload/doctor/' . $doctor_list[$i]->id . "/" . $doctor_list[$i]->image, "image", array()); ?></a>
                                                    <?php } else { ?>
                                                        <?php if ($doctor_list[$i]->gender == 'M') { ?>
                                                            <a href="javascript:void(0);"><?php echo CHtml::image(Yii::app()->request->baseUrl . '/assets/images/avatar.png', "image", array("width" => 200)); ?></a>
                                                        <?php } else { ?>
                                                            <a href="javascript:void(0);"><?php echo CHtml::image(Yii::app()->request->baseUrl . '/assets/images/avatar.png', "image", array("width" => 200)); ?></a>
                                                        <?php } ?>
                                                    <?php } ?>
                                                            
                                                            
                                                        <!-- start rating section -->
                                                        <?php
                                                        $doctor_id = $doctor_list[$i]->id;
                                                        $doctor_review_condition = 't.status = 1 and doctor_id =' . $doctor_id;
                                                        $doctor_review_criteria = new CDbCriteria(array(
                                                            'condition' => $doctor_review_condition,
                                                            'order' => 't.id DESC',
                                                            'limit' => 5
                                                        ));
                                                        $doctor_review = DoctorReview::model()->with('patient_details')->findall($doctor_review_criteria);
                                                        $overall_rating = 0;
                                                        $total_review = count($doctor_review);
                                                        foreach ($doctor_review as $doctor_review_key => $doctor_review_val) {
                                                            $overall_rating += $doctor_review_val->overall_rating;
                                                        }
                                                        if ($total_review == 0)
                                                            $average_rating = 0;
                                                        else {
                                                            $average_rating = round($overall_rating / $total_review);
                                                        }
                                                        ?>

                                                        <div class="rating rating_pos_<?php echo $average_rating; ?>"></div>

                                                        <!-- end rating section -->
                                                            
                                                            
                                                            
                                                </div>
                                                <?php
                                                if (isset($doctor_list[$i]->doctorSpeciality->speciality_id)) {
                                                    $speciality_id = isset($_GET['speciality_id']) ? $_GET['speciality_id'] : 0;
                                                    if (isset($user_speciality[$speciality_id])) {
                                                        $specialId = $_GET['speciality_id'];
                                                    } else {
                                                        $specialId = $doctor_list[$i]->speciality;
                                                    }
                                                }
                                                ?>	
                                                <div class="profpicx_containerbox">						
                                                    <a class="docname" href="<?php echo $this->createAbsoluteUrl('doctor/' . $doctor_list[$i]->slug . '|' . $specialId . '|' . $key2->id); ?>"><?php echo $doctor_list[$i]->full_name; ?></a>
                                                    <div class="doctor-address-distance"> 

                                                        <div class="sub"><?php //Helpers::pre($doctor_list[$i]['ds_speciality_id']);  ?>
                                                            <?php
                                                            echo $user_speciality[$specialId];
                                                            ?>							
                                                            <hr>
                                                        </div> 
                                                        <div class="cmt_block">
                                                            <?php
                                                            echo substr($doctor_list[$i]->comments, 0, 100);
                                                            ?>
                                                            <?php
                                                            if (strlen($doctor_list[$i]->comments) > 100) {
                                                                ?>
                                                                <span><a href="<?php echo $this->createAbsoluteUrl('doctor/' . $doctor_list[$i]->slug . '|' . $specialId); ?>"><img width="10" src="<?php echo Yii::app()->request->baseUrl; ?>/assets/images/sp_icon_left.png"></a></span>
                                                            <?php } ?>
                                                        </div>

                                                    </div>
                                                    <div class="cl_block">
                                                        <div class="cl_name">
                                                            <?php
                                                            echo $key2->practice_affiliation;
                                                            ?>
                                                        </div>
                                                        <div class="doctor-address-distance">  	
                                                            <?php echo $key2->address; ?><br />
                                                            <?php
                                                            echo $key2->city;
                                                            echo ', ';
                                                            ?>
                                                            <?php echo $key2->state; ?>
                                                            <?php if ($key2->zip != '' && $key2->state != '') echo ' - '; ?>
                                                            <?php echo $key2->zip; ?><br />

                                                            <?php
                                                            /* if(isset($_REQUEST['radius']))
                                                              {
                                                              if($_REQUEST['radius']=='radius')
                                                              {
                                                              $ONE = $_REQUEST['geo_location_id'];
                                                              if($key2->zip)
                                                              {
                                                              $value = $this->radiusSearchVal($ONE, $key2->zip);

                                                              if(count($value)>0)
                                                              {
                                                              echo '<div style="font-weight: bold; font-size: 13px;">'.$value['distance']." Mile</div>";
                                                              }
                                                              }

                                                              }
                                                              } */
                                                            ?>
                                                            <div class="distance">

                                                                <?php
                                                                if (!empty($_REQUEST['geo_location_id'])) {
                                                                    if ($latlon[0]['tolat'] > 0) {

                                                                        $tolat = (double) $latlon[0]['tolat'];
                                                                        $tolng = (double) $latlon[0]['tolng'];
                                                                        $fmlat = (double) $key2->latitude;
                                                                        $fmlng = (double) $key2->longitude;


                                                                        $d = distance($tolat, $tolng, $fmlat, $fmlng);

                                                                        echo '<a href="javascript:void(0);"> Min ' . $d . ' Miles</a>';
                                                                        if ($d > 25) {
                                                                            $idval = "div" . $count_d;
                                                                            $notmatchdistance++;
                                                                            echo '<script> $("#' . $idval . '").addClass("disable"); </script> <style>.disable{display:none;} </style>';
                                                                        }
                                                                    }
                                                                }
                                                                ?>					  



                                                            </div>
                                                        </div>
                                                    </div>


                                                    
                                                    <div class="doctor-address-buttons"> 
                                                        
                                                        <?php if ($doctor_list[$i]['comments']) { ?>
                                                            <p><?php //echo $doctor_list[$i]['comments'];    ?></p>
                                                        <?php } ?>

                                                        <div class="clear">

                                                            <a id="pop_<?php echo $count_d; ?>" href="javascript:void(0);" class="bookprofile">Offers</a>
                                                            <a href="<?php echo $this->createAbsoluteUrl('doctor/' . $doctor_list[$i]->slug . '|' . $specialId); ?>" class="viewprof">Profile</a>
                                                            <a href="<?php echo $this->createAbsoluteUrl('doctor/' . $doctor_list[$i]->slug . '|' . $specialId); ?>#Reviews" class="commands">Reviews</a>

                                                            <div id="popup_<?php echo $count_d; ?>" style="display: none;" class="popup1">				

                                                            </div>
                                                            <script type="text/javascript">

                                                                $(function () {
                                                                    $('#pop_<?php echo $count_d; ?>').click(function () {
                                                                        $('#popup_<?php echo $count_d; ?>').bPopup({
                                                                            loadUrl: '<?php echo $this->createAbsoluteUrl("doctor/SpecialOffer?id=" . $doctor_list[$i]["id"] . "&a_id=" . $key2->id); ?>'
                                                                        });
                                                                    });
                                                                });

                                                            </script>


                                                            <!-- PopUP for Calender  -->
                                                            <div id="popup__time_<?php echo $count_d; ?>" style="display: none;" class="popup1 schedualPop">

                                                                <span class="button b-close"></span>
                                                                <div class="popup_container">
                                                                    <div class="clear">
                                                                        <div class="img_wrapper">

                                                                            <?php
                                                                            $filePath = 'assets/upload/doctor/' . $doctor_list[$i]->id . "/" . $doctor_list[$i]->image;
                                                                            if (@file_get_contents($filePath, 0, NULL, 0, 1)) {
                                                                                ?>
                                                                                <a href="javascript:void(0);"><?php echo CHtml::image(Yii::app()->request->baseUrl . '/assets/upload/doctor/' . $doctor_list[$i]->id . "/" . $doctor_list[$i]->image, "image", array()); ?></a>
                                                                            <?php } else { ?>
                                                                                <?php if ($doctor_list[$i]->gender == 'M') { ?>
                                                                                    <a href="javascript:void(0);"><?php echo CHtml::image(Yii::app()->request->baseUrl . '/assets/images/avatar.png', "image", array()); ?></a>
                                                                                <?php } else { ?>
                                                                                    <a href="javascript:void(0);"><?php echo CHtml::image(Yii::app()->request->baseUrl . '/assets/images/avatar.png', "image", array()); ?></a>
                                                                                <?php } ?>
                                                                            <?php } ?>
                                                                        </div>
                                                                        <div class="details_wrapper">
                                                                            <h2><a class="docname" href="<?php echo $this->createAbsoluteUrl('doctor/' . $doctor_list[$i]->slug . '|' . $specialId . '|' . $key2->id); ?>"><?php echo $doctor_list[$i]->first_name . " " . $doctor_list[$i]->last_name; ?></a></h2>
                                                                            <h4><?php echo $user_speciality[$specialId]; ?></h4>
                                                                            <p><?php echo $key2->address; ?><br />
                                                                                <?php
                                                                                echo $key2->city;
                                                                                echo ', ';
                                                                                ?>
                                                                                <?php echo $key2->state; ?>
                                                                                <?php if ($key2->zip != '' && $key2->state != '') echo ' - '; ?>
                                                                                <?php echo $key2->zip; ?></p> 
                                                                        </div>

                                                                        <div class="doctor-address-distance"> 
                                                                            <div class="cmt_block">
                                                                                <?php /* echo substr( $doctor_list[$i]->comments, 0, 75);
                                                                                  if(strlen( $doctor_list[$i]->comments )>75) {
                                                                                  ?>
                                                                                  <span><a href="<?php echo $this->createAbsoluteUrl('doctor/'.$doctor_list[$i]->slug.'|'.$specialId );?>"><img width="10" src="<?php echo Yii::app()->request->baseUrl; ?>/assets/images/sp_icon_left.png"></a></span>
                                                                                  <?php } */ ?>
                                                                            </div> 
                                                                        </div>

                                                                    </div>
                                                                    <div class="clear">
                                                                        <div class="table_schedule_wrapper">

                                                                        </div>
                                                                        <div class="more_schedual_area" style="display:none;" >

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <script type="text/javascript">
                                                                function openTimeSchedule(counter, specialId, docId, docAddId, procedureId) {
                                                                    $(".table_schedule_wrapper").html('<div align="center" style="font-size: 15px; font-family: Arial; font-weight: bold; color: #0b95a4;" >progresing...<img src="<?php echo Yii::app()->request->baseUrl; ?>/assets/images/loader.gif" /></div>');
                                                                    $.post("<?php echo Yii::app()->request->baseUrl; ?>/doctor/scheduleTable", {"index": counter, "docId": docId, "specialId": specialId, "docAddId": docAddId, "procedureId": procedureId},
                                                                    function (response) {
                                                                        $(".table_schedule_wrapper").html(response);

                                                                    });
                                                                    $('#popup__time_' + counter).bPopup();

                                                                }
                                                            </script>

                                                            <!-- ==========================Calender Popup End ===================================== -->

                                                        </div>
                                                    </div>
                                                </div>
                                                <div style="clear:both;"></div>  
                                                <div class="book-button-section">
                                                    <?php $procedure_id = isset($doctor_list[$i]->doctorProcedure->procedure_id) ? $doctor_list[$i]->doctorProcedure->procedure_id : ""; ?>
                                                    <?php if ($condition > 0) { ?>
                                                        <button type="button" style="cursor:pointer;" onclick="javascript: openTimeSchedule('<?php echo $count_d; ?>', '<?php echo $specialId; ?>', '<?php echo $doctor_list[$i]->id; ?>', '<?php echo $key2->id; ?>', '<?php echo $procedure_id; ?>');
                                                                " >Book Now</button> 
                                                            <?php } else { ?>
                                                        <!-- <button type="button" style="background:#FEFBFB;color:#787676;" disabled="disabled" >Book Now</button> -->
                                                    <?php } ?>	
                                                    <!-- <a  href="#"> May 28th 11.30 AM</a> <a href="#">2 PM</a> <a href="#">June 1st 9 AM</a> -->
                                                </div>
                                            </div>

                                            <?php
                                            $count_d++;
                                            ?>


                                            <?php
                                        }
                                    }
                                }
                            }

                            /*
                              $dd=($count_d-$notmatchdistance)-1;
                              if($dd>0)
                              {

                              echo "<script> $('#searchcounter').html('(".$dd.")') ;</script>";
                              } */
                            ?>
                        </div>

                    </div>
                </div>
            <p><BR><p>
            <div class="page-navigation-top">
                 <span id="searchcounter"></span><?php
                $this->widget('CLinkPager', array(
                    'pages' => $pages, 'header' => '', 'prevPageLabel' => '&lt;&lt;', 'nextPageLabel' => '&gt;&gt;',
                ))
                ?>

                <!--	<select name="sort" id="sort" onchange="callZip();"> <option value=""> select</option> <option value="zip" <?php
                if (!empty($_REQUEST['zip']) && $_REQUEST['zip'] == "zip") {
                    echo "selected='selected'";
                }
                ?>> zip</option></select> -->

            </div>
            
            <?php } else { ?>
                <div style="float:left; width:100%; margin-top: 0.5%;">
                    <h2 style="border:none;">eDoctorBook does not have any doctors matching the selected criteria. </h2>
                </div>
            <?php } ?>
        </div>




    </div>
</div>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/assets/js/jquery.nivo.slider.js"></script>
<script type="text/javascript">
                                    $(window).load(function () {
                                        $('#slider').nivoSlider();
                                    });
</script>
<script type="text/javascript">
    $(document).ready(function () {
        function add() {
            if ($(this).val() === '') {
                $(this).val($(this).attr('placeholder')).addClass('placeholder');
            }
        }

        function remove() {
            if ($(this).val() === $(this).attr('placeholder')) {
                $(this).val('').removeClass('placeholder');
            }
        }

        if (!('placeholder' in $('<input>')[0])) {
            $('input[placeholder], textarea[placeholder]').blur(add).focus(remove).each(add);
            $('form').submit(function () {
                $(this).find('input[placeholder], textarea[placeholder]').each(remove);
            });
        }
    });
</script>

<script src="<?php echo Yii::app()->request->baseUrl; ?>/assets/js/jquery.paginate.js" type="text/javascript"></script>
<script type="text/javascript">
    $(function () {
        $("#demo5").paginate({
            count: 10,
            start: 1,
            display: 7,
            border: true,
            border_color: '#fff',
            text_color: '#fff',
            background_color: '#03bf91',
            border_hover_color: '#ccc',
            text_hover_color: '#03bf91',
            background_hover_color: '#fff',
            images: false,
            mouse: 'press',
            onChange: function (page) {
                $('._current', '#paginationdemo').removeClass('_current').hide();
                $('#p' + page).addClass('_current').show();
            }
        });
    });
</script>

<script>

    function appointmentSchedule(type) {
        var today_date = $('#today_date').val();
        var view_date = $('#view_date').val();
        var speciality_id = $('#speciality_id_').val();
        var procedure = $('#procedure_id').val();
        var doctor_id = $('#doctor_id').val();
        var address_id = $('#address_id').val();
        var index_id_pop = $('#index_id_pop').val();

        if (type == 'prev' && today_date == view_date) {
            return false;
        } else {
            $('.schedule_loader').css('display', 'none');
            if (type == 'next') {
                $('#schedule_loader_next_' + address_id).show();
            } else {
                $('#schedule_loader_prev_' + address_id).show();
            }
            var sch_width = "14";
            $.post("<?php echo Yii::app()->request->baseUrl; ?>/doctor/appointmentSchedule",
                    {type: type, today_date: today_date, view_date: view_date, speciality_id: speciality_id, procedure_id: procedure, doctor_id: doctor_id, address_id: address_id, sch_width: sch_width, index_id_pop: index_id_pop},
            function (response) {

                var res = response.split("***%%%***");
                $('.calander_menu').html(res[0]);
                //$('.profilesec_timetable_leftcalnd').html(res[1]);
                $('.calander_box').html(res[1]);
                $('#view_date').val(res[2]);
                if (today_date == res[2])
                    $('.pre_arrow_area img').attr('src', '<?php echo Yii::app()->request->baseUrl; ?>/assets/images/pre_arrow_gry.png');
                else
                    $('.pre_arrow_area img').attr('src', '<?php echo Yii::app()->request->baseUrl; ?>/assets/images/pre_arrow_green.png');
                $('.schedule_loader').css('display', 'none');


            });
        }
    }

    function appointmentSchedule_bkp(type) {
        //alert(type);
        var date = new Date();
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();
        //alert(d);alert(m);alert(y);
        var isChecked = $('#scheduleOnly').is(':checked');
        if (isChecked == true) {
            var scheduleOnly = 1;
        } else {
            var scheduleOnly = 0
        }
        var today_date = $('#today_date').val();
        var view_date = $('#view_date').val();
        var name_autocomp = $('#name_autocomp').val();//alert(name_autocomp);
        var speciality_id = $('#speciality_id').val();//alert(speciality_id);
        var office_name = $('#office_name').val();//alert(office_name);
        var insurance = $('#insurance').val();//alert(insurance);
        var procedure = $('#procedure').val();//alert(procedure);
        var gender = $('#gender').val();//alert(gender);
        var language = $('#language').val();//alert(geo_location_id);
        var geo_location_id = $('#geo_location_id').val();//alert(geo_location_id);
        var page = $('#page').val();//alert(page);
        if (type == 'prev' && today_date == view_date) {
            //$('.pre_arrow_area img').attr('src','/doctorappointments/assets/images/pre_arrow_gry.png');
            return false;
        } else {
            //$('#schedule_body').css('display','block');
            $('.schedule_loader').css('display', 'none');
            if (type == 'next') {
                $('#schedule_loader_next').css('display', 'block');
            } else {
                $('#schedule_loader_prev').css('display', 'block');
            }
            $.post("<?php echo Yii::app()->request->baseUrl; ?>/doctor/appointmentSchedule",
                    {type: type, today_date: today_date, view_date: view_date, name_autocomp: name_autocomp, speciality_id: speciality_id, office_name: office_name, insurance: insurance, procedure: procedure, gender: gender, language: language, geo_location_id: geo_location_id, page: page, scheduleOnly: scheduleOnly},
            function (response) {
                //alert(response);
                var res = response.split("***%%%***");
                $('.calander_menu').html(res[0]);
                $('.profilesec_timetable_leftcalnd').html(res[1]);
                $('#view_date').val(res[2]);
                if (today_date == res[2])
                    $('.pre_arrow_area img').attr('src', '<?php echo Yii::app()->request->baseUrl; ?>/assets/images/pre_arrow_gry.png');
                else
                    $('.pre_arrow_area img').attr('src', '<?php echo Yii::app()->request->baseUrl; ?>/assets/images/pre_arrow_green.png');
                //$('#schedule_body').css('display','none');
                $('.schedule_loader').css('display', 'none');
            });
        }
    }
</script>
<script>
    function profileAppList(today_date, parm_bar) {
        //alert('ok');	
        var parm_arr = parm_bar.split("|");
        var address_id = parm_arr[0];
        var speciality_id = parm_arr[1];//alert(speciality_id);
        var procedure = parm_arr[2];//alert(procedure);
        var doctor_id = parm_arr[3];
        var countPopId = parm_arr[4];
        //var view_date = $('#view_date').val();
        var view_date = today_date;

        $.post("<?php echo Yii::app()->request->baseUrl; ?>/doctor/appointmentScheduleProfileDay",
                {today_date: today_date, view_date: view_date, speciality_id: speciality_id, procedure_id: procedure, doctor_id: doctor_id, address_id: address_id, countPopId: countPopId},
        function (response) {
            //alert(response);
            var res = response.split("***%%%***");
            //$('#app_popup_heading').html(res[0]);
            var content = '<div id="popup_content">';
            content += '<h4 id="app_popup_heading">' + res[0] + '</h4>';
            content += '<p><div class="calander_box_lisiting_popup" >' + res[1] + '</div></p>';
            content += '</div>';
            //$('.more_schedual_area').html(content);
            //$(".table_schedule_wrapper").html(content);
            $(".table_schedule_wrapper").hide();
            $('.more_schedual_area').html(content);
            $('.more_schedual_area').show();

        });
    }

    function openSchedual() {

        $(".table_schedule_wrapper").show();
        $('.more_schedual_area').hide();
    }

    function scheduleOnlyCall()
    {
        var fullUrl = $('#fullUrl').val();
        var isChecked = $('#scheduleOnly').is(':checked');
        if (isChecked == true)
        {
            window.location = fullUrl + '&schedualOnly=1';
        }
        else if (isChecked == false)
        {
            window.location = fullUrl;
        }
    }

</script>
<style>
    .calander_box_lisiting_popup{ background:#fff; float:left; width:100%; /*min-height:209px;*/ border:1px solid #CCCCCC; margin-top:5px;
                                  box-shadow:0 2px 1px 0 #DEDEDE;  -moz-box-shadow: 0 2px 1px 0 #DEDEDE; -webkit-box-shadow: 0 2px 1px 0 #DEDEDE;}
    .calander_box_lisiting_popup .innertable_time_popup{ border-right:1px solid #ccc; border-bottom:1px solid #ccc; display:block; float:left; width:24.7%;}
    .calander_box_lisiting_popup .innertable_time_popup a{ display:block; color:#1bc1bd; font-size:16px; font-weight:bold; line-height:43.5px; height:43.5px;  text-align:center; }
    .calander_box_lisiting_popup .innertable_time_popup a.blank{ cursor:default;}
    .calander_box_lisiting_popup .innertable_time_popup a.blank:hover{ background:none;}
    .calander_box_lisiting_popup .innertable_time_popup a:hover{ background:#1fd3fa; color:#fff;}


    .calander_box_lisiting_popup .innertable_time_popup_first{ border-right:1px solid #ccc; border-bottom:1px solid #ccc; display:block; float:left; width:20%;}
    .calander_box_lisiting_popup .innertable_time_popup_first a{ display:block; color:#1bc1bd; font-size:16px; font-weight:bold; line-height:43.5px; height:43.5px;  text-align:center; }
    .calander_box_lisiting_popup .innertable_time_popup_first a.blank{ cursor:default;}
    .calander_box_lisiting_popup .innertable_time_popup_first a.blank:hover{ background:none;}
    .calander_box_lisiting_popup .innertable_time_popup_first a:hover{ background:#1fd3fa; color:#fff;}
    .address_list{ width:100%; float:left; margin-bottom:30px;}
    .address_list h4{ color:#727272; font-size:13px; font-weight:bold; padding-bottom:2px; float:left; display:block; width:100%; margin:0px; line-height:30px;}
</style>
<div id="toPopup" style="z-index: 999;"> 
    <div class="close_view"></div> 
    <div id="popup_content">
        <h4 id="app_popup_heading"></h4>
        <p>
        <div class="calander_box_lisiting_popup" id="popup_content_val"></div>
        </p>
    </div>
</div>
<input type="hidden" id="today_date" value="<?php echo date('Y-m-d'); ?>" />
<input type="hidden" id="view_date" value="<?php echo date('Y-m-d'); ?>" />
<div style="position:fixed; width:100%; height:100%; background:#fff; opacity:0.8; display:none;" id="schedule_body">
</div>


<script>
    $(document).ready(function () {
        var docid = '<?php
            if (!empty($docid)) {
                echo $docid;
            } else {
                echo "0";
            }
            ?>';
        $.post("<?php echo Yii::app()->request->baseUrl; ?>/doctor/getAllInsurance",
                function (response) {
                    var insurance = '<?php
            if (!empty($usercriteria['insurance'])) {
                echo $usercriteria['insurance'];
            } else {
                echo "0";
            }
            ?>';


                    if ($("select").hasClass("insurance_cls")) {
                        var arr = jQuery.parseJSON(response);
                        $.each(arr, function (key, value) {
                            var sel = ""
                            if (key == insurance) {
                                sel = "selected='selected'";
                            }

                            $(".insurance_cls").append('<option value="' + key + '" ' + sel + '>' + value + '</option>');

                        });
                    }
                });

        $.post("<?php echo Yii::app()->request->baseUrl; ?>/doctor/getAllLanguage",
                function (response) {
                    //   var language='<?php
            if (!empty($usercriteria['language'])) {
                echo $usercriteria['language'];
            } else {
                echo "0";
            }
            ?>';

                    if ($("select").hasClass("language_cls")) {
                        var arr = jQuery.parseJSON(response);
                        var sel = ""
                        //  if(key==language){sel=" selected='selected'";} 	
                        $.each(arr, function (key, value) {
                            $(".language_cls").append('<option value="' + key + '">' + value + '</option>');

                        });
                    }
                });

        $.post("<?php echo Yii::app()->request->baseUrl; ?>/doctor/getAllProcedure",
                function (response) {
                    if ($("select").hasClass("procedure_cls")) {
                        var arr = jQuery.parseJSON(response);
                        $.each(arr, function (key, value) {
                            $(".procedure_cls").append('<option value="' + key + '">' + value + '</option>');
                        });
                    }

                });


        $.post("<?php echo Yii::app()->request->baseUrl; ?>/doctor/getAllReasonVisit",
                function (response) {
                    if ($("select").hasClass("reason_visit_cls")) {
                        var arr = jQuery.parseJSON(response);
                        $.each(arr, function (key, value) {
                            $(".reason_visit_cls").append('<option value="' + key + '">' + value + '</option>');
                        });
                    }
                });


    });






</script>
<!-- Equal Height Js-->
<script>
    equalheight = function (container) {

        var currentTallest = 0,
                currentRowStart = 0,
                rowDivs = new Array(),
                $el,
                topPosition = 0;
        $(container).each(function () {

            $el = $(this);
            $($el).height('auto')
            topPostion = $el.position().top;

            if (currentRowStart != topPostion) {
                for (currentDiv = 0; currentDiv < rowDivs.length; currentDiv++) {
                    rowDivs[currentDiv].height(currentTallest);
                }
                rowDivs.length = 0; // empty the array
                currentRowStart = topPostion;
                currentTallest = $el.height();
                rowDivs.push($el);
            } else {
                rowDivs.push($el);
                currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
            }
            for (currentDiv = 0; currentDiv < rowDivs.length; currentDiv++) {
                rowDivs[currentDiv].height(currentTallest);
            }
        });
    }

    $(window).load(function () {
        equalheight('.doctor-address-distance');
        equalheight('.cl_block');
    });


    $(window).resize(function () {
        equalheight('.doctor-address-distance');
        equalheight('.cl_block');
    });

    function callSearchSubmit()
    {

        var speciality_id = $("#speciality_id").val();
        var geo_location_id = $("#geo_location_id").val();
        var name = $("#name_autocomp").val();


        if (name == "" && speciality_id == "" && geo_location_id == "")
        {
            alert("Enter  Name , speciality or zip ");
            return false;
        }
        /* if(name=="" && speciality_id=="" && geo_location_id=="")
         { alert("Enter Choose Name or speciality and zip combination");  return false;}	
         else if(name=="" && (speciality_id=="" || geo_location_id==""))
         {  alert("Enter Choose speciality and zip combination");;  return false;}	*/

        return true;
    }
    $("#geo_location_id").blur(function () {
        var zip = $(this).val();
<?php /*
  if(zip!=""){
  $.post("<?php echo Yii::app()->request->baseUrl; ?>/doctor/checkZip", {"zip":zip},
  function(response) {
  if(response==0){
  alert('Invalid Zip Code');
  $("#geo_location_id").val('');
  }
  });

  }
 */ ?>
    });

</script>


<?php

function distance($lat1, $lon1, $lat2, $lon2) {
    $theta = $lon1 - $lon2;
    $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
    $dist = acos($dist);
    $dist = rad2deg($dist);
    $miles = $dist * 60 * 1.1515;
    return floor($miles);
}
?>


<script>
    function callZip() {
        var str = $("#sort").val();
        if (str == "zip")
        {
            $('input[name="zip"]').val(str);
        }
        else if (str == "")
        {
            $('input[name="zip"]').val('');
        }
        $("#search_form").submit();
        $('input[type="submit"]').click();

    }


</script>
<style>
    #sort{width:10%;float:right;margin-right:10px;padding:3px 0px;border:1px solid #96EFEF;text-transform: capitalize;}
    .fixeddiv span.rfine_search_last a img{  vertical-align: middle;
                                             margin-left: 21px;margin-right: 12px;}
</style>