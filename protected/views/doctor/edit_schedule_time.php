<?php
/* @var $this DoctorController */
/* @var $model Doctor */

$this->breadcrumbs=array(
	'Dashboard'=>array('index'),
	'Edit Profile',
);

/*$this->menu=array(
	array('label'=>'List Doctor', 'url'=>array('index')),
	array('label'=>'Manage Doctor', 'url'=>array('admin')),
);*/
?>

<!--<h1>Create Doctor</h1>-->

<?php //$this->renderPartial('_form', array('model'=>$model)); ?>

<div class="main">
    <div id="breadcrumb" class="fk-lbreadbcrumb newvd">
        <!--<span><a href="">Home</a></span> >  
        <span>Dashboard</span>--> 
        <?php $this->widget('zii.widgets.CBreadcrumbs', array(
				  'links'=>$this->breadcrumbs,
			  ));
		?>
    </div>
  	  <div class="dashboard_mainarea">
     	<div class="leftmenu">
        	 
       		 <?php /*?><h2>Doctor control panel</h2>
             <ul>
            	 <li><?php echo CHtml::link('Dashboard', $this->createAbsoluteUrl('index')); ?></li>
                 <li><?php echo CHtml::link('Edit My Account', $this->createAbsoluteUrl('doctor/editProfile/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li><?php echo CHtml::link('Special Offers', $this->createAbsoluteUrl('doctor/offers/'.Yii::app()->session['logged_user_id'])); ?></li><?php */?>
                 <?php /*?><li><?php echo CHtml::link('My Addresses', $this->createAbsoluteUrl('doctor/address/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li><?php echo CHtml::link('My Specialities', $this->createAbsoluteUrl('doctor/speciatlity/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li><?php echo CHtml::link('View Profile', $this->createAbsoluteUrl('doctor/profile/'.Yii::app()->session['logged_user_id'])); ?></li><?php */?>
                <?php /*?> <li><a href="#">Appointments</a></li>
                 <li class="active"><?php echo CHtml::link('Schedules', $this->createAbsoluteUrl('doctor/schedule/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li><?php echo CHtml::link('Timeoff', $this->createAbsoluteUrl('doctor/timeoff/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li><?php echo CHtml::link('Todo List', $this->createAbsoluteUrl('doctor/todolist/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li><?php echo CHtml::link('Patients', $this->createAbsoluteUrl('doctor/patient/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li><?php echo CHtml::link('Setting Tab', $this->createAbsoluteUrl('doctor/settingTab/'.Yii::app()->session['logged_user_id'])); ?></li>
             </ul><?php */?>
             <?php $this->renderPartial('//layouts/navigation'); ?>
        </div>
        <div class="rightarea_dashboard">
          <div class="dashboard_content1">
          	<?php if(Yii::app()->user->hasFlash('editSchedulesTime')): ?>
            <span class="flash-success">
                <?php echo Yii::app()->user->getFlash('editSchedulesTime'); ?>
            </span>
        <?php endif; ?>
          	<h1 class="h1"><?php echo $model->isNewRecord ? 'Insert' : 'Update'; ?> Your Schedule Time</h1>
             <?php $form=$this->beginWidget('CActiveForm', array(
				'id'=>'edit_address',
			)); ?>
            <input type="hidden" name="doctor_schedule_data_id" value="<?php echo $doctor_schedule_data_id; ?>" />
            	<span>
					<?php echo $form->labelEx($model,'address_id',array('class'=>'fld_name')); ?>
                    <div class="name_fld">
                    <?php
                          $selected_address = $model->address_id;
                          echo CHtml::dropDownList('address_id', $selected_address, 
                          $doctor_address,
                          array('empty' => 'Select your address','class'=>'fld_class'));
                    ?>
                    <?php echo $form->error($model,'address_id'); ?>
                    </div>
                </span>
                <span>
                	<?php echo $form->labelEx($model,'day',array('class'=>'fld_name')); ?>
                    <div class="name_fld">
					<?php
                          $selected_day = $model->day;
                          echo CHtml::dropDownList('day', $selected_day, 
                          $model->dayOptions,
                          array('empty' => 'Select your day','class'=>'fld_class'));
                    ?>
					<?php echo $form->error($model,'day'); ?>
                    </div>
                </span>
                <span>
                	<?php echo $form->labelEx($model,'from_time',array('class'=>'fld_name')); ?>
                    <div class="name_fld">
					
                    <div class="select_option">
                    <?php
                          $selected_time_from_nc_hour = $from_time['hh'];
                          echo CHtml::dropDownList('time_from_nc_hour', $selected_time_from_nc_hour, 
                          $model->timeHourOptions,
                          array(/*'empty' => 'Select From Time Format',*/'style'=>'width: 124px;','onchange'=>"timeFun('hr','from',this.value);"));
                    ?>
                    </div>
                    <div class="select_option2">
					<?php
                          $selected_time_from_nc_minute = $from_time['mm'];
                          echo CHtml::dropDownList('time_from_nc_minute', $selected_time_from_nc_minute, 
                          $model->timeMinuteOptions,
                          array(/*'empty' => 'Select From Time Format',*/'style'=>'width: 103px;','onchange'=>"timeFun('min','from',this.value);"));
                    ?>
                    </div>
                    
                    <?php
					if($model->from_time)
						$from_time_value = $model->from_time;
					else
						$from_time_value = '01:00:00';
						
					echo $form->hiddenField($model,'from_time',array('value'=>"$from_time_value"));
					?>
                    <?php echo $form->error($model,'from_time'); ?>
                    <div class="select_option3">
                    <?php
                          $selected_from_time_format = $model->from_time_format;
                          echo CHtml::dropDownList('from_time_format', $selected_from_time_format, 
                          $model->timeFormatOptions,
                          array(/*'empty' => 'Select From Time Format',*/'style'=>'width: 156px;'));
                    ?>
                    <?php echo $form->error($model,'from_time_format'); ?>
                    </div>
                    <div class="clear"></div>
                    </div>
                </span>
                
                <span>
                	<?php echo $form->labelEx($model,'to_time',array('class'=>'fld_name')); ?>
                    <div class="name_fld">
					<div class="select_option">
                    <?php
                          $selected_time_to_nc_hour = $to_time['hh'];
                          echo CHtml::dropDownList('time_from_nc_hour', $selected_time_to_nc_hour, 
                          $model->timeHourOptions,
                          array(/*'empty' => 'Select From Time Format',*/'style'=>'width: 124px;','onchange'=>"timeFun('hr','to',this.value);"));
                    ?>
                    </div>
                    <div class="select_option2">
					<?php
                          $selected_time_to_nc_minute = $to_time['mm'];
                          echo CHtml::dropDownList('time_from_nc_minute', $selected_time_to_nc_minute, 
                          $model->timeMinuteOptions,
                          array(/*'empty' => 'Select From Time Format',*/'style'=>'width: 103px;','onchange'=>"timeFun('min','to',this.value);"));
                    ?>
                    </div>
                    <?php
					if($model->to_time)
						$to_time_value = $model->to_time;
					else
						$to_time_value = '01:00:00';
						
					echo $form->hiddenField($model,'to_time',array('value'=>"$to_time_value")); ?>
                    <?php echo $form->error($model,'to_time'); ?>
                    <div class="select_option3">
                    <?php
                          $selected_to_time_format = $model->to_time_format;
                          echo CHtml::dropDownList('to_time_format', $selected_to_time_format, 
                          $model->timeFormatOptions,
                          array(/*'empty' => 'Select From Time Format',*/'style'=>'width: 156px;'));
                    ?>
                    </div>
                    <?php echo $form->error($model,'to_time_format'); ?>
                    </div>
                </span>
                <?php /*?><span>
                	<?php echo $form->labelEx($model,'to_time'); ?>
                    <div class="name_fld">
					<?php echo $form->textField($model,'to_time',array('size'=>32,'maxlength'=>32,'placeholder'=>'Valid From Date','class'=>'fld_class datepicker readonly','readonly'=>'readonly')); ?>
					<?php echo $form->error($model,'to_time'); ?>
                    </div>
                </span><?php */?>
                
                <span>
                	<?php echo $form->labelEx($model,'time_slot',array('class'=>'fld_name')); ?>
                    <div class="name_fld">
					<?php
                          $selected_time_slot = $model->time_slot;
                          echo CHtml::dropDownList('time_slot', $selected_time_slot, 
                          $model->timeSlotOptions,
                          array('empty' => 'Select your Time Slot','class'=>'fld_class'));
                    ?>
					<?php echo $form->error($model,'time_slot'); ?>
                    </div>
                </span>
                <span>
                	<?php echo $form->labelEx($model,'laser_slot',array('class'=>'fld_name')); ?>
                    <div class="name_fld">
					<?php
                          $selected_laser_slot = $model->laser_slot;
                          echo CHtml::dropDownList('laser_slot', $selected_laser_slot, 
                          $model->laserSlotOptions,
                          array('empty' => 'Select your Laser Slot','class'=>'fld_class'));
                    ?>
					<?php echo $form->error($model,'laser_slot'); ?>
                    </div>
                </span>
                
                <span>
                <?php echo CHtml::submitButton($model->isNewRecord ? 'Save' : 'Update',array('class'=>'grn_btn')); ?>
                </span>
            <?php $this->endWidget(); ?>
          </div>
        </div> 
      </div>
</div>
<script>
function timeFun(hr,type,value){
	//$('#DoctorTimeoff_from_time').val();
	var doc_time = $('#DoctorScheduleTime_'+type+'_time').val();
	var doc_time_arr = doc_time.split(":");
	//alert(doc_time_arr);
	var final_time = "";
	if(hr == 'hr'){
		final_time = value+":"+doc_time_arr[1]+":"+doc_time_arr[2];
	}else{
		final_time = doc_time_arr[0]+":"+value+":"+doc_time_arr[2];
	}
	$('#DoctorScheduleTime_'+type+'_time').val(final_time);
}
</script>