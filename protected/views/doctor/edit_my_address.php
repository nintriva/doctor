       	<span class="ecs_tooltip">Press Esc to close <span class="arrow"></span></span>
        <div class="add_popup_area">
        <?php if(Yii::app()->user->hasFlash('editProfile')): ?>
            <span class="flash-success">
                <?php echo Yii::app()->user->getFlash('editProfile'); ?>
            </span>
        <?php endif; ?>
          	<?php $form=$this->beginWidget('CActiveForm', array(
				'id'=>'edit_address',
			)); ?>
		<div id="popup_content" >
        	<div class="dashboardcont_leftbox">
                            <h1>Office Address</h1>
                            <div class="box_content1">
        
                                    <div class="fld_area">
                                        <div class="fld_name fld_name_hight"><?php echo $form->labelEx($model,'address',array()); ?></div>
                                        <div class="name_fld">
                                        <?php echo $form->textArea($model,'address',array('size'=>32,'maxlength'=>255,'placeholder'=> 'Address','class'=>'txtarea_class')); ?>
										<?php echo $form->error($model,'address'); ?>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                   
                                    <div class="fld_area">
                                        <div class="fld_name fld_name_hight"><?php echo $form->labelEx($model,'city',array()); ?></div>
                                        <div class="name_fld">
                                        <?php echo $form->textField($model,'city',array('size'=>32,'maxlength'=>125,'placeholder'=>'City','class'=>'fld_class')); ?>
                    					<?php echo $form->error($model,'city'); ?>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="fld_area">
                                        <div class="fld_name fld_name_hight"><?php echo $form->labelEx($model,'state',array()); ?></div>
                                        <div class="name_fld">
                                        	<?php
												  $selected_state = $model->state;
												  echo CHtml::dropDownList('state', $selected_state, 
												  $data_city,
												  array('class'=>'fld_class2'));
											?>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="fld_area">
                                        <div class="fld_name fld_name_hight"><?php echo $form->labelEx($model,'zip',array()); ?></div>
                                        <div class="name_fld">
                                        <?php echo $form->textField($model,'zip',array('size'=>32,'maxlength'=>15,'placeholder'=>'Zip Code','class'=>'fld_class')); ?>
                    					<?php echo $form->error($model,'zip'); ?>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="fld_area">
                                        <div class="fld_name fld_name_hight"><?php echo $form->labelEx($model,'practice_affiliation',array()); ?></div>
                                        <div class="name_fld">
                                        <?php echo $form->textField($model,'practice_affiliation',array('size'=>32,'maxlength'=>125,'placeholder'=>'Practice','class'=>'fld_class')); ?>
                    					<?php echo $form->error($model,'practice_affiliation'); ?>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="fld_area">
                                        <div class="fld_name fld_name_hight"><?php echo $form->labelEx($model,'office_name',array()); ?></div>
                                        <div class="name_fld">
                                        <?php echo $form->textField($model,'office_name',array('size'=>32,'maxlength'=>25,'placeholder'=>'Office Name','class'=>'fld_class')); ?>
                    					<?php echo $form->error($model,'office_name'); ?>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="fld_area">
                                        <div class="fld_name fld_name_hight"><?php echo $form->labelEx($model,'office_phone',array()); ?></div>
                                        <div class="name_fld">
                                        <?php echo $form->textField($model,'office_phone',array('size'=>32,'maxlength'=>25,'placeholder'=>'Office Phone','class'=>'fld_class')); ?>
                    					<?php echo $form->error($model,'office_phone'); ?>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="fld_area">
                                        <div class="fld_name fld_name_hight"><?php echo $form->labelEx($model,'office_fax',array()); ?></div>
                                        <div class="name_fld">
                                        <?php echo $form->textField($model,'office_fax',array('size'=>32,'maxlength'=>25,'placeholder'=>'Office Fax','class'=>'fld_class')); ?>
                    					<?php echo $form->error($model,'office_fax'); ?>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="fld_area">
                                        <div class="fld_name fld_name_hight"></div>
                                        <div class="name_fld">
                                        <?php echo $form->checkBox($model,'default_status',array('checked'=>'0')); ?>
                                        <?php echo $form->labelEx($model,'default_status',array()); ?>
                    					<?php echo $form->error($model,'default_status'); ?>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                            </div>
                        </div>
                        <div><span>
                        <?php echo CHtml::Button($model->isNewRecord ? 'Save' : 'Update',array('class'=>'grn_btn','onclick'=>'return addressSubmit(\''.$model->id.'\');')); ?>
                        <?php echo CHtml::Button('Cancel',array('class'=>'registbt cancel_button_scp')); ?>
                        </span></div>
        </div>
        <?php $this->endWidget(); ?>
        </div> 