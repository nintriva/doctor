<?php
$this->breadcrumbs=array(
	'Dashboard'=>array('index'),
	'Edit Todo List',
);
?>

<div class="main">
    <div id="breadcrumb" class="fk-lbreadbcrumb newvd">
        <!--<span><a href="">Home</a></span> >  
        <span>Dashboard</span>--> 
        <?php $this->widget('zii.widgets.CBreadcrumbs', array(
				  'links'=>$this->breadcrumbs,
			  ));
		?>
    </div>
  	  <div class="dashboard_mainarea">
     	<div class="leftmenu">
       		 <?php /*?><h2>Doctor control panel</h2>
             <ul>
            	 <li><?php echo CHtml::link('Dashboard', $this->createAbsoluteUrl('index')); ?></li>
                 <li><?php echo CHtml::link('My Account', $this->createAbsoluteUrl('doctor/editProfile/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li><?php echo CHtml::link('Special Offers', $this->createAbsoluteUrl('doctor/offers/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li><?php echo CHtml::link('Appointments', $this->createAbsoluteUrl('doctor/appointment/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li><?php echo CHtml::link('Schedules', $this->createAbsoluteUrl('doctor/schedule/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li><?php echo CHtml::link('Timeoff', $this->createAbsoluteUrl('doctor/timeoff/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li class="active"><?php echo CHtml::link('Todo List', $this->createAbsoluteUrl('doctor/todolist/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li><?php echo CHtml::link('Patients', $this->createAbsoluteUrl('doctor/patient/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li><?php echo CHtml::link('Setting Tab', $this->createAbsoluteUrl('doctor/settingTab/'.Yii::app()->session['logged_user_id'])); ?></li>
             </ul><?php */?>
             <?php $this->renderPartial('//layouts/navigation'); ?>
        </div>
        
        <div class="rightarea_dashboard">
        	<?php $form=$this->beginWidget('CActiveForm', array(
				'id'=>'edit_offers',
			)); ?>
            	<div class="dashboardcont_leftbox">
					<?php if(Yii::app()->user->hasFlash('editOffers')): ?>
                        <span class="flash-success">
                            <?php echo Yii::app()->user->getFlash('editOffers'); ?>
                        </span>
                    <?php endif; ?>
                     <h1><?php echo $model->isNewRecord ? 'Add' : 'Edit'; ?> Todo List</h1>
                     <div class="box_content">
        				<div class="fld_area">
                           <div class="fld_name"><?php echo $form->labelEx($model,'name'); ?></div>
                           <div class="name_fld_new"><?php echo $form->textField($model,'name',array('size'=>32,'maxlength'=>255,'placeholder'=> 'Name','class'=>'fld_class')); ?>
					<?php echo $form->error($model,'name'); ?></div>
                           <div class="clear"></div>             
                        </div>
                        <div class="fld_area">
                           <div class="fld_name fld_name_hight"><?php echo $form->labelEx($model,'description'); ?></div>
                           <div class="name_fld_new"><?php echo $form->textArea($model,'description',array('size'=>32,'maxlength'=>255,'placeholder'=> 'Description','class'=>'txtarea_class')); ?>
					<?php echo $form->error($model,'description'); ?></div>
                           <div class="clear"></div>
                        </div>
                        <div class="fld_area">
                           <div class="fld_name"><?php echo $form->labelEx($model,'priority'); ?></div>
                           <div class="name_fld_new">
						   <?php //echo $form->textField($model,'priority',array('size'=>32,'maxlength'=>2,'placeholder'=> 'Priority','class'=>'fld_class')); ?>
                           <?php
								  $doctor_priority =array('Low'=>'Low','Medium'=>'Medium','High'=>'High');
								  $selected_priority = $model->priority;
								  echo CHtml::dropDownList('TodoList[priority]', $selected_priority, 
								  $doctor_priority,
								  array('class'=>''));
							?>
					<?php echo $form->error($model,'priority'); ?></div>
                           <div class="clear"></div>             
                        </div>
                        <div class="fld_area">
							<div class="fld_name"><label class="fld_name" for="TodoList_deadline">Due Date</label></div>
                            <div class="name_fld_new">
                            <?php echo $form->textField($model,'deadline',array('size'=>32,'maxlength'=>32,'placeholder'=>'Due Date','class'=>'fld_class datepicker readonly','readonly'=>'readonly')); ?>
                            <?php /*?><input id="TodoList_deadline" class="fld_class datepicker readonly" type="text" value="<?php echo $from_date; ?>" name="deadline" readonly="readonly" placeholder="" maxlength="32" size="32"><?php */?>
                            <?php //echo $form->error($model,'deadline'); ?>
                            </div>
                            <div class="clear"></div> 
                        </div>
                        <div class="fld_area">
                           <div class="fld_name">Status</div>
                           <div class="name_fld_new">
						   <?php //echo $form->textField($model,'priority',array('size'=>32,'maxlength'=>2,'placeholder'=> 'Priority','class'=>'fld_class')); ?>
                           <?php
								  $doctor_todolist_status =array('Not Started'=>'Not Started','Pending'=>'Pending','On Hold'=>'On Hold','Complete'=>'Complete');
								  $selected_todolist_status = $model->todolist_status;
								  echo CHtml::dropDownList('todolist_status', $selected_todolist_status, 
								  $doctor_todolist_status,
								  array('class'=>''));
							?>
                           </div>
                           <div class="clear"></div>             
                        </div>
                   <?php /*?><div class="fld_area">
                	<label class="fld_name">Deadline Time</label>
                    <div class="name_fld h_m_f">
                    		<div class="select_option">
							<?php
                                  $selected_time_from_nc_hour = $from_time['hh'];
                                  echo CHtml::dropDownList('time_from_nc_hour', $selected_time_from_nc_hour, 
                                  $model->timeHourOptions,
                                  array('style'=>'width: 124px;','onchange'=>"timeFun('hr','from',this.value);"));
                            ?>
                            </div>
                            <div class="select_option2">
                            <?php
                                  $selected_time_from_nc_minute = $from_time['mm'];
                                  echo CHtml::dropDownList('time_from_nc_minute', $selected_time_from_nc_minute, 
                                  $model->timeMinuteOptions,
                                  array('style'=>'width: 103px;','onchange'=>"timeFun('min','from',this.value);"));
                            ?>
                            </div>
                            <?php
                            $from_time_value = '01:00:00';
							?>
                            <div class="select_option3">
                            <?php
                                  $selected_from_time_format = '';
								  $timeFormatOptions = '';
                                  echo CHtml::dropDownList('from_time_format', $from_format, 
                                  $model->timeFormatOptions,
                                  array('style'=>'width: 156px;'));
                            ?>
                            </div>
                            <div class="clear"></div>
                           </div>
                        </div><?php */?>
                     </div>
                </div>
                <div><span>
                <?php echo CHtml::submitButton($model->isNewRecord ? 'Add' : 'Update',array('class'=>'registbt')); ?>
                <?php echo CHtml::link('Cancel', $this->createAbsoluteUrl('doctor/todolist/'.Yii::app()->session['logged_user_id']),array('class'=>'registbt')); ?>
                </span></div>
            <?php $this->endWidget(); ?>
		</div>
         
      </div>
</div>
<script>
$(function(){
	$('#edit_offers input, #edit_offers textarea').keyup(function(){
		$(this).removeClass('error');
		$(this).parent('div.name_fld_new').children('div.errorMessage').hide();
	});
	$('#edit_offers input').change(function(){
		$(this).removeClass('error');
		$(this).parent('div.name_fld_new').children('div.errorMessage').hide();
	});
});
</script>