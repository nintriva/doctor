<div class="main" >
  	<div class="regis_contentarea" >
		<div class="leftpanel">
      	 <form id="registration_success" name="registration_success" method="post" action="<?php echo Yii::app()->request->baseUrl.'/doctor/JoinMail'; ?>">
        <span>
        <label>Verify your Phone number</label>
        We will send a PIN to this number right away
        <input type="text" class="mrg_top" name="verify_phone" id="verify_phone" placeholder="e.g (877) 951-3062" value="">
        </span> 
        <span>
       	 	<input type="button" value="Text Me" class="registbt" onclick="twilioPin();">
           
        </span>
        <div class="verify_box">
           <h3>PIN</h3>
            Please enter the 4-digit PIN number you received on the phone# you provided above. If you did not receive the text message <br>
			<a href="javascript:void(0);" id="pin_again" onclick="twilioPinAgain();">please try again.</a>
            <span class="mrg_top"><input class="small" type="text" name="verify_pin" id="verify_pin" placeholder="Put 4 digit PIN number">
            <input type="button" value="Verify" class="registbt" onclick="twilioPinVerification();">
           
            </span>   
        </div> 
      </form>
    </div>
    </div>
</div>

<script>
  function twilioPin(){
		var verify_phone = $.trim($('#verify_phone').val());
		if(verify_phone!=''){
		var jqXHR=$.ajax({
				type: "POST",
				url: "<?php echo Yii::app()->request->baseUrl; ?>/doctor/twilioDoctorRegistration",
				data: { verify_phone: verify_phone },
				async: false,
				success:function(result){
					//alert(result);
					var obj= $.parseJSON(result);
					//alert(obj.uri);
					//alert(obj.sid);
					if(obj.sid!=''){
						$('#verify_phone').attr('readonly','readonly');
						alert('Pin send to your mobile.Please Check.');
					}else{
						$('#verify_phone').addClass('field_required');
						$('#verify_phone').attr('title','Phone number or format is wrong.');
						$('input').keyup(function() {
							$(this).removeClass('field_required');
							$(this).removeAttr('title');
						});
					}
				}
			});
		}else{
			//alert('Phone number required');	
			$('#verify_phone').addClass('field_required');
			$('#verify_phone').attr('title','Phone number required.');
			$('input').keyup(function() {
				$(this).removeClass('field_required');
				$(this).removeAttr('title');
			});
		}
  }
  function twilioPinAgain(){
		var verify_phone = $.trim($('#verify_phone').val());
		if(verify_phone!=''){
		var jqXHR=$.ajax({
				type: "POST",
				url: "<?php echo Yii::app()->request->baseUrl; ?>/doctor/twilioDoctorRegistration",
				data: { verify_phone: verify_phone },
				async: false,
				success:function(result){
					//alert(result);
					var obj= $.parseJSON(result);
					//alert(obj.uri);
					//alert(obj.sid);
					if(obj.sid!=''){
						$('#pin_again').hide();
						alert('Pin send to your mobile.Please Check.');
					}else{
						$('#verify_phone').addClass('field_required');
						$('#verify_phone').attr('title','Phone number or format is wrong. or DND.');
						$('input').keyup(function() {
							$(this).removeClass('field_required');
							$(this).removeAttr('title');
						});
					}
				}
			});
		}else{
			//alert('Phone number required');	
			$('#verify_phone').addClass('field_required');
			$('#verify_phone').attr('title','Phone number required.');
			$('input').keyup(function() {
				$(this).removeClass('field_required');
				$(this).removeAttr('title');
			});
		}
  }
  function twilioPinVerification(){
		var verify_pin = $.trim($('#verify_pin').val());
		if(verify_pin!='' && verify_pin.length == 4){
		var jqXHR=$.ajax({
				type: "POST",
				url: "<?php echo Yii::app()->request->baseUrl; ?>/doctor/twilioPinVerificationDoctor",
				data: { verify_pin: verify_pin },
				async: false,
				success:function(result){
					if(result=='ok'){
						document.registration_success.submit();
					}else{
						$('#verify_pin').addClass('field_required');
						$('#verify_pin').attr('title','Pin number is wrong.');
						$('input').keyup(function() {
							$(this).removeClass('field_required');
							$(this).removeAttr('title');
						});
					}
				}
			});
		}else{
			//alert('Phone number required');	
			$('#verify_pin').addClass('field_required');
			$('#verify_pin').attr('title','Pin number not valid.');
			$('input').keyup(function() {
				$(this).removeClass('field_required');
				$(this).removeAttr('title');
			});
		}
  }
</script>