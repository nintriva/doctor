<script src="<?php echo Yii::app()->request->baseUrl; ?>/assets/js/script.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/assets/js/jquery.prettyPhoto.js" type="text/javascript"></script>
<link href="<?php echo Yii::app()->request->baseUrl; ?>/assets/css/prettyPhoto.css" rel="stylesheet" type="text/css" />
<?php
$this->breadcrumbs=array(
	'Dashboard'=>array('index'),
	'Edit Profile',
);
?>

<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/assets/js/jquery.aw-showcase.js"></script>
<script type="text/javascript">
$(document).ready(function(){
  function add() {
    if($(this).val() === ''){
      $(this).val($(this).attr('placeholder')).addClass('placeholder');
    }
  }

  function remove() {
    if($(this).val() === $(this).attr('placeholder')){
      $(this).val('').removeClass('placeholder');
    }
  }
  

  // Create a dummy element for feature detection
  if (!('placeholder' in $('<input>')[0])) {

    // Select the elements that have a placeholder attribute
    $('input[placeholder], textarea[placeholder]').blur(add).focus(remove).each(add);

    // Remove the placeholder text before the form is submitted
    $('form').submit(function(){
      $(this).find('input[placeholder], textarea[placeholder]').each(remove);
    });
  }
});
</script>

<!--for tabs start-->
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/assets/css/tabs.css">
<!--for tabs end-->

<!--for field open start-->
<script language="javascript">
function displayTextField()
{
	/*$("#existItemText").hide();
	$("#existItem").show();
	$("#existItem").focus();
	event.stopPropagation();*/	
}
function hideTextField()
{
	$("#existItemText").show();
	$("#existItem").hide();
}
function displayTextField2()
{
	/*$("#existItemText2").hide();
	$("#existItem2").show();
	$("#existItem2").focus();	*/
}
function hideTextField2()
{
	$("#existItemText2").show();
	$("#existItem2").hide();
}
function displayTextField3()
{
	/*$("#existItemText2").hide();
	$("#existItem2").show();
	$("#existItem2").focus();	*/
}
function hideTextField3()
{
	$("#existItemText3").show();
	$("#existItem3").hide();
}
</script>
<!--for field open end-->

<!--for popup start-->
<link href="<?php echo Yii::app()->request->baseUrl; ?>/assets/css/popup.css" rel="stylesheet" type="text/css" media="all" />
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/assets/js/popupscript.js"></script>
<!--for popup end -->

<!--tooltip start-->
<script type="text/javascript">
var offsetfromcursorX=12 //Customize x offset of tooltip
var offsetfromcursorY=10 //Customize y offset of tooltip

var offsetdivfrompointerX=10 //Customize x offset of tooltip DIV relative to pointer image
var offsetdivfrompointerY=14 //Customize y offset of tooltip DIV relative to pointer image. Tip: Set it to (height_of_pointer_image-1).

document.write('<div id="dhtmltooltip"></div>') //write out tooltip DIV
document.write('<img id="dhtmlpointer" src="<?php echo Yii::app()->request->baseUrl; ?>/assets/images/arrow2.gif">') //write out pointer image

var ie=document.all
var ns6=document.getElementById && !document.all
var enabletip=false
if (ie||ns6)
var tipobj=document.all? document.all["dhtmltooltip"] : document.getElementById? document.getElementById("dhtmltooltip") : ""

var pointerobj=document.all? document.all["dhtmlpointer"] : document.getElementById? document.getElementById("dhtmlpointer") : ""

function ietruebody(){
return (document.compatMode && document.compatMode!="BackCompat")? document.documentElement : document.body
}

function ddrivetip(thetext, thewidth, thecolor){
if (ns6||ie){
if (typeof thewidth!="undefined") tipobj.style.width=thewidth+"px"
if (typeof thecolor!="undefined" && thecolor!="") tipobj.style.backgroundColor=thecolor
tipobj.innerHTML=thetext
enabletip=true
return false
}
}

function positiontip(e){
if (enabletip){
var nondefaultpos=false
var curX=(ns6)?e.pageX : event.clientX+ietruebody().scrollLeft;
var curY=(ns6)?e.pageY : event.clientY+ietruebody().scrollTop;
//Find out how close the mouse is to the corner of the window
var winwidth=ie&&!window.opera? ietruebody().clientWidth : window.innerWidth-20
var winheight=ie&&!window.opera? ietruebody().clientHeight : window.innerHeight-20

var rightedge=ie&&!window.opera? winwidth-event.clientX-offsetfromcursorX : winwidth-e.clientX-offsetfromcursorX
var bottomedge=ie&&!window.opera? winheight-event.clientY-offsetfromcursorY : winheight-e.clientY-offsetfromcursorY

var leftedge=(offsetfromcursorX<0)? offsetfromcursorX*(-1) : -1000

//if the horizontal distance isn't enough to accomodate the width of the context menu
if (rightedge<tipobj.offsetWidth){
//move the horizontal position of the menu to the left by it's width
tipobj.style.left=curX-tipobj.offsetWidth+"px"
nondefaultpos=true
}
else if (curX<leftedge)
tipobj.style.left="5px"
else{
//position the horizontal position of the menu where the mouse is positioned
tipobj.style.left=curX+offsetfromcursorX-offsetdivfrompointerX+"px"
pointerobj.style.left=curX+offsetfromcursorX+"px"
}

//same concept with the vertical position
if (bottomedge<tipobj.offsetHeight){
tipobj.style.top=curY-tipobj.offsetHeight-offsetfromcursorY+"px"
nondefaultpos=true
}
else{
tipobj.style.top=curY+offsetfromcursorY+offsetdivfrompointerY+"px"
pointerobj.style.top=curY+offsetfromcursorY+"px"
}
tipobj.style.visibility="visible"
if (!nondefaultpos)
pointerobj.style.visibility="visible"
else
pointerobj.style.visibility="hidden"
}
}

function hideddrivetip(){
if (ns6||ie){
enabletip=false
tipobj.style.visibility="hidden"
pointerobj.style.visibility="hidden"
tipobj.style.left="-1000px"
tipobj.style.backgroundColor=''
tipobj.style.width=''
}
}

document.onmousemove=positiontip
</script>

<div class="main">
        <div id="breadcrumb" class="fk-lbreadbcrumb newvd">
            <?php $this->widget('zii.widgets.CBreadcrumbs', array(
					  'links'=>$this->breadcrumbs,
				  ));
			?>
        </div>
  	  <div class="dashboard_mainarea">
     	<div class="leftmenu lftmenu_hight">
     		<?php /*
       		 <h2>Patient control panel</h2>
             <ul>
            	 <li><?php echo CHtml::link('Dashboard', $this->createAbsoluteUrl('index')); ?></li>
                 <li class="active"><?php echo CHtml::link('My Account', $this->createAbsoluteUrl('patient/editProfile/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li><?php echo CHtml::link('Reset Password', $this->createAbsoluteUrl('patient/resetpassword/'.Yii::app()->session['logged_user_id'])); ?></li>
             </ul>
             */ ?>
             <?php $this->renderPartial('//layouts/navigation'); ?>
        </div>
        <div class="rightarea_dashboard">
        	<div class="tabBox">
            	<ul class="tabs">
                	<li><a href="#tab1">Personal Info</a></li>
                </ul>
                <div class="tabContainer">
                	<div id="tab1" class="tabContent">
 						<!--<form method="post" action="">-->
                        <?php $form=$this->beginWidget('CActiveForm', array(
							'id'=>'registration',
							'htmlOptions' => array(
								'enctype' => 'multipart/form-data',
							),
						)); ?>
                        <input type="hidden" name="hidden_id" id="hidden_id" value="<?php echo $model->id; ?>" />
                        <div class="dashboardcont_leftbox">
                            <?php if(Yii::app()->user->hasFlash('editProfile')): ?>
                            <span class="flash-success">
                                <?php echo Yii::app()->user->getFlash('editProfile'); ?>
                            </span>
                            <?php endif; ?>
                            <h1>Personal Information</h1>
                            <div class="box_content">
        							<div class="fld_area">
                                        <?php echo $form->labelEx($model,'user_first_name',array('class'=>'fld_name')); ?>
                                        <div class="name_fld">
                                        <?php echo $form->textField($model,'user_first_name',array('size'=>32,'maxlength'=>32,'placeholder'=> 'First Name','class'=>'fld_class')); ?>
                                        <?php echo $form->error($model,'user_first_name'); ?>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="fld_area">
                                        <?php echo $form->labelEx($model,'user_last_name',array('class'=>'fld_name')); ?>
                                        <div class="name_fld">
                                        <?php echo $form->textField($model,'user_last_name',array('size'=>32,'maxlength'=>32,'placeholder'=> 'Last Name','class'=>'fld_class')); ?>
                                        <?php echo $form->error($model,'user_last_name'); ?>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="fld_area">
                                        <?php echo $form->labelEx($model,'user_sex',array('class'=>'fld_name')); ?>
                                        <div class="name_fld">
                                        <?php
											  $selected_gender = $model->user_sex;
											  echo CHtml::dropDownList('user_sex', $selected_gender, 
											  $model->genderOptions,
											  array('class'=>'select_fld_class'));
										?>
										<?php echo $form->error($model,'user_sex'); ?>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="fld_area">
                                        <?php echo $form->labelEx($model,'user_dob',array('class'=>'fld_name')); ?>
                                        <div class="name_fld">
                                            <div class="select_option">
                                                <?php
													  $birth_date_mm = $birth_date['mm'];
													  echo CHtml::dropDownList('mm', $birth_date_mm, 
													  $model->monthOptions,
													  array('class'=>'fld_class2'));
												?>
                                            </div>
                                            <div class="select_option2">
                                                <?php
													  $birth_date_dd = $birth_date['dd'];
													  echo CHtml::dropDownList('dd', $birth_date_dd, 
													  $model->dateOptions,
													  array('class'=>'fld_class2'));
												?>
                                            </div>
                                            <div class="select_option3">
                                            	<?php
													  $birth_date_yy = $birth_date['yy'];
													  echo CHtml::dropDownList('yy', $birth_date_yy, 
													  $yearOptions,
													  array('class'=>'fld_class2'));
												?>
                                            </div>

<!-- Parent/Legal Guardian
 - Phone 
 - Address Line 1
 - City
 - State
 - Zip
 - Preferred Contact Method -->

                                        </div>
                                        <div class="clear"></div>
                                    </div>
									<div class="fld_area">
                                        <?php echo $form->labelEx($model,'user_phone',array('class'=>'fld_name')); ?>
                                        <div class="name_fld">
                                        <?php echo $form->textField($model,'user_phone',array('size'=>32,'maxlength'=>32,'placeholder'=> 'Phone Number','class'=>'fld_class')); ?>
                                        <?php echo $form->error($model,'user_phone'); ?>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
									<div class="fld_area">
                                        <?php echo $form->labelEx($model,'user_address',array('class'=>'fld_name')); ?>
                                        <div class="name_fld">
                                        <?php echo $form->textField($model,'user_address',array('size'=>32,'maxlength'=>32,'placeholder'=> 'Address','class'=>'fld_class')); ?>
                                        <?php echo $form->error($model,'user_address'); ?>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
									<div class="fld_area">
                                        <?php echo $form->labelEx($model,'user_city',array('class'=>'fld_name')); ?>
                                        <div class="name_fld">
                                        <?php echo $form->textField($model,'user_city',array('size'=>32,'maxlength'=>32,'placeholder'=> 'City','class'=>'fld_class')); ?>
                                        <?php echo $form->error($model,'user_city'); ?>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
									<div class="fld_area">
                                        <?php echo $form->labelEx($model,'user_state',array('class'=>'fld_name')); ?>
                                        <div class="name_fld">
                                        <?php echo $form->textField($model,'user_state',array('size'=>32,'maxlength'=>32,'placeholder'=> 'State','class'=>'fld_class')); ?>
                                        <?php echo $form->error($model,'user_state'); ?>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
									<div class="fld_area">
                                        <?php echo $form->labelEx($model,'user_zip',array('class'=>'fld_name')); ?>
                                        <div class="name_fld">
                                        <?php echo $form->textField($model,'user_zip',array('size'=>32,'maxlength'=>32,'placeholder'=> 'Zip','class'=>'fld_class')); ?>
                                        <?php echo $form->error($model,'user_zip'); ?>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
									<!-- <div class="fld_area">
                                        <?php echo $form->labelEx($model,'user_contact_method',array('class'=>'fld_name')); ?>
                                        <div class="name_fld">
                                        <?php echo $form->textField($model,'user_contact_method',array('size'=>32,'maxlength'=>32,'placeholder'=> 'Preferred Contact Method','class'=>'fld_class')); ?>
                                        <?php echo $form->error($model,'user_contact_method'); ?>
                                        </div>
                                        <div class="clear"></div>
                                    </div> -->
									<div class="fld_area">
                                        <?php echo $form->labelEx($model,'user_contact_method',array('class'=>'fld_name')); ?>
                                        <div class="name_fld">
                                        <?php
											  $selected_gender = $model->user_contact_method;
											  echo CHtml::dropDownList('user_contact_method', $selected_gender, 
											  $model->ContactMethod,
											  array('class'=>'select_fld_class'));
										?>
										<?php echo $form->error($model,'user_contact_method'); ?>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
        
                            </div>
                        </div>
                        <div><span>
                        <!--<a class="registbt" href="#">Update</a> <a class="registbt" href="">Reset</a>-->
                        <?php echo CHtml::submitButton($model->isNewRecord ? 'Update' : 'Save',array('class'=>'registbt')); ?>
                        <?php echo CHtml::resetButton($model->isNewRecord ? 'Reset' : 'Reset',array('class'=>'registbt')); ?>
                        </span></div>
                        <!--</form>-->
                        <?php $this->endWidget(); ?>
                    </div>
                </div>
                <script type="text/javascript">// < ![CDATA[
// < ![CDATA[
// < ![CDATA[
function show1()
{ document.getElementById('div1').style.display ='none'; } 
function show2()
{ document.getElementById('div1').style.display = 'block'; }
// ]]>

</script>	

                <script type="text/javascript">
					  $(".tabContent").hide(); 
					  $("ul.tabs li:first").addClass("active").show(); 
					  $(".tabContent:first").show(); 
					 
					  $("ul.tabs li").click(function () {
						$("ul.tabs li").removeClass("active"); 
						$(this).addClass("active"); 
						$(".tabContent").hide(); 
						var activeTab = $(this).find("a").attr("href"); 
						$(activeTab).fadeIn(); 
						return false;
					  });
			    </script>      
            </div> 
      	</div>
    </div>
    </div>
