<link href='<?php echo Yii::app()->getBaseUrl(true); ?>/assets/css/style.css' rel='stylesheet' />
<div class="patient_dashboard">
    <div class="fld_area dashboardcont_leftbox">

        <div class="name_fld">
            <?php if( isset(Yii::app()->session['logged_user_type']) && (Yii::app()->session['logged_user_type'] == "doctor" ) ) {?>
            <input type="text" style="width: 50%" name="document_name" id="patient_search" class="filter_txtfld_calender fld_class" placeholder="Type your patient name.." onkeyup="autocomplet();"  value=""/>
<?php }?>
            <?php if( isset(Yii::app()->session['logged_user_type']) && (Yii::app()->session['logged_user_type'] == "patient" ) ) {?>
                <input type="text" style="width: 50%" name="document_name" id="document_name_search" class="filter_txtfld_calender fld_class" placeholder="Type your document name.." onkeyup="autolist();"  value=""/>
            <?php }?>
           <!-- <button class="search_btn" name="" value="Search" style="float: left" onclick="documentNameSearch();">Search</button>-->
            <ul id="patients_list_id" style="display:none;"></ul>
        </div>

        <div class="clear"></div>
    </div>

    <a class='registbt js-open-modal' data-modal-id="popup" style="font-size: 12px;margin-left: 23%">Create Document</a>

    <h2 class="patient_dashboard_appmnt" style="margin-top: 17px">Documents Vault</h2>

    <div class="dashboardcont_leftbox2"  style="margin-top:20px;">

        <!--patient-->
        <?php if( isset(Yii::app()->session['logged_user_type']) && (Yii::app()->session['logged_user_type'] == "patient" ) ) {?>
        <ul>
            <li class="heading">
                <span class="add_new" style="width: 23%">Document Name</span>
                <span class="patient_colmn" style="width: 20%">Label</span>
                <span class="patient_colmn" style="width:147px;">Note</span>

                <span class="lastcolmn txt_align" style="width: 25%" >Operations</span>

            </li>
            <?php
            foreach($document as $doc){

                ?>

                <li>
                    <span class="add_new" style="width: 23%"><a href="<?php echo Yii::app()->createUrl('documents/view', array('id' =>$doc->id));?>"><?php echo $doc->document_name ?></a></span>
                    <span class="patient_colmn" style="width: 20%"><?php echo $doc->label ?></span>
                    <span class="patient_colmn" style="height:10px;width:158px;overflow: hidden;text-overflow: ellipsis" ><?php echo $doc->note ?></span>

                            <span>
                                <?php
                                if(isset($doc->docFiles) && $doc->docFiles){
                                    if(count($doc->docFiles)>1){?>
                                        <a style="float: left;margin-left: 40px" href="<?php echo Yii::app()->createUrl('documents/view', array('id' =>$doc->id));?>">View &nbsp;&nbsp;</a><?php }

                                    else {
                                        ?>
                                        <a target="_blank" style="float: left;margin-left: 40px" href="<?php echo Yii::app()->createUrl('documents/download', array('url' =>$doc->docFiles[0]['id']));?>">View &nbsp;&nbsp;</a><?php }
                                }?>

                                <?php
                                if(isset($doc->docFiles) && $doc->docFiles){
                                    if(count($doc->docFiles)>1){?>
                                    <a style="float: left;" href="<?php echo Yii::app()->createUrl('documents/view', array('id' =>$doc->id));?>">Download &nbsp;&nbsp;</a><?php }

                                    else {
                                        ?>
                                    <a style="float: left;" href="<?php echo Yii::app()->createUrl('documents/link', array('url' =>$doc->docFiles[0]['id']));?>">Download &nbsp;&nbsp;</a><?php }
                                }?>


                    <a href="<?php echo Yii::app()->createUrl('documents/update', array('id' =>$doc->id));?>">Edit</a></span>
                </li>
            <?php }?>
        </ul>
        <?php }?>

    </div>
</div>
<?php
$this->widget('CLinkPager', array('id'=>'page-link',
    'pages' => $pages, 'header' => '', 'prevPageLabel' => '&lt;&lt;', 'nextPageLabel' => '&gt;&gt;',
))
?>
<script>
    function autocomplet() {
        var keyword = $('#patient_search').val();

        //var sent_to_persion = $('#sent_to_persion').val();
        //keyword != "" || keyword != null && (strlen($keyword) > 3)
        if( keyword != ""  || keyword != null) {
            $.ajax({
                url: '<?php echo Yii::app()->request->baseUrl; ?>/Message/getPatients',
                type: 'POST',
                data: {keyword:keyword},
                success:function(data){
                    $('#patients_list_id').show();
                    $('#patients_list_id').html(data);
                    if(keyword.length<1)
                    {
                        $('#patients_list_id').hide();
                    }
                }
            });
        }
    }
    function set_item(item) {
        var itm = item.split("|");

        $('#patient_search').val(itm[0]);
        $('#patients_list_id').hide();

        var si = itm[1];
        $.ajax({
            method: 'POST',
            url: '<?php echo Yii::app()->createUrl('patient/document'); ?>',
            data: { kword: si},
            success: function (data) {
                $("#document_partial").html(data);

            }


        });

        //alert(itm[0]+"--"+itm[1]+"--"+itm[2]);
    }

    function autolist() {
        var keyword = $('#document_name_search').val();
        if( keyword != ""  || keyword != null) {
            if( keyword.length > 2 ){
            $.ajax({
                url: '<?php echo Yii::app()->request->baseUrl; ?>/Documents/getLists',
                type: 'POST',
                data: {keyword:keyword},
                success:function(data){

                    $('#patients_list_id').show();
                    $('#patients_list_id').html(data);
                    if(keyword.length<1)
                    {
                        $('#patients_list_id').hide();
                    }
                }
            });
        }
            if(keyword.length<1)
            {
                $('#patients_list_id').hide();
            }
        }
    }
    function docs(item) {

        var itm = item.split("|");
        var itmData =itm[0]+" "+ itm[1];
        $('#document_name_search').val(itmData );
        $('#patients_list_id').hide();

        var si = itm[2];

        $.ajax({
            method: 'POST',
            url: '<?php echo Yii::app()->createUrl('patient/document'); ?>',
            data: { kword: si},
            success: function (data) {
                $("#document_partial").html(data);

            }


        });

        //alert(itm[0]+"--"+itm[1]+"--"+itm[2]);
    }
    $(function(){

        var appendthis =  ("<div class='modal-overlay js-modal-close'></div>");

        $('a[data-modal-id]').click(function(e) {
            e.preventDefault();
            $("body").append(appendthis);
            $(".modal-overlay").fadeTo(500, 0.7);
            //$(".js-modalbox").fadeIn(500);
            var modalBox = $(this).attr('data-modal-id');
            $('#'+modalBox).fadeIn($(this).data());
        });


        $(".js-modal-close, .modal-overlay,#close_popup").click(function() {
            $(".modal-box, .modal-overlay").fadeOut(500, function() {
                $(".modal-overlay").remove();
            });
        });

        $(window).resize(function() {
            $(".modal-box").css({
                top: ($(window).height() - $(".modal-box").outerHeight()) / 2,
                left: ($(window).width() - $(".modal-box").outerWidth()) / 2
            });
        });

        $(window).resize();

    });
</script>