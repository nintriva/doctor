<?php
$this->breadcrumbs=array(
	'Reset Password',
);
?>
<div class="main">
    <div id="breadcrumb" class="fk-lbreadbcrumb newvd">
        <!--<span><a href="">Home</a></span> >  
        <span>Dashboard</span>--> 
        <?php $this->widget('zii.widgets.CBreadcrumbs', array(
				  'links'=>$this->breadcrumbs,
			  ));
		?>
    </div>
  	  <div class="dashboard_mainarea">
     	<div class="leftmenu" id="left_id">
     		<?php /*
       		 <h2>Patient control panel</h2>
             <ul>
            	 <li><?php echo CHtml::link('Dashboard', $this->createAbsoluteUrl('index')); ?></li>
                 <li><?php echo CHtml::link('My Account', $this->createAbsoluteUrl('patient/editProfile/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li class="active"><?php echo CHtml::link('Reset Password', $this->createAbsoluteUrl('doctor/resetpassword/'.Yii::app()->session['logged_user_id'])); ?></li>
             </ul>
             */ ?>
             <?php $this->renderPartial('//layouts/navigation'); ?>
        </div>
    
    <div class="rightarea_dashboard">
		<?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'reset_password',
        )); ?>
            <div class="dashboardcont_leftbox">
                <?php if(Yii::app()->user->hasFlash('resetPassword')): ?>
                    <span class="flash-success">
                        <?php echo Yii::app()->user->getFlash('resetPassword'); ?>
                    </span>
                <?php endif; ?>
                 <h1>Reset Password</h1>
                 <div class="box_content">
                    <div class="fld_area">
                       <div class="fld_name">Old Password</div>
                       <div class="name_fld">
                       <input id="old_password" class="fld_class" type="password" name="old_password" placeholder="Old Password" autocomplete="off" size="32" value="<?php if(isset($_REQUEST['old_password'])) echo $_REQUEST['old_password']; ?>">
                       <div class="errorMessage"><?php if(isset($error['old_password'])) echo $error['old_password']; ?></div>
                       </div>
                       <div class="clear"></div>             
                    </div>
                    <div class="fld_area custom-popup">
                       <div class="fld_name fld_name_hight">New Password</div>
                       <div class="name_fld">
                       <span class='rulepassword'>
                       <input id="new_password" class="fld_class" type="password" name="new_password" placeholder="New Password" autocomplete="off" size="32" value="<?php if(isset($_REQUEST['new_password'])) echo $_REQUEST['new_password']; ?>">
                       <div class="errorMessage"><?php if(isset($error['new_password'])) echo $error['new_password']; ?></div>
                            
                    </span>
                       
                      
                       </div>
                      
                       <div id="pswd_info">
                        <h4>Password must meet the following requirements:</h4>
                        <ul>
                         <li id="letter" class="invalid">At least <strong>one letter</strong></li>
                         <li id="capital" class="invalid">At least <strong>one capital letter</strong></li>
                         <li id="number" class="invalid">At least <strong>one number</strong></li>
                         <li id="length" class="invalid">Be at least <strong>8 characters</strong></li>
                        </ul>
                       </div>
                       <div class="clear"></div>
                    </div>
                    <div class="fld_area">
                       <div class="fld_name fld_name_hight">Confirm Password</div>
                       <div class="name_fld">
                       <input id="confirm_password" class="fld_class" type="password" name="confirm_password" placeholder="Confirm Password" autocomplete="off" size="32" value="<?php if(isset($_REQUEST['confirm_password'])) echo $_REQUEST['confirm_password']; ?>">
                       <div class="errorMessage"><?php if(isset($error['confirm_password'])) echo $error['confirm_password']; ?></div>
                       </div>
                       <div class="clear"></div>
                    </div>
                    
                </div>
            </div>
            <div>
                <span>
                <?php echo CHtml::submitButton('Update',array('class'=>'registbt')); ?>
                <?php echo CHtml::link('Cancel', $this->createAbsoluteUrl('patient/index'),array('class'=>'registbt')); ?>
                </span>
            </div>
        <?php $this->endWidget(); ?>
    </div>
     
  </div>
</div>

<script>
$(document).ready(function() {
$('input[type=submit]').attr('disabled',true).addClass('disSubBtnCls');
//you have to use keyup, because keydown will not catch the currently entered value
$('#new_password').keyup(function() {
 // set password variable
 var pswd = $(this).val();
 var lengthValid = 0;
 var letterValid = 0;
 var uppercaseValid = 0;
 var numberValid = 0;
 
 //validate the length
 if ( pswd.length < 8 ) {
  $('#length').removeClass('valid').addClass('invalid');
  lengthValid = 0;
 } else {
  $('#length').removeClass('invalid').addClass('valid');
  lengthValid = 1;
 }
 
 //validate letter
 if ( pswd.match(/[A-z]/) ) {
  $('#letter').removeClass('invalid').addClass('valid');
  letterValid = 1;
 } else {
  $('#letter').removeClass('valid').addClass('invalid');
  letterValid = 0;
 }
 
 //validate uppercase letter
 if ( pswd.match(/[A-Z]/) ) {
  $('#capital').removeClass('invalid').addClass('valid');
  uppercaseValid = 1;
 } else {
  $('#capital').removeClass('valid').addClass('invalid');
  uppercaseValid = 0;
 }
 
 //validate number
 if ( pswd.match(/\d/) ) {
  $('#number').removeClass('invalid').addClass('valid');
  numberValid = 1;
 } else {
  $('#number').removeClass('valid').addClass('invalid');
  numberValid = 0;
 }
 
 if(lengthValid == 1 && letterValid == 1 && uppercaseValid == 1 && numberValid == 1)
 {
  /*$("#btn1").show();
  $("#btn2").hide();*/
  $('input[type=submit]').attr('disabled',false).removeClass('disSubBtnCls');
  setTimeout(function(){$('#pswd_info').hide();},800);      
 }
 else
 {
  $('#pswd_info').show();
  /*$("#btn1").hide();
  $("#btn2").show();*/
  $('input[type=submit]').attr('disabled',true).addClass('disSubBtnCls');
 }
 
}).focus(function() {
 $('#pswd_info').show();
 if($('#new_password').val() == '')
 {
  /*$("#btn1").hide();
  $("#btn2").show();*/
 }     
}).blur(function() {
 $('#pswd_info').hide();
});
});
</script>