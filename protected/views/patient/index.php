<script src="<?php echo Yii::app()->getBaseUrl(true); ?>/assets/js/script.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->getBaseUrl(true); ?>/assets/js/jquery.prettyPhoto.js" type="text/javascript"></script>
<link href="<?php echo Yii::app()->getBaseUrl(true); ?>/assets/css/prettyPhoto.css" rel="stylesheet" type="text/css" />

<link href='<?php echo Yii::app()->getBaseUrl(true); ?>/assets/calender/fullcalendar/fullcalendar.css' rel='stylesheet' />
<link href='<?php echo Yii::app()->getBaseUrl(true); ?>/assets/calender/fullcalendar/fullcalendar.print.css' rel='stylesheet' media='print' />
<script src='<?php echo Yii::app()->getBaseUrl(true); ?>/assets/calender/fullcalendar/fullcalendar.min.js'></script>
<script src='<?php echo Yii::app()->getBaseUrl(true); ?>/assets/calender/fullcalendar/gcal.js'></script>
<script src='<?php echo Yii::app()->getBaseUrl(true); ?>/assets/calender/fullcalendar/moment.min.js'></script>
<script src='<?php echo Yii::app()->getBaseUrl(true); ?>/assets/js/dashboard_script.js'></script>
<style>
    a.cross{background:url(../assets/images/cross.png) no-repeat; display:block; width:19px; height:19px; float:left;}
    a.cross:hover{background:url(../assets/images/cross_hover.png) no-repeat;}
</style>
<?php
$this->breadcrumbs = array(
    'Dashboard',
);
?>
<div class="main">
    <div id="breadcrumb" class="fk-lbreadbcrumb newvd">
        <!--<span><a href="">Home</a></span> >  
        <span>Dashboard</span>--> 
        <?php
        $this->widget('zii.widgets.CBreadcrumbs', array(
            'links' => $this->breadcrumbs,
        ));
        ?>
    </div>
    <div class="dashboard_mainarea">
        <div class="leftmenu" id="left_id">
          
<?php $this->renderPartial('//layouts/navigation'); ?>
        </div>
        <div class="rightarea_dashboard" id="right_id">
            <div class="dashboard_content">

                <span class="profile_brief">
                    <?php echo $dataProvider->user_first_name; ?> <?php echo $dataProvider->user_last_name; ?> <br>
                </span>
                <span class="member_box">Member since <b style="font-weight:bold;"><?php echo date('M, Y', strtotime($dataProvider->date_created)); ?></b></span>
            </div>

            <div class="patient_dashboard">
                <form name="refine search" method="get" action="">
                    <div class="add_area">
                        <span class="add_calender">
                            <div class="filter_search_area_calender">
                                <div class="filter_patient_dash">
                                    <label>Doctor Name</label>
                                    <input type="text" name="doctor_name" placeholder="eg. John" value="<?php if (isset($_REQUEST['doctor_name'])) echo $_REQUEST['doctor_name']; ?>"/>
                                    <label>Start Date</label>
                                    <input type="text" name="start_date" class="filter_txtfld_calender datepicker small" placeholder="yyyy-mm-dd" value="<?php if (isset($_REQUEST['start_date'])) echo $_REQUEST['start_date']; ?>"/>
                                    <label>End Date</label>
                                    <input type="text" name="end_date" class="filter_txtfld_calender datepicker small" placeholder="yyyy-mm-dd" value="<?php if (isset($_REQUEST['end_date'])) echo $_REQUEST['end_date']; ?>"/>
                                    <label>Status</label>
                                    <select name="status">
                                        <option value="All" <?php if (isset($_REQUEST['status']) && $_REQUEST['status'] == 'All') echo 'selected'; ?>>All</option>
                                        <option value="Accepted" <?php if (isset($_REQUEST['status']) && $_REQUEST['status'] == 'Accepted') echo 'selected'; ?>>Accepted</option>
                                        <option value="Pending" <?php if (isset($_REQUEST['status']) && $_REQUEST['status'] == 'Pending') echo 'selected'; ?>>Pending</option>
                                        <option value="Cancel" <?php if (isset($_REQUEST['status']) && $_REQUEST['status'] == 'Cancel') echo 'selected'; ?>>Cancelled</option>
										 <option value="Complete" <?php if (isset($_REQUEST['status']) && $_REQUEST['status'] == 'Complete') echo 'selected'; ?>>Complete</option>
                                    </select>
                                    <input type="submit" class="search_btn" name="" value="Search"/>
                                </div>
                                <div class="filter_apbtn"></div>
                                <div class="clear"></div>
                            </div>
                        </span>
                        <div class="clear"></div>
                    </div>
                </form>
                <h2 class="patient_dashboard_appmnt">Appointments list</h2>
                <span class="dashboard_app_span">
                    <span><img src="<?php echo Yii::app()->getBaseUrl(true); ?>/assets/images/s_no_show_icon_blue.png" title="Accepted" alt="Accepted"/>Accepted</span>
                    <span><img src="<?php echo Yii::app()->getBaseUrl(true); ?>/assets/images/s_no_show_icon_brown.png" title="Pending" alt="Pending"/>Pending</span>
                    <span><img src="<?php echo Yii::app()->getBaseUrl(true); ?>/assets/images/s_no_show_icon.png" title="Cancelled" alt="Cancelled"/>Cancelled</span>
                </span>
                <div class="dashboardcont_leftbox2">
                    <ul>
                        <li class="heading">
                            <span class="add_new">Doctor</span>
                            <span class="patient_colmn">Appointment</span>
                            <span class="patient_colmn">Reason</span>
                            <span class="patient_colmn">Address</span>
                            <span class="lastcolmn txt_align">Status</span>
                            <span class="lastcolmn txt_align">Action</span>
                        </li>
                        <?php if (!empty($user_patient_book)) { ?>
    <?php foreach ($user_patient_book as $user_patient_book_key => $user_patient_book_val) { ?>
                                <li>
                                    <span class="add_new">
                                        <input type="hidden" id="pres_app_title_id_<?php echo $user_patient_book_val['id']; ?>" value="<?php echo date('dS, M h:iA', $user_patient_book_val['book_time']); ?>-<?php echo date('h:iA', ($user_patient_book_val['book_time'] + ($user_patient_book_val['book_duration'] * 60))); ?>" />
                                        <?php
                                        $filePath = 'assets/upload/doctor/' . $user_patient_book_val['d_id'] . "/" . $user_patient_book_val['d_image'];
                                        if (@file_get_contents($filePath, 0, NULL, 0, 1)) {
                                            ?>
                                            <?php echo CHtml::image(Yii::app()->getBaseUrl(true) . '/assets/upload/doctor/' . $user_patient_book_val['d_id'] . "/" . $user_patient_book_val['d_image'], "image", array("width" => 60/* ,"height"=>111 */, 'style' => 'float:left; padding-right:10px;')); ?>
                                        <?php } else { ?>
                                            <?php if ($user_patient_book_val['d_gender'] == 'M') { ?>
                                                <?php echo CHtml::image(Yii::app()->getBaseUrl(true) . '/assets/images/avatar.png', "image", array("width" => 60/* ,"height"=>111 */, 'style' => 'float:left; padding-right:10px;')); ?>
                                            <?php } else { ?>
                                                <?php echo CHtml::image(Yii::app()->getBaseUrl(true) . '/assets/images/avatar.png', "image", array("width" => 60/* ,"height"=>111 */, 'style' => 'float:left; padding-right:10px;')); ?>
                                            <?php } ?>
        <?php } ?> 
                                        <p style="float:left">
                                            <strong><?php echo $user_patient_book_val['d_title'] . ' ' . $user_patient_book_val['d_doctor_name']; ?></strong><br>
        <?php echo ($user_patient_book_val['speciality_id']) ? $user_speciality[$user_patient_book_val['speciality_id']] : ''; ?></p>
                                    </span>
                                    <span class="patient_colmn">
                                        <?php echo date('jS M', $user_patient_book_val['book_time']); ?>
                                        <br>
                                        <?php echo date('h:iA', $user_patient_book_val['book_time']); ?>
                                        -
                                        <?php echo date('h:iA', ($user_patient_book_val['book_time'] + ($user_patient_book_val['book_duration'] * 60))); ?>
                                    </span>
                                    <span class="patient_colmn"><?php echo isset($user_reason_for_visit[$user_patient_book_val['reason_for_visit_id']]) ? $user_reason_for_visit[$user_patient_book_val['reason_for_visit_id']] : ''; ?></span>
                                    <span class="patient_colmn"><?php echo $user_patient_book_val['da_address'] . "<BR>" . $user_patient_book_val['da_city'] . ", " . $user_patient_book_val['da_zip']; ?></span>
                                    <span class="lastcolmn txt_align">
                                        <?php if ($user_patient_book_val['confirm'] == 0) { ?>
                                            <img src="<?php echo Yii::app()->getBaseUrl(true); ?>/assets/images/s_no_show_icon_brown.png" title="Pending" alt="Pending"/>
                                        <?php } ?>
                                        <?php if ($user_patient_book_val['confirm'] == 1) { ?>
                                            <img src="<?php echo Yii::app()->getBaseUrl(true); ?>/assets/images/s_no_show_icon_blue.png" title="Accepted" alt="Accepted"/>
                                        <?php } ?>
                                           <?php if ($user_patient_book_val['confirm'] == 2) { ?>
                                            <img src="<?php echo Yii::app()->getBaseUrl(true); ?>/assets/images/s_no_show_icon.png" title="Cancelled" alt="Cancelled"/>
                                            <a href="javascript:void(0);" class="cancel_view" accesskey="<?php echo $user_patient_book_val['id']; ?>">view</a>
                                            <input type="hidden" id="cancel_view_val_<?php echo $user_patient_book_val['id']; ?>" value="<?php echo $user_patient_book_val['comments']; ?>" />
                                           <?php } ?>
										    <?php if ($user_patient_book_val['confirm'] == 3) { ?>
                                            <img src="<?php echo Yii::app()->getBaseUrl(true); ?>/assets/images/s_incomplete.png" title="Complete" alt="Complete"/>
                                        <?php } ?>
                                    </span>
                                    <span class="lastcolmn txt_align">
                                        <?php if ($user_patient_book_val['confirm'] != 2 && $user_patient_book_val['book_time'] > time()) { ?>
                                            <a id="app_cancel_<?php echo $user_patient_book_val['id']; ?>" class="cross" href="javascript:void(0);" accesskey="<?php echo $user_patient_book_val['id']; ?>" title="Cancel" ></a>
        <?php } ?>
                                    </span>
                                </li>
                            <?php } ?>
                        <?php } else { ?>
                            <li>No Appointment Yet.</li>
<?php } ?>
                    </ul>
                </div>
            </div>
            <?php
            $this->widget('CLinkPager', array(
                'pages' => $pages, 'header' => '', 'prevPageLabel' => '&lt;&lt;', 'nextPageLabel' => '&gt;&gt;',
            ))
            ?>

        </div> 
    </div>
</div>


<div id="toPopup"> 
    <div class="close_view"></div> 
    <div id="popup_content">
        <h4>Comments</h4>
        <p id="popup_content_val"></p>
    </div>
</div>

<div id="toPopup2"> 
    <div class="close"></div>
    <div id="popup_content"> <!--your content start-->
        <input type="hidden" id="cancel_hidden_app_id" value="" />
        <h4>Cancel Appointment</h4>
        <p id="cancel_app_title_id">Support | 9:00AM-9:30AM | Thursday , April 1st </p>
        <form name="" method="post">
            <textarea name="" placeholder="Send an optional note to your doctor..." id="mandatory_text"></textarea>
            <input type="button" name="cancel" value="Cancel" class="cancelbt" />
            <input type="button" name="accept" value="Send Message & Cancel" class="acceptbtcancel" />
        </form>
    </div>
</div>

<div id="backgroundPopup"></div>
<script type="text/javascript">
    $(function () {
        equalHeights($(".leftmenu"), $('.rightarea_dashboard'));
    });
    function appointmentCancel(app_id) {
        var app_id = $('#cancel_hidden_app_id').val();
        //if(confirm('are you sure?')){
        if ($('#mandatory_text').val() != '') {
            $.post("<?php echo Yii::app()->request->baseUrl; ?>/doctor/appointmentCancelPatient", {app_id: app_id, mandatory_text: $('#mandatory_text').val()}, function (response) {
                //$('#app_cancel_'+app_id).remove();
                //alert($('#booking_'+app_id).closest().html());
                location.reload();
            });
        } else {
            $('#mandatory_text').css('border', '1px solid red');
            $('#mandatory_text').keyup(function () {
                $(this).css('border', 'none');
            });
            return false;
        }
        //}
    }
</script>