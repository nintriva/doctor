<?php

$this->breadcrumbs=array(
    'Documents'=>array('view'),
    'Document Details',
);
?>
<div class="main">
    <div id="breadcrumb" class="fk-lbreadbcrumb newvd">
        <!--<span><a href="">Home</a></span> >
        <span>Dashboard</span>-->
        <?php $this->widget('zii.widgets.CBreadcrumbs', array(
            'links'=>$this->breadcrumbs,
        ));
        ?>
    </div>
    <div class="dashboard_mainarea">
        <div class="leftmenu">
            <?php $this->renderPartial('//layouts/navigation'); ?>
        </div>
        <?php //echo "<pre>";print_r($inbox_arr_details); echo "<pre>";print_r($pgn); ?>
        <div class="rightarea_dashboard">
            <div class="dashboardcont_leftbox messg_details" >
                <h1><span>Document Detail</span></h1>
                <div class="massege_title" style="margin-top:10px;">
                    <span class="sender_label">Document Name :</span>
                    <span class="sender_body"><?php echo $model->document_name;?></span>
                </div>
                <div class="massege_title">
                    <span class="sender_label">Label :</span>
                    <span class="sender_body"><?php echo $model->label;?></span>
                </div>
                <div class="massege_title">
                    <span class="sender_label">Note :</span>
                    <span class="sender_body"><?php echo $model->note;?></span>
                </div>
                <div class="massege_title">
                    <span class="sender_label">Date :</span>
                    <span class="sender_body"><?php echo $model->date;?></span>
                </div>
                <?php if($model->docFiles) {
                    ?>
                    <div class="massege_title" style="margin-top: 10px">
                        <h2>Attached Files</h2>
                    </div>
                    <?php
                    foreach($model->docFiles as $files){

                        $file =  $files->file_url;
                        $file1 = explode('_', $file);
						$text = str_replace(end($file1), '', $file);
						$name = rtrim($text, "_")
						
                  ?>
                    <div class="massege_title">
                        <span class="sender_label" style="word-wrap:break-word;width: 32%"><?php echo $name;?></span>
                        <span class="sender_body">

                            <a target="_blank" style="float: left;margin-left: 235px;margin-top: -25px;" href="<?php echo Yii::app()->createUrl('documents/download', array('url' =>$files->id));?>">
                                View&nbsp;&nbsp;
                            </a>
                        <a style="float: left;     margin-top: -25px;" href="<?php echo Yii::app()->createUrl('documents/link', array('url' =>$files->id));?>">
                            Download&nbsp;&nbsp;
                        </a></span>
                    </div>


                <?php   }}?>
                <span class="massege_title">

                    <a class="mesg_rply_btn" href="<?php echo Yii::app()->createUrl('documents/update', array('id' =>$model->id));?>">Edit</a>
                    <?php if( isset(Yii::app()->session['logged_user_type']) && (Yii::app()->session['logged_user_type'] == "doctor"||Yii::app()->session['logged_user_type'] == "parcticeAdmin" ) ) {?>
                        <a class="mesg_rply_btn" href="<?php echo Yii::app()->createUrl('doctor/document');?>">Back</a>
      <?php }else{?>                      <a class="mesg_rply_btn" href="<?php echo Yii::app()->createUrl('patient/document');?>">Back</a>
                    <?php }?>
	            </span>

                </div>
        </div>
    </div>
