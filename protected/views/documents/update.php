<?php

$this->breadcrumbs=array(
    'Update Document',
);

?>
<div class="main">
    <div id="breadcrumb" class="fk-lbreadbcrumb newvd">
        <!--<span><a href="">Home</a></span> >
        <span>Dashboard</span>-->
        <?php $this->widget('zii.widgets.CBreadcrumbs', array(
            'links'=>$this->breadcrumbs,
        ));
        ?>
    </div>
    <div class="dashboard_mainarea">
        <div class="leftmenu" id="left_id">

            <?php $this->renderPartial('//layouts/navigation'); ?>
        </div>

        <div class="rightarea_dashboard">
            <?php $form=$this->beginWidget('CActiveForm', array(
                'id'=>'update_document',
            )); ?>
            <div class="dashboardcont_leftbox">
                <?php if(Yii::app()->user->hasFlash('update_document')): ?>
                    <span class="flash-success">
                        <?php echo Yii::app()->user->getFlash('update_document'); ?>
                    </span>
                <?php endif; ?>
                <h1>Update Document</h1>

                <div class="box_content">
                    <div class="fld_area">
                        <?php echo $form->labelEx($model,'document_name',array('class'=>'fld_name')); ?>
                        <div class="name_fld">
                            <?php echo $form->textField($model,'document_name',array('size'=>32,'maxlength'=>32,'placeholder'=> 'First Name','class'=>'fld_class')); ?>
                            <?php echo $form->error($model,'document_name'); ?>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="fld_area">

                        <?php echo $form->hiddenField($model,'label',array('id'=>'label_id')); ?>
                        <?php echo $form->labelEx($model,'label',array('class'=>'fld_name')); ?>
                        <div class="name_fld">
                            <?php if( isset(Yii::app()->session['logged_user_type']) && (Yii::app()->session['logged_user_type'] == "doctor"||Yii::app()->session['logged_user_type'] == "parcticeAdmin" ) ) {?>
                                <select name="label" id="label_name" style="border: 1px solid #A6AFAF;">
                                    <option>Select Label</option>
                                    <option>Advanced Directive</option>
                                    <option >Authorization</option>
                                    <option >Cardiopulmonary Diagnosis</option>
                                    <option >Clinical Summary</option>
                                    <option >Consent for Treatment</option>
                                    <option >Consultation</option>
                                    <option >CT Report</option>
                                    <option >Demographics</option>
                                    <option >Diagnostic Ultrasound</option>
                                    <option >Disability Info</option>
                                    <option >Discharge Summary</option>
                                    <option >Dismissals</option>
                                    <option >Drivers License</option>
                                    <option >ECHO</option>
                                    <option >EEG-EMG</option>
                                    <option >EKG</option>
                                    <option >Eligibility</option>
                                    <option>Emergency Department</option>
                                    <option>Explanation of Benefits</option>
                                    <option>History and Physical</option>
                                    <option>Hospital History</option>
                                    <option>Immunization History</option>
                                    <option>Imported Document</option>
                                    <option>Insurance Card</option>
                                    <option>Insurance Correspondence</option>
                                    <option>Insurance Referral</option>
                                    <option>Letter of Medical Necessity</option>
                                    <option>Mammography Document</option>
                                    <option>Medical Record Request</option>
                                    <option>Medical Report</option>
                                    <option>Misc. Doc</option>
                                    <option>Misc. Hospital Document</option>
                                    <option>Misc. Lab Result</option>
                                    <option>Missed Appointment</option>
                                    <option>Nursing Home Visit/Home Care</option>
                                    <option>OB Documentation</option>
                                    <option>OB Non-Stress Test</option>
                                    <option>Office Consult Note</option>
                                    <option>Office History and Physical</option>
                                    <option>Office Procedure Report</option>
                                    <option>Other</option>
                                    <option>Other Office Letter</option>
                                    <option>Other Office Note</option>
                                    <option>Pathology Report</option>
                                    <option>Patient Authorization/Referral</option>
                                    <option>Patient Correspondence</option>
                                    <option>Patient Demographics</option>
                                    <option>Patient Driver&#39;s License</option>
                                    <option>Patient Insurance Card</option>
                                    <option>Patient Letter</option>
                                    <option>Physician Established Patient Note</option>
                                    <option>Physician New Patient Note</option>
                                    <option>Prescriptions</option>
                                    <option>Radiology Diagnostic</option>
                                    <option>Radiology/Oncology</option>
                                    <option>Referral Letter</option>
                                    <option>Release of Information</option>
                                    <option>Request for Amendment</option>
                                    <option>Summary of Care Received</option>
                                    <option>Surgery/Procedure Report</option>
                                    <option>Tests</option>
                                    <option>Therapy Note</option>
                                    <option>Treadmill Report</option>
                                    <option>Ultrasound</option>
                                    <option>Ultrasound Document</option>
                                    <option>Urgent Care Visit Note</option>
                                </select>
                            <?php }?>
                            <?php if( isset(Yii::app()->session['logged_user_type']) && (Yii::app()->session['logged_user_type'] == "patient" ) ) {?>
                            <select name="label" id="label_name" style="border: 1px solid #A6AFAF;">
                                <option value="">Select Label</option>
                                <option value="Advanced Directive">Advanced Directive</option>
                                <option value="Authorization">Authorization</option>
                                <option>Consent for Treatment</option>
                                <option>Consultation</option>
                                <option>X-Ray</option>
                                <option>Demographics</option>
                                <option>Diagnostic Ultrasound</option>
                                <option>Disability Info</option>
                                <option>Drivers License</option>
                                <option>ECHO</option>
                                <option>EEG-EMG</option>
                                <option>EKG</option>
                                <option>Eligibility</option>
                                <option>History and Physical</option>
                                <option>Hospital History</option>
                                <option>Immunization History</option>
                                <option>Imported Document</option>
                                <option>Insurance Card</option>
                                <option>Insurance Correspondence</option>
                                <option>Letter of Medical Necessity</option>
                                <option>Mammography Document</option>
                                <option>Medical Record Request</option>
                                <option>Medical Report</option>
                                <option>Misc. Doc</option>
                                <option>Other</option>
                                <option>Pathology Report</option>
                                <option>Prescriptions</option>
                                <option>Release of Information</option>
                                <option>Request for Amendment</option>
                                <option>Summary of Care Received</option>
                                <option>Surgery/Procedure Report</option>
                                <option>Tests</option>
                                <option>Therapy Note</option>
                                <option>Treadmill Report</option>
                                <option>Ultrasound Document</option>
                                <option>Urgent Care Visit Note</option>
                            </select>
                        </div>
                    <?php }?>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="fld_area">
                        <?php echo $form->labelEx($model,'status',array('class'=>'fld_name')); ?>
                        <?php echo $form->hiddenField($model,'status',array('id'=>'status')); ?>
                        <div class="name_fld">
                            <select  id="status_name" name="status" style="border: 1px solid #A6AFAF;">
                                <option value="private">Private</option>
                                <option value="shared">Shared</option>
                            </select>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="fld_area">
                        <?php echo $form->labelEx($model,'note',array('class'=>'fld_name')); ?>
                        <div class="name_fld">
                            <?php echo $form->textArea($model,'note',array('style'=>'margin: 0px 4.734375px 0px 0px;
    height: 96px','placeholder'=> 'First Name','class'=>'fld_class')); ?>
                            <?php echo $form->error($model,'note'); ?>
                        </div>
                        <div class="clear"></div>
                    </div>
                <?php if(isset($model->docFiles) && $model->docFiles){?>
                <div class="fld_area">
                    <?php echo $form->labelEx($model,'attached_files',array('class'=>'fld_name','style'=>'margin-top:10px')); ?>

                    <div class="name_fld">

                     <?php
                    foreach($model->docFiles as $files){

                    $file =  $files->file_url;
                    ?>
                    <div class="massege_title">
                        <span class="sender_label"><?php echo $file ?></span>
                        <!--<span class="sender_body">  <a style="float: left" href="<?php /*echo Yii::app()->createUrl('documents/remove', array('url' =>$files->file_url));*/?>">
                                remove&nbsp;&nbsp;
                            </a></span>-->
                    </div>
                <?php   }?>
                    </div>
            </div>
                <?php }?>

                </div>

            </div>

            <div>
                <span style="margin-left: 300px;">
               <?php echo CHtml::submitButton('Update',array( 'class' => 'registbt'));
                   if (Yii::app()->session['logged_user_type'] == "doctor"||Yii::app()->session['logged_user_type'] == "parcticeAdmin"){
               echo CHtml::link('Cancel',array('doctor/document'),array('type'=>'button','style'=>'font-size:12px','class'=>'registbt')); }
               else if(Yii::app()->session['logged_user_type'] == "patient"){
                    echo CHtml::link('Cancel',array('patient/document'),array('type'=>'button','style'=>'font-size:12px','class'=>'registbt')); }?>

                </span>
            </div>

            <?php $this->endWidget(); ?>
        </div>

    </div>
</div>

<script>
    var label = $('#label_id').val();
    $('#label_name').val(label);

    var status = $('#status').val();
    $('#status_name').val(status);

</script>