<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm */

$this->pageTitle = Yii::app()->name . ' - Login';
$this->breadcrumbs = array(
    'Login',
);

//print "<pre>";
//print_r($_SESSION['logged_user_type']);
//print "</pre>";

?>



<div class="main-loginform">
    <div class="logo"></div>
    <div class="logbox">
        <fieldset id="">
            <?php $form = $this->beginWidget('CActiveForm', array('id' => 'person-form-edit_person-form', 'enableAjaxValidation' => false, 'htmlOptions' => array('onsubmit' => "return false;", /* Disable normal form submit */
                'onkeypress' => " if(event.keyCode == 13){ return signIn(); } " /* Do ajax call when user presses enter key */),)); ?>
            <span id="signin_wrong"
                  style="display:none; color:#ff0000; font-weight:bold;">Wrong User ID and/or Password</span>
            <!-- <span style="padding-bottom:10px; font-size:12px; font-weight:bold;">
                <label style="float:left; display:block; padding-right:10px;"><input type="radio" name="account_type" value="doctor" checked>Doctor</label>
               <label style="float:left; display:block;"> <input type="radio" name="account_type" value="patient" >Patient</label>
            </span>     -->
            <h1>Sign In</h1>

            <input id="username" name="username" value="" title="username" placeholder="User ID" tabindex="2"
                   type="text" autocomplete="off">
            <input id="password" name="password" value="" title="password" placeholder="Password" tabindex="3"
                   type="password" autocomplete="off">

           <span > <div class="g-recaptcha" style="margin-left: 20px;margin-bottom: 10px;"  data-sitekey="6LdH4A4TAAAAAFBuSa1GXBKlXZctnu9htcTcQFWQ"></div></span>
            <span id="captcha" style="margin-left:10px;margin-bottom:10px;color:red" ></span>

            <a class="signin" tabindex="4" id="signin_submit" href="javascript:void(0);" onClick="return signIn();">Sign
                me in</a><!--<input id="signin_submit" value="Go" tabindex="3" type="submit">-->
            <div class="btm_gry">
                <a class="forgotpass" id="forgot_pass"
                   href="<?php echo $this->createAbsoluteUrl('doctor/forgetpassword'); ?>">Forgot password?</a>
                <a class="register" id="forgot_pass" href="<?php echo $this->createAbsoluteUrl('registration/join') ?>">Register?</a>
                <span id="signin_process"></span>


            </div>
            <?php $this->endWidget(); ?>
        </fieldset>
    </div>
</div>
