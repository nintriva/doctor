<?php
$this->pageTitle=Yii::app()->name . ' T&C Privacy Policy';
?>
<!--<div class="wrapper_404">

</div>-->
<div class="main">

    <div style="text-align: center;">

        <h1 style="font-size: 2em; padding-top: 100px;">Please Read and Accept Our Terms and Conditions</h1>

        <div class="regis_contentarea col-sm-10" style="height: 350px; overflow: auto;">

         <div >
                    <h1 class="privacy">Terms of Use</h1>
                    <h2>PLEASE READ THESE TERMS AND CONDITIONS OF USE CAREFULLY BEFORE USING THIS SITE.</h2>

                    <p class="privacy">
                        By using this site, you signify your assent to these Terms and Conditions. If you do not agree to all of these Terms and Conditions of use, do not use this site!</p>
                    <p class="privacy">
                        eDoctorBook Inc may revise and update these Terms and Conditions at any time. Your continued usage of the eDoctorBook website will mean you accept those changes.</p>

                    <h2>The Site Does Not Provide Medical Advice</h2>
                    <p class="privacy">
                        The contents of the eDoctorBook Site, such as text, graphics, images, provider listings, and other material contained on the eDoctorBook Site ("Content") are for informational purposes only. The Content is not intended to be a substitute for professional medical or dental advice, diagnosis, or treatment. Always seek the advice of your doctor, dentist or other qualified health provider with any questions you may have regarding a medical, dental condition. We do not certify accuracy of listings such as provider's address, phone numbers or doctor's or dentist's speciality. Never disregard professional medical advice or delay in seeking it because of something you have read on the eDoctorBook Site!</p>
                    <p class="privacy">
                        If you think you may have a medical emergency, call your doctor or 911 immediately. eDoctorBook does not recommend or endorse any specific tests, dentists, physicians, products, procedures, opinions, or other information that may be mentioned on the Site. Reliance on any information provided by eDoctorBook, eDoctorBook employees, others appearing on the Site at the invitation of eDoctorBook, or other visitors to the Site is solely at your own risk.</p>

                    <h2>Children's Privacy</h2>
                    <p class="privacy">
                        We are committed to protecting the privacy of children. You should be aware that this Site is not intended or designed to attract children under the age of 13. We do not collect personally identifiable information from any person we actually know is a child under the age of 13.</p>
                    <p class="privacy">
                        Use of Content. Provider Relationships and Lists
                        eDoctorBook authorizes you to use material on the eDoctorBook Site solely for your personal, noncommercial use if you include the following copyright notice: "@;2015, eDoctorBook Inc. All rights reserved" and other copyright and proprietary rights notices that are contained in the Content.</p>
                    <p class="privacy">
                        The Content is protected by copyright under both United States and foreign laws. Title to the Content remains with eDoctorBook or its licensors. Any use of the Content not expressly permitted by these Terms and Conditions is a breach of these Terms and Conditions and may violate copyright, trademark, and other laws. Content and features are subject to change or termination without notice in the editorial discretion of eDoctorBook. All rights not expressly granted herein are reserved to eDoctorBook and its licensors.</p>
                    <p class="privacy">
                        In connection with using the Site and the Services to locate or schedule appointments with doctor, dental professionals, you understand that:</p>

                    <h2>YOU ARE ULTIMATELY RESPONSIBLE FOR CHOOSING YOUR OWN DOCTOR or DENTIST.</h2>
                    <p class="privacy">
                        eDoctorBook selects Doctors, Dental Provider to be listed on the Site based on third party data or doctor/dentist's self registration.</p>
                    <p class="privacy">
                        In addition to listing on site, Doctors, Dental Providers must pay a fee to eDoctorBook in order to add offers or featured listings on the Site or other services.</p>
                    <p class="privacy">
                        eDoctorBook will provide you with lists and/or profile previews of Doctors, Dental Providers who may be suitable to deliver the healthcare services that you are seeking based on information that you provide to eDoctorBook (such as proximity to your geographical location, and specialty of the Doctor or Dental Provider). In an effort to aid in the discovery of Doctor, Dental Providers and enable the maximum choice and diversity of Providers who participate in the Services, these lists and/or profile previews may also be based on other criteria (including, for example, Provider availability, past selections by and/or ratings of Providers by you or by other eDoctorBook users, and past experience of eDoctorBook users with Providers); but eDoctorBook (i) does not recommend or endorse any Doctor, Dental Providers, (ii) does not make any representations or warranties with respect to these Doctor, Dental Providers or the quality of the healthcare services they may provide.</p>
                    <p class="privacy">
                        eDoctorBook may also exclude Providers who, in eDoctorBook's discretion, have engaged in inappropriate or unprofessional conduct.</p>

                    <h2>Liability of eDoctorBook and Its associates</h2>
                    <p class="privacy">
                        The use of the eDoctorBook Site and the Content is at your own risk.</p>
                    <p class="privacy">
                        When using the eDoctorBook Site, information will be transmitted over a medium that may be beyond the control and jurisdiction of eDoctorBook and its suppliers. Accordingly, eDoctorBook assumes no liability for or relating to the delay, failure, interruption, or corruption of any data or other information transmitted in connection with use of the eDoctorBook Site.</p>
                    <p class="privacy">
                        The eDoctorBook Site and the content are provided on an "as is" basis. eDoctorBook, ITS LICENSORS, AND ITS SUPPLIERS, TO THE FULLEST EXTENT PERMITTED BY LAW, DISCLAIM ALL WARRANTIES, EITHER EXPRESS OR IMPLIED, STATUTORY OR OTHERWISE, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, NON-INFRINGEMENT OF THIRD PARTIES' RIGHTS, AND FITNESS FOR PARTICULAR PURPOSE. Without limiting the foregoing, eDoctorBook, its licensors, and its suppliers make no representations or warranties about the following:</p>
                    <p class="privacy">
                        1. The accuracy, reliability, completeness, currentness, or timeliness of the Content, software, text, graphics, links, or communications provided on or through the use of the eDoctorBook Site or eDoctorBook.</p>
                    <p class="privacy">
                        2. In no event shall eDoctorBook or any third parties mentioned on the eDoctorBook Site be liable for any damages (including, without limitation, incidental and consequential damages, personal injury/wrongful death, lost profits, or damages resulting from lost data or business interruption) resulting from the use of or inability to use the eDoctorBook Site or the Content, whether based on warranty, contract, tort, or any other legal theory, and whether or not eDoctorBook or any third parties mentioned on the eDoctorBook Site are advised of the possibility of such damages. eDoctorBook or any third parties mentioned on the eDoctorBook Site shall be liable only to the extent of actual damages incurred by you, not to exceed U.S. $100. eDoctorBook or any third parties mentioned on the eDoctorBook Site are not liable for any personal injury, including death, caused by your use or misuse of the Site, Content, or Public Areas (as defined below). Any claims arising in connection with your use of the Site, any Content, or the Public Areas must be brought within one (1) year of the date of the event giving rise to such action occurred. Remedies under these Terms and Conditions are exclusive and are limited to those expressly provided for in these Terms and Conditions.</p>

                    <h2>User Submissions</h2>
                    <p class="privacy">
                        The personal information you submit to eDoctorBook is governed by the eDoctorBook Privacy Policy .</p>
                    <p class="privacy">
                        The Site contains functionality (including blogs, message boards, user reviews of doctors, dentists, etc.) that allows users to upload content to the Site (collectively "Public Areas"). You agree that you will not upload or transmit any communications or content of any type to the Public Areas that infringe or violate any rights of any party. By submitting communications or content to the Public Areas, you agree that such submission is non-confidential for all purposes.</p>
                    <p class="privacy">
                        If you make any such submission you agree that you will not send or transmit to eDoctorBook by email, (including through the email addresses listed on the "Contact Us" page) any communication or content that infringes or violates any rights of any party. If you submit any business information, idea, concept or invention to eDoctorBook by email, you agree such submission is non-confidential for all purposes.</p>
                    <p class="privacy">
                        If you make any submission to a Public Area or if you submit any business information, idea, concept or invention to eDoctorBook by email , you automatically grant-or warrant that the owner of such content or intellectual property has expressly granted eDoctorBook a royalty-free, perpetual, irrevocable, world-wide nonexclusive license to use, reproduce, create derivative works from, modify, publish, edit, translate, distribute, perform, and display the communication or content in any media or medium, or any form, format, or forum now known or hereafter developed. eDoctorBook may sublicense its rights through multiple tiers of sublicenses. If you wish to keep any business information, ideas, concepts or inventions private or proprietary, do not submit them to the Public Areas or to eDoctorBook by email. We try to answer every email in a timely manner, but are not always able to do so.</p>

                    <h2>User Submissions – Image, Video, Audio Files </h2>
                    <p class="privacy">
                        You agree to only post or upload Media (like photos, videos or audio) that you have taken yourself or that you have all rights to transmit and license and which do not violate trademark, copyright, privacy or any other rights of any other person. Photos or videos of celebrities and cartoon or comic images are usually copyrighted by the owner.</p>
                    <p class="privacy">
                        To protect your privacy, you agree that you will not submit any media that contains Personally Identifiable Information (like name, phone number, email address or web site URL) of you or of anyone else. Uploading media like images or video of other people without their permission is strictly prohibited.</p>
                    <p class="privacy">
                        By uploading any media on the eDoctorBook site, you warrant that you have permission from all persons appearing in your media for you to make this contribution and grant rights described herein. Never post a picture or video of or with someone else unless you have their explicit permission.</p>
                    <p class="privacy">
                        It is strictly prohibited to upload media of any kind that contain expressions of hate, abuse, offensive images or conduct, obscenity, pornography, sexually explicit or any material that could give rise to any civil or criminal liability under applicable law or regulations</p>
                    <p class="privacy">
                        You agree that you will not upload any material that contains software viruses or any other computer code , files or programs designed to interrupt, destroy or limit the functionality of any computer software or this Web site.</p>
                    <p class="privacy">
                        By uploading any media like a photo or video, (a) you grant to eDoctorBook a perpetual, non-exclusive, worldwide, royalty-free license to use, copy, print, display, reproduce, modify, publish, post, transmit and distribute the media and any material included in the media; (b) you certify that any person pictured in the submitted media (or, if a minor, his/her parent/legal guardian) authorizes eDoctorBook to use, copy, print, display, reproduce, modify, publish, post, transmit and distribute the media and any material included in such media; and (c) you agree to indemnify eDoctorBook and its affiliates, directors, officers and employees and hold them harmless from any and all claims and expenses, including attorneys' fees, arising from the media and/or your failure to comply with these the terms described in this document.</p>
                    <p class="privacy">
                        eDoctorBook reserves the right to review all media prior to submission to the site and to remove any media for any reason, at any time, without prior notice, at our sole discretion</p>

                    <h2>Passwords</h2>
                    <p class="privacy">
                        eDoctorBook has several tools that allow you to record and store information. You are responsible for taking all reasonable steps to ensure that no unauthorized person shall have access to your eDoctorBook passwords or accounts. It is your sole responsibility to (1) control the dissemination and use of sign-in name, screen name and passwords; (2) authorize, monitor, and control access to and use of your eDoctorBook account and password; (3) promptly inform eDoctorBook if you believe your account or password has been compromised or if there is any other reason you need to deactivate a password. To send us an email, use the "Contact Us" link on our site. You grant eDoctorBook and all other persons or entities involved in the operation of the Site the right to transmit, monitor, retrieve, store, and use your information in connection with the operation of the Site. eDoctorBook cannot and does not assume any responsibility or liability for any information you submit, or your or third parties' use or misuse of information transmitted or received using eDoctorBook tools and services.</p>

                    <h2>eDoctorBook Community and Member to Member Areas ("Public Areas")</h2>
                    <p class="privacy">
                        If you use a Public Area, such as message boards, blogs, Ask Our Expert, User Reviews or other member communities , you are solely responsible for your own communications, the consequences of posting those communications, and your reliance on any communications found in the Public Areas. eDoctorBook and its affiliates are not responsible for the consequences of any communications in the Public Areas. In cases where you feel threatened or believe someone else is in danger, you should contact your local law enforcement agency immediately. If you think you may have a medical emergency, call your doctor or 911 immediately.</p>
                    <p class="privacy">
                        In consideration of being allowed to use the Public Areas, you agree that the following actions shall constitute a material breach of these Terms and Conditions:</p>
                    <p class="privacy">
                        Using a Public Area for any purpose in violation of local, state, national, or international laws;</p>
                    <p class="privacy">
                        Posting material that infringes on the intellectual property rights of others or on the privacy or publicity rights of others;</p>
                    <p class="privacy">
                        Posting material that is unlawful, obscene, defamatory, threatening, harassing, abusive, slanderous, hateful, or embarrassing to any other person or entity as determined by eDoctorBook in its sole discretion;</p>
                    <p class="privacy">
                        Posting advertisements or solicitations of business;</p>
                    <p class="privacy">
                        After receiving a warning, continuing to disrupt the normal flow of dialogue, or posting comments that are not related to the topic being discussed (unless it is clear the discussion is free-form);</p>
                    <p class="privacy">
                        Impersonating another person;</p>
                    <p class="privacy">
                        Distributing viruses or other harmful computer code;</p>
                    <p class="privacy">
                        Harvesting, scraping or otherwise collecting information about others, including email addresses, without their identification for posting or viewing comments;</p>
                    <p class="privacy">
                        Allowing any other person or entity to use your identification for posting or viewing comments
                        Posting the same note more than once or "spamming"; or</p>
                    <p class="privacy">
                        Engaging in any other conduct that restricts or inhibits any other person from using or enjoying the Public Area or the Site, or which, in the judgment of eDoctorBook, exposes eDoctorBook or any of its customers or suppliers to any liability or detriment of any type.</p>
                    <p class="privacy">
                        eDoctorBook Reserves the Right (but is Not Obligated) to Do Any or All of the Following:
                        Record the dialogue in public chat rooms.</p>
                    <p class="privacy">
                        Investigate an allegation that a communication(s) do(es) not conform to the terms of this section and determine in its sole discretion to remove or request the removal of the communication(s).</p>
                    <p class="privacy">
                        Remove communications which are abusive, illegal, or disruptive, or that otherwise fail to conform with these Terms and Conditions.
                        Terminate a user's access to any or all Public Areas and/or the eDoctorBook Site upon any breach of these Terms and Conditions.
                        Monitor, edit, or disclose any communication in the Public Areas.</p>
                    <p class="privacy">
                        Edit or delete any communication(s) posted on the eDoctorBook Site, regardless of whether such communication(s) violate these standards. eDoctorBook have no liability or responsibility to users of the eDoctorBook Site or any other person or entity for performance or nonperformance of the aforementioned activities.</p>

                    <h2>Advertisements, Searches, and Links to Other Sites</h2>
                    <p class="privacy">
                        eDoctorBook may provide links to third-party web sites. eDoctorBook also may select certain sites as priority responses to search terms you enter and eDoctorBook may agree to allow advertisers to respond to certain search terms with advertisements or sponsored content. eDoctorBook does not recommend and does not endorse the content on any third-party websites. eDoctorBook is not responsible for the content of linked third-party sites, sites framed within the eDoctorBook Site, third-party sites provided as search results, or third-party advertisements, and does not make any representations regarding their content or accuracy. Your use of third-party websites is at your own risk and subject to the terms and conditions of use for such sites. eDoctorBook does not endorse any product, service, or treatment advertised on the eDoctorBook Site.</p>

                    <h2>Indemnity</h2>
                    <p class="privacy">
                        You agree to defend, indemnify, and hold eDoctorBook, its officers, directors, employees, agents, licensors, and suppliers, harmless from and against any claims, actions or demands, liabilities and settlements including without limitation, reasonable legal and accounting fees, resulting from, or alleged to result from, your violation of these Terms and Conditions.</p>

                    <h2>Jurisdiction</h2>
                    <p class="privacy">
                        You expressly agree that exclusive jurisdiction for any dispute with eDoctorBook, or in any way relating to your use of the eDoctorBook Site, resides in the courts of the State of Delaware and you further agree and expressly consent to the exercise of personal jurisdiction in the courts of the State of Delaware in connection with any such dispute including any claim involving eDoctorBook or its affiliates, subsidiaries, employees, contractors, officers, directors, telecommunication providers, and content providers.</p>
                    <p class="privacy">
                        These Terms and Conditions are governed by the internal substantive laws of the State of Delaware, without respect to its conflict of laws principles. If any provision of these Terms and Conditions is found to be invalid by any court having competent jurisdiction, the invalidity of such provision shall not affect the validity of the remaining provisions of these Terms and Conditions, which shall remain in full force and effect. No waiver of any of these Terms and Conditions shall be deemed a further or continuing waiver of such term or condition or any other term or condition.</p>

                    <h2>Notice and Takedown Procedures; and Copyright Agent</h2>
                    <p class="privacy">
                        If you believe any materials accessible on or from the Site infringe your copyright, you may request removal of those materials (or access thereto) from this web site by contacting eDoctorBook's copyright agent (identified below) and providing the following information:</p>
                    <p class="privacy">
                        Identification of the copyrighted work that you believe to be infringed. Please describe the work, and where possible include a copy or the location (e.g., URL) of an authorized version of the work.</p>
                    <p class="privacy">
                        Identification of the material that you believe to be infringing and its location. Please describe the material, and provide us with its URL or any other pertinent information that will allow us to locate the material.
                        Your name, address, telephone number and (if available) e-mail address.</p>
                    <p class="privacy">
                        A statement that you have a good faith belief that the complained of use of the materials is not authorized by the copyright owner, its agent, or the law.</p>
                    <p class="privacy">
                        A statement that the information that you have supplied is accurate, and indicating that "under penalty of perjury," you are the copyright owner or are authorized to act on the copyright owner's behalf.</p>
                    <p class="privacy">
                        A signature or the electronic equivalent from the copyright holder or authorized representative.
                        eDoctorBook's agent for copyright issues relating to this web site is as follows:</p>
                    <p class="privacy">
                        info@eDoctorBook.com</p>
                    <p class="privacy">
                        In an effort to protect the rights of copyright owners, eDoctorBook maintains a policy for the termination, in appropriate circumstances, of subscribers and account holders of the Site who are repeat infringers.</p>

                    <h2>Complete Agreement</h2>
                    <p class="privacy">
                        Except as expressly provided in a particular "legal notice" on the eDoctorBook Site, these Terms and Conditions and the eDoctorBook Privacy Policy constitute the entire agreement between you and eDoctorBook with respect to the use of the eDoctorBook Site, and Content.</p>
                    <p class="privacy">
                        Last Updated: April 6, 2015</p>
                    <p class="privacy">
                        @ 2015 eDoctorBook Inc. All rights reserved.</p>

                </div>
         <div >

                    <h1 class="privacy">PRIVACY POLICY:</h1>

                    <p class="privacy">eDoctorBook takes your privacy seriously. We want to make your online experience satisfying and safe.
                        Last Updated on April 6, 2015.</p>

                    <h2>1. Introduction</h2>
                    <p class="privacy">
                        eDoctorBook, Inc. (“us”, ”we”, or “eDoctorBook”) is committed to respecting the privacy rights of our customers, visitors, and other users of eDoctorBook.com (the “Site”) and services provided by eDoctorBook through the Site (collectively, the “Services”). We created this Privacy Policy
                        (“Privacy Policy”) to give you confidence as you visit and use the Services, and to demonstrate our commitment to fair information practices and the protection of privacy. This Privacy Policy is only applicable to our Site and information obtained by us from any of our marketing affiliates, and
                        not to any other websites that you may be able to access from the Site or any website of eDoctorBook’s business partners, each of which may have data collection, storage, and use practices and policies that differ materially from this Privacy Policy. Your use of the Services is
                        governed by this Privacy Policy and Terms of Use.</p>
                    <p class="privacy">
                        BY USING THE SITE, SERVICES, AND/OR BY REGISTERING WITH US, YOU ACCEPT THE PRACTICES AND POLICIES OUTLINED IN THIS PRIVACY POLICY, AND YOU HEREBY
                        CONSENT THAT WE WILL TRACK, COLLECT, USE, AND SHARE YOUR INFORMATION IN THE FOLLOWING WAYS. IF YOU ARE REGISTERING AN ACCOUNT OR USING THE
                        SERVICES ON BEHALF OF AN INDIVIDUAL OR ENTITY OTHER THAN YOURSELF, YOU REPRESENT THAT YOU ARE AUTHORIZED BY SUCH INDIVIDUAL OR ENTITY TO ACCEPT
                        THIS PRIVACY POLICY ON SUCH INDIVIDUAL’S OR ENTITY’S BEHALF.</p>

                    <h2>2. Traffic Data Collected</h2>
                    <p class="privacy">
                        We automatically track and collect the following categories of information when you visit our Site: (1) IP addresses; (2) domain servers; (3) types of computers accessing the Site; (4) types of web browsers used to access the Site; (5) referring source which may have sent you to the Site; and (6) other information associated with the interaction of your browser and the Site (collectively "Traffic Data").</p>

                    <h2>3. Personal Information Collected</h2>
                    <p class="privacy">
                        In order for you to access certain areas of the Site or certain Services where it is important for us to know who you are so that we can best meet your needs, we may require you to provide us with certain information that personally identifies you (“Personal Information”). Personal Information
                        includes the following categories of information: (1) contact data (such as your e­mail address, phone number and password); (2) demographic data (such as your gender, your date of birth and your zip code); (3) insurance data (such as your insurance carrier and insurance plan); and
                        (4) medical data (such as the previous doctors, dentists or other health care providers (“Providers”) you visited, your reason for visiting Providers, your date of visiting Providers, your medical history, and other medical and health information you choose to share with us). If you communicate with us by, for example, e­mail or letter, any information provided in such communication may be collected as Personal Information.
                    </p>

                    <h2>4. Interactive and Installed Tools</h2>
                    <p class="privacy">
                        Access to some eDoctorBook interactive tools and services may be limited to users who have registered. Some of our tools (such as certain cost calculators) do not retain your Personal Information, while others (such as Dental Cleaning schedule) store your Personal Information in
                        accordance with this Privacy Policy and the authorization you provide at the time you submit Personal Information in order to use the tool. eDoctorBook Health Manager, which allows you a secure place to gather, store, manage and share your Personal Information and provides tools
                        and services to better manage your physical health and the oral health of your family, requires additional registration. eDoctorBook Health Manager has the ability to use Personal Information that you provide to send you personalized emails or secure electronic messages. However,
                        eDoctorBook does not allow any third parties to place beacons or cookies or to gather any data about you in connection with your use of the eDoctorBook Health Manager.</p>

                    <h2>5. Cookies</h2>
                    <p class="privacy">
                        We may use small computer files that are transferred to your computer's hard drive that contain information such as user ID, user preferences, lists of pages visited and activities conducted while browsing the Site ("Cookies") to help us improve our Site by tracking your navigation habits and to store some of your preferences. Generally, we use Cookies to customize your experience on our Site and to store your password so you do not have to re-enter it each time you visit the Site. Cookies do not allow our site to gain access to other information on your computer. At your option, expense and responsibility, you may block Cookies or delete Cookies from your hard drive. However, by disabling Cookies, you may not have access to the entire set of features of the Site.
                        In addition, our business partners may use Cookies to provide us with anonymous data and information regarding the use of our Services. Specifically, some of our business partners use Cookies to show eDoctorBook ads on other sites on the internet as a result of you using the Services. Such Cookies do not contain any Personal Information. You may opt out of receiving Cookies placed by such third party vendors by visiting the Network Advertising Initiative opt out page.
                        Other Cookies used by our business partners may collect other non-personally identifying information, such as the computer's IP address, type of operating system, type of internet browsing software, what web pages were viewed at what time, the geographic location of your internet service provider and demographic information, such as gender and age range. This information is used to provide eDoctorBook with more information about our user's demographics and internet behaviors.
                        We do not link the information stored in these Cookies directly to any of your Personal Information you submit while on the Site.</p>

                    <h2>6. Web Beacons</h2>
                    <p class="privacy">
                        In limited circumstances we also may use tiny graphic image files imbedded in a web page or email that provide a presence on the web page or email and send back to its home server information from the user's browser ("Web Beacons") to collect anonymous, non-personal information about your use of our Services and the sites of selected sponsors and advertisers, and your use of emails, special promotions or newsletters we send to you. The information collected by Web Beacons allows us to statistically monitor how many people are using the Services and selected sponsors' and advertisers' sites, or opening our emails, and for what purposes.</p>

                    <h2>7. Website Analytics</h2>
                    <p class="privacy">
                        We may use third party website analytics services in connection with the Services, for example, to record mouse clicks, mouse movements, scrolling activity, as well as any text that you type into the Site. These website analytics services do not collect Personal Information that you do not voluntarily enter into the Site. These services do not track your browsing habits across websites which do not use their services. We use the information collected from these services to find usability problems to make the Services easier to use. The recordings will never identify you or your account; they only record anonymous user information.</p>

                    <h2>8. Storage</h2>
                    <p class="privacy">
                        We store all Traffic Data and review postings indefinitely, even after "deletion," and may archive such information elsewhere. We store all Personal Information until you request that we modify or delete it, in which case we may still retain certain Personal Information for the reasons described in Section 18 below.</p>

                    <h2>9. Information Provided on Behalf of Children</h2>
                    <p class="privacy">
                        eDoctorBook does not collect information from children under the age of 13 in accordance with the Childrens's Online Privacy Protection Act ("COPPA") as discussed below. If you are a parent or legal guardian of a minor child, you may, in compliance with the Terms of Use, use the Services on behalf of such minor child. Any information that you provide us while using the Services on behalf of your minor child will be treated as Personal Information as otherwise provided herein.</p>

                    <h2>10. Children's Online Privacy Protection Act</h2>
                    <p class="privacy">
                        COPPA severely restricts what information can be collected from children under the age of 13. For this reason, children under the age of 13 in the United States are prohibited from using the Services. The Services are not directed at children, and eDoctorBook does not knowingly collect any information from individuals under the age of 13. If we learn that we have received any information from an individual under the age of 13, we will use that information only to respond directly to that child (or a parent or legal guardian) to inform him or her that he or she cannot use the Services and subsequently we will delete that information from our own servers.</p>

                    <h2>11. eDoctorBook's Use of Your Information</h2>
                    <p class="privacy">
                        We may use your Personal Information to recommend certain resources or Providers. We may use your contact data to send you information about eDoctorBook or our products or Services, to contact you when necessary, including to remind you of upcoming or follow-up appointments, and in conjunction with your use of certain Interactive Tools. We may use your demographic data, Traffic Data, insurance data or medical data to customize and tailor your experience through the Services, in emails and in other communications, displaying content that we think you might be interested in and according to your preferences. We may also use your anonymized Personal Information to run (or authorize third parties to run) statistical research on individual or aggregate oral health or dental trends. Such research would only use your Personal Information in an anonymous manner that cannot be tied directly back to you.</p>

                    <h2>12. Sharing of Information</h2>
                    <p class="privacy">
                        We share certain categories of information we collect from you in the ways described in this Privacy Policy, including as described below:
                        You may choose to authorize eDoctorBook to share your Personal Information with selected Dentists and Providers.
                        We may share your contact data, Traffic Data, demographic data, insurance data and medical data with Dentists and Providers you choose to schedule through the Services.
                        In order to customize your advertising interactions, we may share Personal Information with marketing affiliates and other third parties only on an aggregate (i.e., anonymized) basis.
                        We may share your anonymized Personal Information with third parties to enable them to run statistical research on individual or aggregate health or medical trends.
                        We share Personal Information and Traffic Data with our business partners who assist us by performing core services (such as hosting, billing, fulfillment, or data storage and security) related to our operation of the Services and/or by making certain Interactive Tools available to our users. Those business partners shall be bound to uphold the same standards of security and confidentiality that we have promised to you in this Privacy Policy, and they will only use your contact data and other Personal Information to carry out their specific business obligations to eDoctorBook and to provide your requested dental care and services.
                        We may share your contact data with our subcontractors to enable them to send you eDoctorBook promotional materials. These subcontractors will only use your contact data to send you eDoctorBook promotional materials.
                        We may share certain marketing metrics including, but not limited to, anonymized Personal Information and non-personal Information, with our business partners to help determine the effectiveness of certain advertising campaigns.</p>

                    <h2>13. User Choice</h2>
                    <p class="privacy">
                        You may choose not to provide us with any Personal Information. In such an event, you can still access and use some of the Services; however you will not be able to access and use certain key features of the Services that require your Personal Information.</p>

                    <h2>14. Confidentiality and Security</h2>
                    <p class="privacy">
                        Except as otherwise provided in this Privacy Policy, we will keep your Personal Information private and will not share it with third parties, unless we believe in good faith that disclosure of your Personal Information or any other information we collect about you is necessary to: (1) comply with a court order or other legal process; (2) protect the rights, property or safety of eDoctorBook or another party; (3) enforce our Terms of Use; or (4) respond to claims that any posting or other content violates the rights of third-parties.</p>

                    <h2>15. Dentists and Doctors</h2>
                    <p class="privacy">
                        Dentists, doctors, their employees, and their agents and all other entities defined under HIPPA Healthcare Provider definitions should be particularly aware of their obligations to patient confidentiality, including without limitation their obligations under the Health Insurance Portability and Accountability Act (“HIPAA”), both in communicating with eDoctorBook and in responding to a review of their services posted on our Site. eDoctorBook does not have, and will not accept, any obligations of confidentiality with respect to any communications other than those expressly stated in this Privacy Policy and eDoctorBook’s Terms of Use.</p>

                    <h2>16. Public Information</h2>
                    <p class="privacy">
                        Any information that you may reveal in a review posting or other online discussion or forum is intentionally open to the public and is not in any way private. You should think carefully before disclosing any personally identifiable information in any public forum. What you have written may
                        be seen and/or collected by third parties and may be used by others in ways we are unable to control or predict.</p>

                    <h2>17. Security</h2>
                    <p class="privacy">
                        The security of your Personal Information is important to us. We follow generally accepted industry standards to protect the Personal Information submitted to us, both during transmission and once we receive it. For example, when you enter sensitive information on our Site, we encrypt that information using secure socket layer technology ("SSL").
                        Although we make good faith efforts to store Personal Information in a secure operating environment that is not open to the public, you should understand that there is no such thing as complete security, and we do not guarantee that there will be no unintended disclosures of your Personal Information. If we become aware that your Personal Information has been disclosed in a manner not in accordance with this Privacy Policy, we will use reasonable efforts to notify you of the nature and extent of the disclosure (to the extent we know that information) as soon as reasonably possible and as permitted by law.</p>

                    <h2>18. Lost or Stolen Information</h2>
                    <p class="privacy">
                        You must promptly notify us if your contact data is lost, stolen, or used without permission. In such an event, we will remove that contact data from your account and update our records accordingly.</p>

                    <h2>19. Updates and Changes to Privacy Policy</h2>
                    <p class="privacy">
                        We reserve the right, at any time, to add to, change, update, or modify this Privacy Policy so please review it frequently. If we do, then we will post the amended Privacy Policy on the Site; we may also attempt to notify you in some other way. In all cases, use of information we collect is subject to the Privacy Policy in effect at the time such information is collected.</p>

                    <h2>20. Controlling Your Personal Information</h2>
                    <p class="privacy">
                        As a registered user of the Services, you can modify some of the Personal Information you have included in your profile or change your username by logging in and accessing your account. Upon your request, eDoctorBook will use commercially reasonable efforts to delete your account and the Personal Information in your profile; however, it may be impossible to remove your account without some residual information being retained by eDoctorBook. Registered users who wish to close their account should email: privacy@eDoctorBook.com.</p>

                    <h2>21. Links to Other Websites</h2>
                    <p class="privacy">
                        The Site contains links to third party websites to which eDoctorBook has no affiliation. eDoctorBook does not share your Personal Information with those websites and is not responsible for their privacy practices. Some websites may have the look and feel of our Site. Please be aware that you may be on a different site and that this Privacy Policy only covers our Site. A link to a non-eDoctorBook website does not constitute or imply endorsement by eDoctorBook. Additionally, we cannot guarantee the quality or accuracy of information presented on non-eDoctorBook websites. Should you decide to visit one of these third party websites, we suggest that you read its privacy policy.</p>

                    <h2>22. Contacts</h2>
                    <p class="privacy">
                        If you have any comments, concerns or questions about this Privacy Policy, please contact us at privacy@eDoctorBook.com</p>
                    <p class="privacy">
                        Last Updated on April 6, 2015.</p>
                    <p class="privacy">
                        Copyright © 2015 eDoctorBook, Inc. All Rights Reserved.</p>

                </div>



    </div>
    <div class="clearfix"> </div>


        <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'tc',
        )); ?>

        <div class="box_content">
            <input type="checkbox" value="1" id="checkbox" name="checkbox" checked="checked">
            <span> I read and Accept Terms and Conditions , Privacy policy</span>
        </div>
    </div>
    <div style="text-align: center;">
                <span>
                <?php echo CHtml::submitButton('Accept and Continue',array('class'=>'registbt')); ?>
                </span>
    </div>
    <?php $this->endWidget(); ?>
</div>
</div>
