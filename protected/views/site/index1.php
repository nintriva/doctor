<?php
/* @var $this SiteController */

//$this->pageTitle=Yii::app()->name;
?>

<script src="<?php echo Yii::app()->request->baseUrl; ?>/assets/js/zelect.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/assets/js/jquery.editableSelect.js"></script>
<script>
    $(function () {
        //$('#geo_location_id').editableSelect().change(function(){
        //var log = $('#log');
        //log.text(log.text() + 'Select: ' + this.id + ' changed. Value: ' + this.value  + '\r\n');
        //});
    });
</script>
<div class="slider_new">
    <div class="flexslider_new">
        <ul class="slides">
            <?php /* ?><li> <img src="<?php echo Yii::app()->request->baseUrl; ?>/assets/images/bannerpicx1.jpg" alt="" /> </li>
              <li> <img src="<?php echo Yii::app()->request->baseUrl; ?>/assets/images/bannerpicx2.jpg" alt=""/></li><?php */ ?>
            <li> <img src="<?php echo Yii::app()->request->baseUrl; ?>/assets/images/bannerpicx3.jpg" alt=""/></li>
        </ul>       
    </div> 
    <div>
        <br><br><br><br><br>
    </div>
    <div class="main">
			<?php if(isset($_GET['invite']) && $_GET['invite'] != ''){ 
				$sql_inv='select title,first_name,last_name,slug,speciality FROM da_doctor WHERE status = "1" and md5(npi_no) = "'.$_GET['invite'].'"';
				$connection = Yii::app()->db;
				$command = $connection->createCommand($sql_inv);
				$user_inv = $command->queryAll();
			?>
			<div class='inviteBox'>
				<a href='<?php echo $this->createAbsoluteUrl('doctor/'.$user_inv[0]["slug"].'|'.$user_inv[0]["speciality"].'?invited='.$_GET['invite']); ?>'><img src="<?php echo Yii::app()->request->baseUrl; ?>/assets/images/invitation.png"  width='400px' />
                <h3><?php echo $user_inv[0]['title'] . " " . $user_inv[0]['first_name']. " " . $user_inv[0]['last_name']; ?></h3>
				<p>to join eDoctorBook</p></a>
			</div>
			<?php } ?>
        <div <?php if(isset($_GET['invite']) && $_GET['invite'] != ''){ ?>class="searchbg_new_1"<?php }else{ ?>class="searchbg_new"<?php } ?> >
            <form class="home-search searchForm specialization-search" id="home_search" name="home_search" method="GET" action="<?php echo Yii::app()->request->baseUrl; ?>/doctor/doctorSearch" onsubmit="return callSubmit();">
                <input name="city" type="hidden" value=""/>
                <input name="searchfor" value="specialization" type="hidden"/>
                <div class="home-search-tab-container">
                    <a href="javascript:void(0);" class="home-search-tab-selected" id="SPECIALTY">SPECIALTY</a>
                    <a href="javascript:void(0);" class="home-search-tab" id="DOCTOR">DOCTOR</a>
                    <div class="home-search-main-label">Find the best Doctor for you!</div>
                </div>
                <div class="home-search-container">
                    <div class="search-field-wrapper">    
                        <span><img src="<?php echo Yii::app()->request->baseUrl; ?>/assets/images/speciality_icon.png" /></span>                    
                        <?php
                        $selected_doctor_speciality = "";
                        echo CHtml::dropDownList('speciality_id', $selected_doctor_speciality, $user_speciality, array('empty' => 'Select speciality', 'class' => 'fld_class', 'style' => 'display:none;'));
                        ?>
                        <span><img src="<?php echo Yii::app()->request->baseUrl; ?>/assets/images/location.png" style="padding-left:5px;" /></span>  

                        <span id="intro">
                            <div style="float:left; width: 100%; position:relative">                              
						   
								   <script src="http://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places" 
								   type="text/javascript"></script>
									<script type="text/javascript">
										   function initialize() {
													var options = {
															types: ['(locality)'],
															componentRestrictions: {country: "US"}
													};
												   var input = document.getElementById('searchTextField');
												   var autocomplete = new google.maps.places.Autocomplete(input);
										   }
										   google.maps.event.addDomListener(window, 'load', initialize);
								   </script> 
								<input id="searchTextField" type="text" size="50" placeholder="Enter a location" autocomplete="on">
                            </div>
                        </span>


                    </div>
                    <div class="search-field-wrapper" style="display:none">
                        <div class="doctor-search" >
                            <input type="text" id="name_autocomp" name="name_autocomp" placeholder="Enter doctor's name" />
                        </div>
                    </div>
                    <div class="search-field-wrapper" style="display:none">
                        <div class="practice-search" >
                            <input type="text" name="office_name" placeholder="eg. Smile Dental Clinic" />
                        </div>
                    </div>
                    <div class="search-button-wrapper">
					
						<input type="hidden" name="radius" value="radius"/>
                        <input type="submit" value="Search" class="search-button"/>
                    </div>
                    <div class="clear"></div>
                </div>
            </form>

        </div>
    </div>
</div>
<input type="hidden" name="hidden_local_value" id="hidden_local_value" />
<style>
    .esTextBox{font-size: 18px !important;}
</style>
<script>
    /*var info = document.getElementById('hidden_local_value');
     var lat = geoip_latitude();
     var lon = geoip_longitude();
     var city = geoip_city();
     var out = '<h3>Information from your IP</h3>'+
     '<ul>'+
     '<li>Latitude: ' + lat + '</li>'+
     '<li>Longitude: ' + lon + '</li>'+
     '<li>City: ' + city + '</li>'+
     '<li>Region: ' + geoip_region() + '</li>'+
     '<li>Region Name: ' + geoip_region_name() + '</li>'+
     '<li>Postal Code: ' + geoip_postal_code() + '</li>'+
     '<li>Country Code: ' + geoip_country_code() + '</li>'+
     '<li>Country Name: ' + geoip_country_name() + '</li>'+
     '</ul>'*/
    //var city = geoip_city();
    var city = "San Francisco";
    //var out = city + '-'+geoip_region_name() + '-'+geoip_country_name();
    /*var lat = geoip_latitude();
     var lon = geoip_longitude();*/
    var lat = "37.774929";
    var lon = "-122.419416";
    var sum = '';
    /*var geoip_region_name_split = geoip_region_name().split(" ");
     var sum = '';
     $.each( geoip_region_name_split, function( index, value ){
     sum += value.substring(0, 1); ;
     });*/
    var out = city + '-' + sum;
    //alert(out);
    document.getElementById('hidden_local_value').value = out;
    GEOajax($('#base_url').val() + "/site/ajaxLatLong?accuracy=&latlng=" + lat + "," + lon + "&altitude=&altitude_accuracy=&heading=&location_loc_add=" + $('#hidden_local_value').val() + "&speed=");
</script>

<div class="appointment_sect">
    <div class="main">
        <h1>Why use eDoctorBook?</h1>
        <div class="appoinment_section">
            <div class="appoinmrnt_box">
                <img src="<?php echo Yii::app()->request->baseUrl; ?>/assets/images/millions_doct_picx.jpg" alt="" >
                <h1><span>1.</span> Select from more than 1 Million Doctors, Dentists, and other Specialists</h1>
                <p>With more than 1 million practitioners across the country to choose from, you can find exactly the right one for you.</p>
            </div>
            <div class="appoinmrnt_box">
                <img src="<?php echo Yii::app()->request->baseUrl; ?>/assets/images/search_box_picx.jpg" alt="" class="search" >
                <h1><span>2.</span>Find doctors, review profiles and schedule appointments with a tap or a click</h1>
                <p>Everything you need to locate the right practitioner and book an appointment is right here.  And it is available 24/7/365 from anywhere in the world.  No calls, no music-on-hold, and no hassles.</p>
            </div>
            <div class="appoinmrnt_box_last">
                <img src="<?php echo Yii::app()->request->baseUrl; ?>/assets/images/free_memebers_picx.jpg" alt="" >
                <h1><span>3.</span>Never a charge; No credit card needed</h1>
                <p>Our service is free for patients.  We have an experienced team dedicated to making your search and booking fast, accurate and easy.</p>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function () {
        /*$("#name_autocomp").autocomplete({
         source: "<?php echo Yii::app()->request->baseUrl; ?>/site/doctorNameAutocomplete",
         minLength: 1,
         select: function(event, ui) {
         $('#name_autocomp').attr('value', ui.item.label);
         $('#doctor_id').attr('value', ui.item.id);
         
         return false;
         }
         });*/
        $("#speciality_id").selectbox();
        //$("#geo_location_id").selectbox();
    });
    $(".home-search-tab-container a").click(function () {
        var eq_no = $(".home-search-tab-container a").index(this);
        $(".home-search-tab-container a").removeClass('home-search-tab-selected');
        $(".home-search-tab-container a").addClass('home-search-tab');
        $(this).removeClass('home-search-tab');
        $(this).addClass('home-search-tab-selected');
        $("div .search-field-wrapper").eq(eq_no).css('display', 'block');
        $("div .search-field-wrapper").css('display', 'none');
        $("div .search-field-wrapper").eq(eq_no).css('display', 'block');
        //return false;
    });
    function setup() {
        $('#intro select').zelect({ /*placeholder:'Plz select...'*/})
    }
    function onlyDigit(str) {
        ///alert(str);
    }
	
	
	
	

	
	$("#geo_location_id").blur(function(){
			var zip=$(this).val();
			if(zip!=""){
			$.post("<?php echo Yii::app()->request->baseUrl; ?>/doctor/checkZip", {"zip":zip},
					function(response) {
					  if(response==0){
					   alert('Invalid Zip Code');
						$("#geo_location_id").val('');
					  }
					});

			 }		

});

function callSubmit()
	{
	 var speciality_id=$("#speciality_id").val();
	 var zip=$("#geo_location_id").val().trim();
	 var name=$("#name_autocomp").val();
	 if($('.home-search-tab-selected').attr("id")=="SPECIALTY"){
		 
		 if(speciality_id=="") 
		 {
			 
		     $("#speciality_id").next('.sbHolder').find('.sbSelector').addClass('filed_error');
			 
			 $("#speciality_id").next('.sbHolder').find('.sbSelector').html('Select Speciality');
			 
			 return false;
		 }
		 
		 else if(zip==""||zip=="Enter Zip Code"){ 
		 
		  $("#esTextBox").addClass('filed_error');
		  $("#esTextBox").text('Please Enter Zip');
		 return false;
		 
		 }else {return true;}
	 }
	 else {
	  if(name=="")  { 
	  
	
	   $("#name_autocomp").attr('placeholder','Please Enter Name'); 
	   $("#name_autocomp").addClass('filed_error');
	  
	  return false;}else { 


 $("#name_autocomp").attr('placeholder','Enter doctor\'s name'); 
	   $("#name_autocomp").removeClass('filed_error');

	  return true;}
	  
	  }
	}
	
	

$(".home-search-tab-container a").click(function(){
	
	$(".span_error").html('');

	  $("#name_autocomp").attr('placeholder','Enter Doctor\'s name'); 
	   $("#name_autocomp").removeClass('filed_error');
	   var str=$("#speciality_id").val();
	   if(str==""){
			 
			 $("#speciality_id").next('.sbHolder').find('.sbSelector').html('Select Speciality');  
	       }
	       $("#speciality_id").next('.sbHolder').find('.sbSelector').removeClass('filed_error');
	
});


$("#speciality_id").change(function(){
	
	var str=$(this).val();
	
	if(str!=""){
		
				     $("#speciality_id").next('.sbHolder').find('.sbSelector').removeClass('filed_error');

	}
	
});
</script>

<style>
.filed_error{border:1px solid #dd0000 !important;}
</style>