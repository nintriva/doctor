<?php
$this->pageTitle=Yii::app()->name . ' - Session Timeout';
?>
<div class="wrapper_404">
<div class="main">
    <div class="leftHolder">
        <div class="errorNumber403"><p style="font-size: 30px;">Session Timeout !!</p></php></div>
    </div>
    <div class="rightHolder">


            <div class="message"><p>Session timed out. Please login again to continue.</p></div>
            <div class="robotik"><img src="<?php echo Yii::app()->request->baseUrl; ?>/assets/images/doctor404_icon.png" alt="Session timed out" title="Session timed out" id="robot"></div>


        <div class="tryToMessage">
            Try to:
            <ul>

                <li>Visit the <a href="<?php echo $this->createAbsoluteUrl('site/index'); ?>" title="Robotik Sitemap">Home</a></li>
                <li>Go <a href="javascript:window.history.back();" title="Back">back</a></li>
            </ul>
        </div>
    </div>
</div>
</div>
