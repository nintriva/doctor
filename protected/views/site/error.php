<?php
/* @var $this SiteController */
/* @var $error array */

$this->pageTitle=Yii::app()->name . ' - Error';
$this->breadcrumbs=array(
	'Error',
);

?>

<?php /*?><h2>Error <?php echo $code; ?></h2>

<?php echo CHtml::encode($message); ?><?php */?>

<div class="wrapper_404">
<div class="main">
    <div class="leftHolder">
			<div class="errorNumber403"><p><?php echo $error['code'] ?></p></php></div>
    </div>
    <div class="rightHolder">
        <?php if($error['code']=='403' || $error['code']=='400'){
            ?>
            <div class="message"><p> <?php echo $error['message'] ?></p></div>
            <div class="robotik"><img src="<?php echo Yii::app()->request->baseUrl; ?>/assets/images/doctor404_icon.png" alt="Oooops....we can't find that page." title="Oooops....we can't find that page." id="robot"></div>
            <?php
        }else{ ?>

                <div class="message"><p>The page you are looking for might have been removed, had its name changed, or is temporarily unavailable.</p></div>
            <div class="robotik"><img src="<?php echo Yii::app()->request->baseUrl; ?>/assets/images/doctor404_icon.png" alt="Oooops....we can't find that page." title="Oooops....we can't find that page." id="robot"></div>
        <?php
        }  ?>

                <div class="tryToMessage">
                    Try to:
                    <ul>
                        
                        <li>Visit the <a href="<?php echo $this->createAbsoluteUrl('site/index'); ?>" title="Robotik Sitemap">Home</a></li>
                        <li>Go <a href="javascript:window.history.back();" title="Back">back</a></li>
                    </ul>
                </div>
              </div>
</div>
</div>