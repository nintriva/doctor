<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - LoginDpa';
$this->breadcrumbs=array(
	'Login',
);

//print "<pre>";
//print_r($_SESSION['logged_user_type']);
//print "</pre>";

?>

<!-- <h1>Login</h1>

<p>Please fill out the following form with your login credentials:</p>

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'login-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<div class="row">
		<?php echo $form->labelEx($model,'username'); ?>
		<?php echo $form->textField($model,'username'); ?>
		<?php echo $form->error($model,'username'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'password'); ?>
		<?php echo $form->passwordField($model,'password'); ?>
		<?php echo $form->error($model,'password'); ?>
		<p class="hint">
			Hint: You may login with <kbd>demo</kbd>/<kbd>demo</kbd> or <kbd>admin</kbd>/<kbd>admin</kbd>.
		</p>
	</div>

	<div class="row rememberMe">
		<?php echo $form->checkBox($model,'rememberMe'); ?>
		<?php echo $form->label($model,'rememberMe'); ?>
		<?php echo $form->error($model,'rememberMe'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Login'); ?>
	</div>

<?php $this->endWidget(); ?>
</div><!-- form -->

<div class="main-loginform">
	<div class="logo"></div>
	<div class="logbox">
	<fieldset id="">
	<?php $form=$this->beginWidget('CActiveForm',array( 'id'=>'person-form-edit_person-form', 'enableAjaxValidation'=>false, 'htmlOptions'=>array( 'onsubmit'=>"return false;",/* Disable normal form submit */
	'onkeypress'=>" if(event.keyCode == 13){ return signInDpa(); } " /* Do ajax call when user presses enter key */ ), )); ?>

     <br>
     <br>
	<span id="signin_wrong" style=" display:none;color:#ff0000; font-weight:bold;">Wrong User ID and/or Password</span>
     <h1>Sign In as Parctice Admin</h1>
	<input id="username" name="username" value="" title="username" placeholder="User ID" tabindex="2" type="text" autocomplete="off">
	<input id="password" name="password" value="" title="password" placeholder="Password" tabindex="3" type="password" autocomplete="off">


	<a class="signin" tabindex="4" id="signin_submit" href="javascript:void(0);" onClick="return signInDpa();">Sign in</a>
	<div class="btm_gry">

	<a class="forgotpass" id="forgot_pass" href="<?php echo $this->createAbsoluteUrl('PaAdminDoctors/forgetpassword'); ?>">Forgot password?</a>
	<?php /*?><a class="register" id="forgot_pass" href="<?php echo $this->createAbsoluteUrl('registration/join')?>">Register?</a>	*/ ?>

	 <span id="signin_process"></span>
	 <?php /*
	 <a id="fb-root" href="javascript:void(0);" tabindex="5" style="display:none;" onClick="fb_login();">Login with Facebook</a> */ ?>
	</div>
	<?php $this->endWidget(); ?>
	</fieldset>
	</div>
</div>