<script src="<?php echo Yii::app()->request->baseUrl; ?>/assets/js/script.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/assets/js/jquery.prettyPhoto.js"
        type="text/javascript"></script>
<link href="<?php echo Yii::app()->request->baseUrl; ?>/assets/css/prettyPhoto.css" rel="stylesheet" type="text/css"/>

<link href='<?php echo Yii::app()->request->baseUrl; ?>/assets/calender/fullcalendar/fullcalendar.css'
      rel='stylesheet'/>
<link href='<?php echo Yii::app()->request->baseUrl; ?>/assets/calender/fullcalendar/fullcalendar.print.css'
      rel='stylesheet' media='print'/>
<script src='<?php echo Yii::app()->request->baseUrl; ?>/assets/calender/fullcalendar/fullcalendar.min.js'></script>
<script src='<?php echo Yii::app()->request->baseUrl; ?>/assets/calender/fullcalendar/gcal.js'></script>
<script src='<?php echo Yii::app()->request->baseUrl; ?>/assets/calender/fullcalendar/moment.min.js'></script>
<script src='<?php echo Yii::app()->request->baseUrl; ?>/assets/js/dashboard_script.js'></script>
<script>

    <?php /*?>$(document).ready(function() {
        loadCal();
    });
    function loadCal() {

        var date = new Date();
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();

        $('#calendar').fullCalendar({
            defaultView: 'agendaDay',allDaySlot: false,
            eventSources: [
                // your event source
                {
                    url: '<?php echo Yii::app()->request->baseUrl; ?>/doctor/ajaxAppointmentToday',
                    type: 'POST',
                    data: {
                        hidden_date: $.trim($('#hidden_date').val())
                    },
                    error: function() {
                        alert('there was an error while fetching Appointments!');
                    }
                },
                // any other sources...
                <?php if(Yii::app()->session['google_account']){ ?>
                {
                    url: 'https://www.google.com/calendar/feeds/<?php echo Yii::app()->session['google_account']; ?>/public/basic',
                }
                <?php } ?>
            ],
            eventRender: function (event, element) {
                element.attr('href', 'javascript:void(0);');
                element.attr('onclick', 'openModal("' + event.title + '","' + event.description + '","' + event.url + '","' + event.start + '","' + event.end + '");');
            }
        });

    }
    <?php */?>
    function openModal(title, info, url, start, end) {
        if (start && start != 'null') {
            $("#startTime").html("Start: " + moment(start).format('MMMM Do YYYY, h:mm:ss a') + "<br />")
        } else {
            $("#startTime").html(""); //no start (huh?) clear out previous info.
        }
        if (end && end != 'null') {
            $("#endTime").html("End: " + moment(end).format('MMMM Do YYYY, h:mm:ss a') + "<br /><br />")
        } else {
            $("#endTime").html(""); //no end. clear out previous info.
        }
        if (info && info != 'null' && info != 'undefined') {
            $("#eventInfo").html(info);
            $("#eventInfo").show();
        } else {
            $("#eventInfo").hide(); //no end. clear out previous info.
        }
        if (url && url != 'null' && url != 'undefined') {
            $("#eventLink").attr('href', url);
            $("#eventLink").show();
        } else {
            $("#eventLink").hide(); //no end. clear out previous info.
        }
        $("#eventContent").dialog({ modal: true, title: title, width: 350 });
    }
</script>
<style>
        /*#calendar {
            width: 100%;
            margin: 0 auto;padding-top: 15px;
        }
        .fc-event-inner{
            cursor:pointer;
        }
        .ui-widget-overlay {
            background: 50% 50% #AAAAAA;
        }
        .fc-header{
            display: none;
        }*/
        /**** dashboard tab menu ****/
    div.tabBox h3 {
    }

    ul.tabs {
        /*border:1px solid #d8dce0;*/
        margin: 0;
        padding: 0;
        float: left;
        list-style: none;
        width: 99.9%;
    }

    ul.tabs li {
        float: left;
        margin: 0;
        padding: 0;
        height: 48px;
        line-height: 20px;
        margin-bottom: -1px;
        overflow: hidden;
        position: relative;
        width: 33.3%;
    }

    ul.tabs li a {
        background: url(../assets/images/tabmenu_bg.jpg) repeat-x;
        text-decoration: none;
        font-size: 16px;
        color: #6b6969;
        outline: none;
        border: 1px solid #d8dce0;
        padding-left: 18px;
        padding-right: 18px;
        line-height: 18px;
        padding-top: 12px;
        padding-bottom: 12px;
        display: block;
        outline: none;
        font-weight: bold;
    }

    ul.tabs li a:hover {
        background: url(../assets/images/tabmenu_bg.jpg) repeat-x;
    }

    ul.tabs li.active {
        font-size: 18px;
        font-weight: bold;
        color: #464646;
    }

    ul.tabs li.active a, ul.tabs li.active a:hover {
        background: #54cbc8;
        color: #fff;
    }

    div.tabContainer {
        background: #f9f9f9;
        border: 1px solid #e9e9e9;
        margin-top: 13px;
        overflow: hidden;
        clear: both;
        float: left;
        width: 99.9%;
    }

    .arrow-down {
        width: 0;
        height: 0;
        border-left: 10px solid transparent;
        border-right: 10px solid transparent;
        border-top: 10px solid #54cbc8;
        position: relative;
        /*top:42px;*/
        top: 43px;
        left: 14%;
    }

    .tabContainer table {
        width: 100%;
        border: 0px;
        padding: 0px;
        margin: 0px;
        border-collapse: collapse;
    }

    .tabContainer table td {
        border-right: 1px solid #e9e9e9;
        vertical-align: top;
    }

    .tabContainer table td table td.head {
        background: #f5f5f5;
        border-bottom: 1px solid #e9e9e9;
        padding-top: 5px;
        padding-bottom: 5px;
        color: #676767;
        font-weight: bold;
        font-size: 15px;
    }

    .tabContainer table td table td {
        color: #676767;
        font-weight: bold;
        font-size: 15px;
        text-transform: uppercase;
        border-right: 0px;
        padding-top: 10px;
    }

    .tabContainer table td div.booking {
        border-bottom: 1px solid #e9e9e9;
        padding: 15px;
        width: 98%;
        background: #fff;
        float: left;
    }

    .tabContainer table td div.leftbook {
        float: left;
        width: 90%;
    }

    .tabContainer h4 {
        color: #676767;
        font-weight: bold;
        font-size: 15px;
        display: block;
        width: 98%;
        padding: 8px;
        margin: 0px;
    }

    .tabContainer table td div.rightbook {
        float: right;
        width: 10%;
        padding-top: 25px;
    }

    .tabContainer table td div.rightbook a.tick {
        background: url(../assets/images/tick.png) no-repeat;
        display: block;
        padding-right: 17px;
        float: left;
        width: 19px;
        height: 19px;
    }

    .tabContainer table td div.rightbook a.tick:hover {
        background: url(../assets/images/tick_hover.png) no-repeat;
    }

    .tabContainer table td div.rightbook a.cross {
        background: url(../assets/images/cross.png) no-repeat;
        display: block;
        width: 19px;
        height: 19px;
        float: left;
    }

    .tabContainer table td div.rightbook a.cross:hover {
        background: url(../assets/images/cross_hover.png) no-repeat;
    }

    .tabContainer table td div.booking h4 {
        color: #676767;
        font-weight: bold;
        font-size: 15px;
        display: block;
        width: 100%;
        padding: 0px;
        margin: 0px;
    }

    .tabContainer table td div.booking p {
        color: #6b6a6a;
        font-size: 15px;
        font-weight: normal;
        display: block;
        width: 100%;
        line-height: 18px;
        padding-top: 5px;
        padding-bottom: 5px;
        margin: 0px;
    }

    .tabContainer table td div.booking a {
        color: #54cbc8;
        text-decoration: none;
        font-weight: bold;
    }
</style>

<?php
$this->breadcrumbs = array(
    'Dashboard',
);
?>
<div class="main">
<div id="breadcrumb" class="fk-lbreadbcrumb newvd">
    <!--<span><a href="">Home</a></span> >
    <span>Dashboard</span>-->
    <?php $this->widget('zii.widgets.CBreadcrumbs', array(
        'links' => $this->breadcrumbs,
    ));

    /*print_r(Yii::app()->request->);*/
    ?>
</div>
<div class="dashboard_mainarea">
<div class="leftmenu" id="left_id">
    <?php /*?>
       		 <h2>Doctor control panel</h2>
             <ul>
            	 <li class="active"><?php echo CHtml::link('Dashboard', $this->createAbsoluteUrl('index')); ?></li>
                 <li><?php echo CHtml::link('Appointments/Calender', Yii::app()->request->baseUrl.'/doctor/appointment/'.Yii::app()->session['logged_user_id']); ?></li>
                 <li><?php echo CHtml::link('Patients', $this->createAbsoluteUrl('doctor/patient/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li><?php echo CHtml::link('Task Manager', $this->createAbsoluteUrl('doctor/todolist/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li><?php echo CHtml::link('Special Offers', $this->createAbsoluteUrl('doctor/offers/'.$dataProvider->id)); ?></li>
                 <li><?php echo CHtml::link('Timeoff/Holidays Setup', $this->createAbsoluteUrl('doctor/timeoff/'.Yii::app()->session['logged_user_id'])); ?></li>
                 <li><?php echo CHtml::link('Reminders &amp; Alerts Setup', $this->createAbsoluteUrl('doctor/settingTab/'.Yii::app()->session['logged_user_id'])); ?></li> 
                 <li><?php echo CHtml::link('Schedule Setup', $this->createAbsoluteUrl('doctor/schedule/'.Yii::app()->session['logged_user_id'])); ?></li>              
                 <li><?php echo CHtml::link('Profile Setup', $this->createAbsoluteUrl('doctor/editProfile/'.$dataProvider->id)); ?></li>
                 <li><?php echo CHtml::link('Analytics', $this->createAbsoluteUrl('dashboard/default/index')); ?></li>
             </ul>
             */
    ?>
    <?php $this->renderPartial('//layouts/navigation'); ?>
</div>
<div class="rightarea_dashboard" id="right_id">
<div class="dashboard_content">
            <span class="profilepicx">
            	<?php

                if ($dataProvider->image) {
                    $filePath = 'assets/upload/doctor/' . $dataProvider->id . "/" . $dataProvider->image;
                    if (@file_get_contents($filePath, 0, NULL, 0, 1)) {
                        ?>
                        <a class="image-zoom"
                           href="<?php echo Yii::app()->request->baseUrl . '/assets/upload/doctor/' . $dataProvider->id . "/" . $dataProvider->image . '?myVar=text&iframe=true&width=65%&height=80%'; ?>"
                           rel="prettyPhoto[gallery]"
                           title="<?php echo $dataProvider->title; ?> <?php echo $dataProvider->first_name; ?> <?php echo $dataProvider->last_name; ?>">
                            <?php echo CHtml::image(Yii::app()->request->baseUrl . '/assets/upload/doctor/' . $dataProvider->id . "/" . $dataProvider->image, "image", array("width" => 135 /*,"height"=>111*/)); ?>
                        </a>
                    <?php }} else { ?>
                        <?php if ($dataProvider->gender == 'M') { ?>
                            <a href="javascript:void(0);"><?php echo CHtml::image(Yii::app()->request->baseUrl . '/assets/images/avatar.png', "image", array("width" => 135)); ?></a>
                        <?php } else { ?>
                            <a href="javascript:void(0);"><?php echo CHtml::image(Yii::app()->request->baseUrl . '/assets/images/avtar_female.png', "image", array("width" => 135)); ?></a>
                        <?php }
                } ?>
            </span>
            <span class="profile_brief">
            <!--<strong style="font-size:20px; font-weight:normal; color:#3d3d3d;">Welcome!!</strong>-->
            Welcome  <?php echo $dataProvider->title; ?> <?php echo $dataProvider->first_name; ?> <?php echo $dataProvider->last_name; ?>
                <font style="font-family: cursive,Courier,monospace;"><?php echo ($dataProvider->degree) ? ', ' . $dataProvider->degree : ''; ?></font>	 <br>
			  <p>
                  <span
                      style="font-size: 14px;"><?php echo isset($user_selected_speciality[0]) ? $user_speciality[$user_selected_speciality[0]] : ""; ?></span><br><br>
                  <?php //echo $dataProvider->addr1; ?>
                  <?php //echo ($dataProvider->addr1 && $dataProvider->zip)?'<br>':''; ?>
                  <?php //echo $dataProvider->zip; ?>
                  <?php
                  if ($default_data_address) {
                      $city = ($default_data_address->city) ? $default_data_address->city : '';
                      $state = ($default_data_address->state) ? ', ' . $default_data_address->state : '';
                      $zip = ($default_data_address->zip) ? ', ' . $default_data_address->zip : '';
                      echo $default_data_address->address . '<br>' . $city . $state . $zip;
                  }
                  ?>
              </p>
                <?php echo CHtml::link('Edit', $this->createAbsoluteUrl('doctor/editProfile/' . $dataProvider->id)); ?>
            </span>
    <span class="member_box">Member Since <b
            style="font-weight:bold;"><?php echo date('M, Y', strtotime($dataProvider->date_created)); ?></b></span>
</div>

<script>
    $(document).ready(function () {
        if (get_cookie('today_schedule') == 'yes') {
            $("ul.tabs li").removeClass("active");
            $(".tabContent").hide();
            $("ul.tabs li").eq(1).addClass("active").show();
            $(".tabContent").eq(1).show();
            $(".arrow-down").css('left', '50%');
            unset_cookie('today_schedule', '');
        }
    });
    function todaySchedule() {
        if ($('#appointment_req_change').val() == 1) {
            set_cookie('today_schedule', 'yes');
            location.reload();
        }
    }
    function set_cookie(name, value) {
        var today = new Date();
        today.setTime(today.getTime());
        var expires_date = new Date(today.getTime() + (1000 * 60 * 60 * 24 * 90));
        document.cookie = name + "=" + escape(value) + ";expires=" + expires_date.toGMTString();
    }
    function unset_cookie(name, value) {
        var today = new Date();
        today.setTime(today.getTime());
        var expires_date = new Date(today.getTime() + (-(1000 * 60 * 60 * 24 * 90)));
        document.cookie = name + "=" + escape(value) + ";expires=" + expires_date.toGMTString();
    }
    function get_cookie(name) {
        var a = null;
        if (document.cookie) {
            var b = document.cookie.split((escape(name) + '='));
            if (b.length >= 2) {
                var c = b[1].split(';');
                a = unescape(c[0]);
            }
        }
        return a;
    }
</script>

<div class="dashboard_content1">
    <div class="tabBox">
        <ul class="tabs">
            <li><a href="#tab1">Appointment Requests <font style="float:right; font-size:30px;"
                                                           id="appoint_request_cnt">
                        &nbsp;<?php //echo $dashboardAppointmentCnt; ?></font></a></li>
            <li><a href="#tab2" onclick="todaySchedule();">Today's Schedule</a></li>
            <?php /*
				  <li><a href="#tab3">Patient messages<font style="float:right; font-size:30px;"><?php if(!empty($doctor_review)) echo count($doctor_review); ?></font></a></li>
				*/
            ?>
        </ul>
        <div class="arrow-down"></div>

        <div class="tabContainer">
            <?php if (!empty($dashboardAppointment) && $dashboardAppointmentCnt > 0) { ?>
                <div id="tab1" class="tabContent">
                    <?php foreach ($dashboardAppointment as $dashboardAppointment_key => $dashboardAppointment_val) { ?>
                        <?php if (!empty($user_app_request[$dashboardAppointment_key])) { ?>
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <?php if ($dashboardAppointment_val[0]['confirm'] != 1 && $dashboardAppointment_val[0]['confirm'] != 4 && $dashboardAppointment_val[0]['confirm'] != 2) { ?>
                                        <td width="10%">
                                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td class="head"
                                                        align="center"><?php echo date('D', strtotime($dashboardAppointment_key)); ?></td>
                                                </tr>
                                                <tr>
                                                    <td align="center"><font
                                                            style="font-size:32px"><?php echo date('d', strtotime($dashboardAppointment_key)); ?></font>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center"><?php echo date('M', strtotime($dashboardAppointment_key)); ?></td>
                                                </tr>
                                            </table>
                                        </td>
                                    <?php } ?>
                                    <td width="85%">
                                        <?php foreach ($dashboardAppointment_val as $appointment_key => $appointment_val) { ?>
                                            <?php if ($appointment_val['confirm'] != 1 && $appointment_val['confirm'] != 4 && $appointment_val['confirm'] != 2 && $appointment_val['confirm'] != 3) { ?>
                                                <div class="booking" id="booking_<?php echo $appointment_val['id']; ?>">
                                                    <input type="hidden"
                                                           id="booking_id_<?php echo $appointment_val['id']; ?>"
                                                           value="<?php echo $appointment_val['id']; ?>"/>
                                                    <input type="hidden"
                                                           id="pres_app_title_id_<?php echo $appointment_val['id']; ?>"
                                                           value="<?php echo $appointment_val['patient_name']; ?> | <?php echo date('h:iA', strtotime($appointment_val['start'])); ?>-<?php echo date('h:iA', strtotime($appointment_val['end'])); ?> | <?php echo date('l, F dS', strtotime($appointment_val['start'])); ?>"/>

                                                    <div class="leftbook">
                                                        <h4><?php echo date('h:iA', strtotime($appointment_val['start'])); ?>
                                                            -<?php echo date('h:iA', strtotime($appointment_val['end'])); ?></h4>

                                                        <p>Duration <?php echo $appointment_val['book_duration']; ?>
                                                            minutes,
                                                            for <?php echo ($appointment_val['reason_for_visit_id']) ? $user_reason_for_visit[$appointment_val['reason_for_visit_id']] : ''; ?></p>

                                                        <p>
                                                            <?php
                                                            if ($appointment_val['confirm'] == 2) {
                                                                echo '<font color="#FF0000">Status : Cancellation</font> !';
                                                            }
                                                            if ($appointment_val['confirm'] == 3) {
                                                                echo '<font color="#FF0000">Status : complete</font> !';
                                                            }
                                                            ?>
                                                            Appointment Requested by <a
                                                                href="javascript:void(0);"><?php echo $appointment_val['patient_name']; ?></a>
                                                        </p>
                                                    </div>
                                                    <div class="rightbook">
                                                        <?php if ($appointment_val['confirm'] == 0) { ?>
                                                            <a class="tick" href="javascript:void(0);"
                                                               accesskey="<?php echo $appointment_val['id']; ?>"></a>
                                                        <?php } ?>
                                                        <?php if ($appointment_val['confirm'] != 2) { ?>
                                                            <a id="app_cancel_<?php echo $appointment_val['id']; ?>"
                                                               class="cross" href="javascript:void(0);"
                                                               accesskey="<?php echo $appointment_val['id']; ?>"></a>
                                                        <?php } ?>
                                                    </div>

                                                </div>
                                            <?php } ?>
                                        <?php } ?>
                                    </td>

                                </tr>

                            </table>
                        <?php } ?>
                    <?php } ?>
                </div>
            <?php } else { ?>
                <div id="tab1" class="tabContent"><h4>No Appointment Yet.</h4></div>
            <?php } ?>
            <?php if (isset($dashboardAppointment[date('Y-m-d')]) && !empty($dashboardAppointment[date('Y-m-d')])) { ?>
                <div id="tab2" class="tabContent">

                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="10%">
                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td class="head" align="center"><?php echo date('D'); ?></td>
                                    </tr>
                                    <tr>
                                        <td align="center"><font style="font-size:32px"><?php echo date('d'); ?></font>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center"><?php echo date('M'); ?></td>
                                    </tr>
                                </table>
                            </td>
                            <td width="85%">
                                <?php $can_cunt = 0; ?>
                                <?php foreach ($dashboardAppointment[date('Y-m-d')] as $dashboardAppointment_confirm_val) { ?>
                                    <?php if ($dashboardAppointment_confirm_val['confirm'] == 1) { ?>
                                        <div class="booking">
                                            <h4><?php echo date('h:iA', strtotime($dashboardAppointment_confirm_val['start'])); ?>
                                                -<?php echo date('h:iA', strtotime($dashboardAppointment_confirm_val['end'])); ?></h4>

                                            <p>
                                                Duration <?php echo isset($dashboardAppointment_confirm_val['book_duration']) ? $dashboardAppointment_confirm_val['book_duration'] : ''; ?>
                                                minutes, for
                                                <?php
                                                if (isset($dashboardAppointment_confirm_val['reason_for_visit_id']) && ($dashboardAppointment_confirm_val['reason_for_visit_id'] != 0)) {
                                                    echo $user_reason_for_visit[$dashboardAppointment_confirm_val['reason_for_visit_id']];
                                                }
                                                ?>.</p>

                                            <p>Requested by <a
                                                    href="javascript:void(0);"><?php echo $dashboardAppointment_confirm_val['patient_name']; ?></a>
                                            </p>
                                        </div>
                                        <?php $can_cunt++; ?>
                                    <?php } ?>
                                <?php } ?>
                                <?php if ($can_cunt == 0) { ?>
                                    <div class="booking"><h4>No appointment Yet.</h4></div>
                                <?php } ?>
                            </td>
                        </tr>
                    </table>

                </div>
            <?php } else { ?>
                <div id="tab2" class="tabContent"><h4>No Appointment Yet.</h4></div>
            <?php } ?>
            <?php if (!empty($doctor_review)) { ?>
                <div id="tab3" class="tabContent">
                    <?php foreach ($doctor_review as $doctor_review_key => $doctor_review_val) { ?>
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="10%">
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td class="head"
                                                align="center"><?php echo date('D', strtotime($doctor_review_val->date_created)); ?></td>
                                        </tr>
                                        <tr>
                                            <td align="center"><font
                                                    style="font-size:32px"><?php echo date('d', strtotime($doctor_review_val->date_created)); ?></font>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center"><?php echo date('M', strtotime($doctor_review_val->date_created)); ?></td>
                                        </tr>

                                    </table>
                                </td>
                                <td width="85%">
                                    <div class="booking">
                                        <h4><?php if (!empty($doctor_review_val->title)) {
                                                echo $doctor_review_val->title; ?>, <?php }
                                            echo date('h:i A', strtotime($doctor_review_val->date_created)); ?></h4>

                                        <p><?php echo $doctor_review_val->message; ?></p>

                                        <p>from <a
                                                href="javascript:void(0);"><?php echo $doctor_review_val->patient_details->user_first_name . ' ' . $doctor_review_val->patient_details->user_last_name; ?></a>
                                        </p>
                                    </div>

                                </td>
                            </tr>
                        </table>
                    <?php } ?>
                </div>
            <?php } else { ?>
                <div id="tab3" class="tabContent"><h4>No Messages Yet.</h4></div>
            <?php } ?>
        </div>

        <script type="text/javascript">
            $(".tabContent").hide();
            $("ul.tabs li:first").addClass("active").show();
            $(".tabContent:first").show();

            $("ul.tabs li").click(function () {
                var eq_no = $("ul.tabs li").index(this);
                $("ul.tabs li").removeClass("active");
                $(this).addClass("active");
                $(".tabContent").hide();
                var activeTab = $(this).find("a").attr("href");
                $(activeTab).fadeIn();
                if (eq_no == 0)    $(".arrow-down").css('left', '14%');
                if (eq_no == 1)    $(".arrow-down").css('left', '50%');
                if (eq_no == 2)    $(".arrow-down").css('left', '84%');
                return false;
            });
        </script>
    </div>

    <div id="eventContent" title="Event Details" style="display:none;">
        <span id="startTime"></span>
        <span id="endTime"></span>

        <div id="eventInfo"></div>
        <p><strong><a id="eventLink" href="javascript:void(0);" target="_blank">Read More</a></strong></p>
    </div>
</div>

<div class="dashboard_content1">
    <h2 class="dashboard_app">Todo List</h2>
    <?php /*?><span class="dashboard_app_span">
                	<span><img src="<?php echo Yii::app()->request->baseUrl; ?>/assets/images/s_incomplete.png" title="Active" alt=""/>Active</span>
                	<span><img src="<?php echo Yii::app()->request->baseUrl; ?>/assets/images/s_no_show_icon.png" title="Inactive" alt=""/>Inactive</span><?php */
    ?>
    </span>
    <div class="dashboardcont_leftbox2">
        <ul>
            <li class="heading">
                <span class="tsk">Task</span>
                <span class="tsk">Priority</span>
                <span class="add txt_align">Due Date</span>
                <span class="att txt_align">Status</span>
                <span class="att txt_align">Action</span>
            </li>
            <?php
            if ($todo_list):
                for ($i = 0; $i < count($todo_list); $i++) {
                    ?>
                    <li>
                        <span class="tsk"><?php echo $todo_list[$i]['name']; ?></span>
                        <span class="tsk"><?php echo $todo_list[$i]['priority']; ?></span>
                         <span class="add txt_align">
                         <?php echo date('F d, Y', strtotime($todo_list[$i]['deadline'])); ?>
                         </span>
                        <span class="att txt_align"><?php echo $todo_list[$i]['todolist_status']; ?></span>
                         <span class="att txt_align">
                         	<?php echo CHtml::link('<img src="' . Yii::app()->request->baseUrl . '/assets/images/edit_icon.png" alt=""/>', $this->createAbsoluteUrl('doctor/editTodolist/' . $todo_list[$i]['id']), array('title' => 'Edit')); ?>
                         </span>
                    </li>
                <?php
                } else:
                echo '<li style="text-align: center;">No Todo List Yet.</li>';
            endif;
            ?>
        </ul>
    </div>
</div>

<?php /*?><div class="dashboard_content1">
          		<h2 class="dashboard_app">Inbox</h2>
                <span class="dashboard_app_span">
                	<span><img src="<?php echo Yii::app()->request->baseUrl; ?>/assets/images/s_open_iocn.png" title="Read" alt=""/>Read</span>
                	<span><img src="<?php echo Yii::app()->request->baseUrl; ?>/assets/images/s_no_show_icon.png" title="Unread" alt=""/>Unread</span>
                </span>
                <div class="dashboardcont_leftbox2">
                	<ul>
                        <li class="heading">
                         <span class="add">From</span> 
                         <span class="add">Subject</span> 
                         <span class="add txt_align">Date/Time</span>
                         <span class="att txt_align">Action</span>
                        </li>
                        
                        <li>
                         <span class="add">Chris Martin</span> 
                         <span class="add">Tooth Problem</span> 
                         <span class="add txt_align">March 10<sup>th</sup>, 9.30 AM</span>
                         <span class="att txt_align">
                         	<a href="javascript:void(0);"><img src="<?php echo Yii::app()->request->baseUrl; ?>/assets/images/view_icon.png" alt=""/></a>
                         </span>
                        </li>
                        
                        <li>
                         <span class="add">Chris Martin</span> 
                         <span class="add">Tooth Problem</span> 
                         <span class="add txt_align">March 10<sup>th</sup>, 9.30 AM</span>
                         <span class="att txt_align">
                         	<a href="javascript:void(0);"><img src="<?php echo Yii::app()->request->baseUrl; ?>/assets/images/view_icon.png" alt=""/></a>
                         </span>
                        </li>
                        
                        <li>
                         <span class="add">Chris Martin</span> 
                         <span class="add">Tooth Problem</span> 
                         <span class="add txt_align">March 10<sup>th</sup>, 9.30 AM</span>
                         <span class="att txt_align">
                         	<a href="javascript:void(0);"><img src="<?php echo Yii::app()->request->baseUrl; ?>/assets/images/view_icon.png" alt=""/></a>
                         </span>
                        </li>	
                    </ul>
                    <div class="bold_font"><a href="#">Show more</a></div>
                </div>
          </div><?php */
?>

<?php /*?><div class="dashboard_content1">
          		<h2 class="dashboard_app">Todo List</h2>
                <span class="dashboard_app_span">
                	<span><img src="<?php echo Yii::app()->request->baseUrl; ?>/assets/images/s_complete_iocn.png" title="Complete" alt=""/>Complete</span>
                	<span><img src="<?php echo Yii::app()->request->baseUrl; ?>/assets/images/s_incomplete.png" title="Incomplete" alt=""/>Incomplete</span>
                </span>
                <div class="dashboardcont_leftbox2">
                	<ul>
                        <li class="heading">
                         <span class="add">Title</span> 
                         <span class="add">Priority</span> 
                         <span class="add txt_align">Deadline</span>
                         <span class="att txt_align">Status</span>
                        </li>
                        
                        <li>
                         <span class="add">Call Robin</span> 
                         <span class="add">1</span> 
                         <span class="add txt_align">March 10<sup>th</sup>, 9.30 AM</span>
                         <span class="att txt_align">
                         	<div align="center"><img src="<?php echo Yii::app()->request->baseUrl; ?>/assets/images/s_incomplete.png" title="Confirmed" alt=""/></div>
                         </span>
                        </li>
                        
                         <li>
                         <span class="add">Call John</span> 
                         <span class="add">2</span> 
                         <span class="add txt_align">March 10<sup>th</sup>, 9.30 AM</span>
                         <span class="att txt_align">
                         	<div align="center"><img src="<?php echo Yii::app()->request->baseUrl; ?>/assets/images/s_incomplete.png" title="Confirmed" alt=""/></div>
                         </span>
                        </li>
                        
                         <li>
                         <span class="add">Call Mark</span> 
                         <span class="add">3</span> 
                         <span class="add txt_align">March 10<sup>th</sup>, 9.30 AM</span>
                         <span class="att txt_align">
                         	<div align="center"><img src="<?php echo Yii::app()->request->baseUrl; ?>/assets/images/s_incomplete.png" title="Confirmed" alt=""/></div>
                         </span>
                        </li>	
                    </ul>
                    <div class="bold_font"><a href="#">Show more</a></div>
                </div>
          </div><?php */
?>

</div>
</div>
</div>


<div id="toPopup">
    <div class="close"></div>
    <div id="popup_content"> <!--your content start-->
        <input type="hidden" id="hidden_app_id" value=""/>
        <h4>Accept Appointment</h4>

        <p id="app_title_id">BookFresh Support | 9:00AM-9:30AM | Thursday , April 1st </p>

        <form name="" method="post">
            <textarea name="" placeholder="Send an optional note to your client..." id="comments_text"></textarea>
            <input type="button" name="cancel" value="Cancel" class="cancelbt"/>
            <input type="button" name="accept" value="Send Message & Accept" class="acceptbt"/>
        </form>
    </div>
</div>

<div id="toPopup2">
    <div class="close"></div>
    <div id="popup_content"> <!--your content start-->
        <input type="hidden" id="cancel_hidden_app_id" value=""/>
        <h4>Cancel Appointment</h4>

        <p id="cancel_app_title_id">BookFresh Support | 9:00AM-9:30AM | Thursday , April 1st </p>

        <form name="" method="post">
            <textarea name="" placeholder="Send an optional note to your client..." id="mandatory_text"></textarea>
            <input type="button" name="cancel" value="Cancel" class="cancelbt"/>
            <input type="button" name="accept" value="Send Message & Cancel" class="acceptbtcancel"/>
        </form>
    </div>
</div>
<input type="hidden" name="appointment_req_change" id="appointment_req_change" value="0"/>
<div id="backgroundPopup"></div>
<script>
    function appointmentConfirm() {
        var app_id = $('#hidden_app_id').val();
        $.post("<?php echo Yii::app()->request->baseUrl; ?>/doctor/appointmentConfirm", {app_id: app_id, comments_text: $('#comments_text').val()}, function (response) {
            /*$('#booking_'+app_id).remove();
             var app_cnt = $.trim($('#appoint_request_cnt').html())-1;
             $('#appoint_request_cnt').html(app_cnt);
             if(app_cnt == 0)	$('#tab1').html('<h4>No Appointment Yet.</h4>');
             $('#appointment_req_change').val(1);*/
            location.reload();
        });
        return false;
    }
    function appointmentCancel(app_id) {
        var app_id = $('#cancel_hidden_app_id').val();
        //if(confirm('are you sure?')){
        if ($('#mandatory_text').val() != '') {
            $.post("<?php echo Yii::app()->request->baseUrl; ?>/doctor/appointmentCancel", {app_id: app_id, mandatory_text: $('#mandatory_text').val()}, function (response) {
                $('#app_cancel_' + app_id).remove();
                //alert($('#booking_'+app_id).closest().html());
                location.reload();
            });
        } else {
            $('#mandatory_text').css('border', '1px solid red');
            $('#mandatory_text').keyup(function () {
                $(this).css('border', 'none');
            });
            return false;
        }
        //}
    }
</script>