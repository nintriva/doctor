<link href='<?php echo Yii::app()->getBaseUrl(true); ?>/assets/css/modal_popup.css' rel='stylesheet'/>
<link href='<?php echo Yii::app()->getBaseUrl(true); ?>/assets/css/style.css' rel='stylesheet'/>
<?php


if (isset(Yii::app()->session['logged_user_type']) && (Yii::app()->session['logged_user_type'] == "doctor")|| (Yii::app()->session['logged_user_type']=="parcticeAdmin")) {
    $user_type = 1;
    $owner_type = "d";
} elseif ((Yii::app()->session['logged_user_type'] == "patient")) {
    $user_type = 0;
    $owner_type = "p";
}

$this->breadcrumbs = array(
    'Messaging Center' => array('compose'),
    'Compose Message',
);
?>
<div class="main">
    <div id="breadcrumb" class="fk-lbreadbcrumb newvd">
        <!--<span><a href="">Home</a></span> >
        <span>Dashboard</span>-->
        <?php $this->widget('zii.widgets.CBreadcrumbs', array(
            'links' => $this->breadcrumbs,
        ));
        ?>
    </div>
    <div class="dashboard_mainarea">
        <div class="leftmenu">
            <?php $this->renderPartial('//layouts/navigation'); ?>
        </div>
        <div class="rightarea_dashboard">
            <?php if (Yii::app()->user->hasFlash('sentNewMail')) { ?>
                <span
                    style="display:block; height:25px;text-align: center;font-weight: bold;"><?php echo Yii::app()->user->getFlash('sentNewMail'); ?></span>
            <?php } ?>
            <div class="dashboardcont_leftbox">
                <form id="compose_mail" name="compose_mail" method="post"
                      action="<?php echo Yii::app()->request->baseUrl . '/message/Compose'; ?>"
                      onsubmit="javascript: return validate_compose_mail();" autocomplete="off">
                    <h1 style="margin-bottom: 20px;">Compose Message</h1>
                    <?php if ($owner_type == "p") {
                        ?>
                        <div style="margin-left: 14px !important;word-wrap: break-word ;font-weight: bold;"><span style="color:#0096CE;">Disclaimer : </span><span>If you think you or your family member has a medical or psychiatric emergency
                    ,call 911 or go to the nearest hospital.Do not attempt to access emergency care through this website.
                    All Messages and attachments you send and receive through this website will become part of your medical records on eDoctorBook </span></div>

                    <?php
                    } ?>

                    <div class="box_content">
                        <div class="select_mail fld_area">

                            <label class="fld_name required">Message for</label>
                            <?php

                            $userType = Yii::app()->session['logged_user_type'];

                            if ($userType == "doctor"||$userType == 'parcticeAdmin') {
                                ?>
                                <div class="name_fld">
                                    <select id="sent_to_persion"
                                            class="select_fld_class" style="cursor:pointer;">

                                        <option value="patient">Patient</option>
                                        <option value="doctor">Doctor</option>
                                    </select>
                                </div>
                            <?php
                            } else if ($userType == "patient") {
                                ?>
                                <select id="sent_to_persion">
                                    <option value="doctor">Doctor</option>
                                </select>
                            <?php
                            } else {
                                ?>
                                <select id="sent_to_persion">
                                    <option value="0">--- Select One ---</option>
                                </select>
                            <?php } ?>
                            <input type="hidden" id="login_type" name="login_type" value="<?php echo $userType; ?>">
                            <div class="clear"></div>
                        </div>
                        <div class="fld_area">
                            <label class="fld_name required">To</label>

                            <div class="newtokeninputfield">
                                <div class="name_fld" style="margin-bottom: 50px;">
                                    <input type="text" class="fld_class" id="demo-input-facebook-theme"
                                           name="contact_id"/>
                                    <input type="hidden" id="contact_r_id" name="contact_r_id[]" value="0">
                                    <input type="hidden" id="contact_r_email" name="contact_r_email[]" value="">
                                    <input type="hidden" id="contact_r_type" name="contact_r_type" value="">
                                </div>
                            </div>


                            <i type="button" style="    background-color: #54cbc8;
    border-radius: 5px;
    color: #ffffff !important;
    border: 0px;
    height: 20px;
    margin-left: 10px;
    margin-top: 4px;
    position: absolute;
    font-size: 14px;
    padding: 10px !important;
    width: 13px !important;
    padding-bottom: 2px !important;" title="Favourites"
                                   class="fav_rply_btn registbt fa fa-heart" id="favourite_but"></i>
                            <!--  <a onclick="javascript: userDetails();" style="cursor:pointer;">click For Details</a>-->

                            <div class="clear"></div>
                        </div>
                        <div class="fld_area">
                            <label class="fld_name required">Subject</label>

                            <div class="name_fld">
                                <input type="text" class="fld_class" maxlength="100" size="32" id="subject"
                                       name="subject">
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="fld_area">
                            <label class="fld_name required">Department/Category</label>

                            <div class="name_fld">
                                <?php
                                $department = Department::model()->findAll();
                                ?>
                                <select id="department" name="department" class="select_fld_class" style="cursor:pointer;" ">
                                    <?php foreach ($department as $d){ ?>
                                    <option value="<?php echo $d->id ?>"><?php echo $d->name ?></option>
                                    <?php } ?>

                                </select>
                            </div>


                            <div class="clear"></div>
                        </div>
                        <div class="fld_area">
                            <div class="fld_name fld_name_hight">Message</div>
                            <div class="name_fld">
                                <i type="button"
                                   style="background-color: #54cbc8;border-radius: 5px;color: #ffffff !important;border: 0px;   height: 21px;
    margin-left: 420px;
    margin-top: 0px;
    position: absolute;
     font-size: 14px;
    padding-top: 10px !important;
    width: 14px !important;"
                                   class="registbt mesg_rply_btn fa fa-envelope" title="Quick Message" id="quick_reply_but" ></i>
                                <textarea class="txtarea_class" maxlength="1000" size="32" id="msg_body"
                                          name="msg_body" style="resize:both"></textarea>
                            </div>
                            <div name="search_document" id="search_document" title="Attachments" class="btn mapbook fa fa-paperclip"style="            background-color: #54cbc8;
    border-radius: 5px;
    color: #ffffff !important;
    border: 0px;
    height: 21px;
    margin-left: 13px;
    margin-top: 45px;
    position: absolute;
     font-size: 14px;
    padding: 7px 7px 2px 10px !important;
    width: 14px !important;"onclick="documentSearch();"></div>
                            <!--<a id="attachment" style="cursor:pointer;">click For Attachment</a>-->
                            <div class="clear"></div>
                        </div>


                        <div class="fld attachment">

                            <label class="fld_name required">&nbsp;</label>
                        <span>
                            <!--<label href="#" class='js-open-modal' style="width: 150px;
    background-color: #eaeaea;
    float: left;margin-bottom: 20px;border-radius: 5px;">Attach Existing Documents</label>-->


                            <a style=" width:390px;display: none; margin-left: 220px;margin-top: -8px;word-break: break-word; background-color: #EAEAEA;
    margin-bottom: 5px;
    border-bottom: 2px solid white;"
                               href="#" id="docs_search" name="docs_search"></a>
                            <input type="hidden" id="docs_search_id" name="docs_search_id[]" value=""/>

                            <!--new document attach-->
	              	 <!--<button  class='js-open-modal btn mapbook' data-modal-id="popup" style="width:173px;margin-left: 225px;font-size: 12px">Attach New Document</button>
                            <input type="hidden" id="doc_id" name="doc_id" value=""/>
                            <a  style="    float: right;
    margin-top: 14px;
    margin-right: 208px;
    background-color: white;" href="#" type="hidden" id="doc_name"></a>-->
	              </span>
                        </div>
                        <div style="clear: both"></div>

                            <label class="fld_name required">&nbsp;</label>

	              <span>
                      <?php if ($owner_type == "p") {
                          ?>
<div style="margin-left: 2px !important;word-wrap: break-word;margin-top: 16px !important;font-weight: bold;padding-left: 220px;"><span style="color: #0096CE;text-align: center !important;">Disclaimer : </span><span >By clicking the send button,you are providing your acknowledgement and agreement to eDoctorBook Terms and Conditions and Privacy and Policies </span></div>   <?php } ?>
                      <div style="clear: both"></div>
	              	<input
                        style="margin-top: 25px;border: none;border-radius: 5px;font-size: 13px;font-weight: bold;height: auto;padding: 6px;width: auto;background-color: #54cbc8;margin-left: 350px;"
                        type="submit" value="Send" class="registbt" name="submit">
	              	<input
                        style="margin-top: 25px;border: none;border-radius: 5px;font-size: 13px;font-weight: bold;height: auto;padding: 6px;width: auto;background-color: #54cbc8; "
                        type="reset" value="Cancel" class="registbt" id="cancel" >
	              </span>


                    </div>
                </form>

            </div>
        </div>
    </div>
</div>
<div id="patient_detail" style="display:none;">
    <div class="box_content_new">
        <div class="fld_area_new1">
            <div class="fld_name">First Name:</div>
            <div class="name_fld">
                <div id="popup_user_first_name_html" class="popup_non_edit"></div>
            </div>
            <div class="clear"></div>
        </div>
        <div class="fld_area_new1">
            <div class="fld_name">Last Name:</div>
            <div class="name_fld">
                <div id="popup_user_last_name_html" class="popup_non_edit"></div>
            </div>
            <div class="clear"></div>
        </div>
        <div class="fld_area_new1">
            <div class="fld_name">Email:</div>
            <div class="name_fld">
                <div id="popup_user_email_html" class="popup_non_edit"></div>
            </div>
            <div class="clear"></div>
        </div>
        <div class="fld_area_new1">
            <div class="fld_name">Phone:</div>
            <div class="name_fld">
                <div id="popup_user_phone_html" class="popup_non_edit"></div>
            </div>
            <div class="clear"></div>
        </div>
        <div class="fld_area_new1">
            <div class="fld_name">Address:</div>
            <div class="name_fld">
                <div id="popup_address_html" class="popup_non_edit"></div>
            </div>
            <div class="clear"></div>
        </div>
        <div class="fld_area_new1">
            <div class="fld_name">City:</div>
            <div class="name_fld">
                <div id="popup_user_city_html" class="popup_non_edit"></div>
            </div>
            <div class="clear"></div>
        </div>
        <div class="fld_area_new1">
            <div class="fld_name">State:</div>
            <div class="name_fld">
                <div id="popup_user_state_html" class="popup_non_edit"></div>
            </div>
            <div class="clear"></div>
        </div>
        <div class="fld_area_new1">
            <div class="fld_name">Zip:</div>
            <div class="name_fld">
                <div id="popup_user_zip_html" class="popup_non_edit"></div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>


<div id="patient_search_popup" style="display:none;z-index: 999 !important;">
    <div class="dashboard_content1">
        <div class="dashboardcont_leftbox">
            <div class="add_area">
                <span class="add_calender">
                    <div class="filter_search_area_calender">


                        <div class="filter_apdate_calender_new1">
                            <div style="width: 134%; ">

                                <?php if (isset(Yii::app()->session['logged_user_type']) && (Yii::app()->session['logged_user_type'] == "doctor")) { ?>
                                    <input type="text" style="width: 50%" name="document_name" id="mail_doctor_search"
                                           class="filter_txtfld_calender fld_class"
                                           placeholder="Type your patient name.." onkeyup="patientList();" value=""/>
                                <?php } ?>

                                <?php if (isset(Yii::app()->session['logged_user_type']) && (Yii::app()->session['logged_user_type'] == "patient")) { ?>
                                    <input type="text" style="width: 50%" name="document_name" id="mail_doc_search"
                                           class="filter_txtfld_calender fld_class"
                                           placeholder="Type your Document name.." onkeyup="documentList();" value=""/>
                                <?php } ?>

                                <ul id="mail_patient_id" style="display:none;width: 42%"></ul>

                            </div>

                        </div>
                        <!--<div class="filter_apbtn_new"><input type="button" class="search_btn" name="" value="Search" onclick="patientResultSearch();"/></div>-->
                        <div class="clear"></div>

                    </div>
                </span>

                <div class="clear"></div>
            </div>
        </div>
        <div class="dashboardcont_leftbox2">
            <ul>
                <li class="heading">
                    <span class="active" style="width:25%">Document Name</span>
                    <span class="active" style="width:20%"> Label</span>
                    <span class="active" style="width:20%"> Date</span>

                    <span class="att txt_align">Action</span>
                </li>
                <li style="overflow: auto; max-height: 250px; height: 100%;">
                    <ul id="patient_result_search">

                        <?php
                        $newcriteria = new CDbCriteria(array('order'=>'id DESC'));
						$doc = Documents::model()->findAllByAttributes(array('owner' => Yii::app()->session['logged_user_id'], 'owner_type' => $owner_type),$newcriteria);
                        $criteria1 = new CDbCriteria();
                        $criteria1->addCondition('status="shared"');
                        $criteria1->addCondition('db_shared_document.shared_type=' . "'$owner_type'");
                        $criteria1->addCondition('db_shared_document.shared_id=' . Yii::app()->session['logged_user_id']);
                        $criteria1->join = 'LEFT JOIN db_shared_document ON t.id = db_shared_document.document_id';
                        $shared = Documents::model()->findAll($criteria1);
                        $document = array_merge($doc, $shared);
                        $document = array_unique($document, SORT_REGULAR);
				
                        foreach ($document as $docu) {
                            ?>
                            <li>
                                <span class="active" style="width:25%"><?php echo $docu->document_name; ?></span>
                                <span class="active" style="width:20%"><?php echo $docu->label; ?></span>
                                <span class="active" style="width:20%"><?php echo $docu->date; ?></span>

                         <span class="att txt_align">
                            <?php echo CHtml::CheckBox("idList[]", false, array('id' => 'idList', 'value' => $docu->id . '_' . $docu->document_name)); ?>
                         </span>
                            </li>
                        <?php } ?>
                    </ul>
                </li>
            </ul>
        </div>

        <input style="float: right" onclick="selectdocument();" class="registbt" data-dismiss="modal"
               id="select_checkbox" type="button" value="Select">
    </div>
</div>

<div id="quickmessage_popup" style="display:none;">
    <div class="dashboard_content1">
        <div class="dashboardcont_leftbox">
            <div class="add_area">
                <span class="add_calender">
                    <div class="filter_search_area_calender">

                        <div id="success_message_add" style="    color: #717171;font-weight: bold;"></div>
                        <div class="filter_apdate_calender_new1">

                            <div style="width: 134%; ">

                                <input type="text" style="width: 50%" name="message_add" id="message_add"
                                       class="filter_txtfld_calender fld_class" placeholder="Add New Message.."
                                       value=""/>
                                <input type="button" style="cursor:pointer;" id="message_add_button"
                                       class="message_add_button" value="Add">


                            </div>

                        </div>
                        <!--<div class="filter_apbtn_new"><input type="button" class="search_btn" name="" value="Search" onclick="patientResultSearch();"/></div>-->
                        <div class="clear"></div>

                    </div>
                </span>

                <div class="clear"></div>
            </div>
        </div>
        <div class="dashboardcont_leftbox2">
            <ul>
                <li class="heading">
                    <span class="active" style="width:25%">messages</span>

                    <span class="att txt_align">Action</span>
                </li>
                <li style="overflow: auto; max-height: 250px; height: 100%;">
                    <ul id="quickmessage_result">


                        <?php
                        $criteria = new CDbCriteria();
                        $criteria->order = 'id DESC';
                        $Quickmessages = Quickmessages::model()->findAllByAttributes(array('user_id' => Yii::app()->session['logged_user_id'], 'user_type' => $user_type), $criteria);

                        foreach ($Quickmessages as $quick_mes) {

                            ?>
                            <li>
                                <span class="active" style="width:25%"><?php echo $quick_mes->messages; ?></span>


                         <span class="att txt_align">
                            <?php echo CHtml::CheckBox("messagelist[]", false, array('value' => $quick_mes->id . '_' . $quick_mes->messages)); ?>
                         </span>
                            </li>
                        <?php } ?>


                    </ul>
                </li>
            </ul>
        </div>

        <input style="float: right;border-radius:7px;height:35px;" class="registbt" data-dismiss="modal"
               id="select_quickmessage_checkbox"
               type="button" value="Select">
    </div>
</div>

<div id="favourite_popup" style="display:none;">
    <div class="dashboard_content">
        <div class="dashboardcont_leftbox">
            <div class="add_area">
                <span class="add_calender">
                    <div class="filter_search_area_calender">

                        <div id="success_message_favourite"
                             style=" position: absolute;margin-left: 120px;color: #717171;font-weight: bold;"></div>
                        <div style="clear: both !important;"></div>
                        <div class="filter_apdate_calender_new1">

                            <div class="newtokeninputfield popuptockenmail">
                                <div>

                                    <input type="text" class="filter_txtfld_calender fld_class" id="favourite_add"
                                           name="favourite_add" value=""/>
                                    <input type="hidden" id="fav_r_id" name="fav_r_id[]" value="0">
                                    <input type="hidden" id="fav_r_email" name="fav_r_email[]" value="">
                                    <input type="hidden" id="fav_name" name="fav_name[]" value="">
                                    <input type="hidden" id="fav_r_type" name="fav" value="">

                                    <!--<input type="text" style="width: 50%" name="message_add" id="favourite_add"
                                           class="filter_txtfld_calender fld_class" placeholder="Add Favourite.."
                                           value=""/>-->
                                    <input type="button"
                                           style="cursor:pointer; float: right;margin-left: 359px;    margin-top: 12px; "
                                           id="favourite_add_button" class="message_add_button" value="Add">


                                </div>
                            </div>

                        </div>
                        <!--<div class="filter_apbtn_new"><input type="button" class="search_btn" name="" value="Search" onclick="patientResultSearch();"/></div>-->
                        <div class="clear"></div>

                    </div>
                </span>

                <div class="clear"></div>
            </div>
        </div>
        <div class="dashboardcont_leftbox2" style="margin-top: 50px !important;">
            <ul>
                <li class="heading">
                        <span class="active" style="width:25%">Name</span>
						
                        <span class="att txt_align">Action</span>
                </li>
                <li style="overflow: auto; max-height: 250px; height: 100%;">
                    <ul id="favourite_result">
                    </ul>
                </li>
            </ul>
        </div>

        <input style="float: right;border-radius:7px;height:35px;" class="registbt" data-dismiss="modal"
               id="select_favourite_checkbox"
               type="button" value="Select">
    </div>
</div>


<script>

    function selectdepartment(){

    }

    function selectdocument() {
        $("#patient_search_popup").dialog('close');
        var idList = $("input[type=checkbox]:checked").serialize();

        var itm = idList.split("&");
        var array = [];
        $.each(itm, function (key, value) {
            var itm = value.split("_");

            itm[1] = itm[1].replace(/\+/g, ' ');
            array.push(itm[1]);
        });

        $('#docs_search').html("Attached Files: " + array);
        $('#docs_search').show();
        $('#docs_search_id').val(idList);
        $('input[type=checkbox]:checked').removeAttr('checked');
    }
</script>
<script type="text/javascript">
$('#attachment').click(function () {
    $('.attachment').show();
});
function documentSearch() {
    // $("#patient_result_search").html('No result found.');
    $("#patient_search_popup").dialog({ modal: true, title: 'Search Options', width: 600, position: ['top', 20]  });
}

function patientList() {
    var keyword = $('#mail_doctor_search').val();

    if (keyword != "" || keyword != null) {
        if (keyword.length > 2) {
            $.ajax({
                url: '<?php echo Yii::app()->request->baseUrl; ?>/Patient/getLists',
                type: 'POST',
                data: {keyword: keyword},
                success: function (data) {
                    $('#mail_patient_id').show();
                    $('#mail_patient_id').html(data);
                    if (keyword.length < 1) {
                        $('#mail_patient_id').hide();
                    }
                }
            });
        }
        if (keyword.length < 1) {
            $('#mail_patient_id').hide();
        }
    }
}

function mail_patient(item) {

    var si;
    if(typeof (item)=='string'){
        var itm = item.split("|");
        var date = new Date(itm[1]);
        var date = (date.getMonth() + 1) + '-' + date.getDate() + '-' + date.getFullYear();
        var itmData = itm[0] + " " + date;
        $('#mail_doctor_search').val(itmData);
        $('#mail_patient_id').hide();

         si = itm[2];
    }else if(typeof (item)=='number' && item!='' ){
        si=item;
        $('#mail_doctor_search').hide();
    }else if(item==''){
        si=item;

    }

    $.ajax({
        method: 'POST',
        url: '<?php echo Yii::app()->createUrl('documents/search'); ?>',
        data: { kword: si},
        success: function (data) {
            $("#patient_result_search").html(data);
            $('#mail_doctor_search').val('');

        }


    });

    //alert(itm[0]+"--"+itm[1]+"--"+itm[2]);
}

function documentList() {
    var keyword = $('#mail_doc_search').val();

    if (keyword != "" || keyword != null) {
        if (keyword.length > 2) {
            $.ajax({
                url: '<?php echo Yii::app()->request->baseUrl; ?>/Documents/getLists',
                type: 'POST',
                data: {keyword: keyword},
                success: function (data) {
                    $('#mail_patient_id').show();
                    $('#mail_patient_id').html(data);
                    if (keyword.length < 1) {
                        $('#mail_patient_id').hide();
                    }
                }
            });
        }
        if (keyword.length < 1) {
            $('#mail_patient_id').hide();
        }
    }
}

function docs(item) {

    var itm = item.split("|");
    var date = new Date(itm[1]);
    var date = (date.getMonth() + 1) + '-' + date.getDate() + '-' + date.getFullYear();
    var itmData = itm[0] + " " + date;
    $('#mail_doc_search').val(itmData);
    $('#mail_patient_id').hide();

    var si = itm[2];
    $.ajax({
        method: 'POST',
        url: '<?php echo Yii::app()->createUrl('documents/list'); ?>',
        data: { kword: si},
        success: function (data) {
            $("#patient_result_search").html(data);

        }


    });

}

/*  document.getElementById('patient_search').onblur = function() {
 alert();
 $('#patients_list_id').hide();
 }*/

function patientSelect(value, id) {
    $("#patient_search_popup").dialog('close');
    $('#docs_search').html("Attached File: " + value);
    $('#docs_search_id').val(id);
    $('#docs_search').show();
}

$(function () {

    var appendthis = ("<div class='modal-overlay js-modal-close'></div>");

    $('button[data-modal-id]').click(function (e) {
        e.preventDefault();
        $("body").append(appendthis);
        $(".modal-overlay").fadeTo(500, 0.7);
        //$(".js-modalbox").fadeIn(500);
        var modalBox = $(this).attr('data-modal-id');
        $('#' + modalBox).fadeIn($(this).data());
    });


    $(".js-modal-close, .modal-overlay,#close_popup").click(function () {
        $(".modal-box, .modal-overlay").fadeOut(500, function () {
            $(".modal-overlay").remove();
        });
    });

    $(window).resize(function () {
        $(".modal-box").css({
            top: ($(window).height() - $(".modal-box").outerHeight()) / 2,
            left: ($(window).width() - $(".modal-box").outerWidth()) / 2
        });
    });

    $(window).resize();

});
//
$("#cancel").click(function(){
    $('#compose_mail')[0].reset();
    $('#compose_mail').trigger("reset");
    $("#demo-input-facebook-theme").tokenInput("clear");

});

$("#sub").click(function () {

    var obj = $('#compose_document')[0];
    var formData = new FormData(obj);

    $.ajax({
        url: '<?php echo Yii::app()->createUrl('documents/document');?>',
        type: 'POST',
        data: formData,
        async: false,
        success: function (data) {
            $(".modal-box, .modal-overlay").hide();
            var dat = JSON.parse(data);
            $('#doc_id').val(parseInt(dat.id));
            $('#doc_name').html("Attached file: " + dat.document_name);
            $('#doc_name').show();

        },
        cache: false,
        contentType: false,
        processData: false
    });

    return false;
});

function set_item(item) {
    var itm = item.split("|");

    var itmData = "";
    if (itm[2] != "") {
        itmData = " < " + itm[2] + " > ";
    }
    itmData = itmData + itm[1];
    $('#contact_r_id').val(itm[0]);
    $('#contact_r_email').val(itm[2]);
    $('#contact_r_type').val(itm[3]);
    $('#contact_id').val(itmData);
    $('#contact_list_id').hide();

    //alert(itm[0]+"--"+itm[1]+"--"+itm[2]);
}

function validate_compose_mail() {
    var contact_id = $("#contact_id").val();
    var contact_r_id = $("#contact_r_id").val();
    var subject = $("#subject").val();
    var msg_body = $("#msg_body").val();

    /*  if (contact_id == "" || contact_id == null) {
     alert();
     $('#contact_id').val("");
     $('#contact_id').effect("highlight", {}, 5000);
     return false;
     }*/
    if (contact_r_id == "" || contact_r_id == null) {
        $('#contact_r_id').val("");
        $('#contact_r_id').effect("highlight", {}, 5000);
        return false;
    }
    if (subject == "" || subject == null) {
        $('#subject').val("");
        $('#subject').effect("highlight", {}, 5000);
        return false;
    }
    if (msg_body == "" || msg_body == null) {
        $('#msg_body').val("");
        $('#msg_body').effect("highlight", {}, 5000);
        return false;
    }
}

function reset_fileds() {


}

</script>
<style>
    .ui-widget-header {
        background-color: #54cbc8;
        background-image: none;
    }
</style>
<script>
    var name = 0;
    $("#upload1").change(function () {
        var inp = document.getElementById('upload1');

        for (var i = 0; i < inp.files.length; ++i) {
            //name+= ','+ inp.files.item(i).name;
            //  name ++;*/
            if (/.*\.(png)|(jpeg)|(jpg)|(doc)|(docx)|(pdf)$/.test(inp.files.item(i).name.toLowerCase())) {
                return true;
            }
            else {
                alert('file type not allowed');
                //  inp.files.item(i).name.focus();
                $("#upload1").val('');
                return false;
            }
            // alert("here is a file name: " + name;
        }

    });

    $('#select_quickmessage_checkbox').click(function () {
        var messagelist = $("input[type=checkbox]:checked").serialize();
        if (messagelist != '') {
            var itm = messagelist.split("&");
            var array = [];
            $.each(itm, function (key, value) {
                var itm = value.split("_");
                itm[1] = itm[1].replace(/\+/g, ' ');
                console.log(itm[1]);

                var dec = unescape(itm[1]);
                array.push(dec);
            });

        }
        $("#msg_body").val(array);
        $("#msg_body").show();
        $("#quickmessage_popup").dialog('close');
        $('input[type=checkbox]:checked').removeAttr('checked');
    });

    $('#select_favourite_checkbox').click(function () {

        var favouritelist = $("input[type=checkbox]:checked").serialize();
        if (favouritelist != '') {
            var itm = favouritelist.split("&");
            var array = [];
            $.each(itm, function (key, value) {
                var itm = value.split("--");
                var item1 = itm[0].split("=");
                itm[1] = itm[1].replace(/\+/g, ' ');
                itm[2] = itm[2].replace(/\%40/g, '@');

                $("#demo-input-facebook-theme").tokenInput("add", {id: parseInt(item1[1]), name: itm[1],vic_id:parseInt(item1[1]),vic_email:itm[2]});

            });

        }
        $("#favourite_popup").dialog('close');

        $('input[type=checkbox]:checked').removeAttr('checked');
    });

    $('#quick_reply_but').click(function () {
        $("#quickmessage_popup").dialog({ modal: true, title: 'Search Options', width: 600, position: ['top', 20]  });
    });

    $('#favourite_but').click(function () {
        var send_type = $("#sent_to_persion").val();
        $.ajax({
            url: '<?php echo Yii::app()->request->baseUrl; ?>/Message/FavouriteSelect',
            type: 'POST',
            data: {send_type: send_type},
            success: function (data) {
                if (data != 'No Result Found') {
                    $('#favourite_result').html(data);

                }
            }
        });


        $("#favourite_popup").dialog({ modal: true, title: 'Search Options', width: 600, position: ['top', 20]  });
        $("input").focus();
    });

    $('#message_add_button').click(function () {
        if ($("#message_add").val() != '') {
            var message = $("#message_add").val();
            $.ajax({
                url: '<?php echo Yii::app()->request->baseUrl; ?>/Message/AddQuickMail',
                type: 'POST',
                data: {message: message},
                success: function (data) {
                    $('#message_add').val('');
                    $('#success_message_add').html('Message Added ');
                    $('#success_message_add').show();
                    $('#quickmessage_result').prepend(data);
                }
            });

        } else {
            alert('Please Enter a Message');
        }

    });


    $('#favourite_add_button').click(function () {
        if ($("#favourite_add").val() != '') {
            var contact_id = $("#fav_r_id").val();
            var contact_email = $("#fav_r_email").val();
            var contact_type = $("#fav_r_type").val();
            var contact_name = $("#fav_name").val();
            $.ajax({
                url: '<?php echo Yii::app()->request->baseUrl; ?>/Message/AddFavouriteMail',
                type: 'POST',
                data: {contact_id: contact_id, contact_email: contact_email, contact_type: contact_type, contact_name: contact_name},
                success: function (data) {

                    if (data != 'No Result Found') {
                        $('#success_message_favourite').html('Message Added ');
                        $('#success_message_favourite').show();
                        $("#favourite_add").tokenInput("clear");
                        $('#favourite_add').val('');
                        $('#favourite_add').html('');
                        $('#favourite_result').html(data);

                    }

                }
            });

        } else {
            alert('Please Enter a valid user');
        }

    });

</script>

<script type="text/javascript">

    $(document).ready(function () {
        var tempData = $('#sent_to_persion').val();
        $("#sent_to_persion").change(function () {
            tempData = $('#sent_to_persion').val();
            var tokenlimiting= null;
            if(tempData=='patient'){
                tokenlimiting=1;
            }
            if($('#login_type').val()=='patient'){
                tokenlimiting=1;
            }
            $('#contact_r_id').val(0);
            $('#contact_r_email').val("");
            $('#contact_r_type').val("");
            $('#contact_id').val("");
            $("#demo-input-facebook-theme").tokenInput('<?php echo Yii::app()->request->baseUrl; ?>/Message/getContacts?sent_to_persion=' + tempData, {
                tokenLimit: tokenlimiting,
                preventDuplicates: true,
                queryParam: "keyword",
                theme: "facebook",

                onAdd: function (results) {
                    arr = [];
                    arr1 = [];
                    arr2 = [];
                    if(tempData=='patient'){
                        mail_patient(results['vic_id']);

                    }else  if($('#login_type').val()=='doctor'){
                        if(tempData=='doctor'){
                            $('#mail_doctor_search').show();
                            mail_patient('');
                        }
                    }
                    if ($('#contact_r_id').val() == '' || $('#contact_r_id').val() == null || $('#contact_r_id').val() == 0) {
                        $('#contact_r_id').val(results['vic_id']);
                    } else {
                        arr.push($('#contact_r_id').val(), results['vic_id'])
                        $('#contact_r_id').val(arr);
                    }
                    if ($('#contact_r_email').val() == '' || $('#contact_r_email').val() == null) {
                        $('#contact_r_email').val(results['vic_email']);
                    } else {
                        arr1.push($('#contact_r_email').val(), results['vic_email'])
                        $('#contact_r_email').val(arr1);
                    }

                    $('#contact_r_type').val(tempData);
                    return results;
                },
                onDelete: function (item) {


                    console.log(item);
                    var ids = $('#contact_r_id').val().toString();
                    if (ids.indexOf(item.id) != 0) {
                        var tem = ids.replace(',' + item.id, '');
                    } else {
                        tem = ids.replace(item.id + ',', '');
                        tem = ids.replace(item.id, '');
                    }
                    $('#contact_r_id').val(tem);


                    var emails = $('#contact_r_email').val().toString();
                    console.log(emails);

                    if (emails.indexOf(item.vic_email) != 0) {
                        var tem1 = emails.replace(',' + item.vic_email, '');
                    } else {
                        tem1 = emails.replace(item.vic_email + ',', '');
                        tem1 = emails.replace(item.vic_email, '');
                    }
                    $('#contact_r_email').val(tem1);

                },
                resultsFormatter: function (item) {
                    if (tempData == 'doctor') {
                        item.vic_dob = '';
                    }
                    return "<li>" + item.name + " " + item.vic_dob + "</li>"
                }

            });


            $("#favourite_add").tokenInput('<?php echo Yii::app()->request->baseUrl; ?>/Message/getContacts?sent_to_persion=' + tempData, {
                preventDuplicates: true,
                queryParam: "keyword",
                theme: "facebook",

                onAdd: function (results) {
                    arr = [];
                    arr1 = [];
                    arr2 = [];
                    if ($('#fav_r_id').val() == '' || $('#fav_r_id').val() == null || $('#fav_r_id').val() == 0) {
                        $('#fav_r_id').val(results['vic_id']);
                    } else {
                        arr.push($('#fav_r_id').val(), results['vic_id'])
                        $('#fav_r_id').val(arr);
                    }
                    if ($('#fav_r_email').val() == '' || $('#fav_r_email').val() == null) {
                        $('#fav_r_email').val(results['vic_email']);
                    } else {
                        arr1.push($('#fav_r_email').val(), results['vic_email'])
                        $('#fav_r_email').val(arr1);
                    }

                    if ($('#fav_name').val() == '' || $('#fav_name').val() == null) {
                        $('#fav_name').val(results['vic_name']);
                    } else {
                        arr2.push($('#fav_name').val(), results['vic_name'])
                        $('#fav_name').val(arr2);
                    }
                    $('#fav_r_type').val(tempData);
                    return results;
                },
                onDelete: function (item) {


                    var ids = $('#fav_r_id').val().toString();
                    if (ids.indexOf(item.id) != 0) {
                        var tem = ids.replace(',' + item.id, '');
                    } else {
                        tem = ids.replace(item.id + ',', '');
                        tem = ids.replace(item.id, '');
                    }
                    $('#fav_r_id').val(tem);


                    var emails = $('#fav_r_email').val().toString();
                    console.log(emails);

                    if (emails.indexOf(item.vic_email) != 0) {
                        var tem1 = emails.replace(',' + item.vic_email, '');
                    } else {
                        tem1 = emails.replace(item.vic_email + ',', '');
                        tem1 = emails.replace(item.vic_email, '');
                    }
                    $('#fav_r_email').val(tem1);

                    var names = $('#fav_name').val().toString();
                    if (names.indexOf(item.user_first_name + ' ' + item.user_last_name) != 0) {
                        var tem2 = names.replace(',' + item.user_first_name + ' ' + item.user_last_name, '');
                    } else {
                        tem2 = names.replace(item.user_first_name + ' ' + item.user_last_name + ',', '');
                        tem2 = names.replace(item.user_first_name + ' ' + item.user_last_name, '');
                    }
                    $('#fav_name').val(tem2);


                },
                resultsFormatter: function (item) {
                    if (tempData == 'doctor') {
                        item.vic_dob = '';
                    }
                    return "<li>" + item.name + " " + item.vic_dob + "</li>"
                }
            });
        });
        $('#sent_to_persion').trigger('change');


    });
</script>