<?php

$this->breadcrumbs = array(
    'Messaging Center' => array('inbox'),
    'Inbox Mail',
);
if (isset(Yii::app()->session['logged_user_type']) && (Yii::app()->session['logged_user_type'] == "doctor"||Yii::app()->session['logged_user_type'] == "parcticeAdmin")) {
    $user_type = 1;
    $owner_type = "d";
} elseif ((Yii::app()->session['logged_user_type'] == "patient")) {
    $user_type = 0;
    $owner_type = "p";
}
?>
<div class="main">
    <div id="breadcrumb" class="fk-lbreadbcrumb newvd">
        <!--<span><a href="">Home</a></span> >
        <span>Dashboard</span>-->
        <?php $this->widget('zii.widgets.CBreadcrumbs', array(
            'links' => $this->breadcrumbs,
        ));
        ?>
    </div>
    <!--<div class="name_fld">

        <input type="text" style="width: 50%" id="patient_name_search" class="filter_txtfld_calender fld_class doccustom" placeholder="Type your patient name.." onkeyup="autosearch();"  value=""/>
        <input type="hidden" name="patient_id" id="patient_name_id" value="" />
        <input type="hidden" name="patient_name" id="patient_name_save" value="" />
        <ul id="patients_list" style="display:none;"></ul>
    </div>-->


    <div class="dashboard_mainarea">
        <div class="leftmenu">
            <?php $this->renderPartial('//layouts/navigation'); ?>
        </div>
        <div class="rightarea_dashboard">
            <div class="dashboardcont_leftbox">

                <?php if (isset(Yii::app()->session['logged_user_type']) && (Yii::app()->session['logged_user_type'] == "doctor"||Yii::app()->session['logged_user_type'] == "parcticeAdmin"||Yii::app()->session['logged_user_type'] == "patient")) { ?>
                <form id="searchform" style="margin: 1px !important;margin-bottom: 18px !important;display: inline-block;margin-top: 14px !important">
                    <div>
                      <span style="font-size: 19px;color: #0096CE;font-weight: bold;">Search </span>
                        <div style="display: inline-block;">
                            <div>
                                <label style="margin-bottom: 0px!important;float: left; margin-left: 75px;   color: #0096CE;">Type</label>
                                <label style="margin-left: 130px !important;float: left;    color: #0096CE;">Search Name </label>
                                <label style="margin-bottom: 0px!important;float: right; margin-left: 250px;   color: #0096CE;">Department/Category</label>
                            </div>

                            <div class="newtokeninputfield" style="float: left;margin-right: 300px !important;">
                            <div class="name_fld" style="margin-left: 200px !important;">
                                <input type="text" class="fld_class" style="width=300px;" id="demo-input-facebook-theme" name="search"  placeholder="Type your search name.." />
                            </div>
                        </div>

                    </div>
                        <?php if(Yii::app()->session['logged_user_type'] == "patient"){
                            $list = array('doctor'=>'doctor');
                        }
                        else{
                            $list = array('patient'=>'patient','doctor'=>'doctor');
                        }
                         echo CHtml::dropDownList('searchtype',$searchType,
                            $list,
                            array('class'=>'select_fld_class inboxdropdown')); ?>
                        <div style="float: right;margin-top: -6px;height:30px;">
                            <?php
                            $department = Department::model()->findAll();
                            ?>
                            <select id="department" name="department" class="select_fld_class" style="height: 30px;">
                                <option value="">All </option>
                            <?php foreach ($department as $d){ ?>
                                <option <?php if($dept==$d->id){echo 'selected="selected"'; } ?> value="<?php echo $d->id ?>"><?php echo $d->name ?></option>
                            <?php } ?>

                            </select>
                        </div>
                    </div>



            <div style="clear: both"></div>

            <!-- <input type="submit" style="margin-top: 4px;border: none;border-radius: 5px;font-size: 11px;font-weight: bold;height: auto;padding: 10px;width: auto;background-color: #54cbc8;margin-left: 5px;" />-->
            </form>
            <?php } ?>
            <h1>
                <span>Inbox</span>

                <form id="msg_limit" name="msg_limit" method="POST"
                      action="<?php echo Yii::app()->request->baseUrl . '/message/inbox'; ?>"
                      onchange="javascript: return page_limit_submit();" autocomplete="off">
          <span class="select_mail">
          	<select name="msg_limit_drp">
                <option value="10" <?php if ($msg_limit_drp == "10") {
                    echo "selected";
                } ?>>10
                </option>
                <option value="15" <?php if ($msg_limit_drp == "15") {
                    echo "selected";
                } ?> >15
                </option>
                <option value="20" <?php if ($msg_limit_drp == "20") {
                    echo "selected";
                } ?> >20
                </option>
            </select>
          </span>
                </form>
                <script>
                    function page_limit_submit() {
                        $("#msg_limit").submit();
                    }
                </script>
            </h1>
            <div class="massege_title custom_message_title">
                <span class="chkbox"><input type="checkbox" id="selectall"/></span>
                <span class="subject">Subject</span>
                <span class="mbody" style="width: 30%">Message body</span>
                <span class="from">From</span>
                <span class="from" style="width: 12%">Category</span>
                <span class="date" style="width: 15%">Date</span>


                <span style="font-size:16px;" class="delete fa fa-trash" id="delete" Title="Delete" ></span>
            <span style="float:right; margin-top: 2px; position: absolute;cursor: pointer;    color:#2c9492;
    padding-left: 8px;" class="archive fa fa-file-archive-o " Title="File Message"
                  id="archive"></span></div>


            <?php
            if (count($inbox_data) > 0) {
                $pagination_no = "";
                if (isset($_GET['page']) && ($_GET['page'] != "")) {
                    $page_number = $_GET['page'];
                    $pagination_no = "/pgn/" . $page_number;
                }
                foreach ($inbox_data as $key) {

                    ?>
                    <div
                        class="massege_content" <?php if ($key['max_read'] != 1) { ?> style="background: #E2FDDE;" <?php } ?> >
                            <span class="chk"><input type="checkbox" class="inbox_row"
                                                     value="<?php echo $key['thread_id']; ?>" name="getR[]"/></span>
	                      	<span class="sender-subject">&nbsp;

	                      		<a href="<?php echo Yii::app()->request->baseUrl; ?>/Message/details/msgType/in/msg/<?php echo $key['thread_id']; ?><?php echo $pagination_no; ?>" <?php if ($key['max_read'] != 1) { ?> style="color: #3528EB;font-weight: bold;overflow: hidden;
    text-overflow: ellipsis;
    width: 70px;" <?php } ?> >

                                    <?php

                                    $content = trim($key['subject']);
                                    $content_summery = strip_tags($content);
                                    if (strlen($content_summery) > 10) {
                                        echo substr($content_summery, 0, 10) . "..";
                                    } else {
                                        echo substr($content_summery, 0, 10);
                                    }

                                    ?>
                                </a>
	                      	</span>

	                      	<span class="sender-sub"style="width: 29%">
	                      		<a href="<?php echo Yii::app()->request->baseUrl; ?>/Message/details/msgType/in/msg/<?php echo $key['thread_id']; ?><?php echo $pagination_no; ?>" <?php if ($key['max_read'] != 1) { ?> style="color: #3528EB;font-weight: bold;" <?php } ?> >
                                    <?php
                                    $content = trim($key['msg_body']);
                                    $content_summery = strip_tags($content);

                                    echo substr($content_summery, 0, 100) . " ...";
                                    ?>
                                </a>
                                <?php if (isset($key['shared_id']) && $key['shared_id'] != '' && $key['shared_type'] == $owner_type) {

                                    ?>

                                    <span class="sender-name1 fa fa-paperclip" style="font-size: 19px;"></span>
                                <?php } ?>
	                      	</span>

                        </a>
                        <span class="sender-name">&nbsp;<?php echo $key['vic_name']; ?></span>
                        <?php $id = $key['department_id'];
                        $department = Department::model()->findByPk($id);
                        ?>
                        <span class="sender-date"style="width: 12%">&nbsp;<?php if(isset($department)){  echo $department->name; } ?></span>

                        <span class="sender-date">&nbsp;<?php echo date("m-d-Y h:i A", $key['created']); ?></span>
                    </div>
                <?php
                }
                ?>
                <div class="message-pagein-section">
                    <?php
                    $this->widget('CLinkPager', array(
                        'pages' => $pages, 'header' => '', 'prevPageLabel' => '&lt;&lt;', 'nextPageLabel' => '&gt;&gt;',
                    ));
                    ?>
                </div>
            <?php
            } else {
                ?>
                <div class="massege_content">
                    No Message Found.
                </div>
            <?php } ?>
        </div>
    </div>
</div>
<script type="text/javascript">
    var vals = [];
    $(function () {
        $("#selectall").click(function () {
            $('.inbox_row').attr('checked', this.checked);
        });
        $(".inbox_row").click(function () {
            if ($(".inbox_row").length == $(".inbox_row:checked").length) {
                $("#selectall").attr("checked", "checked");
            } else {
                $("#selectall").removeAttr("checked");
            }
        });
    });

    $('#delete').click(function () {
        var list_r = "";
        $(".inbox_row").each(function (e) {
            if ($(this).is(':checked')) {
                if (list_r == "") {
                    list_r = $(this).attr('value');
                } else {
                    list_r = list_r + "|" + $(this).attr('value');
                }
            }
        });

        if (list_r == "") {
            alert("Please Mark Message first.");
            return false;
        } else {
            if (confirm('Are you sure you want to delete this?')) {
                $.ajax({
                    url: '<?php echo Yii::app()->request->baseUrl; ?>/Message/deletemessage',
                    type: 'POST',
                    data: {keyword: list_r},
                    success: function (data) {
                        // location.reload();
                    }
                });
            }
        }
    });

    $('#archive').click(function () {
        var list_r = "";
        $(".inbox_row").each(function (e) {
            if ($(this).is(':checked')) {
                if (list_r == "") {
                    list_r = $(this).attr('value');
                } else {
                    list_r = list_r + "|" + $(this).attr('value');
                }
            }
        });

        if (list_r == "") {
            alert("Please Mark Message first.");
            return false;
        } else {
           // if (confirm('Are you sure you want to Archieve this?')) {
                $.ajax({
                    url: '<?php echo Yii::app()->request->baseUrl; ?>/Message/archivemessage',
                    type: 'POST',
                    data: {keyword: list_r},
                    success: function (data) {
                        location.reload();
                    }
                });
            //}
        }
    });
</script>
<script type="text/javascript">

    $(document).ready(function () {
        var tempData = $('#searchtype').val();
        $("#searchtype").change(function () {
            tempData = $('#searchtype').val();

            $("#demo-input-facebook-theme").tokenInput('<?php echo Yii::app()->request->baseUrl; ?>/Message/getContacts?sent_to_persion=' + tempData, {
                tokenLimit: 1,
                preventDuplicates: true,
                queryParam: "keyword",
                theme: "facebook",
                hintText:"Type your search name..",
                <?php if(isset($searchName) && $searchName!="" && $searchId!="") { ?>
                prePopulate:[{id:<?php echo $searchId; ?>,name:'<?php echo $searchName; ?>'}],
                <?php } ?>
                onAdd: function (results) {
                    $( "#searchform" ).submit();
                    return results;
                },
                resultsFormatter: function (item) {
                    if (tempData == 'doctor') {
                        item.vic_dob = '';
                    }
                    return "<li>" + item.name + " " + item.vic_dob + "</li>"
                }
            });
        });
        $('#searchtype').trigger('change');

        $( "#department" ).change(function() {
            $( "#searchform" ).submit();
        });



    });
</script>