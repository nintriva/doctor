<?php

$this->breadcrumbs = array(
    'Messaging Center' => array('inbox'),
    'Message Details',
);

if (isset(Yii::app()->session['logged_user_type']) && (Yii::app()->session['logged_user_type'] == "doctor" ||Yii::app()->session['logged_user_type']=='parcticeAdmin')) {
    $user_type = 1;
    $owner_type = "d";
} elseif ((Yii::app()->session['logged_user_type'] == "patient")) {
    $user_type = 0;
    $owner_type = "p";
}
?>
<div class="main">
<div id="breadcrumb" class="fk-lbreadbcrumb newvd">
    <!--<span><a href="">Home</a></span> >  
    <span>Dashboard</span>-->
    <?php $this->widget('zii.widgets.CBreadcrumbs', array(
        'links' => $this->breadcrumbs,
    ));
    /*echo "<pre>";
    print_r($inbox_arr_details);exit;*/
    ?>
</div>
<div class="dashboard_mainarea">
<div class="leftmenu">
    <?php $this->renderPartial('//layouts/navigation'); ?>
</div>
<?php //echo "<pre>";print_r($inbox_arr_details);  exit;?>
<div class="rightarea_dashboard">
<?php if (count($inbox_arr_details) > 0) {
    $first_key = key($inbox_arr_details);
    end($inbox_arr_details);
    $key = key($inbox_arr_details);
    ?>
    <div class="dashboardcont_leftbox messg_details" >
    <div class="massege_title" style=" margin-bottom: 10px;">


        <?php
        if ($_GET['msgType'] == "se") {
            if ($pgn == "0") {
                $back_action = Yii::app()->request->baseUrl . '/message/sentMail';
            } else {
                $back_action = Yii::app()->request->baseUrl . '/message/sentMail?page=' . $pgn;
            }
        } elseif ($_GET['msgType'] == "ar") {
            if ($pgn == "0") {
                $back_action = Yii::app()->request->baseUrl . '/message/ArchiveMail';
            } else {
                $back_action = Yii::app()->request->baseUrl . '/message/ArchiveMail?page=' . $pgn;
            }
        } else {
            if ($pgn == "0") {
                $back_action = Yii::app()->request->baseUrl . '/message/inbox';
            } else {
                $back_action = Yii::app()->request->baseUrl . '/message/inbox?page=' . $pgn;
            }
        }
        ?>
        <span class="massege_title">

                        <input type="button" style="width: 80px !important;    padding: 4px !important;" class="mesg_rply_btn customreplybutton" id="reply_but" value="Reply">


            <a href="<?php echo $back_action; ?>" style=" font-weight: 700 !important;" class="mesg_rply_btn customreplybutton">Back</a>
	            </span>
        </span>
    </div>



    <div style="clear:both"></div>
    <h1 style="    margin-right: 36px;"><span>Subject : <?php echo $inbox_arr_details[$first_key]['subject']; ?></span></h1>
    <div class="mesg_rply_box_custom" id="reply_msg_area" style="display: none;margin-top: 10px;">
        <?php if ($owner_type == "p") {
            ?>
            <div style="margin-left: 14px !important;word-wrap: break-word ;font-weight: bold;"><span style="color:#0096CE;">Disclaimer : </span><span>If you think you or your family member has a medical or psychiatric emergency
                    ,call 911 or go to the nearest hospital.Do not attempt to access emergency care through this website.
                    All Messages and attachments you send and receive through this website will become part of your medical records on eDoctorBook </span></div>

        <?php
        } ?>
        <div class="fld_area" style="    height: 60px;">

            <div style="clear:both; margin-bottom: 10px !important;"></div>
            <label class="fld_name required">To</label>

            <div class="name_fld">


                <div class="newtokeninputfield replymultiple" style="">
                    <input type="text" class="fld_class" id="demo-input-facebook-theme" name="contact_id"/>
                </div>

                <?php
                if (isset($inbox_arr_details[$first_key]['form_email'])&&Yii::app()->session['logged_user_email_address'] == $inbox_arr_details[$first_key]['form_email']) {
                    ?>

                    <input type="hidden" class="fld_class" id="mailval"
                           value="<?php echo htmlspecialchars(json_encode($inbox_arr_details[$first_key]['to'], JSON_NUMERIC_CHECK)) ?>"/>

                <?php

                } elseif (isset($inbox_arr_details[$key]['form_email'])&&Yii::app()->session['logged_user_email_address'] != $inbox_arr_details[$key]['form_email']) {
                    $checkvar = 0;
                    foreach ($inbox_arr_details[$first_key]['to'] as $checkarray) {
                        if (Yii::app()->session['logged_user_email_address'] == $checkarray['email']) {
                            $checkvar = 1;
                        }
                    }
                    if ($checkvar == 0) {
                        ?>

                        <input type="hidden" class="fld_class" id="mailval"
                               value="<?php echo htmlspecialchars(json_encode($inbox_arr_details[$first_key], JSON_NUMERIC_CHECK)) ?>"/>
                    <?php
                    } else {
                        ?>
                        <input type="hidden" class="fld_class" id="mailval"
                               value="<?php echo htmlspecialchars(json_encode($inbox_arr_details[$key]['from'], JSON_NUMERIC_CHECK)) ?>"/>
                    <?php
                    }
                } else {
                    ?>
                    <input type="hidden" class="fld_class" id="mailval"
                           value="<?php echo htmlspecialchars(json_encode($inbox_arr_details[$key]['to'], JSON_NUMERIC_CHECK)) ?>"/>

                <?php
                }
                ?>

                <input type="hidden" id="contact_r_id" name="contact_r_id"
                       value="0">
                <input type="hidden" id="contact_r_email" name="contact_r_email"
                       value="">
                <input type="hidden" id="contact_r_type" name="contact_r_type" value="">

                <input type="hidden" id="subject" name="subject"
                       value="<?php echo $inbox_arr_details[$key]['subject']; ?>">
                <input type="hidden" id="thread" name="thread"
                       value="<?php echo $inbox_arr_details[$first_key]['thread_id']; ?>">
                <input type="hidden" id="message_id" name="message_id"
                       value="<?php echo $inbox_arr_details[$key]['inbox_id']; ?>">
                <input type="hidden" id="department" name="department"
                       value=<?php echo $inbox_arr_details[$first_key]['department_id']?>>
            </div>
            <div class="clear"></div>
        </div>
        <div class="fld_area">
            <div class="fld_name fld_name_hight">Message</div>
            <div class="name_fld">
                <i type="button"
                   style="background-color: #54cbc8;border-radius: 5px;color: #ffffff !important;border: 0px;   height: 20px;
    margin-left: 420px;
    margin-top: 0px;
    position: absolute;
    font-size: 14px;
    padding-top: 5px !important;
    width: 18px !important;"
                   class="registbt mesg_rply_btn fa fa-envelope" id="quick_reply_but" ></i>
                <textarea name="msg_body" id="msg_body" size="32" maxlength="256" class="txtarea_class"></textarea>


            </div>
            <div name="search_document" id="search_document" class="btn mapbook fa fa-paperclip"style="            background-color: #54cbc8;
    border-radius: 5px;
    color: #ffffff !important;
    border: 0px;
    height: 20px;
    margin-left: 13px;
  margin-top: 35px;
    position: absolute;
    font-size: 18px;
padding: 4px 7px 1px 10px !important;
    width: 20px !important;"onclick="documentSearch();"></div>
            <div class="clear"></div>
        </div>
        <div class="fld attachment">

            <label class="fld_name required">&nbsp;</label>
            <?php if ($owner_type == "p") {
                ?>
                <div style="margin-left: 2px !important;word-wrap: break-word;margin-top: 16px !important;font-weight: bold;padding-left: 220px;"><span style="color: #0096CE;text-align: center !important;">Disclaimer : </span><span >By clicking the send button,you are providing your acknowledgement and agreement to eDoctorBook Terms and Conditions and Privacy and Policies </span></div>   <?php } ?>
            <div style="clear: both"></div>
                        <span>
                            <!--<label href="#" class='js-open-modal' style="width: 150px;
    background-color: #eaeaea;
    float: left;margin-bottom: 20px;border-radius: 5px;">Attach Existing Documents</label>-->

  <a style=" width:390px;display: none; margin-left: 220px;margin-top: 5px;word-break: break-word; background-color: #EAEAEA;
    margin-bottom: 5px;
    border-bottom: 2px solid white;"
     href="#" id="docs_search" name="docs_search"></a>

                             <input type="hidden" id="docs_search_id" name="docs_search_id[]" value=""/>

                              <span class="attachbtngroup">
		              <input type="button" id="submit" class="registbt" value="Send">
		              <input type="button" id="cancel" class="registbt" value="cancel"
                             onclick="document.getElementById('reply_msg_area').style.display='none';document.getElementById('msg_body').value=''">
		            </span>

                            <!--new document attach-->
	              	 <!--<button  class='js-open-modal btn mapbook' data-modal-id="popup" style="width:173px;margin-left: 225px;font-size: 12px">Attach New Document</button>
                            <input type="hidden" id="doc_id" name="doc_id" value=""/>
                            <a  style="    float: right;
    margin-top: 14px;
    margin-right: 208px;
    background-color: white;" href="#" type="hidden" id="doc_name"></a>-->
	              </span>
        </div>
        <div>
            <label class="fld_name required">&nbsp;</label>

        </div>

    </div>
    <div style="clear: both; margin-top: 10px;"></div>
    <?php  foreach ($inbox_arr_details as $arr_details){
    ?>
        <div>
        <div class="detailcustom">



            <div class="massege_title msgbodycustm " style="color: #808080;">
                <div class="detailtofrom" style="height:90px">
                <div class="massege_title marginzero" style="margin-top:0px;">
                    <span class="sender_label custombold">To :</span>
                    <!--               --><?php //foreach($arr_details['to'] as $to ){?>
                    <span class="sender_body"><?php foreach ($arr_details['to'] as $key1 => $to) {
                            if ($key1 > 0) {
                                echo ',';
                            }
                            echo $to['name'];
                        }?></span>
                    <?php

                    //                }
                    ?>
                </div>
                    <?php $id = $inbox_arr_details[$first_key]['department_id'];
                    $department = Department::model()->findByPk($id);
                    if(isset($department)){   ?>
                    <div class="massege_title marginzero">
                        <span class="sender_label custombold" sty>Department:</span>
                        <span class="sender_body"><?php echo $department->name; ?></span>
                    </div>
                    <?php }?>

                <div class="massege_title marginzero">
                    <span class="sender_label custombold">From :</span>
                    <span class="sender_body"><?php echo $arr_details['form_name']; ?></span>
                </div>

                <div class="massege_title marginzero">
                    <span class="sender_label custombold">Received :</span>
	          <span class="sender_body">
	          	<span class="massege_title marginzero"
                      style="font-weight:normal;"><?php echo date('m-d-Y  h:i A', $arr_details['created']); ?></span></span>
                </div>
                </div>

               <!-- <div class="massege_title"  style="margin-top: -8px;">
                    <span class="sender_label ">Subject :</span>
                    <span class="sender_body"  ><?php /*echo $arr_details['subject']; */?></span>

                </div>-->
                <div style="clear: both;"></div>
	            <span>
	            	<p class="msgbodynew">
                        <?php echo $arr_details['msg_body']; ?>
                    </p>
	            </span>
                <?php

                $shared = SharedDocument::model()->findAllByAttributes(array('message_id' => $arr_details['inbox_id']));

                if (isset($shared) && $shared != null){

                ?>
                <br><br>

                <span class="sender_label"
                      style="font-size: 17px;margin-top: 2%;float: none;"><strong style="font-weight: bold;font-size: 12px;">Attachment:</strong></span>
                <span>

<table class="attachtable">
    <?php
    foreach ($shared as $share) {
    $document = Documents::model()->findAllByAttributes(array('id' => $share->document_id));
    if (isset($document)) {
    foreach ($document as $doc) {
    foreach ($doc->docFiles as $files) {

    $file =  $files->file_url;

    $file1 = explode('_', $file);
    $text = str_replace(end($file1), '', $file);
    $name = rtrim($text, "_")
    ?>
    <tr ><td  style="width:50%">

            <div class="attachcustom" target="_blank" style="word-wrap: break-word;
    ">
                <?php echo $name; ?>
            </div>
            </div>


            </td><td style="width:25%;">
                <div style="margin-left: 50px;">
                    <a  class="attachcustom" target="_blank" style="float: left;" href="<?php echo Yii::app()->createUrl('documents/download', array('url' => $files->id)); ?>">
                        View
                    </a>
            </td>
            <td style="width:25%;">
                <a  class="attachcustom"  style="float: left;"
                    href="<?php echo Yii::app()->createUrl('documents/link', array('url' => $files->id)); ?>">
                    Download
                </a>
        </div>
            </td>
            <?php
            }
            }
            }
            }
            echo '</table>';
            } ?>

            <div class="clear">&nbsp;</div>
	            <span class="massege_title ">
	             <strong class="msgbodynew" style="font-size:12px !important;">Regards,</strong><br/>
                  <p class="msgbodynew" style="margin-top: 0px;font-size:12px !important;"><?php echo $arr_details['form_name']; ?></p>
	            </span>

            </span>





            </div>



        </div>

    <div class="massege_title">
        <div style="margin-top: 0px;"></div>
<!--        <hr style="    width: 720px;border: 1px solid #e5e5e5;">-->

        <?php } ?>

    </div>
    </div>
    <div class="clear"> &nbsp;</div>
    <?php /*
	        <form id="compose_reply_mail" name="compose_reply_mail" method="post" action="<?php echo Yii::app()->request->baseUrl.'/message/details'; ?>" onsubmit="javascript: return validate_compose_reply_mail();" autocomplete="off" >
		    */
    ?>



    <div id="quickmessage_popup" style="display:none;">
        <div class="dashboard_content1">
            <div class="dashboardcont_leftbox">
                <div class="add_area">
                <span class="add_calender">
                    <div class="filter_search_area_calender">

                        <div id="success_message_add" style="    color: #717171;font-weight: bold;"></div>
                        <div class="filter_apdate_calender_new1">
                            <div style="width: 134%; ">

                                <input type="text" style="width: 50%" name="message_add" id="message_add"
                                       class="filter_txtfld_calender fld_class" placeholder="Add New Message.."
                                       value=""/>
                                <input type="button" id="message_add_button" style="cursor:pointer;"
                                       class="message_add_button" value="Add">


                            </div>

                        </div>
                        <!--<div class="filter_apbtn_new"><input type="button" class="search_btn" name="" value="Search" onclick="patientResultSearch();"/></div>-->
                        <div class="clear"></div>

                    </div>
                </span>

                    <div class="clear"></div>
                </div>
            </div>
            <div class="dashboardcont_leftbox2">
                <ul>
                    <li class="heading">
                        <span class="active" style="width:25%">messages</span>

                        <span class="att txt_align">Action</span>
                    </li>
                    <li style="overflow: auto; max-height: 250px; height: 100%;">
                        <ul id="quickmessage_result">


                            <?php
                            $criteria = new CDbCriteria();
                            $criteria->order = 'id DESC';
                            $Quickmessages = Quickmessages::model()->findAllByAttributes(array('user_id' => Yii::app()->session['logged_user_id'], 'user_type' => $user_type), $criteria);

                            foreach ($Quickmessages as $quick_mes) {

                                ?>
                                <li>
                                    <span class="active" style="width:25%"><?php echo $quick_mes->messages; ?></span>


                         <span class="att txt_align">
                            <?php echo CHtml::CheckBox("messagelist[]", false, array('value' => $quick_mes->id . '_' . $quick_mes->messages)); ?>
                         </span>
                                </li>
                            <?php } ?>


                        </ul>
                    </li>
                </ul>
            </div>

            <input style="float: right;border-radius:7px;height:35px;" class="registbt" data-dismiss="modal"
                   id="select_quickmessage_checkbox"
                   type="button" value="Select">
        </div>
    </div>







    <?php /*
		    </form>
		    	*/
    ?>
    </div>
<?php } ?>

</div>

</div>

<div id="patient_search_popup" style="display:none;">
    <div class="dashboard_content1">
        <div class="dashboardcont_leftbox">
            <div class="add_area">
                <span class="add_calender">
                    <div class="filter_search_area_calender">


                        <div class="filter_apdate_calender_new1">
                            <div style="width: 134%; ">

                                <?php if (isset(Yii::app()->session['logged_user_type']) && (Yii::app()->session['logged_user_type'] == "doctor")) { ?>
                                    <input type="text" style="width: 50%" name="document_name" id="mail_doctor_search"
                                           class="filter_txtfld_calender fld_class"
                                           placeholder="Type your patient name.." onkeyup="patientList();" value=""/>
                                <?php } ?>

                                <?php if (isset(Yii::app()->session['logged_user_type']) && (Yii::app()->session['logged_user_type'] == "patient")) { ?>
                                    <input type="text" style="width: 50%" name="document_name" id="mail_doc_search"
                                           class="filter_txtfld_calender fld_class"
                                           placeholder="Type your Document name.." onkeyup="documentList();" value=""/>
                                <?php } ?>

                                <ul id="mail_patient_id" style="display:none;width: 42%"></ul>

                            </div>

                        </div>
                        <!--<div class="filter_apbtn_new"><input type="button" class="search_btn" name="" value="Search" onclick="patientResultSearch();"/></div>-->
                        <div class="clear"></div>

                    </div>
                </span>

                <div class="clear"></div>
            </div>
        </div>
        <div class="dashboardcont_leftbox2">
            <ul>
                <li class="heading">
                    <span class="active" style="width:25%">Document Name</span>
                    <span class="active" style="width:20%"> Label</span>
                    <span class="active" style="width:20%"> Date</span>

                    <span class="att txt_align">Action</span>
                </li>
                <li style="overflow: auto; max-height: 250px; height: 100%;">
                    <ul id="patient_result_search">

                        <?php $doc = Documents::model()->findAllByAttributes(array('owner' => Yii::app()->session['logged_user_id'], 'owner_type' => $owner_type));
                        $criteria1 = new CDbCriteria();
                        $criteria1->addCondition('status="shared"');
                        $criteria1->addCondition('db_shared_document.shared_type=' . "'$owner_type'");
                        $criteria1->addCondition('db_shared_document.shared_id=' . Yii::app()->session['logged_user_id']);
                        $criteria1->join = 'LEFT JOIN db_shared_document ON t.id = db_shared_document.document_id';
                        $shared = Documents::model()->findAll($criteria1);
                        $document = array_merge($doc, $shared);
                        $document = array_unique($document, SORT_REGULAR);
                        foreach ($document as $docu) {
                            ?>
                            <li>
                                <span class="active" style="width:25%"><?php echo $docu->document_name; ?></span>
                                <span class="active" style="width:20%"><?php echo $docu->label; ?></span>
                                <span class="active" style="width:20%"><?php echo $docu->date; ?></span>

                         <span class="att txt_align">
                            <?php echo CHtml::CheckBox("idList[]", false, array('id' => 'idList', 'value' => $docu->id . '_' . $docu->document_name)); ?>
                         </span>
                            </li>
                        <?php } ?>
                    </ul>
                </li>
            </ul>
        </div>

        <input style="float: right" onclick="selectdocument();" class="registbt" data-dismiss="modal"
               id="select_checkbox" type="button" value="Select">
    </div>
</div>

<script type="text/javascript">
    function validate_compose_reply_mail() {
        var contact_r_id = $("#contact_r_id").val();
        var msg_body = $("#msg_body").val();
        if (contact_r_id == "" || contact_r_id == null) {
            $('#contact_r_id').val("");
            $('#contact_r_id').effect("highlight", {}, 5000);
            return false;
        }
        if (msg_body == "" || msg_body == null) {
            $('#msg_body').val("");
            $('#msg_body').effect("highlight", {}, 5000);
            return false;
        }
    }

    $("#submit").click(function () {
        var contact_r_id = $("#contact_r_id").val();
        var msg_body = $("#msg_body").val();
        var contact_r_email = $("#contact_r_email").val();
        var subject = $("#subject").val();
        var thread = $("#thread").val();
        var message_id = $("#message_id").val();
        var docs_search_id = $('#docs_search_id').val();
        var contact_r_type = $('#contact_r_type').val();
        var department = $('#department').val();
        if (contact_r_id == "" || contact_r_id == 0) {
            $('#contact_r_id').val("");
            $('#contact_r_id').effect("highlight", {}, 5000);
            return false;
        }
        if (msg_body == "" || msg_body == null) {
            $('#msg_body').val("");
            $('#msg_body').effect("highlight", {}, 5000);
            return false;
        }

        if (contact_r_id != "" || contact_r_id != 0) {
            $.ajax({
                url: '<?php echo Yii::app()->request->baseUrl; ?>/Message/replyMail',
                type: 'POST',
                data: {contact_r_id: contact_r_id, contact_r_email: contact_r_email, subject: subject, msg_body: msg_body, thread: thread, message_id: message_id, docs_search_id: docs_search_id, contact_r_type: contact_r_type,department :department},

                success: function (data) {
                    $('#reply_msg_area').hide();
                    $('#msg_body').html(" ");
                    $('#msg_body').val(" ");
                    location.reload();

                }
            });
        }
    });
    $('#reply_but').click(function () {
        $('#reply_msg_area').show();
    });


    $('#select_quickmessage_checkbox').click(function () {
        var messagelist = $("input[type=checkbox]:checked").serialize();
        var array1 = [];
        if (messagelist != '') {
            var itm1 = messagelist.split("&");
            $.each(itm1, function (key, value) {
                var itm1 = value.split("_");
                itm1[1] = itm1[1].replace(/\+/g, ' ');
                var dec = unescape(itm1[1]);
                array1.push(dec);
            });

        }
        $("#msg_body").val(array1);
        $("#msg_body").show();
        $("#quickmessage_popup").dialog('close');
        $('input[type=checkbox]:checked').removeAttr('checked');
    });


    $('#quick_reply_but').click(function () {
        $("#quickmessage_popup").dialog({ modal: true, title: 'Search Options', width: 600, position: ['top', 20]  });
    });

    $('#message_add_button').click(function () {
        if ($("#message_add").val() != '') {
            var message = $("#message_add").val();
            $.ajax({
                url: '<?php echo Yii::app()->request->baseUrl; ?>/Message/AddQuickMail',
                type: 'POST',
                data: {message: message},
                success: function (data) {
                    $('#message_add').val('');
                    $('#success_message_add').html('Message Added ');
                    $('#success_message_add').show();
                    $('#quickmessage_result').prepend(data);
                }
            });

        } else {
            alert('Please Enter a Message');
        }

    });
</script>


<script type="text/javascript">

    function selectdocument() {
        $("#patient_search_popup").dialog('close');
        var idList = $("input[type=checkbox]:checked").serialize();
        console.log(idList);
        var itm = idList.split("&");
        var array = [];
        $.each(itm, function (key, value) {
            var itm = value.split("_");
            itm[1] = itm[1].replace(/\+/g, ' ');
            array.push(itm[1]);
        });

        $('#docs_search').html("Attached Files: " + array);
        $('#docs_search').show();
        $('#docs_search_id').val(idList);
        $('input[type=checkbox]:checked').removeAttr('checked');

    }
    $('#attachment').click(function () {
        $('.attachment').show();
    });
    function documentSearch() {
        // $("#patient_result_search").html('No result found.');
        $("#patient_search_popup").dialog({ modal: true, title: 'Search Options', width: 600, position: ['top', 20]  });
    }

    function patientList() {
        var keyword = $('#mail_doctor_search').val();

        if (keyword != "" || keyword != null) {
            if (keyword.length > 2) {
                $.ajax({
                    url: '<?php echo Yii::app()->request->baseUrl; ?>/Patient/getLists',
                    type: 'POST',
                    data: {keyword: keyword},
                    success: function (data) {
                        $('#mail_patient_id').show();
                        $('#mail_patient_id').html(data);

                        if (keyword.length < 1) {
                            $('#mail_patient_id').hide();
                        }
                    }
                });
            }
            if (keyword.length < 1) {
                $('#mail_patient_id').hide();
            }
        }
    }

    function mail_patient(item) {

        var si;
        if(typeof (item)=='string'){
            var itm = item.split("|");
            var date = new Date(itm[1]);
            var date = (date.getMonth() + 1) + '-' + date.getDate() + '-' + date.getFullYear();
            var itmData = itm[0] + " " + date;
            $('#mail_doctor_search').val(itmData);
            $('#mail_patient_id').hide();
            si = itm[2];
        }else if(typeof (item)=='number' && item!='' ){
            si=item;
            $('#mail_doctor_search').hide();
        }else if(item==''){
            si=item;

        }

        $.ajax({
            method: 'POST',
            url: '<?php echo Yii::app()->createUrl('documents/search'); ?>',
            data: { kword: si},
            success: function (data) {
                $("#patient_result_search").html(data);
                $('#mail_doctor_search').val('');

            }


        });

        //alert(itm[0]+"--"+itm[1]+"--"+itm[2]);
    }

    function documentList() {
        var keyword = $('#mail_doc_search').val();

        if (keyword != "" || keyword != null) {
            if (keyword.length > 2) {
                $.ajax({
                    url: '<?php echo Yii::app()->request->baseUrl; ?>/Documents/getLists',
                    type: 'POST',
                    data: {keyword: keyword},
                    success: function (data) {
                        $('#mail_patient_id').show();
                        $('#mail_patient_id').html(data);
                        if (keyword.length < 1) {
                            $('#mail_patient_id').hide();
                        }
                    }
                });
            }
            if (keyword.length < 1) {
                $('#mail_patient_id').hide();
            }
        }
    }

    function docs(item) {

        var itm = item.split("|");
        var date = new Date(itm[1]);
        var date = (date.getMonth() + 1) + '-' + date.getDate() + '-' + date.getFullYear();
        var itmData = itm[0] + " " + date;
        $('#mail_doc_search').val(itmData);
        $('#mail_patient_id').hide();

        var si = itm[2];
        $.ajax({
            method: 'POST',
            url: '<?php echo Yii::app()->createUrl('documents/list'); ?>',
            data: { kword: si},
            success: function (data) {
                $("#patient_result_search").html(data);

            }


        });

    }


    function patientSelect(value, id) {
        $("#patient_search_popup").dialog('close');
        $('#docs_search').html("Attached File: " + value);
        $('#docs_search_id').val(id);
        $('#docs_search').show();
    }

    $(function () {

        var appendthis = ("<div class='modal-overlay js-modal-close'></div>");

        $('button[data-modal-id]').click(function (e) {
            e.preventDefault();
            $("body").append(appendthis);
            $(".modal-overlay").fadeTo(500, 0.7);
            //$(".js-modalbox").fadeIn(500);
            var modalBox = $(this).attr('data-modal-id');
            $('#' + modalBox).fadeIn($(this).data());
        });


        $(".js-modal-close, .modal-overlay,#close_popup").click(function () {
            $(".modal-box, .modal-overlay").fadeOut(500, function () {
                $(".modal-overlay").remove();
            });
        });

        $(window).resize(function () {
            $(".modal-box").css({
                top: ($(window).height() - $(".modal-box").outerHeight()) / 2,
                left: ($(window).width() - $(".modal-box").outerWidth()) / 2
            });
        });

        $(window).resize();

    });


    function validate_compose_mail() {
        var contact_id = $("#contact_id").val();
        var contact_r_id = $("#contact_r_id").val();
        var subject = $("#subject").val();
        var msg_body = $("#msg_body").val();

        if (contact_id == "" || contact_id == null) {
            $('#contact_id').val("");
            $('#contact_id').effect("highlight", {}, 5000);
            return false;
        }
        if (contact_r_id == "" || contact_r_id == null) {
            $('#contact_r_id').val("");
            $('#contact_r_id').effect("highlight", {}, 5000);
            return false;
        }
        if (subject == "" || subject == null) {
            $('#subject').val("");
            $('#subject').effect("highlight", {}, 5000);
            return false;
        }
        if (msg_body == "" || msg_body == null) {
            $('#msg_body').val("");
            $('#msg_body').effect("highlight", {}, 5000);
            return false;
        }
    }

    function reset_fileds() {
        $('#contact_r_id').val(0);
        $('#contact_r_email').val("");
        $('#contact_r_type').val("");
        $('#contact_id').val("");
    }

</script>
<style>
    .ui-widget-header {
        background-color: #54cbc8;
        background-image: none;
    }
</style>
<script type="text/javascript">

    $(document).ready(function () {


        var mailValue = $("#mailval").val();
        $("#demo-input-facebook-theme").tokenInput(JSON.parse(mailValue)
            , {
                preventDuplicates: true,
                theme: "facebook",
                onAdd: function (results) {
                    console.log('onadd:' + results);
                    arr = [];
                    arr1 = [];
                    arr2 = [];
                    if ($('#contact_r_id').val() == '' || $('#contact_r_id').val() == null || $('#contact_r_id').val() == 0) {
                        $('#contact_r_id').val(results['id']);
                    } else {
                        arr.push($('#contact_r_id').val(), results['id'])
                        $('#contact_r_id').val(arr);
                    }
                    if ($('#contact_r_email').val() == '' || $('#contact_r_email').val() == null) {
                        $('#contact_r_email').val(results['email']);
                    } else {
                        arr1.push($('#contact_r_email').val(), results['email'])
                        $('#contact_r_email').val(arr1);
                    }

                    $('#contact_r_type').val(results['type']);
                    if($('#contact_r_type').val()=='patient'){
                        var value= parseInt($('#contact_r_id').val());
                        mail_patient(value);

                    }

                    return results;
                },
                onDelete: function (item) {
                    if($('#contact_r_type').val()=='patient'){
                        $('#mail_doctor_search').show();
                        mail_patient('');
                    }
                    var ids = $('#contact_r_id').val().toString();
                    if (ids.indexOf(item.id) != 0) {
                        var tem = ids.replace(',' + item.id, '');
                    } else {
                        tem = ids.replace(item.id + ',', '');
                        tem = ids.replace(item.id, '');
                    }
                    $('#contact_r_id').val(tem);


                    var emails = $('#contact_r_email').val().toString();
                    console.log(emails);

                    if (emails.indexOf(item.email) != 0) {
                        var tem1 = emails.replace(',' + item.email, '');
                    } else {
                        tem1 = emails.replace(item.email + ',', '');
                        tem1 = emails.replace(item.email, '');
                    }
                    $('#contact_r_email').val(tem1);

                },

                resultsFormatter: function (item) {
                    if (item.type == 'doctor') {
                        item.vic_dob = '';
                    }
                    return "<li>" + item.name + " " + item.vic_dob + "</li>"
                }





            });


        $.each(JSON.parse(mailValue), function (key, value) {
            $("#demo-input-facebook-theme").tokenInput("add", JSON.parse(mailValue)[key]);

        });

    });


</script>