<?php

$this->breadcrumbs = array(
    'Messaging Center' => array('inbox'),
    'Sent Message',
);
if (isset(Yii::app()->session['logged_user_type']) && (Yii::app()->session['logged_user_type'] == "doctor")) {
    $user_type = 1;
    $owner_type = "d";
} elseif ((Yii::app()->session['logged_user_type'] == "patient")) {
    $user_type = 0;
    $owner_type = "p";
}
?>
<div class="main">
    <div id="breadcrumb" class="fk-lbreadbcrumb newvd">
        <!--<span><a href="">Home</a></span> >
        <span>Dashboard</span>-->
        <?php $this->widget('zii.widgets.CBreadcrumbs', array(
            'links' => $this->breadcrumbs,
        ));

        ?>
    </div>
    <div class="dashboard_mainarea">
        <div class="leftmenu">
            <?php $this->renderPartial('//layouts/navigation'); ?>
        </div>
        <div class="rightarea_dashboard">
            <div class="dashboardcont_leftbox">
                <h1>
                    <span>Sent Message</span>

                    <form id="msg_limit" name="msg_limit" method="POST"
                          action="<?php echo Yii::app()->request->baseUrl . '/message/sentMail'; ?>"
                          onchange="javascript: return page_limit_submit();" autocomplete="off">
	          <span class="select_mail">
	          	<select name="msg_limit_drp">
                    <option value="10" <?php if ($msg_limit_drp == "10") {
                        echo "selected";
                    } ?>>10
                    </option>
                    <option value="15" <?php if ($msg_limit_drp == "15") {
                        echo "selected";
                    } ?> >15
                    </option>
                    <option value="20" <?php if ($msg_limit_drp == "20") {
                        echo "selected";
                    } ?> >20
                    </option>
                </select>
	          </span>
                    </form>
                    <script>
                        function page_limit_submit() {
                            $("#msg_limit").submit();
                        }
                    </script>
                </h1>

                <div class="massege_title custom_message_title">
                    <span class="chkbox"><input type="checkbox" id="selectall"/></span>
                    <span class="subject">Subject</span>
                    <span class="mbody" style="width: 30%">Message body</span>
                    <span class="from">To</span>
                    <span class="from" style="width: 12%">Category</span>
                    <span class="date" style="width: 15%">Date</span>
                     <span style="font-size:16px;" class="delete fa fa-trash" id="delete" Title="Delete" ></span>
            <span style="float:right; margin-top: 2px; position: absolute;cursor: pointer;    color:#2c9492;
    padding-left: 8px;" class="archive fa fa-file-archive-o " Title="File Message"
                  id="archive"></span>
                </div>
            </div>

            <?php
            if (count($inbox_data) > 0) {

                foreach ($inbox_data as $key) {

                    ?>
                    <div class="massege_content">
                        <span class="chk"><input type="checkbox" class="inbox_row"
                                                 value="<?php echo $key['thread_id']; ?>" name="getR[]"/></span>
	                      	<span class="sender-subject">&nbsp;
	                      		<a href="<?php echo Yii::app()->request->baseUrl; ?>/Message/details/msgType/se/msg/<?php echo $key['thread_id']; ?>">
                                    <?php
                                    $content = trim($key['subject']);
                                    $content_summery = strip_tags($content);
                                    if (strlen($content_summery) > 10) {
                                        echo substr($content_summery, 0, 10) . "..";
                                    } else {
                                        echo substr($content_summery, 0, 10);
                                    }

                                    ?>
                                </a>
	                      	</span>
	                      	<span class="sender-sub" style="width: 29%">
	                      		<a href="<?php echo Yii::app()->request->baseUrl; ?>/Message/details/msgType/se/msg/<?php echo $key['thread_id']; ?>">
                                    <?php
                                    $content = trim($key['msg_body']);
                                    $content_summery = strip_tags($content);
                                    $exts = explode('Click', $content_summery);
                                    echo substr($exts[0], 0, 100) . " ...";
                                    ?>
                                </a>
								<?php if (isset($key['attachment_count']) && $key['attachment_count'] >0) {

                                    ?>

                                    <span class="sender-name1 fa fa-paperclip" style="font-size: 19px;"></span>
                                <?php } ?>
	                      	</span>
                        </a>
	                      <span class="sender-name">&nbsp;<?php
                              if(isset($key['to']) && $key['to']){
                              if (count($key['to']) > 1) {
                                  $content = trim($key['to'][0]['vic_name']);
                                  $content_summery = strip_tags($content);
                                  $exts = explode('Click', $content_summery);
                                  echo substr($exts[0], 0, 20) . " ...";
                              } else {
                                  if(isset($key['to'][0]['vic_name'])){
                                      echo $key['to'][0]['vic_name'];
                                  }
                              }
                              }
                              ?></span>
                    <?php $id = $key['department_id'];
                    $department = Department::model()->findByPk($id);
                      ?>
                     <span class="sender-date"style="width: 12%">&nbsp;<?php if(isset($department)){  echo $department->name; } ?></span>

                        <span class="sender-date">&nbsp;<?php echo date("m-d-Y  h:i A", $key['created']); ?></span>
                    </div>
                <?php } ?>
                <div class="message-pagein-section">
                    <?php
                    $this->widget('CLinkPager', array(
                        'pages' => $pages, 'header' => '', 'prevPageLabel' => '&lt;&lt;', 'nextPageLabel' => '&gt;&gt;',
                    ));
                    ?>
                </div>
            <?php
            } else {
                ?>
                <div class="massege_content">
                    No Message Found.
                </div>
            <?php } ?>
        </div>
    </div>
</div>
<script type="text/javascript">
    var vals = [];
    $(function () {
        $("#selectall").click(function () {
            $('.inbox_row').attr('checked', this.checked);
        });
        $(".inbox_row").click(function () {
            if ($(".inbox_row").length == $(".inbox_row:checked").length) {
                $("#selectall").attr("checked", "checked");
            } else {
                $("#selectall").removeAttr("checked");
            }
        });
    });

    $('#delete').click(function () {
        var list_r = "";
        $(".inbox_row").each(function (e) {
            if ($(this).is(':checked')) {
                if (list_r == "") {
                    list_r = $(this).attr('value');
                } else {
                    list_r = list_r + "|" + $(this).attr('value');
                }
            }
        });

        if (list_r == "") {
            alert("Please Mark Message first.");
            return false;
        } else {
           // if (confirm('Are you sure you want to delete this?')) {
                $.ajax({
                    url: '<?php echo Yii::app()->request->baseUrl; ?>/Message/deletemessage',
                    type: 'POST',
                    data: {keyword: list_r},
                    success: function (data) {
                        if (data == 1) {
                            location.reload();
                        }
                    }
                });
            //}

        }
    });
    $('#archive').click(function () {
        var list_r = "";
        $(".inbox_row").each(function (e) {
            if ($(this).is(':checked')) {
                if (list_r == "") {
                    list_r = $(this).attr('value');
                } else {
                    list_r = list_r + "|" + $(this).attr('value');
                }
            }
        });

        if (list_r == "") {
            alert("Please Mark Message first.");
            return false;
        } else {

           // if (confirm('Are you sure you want to Archieve this?')) {
                $.ajax({
                    url: '<?php echo Yii::app()->request->baseUrl; ?>/Message/archivemessage',
                    type: 'POST',
                    data: {keyword: list_r},
                    success: function (data) {

                        if (data == 1) {
                            location.reload();
                        }

                    }
                });
           // }
        }
    });
</script>