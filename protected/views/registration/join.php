<?php
/* @var $this RegistrationController */

$this->breadcrumbs = array(
    'Registration' => array('/registration'),
    'Join eDoctorBook',
);

?>
<div class="main">
    <div class="regis_contentarea">
        <p>
            <?php if (Yii::app()->user->hasFlash('join')): ?>
                <span class="flash-success">
                    <?php echo Yii::app()->user->getFlash('join'); ?>
                </span>
            <?php endif; ?>
            <a href="<?php echo $this->createAbsoluteUrl('site/index'); ?>" class="click">Not a Doctor? Click here.</a> 

        </p>   
        <div class="leftpanel">
            <h1>Join eDoctorBook!</h1>
            <?php
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'registration',
            ));
            ?>

            <span>
                <?php echo $form->textField($model, 'first_name', array('size' => 32, 'maxlength' => 32, 'placeholder' => 'First name')); ?>
            </span>
            <span>
                <?php echo $form->error($model, 'first_name'); ?>
            </span>
            <span>
                <?php echo $form->textField($model, 'last_name', array('size' => 32, 'maxlength' => 32, 'placeholder' => 'Last name')); ?>
            </span>
            <span>
                <?php echo $form->error($model, 'last_name'); ?>
            </span>
            <span>
                <?php echo $form->textField($model, 'email', array('size' => 32, 'maxlength' => 32, 'placeholder' => 'Email address', 'id' => 'email', 'onblur' => 'signupEmailValidation();')); ?>
            </span>
            <span>                  
                <?php echo $form->error($model, 'email'); ?>
            </span>
            <span>

                <?php echo $form->textField($model, 'phone', array('size' => 32, 'maxlength' => 32, 'placeholder' => 'Your phone #')); ?>
                <?php echo $form->error($model, 'phone'); ?>

            </span>
            <span></span>
            <span>
                <?php
                $selected = $model->speciality;
                echo CHtml::dropDownList('speciality', $selected, $user_speciality, array('empty' => 'Your specialty'));
                ?>
                <?php echo $form->error($model, 'speciality'); ?>
            </span>

            <span>

                <?php if (CCaptcha::checkRequirements() && !Yii::app()->session['doctor_join_level_1']): ?>
                    <BR>
                    <?php echo $form->labelEx($model, 'verifyCode'); ?>
                    <?php $this->widget('CCaptcha'); ?>
                    <?php echo $form->textField($model, 'verifyCode'); ?>
                    <div class="hint">
                        <br>
                        Please enter the letters as shown in the image above.
                        <br>
                        Letters are not case-sensitive.
                        <br>
                        <br>
                    </div>
                <?php endif; ?>
            </span>
            <span>

                <?php echo CHtml::submitButton($model->isNewRecord ? 'Join Now!' : 'Save', array('class' => 'registbt')); ?>
            </span>

            <?php $this->endWidget(); ?>
        </div>
        <div class="rightpanel">
            <br>
            <br>
            <a href="<?php echo $this->createAbsoluteUrl('/invitation'); ?>">
                <img src="<?php echo Yii::app()->request->baseUrl; ?>/assets/images/empower-your-patients.png" alt="Empower your patients with eDoctorBook" width="350">
            </a>
        </div>
    </div>
</div>
