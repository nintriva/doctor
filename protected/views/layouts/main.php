<?php /* @var $this Controller */ ?>
<?php
if (isset($_SESSION['url'])) {
    $_SESSION['url']['second'] = $_SESSION['url']['first'];
    $_SESSION['url']['first'] = $_SERVER['REQUEST_URI'];
} else {
    $_SESSION['url']['first'] = $_SERVER['REQUEST_URI'];
    $_SESSION['url']['second'] = '';
}
$url_redirect_d = '';
if ($_SESSION['url']['second']) {
    $url_redirect_d = 'http://' . $_SERVER['SERVER_NAME'] . $_SESSION['url']['second'];
}
$page_name_d = '';
$connection = Yii::app()->db;
$sqlCount = "select * FROM da_inbox  LEFT JOIN `da_message_inbox`  ON(da_inbox.inbox_id = da_message_inbox.`message_id`) WHERE status=1  AND da_message_inbox.user_id='" . Yii::app()->session['logged_user_id'] . "' AND da_message_inbox.message_type=1 AND `message_read_flag`=0";
$commandCount = $connection->createCommand($sqlCount);
$inbox_arr_count = $commandCount->queryAll();
$count = count($inbox_arr_count);
Yii::app()->session['inbox_count'] = $count;

?>
<!DOCTYPE HTML>
<!--[if lt IE 7 ]>
<html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]>
<html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]>
<html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="eng">
<!--<![endif]-->
<head>
<meta charset="utf-8">
<title><?php echo CHtml::encode(Yii::app()->name); ?></title>
<meta name="description"
      content="Find the best Doctors and book appointments online instantly! Read reviews and ratings by real patients.">
<meta name="keywords"
      content="Dental, Dentist, Dentists,Doctors,Teeth Pain,appointments, Teeth Whitening, Emergency, Metlife, Cigna, Delta Dental, Aetna, VSP, cancer,diabetes,depression,asthma,cholesterol,herpes,bipolar,diet,weight loss">


<link rel="shortcut icon" href="<?php echo Yii::app()->request->baseUrl; ?>/assets/images/eDoctorBookfav.ico"
      type="image/x-icon"/>
<link href="<?php echo Yii::app()->request->baseUrl; ?>/assets/css/style.css?r=<?php echo time(); ?>" rel="stylesheet"
      type="text/css">
<link href="<?php echo Yii::app()->request->baseUrl; ?>/assets/css/pager.css?r=<?php echo time(); ?>" rel="stylesheet"
      type="text/css">
<link href="<?php echo Yii::app()->request->baseUrl; ?>/assets/css/responsive.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/assets/css/flexslider.css" type="text/css"
      media="screen"/>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<link href="<?php echo Yii::app()->request->baseUrl; ?>/assets/css/royalslider.css" rel="stylesheet"/>
<link href="<?php echo Yii::app()->request->baseUrl; ?>/assets/css/custom.css" rel="stylesheet" type="text/css">
<!--<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/assets/css/default.css" type="text/css" media="screen" />-->
<!--[if IE 8]>
<style>
    span.loginpanel a {
        behavior: url(pie/PIE.htc);
    }

    nav span {
        behavior: url(pie/PIE.htc);
    }

    .searchbg {
        behavior: url(pie/PIE.htc);
    }

    .searchbg form input {
        behavior: url(pie/PIE.htc);
    }

    .sbHolder {
        behavior: url(pie/PIE.htc);
    }

    .appoinmrnt_box h1 span {
        behavior: url(pie/PIE.htc);
    }

    .appoinmrnt_box_last h1 span {
        behavior: url(pie/PIE.htc);
    }

    #signin_menu input[type=text], #signin_menu input[type=password] {
        behavior: url(pie/PIE.htc);
        padding: 5px;
        width: 180px;
    }

    #signin_submit {
        behavior: url(pie/PIE.htc);
    }

    #signin_menu {
        right: -10px;
        width: 190px;
        padding: 19px 10px 10px 10px;
    }


</style>
<![endif]-->

<!--[if IE 7]>
<style>
    span.loginpanel a {
        behavior: url(pie/PIE.htc);
    }

    nav span {
        behavior: url(pie/PIE.htc);
    }

    .searchbg {
        behavior: url(pie/PIE.htc);
    }

    .searchbg form input {
        behavior: url(pie/PIE.htc);
    }

    .sbHolder {
        behavior: url(pie/PIE.htc);
    }

    .appoinmrnt_box h1 span {
        behavior: url(pie/PIE.htc);
    }

    .appoinmrnt_box_last h1 span {
        behavior: url(pie/PIE.htc);
    }

    #signin_menu input[type=text], #signin_menu input[type=password] {
        behavior: url(pie/PIE.htc);
        padding: 5px;
        width: 180px;
    }

    #signin_submit {
        behavior: url(pie/PIE.htc);
    }

    #signin_menu {
        right: -10px;
        width: 190px;
        padding: 19px 10px 10px 10px;
    }
</style>
<![endif]-->
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>

<![endif]-->
<?php /* ?><script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/assets/js/jquery-1.7.2.min.js"></script><?php */ ?>
<link href="<?php echo Yii::app()->request->baseUrl; ?>/assets/css/jquery-ui.css" rel="stylesheet" type="text/css">
<!--        <link href="-->
<?php //echo Yii::app()->request->baseUrl; ?><!--/assets/css/token-input.css" rel="stylesheet" type="text/css">-->
<link href="<?php echo Yii::app()->request->baseUrl; ?>/assets/css/token-input-facebook.css" rel="stylesheet"
      type="text/css">
<?php
$CS = Yii::app()->clientScript;
$CS->registerScript('path', '
     var basePath="' . Yii::app()->request->baseUrl . '";
    ', CClientScript::POS_HEAD);
?>
<?php
Yii::app()->clientScript->scriptMap['jquery.js'] = false;

$CS->registerScriptFile(Yii::app()->baseUrl . '/assets/js/jquery-1.10.2.js');
$CS->registerScriptFile(Yii::app()->baseUrl . '/assets/js/jquery-migrate-1.0.0.js');
$CS->registerScriptFile(Yii::app()->baseUrl . '/assets/js/jquery-ui.js');
$CS->registerScriptFile(Yii::app()->baseUrl . '/assets/js/jquery.multifile.js'); ?>


<?php /* ?>
          <link href="<?php echo Yii::app()->request->baseUrl; ?>/assets/css/jquery-ui.css" rel="stylesheet" />
          <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/assets/js/jquery-1.9.1.js"></script>
          <script src="http://code.jquery.com/jquery-migrate-1.0.0.js"></script>
          <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/assets/js/jquery-ui.js"></script><?php */
?>
<!--<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/assets/js/jquery.nivo.slider.js"></script>
<script type="text/javascript">
  $(window).load(function() {
       // $('#slider').nivoSlider();
  });
</script>-->
<!-- <script type="text/JavaScript" src="<?php echo Yii::app()->request->baseUrl; ?>/assets/js/jquery.bpopup.js"></script> -->
<script type="text/JavaScript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/assets/js/jquery.bpopup.min.js"></script>

<script type="text/JavaScript" src="<?php echo Yii::app()->request->baseUrl; ?>/assets/js/geo.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/assets/js/jquery.easing-1.3.js"></script>
<script type="text/javascript" language="javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/assets/js/jscriptnew.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/assets/js/jquery.royalslider.min.js"></script>
<script type="text/javascript" language="javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/assets/js/jscript_omlp_widget_476.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/assets/js/jquery.selectbox-0.2.js"></script>

<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/assets/js/geoip2.js"></script>
<script src='https://www.google.com/recaptcha/api.js'></script>

<script type="text/javascript">
    $(function () {
        $("#location_id").selectbox();
    });
    $(document).ready(function () {
        $.ajaxSetup({cache: true});
    });
</script>
<script type="text/javascript">
    $(document).ready(function () {
        function add() {
            if ($(this).val() === '') {
                $(this).val($(this).attr('placeholder')).addClass('placeholder');
            }
        }

        function remove() {
            if ($(this).val() === $(this).attr('placeholder')) {
                $(this).val('').removeClass('placeholder');
            }
        }

        // Create a dummy element for feature detection
        if (!('placeholder' in $('<input>')[0])) {

            // Select the elements that have a placeholder attribute
            $('input[placeholder], textarea[placeholder]').blur(add).focus(remove).each(add);

            // Remove the placeholder text before the form is submitted
            $('form').submit(function () {
                $(this).find('input[placeholder], textarea[placeholder]').each(remove);
            });
        }
    });
</script>


<script type="text/javascript">
    jQuery(document).ready(function ($) {
        $('html').click(function () {
            $('#signin_menu').hide();
            $('#invitation_box').hide();
            $(".signin").removeClass('active');
            $(".invitation").removeClass('active');
        });


        $('.signin, #signin_menu').bind('click', function (event) {
            event.stopPropagation();
        });
        $('.invitation, #invitation_box').bind('click', function (event) {
            event.stopPropagation();
        });

        $(".signin").click(function (e) {
            e.preventDefault();
            $("fieldset#signin_menu").toggle();
            $(".signin").toggleClass("menu-open");
            $('#invitation_box').hide();
            $(".signin").toggleClass('active');
            $("#username").focus();
        });

        $(".invitation").click(function (e) {
            e.preventDefault();
            $("fieldset#invitation_box").toggle();
            $(".invitation").toggleClass("menu-open");
            $('#signin_menu').hide();
            $(".invitation").toggleClass('active');
            $("#invitationcode").focus();
        });


    });
</script>
<script>
    $(function () {
        $(".datepicker").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'yy-mm-dd'
        });
    });
</script>
<script type="text/javascript">
    function equalHeights(element1, element2) {
        var height;
        if (element1.outerHeight() > element2.outerHeight()) {
            height = element1.outerHeight();
            element2.css('height', height);
        }
        else {
            height = element2.outerHeight();
            element1.css('height', height);
        }
    }


</script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#nav li').hover(function () {
            $('ul', this).slideDown(200);
            $(this).children('a:first').addClass("hov");
        }, function () {
            $('ul', this).slideUp(100);
            $(this).children('a:first').removeClass("hov");
        });
    });
</script>
<?php if (Yii::app()->controller->action->id != 'appointment') { ?>
    <script type="text/javascript">
        $(function () {
            // equalHeights ($(".leftmenu"), $('.rightarea_dashboard'));
        });
    </script>
<?php } ?>
<?php if (!isset($_SESSION['timeZoneId'])) { ?>
    <script>
        /*var time_zone_lat = geoip_latitude();
         var time_zone_lon = geoip_longitude();*/
        var time_zone_lat = "0";
        var time_zone_lon = "0";

        //GEOajax("<?php echo Yii::app()->request->baseUrl; ?>/site/ajaxTimeZone?accuracy=&latlng=" + time_zone_lat + "," + time_zone_lon+"&altitude=&altitude_accuracy=&heading=&location_loc_add=&speed=");
      $(function () {
            $.ajax({
                type: "POST",
                url: "<?php echo Yii::app()->request->baseUrl; ?>/site/ajaxTimeZone",
                data: {latlng: time_zone_lat + "," + time_zone_lon},
                async: false,
                success: function (result) {
                }
            });
        });
    </script>
<?php } ?>

</head>

<body>
<?php //include_once("analyticstracking.php") ?>
<!--heAder part start-->
<input type="hidden" id="base_url" name="base_url" value="<?php echo Yii::app()->request->baseUrl; ?>">
<header>
    <div class="topbar">
        <div class="topbar_container">
            <div class="logo"><a href="<?php echo $this->createAbsoluteUrl('site/index'); ?>"><img
                        src="<?php echo Yii::app()->request->baseUrl; ?>/assets/images/logo.png" alt="eDoctorBook"></a>
            </div>
            <?php
            if (Yii::app()->session['logged_in']) {
                $class = "margin-top: 4px;";
            } else {
                $class = "margin-top: 0;";
            }
            $connection = Yii::app()->db;
            $loggedUserSql = "select user_first_name FROM da_patient WHERE status=1 AND `id`='" . Yii::app()->session['logged_user_id'] . "'";
            $loggedUserCount = $connection->createCommand($loggedUserSql);
            $loggedUserRes = $loggedUserCount->queryAll();
            ?>

            <div class="loginsect" style="<?= $class ?>">

                <?php if (Yii::app()->session['logged_in']) { ?>

                    <?php
                    $connection = Yii::app()->db;
                    $sqlCount = "select doctor_speciality,speciality FROM da_favourite_doctor,da_speciality WHERE da_speciality.id = da_favourite_doctor.doctor_speciality AND da_favourite_doctor.status = '1' AND patient_id='" . Yii::app()->session['logged_user_id'] . "' group by doctor_speciality";
                    $commandCount = $connection->createCommand($sqlCount);
                    $favArr = $commandCount->queryAll();


                    if (Yii::app()->session['logged_user_type'] == 'patient') {
                        if (count($favArr) > 0) {
                            ?>
                            <span class="favorites">

                                        <img src="<?php echo Yii::app()->request->baseUrl; ?>/assets/images/fav_red.png"
                                             alt="Inbox"/>

                                        <a href="#">My Doctors</a>

                                        <div class="subnav_area">
                                            <span class="top_arrow">&nbsp;</span>
                                            <ul class="submenu">
                                                <?php
                                                foreach ($favArr as $favspclity) {
                                                    $sqlCount2 = "select doctor_id,slug,first_name,last_name FROM da_favourite_doctor,da_doctor WHERE da_doctor.id = da_favourite_doctor.doctor_id AND da_doctor.status = '1' AND da_favourite_doctor.doctor_speciality = '" . $favspclity['doctor_speciality'] . "' AND patient_id='" . Yii::app()->session['logged_user_id'] . "'";
                                                    $commandCount2 = $connection->createCommand($sqlCount2);
                                                    $favArr2 = $commandCount2->queryAll();
                                                    //echo "<pre>";
                                                    //print_r($favArr2);
                                                    ?>
                                                    <li><a href="#"><?php echo $favspclity['speciality'] ?></a>
                                                        <ul class="submenu1">
                                                            <?php foreach ($favArr2 as $favDoc) { ?>
                                                                <li>
                                                                    <a href="<?php echo $this->createAbsoluteUrl('doctor/' . $favDoc['slug'] . '|' . $favspclity['doctor_speciality']); ?>">Dr. <?php echo ucfirst($favDoc['first_name'] . " " . $favDoc['last_name']); ?></a>
                                                                </li>
                                                            <?php } ?>
                                                        </ul>
                                                    </li>
                                                <?php } ?>

                                            </ul>
                                        </div>

                                    </span>
                        <?php } else { ?>
                            <span class="favorites">
                                        <img
                                            src="<?php echo Yii::app()->request->baseUrl; ?>/assets/images/fav_icon.png"
                                            alt="Inbox"/>
                                        <a href="#">My Doctors</a>
                                    </span>
                        <?php } ?>

                    <?php } ?>

                    <?php if (Yii::app()->session['logged_user_type'] == 'parcticeAdmin') { ?>
                        <span class="username"
                              style="font-size: 13px;color:#FAFAFA;font-weight: bold;">Welcome <?php echo Ucfirst(Yii::app()->session['first_name'] . " " . Yii::app()->session['middle_name'] . " " . Yii::app()->session['last_name']); ?></span>
                        <span> &nbsp</span>
                        <span class="username"><img style="height: 15px;width: 15px;" src="<?php echo Yii::app()->request->baseUrl; ?>/images/file98.png" >
                            <?php echo CHtml::link('Document Vault', $this->createAbsoluteUrl('doctor/document/'.Yii::app()->session['logged_user_id'])); ?> </span>
                    <?php } else { ?>
                        <?php if (!isset(Yii::app()->session['logged_with_claim'])) { ?>
                            <span class="username"><img style="height: 15px;width: 15px;" src="<?php echo Yii::app()->request->baseUrl; ?>/images/file98.png" >
                                <?php echo CHtml::link('Document Vault', $this->createAbsoluteUrl(''.Yii::app()->session['logged_user_type'].'/document/'.Yii::app()->session['logged_user_id'])); ?> </span>
                            <span class="username"><img src="<?php echo Yii::app()->request->baseUrl; ?>/assets/images/mail_box_icon.png"
                                    alt="Inbox"><a href="<?php echo $this->createAbsoluteUrl('message/inbox/' . Yii::app()->session['logged_user_id']); ?>">
                                    Inbox (
                                    <strong><?php echo isset(Yii::app()->session['inbox_count']) ? Yii::app()->session['inbox_count'] : 0; ?></strong>
                                    ) </a></span>
                        <?php } ?>
                    <?php } ?>
                    <?php if (Yii::app()->session['logged_user_type'] == 'doctor') { ?>
                        <?php if (!isset(Yii::app()->session['logged_with_claim'])) { ?>
                            <span class="username"><img
                                    src="<?php echo Yii::app()->request->baseUrl; ?>/assets/images/user.png"
                                    alt="My Dashboard"><a
                                    href="<?php echo $this->createAbsoluteUrl('doctor/index'); ?>"><?php //echo Yii::app()->session['logged_user_email'];               ?>
                                    My Dashboard </a></span>

                        <?php
                        } else {
                            ?>
                            <span class="username"><img
                                    src="<?php echo Yii::app()->request->baseUrl; ?>/assets/images/user.png"
                                    alt="My Dashboard"><a
                                    href="<?php echo $this->createAbsoluteUrl('doctor/editProfile/' . Yii::app()->session['logged_user_email']); ?>"><?php //echo Yii::app()->session['logged_user_email'];               ?>
                                    Profile Setup</a></span>
                        <?php
                        }
                    } elseif (Yii::app()->session['logged_user_type'] == 'parcticeAdmin') {

                    } elseif (Yii::app()->session['logged_user_type'] == 'patient') {
                        ?>
                        <span class="username"><img
                                src="<?php echo Yii::app()->request->baseUrl; ?>/assets/images/user.png"
                                alt="My Account"><a href="<?php echo $this->createAbsoluteUrl('patient/index'); ?>">Dashboard</a></span>
                    <?php
                    }
                }
                ?>



                <?php $page_name_d = Yii::app()->controller->action->id; ?>

                <?php if (Yii::app()->session['logged_in'] != 1) { ?>
                    <!-- <span class="invitationcodepanel">
                         <a class="invitation" href="javascript:void(0);">Got invitation?</a>
                     </span> -->
                <?php } ?>



                <span class="loginpanel">

                            <?php
                            if (Yii::app()->session['logged_in']) { ?>
                                <a class="signout" href="javascript:void(0);" onClick="signOut();">Logout</a>
                            <?php } else { ?>
                                <?php
                                /* if ($page_name_d != "login") {
                                     ?>
                                     <a class="signin" href="javascript:void(0);">Login</a>
                                 <?php } */
                                ?>
                                <?php /* ------------- for parctice Admin Login Redirect-------- IB */ ?>
                                <?php

                                if ($page_name_d == "login" || $page_name_d == "LoginDpa" ||$page_name_d == "LoginDpa")
                                {
                                    /* ----- do nothing --------*/
                                }
                                else {
                                    ?>
                                    <a class="signin" href="javascript:void(0);">Login</a>
                                <?php } ?>
                                <?php /* ------------- X-------- IB */ ?>


                            <?php } ?>
                        </span>
            </div>
            <?php
            //if($page_name_d!="login") {

            if ($page_name_d == "login" || $page_name_d == "LoginDpa" ||$page_name_d =='loginDpa') {
                /* ----- do nothing --------*/
            } else {
                ?>

                <fieldset id="invitation_box">
                    <?php
                    $form = $this->beginWidget('CActiveForm', array('id' => 'invitation-form', 'enableAjaxValidation' => false, 'htmlOptions' => array('onsubmit' => "return false;", /* Disable normal form submit */
                        'onkeypress' => " if(event.keyCode == 13){ return invitation_validate(); } " /* Do ajax call when user presses enter key */),));
                    ?>               <span id="invitation-code-panel"
                                           style="margin-top: -93px; float:right; width:95%; display:none; color:#ff0000; font-weight:bold; padding:3px; padding-top:20px;">Invalid invitation code</span>

                    <!--   <input id="invitationcode" name="invitationcode" value="" title="Invitation Code" placeholder="Invitation Code" tabindex="1" type="text" autocomplete="off">  -->
                            <span>

                                <!-- <a tabindex="4" id="signin_submit" href="javascript:void(0);" onClick="return invitation_validate();">Validate</a> -->
                                <span id="signin_process" style="margin-top: -33px; margin-left:10px;"></span>

                            </span>
                    <?php $this->endWidget(); ?>
                </fieldset>


                <fieldset id="signin_menu">
                    <?php
                    $form = $this->beginWidget('CActiveForm', array('id' => 'person-form-edit_person-form', 'enableAjaxValidation' => false, 'htmlOptions' => array('onsubmit' => "return false;", /* Disable normal form submit */
                        'onkeypress' => " if(event.keyCode == 13){ return signIn(); } " /* Do ajax call when user presses enter key */),));
                    ?>
                    <span id="signin_wrong"  style="margin-op: -33px; float:left; width:95%; display:none; color:#ff0000; font-weight:bold; padding:3px; padding-top:20px;  ">Wrong User ID and/or Password</span>
                    <span style="padding-bottom:10px; font-size:12px; font-weight:bold;"><label>Login credentials:</label></span>
                    <input id="username" name="username" value="" title="username" placeholder="User ID" tabindex="2"  type="text" autocomplete="off">
                    <input id="password" name="password" value="" title="password" placeholder="Password" tabindex="3" type="password" autocomplete="off">
                            <span>
                             <a tabindex="4" id="signin_submit" href="javascript:void(0);" onClick="return signIn();">Go</a><!--<input id="signin_submit" value="Go" tabindex="3" type="submit">-->
                             <a id="forgot_pass" href="<?php echo $this->createAbsoluteUrl('doctor/forgetpassword'); ?>">Forgot password?</a>
                             <span id="signin_process" style="margin-top: -33px; margin-left:10px;"></span>
                             <!--  <a id="fb-root" href="javascript:void(0);" tabindex="5" style="display:none;" onClick="fb_login();">Login with Facebook</a>-->
                            </span>
                    <?php $this->endWidget(); ?>
                </fieldset>
            <?php } ?>
        </div>
    </div>


</header>
<!--body part start-->
<section>
    <?php echo $content; ?>
</section>

<!--footer part start-->

<footer>
    <?php
    $controller_name_footer = Yii::app()->controller->id;
    $action_id_footer = Yii::app()->controller->action->id;
    if (($controller_name_footer == 'site' && $action_id_footer == 'index') || ($controller_name_footer == 'doctor' && $action_id_footer == 'doctorSearch')) {
        ?>
        <div class="main">
            <div class="footersection">
                <div class="footer_box1">
                    <h1>Insurance </h1>
                    <ul>
                        <li>
                            <a href="<?php echo $this->createAbsoluteUrl('doctor/doctorSearch?insurance=1'); ?>">Aetna </a>
                        </li>
                        <li>
                            <a href="<?php echo $this->createAbsoluteUrl('doctor/doctorSearch?insurance=6'); ?>">Cigna </a>
                        </li>
                        <li><a href="<?php echo $this->createAbsoluteUrl('doctor/doctorSearch?insurance=3'); ?>">Blue
                                Cross Blue Shield </a></li>
                        <li><a href="<?php echo $this->createAbsoluteUrl('doctor/doctorSearch?insurance=9'); ?>">Delta
                                Dental</a></li>
                        <li>
                            <a href="<?php echo $this->createAbsoluteUrl('doctor/doctorSearch?insurance=5'); ?>">Humana </a>
                        </li>
                        <li><a href="<?php echo $this->createAbsoluteUrl('doctor/doctorSearch?insurance=7'); ?>">Health
                                Net </a></li>
                        <li><a href="<?php echo $this->createAbsoluteUrl('doctor/doctorSearch?insurance=2'); ?>">United
                                Health Care </a></li>
                        <li><a href="<?php echo $this->createAbsoluteUrl('doctor/doctorSearch?insurance=8'); ?>">VSP</a>
                        </li>
                    </ul>
                    <?php /* ?><ul>
                              <li><?php echo CHtml::link('Doctor Name', $this->createAbsoluteUrl('doctor/doctorSearchByName/')); ?></li>
                              <li><a href="javascript:void(0);">Practice Name</a></li>
                              <li><?php echo CHtml::link('Procedure', $this->createAbsoluteUrl('doctor/doctorSearchByProcedure/')); ?></li>
                              <li><?php echo CHtml::link('Language', $this->createAbsoluteUrl('doctor/doctorSearchByLanguage/')); ?></li>
                              <li><a href="javascript:void(0);">Location</a></li>
                              <li><?php echo CHtml::link('Hospital', $this->createAbsoluteUrl('doctor/doctorSearchByHospital/')); ?></li>
                              <li><?php echo CHtml::link('Insurance', $this->createAbsoluteUrl('doctor/doctorSearchByInsurance/')); ?></li>
                              </ul><?php */
                    ?>
                </div>
                <div class="footer_box1">
                    <h1>Cities</h1>
                    <ul>
                        <li>
                            <a href="<?php echo $this->createAbsoluteUrl('doctor/doctorSearch?geo_location_id=New York, NY'); ?>">New
                                York</a></li>
                        <li>
                            <a href="<?php echo $this->createAbsoluteUrl('doctor/doctorSearch?geo_location_id=Los Angeles, CA'); ?>">Los
                                Angeles</a></li>
                        <li>
                            <a href="<?php echo $this->createAbsoluteUrl('doctor/doctorSearch?geo_location_id=Chicago, IL'); ?>">Chicago</a>
                        </li>
                        <li>
                            <a href="<?php echo $this->createAbsoluteUrl('doctor/doctorSearch?geo_location_id=Houston, TX'); ?>">Houston</a>
                        </li>
                        <li>
                            <a href="<?php echo $this->createAbsoluteUrl('doctor/doctorSearch?geo_location_id=Philadelphia'); ?>">Philadelphia</a>
                        </li>
                        <li>
                            <a href="<?php echo $this->createAbsoluteUrl('doctor/doctorSearch?geo_location_id=San Jose, CA'); ?>">San
                                Jose</a></li>
                        <li>
                            <a href="<?php echo $this->createAbsoluteUrl('doctor/doctorSearch?geo_location_id=Miami, FL'); ?>">Miami</a>
                        </li>
                        <li>
                            <a href="<?php echo $this->createAbsoluteUrl('doctor/doctorSearch?geo_location_id=Phoenix, AZ'); ?>">Phoenix</a>
                        </li>
                    </ul>
                </div>
                <div class="footer_box1">
                    <h1>Specialties</h1>
                    <ul>
                        <li><a href="<?php echo $this->createAbsoluteUrl('doctor/doctorSearch?speciality_id=1'); ?>">Allergy
                                & Immunology</a></li>
                        <li><a href="<?php echo $this->createAbsoluteUrl('doctor/doctorSearch?speciality_id=3'); ?>">Cardiology</a>
                        </li>
                        <li><a href="<?php echo $this->createAbsoluteUrl('doctor/doctorSearch?speciality_id=33'); ?>">Dentist</a>
                        </li>
                        <li><a href="<?php echo $this->createAbsoluteUrl('doctor/doctorSearch?speciality_id=4'); ?>">
                                Dermatology</a></li>
                        <li><a href="<?php echo $this->createAbsoluteUrl('doctor/doctorSearch?speciality_id=12'); ?>">Internal
                                Medicine</a></li>
                        <li><a href="<?php echo $this->createAbsoluteUrl('doctor/doctorSearch?speciality_id=19'); ?>">Ophthalmology</a>
                        </li>
                        <li><a href="<?php echo $this->createAbsoluteUrl('doctor/doctorSearch?speciality_id=20'); ?>">Orthopedics</a>
                        </li>
                        <li><a href="<?php echo $this->createAbsoluteUrl('doctor/doctorSearch?speciality_id=24'); ?>">Pediatrics</a>
                        </li>
                    </ul>
                </div>

                <div class="footer_box1">

                    <a href="<?php echo $this->createAbsoluteUrl('/invitation'); ?>">
                        <img src="<?php echo Yii::app()->request->baseUrl; ?>/assets/images/doc-invitation.png"
                             alt="Support" width="275">
                    </a>

                    </h2>

                </div>

            </div>
        </div>


    <?php } ?>
    <div class="footer_nav">
        <div class="main">
            <ul>
                <li><a href="<?php echo $this->createAbsoluteUrl('doctor/about'); ?>">About</a></li>
                <li><a href="<?php echo $this->createAbsoluteUrl('doctor/careers'); ?>">Jobs</a></li>
                <li><a href="<?php echo $this->createAbsoluteUrl('doctor/contact'); ?>">Contact Us</a></li>
                <li><a href="<?php echo $this->createAbsoluteUrl('doctor/faq'); ?>">FAQ</a></li>
                <li><a href="<?php echo $this->createAbsoluteUrl('doctor/termsCondition'); ?>">Terms of Use</a></li>
                <li><a href="<?php echo $this->createAbsoluteUrl('doctor/privacy'); ?>">Privacy Policy</a></li>
                <li><a href="<?php echo $this->createAbsoluteUrl('doctor/ContactDoctor'); ?>">Invite a Doctor</a></li>
                <?php
                if (Yii::app()->session['logged_in'] != 1) {
                    ?>
                    <li><a href="<?php echo Yii::app()->baseUrl . "/claimprofile"; ?>">
                        Doctors: Claim your profile</a></li><?php
                }
                ?>
                <?php
                if (Yii::app()->session['logged_in'] != 1) {
                    ?>
                    <li><a href="<?php echo Yii::app()->baseUrl . "/registration/join"; ?>">
                        Doctors: Join us</a></li><?php
                }
                ?>

            </ul>
            <span class="copyright">eDoctorBook © <?php echo date('Y'); ?></span>
        </div>
    </div>
    <div class="footer_social">
        <div class="main">

            <ul>
                <li class="facebook"><a href="https://www.facebook.com/edoctorbook" onclick="window.open(this.href, 'eDoctorBook',
                                        'left=20,top=20,width=500,height=500,toolbar=1,resizable=0');
                                return false;"></a></li>
                <li class="twitter"><a
                        href="http://www.twitter.com/share?text=eDoctorBook&url=&hashtags=eDoctorBook,Appointment,twitt"
                        onclick="window.open(this.href, 'eDoctorBookTwitter',
                                        'left=20,top=20,width=500,height=500,toolbar=1,resizable=0');
                                return false;"></a></li>
                <li class="gplus"><a
                        href="https://plus.google.com/share?url=http%3A%2F%2Farbcloudsoft.com%2Fdev%2Fdoctorappointment"
                        onclick="window.open(this.href, 'eDoctorBookGooglePlus',
                                        'left=20,top=20,width=500,height=500,toolbar=1,resizable=0');
                                return false;"></a></li>
            </ul>
        </div>
    </div>

</footer>
<script defer src="<?php echo Yii::app()->request->baseUrl; ?>/assets/js/jquery.flexslider.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/assets/js/modernizr.js"></script>
<script type="text/javascript">

    /* $(function(){
     SyntaxHighlighter.all();
     });*/
    $(window).load(function () {
        $('.flexslider').flexslider({
            animation: "fade",
            start: function (slider) {
                // $('body').removeClass('loading');
            }
        });
        $("input[name='account_type']").click(function () {
            if ($(this).val() == 'patient')
                $('#fb-root').show();
            else
                $('#fb-root').hide();
        });
    });
</script>
<script>
    function invitation_validate() {
        if ($.trim($('#txtInput').val()) == '') {
            return false;
        }
    }
    // chechk the captcha code......



    //Generates the captcha function


    // Validate the Entered input aganist the generated security code function
    /*function ValidCaptcha(){
        var str1 = removeSpaces(document.getElementById('txtCaptcha').value);
        var str2 = removeSpaces(document.getElementById('txtInput').value);
        if (str1 == str2){
            return true;
        }else{
            return false;
        }
    }*/

    // Remove the spaces from the entered and generated code
    /*
    function removeSpaces(string){
        return string.split(' ').join('');
    }*/

    function signIn() {
     /*   var why = "";
        if($.trim($('#txtInput').val()) == ""){
            why += "- Security code should not be empty.\n";
        }
        if($.trim($('#txtInput').val())!= ""){

            if(ValidCaptcha($.trim($('#txtInput').val())) == false){
                why += "- Security code did not match.\n";
            }
        }
        if(why != ""){
            alert(why);
            return false;
        }*/
        <?php if(Yii::app()->controller->id == "site" && Yii::app()->controller->action->id=="login"){ ?>
        var v = grecaptcha.getResponse();
        if(v.length == 0)
        {
            document.getElementById('captcha').innerHTML="You can't leave Captcha Code empty";
            return false;
        }
        if(v.length != 0)
        {
            document.getElementById('captcha').innerHTML="Captcha completed";

        <?php } ?>

        if ($.trim($('#username').val()) == '' || $.trim($('#password').val()) == '') {
            return false;
        } else {
            $('#signin_process').html('<img src="<?php echo Yii::app()->request->baseUrl; ?>/assets/images/process.gif">');
            var jqXHR = $.ajax({
                type: "POST",
                url: "<?php echo Yii::app()->request->baseUrl; ?>/registration/signin",
                data: {email: $.trim($('#username').val()), password: $.trim($('#password').val())},
                async: false,
                success: function (result) {
                    console.log(result);
                }
            });console.log(jqXHR.responseText);
            if (jqXHR.responseText == 1) {



                //
                <?php if (Yii::app()->session['logged_user_type'] == 'doctor') { ?>

                window.location.href = "<?php echo $this->createAbsoluteUrl('doctor/index'); ?>";
                <?php } ?>
                <?php if (Yii::app()->session['logged_user_type'] == 'patient') { ?>

                window.location.href = "<?php echo $this->createAbsoluteUrl('patient/index'); ?>";
                <?php } ?>

                window.location.href = "<?php echo $this->createAbsoluteUrl('/index.php');?>";

                //location.reload();
                return true;
            }
            if(jqXHR.responseText == 2){

                window.location.href = "<?php echo $this->createAbsoluteUrl('site/login'); ?>";
             }

            else {
                $('#signin_process').html('');
                $('#signin_wrong').show();
                $("#signin_submit").focus();
                $('#signin_menu input').keyup(function () {
                    $('#signin_wrong').hide();
                });
                return false;
            }
        }
        <?php if(Yii::app()->controller->id == "site" && Yii::app()->controller->action->id=="login"){?>
         }<?php } ?>
    }
    function signOut() {
        $.ajax({
            type: "POST",
            url: "<?php echo Yii::app()->request->baseUrl; ?>/registration/signout",
            data: {},
            success: function (result) {
                window.location.href = "<?php echo Yii::app()->request->baseUrl; ?>";
            }
        });
    }
    function signupEmailValidation() {

        var emailval = $('#email').val();

        var user_emailVal = $('#user_email').val();
        if (emailval) {
            url = '<?php echo Yii::app()->request->baseUrl; ?>/registration/JoinValidEmail';
            $.post(url, {Email: emailval})
                .done(function (data) {
                    if (data == 'ok') {

                    } else {
                        $('#email').css("border", "1px #FF0000 solid");
                        $("#email").focus();
                        $("#email").val('');
                        $('#email').attr("placeholder", "Email already exists");
                        $("#email").change(function () {
                            $('#email').css("border", "1px #ccc solid");
                        });
                    }
                });
        }
        if (user_emailVal) {
            url = '<?php echo Yii::app()->request->baseUrl; ?>/registration/JoinValidEmail';
            $.post(url, {Email: user_emailVal})
                .done(function (data) {
                    if (data == 'ok') {

                    } else {
                        $('#user_email').css("border", "1px #FF0000 solid");
                        $("#user_email").focus();
                        $("#user_email").val('');
                        $('#user_email').attr("placeholder", "Email already exists");
                        $("#user_email").change(function () {
                            $('#user_email').css("border", "1px #ccc solid");
                        });
                    }
                });
        }
    }

    /* ------------- for parctice Admin Sign function -------- IB*/

    function signInDpa() {
        if ($.trim($('#username').val()) == '' || $.trim($('#password').val()) == '') {
            return false;
        } else {
            //$('#signin_process').html('<img src=" <?php echo Yii::app()->request->baseUrl; ?>/assets/images/process.gif">');
            var jqXHR = $.ajax({
                type: "POST",
                url: "<?php echo Yii::app()->request->baseUrl; ?>/PaAdminDoctors/signIn",
                data: { email: $.trim($('#username').val()), password: $.trim($('#password').val()), account_type: $("input[name='account_type']:checked").val() },
                async: false,
                success: function (result) {
                }
            });

            if (jqXHR.responseText == 1) {
                <?php if(Yii::app()->session['logged_user_type']=='parcticeAdmin'){ ?>
                window.location.href = "<?php echo $this->createAbsoluteUrl('doctorPracticeAdmin/index'); ?>";
                <?php } ?>
                location.reload();
                return true;
            }
            else {
                $('#signin_process').html('');
                $('#signin_wrong').show();
                $("#signin_submit").focus();
                $('#signin_menu input').keyup(function () {
                    $('#signin_wrong').hide();
                });
                return false;
            }
        }
    }

    function setDoc(val) {
        var jqXHR = $.ajax({
            type: "POST",
            url: "<?php echo Yii::app()->request->baseUrl; ?>/PaAdminDoctors/docChange",
            data: { doc: $.trim(val) },
            async: false,
            success: function (result) {
            }
        });
        if (jqXHR.responseText == 1) {
            <?php if(Yii::app()->session['logged_user_type']=='parcticeAdmin'){ ?>
            window.location.href = "<?php echo $this->createAbsoluteUrl('PaAdminDoctors/index'); ?>";
            <?php } ?>
            //location.reload();
            return true;
        }
        else {
            return false;
        }
    }
    /* -----------------------------X-----------------------------*/
</script>

<?php if (!Yii::app()->session['logged_in']) { ?>
    <script>
        window.fbAsyncInit = function () {
            FB.init({
                appId: '1496711513882013', //doctor-local(592317720864007), doctor-server(1496711513882013)
                oauth: true,
                status: true, // check login status
                cookie: true, // enable cookies to allow the server to access the session
                xfbml: true // parse XFBML
            });

        };
        (function () {
            /*var e = document.createElement('script');
             e.src = document.location.protocol + '//connect.facebook.net/en_US/all.js';
             e.async = true;
             document.getElementById('fb-root').appendChild(e);*/
        }());

        function fb_login() {
            FB.login(function (response) {

                if (response.authResponse) {
                    //console.log('Welcome!  Fetching your information.... ');

                    //console.log(response); // dump complete info
                    access_token = response.authResponse.accessToken; //get access token
                    user_id = response.authResponse.userID; //get FB UID


                    FB.api('/me', function (response) {

                        var user_name = response.email;
                        var fb_id = response.id;

                        var jqXHR = $.ajax({
                            type: "POST",
                            url: "<?php echo Yii::app()->request->baseUrl; ?>/registration/signin",
                            data: {email: user_name, fb_id: fb_id, account_type: $("input[name='account_type']:checked").val()},
                            async: false,
                            success: function (result) {
                            }
                        });//alert(jqXHR.toSource());
                        if (jqXHR.responseText == 1) {
                            window.location = '<?php echo $this->createAbsoluteUrl('patient/index'); ?>';
                            return true;
                        } else {
                            $('#signin_wrong').show();
                            return false;
                        }
                    });

                } else {
                    //user hit cancel button
                    //console.log('User cancelled login or did not fully authorize.');
                    alert('User cancelled login or did not fully authorize.');
                }
            }, {
                scope: 'publish_stream,email'
            });
        }
    </script>
<?php } ?>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/assets/js/jquery.accordion.2.0.js"></script>
<script type="text/javascript">
    $('#example2').accordion({
        canToggle: true
    });
    $(".loading").removeClass("loading");
</script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/assets/js/jquery.tokeninput.js"></script>

</body>
</html>
