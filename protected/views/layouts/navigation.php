<script type="text/javascript">
    // <![CDATA[
    function showHide(id) {

        if(!$('#' + id).is(':visible')){
            $('.hide').slideUp(100);
            $('#' + id).slideDown(500);
        }else{
            $('#' + id).slideUp(500);
        }
    }
    // ]]>
</script>
<style>
    #showHideDiv10 {
        background-color: #FFFFFF;
        display: none;
        position: relative;
        z-index: 100;
    }
</style>
<?php /*
<ul>
	<li class="active"><a href="/doctor/index">Dashboard</a></li>
	<li><a href="/doctor/editProfile/<?php echo Yii::app()->session['logged_user_id']; ?>">Edit My Account</a> </li>
	<li><a href="/doctor/offers/<?php echo Yii::app()->session['logged_user_id']; ?>">Special Offers</a></li>
	<li><a href="/doctor/appointment/<?php echo Yii::app()->session['logged_user_id']; ?>">Appointments</a></li>
	<li><a href="/doctor/schedule/<?php echo Yii::app()->session['logged_user_id']; ?>">Schedules</a></li>
	<li><a href="/doctor/timeoff/<?php echo Yii::app()->session['logged_user_id']; ?>">Timeoff</a></li>
	<li><a href="/doctor/todolist/<?php echo Yii::app()->session['logged_user_id']; ?>">Todo List</a></li>
	<li><a href="/doctor/patient/<?php echo Yii::app()->session['logged_user_id']; ?>">Patients</a></li>
	<li><a href="/doctor/settingTab/<?php echo Yii::app()->session['logged_user_id']; ?>">Setting Tab</a></li>
	<li ><a href="/dashboard/default/index">Analytics</a></li>
</ul>
 */ ?>
<?php
$connection = Yii::app()->db;
$sqlCount="select * FROM da_inbox  LEFT JOIN `da_message_inbox`  ON(da_inbox.inbox_id = da_message_inbox.`message_id`) WHERE status=1  AND da_message_inbox.user_id='" . Yii::app()->session['logged_user_id'] . "' AND da_message_inbox.message_type=1 AND `message_read_flag`=0";
$commandCount = $connection->createCommand($sqlCount);
$inbox_arr_count = $commandCount->queryAll();
$count = count($inbox_arr_count);
Yii::app()->session['inbox_count'] = $count;



?>
<?php $server_prefix = Yii::app()->request->baseUrl;

?>
<?php if( isset(Yii::app()->session['logged_user_type']) && (Yii::app()->session['logged_user_type'] == "doctor" ) ) { ?>
    <h2>Doctor control panel</h2>
    <ul>
        <?php if(isset(Yii::app()->session['logged_with_claim']) && Yii::app()->session['logged_with_claim'] == '1'){  ?>
            <li class="active" ><a href="<?php echo $server_prefix; ?>/doctor/editProfile/<?php echo Yii::app()->session['logged_user_id']; ?>">Profile Setup</a> </li>


        <?php }else{  ?>
            <li class="active"><a href="<?php echo $server_prefix; ?>/doctor/index">Dashboard</a></li>
            <li ><a href="#">Messaging Center</a></li>

                    <li><a href="<?php echo $server_prefix; ?>/message/compose/<?php echo Yii::app()->session['logged_user_id']; ?>"> » Compose Message</a></li>
                    <li><a href="<?php echo $server_prefix; ?>/message/inbox/<?php echo Yii::app()->session['logged_user_id']; ?>"> » Inbox</a></li>
                    <li><a href="<?php echo $server_prefix; ?>/message/sentMail/<?php echo Yii::app()->session['logged_user_id']; ?>">  » Sent Message</a></li>
                    <li><a href="<?php echo $server_prefix; ?>/message/ArchiveMail/<?php echo Yii::app()->session['logged_user_id']; ?>">  » Filed Messages</a></li>

           <!-- <li><?php /*echo CHtml::link('Documents', $this->createAbsoluteUrl('doctor/document/'.Yii::app()->session['logged_user_id'])); */?></li>-->
            <li><a href="<?php echo $server_prefix; ?>/doctor/appointment/<?php echo Yii::app()->session['logged_user_id']; ?>">Appointments / Calendar</a></li>
            <li><a href="<?php echo $server_prefix; ?>/doctor/patient/<?php echo Yii::app()->session['logged_user_id']; ?>">Patients</a></li>
            <li><a href="<?php echo $server_prefix; ?>/doctor/todolist/<?php echo Yii::app()->session['logged_user_id']; ?>">Task Manager</a></li>
            <li><a href="<?php echo $server_prefix; ?>/doctor/offers/<?php echo Yii::app()->session['logged_user_id']; ?>">Special Offers</a></li>
            <li><a href="<?php echo $server_prefix; ?>/doctor/timeoff/<?php echo Yii::app()->session['logged_user_id']; ?>">Timeoff / Holidays Setup</a></li>
            <li><a href="<?php echo $server_prefix; ?>/doctor/settingTab/<?php echo Yii::app()->session['logged_user_id']; ?>">Reminder &amp; Alert Setup</a></li>
            <li><a href="<?php echo $server_prefix; ?>/doctor/schedule/<?php echo Yii::app()->session['logged_user_id']; ?>">Schedule Setup</a></li>
            <li><a href="<?php echo $server_prefix; ?>/doctor/editProfile/<?php echo Yii::app()->session['logged_user_id']; ?>">Practice Setup</a> </li>


            <?php /*?><li><a href="<?php echo $server_prefix; ?>doctor/address/<?php echo Yii::app()->session['logged_user_id']; ?>">My Addresses</a></li><?php */?>
            <?php /*?><li><a href="<?php echo $server_prefix; ?>doctor/speciatlity/<?php echo Yii::app()->session['logged_user_id']; ?>">My Specialities</a></li><?php */?>
            <?php /*?><li><a href="<?php echo $server_prefix; ?>doctor/profile/<?php echo Yii::app()->session['logged_user_id']; ?>">View Profile</a></li><?php */?>
            <li ><a href="<?php echo $server_prefix; ?>/dashboard/default/index">Analytics</a></li>
        <?php } ?>
    </ul>
<?php }
if( isset(Yii::app()->session['logged_user_type']) && (Yii::app()->session['logged_user_type'] == "patient" ) ) { ?>
    <h2>Patient control panel</h2>
    <ul>
        <li class="active"><a href="<?php echo $server_prefix; ?>/patient/index">Dashboard</a></li>
        <li class="active"><?php echo CHtml::link('My Account', $this->createAbsoluteUrl('patient/editProfile/'.Yii::app()->session['logged_user_id'])); ?></li>
        <li><?php echo CHtml::link('Reset Password', $this->createAbsoluteUrl('patient/resetpassword/'.Yii::app()->session['logged_user_id'])); ?></li>
        <li ><a href="#">Messaging Center</a></li>

                <li><a href="<?php echo $server_prefix; ?>/message/compose/<?php echo Yii::app()->session['logged_user_id']; ?>"> » Compose Message</a></li>
                <li><a href="<?php echo $server_prefix; ?>/message/inbox/<?php echo Yii::app()->session['logged_user_id']; ?>"> » Inbox</a></li>
                <li><a href="<?php echo $server_prefix; ?>/message/sentMail/<?php echo Yii::app()->session['logged_user_id']; ?>">  » Sent Message</a></li>
                <li><a href="<?php echo $server_prefix; ?>/message/archiveMail/<?php echo Yii::app()->session['logged_user_id']; ?>">  » Filed Messages</a></li>


        <!--<li><?php /*echo CHtml::link('Documents', $this->createAbsoluteUrl('patient/document/'.Yii::app()->session['logged_user_id'])); */?></li>-->

    </ul>
<?php } ?>

<?php /*------------------ Parctice Admin -------------------------- IB*/?>
<?php if( isset(Yii::app()->session['logged_user_type']) && (Yii::app()->session['logged_user_type'] == "parcticeAdmin" ) ) { ?>
    <h2>Doctor control panel</h2>
    <ul>
        <?php
        if(isset(Yii::app()->session['assigned_doctor'])) {
            $doctorAsigName = explode(",",Yii::app()->session['assigned_doctor']);

            if(count($doctorAsigName) > 0) {
                ?>
                <li>
                    <select id="doctorAsigned" onchange="setDoc(this.value);" >
                        <?php
                        foreach ($doctorAsigName as $key => $val) {
                            $asDocNameArr = PaAdminDoctors::model()->doctorsName($val);
                            ?>
                            <option
                                value="<?php echo $val; ?>"
                                <?php if (isset(Yii::app()->session['logged_user_id']) && (Yii::app()->session['logged_user_id'] == $val)) {
                                echo "selected";
                            } ?>><?php echo $asDocNameArr[0]->doctor_first_name . " " . $asDocNameArr[0]->doctor_last_name; ?></option>
                        <?php } ?>
                    </select>
                </li>
            <?php } } ?>
        <li class="active"><a href="<?php echo $server_prefix; ?>/paAdminDoctors/index">Dashboard</a></li>
        <li ><a href="#">Messaging Center</a></li>
        <div  id="showHideDiv101" class="hide1" >
            <ul>
                <li><a href="<?php echo $server_prefix; ?>/message/compose/<?php echo Yii::app()->session['logged_user_id']; ?>"> Â» Compose Message</a></li>
                <li><a href="<?php echo $server_prefix; ?>/message/inbox/<?php echo Yii::app()->session['logged_user_id']; ?>"> Â» Inbox</a></li>
                <li><a href="<?php echo $server_prefix; ?>/message/sentMail/<?php echo Yii::app()->session['logged_user_id']; ?>">  Â» Sent Message</a></li>
                <li><a href="<?php echo $server_prefix; ?>/message/ArchiveMail/<?php echo Yii::app()->session['logged_user_id']; ?>"> Â» Filed Messages</a></li>
            </ul>
        </div>
        <li><?php echo CHtml::link('Documents Vault', $this->createAbsoluteUrl('doctor/document/'.Yii::app()->session['logged_user_id'])); ?></li>
        <li><a href="<?php echo $server_prefix; ?>/doctor/appointment/<?php echo Yii::app()->session['logged_user_id']; ?>">Appointments / Calendars</a></li>
        <li><a href="<?php echo $server_prefix; ?>/doctor/patient/<?php echo Yii::app()->session['logged_user_id']; ?>">Patients</a></li>
        <li><a href="<?php echo $server_prefix; ?>/doctor/todolist/<?php echo Yii::app()->session['logged_user_id']; ?>">Task Manager</a></li>
        <li><a href="<?php echo $server_prefix; ?>/doctor/offers/<?php echo Yii::app()->session['logged_user_id']; ?>">Special Offers</a></li>
        <li><a href="<?php echo $server_prefix; ?>/doctor/timeoff/<?php echo Yii::app()->session['logged_user_id']; ?>">Timeoff / Holidays Setup</a></li>
        <li><a href="<?php echo $server_prefix; ?>/doctor/settingTab/<?php echo Yii::app()->session['logged_user_id']; ?>">Reminder &amp; Alert Setup</a></li>
        <li><a href="<?php echo $server_prefix; ?>/doctor/schedule/<?php echo Yii::app()->session['logged_user_id']; ?>">Schedule Setup</a></li>
        <li><a href="<?php echo $server_prefix; ?>/doctor/editProfile/<?php echo Yii::app()->session['logged_user_id']; ?>">Profile Setup</a> </li>
        <li ><a href="<?php echo $server_prefix; ?>/dashboard/default/index">Analytics</a></li>
    </ul>
<?php } ?>
<?php /*
 <ul>
	 <li class="active" ><?php echo CHtml::link('Dashboard', $this->createAbsoluteUrl('index')); ?></li>
	 <li><?php echo CHtml::link('Appointments/Calender', $this->createAbsoluteUrl('doctor/appointment/'.Yii::app()->session['logged_user_id'])); ?></li>
	 <li ><?php echo CHtml::link('My Addresses', $this->createAbsoluteUrl('doctor/address/'.Yii::app()->session['logged_user_id'])); ?></li>
	 <li><?php echo CHtml::link('My Specialities', $this->createAbsoluteUrl('doctor/speciatlity/'.Yii::app()->session['logged_user_id'])); ?></li>
	 <li><?php echo CHtml::link('View Profile', $this->createAbsoluteUrl('doctor/profile/'.Yii::app()->session['logged_user_id'])); ?></li>
	 <li><?php echo CHtml::link('Schedules', $this->createAbsoluteUrl('doctor/schedule/'.Yii::app()->session['logged_user_id'])); ?></li>
	 <li><?php echo CHtml::link('Timeoff', $this->createAbsoluteUrl('doctor/timeoff/'.Yii::app()->session['logged_user_id'])); ?></li>
	 <li><?php echo CHtml::link('Todo List', $this->createAbsoluteUrl('doctor/todolist/'.Yii::app()->session['logged_user_id'])); ?></li>
	 <li><?php echo CHtml::link('Patients', $this->createAbsoluteUrl('doctor/patient/'.Yii::app()->session['logged_user_id'])); ?></li>
	 <li><?php echo CHtml::link('Setting Tab', $this->createAbsoluteUrl('doctor/settingTab/'.Yii::app()->session['logged_user_id'])); ?></li>
	 <li><?php echo CHtml::link('Analytics', $this->createAbsoluteUrl('dashboard/default/index')); ?></li>
</ul>
*/ ?>