<?php /* @var $this Controller */ ?>
<!DOCTYPE HTML>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="eng">
    <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <title><?php echo CHtml::encode(Yii::app()->name); ?></title>
        <link rel="shortcut icon" href="<?php echo Yii::app()->request->baseUrl; ?>/assets/images/eDoctorBookfav.ico" type="image/x-icon" />
        <link href="<?php echo Yii::app()->request->baseUrl; ?>/assets/css/style.css?r=<?php echo time(); ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo Yii::app()->request->baseUrl; ?>/assets/css/pager.css?r=<?php echo time(); ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo Yii::app()->request->baseUrl; ?>/assets/css/responsive.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/assets/css/flexslider.css" type="text/css" media="screen" />
        <link href="<?php echo Yii::app()->request->baseUrl; ?>/assets/css/royalslider.css" rel="stylesheet" />
        <!--<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/assets/css/default.css" type="text/css" media="screen" />-->
        <!--[if IE 8]>
        <style>
        span.loginpanel a{ behavior:url(pie/PIE.htc);}
        nav span{behavior:url(pie/PIE.htc);}
        .searchbg{behavior:url(pie/PIE.htc);}
        .searchbg form input{behavior:url(pie/PIE.htc);}
        .sbHolder{behavior:url(pie/PIE.htc);}
        .appoinmrnt_box h1 span{behavior:url(pie/PIE.htc);}
        .appoinmrnt_box_last h1 span{behavior:url(pie/PIE.htc);}
        #signin_menu input[type=text], #signin_menu input[type=password]{behavior:url(pie/PIE.htc); padding:5px; width:180px;}
        #signin_submit{behavior:url(pie/PIE.htc);}
        #signin_menu{right:-10px; width:190px; padding:19px 10px 10px 10px;}
        
        
        </style>
        <![endif]-->

        <!--[if IE 7]>
        <style>
        span.loginpanel a{ behavior:url(pie/PIE.htc);}
        nav span{behavior:url(pie/PIE.htc);}
        .searchbg{behavior:url(pie/PIE.htc);}
        .searchbg form input{behavior:url(pie/PIE.htc);}
        .sbHolder{behavior:url(pie/PIE.htc);}
        .appoinmrnt_box h1 span{behavior:url(pie/PIE.htc);}
        .appoinmrnt_box_last h1 span{behavior:url(pie/PIE.htc);}
        #signin_menu input[type=text], #signin_menu input[type=password]{behavior:url(pie/PIE.htc); padding:5px; width:180px;}
        #signin_submit{behavior:url(pie/PIE.htc);}
        #signin_menu{right:-10px; width:190px; padding:19px 10px 10px 10px;}
        </style>
        <![endif]-->
        <!--[if lt IE 9]>
                        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <?php /* ?><script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/assets/js/jquery-1.7.2.min.js"></script><?php */ ?>
        <link href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="//code.jquery.com/jquery-1.10.2.js"></script>
        <script src="http://code.jquery.com/jquery-migrate-1.0.0.js"></script>
        <script type="text/javascript" src="//code.jquery.com/ui/1.10.2/jquery-ui.js"></script>

        <?php /* ?>
          <link href="<?php echo Yii::app()->request->baseUrl; ?>/assets/css/jquery-ui.css" rel="stylesheet" />
          <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/assets/js/jquery-1.9.1.js"></script>
          <script src="http://code.jquery.com/jquery-migrate-1.0.0.js"></script>
          <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/assets/js/jquery-ui.js"></script><?php */ ?>

<!--<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/assets/js/jquery.nivo.slider.js"></script>
<script type="text/javascript">
  $(window).load(function() {
       // $('#slider').nivoSlider();
  });
</script>-->
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/assets/js/jquery.easing-1.3.js"></script>
        <script type="text/javascript" language="javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/assets/js/jscriptnew.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/assets/js/jquery.royalslider.min.js"></script>
        <script type="text/javascript" language="javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/assets/js/jscript_omlp_widget_476.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/assets/js/jquery.selectbox-0.2.js"></script>

        <script type="text/javascript">
            $(function () {
                $("#location_id").selectbox();
            });
            $(document).ready(function () {
                $.ajaxSetup({cache: true});
            });
        </script>
        <script type="text/javascript">
            $(document).ready(function () {
                function add() {
                    if ($(this).val() === '') {
                        $(this).val($(this).attr('placeholder')).addClass('placeholder');
                    }
                }

                function remove() {
                    if ($(this).val() === $(this).attr('placeholder')) {
                        $(this).val('').removeClass('placeholder');
                    }
                }

                // Create a dummy element for feature detection
                if (!('placeholder' in $('<input>')[0])) {

                    // Select the elements that have a placeholder attribute
                    $('input[placeholder], textarea[placeholder]').blur(add).focus(remove).each(add);

                    // Remove the placeholder text before the form is submitted
                    $('form').submit(function () {
                        $(this).find('input[placeholder], textarea[placeholder]').each(remove);
                    });
                }
            });
        </script>

        <script type="text/javascript">
            jQuery(document).ready(function ($) {
                $('html').click(function () {
                    $('#signin_menu').hide();
                    $(".signin").removeClass('active');
                });


                $('.signin, #signin_menu').bind('click', function (event) {
                    event.stopPropagation();
                });
                $(".signin").click(function (e) {
                    e.preventDefault();
                    $("fieldset#signin_menu").toggle();
                    $(".signin").toggleClass("menu-open");
                    $(".signin").toggleClass('active');
                    $("#username").focus();
                });

                /*$("fieldset#signin_menu").mouseup(function() {
                 return false
                 });
                 $(document).mouseup(function(e) {
                 if($(e.target).parent("a.signin").length==0) {
                 $(".signin").removeClass("menu-open");
                 $("fieldset#signin_menu").hide();
                 }
                 });*/
            });
        </script>
        <script>
            $(function () {
                $(".datepicker").datepicker({
                    changeMonth: true,
                    changeYear: true,
                    dateFormat: 'yy-mm-dd'
                });
            });
        </script>
        <script type="text/javascript">
            function equalHeights(element1, element2) {
                var height;
                if (element1.outerHeight() > element2.outerHeight())
                {
                    height = element1.outerHeight();
                    element2.css('height', height);
                }
                else {
                    height = element2.outerHeight();
                    element1.css('height', height);
                }
            }


        </script>
        <script type="text/javascript">
            $(document).ready(function () {
                $('#nav li').hover(function () {
                    $('ul', this).slideDown(200);
                    $(this).children('a:first').addClass("hov");
                }, function () {
                    $('ul', this).slideUp(100);
                    $(this).children('a:first').removeClass("hov");
                });
            });
        </script>
        <?php if (Yii::app()->controller->action->id != 'appointment') { ?>
            <script type="text/javascript">
                $(function () {
                    // equalHeights ($(".leftmenu"), $('.rightarea_dashboard'));
                });
            </script>
        <?php } ?>
    </head>

    <body>

        <!--haeder part start-->
        <input type="hidden" id="base_url" name="base_url" value="<?php echo Yii::app()->request->baseUrl; ?>">
        <input type="hidden" id="base_url" name="base_url" value="<?php echo Yii::app()->request->baseUrl; ?>">
        <header>
            <div class="topbar">
                <div class="topbar_container">
                    <div class="logo">
                        <a href="<?php echo $this->createAbsoluteUrl('site/index'); ?>">
                            <img src="<?php echo Yii::app()->request->baseUrl; ?>/assets/images/logo.png" alt="eDoctorBook"></a>
                    </div>
                    <?php
                    if (Yii::app()->session['logged_in']) {
                        $class = "margin-top: 4px;";
                    } else {
                        $class = "margin-top: 18px;";
                    }
                    ?>
                    <div class="loginsect" style="<?= $class ?>">

                        <?php if (Yii::app()->session['logged_in']) { ?>
                            <span class="username"><img src="<?php echo Yii::app()->request->baseUrl; ?>/assets/images/mail_box_icon.png"><a href="<?php echo $this->createAbsoluteUrl('message/inbox/' . Yii::app()->session['logged_user_id']); ?>"> Inbox ( <strong><?php echo isset(Yii::app()->session['inbox_count']) ? Yii::app()->session['inbox_count'] : 0; ?></strong> ) </a></span>
                            <?php if (Yii::app()->session['logged_user_type'] == 'doctor') { ?>
                                <span class="username"><img src="<?php echo Yii::app()->request->baseUrl; ?>/assets/images/user.png"><a href="<?php echo $this->createAbsoluteUrl('doctor/index'); ?>"><?php //echo Yii::app()->session['logged_user_email'];  ?>My Account</a></span>
                            <?php } else { ?>
                                <span class="username"><img src="<?php echo Yii::app()->request->baseUrl; ?>/assets/images/user.png"><a href="<?php echo $this->createAbsoluteUrl('patient/index'); ?>"><?php //echo Yii::app()->session['logged_user_email'];  ?>My Account</a></span>
                            <?php } ?>
                        <?php } ?>

                        <span class="loginpanel">
                            <?php /* ?> <?php if(!Yii::app()->session['logged_in']){ ?>
                              <a class="docttt">Doctor's or Dentist? </a>
                              <?php } ?><?php */ ?>
                            <!--<a href="doctor_registration.html">Join Us Now!</a>-->
                            <?php if (Yii::app()->session['logged_in']) { ?>
                                <a class="signout" href="javascript:void(0);" onClick="signOut();">Logout</a>
                            <?php } else { ?>
                                <?php //echo CHtml::link('Doctors or Dentist? Join Us Now!', $this->createAbsoluteUrl('registration/join')); ?>
                                <?php
                                $page_name_d = Yii::app()->controller->action->id;
                                if ($page_name_d != "login") {
                                    ?>
                                    <a class="signin" href="javascript:void(0);">Login</a>
                                <?php } ?>
                                                    <!-- <a class="signin" href="javascript:void(0);" onclick="location.href='<?php echo Yii::app()->request->baseUrl; ?>/ddb'">Login</a> -->
                            <?php } ?>
                        </span>
                    </div>
                    <?php
                    
                     $page_name_d ="";
                    if ($page_name_d != "login") {
                        ?>
                        <fieldset id="signin_menu">
                        <?php $form = $this->beginWidget('CActiveForm', array('id' => 'person-form-edit_person-form', 'enableAjaxValidation' => false, 'htmlOptions' => array('onsubmit' => "return false;", /* Disable normal form submit */
                                'onkeypress' => " if(event.keyCode == 13){ return signIn(); } " /* Do ajax call when user presses enter key */),));
                        ?>               <span id="signin_wrong" style="margin-top: -33px; float:left; width:95%; display:none; color:#ff0000; font-weight:bold; padding:3px; padding-top:20px;  ">Wrong User ID and/or Password</span>
                           <!-- <span style="padding-bottom:10px; font-size:12px; font-weight:bold;">
                                   <label style="float:left; display:block; padding-right:10px;"><input type="radio" name="account_type" value="doctor" checked>Doctor</label>
                              <label style="float:left; display:block;"> <input type="radio" name="account_type" value="patient" >Patient</label>
                           </span> -->
                            <span style="padding-bottom:10px; font-size:12px; font-weight:bold;"><label>Login Form</label></span>


                            <input id="username" name="username" value="" title="username" placeholder="User ID" tabindex="2" type="text" autocomplete="off">
                            <input id="password" name="password" value="" title="password" placeholder="Password" tabindex="3" type="password" autocomplete="off">
                            <span>

                    <a tabindex="4" id="signin_submit" href="javascript:void(0);" onClick="return signIn();">Go</a><!--<input id="signin_submit" value="Go" tabindex="3" type="submit">-->
                                <a id="forgot_pass" href="<?php echo $this->createAbsoluteUrl('doctor/forgetpassword'); ?>">Forgot password?</a>
                                <span id="signin_process" style="margin-top: -33px; margin-left:10px;"></span>

                                <a id="fb-root" href="javascript:void(0);" tabindex="5" style="display:none;" onClick="fb_login();">Login with Facebook</a>
                            </span>
                            <?php $this->endWidget(); ?>
                        </fieldset>
                        <?php }?>
                    </div>
                </div>
            </header>
            <!--<div class="bannerimg">-->

            <!-- </div>-->

            <!--body part start-->
            <?php echo $content; ?>
            <!--footer part start-->

            <footer>
                <?php
                $controller_name_footer = Yii::app()->controller->id;
                $action_id_footer = Yii::app()->controller->action->id;
                if (($controller_name_footer == 'site' && $action_id_footer == 'index') || ($controller_name_footer == 'doctor' && $action_id_footer == 'doctorSearch')) {
                    ?>
                    <div class="main">
                        <div class="footersection">
                            <div class="footer_box1">
                                <h1>Search By </h1>
                                <ul>
                                    <li><?php echo CHtml::link('Doctor Name', $this->createAbsoluteUrl('doctor/doctorSearchByName/')); ?></li>
                                    <li><a href="javascript:void(0);">Practice Name</a></li>
                                    <li><?php echo CHtml::link('Procedure', $this->createAbsoluteUrl('doctor/doctorSearchByProcedure/')); ?></li>
                                    <li><?php echo CHtml::link('Language', $this->createAbsoluteUrl('doctor/doctorSearchByLanguage/')); ?></li>
                                    <li><a href="javascript:void(0);">Location</a></li>
                                    <li><?php echo CHtml::link('Hospital', $this->createAbsoluteUrl('doctor/doctorSearchByHospital/')); ?></li>
                                    <li><?php echo CHtml::link('Insurance', $this->createAbsoluteUrl('doctor/doctorSearchByInsurance/')); ?></li>
                                </ul>
                            </div>
                            <div class="footer_box1">
                                <h1>Cities</h1>
                                <ul>
                                    <li><a href="<?php echo $this->createAbsoluteUrl('doctor/doctorSearch?geo_location_id=NY'); ?>">New York City</a></li>
                                    <li><a href="<?php echo $this->createAbsoluteUrl('doctor/doctorSearch?geo_location_id=GA'); ?>">Georgia</a></li>
                                    <li><a href="<?php echo $this->createAbsoluteUrl('doctor/doctorSearch?geo_location_id=AZ'); ?>">Arizona</a></li>
                                    <li><a href="<?php echo $this->createAbsoluteUrl('doctor/doctorSearch?geo_location_id=CA'); ?>">California</a></li>
                                    <li><a href="<?php echo $this->createAbsoluteUrl('doctor/doctorSearch?geo_location_id=FL'); ?>">Florida</a></li>
                                    <li><a href="<?php echo $this->createAbsoluteUrl('doctor/doctorSearch?geo_location_id=MO'); ?>">Missouri</a></li>
                                    <li><a href="<?php echo $this->createAbsoluteUrl('doctor/doctorSearch?geo_location_id=TX'); ?>">Texas</a></li>
                                </ul>
                            </div>
                            <div class="footer_box1">
                                <h1>Specialties</h1>
                                <ul>
                                    <li><a href="<?php echo $this->createAbsoluteUrl('doctor/doctorSearch?speciality_id=1'); ?>">Allergy & Immunology</a></li>
                                    <li><a href="<?php echo $this->createAbsoluteUrl('doctor/doctorSearch?speciality_id=2'); ?>">Anesthesiology</a></li>
                                    <li><a href="<?php echo $this->createAbsoluteUrl('doctor/doctorSearch?speciality_id=3'); ?>">Cardiology</a></li>
                                    <li><a href="<?php echo $this->createAbsoluteUrl('doctor/doctorSearch?speciality_id=4'); ?>">Dermatology</a></li>
                                    <li><a href="<?php echo $this->createAbsoluteUrl('doctor/doctorSearch?speciality_id=5'); ?>">Emergency Medicine</a></li>
                                    <li><a href="<?php echo $this->createAbsoluteUrl('doctor/doctorSearch?speciality_id=6'); ?>">Endocrinology, Diabetes, & Metabolism</a></li>
                                    <li><a href="<?php echo $this->createAbsoluteUrl('doctor/doctorSearch?speciality_id=7'); ?>">Family Practice</a></li>
                                </ul>
                            </div>
                            <div class="footer_lastbox">
                                <h1>Need help booking?<br>Call</h1>
                                <h2>eDoctorBook<br><font style="color:#fff; font-weight:normal">at</font><br>
                                    <strong style="font-size:20px;">(000) 000-0000</strong></h2>
                                OR Email <a href="javascript:void(0);">Service@dab.com</a>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                <div class="footer_nav">
                    <div class="main">
                        <ul>
                            <li><a href="<?php echo $this->createAbsoluteUrl('doctor/about'); ?>">About</a></li>
                            <li><a href="<?php echo $this->createAbsoluteUrl('doctor/careers'); ?>">Jobs</a></li>
                            <li><a href="<?php echo $this->createAbsoluteUrl('doctor/media'); ?>">Advertise</a></li>
                            <li><a href="<?php echo $this->createAbsoluteUrl('doctor/faq'); ?>">FAQ</a></li>
                            <li><a href="javascript:void(0);">Blog</a></li>
                            <li><a href="<?php echo $this->createAbsoluteUrl('doctor/termsCondition'); ?>">Terms of Use</a></li>
                            <li><a href="<?php echo $this->createAbsoluteUrl('doctor/privacy'); ?>">Privacy Policy</a></li>
                        </ul>
                        <span class="copyright">eDoctorBook © <?php echo date('Y'); ?></span>
                    </div>
                </div>
                <div class="footer_social">
                    <div class="main">

                        <ul>
                            <li class="facebook"><a href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Farbcloudsoft.com%2Fdev%2Fdoctorappointment" onclick="window.open(this.href, 'eDoctorBookFacebook',
                                            'left=20,top=20,width=500,height=500,toolbar=1,resizable=0');
                                    return false;"></a></li>
                            <li class="twitter"><a href="http://www.twitter.com/share?text=eDoctorBook&url=&hashtags=eDoctorBook,Appointment,twitt" onclick="window.open(this.href, 'eDoctorBookTwitter',
                                            'left=20,top=20,width=500,height=500,toolbar=1,resizable=0');
                                    return false;"></a></li>
                            <li class="gplus"><a href="https://plus.google.com/share?url=http%3A%2F%2Farbcloudsoft.com%2Fdev%2Fdoctorappointment" onclick="window.open(this.href, 'eDoctorBookGooglePlus',
                                            'left=20,top=20,width=500,height=500,toolbar=1,resizable=0');
                                    return false;"></a></li>
                        </ul>
                    </div>
                </div>

            </footer>
            <script defer src="<?php echo Yii::app()->request->baseUrl; ?>/assets/js/jquery.flexslider.js"></script>
            <script src="<?php echo Yii::app()->request->baseUrl; ?>/assets/js/modernizr.js"></script>
            <script type="text/javascript">
                                /* $(function(){
                                 SyntaxHighlighter.all();
                                 });*/
                                $(window).load(function () {
                                    $('.flexslider').flexslider({
                                        animation: "fade",
                                        start: function (slider) {
                                            // $('body').removeClass('loading');
                                        }
                                    });
                                    $("input[name='account_type']").click(function () {
                                        if ($(this).val() == 'patient')
                                            $('#fb-root').show();
                                        else
                                            $('#fb-root').hide();
                                    });
                                });
            </script>
            <script>
                function signIn() {
                    if ($.trim($('#username').val()) == '' || $.trim($('#password').val()) == '') {
                        return false;
                    } else {
                        $('#signin_process').html('<img src="<?php echo Yii::app()->request->baseUrl; ?>/assets/images/process.gif">');
                        var jqXHR = $.ajax({
                            type: "POST",
                            url: "<?php echo Yii::app()->request->baseUrl; ?>/registration/signin",
                            data: {email: $.trim($('#username').val()), password: $.trim($('#password').val()), account_type: $("input[name='account_type']:checked").val()},
                            async: false,
                            success: function (result) {
                            }
                        });//alert(jqXHR.responseText);return false;
                        if (jqXHR.responseText == 1) {
                            /*if($('#signin_cook').is(':checked')){
                             set_cookie('email', $.trim($('#signin_email').val()));
                             }else{
                             unset_cookie('email', '');
                             }*/
                            if ($("input[name='account_type']:checked").val() == 'doctor')
                                window.location.href = "<?php echo $this->createAbsoluteUrl('doctor/index'); ?>";
                            else
                                window.location.href = "<?php echo $this->createAbsoluteUrl('patient/index'); ?>";
                            return true;
                        }
                        else {
                            $('#signin_process').html('');
                            $('#signin_wrong').show();
                            $("#signin_submit").focus();
                            $('#signin_menu input').keyup(function () {
                                $('#signin_wrong').hide();
                            });
                            return false;
                        }
                    }
                }
                function signOut() {
                    $.ajax({
                        type: "POST",
                        url: "<?php echo Yii::app()->request->baseUrl; ?>/registration/signout",
                        data: {},
                        success: function (result) {
                            window.location.href = "<?php echo Yii::app()->request->baseUrl; ?>";
                        }
                    });
                }
            </script>

    <?php if (!Yii::app()->session['logged_in']) { ?>
                <script>
                    window.fbAsyncInit = function () {
                        FB.init({
                            appId: '1496711513882013', //doctor-local(592317720864007), doctor-server(1496711513882013)
                            oauth: true,
                            status: true, // check login status
                            cookie: true, // enable cookies to allow the server to access the session
                            xfbml: true // parse XFBML
                        });

                    };
                    (function () {
                        var e = document.createElement('script');
                        e.src = document.location.protocol + '//connect.facebook.net/en_US/all.js';
                        e.async = true;
                        document.getElementById('fb-root').appendChild(e);
                    }());

                    function fb_login() {
                        FB.login(function (response) {

                            if (response.authResponse) {
                                //console.log('Welcome!  Fetching your information.... ');
                                //console.log(response); // dump complete info
                                access_token = response.authResponse.accessToken; //get access token
                                user_id = response.authResponse.userID; //get FB UID


                                FB.api('/me', function (response) {

                                    var user_name = response.email;
                                    var fb_id = response.id;

                                    var jqXHR = $.ajax({
                                        type: "POST",
                                        url: "<?php echo Yii::app()->request->baseUrl; ?>/registration/signin",
                                        data: {email: user_name, fb_id: fb_id, account_type: $("input[name='account_type']:checked").val()},
                                        async: false,
                                        success: function (result) {
                                        }
                                    });//alert(jqXHR.toSource());
                                    if (jqXHR.responseText == 1) {
                                        window.location = '<?php echo $this->createAbsoluteUrl('patient/index'); ?>';
                                        return true;
                                    } else {
                                        $('#signin_wrong').show();
                                        return false;
                                    }
                                });

                            } else {
                                //user hit cancel button
                                //console.log('User cancelled login or did not fully authorize.');
                                alert('User cancelled login or did not fully authorize.');
                            }
                        }, {
                            scope: 'publish_stream,email'
                        });
                    }
                </script>
    <?php } ?>

    </body>
</html>
