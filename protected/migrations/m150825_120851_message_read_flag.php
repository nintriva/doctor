<?php

class m150825_120851_message_read_flag extends CDbMigration
{
	public function up()
	{
        $this->execute("ALTER TABLE `da_message_inbox` ADD  `message_read_flag` TINYINT(4) NOT NULL DEFAULT '0';");
	}

	public function down()
	{
		echo "m150825_120851_message_read_flag does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}