<?php

class m151027_074610_archived_at extends CDbMigration
{
	public function up()
	{
        $this->execute("ALTER TABLE `da_message_inbox` ADD `archived_at` INT NULL ;");
        return true;
	}

	public function down()
	{
		echo "m151027_074610_archived_at does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}