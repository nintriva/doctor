<?php

class m150724_065939_document_table extends CDbMigration
{
	public function up()
	{
        $this->execute("
       CREATE TABLE IF NOT EXISTS 'db_document' (
    'id' int(11) NOT NULL AUTO_INCREMENT,
  'owner' varchar(500) NOT NULL,
  'document_name' varchar(1000) NOT NULL,
  'label' varchar(100) NOT NULL,
  'status' enum('','') NOT NULL,
  'date' date NOT NULL,
  'note' varchar(2000) NOT NULL,
  'file_url' varchar(500) NOT NULL,
  'is_private' enum('','') NOT NULL,
  'is_share' enum('','') NOT NULL,
  'key' varchar(200) NOT NULL,
  'created_date' DATETIME NOT NULL,
 'updated_date' DATETIME NOT NULL,
  PRIMARY KEY ('id')
) ENGINE=InnoDB DEFAULT CHARSET=utf32 AUTO_INCREMENT=1 ;
");

	}

	public function down()
	{
		echo "m150724_065939_document_table does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}