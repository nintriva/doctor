<?php

class m151008_092056_department_table extends CDbMigration
{
	public function up()
	{
        $this->execute("CREATE TABLE IF NOT EXISTS `da_department_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;
INSERT INTO `da_department_category` (`id`, `name`) VALUES
(1, 'Billing'),
(2, 'General'),
(3, 'Medical Records'),
(4, 'Refill Requests'),
(5, 'Lab/XRay results'),
(6, 'Referrals');");

        $this->execute('ALTER TABLE `da_inbox` ADD `department_id` TINYINT NULL ;');
        $this->execute('ALTER TABLE `da_message_inbox` ADD `department_id` TINYINT NULL ;');

        return true ;
	}

	public function down()
	{
        $this->dropTable('da_department_category');
		return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}