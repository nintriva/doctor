<?php

class m150819_122236_inboxtype_delete extends CDbMigration
{
	public function up()
	{
        $this->execute("ALTER TABLE `da_inbox` ADD `sent_from_user_type` TINYINT NOT NULL AFTER `sent_from_email`;
ALTER TABLE `da_message_inbox` ADD `is_delete` TINYINT NOT NULL ;
");
	}

	public function down()
	{
		echo "m150819_122236_inboxtype_delete does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}