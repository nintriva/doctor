<?php

class m150820_051123_Alter_thread_id extends CDbMigration
{
	public function up()
	{
        $this->execute("ALTER TABLE `da_inbox` CHANGE `thread_id` `thread_id` VARCHAR(300) NOT NULL;");
	}

	public function down()
	{
		echo "m150820_051123_Alter_thread_id does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}