<?php

class m151031_053445_force_pw_change extends CDbMigration
{
	public function up()
	{
        $this->execute('ALTER TABLE `da_patient` ADD `force_pw_change` INT NULL ;');
        $this->execute('ALTER TABLE `da_patient` ADD `force_pw_change_reason` varchar(200) NULL ;');
        $this->execute('ALTER TABLE `da_patient` ADD `force_pw_changed_at` INT NULL ;');

        return true;
	}

	public function down()
	{
		echo "m151031_053445_force_pw_change does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}