<?php

class m150819_071344_quickmessage_table extends CDbMigration
{
	public function up()
	{
        $this->execute("
CREATE TABLE IF NOT EXISTS `da_quickmessages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `messages` varchar(500) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_type` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=0 ;
");
	}

	public function down()
	{
		echo "m150819_071344_quickmessage_table does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}