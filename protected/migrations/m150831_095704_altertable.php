<?php

class m150831_095704_altertable extends CDbMigration
{
	public function up()
	{
        $this->execute("ALTER TABLE `da_message_inbox` ADD `message_type`  tinyint(4) NOT NULL;
        ALTER TABLE `db_document` ADD `owner_type`  varchar(10) NOT NULL;
        ALTER TABLE `db_shared_document` ADD `shared_type`  varchar(10) NOT NULL;");
	}

	public function down()
	{
		echo "m150831_095704_altertable does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}