<?php

class m150914_094150_emailtemplate extends CDbMigration
{
	public function up()
	{
        $this->execute("INSERT INTO `doctor_db1`.`da_email_template` (`id`, `name`, `subject`, `tempalte_body`, `date_created`, `date_modified`, `status`) VALUES
(NULL, 'New Message Received', 'eDoctorBook - New Message', 'Hi {{$to_name}},
You have received a message from Dr.{{$from_name}} in
eDoctorBook,Kindly Login For More Details
Thank you.
 ', '', '', '1');");
	}

	public function down()
	{
		echo "m150914_094150_emailtemplate does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}