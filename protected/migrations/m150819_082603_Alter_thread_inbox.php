<?php

class m150819_082603_Alter_thread_inbox extends CDbMigration
{
	public function up()
	{
        $this->execute("ALTER TABLE `da_inbox` CHANGE `thread_id` `thread_id` INT(11) NOT NULL;");
	}

	public function down()
	{
		echo "m150819_082603_Alter_thread_inbox does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}