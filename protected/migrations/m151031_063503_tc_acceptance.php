<?php

class m151031_063503_tc_acceptance extends CDbMigration
{
	public function up()
	{
        $this->execute('CREATE TABLE IF NOT EXISTS `tc_acceptance_tracking` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `acceptance_validity` int(11) DEFAULT NULL,
  `pp_version` int(11) DEFAULT NULL,
  `tc_version` int(11) DEFAULT NULL,
  `accepted_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;');

        return true;
	}

	public function down()
	{
		echo "m151031_063503_tc_acceptance does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}