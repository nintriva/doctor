<?php

class m150817_104525_table_alter_message extends CDbMigration
{
	public function up()
	{
        $this->execute("ALTER TABLE 'da_inbox' ADD 'thread_id' INT(11) NOT NULL AFTER `attachments_path`;
ALTER TABLE `da_message_inbox` ADD `user_type` TINYINT NOT NULL , ADD `user_email` VARCHAR(300) NOT NULL , ADD `is_archive` TINYINT NOT NULL DEFAULT '0' ;");
	}

	public function down()
	{
		echo "m150817_104525_table_alter_message does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}