<?php

class m150917_051434_mailfavourites extends CDbMigration
{
	public function up()
	{
        $this->execute("CREATE TABLE IF NOT EXISTS 'da_mails_favorites' (
  'id' int(11) NOT NULL AUTO_INCREMENT,
  'user_id' int(11) NOT NULL,
  'user_type' tinyint(4) NOT NULL,
  'fav_id' int(11) NOT NULL,
  'fav_name' varchar(300) NOT NULL,
  'fav_type' tinyint(4) NOT NULL,
  'fav_email' varchar(300) NOT NULL,
  PRIMARY KEY ('fav_email')
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
");
	}

	public function down()
	{
		echo "m150917_051434_mailfavourites does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}