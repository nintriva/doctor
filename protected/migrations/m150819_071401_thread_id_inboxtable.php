<?php

class m150819_071401_thread_id_inboxtable extends CDbMigration
{
	public function up()
	{
        $this->execute("ALTER TABLE `da_inbox` CHANGE `thread_id` `thread_id` VARCHAR(300) NOT NULL;");
	}

	public function down()
	{
		echo "m150819_071401_thread_id_inboxtable does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}