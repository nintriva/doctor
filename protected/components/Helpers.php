<?php
// protected/components/Helpers.php       NOT RECOMMENDED!
 
class Helpers {
 	//need to call Helpers::pre($model);
    // does the first string start with the second?
    public static function startsWith($haystack, $needle)
    {
        return strpos($haystack, $needle) === 0;
    }
	
	public static function pre($data){
		echo "<pre>";
		print_r($data);
	}
	public static function truncate_string($string,$limit){
		if(strlen($string) > $limit){
			return '<span title="'.$string.'">'.substr($string,0,$limit).'...</span>';
		}else{	
			return '<span title="'.$string.'">'.$string.'</span>';
		}
	}
	public static function mailsend($to,$from,$subject,$message,$attachment=''){
		/*Yii::import('application.extensions.phpmailer.JPhpMailer'); 
		$mail = new JPhpMailer;
		$mail->IsSMTP(); 
		//$mail->SMTPSecure = "ssl";  
		$mail->Host = 'mail.edoctorbook.org'; 
		$mail->SMTPAuth = true; 
		$mail->Username = 'support@edoctorbook.org'; 
		$mail->Port = '25';
		$mail->Password = '123456'; 
		$mail->SMTPKeepAlive = true;  
		$mail->SMTPDebug  = 0;
		$mail->SetFrom($from[0], $from[1]); 
		$mail->Subject = $subject; 
		$mail->AltBody = $message; 
		if($attachment!='')
			$mail->AddAttachment($attachment); 
		$mail->MsgHTML($message); 
		$mail->AddAddress($to[0], $to[1]);
		$mail->Send();*/
		
		$form_name = "eDoctorBook < support@edoctorbook.com >";
		$to_email = " support@edoctorbook.com";
		$to_full_name = "";
		if ( isset ( $from ) ) {
			$form_name = $from[1]. " < ".$from[0]." > ";
		}if ( isset ( $to ) ) {
			$to_email = $to[0];
			$to_full_name = $to[1]. " < ".$to[0]." > ";
		}
		$headers = 'To: '.$to_full_name. "\r\n";
		$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
		$headers .= 'From: ' .$form_name. "\r\n" .
				'Reply-To: '.$form_name . "\r\n" .
				'X-Mailer: PHP/' . phpversion();
		if( mail( $to_email, $subject, $message, $headers ) ){
			return true;
		} else {
			return false;
		}
		
	}


	public static function mailsend_($to,$from,$subject,$message,$attachment=''){
		$form_name = "eDoctorBook <donotreply@edoctorbook.com>";
		$to_email = "donotreply@edoctorbook.com";
		$to_full_name = "";
		if ( isset ( $from ) ) {
			$form_name = $from[1]. " < ".$from[0]." > ";
		}if ( isset ( $to ) ) {
				$to_email = $to[0];
				$to_full_name = $to[1]. " < ".$to[0]." > ";
		}
		
		$headers = 'To: '.$to_full_name. "\r\n";
		$headers .= 'From: ' .$form_name. "\r\n" .
				'Reply-To: '.$form_name . "\r\n" .
				'X-Mailer: PHP/' . phpversion();	
		if($attachment!='') {
			$filename = basename($attachment);
			$file_size = filesize($attachment);
			$content = chunk_split(base64_encode(file_get_contents($attachment)));
			$uid = md5(uniqid(time()));
			$headers .= "MIME-Version: 1.0\r\n"
						."Content-Type: multipart/mixed; boundary=\"".$uid."\"\r\n\r\n"
						."This is a multi-part message in MIME format.\r\n"
						."--".$uid."\r\n"
						."Content-type: text/html; charset=utf-8\r\n"
						."Content-Transfer-Encoding: 7bit\r\n\r\n"
						.$message."\r\n\r\n"
						."--".$uid."\r\n"
						."Content-Type: application/octet-stream; name=\"".$filename."\"\r\n"
						."Content-Transfer-Encoding: base64\r\n"
						."Content-Disposition: attachment; filename=\"".$filename."\"\r\n\r\n"
						.$content."\r\n\r\n"
						."--".$uid."--";
			$message = '';
		}
		if( mail( $to_email, $subject, $message, $headers ) ){
			return true;
		} else {
			return false;
		}
	}
	
	public static function requestByCurl($remoteServer, $remoteServerPath = '', $requestType = 'GET', $queryString='', $second=60){
		$ch = curl_init();
		if(strlen($remoteServer) > 5 && strtolower(substr($remoteServer,0,5)) == "https" ) {
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		}
		if(!empty($remoteServerPath)){
			$remoteServer .= $remoteServerPath;
		}
		if($requestType == 'GET'){
			$remoteServer .= '?'.$queryString;
		}//echo $remoteServer;//die;
		//curl_setopt($ch, CURLOPT_PORT, 80);
		curl_setopt($ch,CURLOPT_URL,$remoteServer);
		if($requestType == 'POST'){
			curl_setopt($ch,CURLOPT_POSTFIELDS,$queryString);
		}
		curl_setopt($ch,CURLOPT_TIMEOUT,$second); 
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
		curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; zh-CN; rv:1.9.1.2) Gecko/20090729 Firefox/3.5.2');
		$data = curl_exec($ch);
		curl_close($ch);
		return $data;
	}
	
	public static function createSlug($string, $replace = '-', $lower = false){
        return preg_replace('/[\'\/~`\!@#\$%\^&\*\(\)_\-\+=\{\}\[\]\|;:"\<\>,\.\?\\\ ]/', $replace, ($lower ? strtolower($string) : $string));
    }
}



