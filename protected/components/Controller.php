<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='//layouts/column1';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();


    public function beforeAction($action)
    {
        //for chechking up the timeout of logged user
        if ($action->id != 'Signin' && $action->id != 'signout' && Yii::app()->session['logged_in']) {

            if ($action->id != 'forceresetpassword' && Yii::app()->session['logged_user_type'] == 'patient' && Yii::app()->session['force_pw_change'] == 1) {
                $this->redirect(array('/patient/forceresetpassword'));
            }
            elseif (Yii::app()->session['logged_time']) {

                $current_time = time();
                $logged_time = Yii::app()->session['logged_time'];
                $duration = $current_time - $logged_time;
                if ($duration > 600) {
                    Yii::app()->session->destroy();
                    $this->redirect(array('/site/sessionexpired'));
                }
            }
            elseif (($action->id != 'tc') && Yii::app()->session['logged_user_type'] == 'patient'&&Yii::app()->session['force_pw_change'] != 1) {
                $connection = Yii::app()->db;
                $sql = "select * FROM tc_acceptance_tracking WHERE `user_id`=" . Yii::app()->session['logged_user_id'] . "";
                $command = $connection->createCommand($sql);
                $user = $command->queryAll();

                if ($user) {
                    $validity = $user[0]['acceptance_validity'];
                    //86400 secnds is one Day . :)...
                    $validity = $validity * 86400;
                    $accepeted_at = $user[0]['accepted_at'];
                    $time = time();
                    $value = $time - $accepeted_at;

                    if ($value > $validity) {
                        $this->redirect(array('/site/tc'));
                    }
                }

            }


        }













        return true;




    }

    public function afterAction($action)
    {
        //for timeout ->>>> logout action
        if(Yii::app()->session['logged_in']){
            Yii::app()->session['logged_time'] = time();
        }


        $model = new Log ;
        $model->user_id = Yii::app()->session['logged_user_id'];
        $model->user_type = Yii::app()->session['logged_user_type'];
        $model->controller = $this->getId();
        $model->action = $action->id;
        $model->created_at = time();
        $model->ip = Yii::app()->request->getUserHostAddress();
        $model->request =Yii::app()->request->requestUri ;
        if($model->action =='deletemessage'&&$model->controller=='message'){
            $param = current($_REQUEST);
            $model->param = $param;
        }
        if($model->action == 'update' && $model->controller == 'documents'){
            $param = current($_REQUEST);
            $model->param = $param;
        }
        //&&($model->action=='sentMail'||$model->action=='deletemessage'||$model->action=='deletemessage'||$model->action=='deletemessage'||$model->action=='deletemessage'
        if(($model->controller!='site' && $model->action !='404error')&&($model->action=='update'||$model->action=='deletemessage'||$model->action=='deletemessage'||($model->action=='update'||$model->controller =='documents')||($model->action=='forceresetpassword'&&Yii::app()->session['force_pw_change']!=1))){
            $model->save();
        }


        // chechks force pasword reste status..........




        return true;




    }


}