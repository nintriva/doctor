<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'eDoctorBook',


	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
		'application.vendor.S3.*',
        'application.vendor.guzzle.guzzle.src.Guzzle.*',
        'application.vendor.guzzle.guzzle.src.Guzzle.Common.*',
        'application.vendor.guzzle.guzzle.src.Guzzle.Http.*',
        'application.extensions.*',
        'application.extensions.SwiftMailer.*',


		/*'application.vendor.aws.*',
		'application.vendor.aws.aws-sdk-php.*',
		'application.vendor.aws.aws-sdk-php.src.*',
		'application.vendor.aws.aws-sdk-php.src.Aws.*',
		'application.vendor.aws.aws-sdk-php.src.Aws.S3.*',
		'application.vendor.aws.aws-sdk-php.src.Aws.S3.Enum.*',
         'application.vendor.aws.aws-sdk-php.src.Aws.Common.Enum.*',
		'application.vendor.2amigos.resource-manager.*',*/
	),
    'timeZone' => 'US/Pacific',
	'modules'=>array(
		// uncomment the following to enable the Gii tool
		'reports',
        'dashboard',
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'12345',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			//'ipFilters'=>array('127.0.0.1','::1'),
			'ipFilters' => array('127.0.0.1', '192.168.12.74', '192.168.12.66', '61.16.146.185'),
		),
		'yiiadmin'=>array(
                    'password'=>'12345',
                    'registerModels'=>array(
                        //'application.models.Contests',
                        //'application.models.BlogPosts',
                        'application.models.*',
                    ),
                    'excludeModels'=>array(
                        'ContactForm',
                        'LoginForm',
                        'DoctorAddress',
                        'DoctorOffers',
                        'DoctorSchedule',
                        'DoctorScheduleTime',
                        'DoctorTimeoff',                        
                        'DoctorInsurance',
                        'DoctorInsurancePlan',
                        'Procedures',
                        'Conditions',
                        'DoctorVideo',
                        'Contact',
                        //'Doctor',
                        'DoctorBook',
                        'DoctorReview',
                        'Patient',
                        'TodoList',
                        'DoctorReminder',
                        'DoctorNotificationSettings',
                        'DoctorAppointmentSettings'
                    ),
                    'components'=>array(
                        'user'=>array(
                                'allowAutoLogin'=>false,
                        ),
                    ),
                ),
		
	),

	// application components
	'components'=>array(
        'session' => array(
            'class'=>'CDbHttpSession',
            'timeout'=>10,
            'autoStart'=>true,
            'gCProbability' => 100,
        ),
        'resourceManager' => array(
            'class' => 'EAmazonS3ResourceManager',
            'key' => 'AKIAI2XHIZGFTUNBM2VQ',
            'secret' => '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',
            'bucket' => 'nintrivatestapp',
            'region' => 'singapore',
        ),
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>false,
		),
		// uncomment the following to enable URLs in path-format
		'urlManager' => array(
            'urlFormat' => 'path',

            'showScriptName' => false,
            //'urlSuffix' => '.html',
            'rules' => array(
                '/' => '/',
                '<controller:\w+>/<id:\d+>' => '<controller>/<id>',
				'ddb'=>'site/login',
				'dpa'=>'PaAdminDoctors/loginDpa', /* ------------- for parctice Admin -------- IB*/
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<modules>/<controller:\w+>/<id:\d+>' => '<modules>/<controller>/<id>',
                '<modules>/<controller:\w+>/<action:\w+>' => '<modules>/<controller>/<action>',
                '<modules>/<controller:\w+>/<action:\w+>/<id:\d+>' => '<modules>/<controller>/<action>',
				'doctor/<slug:[a-zA-Z-0-9|]+>' => 'doctor/searchDoctorProfile/slug/<slug>',

            ),
        ),

		// MySQL database connection
		
		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=doctor_db1',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => '',
			'charset' => 'utf8',
		),
      /*  'db'=>array(
            'connectionString' => 'mysql:host=localhost;dbname=nintriv4_dp',
            'emulatePrepare' => true,
            'username' => 'nintriv4_jishma',
            'password' => 'R^kJQL~wHWyh',
            'charset' => 'utf8',
        ),*/

        'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// show log messages on web pages
                                /*
                                array(
					'class'=>'CWebLogRoute',
				), */
                            
			),
		),
        'mailer' => array(
            'class' => 'ext.SwiftMailer.SwiftMailer',
            // For SMTP
            'mailer' => 'smtp',
            'host'=>'email-smtp.us-west-2.amazonaws.com',
            'From'=>"support@nintriva.com",
            'username'=>'AKIAIUJBUOAOWPV3YLJA',
            'password'=>'AtWIOMI+U50xr+NE5w6yGTo5xb7oTYLeYwr6LpxNp9Zv',
            'port' => '587',
            'security' => 'tls',
            // For sendmail:
          //  'mailer'=>'sendmail',
        ),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'vijay@edoctorbook.com',
		'dbPrefix' => 'da_',
        'awsAccessKey'=>'AKIAIUMQ4UXYONFR4DMA',
        'awsSecretKey'=>'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',
        'awsBucket'=>'nintrivatestapp',
        'awsEndPoint' =>'s3-ap-southeast-1.amazonaws.com',
        'time'=>10,
        'ServerSideEncryption' => 'AES256',
        'fileEncryptKey' => 'testkey123456'
	),

);