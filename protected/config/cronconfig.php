<?php
/**
 * Created by PhpStorm.
 * User: xavie
 * Date: 10/16/15
 * Time: 4:31 PM
 */
return array(
    // This path may be different. You can probably get it from `config/main.php`.
    'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
    'name'=>'Cron',

    'preload'=>array('log'),

    'import'=>array(
        'application.components.*',
        'application.models.*',
    ),
    // We'll log cron messages to the separate files
    'components'=>array(
        'log'=>array(
            'class'=>'CLogRouter',
            'routes'=>array(
                array(
                    'class'=>'CFileLogRoute',
                    'logFile'=>'cron.log',
                    'levels'=>'error, warning',
                ),
                array(
                    'class'=>'CFileLogRoute',
                    'logFile'=>'cron_trace.log',
                    'levels'=>'trace',
                ),
            ),
        ),

        // Your DB connection
        'db'=>array(
            'connectionString' => 'mysql:host=localhost;dbname=doctor_db1',
            'emulatePrepare' => true,
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
        ),
        /*  'db'=>array(
              'connectionString' => 'mysql:host=localhost;dbname=nintriv4_dp',
              'emulatePrepare' => true,
              'username' => 'nintriv4_jishma',
              'password' => 'R^kJQL~wHWyh',
              'charset' => 'utf8',
          ),*/


    ),
);