<?php
ini_set("display_errors",1);
error_reporting(E_ALL);
// change the following paths if necessary
//$yii=dirname(__FILE__).'/../yii/framework/yii.php';
$yii=dirname(__FILE__).'/framework/yiilite.php';
$yii = dirname(__FILE__).'/core/yii.php';
$config=dirname(__FILE__).'/protected/config/main.php';
require_once( dirname(__FILE__) . '/protected/components/Helpers.php');
// remove the following lines when in production mode
defined('YII_DEBUG') or define('YII_DEBUG',true);
// specify how many levels of call stack should be shown in each log message
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);

session_start();//echo $_SESSION['timeZoneId'];
if(isset($_SESSION['timeZoneId'])){
	//date_default_timezone_set($_SESSION['timeZoneId']);
	date_default_timezone_set("UTC");
}
require_once($yii);
Yii::createWebApplication($config)->run();

