<?php
// Specify the target directory and add forward slash
$path = "/home/edoctor8790/";
// Loop over all of the .txt files in the folder

foreach(glob($path ."EmailReminder.*") as $file) {
    unlink($file); // Delete only .txt files through the loop
}

foreach(glob($path ."EmailSurveyReview.*") as $file) {
    unlink($file); // Delete only .txt files through the loop
}

foreach(glob($path ."AppointmentConfirm.*") as $file) {
    unlink($file); // Delete only .txt files through the loop
}

foreach(glob($path ."AppointmentCancel.*") as $file) {
    unlink($file); // Delete only .txt files through the loop
}

foreach(glob($path ."index.php.*") as $file) {
    unlink($file); // Delete only .txt files through the loop
}

foreach(glob($path ."SmsReminder.*") as $file) {
    unlink($file); // Delete only .txt files through the loop
}

?>